<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class NewsTagsDetail
 */
class NewsTagsDetail extends Model
{
    protected $table = 'news_tags_detail';

    protected $primaryKey = 'tag_id';

	public $timestamps = false;

    protected $fillable = [
        'tag_slug',
        'tag_name',
        'created_date',
        'created_user',
        'updated_date',
        'updated_user',
        'status'
    ];

    protected $guarded = [];

        
}