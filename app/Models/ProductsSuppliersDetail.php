<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductsSuppliersDetail
 */
class ProductsSuppliersDetail extends Model
{
    protected $table = 'products_suppliers_detail';

    protected $primaryKey = 'supplier_id';

	public $timestamps = false;

    protected $fillable = [
        'supplier_code',
        'supplier_name',
        'supplier_address_number',
        'supplier_provinces',
        'supplier_districts',
        'supplier_wards',
        'supplier_email',
        'supplier_phone',
        'supplier_fax',
        'supplier_owner_full_name',
        'supplier_owner_phone',
        'supplier_owner_email',
        'created_date',
        'created_user',
        'updated_date',
        'updated_user',
        'status'
    ];

    protected $guarded = [];

        
}