<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SystemSetting
 */
class SystemSetting extends Model
{
    protected $table = 'system_settings';

    protected $primaryKey = 'setting_id';

	public $timestamps = false;

    protected $fillable = [
        'setting_key',
        'setting_type',
        'setting_values',
        'autoload',
        'status'
    ];

    protected $guarded = [];

        
}