<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CustomersGroup
 */
class CustomersGroup extends Model
{
    protected $table = 'customers_groups';

    protected $primaryKey = 'group_id';

	public $timestamps = false;

    protected $fillable = [
        'group_name',
        'group_description',
        'created_date',
        'created_user',
        'updated_date',
        'updated_user',
        'status'
    ];

    protected $guarded = [];

        
}