<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductsOriginsDetail
 */
class ProductsOriginsDetail extends Model
{
    protected $table = 'products_origins_detail';

    protected $primaryKey = 'origin_id';

	public $timestamps = false;

    protected $fillable = [
        'origin_slug',
        'origin_name',
        'origin_description',
        'created_user',
        'created_date',
        'updated_user',
        'updated_date',
        'status'
    ];

    protected $guarded = [];

        
}