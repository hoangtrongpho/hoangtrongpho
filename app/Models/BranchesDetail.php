<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BranchesDetail
 */
class BranchesDetail extends Model
{
    protected $table = 'branches_detail';

    protected $primaryKey = 'branch_id';

	public $timestamps = false;

    protected $fillable = [
        'branch_code',
        'branch_name',
        'branch_address_number',
        'branch_provinces',
        'branch_districts',
        'branch_wards',
        'branch_phone',
        'branch_fax',
        'created_date',
        'created_user',
        'updated_date',
        'updated_user',
        'status'
    ];

    protected $guarded = [];

        
}