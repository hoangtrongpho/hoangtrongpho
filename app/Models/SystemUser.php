<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SystemUser
 */
class SystemUser extends Model
{
    protected $table = 'system_users';

     protected $primaryKey = 'user_id';

    public $timestamps = false;

    protected $fillable = [
        'user_account',
        'user_email',
        'auth_id',
        'user_avatar',
        'user_fullname',
        'user_sex',
        'user_address',
        'branch_id',
        'province_id',
        'district_id',
        'ward_id',
        'user_phone',
        'user_mobile',
        'user_identification',
        'user_birthday',
        'user_joined_company',
        'user_password',
        'user_login_count',
        'user_secure_code',
        'created_date',
        'created_user',
        'updated_date',
        'updated_user',
        'status'
    ];

    protected $guarded = [];

        
}