<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductsColorsDetail
 */
class ProductsColorsDetail extends Model
{
    protected $table = 'products_colors_detail';

    protected $primaryKey = 'color_id';

	public $timestamps = false;

    protected $fillable = [
        'color_slug',
        'color_name',
        'color_description',
        'color_code',
        'color_duotone',
        'created_user',
        'created_date',
        'updated_user',
        'updated_date',
        'status'
    ];

    protected $guarded = [];

        
}