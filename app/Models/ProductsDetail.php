<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductsDetail
 */
class ProductsDetail extends Model
{
    protected $table = 'products_detail';
    protected $primaryKey = 'product_id';
    public $timestamps = false;

    protected $fillable = [ 
        'product_slug',
        'unit_id',
        'cate_id',
        'origin_id',
        'manufacturer_id',
        'color_id',
        'product_name',
        'product_description',
        'product_specifications',
        'product_imported_prices',
        'product_retail_prices',
        'product_deals',
        'product_ratings',
        'product_thumbnail',
        'product_images_list',
        'product_release_date',
        'product_expiration_date',
        'product_comment_status',
        'product_saleoff_pinned',
        'product_new_pinned',
        'product_quick_pinned',
        'store_quantity',
        'news_seo_title',
        'news_seo_description',
        'news_seo_keywords',
        'created_date',
        'created_user',
        'updated_date',
        'updated_user',
        'status'
    ];

    protected $guarded = [];

        
}