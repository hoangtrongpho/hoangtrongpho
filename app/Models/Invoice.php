<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Invoice
 */
class Invoice extends Model
{
    protected $table = 'invoice';

    protected $primaryKey = 'invoice_id';

	public $timestamps = false;

    protected $fillable = [
        'invoice_number',
        'customer_id',
        'receiver_name',
        'receiver_phone',
        'receiver_address',
        'total_sum',
        'delivery_date',
        'invoice_note',
        'export_invoice_status',
        'payment_method',
        'branch_id',
        'created_date',
        'created_user',
        'updated_date',
        'updated_user',
        'status'
    ];

    protected $guarded = [];

        
}