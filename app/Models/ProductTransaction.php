<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductTransaction
 */
class ProductTransaction extends Model
{
    protected $table = 'product_transactions';

    protected $primaryKey = 'trans_id';

    public $timestamps = false;

    protected $fillable = [
        'product_id',
        'customer_id',
        'trans_date',
        'trans_type',
        'trans_costs',
        'created_date',
        'created_user',
        'updated_date',
        'updated_user',
        'status'
    ];

    protected $guarded = [];

        
}