<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class NewsCategoriesDetail
 */
class NewsCategoriesDetail extends Model
{
    protected $table = 'news_categories_detail';

    protected $primaryKey = 'news_cate_id';

    public $timestamps = false;

    protected $fillable = [
        'news_cate_parent_id',
        'news_cate_slug',
        'news_cate_name',
        'news_cate_description',
        'news_cate_thumbnail',
        'news_cate_date_public',
        'news_cate_orders',
        'created_user',
        'created_date',
        'updated_user',
        'updated_date',
        'status'
    ];

    protected $guarded = [];

        
}