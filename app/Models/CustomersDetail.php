<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CustomersDetail
 */
class CustomersDetail extends Model
{
    protected $table = 'customers_detail';
    
    protected $primaryKey = 'customer_id';

    public $timestamps = false;

    protected $fillable = [
        'group_id',
        'customer_account',
        'customer_email',
        'customer_fullname',
        'customer_address',
        'area_id',
        'customer_phone',
        'customer_identify',
        'customer_student_card',
        'customer_university',
        'customer_avatar',
        'customer_birthday',
        'customer_password',
        'customer_login_count',
        'customer_secure_code',
        'created_date',
        'created_user',
        'updated_date',
        'updated_user',
        'status'
    ];

    protected $guarded = [];

        
}