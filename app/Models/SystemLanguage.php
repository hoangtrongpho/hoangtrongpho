<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SystemLanguage
 */
class SystemLanguage extends Model
{
    protected $table = 'system_languages';

    protected $primaryKey = 'lang_id';

	public $timestamps = false;

    protected $fillable = [
        'lang_type',
        'lang_kind',
        'lang_page',
        'lang_key',
        'lang_value',
        'created_date',
        'created_user',
        'updated_date',
        'updated_user',
        'status'
    ];

    protected $guarded = [];

        
}