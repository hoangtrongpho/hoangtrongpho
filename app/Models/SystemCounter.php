<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SystemCounter
 */
class SystemCounter extends Model
{
    protected $table = 'system_counter';

    protected $primaryKey = 'counter_id';

	public $timestamps = false;

    protected $fillable = [
        'counter_client_ip',
        'counter_date',
        'counter_browser_name_pattern',
        'counter_browser',
        'counter_browser_version',
        'counter_platform',
        'counter_country_code',
        'counter_country_name',
        'counter_region_code',
        'counter_region_name',
        'counter_city',
        'counter_zip_code',
        'counter_latitude',
        'counter_longitude',
        'counter_isp',
        'counter_org',
        'counter_as',
        'counter_time_zone'
    ];

    protected $guarded = [];

        
}