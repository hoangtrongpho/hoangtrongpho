<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductSpecificationsTemplate
 */
class ProductSpecificationsTemplate extends Model
{
    protected $table = 'product_specifications_templates';

    protected $primaryKey = 'spect_id';

	public $timestamps = false;

    protected $fillable = [
        'spect_name',
        'spect_detail',
        'created_date',
        'created_user',
        'updated_date',
        'updated_user',
        'status'
    ];

    protected $guarded = [];

        
}