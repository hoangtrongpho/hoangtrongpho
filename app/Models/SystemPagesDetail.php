<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SystemPagesDetail
 */
class SystemPagesDetail extends Model
{
    protected $table = 'system_pages_detail';

    protected $primaryKey = 'page_id';

    public $timestamps = false;

    protected $fillable = [
        'cate_id',
        'page_slug',
        'page_title',
        'page_content',
        'created_date',
        'created_user',
        'updated_date',
        'updated_user',
        'status'
    ];

    protected $guarded = [];

        
}