<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CompanyRevenueExpenditure
 */
class CompanyRevenueExpenditure extends Model
{
    protected $table = 'company_revenue_expenditure';

    protected $primaryKey = 'cre_id';

	public $timestamps = false;

    protected $fillable = [
        'cre_date',
        'cre_total',
        'cre_type',
        'cre_note',
        'created_date',
        'created_user',
        'updated_date',
        'updated_user',
        'status'
    ];

    protected $guarded = [];

        
}