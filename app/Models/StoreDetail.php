<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class StoreDetail
 */
class StoreDetail extends Model
{
    protected $table = 'store_detail';

    public $timestamps = false;

    protected $fillable = [
        'branch_id',
        'product_id',
        'branch_store_quantity'
    ];

    protected $guarded = [];

        
}