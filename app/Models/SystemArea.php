<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SystemArea
 */
class SystemArea extends Model
{
    protected $table = 'system_areas';

    protected $primaryKey = 'area_id';

    public $timestamps = false;

    protected $fillable = [
        'area_parent_id',
        'area_slug',
        'area_name',
        'area_type',
        'area_order',
        'created_user',
        'created_date',
        'updated_user',
        'updated_date',
        'status'
    ];

    protected $guarded = [];

        
}