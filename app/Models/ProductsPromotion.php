<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductsPromotion
 */
class ProductsPromotion extends Model
{
    protected $table = 'products_promotions';

    protected $primaryKey = 'promotion_id';

    public $timestamps = false;

    protected $fillable = [
        'product_id',
        'promotion_type',
        'promotion_value',
        'promotion_release_date',
        'promotion_expiration_date',
        'created_user',
        'created_date',
        'status'
    ];

    protected $guarded = [];

        
}