<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductsGalleriesDetail
 */
class ProductsGalleriesDetail extends Model
{
    protected $table = 'products_galleries_detail';

    public $timestamps = false;

    protected $fillable = [
        'photo_id',
        'product_id',
        'photo_url',
        'photo_orders',
        'created_user',
        'created_date',
        'status'
    ];

    protected $guarded = [];

        
}