<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class LaravelSession
 */
class LaravelSession extends Model
{
    protected $table = 'laravel_sessions';

    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'ip_address',
        'user_agent',
        'payload',
        'last_activity'
    ];

    protected $guarded = [];

        
}