<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductsUnitsDetail
 */
class ProductsUnitsDetail extends Model
{
    protected $table = 'products_units_detail';

    protected $primaryKey = 'unit_id';

	public $timestamps = false;

    protected $fillable = [
        'unit_slug',
        'unit_name',
        'unit_description',
        'created_user',
        'created_date',
        'updated_user',
        'updated_date',
        'status'
    ];

    protected $guarded = [];

        
}