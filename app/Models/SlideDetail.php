<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SlideDetail
 */
class SlideDetail extends Model
{
    protected $table = 'slide_detail';

    protected $primaryKey = 'slide_id';

	public $timestamps = false;

    protected $fillable = [
        'slide_name',
        'slide_url',
        'slide_img',
        'created_date',
        'created_user',
        'updated_date',
        'updated_user',
        'status'
    ];

    protected $guarded = [];

        
}