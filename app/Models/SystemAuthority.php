<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SystemAuthority
 */
class SystemAuthority extends Model
{
    protected $table = 'system_authorities';

    protected $primaryKey = 'auth_id';

	public $timestamps = false;

    protected $fillable = [
        'auth_name',
        'auth_description',
        'created_date',
        'created_user',
        'updated_date',
        'updated_user',
        'status'
    ];

    protected $guarded = [];

        
}