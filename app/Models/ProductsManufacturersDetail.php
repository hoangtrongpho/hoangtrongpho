<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductsManufacturersDetail
 */
class ProductsManufacturersDetail extends Model
{
    protected $table = 'products_manufacturers_detail';

    protected $primaryKey = 'manufacturer_id';

	public $timestamps = false;

    protected $fillable = [
        'manufacturer_slug',
        'manufacturer_name',
        'manufacturer_description',
        'created_user',
        'created_date',
        'updated_user',
        'updated_date',
        'status'
    ];

    protected $guarded = [];

        
}