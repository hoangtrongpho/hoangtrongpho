<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SystemAreasWard
 */
class SystemAreasWard extends Model
{
    protected $table = 'system_areas_wards';

    protected $primaryKey = 'ward_id';

	public $timestamps = false;

    protected $fillable = [
        'district_id',
        'ward_name',
        'ward_orders',
        'status'
    ];

    protected $guarded = [];

        
}