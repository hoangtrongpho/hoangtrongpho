<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class InvoiceDetail
 */
class InvoiceDetail extends Model
{
    protected $table = 'invoice_detail';

    public $timestamps = false;

    protected $fillable = [
        'invoice_id',
        'product_id',
        'product_quantity',
        'product_retail_prices',
        'total_price'
    ];

    protected $guarded = [];

        
}