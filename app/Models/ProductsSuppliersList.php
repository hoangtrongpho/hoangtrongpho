<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductsSuppliersList
 */
class ProductsSuppliersList extends Model
{
    protected $table = 'products_suppliers_list';

    public $timestamps = false;

    protected $fillable = [
        'product_id',
        'supplier_id'
    ];

    protected $guarded = [];

        
}