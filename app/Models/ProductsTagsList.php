<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductsTagsList
 */
class ProductsTagsList extends Model
{
    protected $table = 'products_tags_list';

    public $timestamps = false;

    protected $fillable = [
        'product_id',
        'tag_id'
    ];

    protected $guarded = [];

        
}