<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductsTagsDetail
 */
class ProductsTagsDetail extends Model
{
    protected $table = 'products_tags_detail';

    protected $primaryKey = 'tag_id';

	public $timestamps = false;

    protected $fillable = [
        'tag_slug',
        'tag_name',
        'tag_description',
        'created_date',
        'created_user',
        'updated_date',
        'updated_user',
        'status'
    ];

    protected $guarded = [];

        
}