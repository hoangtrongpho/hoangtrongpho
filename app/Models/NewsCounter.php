<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class NewsCounter
 */
class NewsCounter extends Model
{
    protected $table = 'news_counter';

    public $timestamps = false;

    protected $fillable = [
        'counter_client_ip',
        'news_id',
        'counter_date',
        'counter_browser_name_pattern',
        'counter_browser',
        'counter_browser_version',
        'counter_platform',
        'counter_country_code',
        'counter_country_name',
        'counter_region_code',
        'counter_region_name',
        'counter_city',
        'counter_zip_code',
        'counter_latitude',
        'counter_longitude',
        'counter_isp',
        'counter_org',
        'counter_as',
        'counter_time_zone'
    ];

    protected $guarded = [];

        
}