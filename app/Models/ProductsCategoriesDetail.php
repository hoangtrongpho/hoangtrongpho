<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductsCategoriesDetail
 */
class ProductsCategoriesDetail extends Model
{
    protected $table = 'products_categories_detail';
    protected $primaryKey = 'cate_id';
    public $timestamps = false;

    protected $fillable = [
        'cate_parent_id',
        'cate_slug',
        'cate_name',
        'cate_description',
        'cate_thumbnail',
        'cate_date_public',
        'cate_orders',
        'cate_pinned',
        'created_user',
        'created_date',
        'updated_user',
        'updated_date',
        'status'
    ];

    protected $guarded = [];

        
}