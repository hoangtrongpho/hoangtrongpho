<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SystemAreasProvince
 */
class SystemAreasProvince extends Model
{
    protected $table = 'system_areas_provinces';

    protected $primaryKey = 'province_id';

	public $timestamps = false;

    protected $fillable = [
        'province_name',
        'province_orders',
        'status'
    ];

    protected $guarded = [];

        
}