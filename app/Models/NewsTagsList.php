<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class NewsTagsList
 */
class NewsTagsList extends Model
{
    protected $table = 'news_tags_list';

    public $timestamps = false;

    protected $fillable = [
        'news_id',
        'news_tag_id'
    ];

    protected $guarded = [];

        
}