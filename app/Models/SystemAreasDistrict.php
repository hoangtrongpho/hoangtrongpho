<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SystemAreasDistrict
 */
class SystemAreasDistrict extends Model
{
    protected $table = 'system_areas_districts';

    protected $primaryKey = 'district_id';

	public $timestamps = false;

    protected $fillable = [
        'province_id',
        'district_name',
        'district_orders',
        'status'
    ];

    protected $guarded = [];

        
}