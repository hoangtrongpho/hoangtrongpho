<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CompanyInformation
 */
class CompanyInformation extends Model
{
    	protected $table = 'company_information';

		protected $primaryKey = 'info_id';

		public $timestamps = false;

		protected $fillable = [
		  	'info_key',
		  	'info_values'
		];

		protected $guarded = [];

        
}