<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class NewsDetail
 */
class NewsDetail extends Model
{
    protected $table = 'news_detail';

    protected $primaryKey = 'news_id';

    public $timestamps = false;

    protected $fillable = [
        'cate_id',
        'news_slug',
        'news_title',
        'news_description',
        'news_content',
        'news_author_aliases',
        'news_ratings',
        'news_thumbnail',
        'news_release_date',
        'news_expiration_date',
        'news_format_page',
        'news_comment_status',
        'news_download',
        'news_seo_title',
        'news_seo_description',
        'news_seo_keywords',
        'created_date',
        'created_user',
        'updated_date',
        'updated_user',
        'status',
        'visibility'
    ];

    protected $guarded = [];

        
}