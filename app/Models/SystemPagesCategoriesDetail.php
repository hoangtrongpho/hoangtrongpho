<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SystemPagesCategoriesDetail
 */
class SystemPagesCategoriesDetail extends Model
{
    protected $table = 'system_pages_categories_detail';

    protected $primaryKey = 'cate_id';

	public $timestamps = false;

    protected $fillable = [
        'cate_slug',
        'cate_name',
        'cate_description',
        'created_user',
        'created_date',
        'updated_user',
        'updated_date',
        'status'
    ];

    protected $guarded = [];

        
}