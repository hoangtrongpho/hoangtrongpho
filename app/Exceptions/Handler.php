<?php

namespace App\Exceptions;

use Exception;
use Lang;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if($e instanceof PDOException)
        {
            return response()->view('front_end.errors.500');
        }

        //Trường hợp phát sinh lỗi 
        if ($this->isHttpException($e)) {

            $statusCode = $e->getStatusCode();

            $module = 'System';
            $page   = $request->url();
            $checkModule = strpos($page, $module);

            $moduleApi = 'SystemApi';
            $checkModuleApi = strpos($page, $moduleApi);

            if(preg_match('/SystemApi/',$page))
            {
                //Trường hợp gọi SystemApi
                switch ($statusCode) {
                    case '403':
                        $response["systemerror"] = Lang::get('messages.common_error_403');
                        $response["errorCode"] = 404;
                        return response()->json($response);
                    case '404':
                        $response["systemerror"] = Lang::get('messages.common_error_404');
                        $response["errorCode"] = 404;
                        return response()->json($response);
                    case '500':
                        $response["systemerror"] = Lang::get('messages.common_error_500');
                        $response["errorCode"] = 500;
                        return response()->json($response);
                    default:
                        $response["systemerror"] = "Hệ Thống Đang Được Bảo Trì";
                        $response["errorCode"] = "systemdown";
                        return response()->json($response);
                }
            }
            else if(preg_match('/System/',$page))
            {
                //Trường hợp tải trang trong back-end
                switch ($statusCode) {
                    case '403':
                        return response()->view('system.errors.403');
                    case '404':
                        return response()->view('system.errors.404');
                    case '500':
                        return response()->view('system.errors.500');
                    default:
                        return response()->view('system.errors.systemdown');
                }
            }
            else if(preg_match('//',$page))
            {
                //Trường hợp tải trang trong back-end
                switch ($statusCode) {
                    case '403':
                        return response()->view('system.errors.403');
                    case '404':
                        return response()->view('system.errors.404');
                    case '500':
                        return response()->view('system.errors.500');
                    default:
                        return response()->view('system.errors.systemdown');
                }
            }
            else
            {
                //Trường hợp tải trang trong front-end
                switch ($statusCode) {
                    case '403':
                        return response()->view('front_end.errors.403');
                    case '404':
                        return response()->view('front_end.errors.404');
                    case '500':
                        return response()->view('front_end.errors.500');
                    default:
                        return response()->view('front_end.errors.systemdown');
                }
            }
        }

        return parent::render($request, $e);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest('login');
    }
}
