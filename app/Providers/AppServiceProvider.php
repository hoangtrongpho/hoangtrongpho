<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //

        //Laravel 5 model generator for an existing schema.
        /* Options:
         * --dir="" Model directory (default: "Models/")
         * --extends="" Parent class (default: "Model")
         * --fillable="" Rules for $fillable array columns (default: "")
         * --guarded="" Rules for $guarded array columns (default: "ends:_id|ids,equals:id")
         * --timestamps="" Rules for $timestamps columns (default: "ends:_at")
         * --ignore=""|-i="" A table names to ignore
         * --ignoresystem|-s List of system tables (auth, migrations, entrust package)
         * --tables="" Tables to generate (Ex: --tables="my_db_table1,my_db_table2"
         * command line: php artisan make:models
         */
        if ($this->app->environment() == 'local') {
            $this->app->register('Iber\Generator\ModelGeneratorProvider');
        }
    }
}
