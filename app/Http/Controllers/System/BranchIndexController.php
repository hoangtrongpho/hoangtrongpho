<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\System\SystemController;

use DB;
use Session;
use Redirect;

class BranchIndexController extends SystemController
{

    protected function indexListBranches(Request $request)
    {
        // $auth = $this->checkSystemAuth($request);
        // if($auth["auth"])
        // {
        //     $request->session()->put('errorMessage', $auth["message"]);
        //     return $this->requestLogout($request);
        // }

        return view('system.branchesmodule.listbranches');
    }

    protected function indexListDepartments(Request $request)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }

        return view('system.branchesmodule.listdepartments');
    }

    protected function indexListPotitions(Request $request)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }

        return view('system.branchesmodule.listpositions');
    }
}