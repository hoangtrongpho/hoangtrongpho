<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\System\SystemController;

use DB;
use Session;
use Redirect;

class NewsIndexController extends SystemController
{
    //Hiển thị trang danh sách tin tức
    protected function indexListNews(Request $request)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }
        return view('system.newsmodule.listnews');
    }

    //Hiển thị trang thêm mới tin tức
    protected function indexCreateNews(Request $request)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }

        return view('system.newsmodule.createnews')
            ->with('slug', null);
    }

    //Hiển thị trang cập nhật tin tức
    protected function indexUpdateNews(Request $request, $slug)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }

        return view('system.newsmodule.createnews')
            ->with('slug', $slug);

    }

    //Hiển thị trang danh sách danh mục tin
    protected function indexListNewsCategories(Request $request)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }
        
        return view('system.newsmodule.listnewscategories');
    }

    //Hiển thị trang danh sách thẻ Tags
    protected function indexListNewsTags(Request $request)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }
        
        return view('system.newsmodule.listnewstags');
    }

    //Hiển thị trang danh sách bình luận
    protected function indexListNewsComments(Request $request)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }
        
        return view('system.newsmodule.listnewscomments');
    }

    //Hiển thị thống kê tổng quan
    protected function indexStatisticSummary(Request $request)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }
        
        return view('system.newsmodule.statisticsummary');
    }

    //Hiển thị thống kê bài viết
    protected function indexStatisticNews(Request $request)
    {
       $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }

        return view('system.newsmodule.statisticnews');
    }


}
