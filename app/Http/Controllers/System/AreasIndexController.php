<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\System\SystemController;

use DB;
use Session;
use Redirect;

class AreasIndexController extends SystemController
{

    protected function indexListAreas(Request $request)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }

        return view('system.areasmodule.listareas');
    }



}