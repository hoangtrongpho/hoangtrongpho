<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\System\SystemController;

use Cookie;
use DB;
use Debugbar;
use Session;
use Redirect;

class SystemIndexController extends SystemController
{
	//Hiển thị trang đăng nhập
    protected function indexLogin(Request $request)
    {
        $errorMessage = $request->session()->get('errorMessage');

        $request->session()->forget('errorMessage');

        Debugbar::info(Cookie::get('userAuth'));
        
        if(Cookie::get('userAuth') !== null)
        {
            $request->session()->put('lockScreen', false);
            $request->session()->put('userAuth', Cookie::get('userAuth'));

            return Redirect::to('System/Dashboard'); 
        }

    	return view('system.login')
             ->with('errorMessage', $errorMessage);
    }

    //Đăng xuất khỏi hệ thống
    protected function indexLogout(Request $request)
    {
        return $this->requestLogout($request);
    }

    //Hiển thị trang dashboard
    protected function indexDashboard(Request $request){ 

        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }

    	return view('system.dashboard');
    }

    //Hiển thị trang khóa màn hình
    protected function indexLockScreen(Request $request){ 

        Session::put('lockScreen', true);

        if(Session::has('userAuth') && Session::has('lockScreen') && Session::get('lockScreen') == true)
        {
            $userAuth = Session::get('userAuth');
            return view('system.lockscreen', ['userAuth' => $userAuth]);
        }

        return Redirect::to('System/Logout'); 
    }


    //Hiển thị trang dashboard
    protected function indexInfoCompany(Request $request){ 

        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }

        return view('system.dashboard');
    }
}
