<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\System\SystemController;

use DB;
use Session;
use Redirect;

class PageIndexController extends SystemController
{
    //Hiển thị trang danh sách tin tức
    protected function indexListPages(Request $request)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }
        return view('system.pagemodule.listpages');
    }

    //Hiển thị trang thêm mới tin tức
    protected function indexCreatePage(Request $request)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }

        return view('system.pagemodule.createpage')
            ->with('slug', null);
    }

    //Hiển thị trang cập nhật tin tức
    protected function indexUpdatePage(Request $request, $slug)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }

        return view('system.pagemodule.createpage')
            ->with('slug', $slug);

    }
}
