<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\System\SystemController;

use DB;
use Session;
use Redirect;

class StatisticIndexController extends SystemController
{
    
    protected function indexStatisticAccess(Request $request)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }

        return view('system.statisticalmodule.statisticaccess');
    }
    protected function indexSaleReport(Request $request)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }
        if(Session::get('userAuth')->auth_id !=1 ){
            return Redirect::to('System/Dashboard'); 
        }
       
        return view('system.statisticalmodule.salereport');
    }
    protected function indexCustomerReport(Request $request)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }
        
        return view('system.statisticalmodule.customerreport');
    }
    protected function indexPrivateReport(Request $request)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }

        return view('system.statisticalmodule.privatereport');
    }
    protected function indexZingChartTest(Request $request)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }

        return view('system.statisticalmodule.zingcharttest');
    }
    protected function indexDashboard2(Request $request)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }

        return view('system.dashboardmodule.dashboard2');
    }
}
