<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\System\SystemController;

use DB;
use Session;
use Redirect;

class InvoicesIndexController extends SystemController
{
    //Hiển thị trang danh sách
    protected function indexListInvoices(Request $request)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }

        return view('system.invoicemodule.listinvoices');
    }
    //Hiển thị trang danh sách
    protected function indexListPrivateInvoices(Request $request)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }

        return view('system.invoicemodule.listprivateinvoices');
    }
    //Hiển thị indexCreateInvoice
    protected function indexCreateInvoice(Request $request)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }

        return view('system.invoicemodule.createinvoice')
             ->with('slug', null);
    }
    //indexUpdateInvoice
    protected function indexUpdateInvoice(Request $request, $slug)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }

        return view('system.invoicemodule.createinvoice')
             ->with('slug', $slug);
    }

}
