<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\System\SystemController;

use DB;
use Session;
use Redirect;

class UserIndexController extends SystemController
{
    
    //Hiển thị trang danh sách tài khoản
    protected function indexListUsers(Request $request)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }
        if(Session::get('userAuth')->auth_id !=1 ){
            return Redirect::to('System/Dashboard'); 
        }
        return view('system.usermodule.userlist');
    }

    //Hiển thị trang danh sách tài khoản
    protected function indexProfile(Request $request)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }
        if(Session::get('userAuth')->auth_id !=1 ){
            return Redirect::to('System/Dashboard'); 
        }
        return view('system.usermodule.profile');
    }

}
