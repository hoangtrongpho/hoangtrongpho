<?php

namespace App\Http\Controllers\System;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Cookie;
use DB;
use Lang;
use Redirect;
use Session;
use Debugbar;

class SystemController extends BaseController
{
   use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

   private $listFunction = array(
      "Logout"  => [
         "roles" => [ 1, 2, 3]
      ],
      //Trang Dashboard
      "Dashboard"  => [
         "roles" => [ 1, 2, 3]
      ],
      "Dashboard2"  => [
         "roles" => [ 1, 2, 3]
      ],
      //Quản Trị Tin Tức
      "ListNews"  => [
         "roles" => [ 1, 2, 3]
      ],
      "CreateNews"  => [
         "roles" => [ 1, 2, 3]
      ],
      "UpdateNews"  => [
         "roles" => [ 1, 2, 3]
      ],
      //Quản Trị Thể Loại Tin Tức
      "ListNewsCategories"  => [
         "roles" => [ 1, 2, 3]
      ],
      // //Quản Trị Thẻ Tags Tin tức
      // "ListNewsTags"  => [
      //    "roles" => [ 1, 2, 3]
      // ],
      //Quản Trị Hình Ảnh
      "GalleryLirary"  => [
         "roles" => [ 1, 2, 3]
      ],
      //Quản Trị Tài Khoản
      "ListUsers"  => [
         "roles" => [ 1, 2, 3]
      ],
      "Profile"  => [
         "roles" => [ 1, 2, 3]
      ],
      //Thống Kê
      "StatisticAccess"  => [
         "roles" => [ 1, 2, 3]
      ],
      //Quản Trị Khu Vực
      "ListProvinces"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SaleReport"  => [
         "roles" => [ 1, 2, 3]
      ],
      "CustomerReport"  => [
         "roles" => [ 1, 2, 3]
      ],
      "PrivateReport"  => [
         "roles" => [ 1, 2, 3]
      ],
      "ZingChartTest"  => [
         "roles" => [ 1, 2, 3]
      ],
      /*** Sản Phẩm Hàng Hóa ***/
      "ListProductCategories"  => [
         "roles" => [ 1, 2, 3]
      ],
      "ExportProducts"  => [
         "roles" => [ 1, 2, 3]
      ],
      "ListProducts"  => [
         "roles" => [ 1, 2, 3]
      ],
      "CreateProduct"  => [
         "roles" => [ 1, 2, 3]
      ],
      "UpdateProduct"  => [
         "roles" => [ 1, 2, 3]
      ],

      "ListProductUnits"  => [
         "roles" => [ 1, 2, 3]
      ],
      "ListProductColors"  => [
         "roles" => [ 1, 2, 3]
      ],
      "ListProductOrigins"  => [
         "roles" => [ 1, 2, 3]
      ],
      "ListProductTags"  => [
         "roles" => [ 1, 2, 3]
      ],
      "ListAreas"  => [
         "roles" => [ 1, 2, 3]
      ],
      "ListManufacturers"  => [
         "roles" => [ 1, 2, 3]
      ],
        
      "ListPages"  => [
         "roles" => [ 1, 2, 3]
      ],
      "CreatePages"  => [
         "roles" => [ 1, 2, 3]
      ],
      "UpdatePages"  => [
         "roles" => [ 1, 2, 3]
      ],

      "ListCustomers"  => [
         "roles" => [ 1, 2, 3]
      ],
      "CreateCustomer"  => [
         "roles" => [ 1, 2, 3]
      ],
      "UpdateCustomer"  => [
         "roles" => [ 1, 2, 3]
      ],

      "ListPages"  => [
         "roles" => [ 1, 2, 3]
      ],
      "CreatePage"  => [
         "roles" => [ 1, 2, 3]
      ],
      "UpdatePage"  => [
         "roles" => [ 1, 2, 3]
      ],

      "WebConfig"  => [
         "roles" => [ 1, 2, 3]
      ],
      "InfomationCompany"  => [
         "roles" => [ 1, 2, 3]
      ],
      "ListBranch"  => [
         "roles" => [ 1, 2, 3]
      ],
      "RemainStore"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SaleStore"  => [
         "roles" => [ 1, 2, 3]
      ],
      "PrivateRemainStore"  => [
         "roles" => [ 1, 2, 3]
      ],
      "PrivateSaleStore"  => [
         "roles" => [ 1, 2, 3]
      ],
      "ImportExportStore"  => [
         "roles" => [ 1, 2, 3]
      ],
      "ListInvoices"  => [
         "roles" => [ 1, 2, 3]
      ],
      "CreateInvoice"  => [
         "roles" => [ 1, 2, 3]
      ],
      "UpdateInvoice"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemInformation"  => [
         "roles" => [ 1, 2, 3]
      ],
      "ListPrivateInvoices"  => [
         "roles" => [ 1, 2, 3]
      ],
      
   );

    //Check đăng nhập
    public function checkSystemAuth($request)
    {
        //Tạo biến lưu kết quả kiểm tra
      $result["auth"] = "";
      // $result["redirect"] = "";
      $result["message"] = "";

      $sessionUser = null; 
      if(Cookie::get('userAuth') !== null)
      {
         $sessionUser = Cookie::get('userAuth');
      }
      else if($request->session()->has('userAuth'))
      {
         $sessionUser = $request->session()->get('userAuth'); 
      }

      if($sessionUser)
      {  //Trường hợp đã đăng nhập

         $userInfo = DB::table('system_users')
                      ->leftJoin('system_authorities', 'system_users.auth_id', '=', 'system_authorities.auth_id')
                      ->select('user_id'
                              ,'user_fullname'
                              ,'user_account'
                              ,'user_avatar'
                              ,'system_authorities.auth_id'
                              ,'auth_name'
                              ,'user_avatar'
                              ,'user_sex'
                              ,'system_users.status')
                  ->where(function($query) use ($sessionUser)
                  {
                     $query->where('user_account', '=', $sessionUser->user_account);
                     $query->where('system_users.status', '<>', 0);
                  })
                  ->first();

         if($userInfo)
         {  //Trường hợp tài khoản hợp lệ
            Session::put('lockScreen', false);
            Session::put('userAuth', $userInfo);

            $url  = $request->path();
            $module = explode("/", $url);

            // Debugbar::info($this->listFunction[$module]);

            if (array_key_exists($module[1], $this->listFunction)) 
            {  //Trường hợp chức năng tồn tại trong hệ thống
               $roles = $this->listFunction[$module[1]]["roles"];

               if(in_array($userInfo->auth_id, $roles))
               {  //Trường hợp có quyền sử dụng chức năng
                  //Có quyền truy xuất API
                  $result["auth"] = false;
               }
               else
               {  //Trường hợp không có quyền sử dụng chức năng
                  //Không có quyền truy xuất API
                  $result["auth"] = true;
                  // $result["redirect"] = $postback;
                  $result["message"] = Lang::get('messages.common_error_permission_api');
               }
            }
            else
            {  //Trường hợp chức năng không tồn tại trong hệ thống
               //Không có quyền truy xuất API
               $result["auth"] = true;
               // $result["redirect"] = $postback;
               $result["message"] = Lang::get('messages.common_error_permission_api');
            }
         }
         else
         {  //Trường hợp tài khoản đã bị khóa hoặc tài khoản không tồn tại
            //Không có quyền truy xuất API
            $result["auth"] = true;
            // $result["redirect"] = "/Login";
            $result["message"] = Lang::get('messages.common_error_user_not_exists');
         }
      }
      else
      {  //Trường hợp chưa đăng nhập
         //Không có quyền truy xuất API
         $result["auth"] = true;
         // $result["redirect"] = "/Login";
         $result["message"] = "Hãy đăng nhập trước khi sử dụng chức năng này.";
      }

      return $result;
   }

   //Check đăng nhập
   public function requestLogout($request)
   {
      $request->session()->forget('lockScreen');
      $request->session()->forget('userAuth');

      $cookieUserAuth = Cookie::forget('userAuth');

      return Redirect::to('/System/Login')->withCookie($cookieUserAuth); 
   }
}
