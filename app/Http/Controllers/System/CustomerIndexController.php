<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\System\SystemController;

use DB;
use Session;
use Redirect;

class CustomerIndexController extends SystemController
{
    protected function indexListCustomers(Request $request)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }

        return view('system.customermodule.listcustomers');
    }

    //Hiển thị trang thêm 
    protected function indexCreateCustomer(Request $request)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }

        return view('system.customermodule.createcustomer')
             ->with('slug', null);
    }

    //Hiển thị trang cập nhật 
    protected function indexUpdateCustomer(Request $request, $slug)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }

        return view('system.customermodule.createcustomer')
               ->with('slug', $slug);
    }

}
