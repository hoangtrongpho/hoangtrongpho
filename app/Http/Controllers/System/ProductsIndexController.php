<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\System\SystemController;

use DB;
use Session;
use Redirect;

class ProductsIndexController extends SystemController
{
    //Hiển thị trang danh sách nhà sản xuất
    protected function indexListCategories(Request $request)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }

        return view('system.productmodule.listcategoires');
    }

    //Hiển thị trang danh sách đơn vị sản phẩm
    protected function indexListUnits(Request $request)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }

        return view('system.productmodule.listunits');
    }

    //Hiển thị trang danh sách màu sắc sản phẩm
    protected function indexListColors(Request $request)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }

        return view('system.productmodule.listcolors');
    }

    //Hiển thị trang danh sách xuất xứ sản phẩm
    protected function indexListOrigins(Request $request)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }

        return view('system.productmodule.listorigins');
    }

    //Hiển thị trang danh sách thẻ sản phẩm
    protected function indexListProductTags(Request $request)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }

        return view('system.productmodule.listtags');
    }

    //Hiển thị trang danh sách nhà sản xuất
    protected function indexListManufacturers(Request $request)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }

        return view('system.productmodule.listmanufacturers');
    }

    //Hiển thị trang danh sách sản phẩm
    protected function indexListProducts(Request $request)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }

        return view('system.productmodule.listproducts');
    }

    //Hiển thị trang thêm sản phẩm
    protected function indexCreateProduct(Request $request)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }

        return view('system.productmodule.createproduct')
             ->with('slug', null);
    }

    //Hiển thị trang cập nhật sản phẩm
    protected function indexUpdateProduct(Request $request,$slug)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }

        return view('system.productmodule.createproduct')
             ->with('slug', $slug);
    }

}
