<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\System\SystemController;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

use App\Http\Controllers\SystemApi\SystemCommonController as commonCtl;

use DB;
use Session;
use Redirect;

class GalleryIndexController extends SystemController
{
    /*** Khởi Tạo Giá Trị ***/
    private $commonCtl;

    private $sessionUser;

    /*** Khởi Tạo Giá Trị ***/
    public function __construct(commonCtl $SystemCommonController){
        $this->commonCtl =  $SystemCommonController;
    }

    protected function indexGalleryLirary(Request $request)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }
        $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        return view('system.gallerymodule.gallerylirary');
    }


}
