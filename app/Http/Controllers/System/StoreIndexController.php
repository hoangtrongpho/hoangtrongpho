<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\System\SystemController;

use DB;
use Session;
use Redirect;

class StoreIndexController extends SystemController
{
    protected function indexRemainStore(Request $request)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }
        if(Session::get('userAuth')->auth_id !=1 ){
            return Redirect::to('System/Dashboard'); 
        }
        return view('system.storemodule.remainstore');
    }
    protected function indexSaleStore(Request $request)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }
        if(Session::get('userAuth')->auth_id !=1 ){
            return Redirect::to('System/Dashboard'); 
        }
        return view('system.storemodule.salestore');
    }
    protected function indexImportExportStore(Request $request)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }
        if(Session::get('userAuth')->auth_id !=1 ){
            return Redirect::to('System/Dashboard'); 
        }
        return view('system.storemodule.importexportstore');
    }
    //  ===============private at SUB admin===================
    protected function indexPrivateRemainStore(Request $request)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }

        return view('system.storemodule.privateremainstore');
    }
    protected function indexPrivateSaleStore(Request $request)
    {
        $auth = $this->checkSystemAuth($request);
        if($auth["auth"])
        {
            $request->session()->put('errorMessage', $auth["message"]);
            return $this->requestLogout($request);
        }

        return view('system.storemodule.privatesalestore');
    }
}