<?php

namespace App\Http\Controllers\Website;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Cookie;
use DB;
use Lang;
use Redirect;
use Session;
use Request;
use \DateTime;

use App\Models\SystemCounter;

class WebsiteController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    //Check đăng nhập
    public function checkSystemAuth(){

        if(Session::has('userAuth'))
        {
            if(Session::has('lockScreen') && Session::get('lockScreen') == false)
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }
        else
        {
            return 0;
        }
    }

    //Check đăng nhập
    public function checkWebsiteAuth($request)
    {
        //Tạo biến lưu kết quả kiểm tra
      $result["auth"] = "";
      // $result["redirect"] = "";
      $result["message"] = "";

      $sessionUser = null; 
      if(Cookie::get('agencyAuth') !== null)
      {
         $sessionUser = Cookie::get('agencyAuth');
      }
      else if($request->session()->has('agencyAuth'))
      {
         $sessionUser = $request->session()->get('agencyAuth'); 
      }

      if($sessionUser)
      {  //Trường hợp đã đăng nhập

         $agencyInfo = DB::table('agencies_detail')
                ->select('agency_id', 
                         'agency_code',
                         'agency_name', 
                         'agency_address_number', 
                         'agency_provinces',
                         'agency_districts',  
                         'agency_wards',
                         'agency_phone', 
                         'agency_owner_full_name', 
                         'agency_owner_birthday',
                         'agency_owner_email')
                ->where(function($query) use ($sessionUser)
                {
                    $query->where('agency_id', '=', $sessionUser->agency_id);
                    $query->where('status', '<>', 0);
                })
                ->first();

         if($agencyInfo)
         {  //Trường hợp tài khoản hợp lệ
            //Có quyền truy xuất API
            Session::put('agencyAuth', $agencyInfo);
             $result["auth"] = false;
         }
         else
         {  //Trường hợp tài khoản đã bị khóa hoặc tài khoản không tồn tại
            //Không có quyền truy xuất API
            $result["auth"] = true;
            // $result["redirect"] = "/Login";
            $result["message"] = Lang::get('messages.common_error_user_not_exists');
         }
      }
      else
      {  //Trường hợp chưa đăng nhập
         //Không có quyền truy xuất API
         $result["auth"] = true;
         // $result["redirect"] = "/Login";
         $result["message"] = "Bạn chưa đăng nhập, hãy đăng nhập để sử dụng hệ thống.";
      }

      return $result;
   }

    // //Check đăng nhập
    // public function checkWebsiteAuth(){

    //     if(Session::has('agencyAuth'))
    //     {
    //         return false;
    //     }
    //     else
    //     {
    //         return true;
    //     }
    // }

    //Check đăng nhập
    public function requestLogout($request)
    {
        $request->session()->forget('agencyAuth');

        $request->session()->forget('changePassword');
        $request->session()->forget('userForChange');

        $cookieUserAuth = Cookie::forget('agencyAuth');

        return Redirect::to('dang-nhap')->withCookie($cookieUserAuth); 
    }

    //Thêm Lượt Truy Cập
    protected function frontEndsCounter(){
        //Khởi Tạo Dữ Liệu
        $counterData = new SystemCounter();

        //Get IP máy client
        if(Request::ip() !== "::1")
        {
            $counterData->counter_client_ip = Request::ip();
        }
        else
        {
            $counterData->counter_client_ip = "127.0.0.1";
        }


        //Get Ngày Tháng Năm Hiện Tại
        $counterData->counter_date = new DateTime();

        // if (get_cfg_var('browscap'))
        // require_once($_SERVER['DOCUMENT_ROOT']."/includes/php-local-browscap.php");
        // $browser=get_browser_local(null, true);
        // }

        // $browser = get_browser(null, true);

        // $pos_1 = strpos(strtoupper($browser["browser"]), "BOT");
        // $pos_2 = strpos(strtoupper($browser["browser"]), "APP");
        // $pos_3 = strpos(strtoupper($browser["browser"]), "CURL");
        // $pos_4 = strpos(strtoupper($browser["browser"]), "JAVA");
        // $pos_5 = strpos(strtoupper($browser["browser"]), "FACEBOOK");
        // $pos_6 = strpos(strtoupper($browser["browser"]), "GOOGLE");
        // $pos_7 = strpos(strtoupper($browser["browser"]), "YAHOO");
        // $pos_8 = strpos(strtoupper($browser["browser"]), "VKSHARE");
        // $pos_9 = strpos(strtoupper($browser["browser"]), "XML");
        // $pos_10 = strpos(strtoupper($browser["browser"]), "SEO");

        // if($pos_1 || $pos_2 || $pos_3 || $pos_4 || $pos_5 || $pos_6 || $pos_7 || $pos_8 || $pos_9 || $pos_10)
        // {
        //     return false;
        // }

        // $counterData->counter_browser_name_pattern = $browser["browser_name_pattern"];
        // $counterData->counter_browser = $browser["browser"];
        // $counterData->counter_browser_version = $browser["version"];
        // $counterData->counter_platform = $browser["platform"];

        //$url="http://freegeoip.net/json/".$counterData->counter_client_ip;
        $url="http://ip-api.com/json/".$counterData->counter_client_ip;
        //  Initiate curl
        $ch = curl_init();
        // Disable SSL verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // Will return the response, if false it print the response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Set the url
        curl_setopt($ch, CURLOPT_URL,$url);
        // Execute
        $data=curl_exec($ch);
        // Closing
        curl_close($ch);

        $result = json_decode($data,true);

        if($result["status"] == "success")
        {
            $counterData->counter_country_code = $result["countryCode"];
            $counterData->counter_country_name = $result["country"];
            $counterData->counter_region_code = $result["region"];
            $counterData->counter_region_name = $result["regionName"];
            $counterData->counter_city = $result["city"];
            $counterData->counter_zip_code = $result["zip"];
            $counterData->counter_latitude = $result["lat"];
            $counterData->counter_longitude = $result["lon"];
            $counterData->counter_time_zone = $result["timezone"];

            $counterData->counter_isp = $result["isp"];
            $counterData->counter_org = $result["org"];
            $counterData->counter_as = $result["as"];
            
            // Start transaction!
            DB::beginTransaction();

            try 
            {
                $counterData->save();
            } 
            catch(\Exception $e)
            {

                DB::rollback();
                abort(403, 'Unauthorized action.'.$e);
            }

            DB::commit();
        }
    }
}
