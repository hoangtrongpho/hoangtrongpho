<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Http\Requests;

use Cookie;
use DB;
use Carbon;
use Config;
use Mail;
use Lang;
use DateTime;
use Session;
use Redirect;

use App\Http\Controllers\SystemApi\SystemCommonController as commonCtl;

use App\Models\NewsDetail;
use App\Models\NewsCategoriesDetail;
use App\Models\NewsCategoriesList;
use App\Models\NewsTagsDetail;
use App\Models\NewsTagsList;
use App\Models\NewsComment;
use App\Models\NewsCounter;
use App\Models\ProductsCategoriesDetail;

class HomePageController extends WebsiteController
{
    private $commonCtl;

    private $sessionUser;

    public function __construct(commonCtl $SystemCommonController){
        $this->commonCtl =  $SystemCommonController;
    }
    protected function indexHome(Request $request)
    {
        Carbon\Carbon::setLocale('vi');
        return view('front_end.home');
    }
    protected function indexHomePage(Request $request)
    {
        // ===============listCategories==================
        $listCategories = array();
        $whereFunctions = array(
            ['cate_parent_id', '=', 0],
            ['status', '<>', 0]
        );
        $listParent = DB::table('products_categories_detail')
                          ->select('cate_id', 
                                   'cate_parent_id',
                                   'cate_slug',
                                   'cate_name', 
                                   'cate_description',
                                   'cate_thumbnail',
                                   'cate_orders',
                                   'cate_pinned')
                          ->where($whereFunctions)
                          ->orderBy('cate_orders', 'asc')
                          ->get();
        $listProducts = DB::table('products_detail')
                          ->select('cate_id',
                                    'product_slug', 
                                   'product_id',
                                   'product_name',
                                   'product_thumbnail', 
                                   'product_imported_prices',
                                   'product_retail_prices',
                                   'product_description',
                                   'product_saleoff_pinned',
                                   'product_new_pinned')
                          ->where('status', '<>', 0)
                          ->get();
        $whereFunctions = array(
            ['cate_parent_id', '<>', 0],
            ['status', '<>', 0]
        );
        $listChild = DB::table('products_categories_detail')
                          ->select('cate_id', 
                                   'cate_parent_id',
                                   'cate_slug',
                                   'cate_name', 
                                   'cate_description',
                                   'cate_thumbnail',
                                   'cate_orders',
                                   'cate_pinned')
                          ->where($whereFunctions)
                          ->orderBy('cate_orders', 'asc')
                          ->get();

        foreach ($listParent as $value_p) {

            $itemParent = [
                "cate_id" => $value_p->cate_id,
                "cate_parent_id" => $value_p->cate_parent_id,
                "cate_slug" => $value_p->cate_slug,
                "cate_name" => $value_p->cate_name,
                "cate_description" => $value_p->cate_description,
                "cate_thumbnail" => $value_p->cate_thumbnail,
                "cate_orders" => $value_p->cate_orders,
                "cate_pinned" => $value_p->cate_pinned,
                "nodes" => array(),
                "product_list" => array()
            ];
            // foreach add con 1
            foreach ($listChild as $value_c) {
                // nếu cate_parent_id = cate_id
                if($value_c->cate_parent_id == $value_p->cate_id)
                {
                    $itemChild = [
                        "cate_id" => $value_c->cate_id,
                        "cate_parent_id" => $value_p->cate_id,
                        "cate_slug" => $value_c->cate_slug,
                        "cate_name" => $value_c->cate_name,
                        "cate_description" => $value_c->cate_description,
                        "cate_thumbnail" => $value_c->cate_thumbnail,
                        "cate_orders" => $value_c->cate_orders,
                        "cate_pinned" => $value_p->cate_pinned,
                        "nodes" => array()
                    ];
                    // find product con 1
                    // find product con value_p ------------1-------------
                    foreach ($listProducts as $pr) {
                        if($value_c->cate_id == $pr->cate_id)
                        {
                            $itemPro = [
                                "product_slug" => $pr->product_slug,
                                "product_id" => $pr->product_id,
                                "product_name" => $pr->product_name,
                                "product_thumbnail" => $pr->product_thumbnail,
                                "product_imported_prices" => $pr->product_imported_prices,
                                "product_retail_prices" => $pr->product_retail_prices,
                                "product_description" => $pr->product_description,
                                "product_saleoff_pinned" => $pr->product_saleoff_pinned,
                                "product_new_pinned" => $pr->product_new_pinned
                            ];
                            // push item
                            array_push($itemParent["product_list"], $itemPro);  
                        }
                        
                    }
                    //  end each ------------1-------------
                    // add con 2
                    foreach ($listChild as $value_c2) {
                        if($value_c2->cate_parent_id == $value_c->cate_id){
                            $itemChild2 = [
                                "cate_id" => $value_c2->cate_id,
                                "cate_parent_id" => $value_c->cate_id,
                                "cate_slug" => $value_c2->cate_slug,
                                "cate_name" => $value_c2->cate_name,
                                "cate_description" => $value_c2->cate_description,
                                "cate_thumbnail" => $value_c2->cate_thumbnail,
                                "cate_orders" => $value_c2->cate_orders,
                                "cate_pinned" => $value_p->cate_pinned,
                                "nodes" => array()
                            ];
                            array_push($itemChild["nodes"], $itemChild2); 
                            // find product con 2 
                            // find product con value_p ------------2-------------
                            foreach ($listProducts as $pr) {
                                if($value_c2->cate_id == $pr->cate_id)
                                {
                                    $itemPro = [
                                        "product_slug" => $pr->product_slug,
                                        "product_id" => $pr->product_id,
                                        "product_name" => $pr->product_name,
                                        "product_thumbnail" => $pr->product_thumbnail,
                                        "product_imported_prices" => $pr->product_imported_prices,
                                        "product_retail_prices" => $pr->product_retail_prices,
                                        "product_description" => $pr->product_description,
                                        "product_saleoff_pinned" => $pr->product_saleoff_pinned,
                                        "product_new_pinned" => $pr->product_new_pinned
                                    ];
                                    // push item
                                    array_push($itemParent["product_list"], $itemPro);  
                                }
                                
                            }
                            //  end each ------------2-------------
                        }
                    }
                    // add con 1
                    array_push($itemParent["nodes"], $itemChild);
                    
                }
            }
            // find product con value_p ------------0-------------
            foreach ($listProducts as $pr) {
                if($value_p->cate_id == $pr->cate_id)
                {
                    $itemPro = [
                        "product_slug" => $pr->product_slug,
                        "product_id" => $pr->product_id,
                        "product_name" => $pr->product_name,
                        "product_thumbnail" => $pr->product_thumbnail,
                        "product_imported_prices" => $pr->product_imported_prices,
                        "product_retail_prices" => $pr->product_retail_prices,
                        "product_description" => $pr->product_description,
                        "product_saleoff_pinned" => $pr->product_saleoff_pinned,
                        "product_new_pinned" => $pr->product_new_pinned
                    ];
                    // push item
                    array_push($itemParent["product_list"], $itemPro);  
                }
                
            }
            //  end each ------------0-------------
            //  add cha
            array_push($listCategories, $itemParent);
        }
        // ===============Thẻ Sản Phẩm==================
        $listTags = DB::table('products_tags_detail')
                          ->select('tag_slug',
                                    'tag_id',
                                    'tag_name',
                                    'tag_description',
                                    'created_date',
                                    'created_user',
                                    'updated_date',
                                    'updated_user',
                                    'status')
                          ->orderBy('created_date', 'asc')
                          ->get();
        // ======================San Pham Ban Chay===================
        $listQuickSales = DB::select("
            select
            pr.product_slug,
            pr.product_id,
            pr.product_name,
            pr.product_thumbnail,
            pr.product_imported_prices,
            pr.product_retail_prices,
            pr.product_description,
            pr.updated_date
            FROM products_detail AS pr
            WHERE pr.status != 0 AND pr.product_quick_pinned=1
            ORDER BY pr.updated_date DESC
            LIMIT 10
        ");      
        // ============list limit product_saleoff_pinned: 4==============
        $listLimitSaleOff = DB::select("
            select 
            pr.product_slug,
            pr.product_id,
            pr.product_name,
            pr.product_thumbnail,
            pr.product_imported_prices,
            pr.product_retail_prices,
            pr.product_description,
            pr.product_saleoff_pinned,
            pr.product_new_pinned
            FROM products_detail AS pr
            WHERE pr.product_saleoff_pinned=1
            AND pr.status != 0
            ORDER BY pr.updated_date DESC
            LIMIT 4
        "); 
        // ================list limit product_new_pinned: 4==============
        $listLimitNew = DB::select("
            select 
            pr.product_slug,
            pr.product_id,
            pr.product_name,
            pr.product_thumbnail,
            pr.product_imported_prices,
            pr.product_retail_prices,
            pr.product_description,
            pr.product_saleoff_pinned,
            pr.product_new_pinned
            FROM products_detail AS pr
            WHERE pr.product_new_pinned=1
            AND pr.status != 0
            ORDER BY pr.updated_date DESC
            LIMIT 4
        ");
        //======================= get list all product=====================
        $listCatePro = array();
        foreach ($listParent as $p_cate) {
            $itemParent = [
                "cate_id" => $p_cate->cate_id,
                "cate_parent_id" => $p_cate->cate_parent_id,
                "cate_slug" => $p_cate->cate_slug,
                "cate_name" => $p_cate->cate_name,
                "cate_description" => $p_cate->cate_description,
                "cate_thumbnail" => $p_cate->cate_thumbnail,
                "cate_orders" => $p_cate->cate_orders,
                "cate_pinned" => $p_cate->cate_pinned,
                "product_list" => array()
            ];
            // foreach add product
            foreach ($listProducts as $pr) {
                if($p_cate->cate_id == $pr->cate_id)
                {
                    $itemPro = [
                        "product_slug" => $pr->product_slug,
                        "product_id" => $pr->product_id,
                        "product_name" => $pr->product_name,
                        "product_thumbnail" => $pr->product_thumbnail,
                        "product_imported_prices" => $pr->product_imported_prices,
                        "product_retail_prices" => $pr->product_retail_prices,
                        "product_description" => $pr->product_description,
                        "product_saleoff_pinned" => $pr->product_saleoff_pinned,
                        "product_new_pinned" => $pr->product_new_pinned
                    ];
                    // push item
                    array_push($itemParent["product_list"], $itemPro);  
                }
                
            }

            array_push($listCatePro, $itemParent);
        }
        // ========================= get company info ==================
        $listInfo = DB::table('company_information')
                          ->select('info_id',
                                    'info_key', 
                                   'info_values')
                          ->get();
        // ========================= listSlide ================== 
        $listSlide = DB::table('slide_detail AS sl')
                ->select( 
                        'sl.slide_id',
                        'sl.slide_name',
                        'sl.slide_url',
                        'sl.slide_img'
                         )
                ->where('sl.status','<>',0)
                ->get();
    
        // =============language=============
        Carbon\Carbon::setLocale('vi');
        return view('front_end.homepage',
        [
            'listCategories' => $listCategories,
            'listTags' => $listTags,
            'listQuickSales' => $listQuickSales,
            'listLimitSaleOff' => $listLimitSaleOff,
            'listLimitNew' => $listLimitNew,
            'listCatePro' => $listCatePro,
            'listSlide' => $listSlide,
            'listInfo' => $listInfo
        ]);
    }
    // indexCategoryDetail
    protected function indexCategoryDetail(Request $request, $slug)
    {
      
        // ===============products_categories_detail==================
        $listCategories = array();
        $whereFunctions = array(
            ['cate_parent_id', '=', 0],
            ['status', '<>', 0]
        );
        $listParent = DB::table('products_categories_detail')
                          ->select('cate_id', 
                                   'cate_parent_id',
                                   'cate_slug',
                                   'cate_name', 
                                   'cate_description',
                                   'cate_thumbnail',
                                   'cate_orders',
                                   'cate_pinned')
                          ->where($whereFunctions)
                          ->orderBy('cate_orders', 'asc')
                          ->get();
        $whereFunctions = array(
            ['cate_parent_id', '<>', 0],
            ['status', '<>', 0]
        );
        $listChild = DB::table('products_categories_detail')
                          ->select('cate_id', 
                                   'cate_parent_id',
                                   'cate_slug',
                                   'cate_name', 
                                   'cate_description',
                                   'cate_thumbnail',
                                   'cate_orders',
                                   'cate_pinned')
                          ->where($whereFunctions)
                          ->orderBy('cate_orders', 'asc')
                          ->get();
        foreach ($listParent as $value_p) {
            $itemParent = [
                "cate_id" => $value_p->cate_id,
                "cate_parent_id" => $value_p->cate_parent_id,
                "cate_slug" => $value_p->cate_slug,
                "cate_name" => $value_p->cate_name,
                "cate_description" => $value_p->cate_description,
                "cate_thumbnail" => $value_p->cate_thumbnail,
                "cate_orders" => $value_p->cate_orders,
                "cate_pinned" => $value_p->cate_pinned,
                "nodes" => array()
            ];
            // foreach add con 1
            foreach ($listChild as $value_c) {
                // nếu cate_parent_id = cate_id
                if($value_c->cate_parent_id == $value_p->cate_id)
                {
                    $itemChild = [
                        "cate_id" => $value_c->cate_id,
                        "cate_parent_id" => $value_p->cate_id,
                        "cate_slug" => $value_c->cate_slug,
                        "cate_name" => $value_c->cate_name,
                        "cate_description" => $value_c->cate_description,
                        "cate_thumbnail" => $value_c->cate_thumbnail,
                        "cate_orders" => $value_c->cate_orders,
                        "cate_pinned" => $value_p->cate_pinned,
                        "nodes" => array()
                    ];
                    // add con 2
                    foreach ($listChild as $value_c2) {
                        if($value_c2->cate_parent_id == $value_c->cate_id){
                            $itemChild2 = [
                                "cate_id" => $value_c2->cate_id,
                                "cate_parent_id" => $value_c->cate_id,
                                "cate_slug" => $value_c2->cate_slug,
                                "cate_name" => $value_c2->cate_name,
                                "cate_description" => $value_c2->cate_description,
                                "cate_thumbnail" => $value_c2->cate_thumbnail,
                                "cate_orders" => $value_c2->cate_orders,
                                "cate_pinned" => $value_p->cate_pinned,
                                "nodes" => array()
                            ];
                            array_push($itemChild["nodes"], $itemChild2);  
                        }
                    }
                    // add con 1
                    array_push($itemParent["nodes"], $itemChild);
                }
            }
            //  add cha
            array_push($listCategories, $itemParent);
        }
        // ===============Thẻ Sản Phẩm==================
        $listTags = DB::table('products_tags_detail')
                          ->select('tag_slug',
                                    'tag_id',
                                    'tag_name',
                                    'tag_description',
                                    'created_date',
                                    'created_user',
                                    'updated_date',
                                    'updated_user',
                                    'status')
                          ->orderBy('created_date', 'asc')
                          ->get();
        // ======================San Pham Ban Chay===================
        $listQuickSales = DB::select("
            select
            pr.product_slug,
            pr.product_id,
            pr.product_name,
            pr.product_thumbnail,
            pr.product_imported_prices,
            pr.product_retail_prices,
            pr.product_description,
            pr.updated_date
            FROM products_detail AS pr
            WHERE pr.status != 0 AND pr.product_quick_pinned=1
            ORDER BY pr.updated_date DESC
            LIMIT 10
        ");  
        // ============list limit product_saleoff_pinned: 4==============
        $listLimitSaleOff = DB::select("
            select 
            pr.product_slug,
            pr.product_id,
            pr.product_name,
            pr.product_thumbnail,
            pr.product_imported_prices,
            pr.product_retail_prices,
            pr.product_description,
            pr.product_saleoff_pinned,
            pr.product_new_pinned
            FROM products_detail AS pr
            WHERE pr.product_saleoff_pinned=1
            AND pr.status != 0
            ORDER BY pr.updated_date DESC
            LIMIT 4
        "); 
        //======================= get list  product by cate=====================
        $listCatePro = DB::select("
            select * FROM products_detail AS pr 
            LEFT JOIN products_categories_detail AS ca ON ca.cate_id=pr.cate_id
            WHERE pr.status != 0
            AND pr.cate_id IN
            (
                SELECT * FROM(
                (
                    SELECT 
                    ca.cate_id
                    FROM products_categories_detail AS ca
                    WHERE ca.cate_slug='".$slug."'
                    )
                    UNION
                    (
                    SELECT 
                    ca1.cate_id
                    FROM products_categories_detail AS ca
                    LEFT JOIN products_categories_detail AS ca1 ON ca1.cate_parent_id=ca.cate_id
                    WHERE ca.cate_slug='".$slug."' 
                    )
                    UNION
                    (
                    SELECT 
                    ca2.cate_id 
                    FROM products_categories_detail AS ca
                    LEFT JOIN products_categories_detail AS ca1 ON ca1.cate_parent_id=ca.cate_id
                    LEFT JOIN products_categories_detail AS ca2 ON ca2.cate_parent_id=ca1.cate_id
                    WHERE ca.cate_slug='".$slug."' 
                    )
                ) as abc
                

            )
        ");
        // ========================= get company info =============================
        $listInfo = DB::table('company_information')
                          ->select('info_id',
                                    'info_key', 
                                   'info_values')
                          ->get();
        // =============language=============
        Carbon\Carbon::setLocale('vi');
        return view('front_end.categorydetail',
        [
            'listCategories' => $listCategories,
            'listTags' => $listTags,
            'listQuickSales' => $listQuickSales,
            'listLimitSaleOff' => $listLimitSaleOff,
            'listCatePro' => $listCatePro,
            'listInfo' => $listInfo
        ]);
    }
    // indexSaleOffProduct
    protected function indexSaleOffProduct(Request $request)
    {
      
        // ===============products_categories_detail==================
        $listCategories = array();
        $whereFunctions = array(
            ['cate_parent_id', '=', 0],
            ['status', '<>', 0]
        );
        $listParent = DB::table('products_categories_detail')
                          ->select('cate_id', 
                                   'cate_parent_id',
                                   'cate_slug',
                                   'cate_name', 
                                   'cate_description',
                                   'cate_thumbnail',
                                   'cate_orders',
                                   'cate_pinned')
                          ->where($whereFunctions)
                          ->orderBy('cate_orders', 'asc')
                          ->get();
        $whereFunctions = array(
            ['cate_parent_id', '<>', 0],
            ['status', '<>', 0]
        );
        $listChild = DB::table('products_categories_detail')
                          ->select('cate_id', 
                                   'cate_parent_id',
                                   'cate_slug',
                                   'cate_name', 
                                   'cate_description',
                                   'cate_thumbnail',
                                   'cate_orders',
                                   'cate_pinned')
                          ->where($whereFunctions)
                          ->orderBy('cate_orders', 'asc')
                          ->get();
        foreach ($listParent as $value_p) {
            $itemParent = [
                "cate_id" => $value_p->cate_id,
                "cate_parent_id" => $value_p->cate_parent_id,
                "cate_slug" => $value_p->cate_slug,
                "cate_name" => $value_p->cate_name,
                "cate_description" => $value_p->cate_description,
                "cate_thumbnail" => $value_p->cate_thumbnail,
                "cate_orders" => $value_p->cate_orders,
                "cate_pinned" => $value_p->cate_pinned,
                "nodes" => array()
            ];
            // foreach add con 1
            foreach ($listChild as $value_c) {
                // nếu cate_parent_id = cate_id
                if($value_c->cate_parent_id == $value_p->cate_id)
                {
                    $itemChild = [
                        "cate_id" => $value_c->cate_id,
                        "cate_parent_id" => $value_p->cate_id,
                        "cate_slug" => $value_c->cate_slug,
                        "cate_name" => $value_c->cate_name,
                        "cate_description" => $value_c->cate_description,
                        "cate_thumbnail" => $value_c->cate_thumbnail,
                        "cate_orders" => $value_c->cate_orders,
                        "cate_pinned" => $value_p->cate_pinned,
                        "nodes" => array()
                    ];
                    // add con 2
                    foreach ($listChild as $value_c2) {
                        if($value_c2->cate_parent_id == $value_c->cate_id){
                            $itemChild2 = [
                                "cate_id" => $value_c2->cate_id,
                                "cate_parent_id" => $value_c->cate_id,
                                "cate_slug" => $value_c2->cate_slug,
                                "cate_name" => $value_c2->cate_name,
                                "cate_description" => $value_c2->cate_description,
                                "cate_thumbnail" => $value_c2->cate_thumbnail,
                                "cate_orders" => $value_c2->cate_orders,
                                "cate_pinned" => $value_p->cate_pinned,
                                "nodes" => array()
                            ];
                            array_push($itemChild["nodes"], $itemChild2);  
                        }
                    }
                    // add con 1
                    array_push($itemParent["nodes"], $itemChild);
                }
            }
            //  add cha
            array_push($listCategories, $itemParent);
        }
        // ===============Thẻ Sản Phẩm==================
        $listTags = DB::table('products_tags_detail')
                          ->select('tag_slug',
                                    'tag_id',
                                    'tag_name',
                                    'tag_description',
                                    'created_date',
                                    'created_user',
                                    'updated_date',
                                    'updated_user',
                                    'status')
                          ->orderBy('created_date', 'asc')
                          ->get();
        // ======================San Pham Ban Chay===================
        $listQuickSales = DB::select("
            select
            pr.product_slug,
            pr.product_id,
            pr.product_name,
            pr.product_thumbnail,
            pr.product_imported_prices,
            pr.product_retail_prices,
            pr.product_description,
            pr.updated_date
            FROM products_detail AS pr
            WHERE pr.status != 0 AND pr.product_quick_pinned=1
            ORDER BY pr.updated_date DESC
            LIMIT 10
        "); 
        // ============list limit product_saleoff_pinned: 4==============
        $listLimitSaleOff = DB::select("
            select 
            pr.product_slug,
            pr.product_id,
            pr.product_name,
            pr.product_thumbnail,
            pr.product_imported_prices,
            pr.product_retail_prices,
            pr.product_description,
            pr.product_saleoff_pinned,
            pr.product_new_pinned
            FROM products_detail AS pr
            WHERE pr.product_saleoff_pinned=1
            AND pr.status != 0
            ORDER BY pr.updated_date DESC
            LIMIT 4
        "); 
        //======================= get list  product by cate=====================
        $listCatePro = DB::select("
            select 
            pr.product_slug,
            pr.product_id,
            pr.product_name,
            pr.product_thumbnail,
            pr.product_imported_prices,
            pr.product_retail_prices,
            pr.product_description,
            pr.product_saleoff_pinned,
            pr.product_new_pinned
            FROM products_detail AS pr
            WHERE pr.product_saleoff_pinned=1
            AND pr.status != 0
            ORDER BY pr.updated_date DESC
        ");
        // ========================= get company info =============================
        $listInfo = DB::table('company_information')
                          ->select('info_id',
                                    'info_key', 
                                   'info_values')
                          ->get();
        // =============language=============
        Carbon\Carbon::setLocale('vi');
        return view('front_end.listsaleoffproduct',
        [
            'listCategories' => $listCategories,
            'listTags' => $listTags,
            'listQuickSales' => $listQuickSales,
            'listLimitSaleOff' => $listLimitSaleOff,
            'listCatePro' => $listCatePro,
            'listInfo' => $listInfo
        ]);
    }
    // indexTagDetail
    protected function indexTagDetail(Request $request, $slug)
    {
      
        // ===============products_categories_detail==================
        $listCategories = array();
        $whereFunctions = array(
            ['cate_parent_id', '=', 0],
            ['status', '<>', 0]
        );
        $listParent = DB::table('products_categories_detail')
                          ->select('cate_id', 
                                   'cate_parent_id',
                                   'cate_slug',
                                   'cate_name', 
                                   'cate_description',
                                   'cate_thumbnail',
                                   'cate_orders',
                                   'cate_pinned')
                          ->where($whereFunctions)
                          ->orderBy('cate_orders', 'asc')
                          ->get();
        $whereFunctions = array(
            ['cate_parent_id', '<>', 0],
            ['status', '<>', 0]
        );
        $listChild = DB::table('products_categories_detail')
                          ->select('cate_id', 
                                   'cate_parent_id',
                                   'cate_slug',
                                   'cate_name', 
                                   'cate_description',
                                   'cate_thumbnail',
                                   'cate_orders',
                                   'cate_pinned')
                          ->where($whereFunctions)
                          ->orderBy('cate_orders', 'asc')
                          ->get();
        foreach ($listParent as $value_p) {
            $itemParent = [
                "cate_id" => $value_p->cate_id,
                "cate_parent_id" => $value_p->cate_parent_id,
                "cate_slug" => $value_p->cate_slug,
                "cate_name" => $value_p->cate_name,
                "cate_description" => $value_p->cate_description,
                "cate_thumbnail" => $value_p->cate_thumbnail,
                "cate_orders" => $value_p->cate_orders,
                "cate_pinned" => $value_p->cate_pinned,
                "nodes" => array()
            ];
            // foreach add con 1
            foreach ($listChild as $value_c) {
                // nếu cate_parent_id = cate_id
                if($value_c->cate_parent_id == $value_p->cate_id)
                {
                    $itemChild = [
                        "cate_id" => $value_c->cate_id,
                        "cate_parent_id" => $value_p->cate_id,
                        "cate_slug" => $value_c->cate_slug,
                        "cate_name" => $value_c->cate_name,
                        "cate_description" => $value_c->cate_description,
                        "cate_thumbnail" => $value_c->cate_thumbnail,
                        "cate_orders" => $value_c->cate_orders,
                        "cate_pinned" => $value_p->cate_pinned,
                        "nodes" => array()
                    ];
                    // add con 2
                    foreach ($listChild as $value_c2) {
                        if($value_c2->cate_parent_id == $value_c->cate_id){
                            $itemChild2 = [
                                "cate_id" => $value_c2->cate_id,
                                "cate_parent_id" => $value_c->cate_id,
                                "cate_slug" => $value_c2->cate_slug,
                                "cate_name" => $value_c2->cate_name,
                                "cate_description" => $value_c2->cate_description,
                                "cate_thumbnail" => $value_c2->cate_thumbnail,
                                "cate_orders" => $value_c2->cate_orders,
                                "cate_pinned" => $value_p->cate_pinned,
                                "nodes" => array()
                            ];
                            array_push($itemChild["nodes"], $itemChild2);  
                        }
                    }
                    // add con 1
                    array_push($itemParent["nodes"], $itemChild);
                }
            }
            //  add cha
            array_push($listCategories, $itemParent);
        }
        // ===============Thẻ Sản Phẩm==================
        $listTags = DB::table('products_tags_detail')
                          ->select('tag_slug',
                                    'tag_id',
                                    'tag_name',
                                    'tag_description',
                                    'created_date',
                                    'created_user',
                                    'updated_date',
                                    'updated_user',
                                    'status')
                          ->orderBy('created_date', 'asc')
                          ->get();
        // ======================San Pham Ban Chay===================
        $listQuickSales = DB::select("
            select
            pr.product_slug,
            pr.product_id,
            pr.product_name,
            pr.product_thumbnail,
            pr.product_imported_prices,
            pr.product_retail_prices,
            pr.product_description,
            pr.updated_date
            FROM products_detail AS pr
            WHERE pr.status != 0 AND pr.product_quick_pinned=1
            ORDER BY pr.updated_date DESC
            LIMIT 10
        ");     
        // ============list limit product_saleoff_pinned: 4==============
        $listLimitSaleOff = DB::select("
            select 
            pr.product_slug,
            pr.product_id,
            pr.product_name,
            pr.product_thumbnail,
            pr.product_imported_prices,
            pr.product_retail_prices,
            pr.product_description,
            pr.product_saleoff_pinned,
            pr.product_new_pinned
            FROM products_detail AS pr
            WHERE pr.product_saleoff_pinned=1
            AND pr.status != 0
            ORDER BY pr.updated_date DESC
            LIMIT 4
        "); 
        //======================= get list  product by cate=====================
        $listCatePro = DB::select("
            select * FROM products_detail AS pr
            LEFT JOIN products_tags_list AS tg ON pr.product_id = tg.product_id
            LEFT JOIN products_tags_detail AS de ON de.tag_id = tg.tag_id
            WHERE pr.status != 0
            AND de.tag_slug='".$slug."'
        ");
        // ========================= get company info =============================
        $listInfo = DB::table('company_information')
                          ->select('info_id',
                                    'info_key', 
                                   'info_values')
                          ->get();
       
        // =============language=============
        Carbon\Carbon::setLocale('vi');
        return view('front_end.tagdetail',
        [
            'listCategories' => $listCategories,
            'listTags' => $listTags,
            'listQuickSales' => $listQuickSales,
            'listLimitSaleOff' => $listLimitSaleOff,
            'listCatePro' => $listCatePro,
            'listInfo' => $listInfo
        ]);
    }
    // indexProductDetail
    protected function indexProductDetail(Request $request, $slug)
    {
        
        // ===============products_categories_detail==================
        $listCategories = array();
        $whereFunctions = array(
            ['cate_parent_id', '=', 0],
            ['status', '<>', 0]
        );
        $listParent = DB::table('products_categories_detail')
                          ->select('cate_id', 
                                   'cate_parent_id',
                                   'cate_slug',
                                   'cate_name', 
                                   'cate_description',
                                   'cate_thumbnail',
                                   'cate_orders',
                                   'cate_pinned')
                          ->where($whereFunctions)
                          ->orderBy('cate_orders', 'asc')
                          ->get();
        $whereFunctions = array(
            ['cate_parent_id', '<>', 0],
            ['status', '<>', 0]
        );
        $listChild = DB::table('products_categories_detail')
                          ->select('cate_id', 
                                   'cate_parent_id',
                                   'cate_slug',
                                   'cate_name', 
                                   'cate_description',
                                   'cate_thumbnail',
                                   'cate_orders',
                                   'cate_pinned')
                          ->where($whereFunctions)
                          ->orderBy('cate_orders', 'asc')
                          ->get();
        foreach ($listParent as $value_p) {
            $itemParent = [
                "cate_id" => $value_p->cate_id,
                "cate_parent_id" => $value_p->cate_parent_id,
                "cate_slug" => $value_p->cate_slug,
                "cate_name" => $value_p->cate_name,
                "cate_description" => $value_p->cate_description,
                "cate_thumbnail" => $value_p->cate_thumbnail,
                "cate_orders" => $value_p->cate_orders,
                "cate_pinned" => $value_p->cate_pinned,
                "nodes" => array()
            ];
            // foreach add con 1
            foreach ($listChild as $value_c) {
                // nếu cate_parent_id = cate_id
                if($value_c->cate_parent_id == $value_p->cate_id)
                {
                    $itemChild = [
                        "cate_id" => $value_c->cate_id,
                        "cate_parent_id" => $value_p->cate_id,
                        "cate_slug" => $value_c->cate_slug,
                        "cate_name" => $value_c->cate_name,
                        "cate_description" => $value_c->cate_description,
                        "cate_thumbnail" => $value_c->cate_thumbnail,
                        "cate_orders" => $value_c->cate_orders,
                        "cate_pinned" => $value_p->cate_pinned,
                        "nodes" => array()
                    ];
                    // add con 2
                    foreach ($listChild as $value_c2) {
                        if($value_c2->cate_parent_id == $value_c->cate_id){
                            $itemChild2 = [
                                "cate_id" => $value_c2->cate_id,
                                "cate_parent_id" => $value_c->cate_id,
                                "cate_slug" => $value_c2->cate_slug,
                                "cate_name" => $value_c2->cate_name,
                                "cate_description" => $value_c2->cate_description,
                                "cate_thumbnail" => $value_c2->cate_thumbnail,
                                "cate_orders" => $value_c2->cate_orders,
                                "cate_pinned" => $value_p->cate_pinned,
                                "nodes" => array()
                            ];
                            array_push($itemChild["nodes"], $itemChild2);  
                        }
                    }
                    // add con 1
                    array_push($itemParent["nodes"], $itemChild);
                }
            }
            //  add cha
            array_push($listCategories, $itemParent);
        }
        // ===============Thẻ Sản Phẩm==================
        $listTags = DB::table('products_tags_detail')
                          ->select('tag_slug',
                                    'tag_id',
                                    'tag_name',
                                    'tag_description',
                                    'created_date',
                                    'created_user',
                                    'updated_date',
                                    'updated_user',
                                    'status')
                          ->orderBy('created_date', 'asc')
                          ->get();
        // ======================San Pham Ban Chay===================
        $listQuickSales = DB::select("
            select
            pr.product_slug,
            pr.product_id,
            pr.product_name,
            pr.product_thumbnail,
            pr.product_imported_prices,
            pr.product_retail_prices,
            pr.product_description,
            pr.updated_date
            FROM products_detail AS pr
            WHERE pr.status != 0 AND pr.product_quick_pinned=1
            ORDER BY pr.updated_date DESC
            LIMIT 10
        ");    
        // ============list limit product_saleoff_pinned: 4==============
        $listLimitSaleOff = DB::select("
            select 
            pr.product_slug,
            pr.product_id,
            pr.product_name,
            pr.product_thumbnail,
            pr.product_imported_prices,
            pr.product_retail_prices,
            pr.product_description,
            pr.product_saleoff_pinned,
            pr.product_new_pinned
            FROM products_detail AS pr
            WHERE pr.product_saleoff_pinned=1
            AND pr.status != 0
            ORDER BY pr.updated_date DESC
            LIMIT 4
        "); 
        // ================list product_new_pinned: no limit==============
        $listNew = DB::select("
            select 
            pr.product_slug,
            pr.product_id,
            pr.product_name,
            pr.product_thumbnail,
            pr.product_imported_prices,
            pr.product_retail_prices,
            pr.product_description,
            pr.product_saleoff_pinned,
            pr.product_new_pinned
            FROM products_detail AS pr
            WHERE pr.product_new_pinned=1
            AND pr.status != 0
            ORDER BY pr.updated_date DESC
        ");
        // ============list  product_saleoff_pinned==============
        $listSaleOff = DB::select("
            select 
            pr.product_slug,
            pr.product_id,
            pr.product_name,
            pr.product_thumbnail,
            pr.product_imported_prices,
            pr.product_retail_prices,
            pr.product_description,
            pr.product_saleoff_pinned,
            pr.product_new_pinned
            FROM products_detail AS pr
            WHERE pr.product_saleoff_pinned=1
            AND pr.status != 0
            ORDER BY pr.updated_date DESC
        ");
        //======================= get detail=====================
        $whereFunctions = array(
                    ['nd.product_slug', '=', $slug]
                );
        $productDetail = DB::table('products_detail AS nd')
                ->leftJoin('products_units_detail AS un', 'nd.unit_id', '=', 'un.unit_id')
                ->leftJoin('products_categories_detail AS ca', 'nd.cate_id', '=', 'ca.cate_id')
                ->leftJoin('products_origins_detail AS or', 'nd.origin_id', '=', 'or.origin_id')
                ->leftJoin('products_manufacturers_detail AS ma', 'nd.manufacturer_id', '=', 'ma.manufacturer_id')
                ->leftJoin('products_colors_detail AS co', 'nd.color_id', '=', 'co.color_id')
                //phai theo thứ tự (product_id > quantity >product_imported_prices)
                ->select('nd.product_id',
                    'nd.product_slug',
                    'nd.unit_id',
                    'nd.cate_id',
                    'nd.origin_id',
                    'nd.manufacturer_id',
                    'nd.color_id',
                    'nd.product_name',
                    'nd.product_description',
                    'nd.product_specifications',
                    'nd.product_imported_prices',//gia ban
                    'nd.product_retail_prices',//gia khuyen mai
                    'nd.product_deals',
                    'nd.product_ratings',
                    'nd.product_thumbnail',
                    'nd.product_images_list',
                    'nd.product_release_date',
                    'nd.product_expiration_date',
                    'nd.product_comment_status',
                    'nd.news_seo_title',
                    'nd.news_seo_description',
                    'nd.news_seo_keywords',
                    'nd.status',
                    'un.unit_name',
                    'ca.cate_name',
                    'or.origin_name',
                    'ma.manufacturer_name',
                    'co.color_name'
                         )
                ->where($whereFunctions)
                ->first();
        // list tag of this product
        $listProTags = DB::table('products_detail AS nd')
        ->leftJoin('products_tags_list AS tl', 'nd.product_id', '=', 'tl.product_id')
        ->leftJoin('products_tags_detail AS td', 'tl.tag_id', '=', 'td.tag_id')
        ->select('td.tag_id',
                 'td.tag_slug',
                 'td.tag_name')
        ->where($whereFunctions)
        ->get();
        // ========================= get company info =============================
        $listInfo = DB::table('company_information')
                          ->select('info_id',
                                    'info_key', 
                                   'info_values')
                          ->get();
      
        // =============language=============
        Carbon\Carbon::setLocale('vi');
        return view('front_end.productdetail',
        [
            'listCategories' => $listCategories,
            'listTags' => $listTags,
            'listQuickSales' => $listQuickSales,
            'listLimitSaleOff' => $listLimitSaleOff,
            'productDetail' => $productDetail,
            'listProTags' => $listProTags,
            'listNew' => $listNew,
            'listSaleOff' => $listSaleOff,
            'listInfo' => $listInfo
        ]);
    }
    // indexCart
    protected function indexCart(Request $request)
    {
    
        // ===============products_categories_detail==================
        $listCategories = array();
        $whereFunctions = array(
            ['cate_parent_id', '=', 0],
            ['status', '<>', 0]
        );
        $listParent = DB::table('products_categories_detail')
                          ->select('cate_id', 
                                   'cate_parent_id',
                                   'cate_slug',
                                   'cate_name', 
                                   'cate_description',
                                   'cate_thumbnail',
                                   'cate_orders',
                                   'cate_pinned')
                          ->where($whereFunctions)
                          ->orderBy('cate_orders', 'asc')
                          ->get();
        $whereFunctions = array(
            ['cate_parent_id', '<>', 0],
            ['status', '<>', 0]
        );
        $listChild = DB::table('products_categories_detail')
                          ->select('cate_id', 
                                   'cate_parent_id',
                                   'cate_slug',
                                   'cate_name', 
                                   'cate_description',
                                   'cate_thumbnail',
                                   'cate_orders',
                                   'cate_pinned')
                          ->where($whereFunctions)
                          ->orderBy('cate_orders', 'asc')
                          ->get();
        foreach ($listParent as $value_p) {
            $itemParent = [
                "cate_id" => $value_p->cate_id,
                "cate_parent_id" => $value_p->cate_parent_id,
                "cate_slug" => $value_p->cate_slug,
                "cate_name" => $value_p->cate_name,
                "cate_description" => $value_p->cate_description,
                "cate_thumbnail" => $value_p->cate_thumbnail,
                "cate_orders" => $value_p->cate_orders,
                "cate_pinned" => $value_p->cate_pinned,
                "nodes" => array()
            ];
            // foreach add con 1
            foreach ($listChild as $value_c) {
                // nếu cate_parent_id = cate_id
                if($value_c->cate_parent_id == $value_p->cate_id)
                {
                    $itemChild = [
                        "cate_id" => $value_c->cate_id,
                        "cate_parent_id" => $value_p->cate_id,
                        "cate_slug" => $value_c->cate_slug,
                        "cate_name" => $value_c->cate_name,
                        "cate_description" => $value_c->cate_description,
                        "cate_thumbnail" => $value_c->cate_thumbnail,
                        "cate_orders" => $value_c->cate_orders,
                        "cate_pinned" => $value_p->cate_pinned,
                        "nodes" => array()
                    ];
                    // add con 2
                    foreach ($listChild as $value_c2) {
                        if($value_c2->cate_parent_id == $value_c->cate_id){
                            $itemChild2 = [
                                "cate_id" => $value_c2->cate_id,
                                "cate_parent_id" => $value_c->cate_id,
                                "cate_slug" => $value_c2->cate_slug,
                                "cate_name" => $value_c2->cate_name,
                                "cate_description" => $value_c2->cate_description,
                                "cate_thumbnail" => $value_c2->cate_thumbnail,
                                "cate_orders" => $value_c2->cate_orders,
                                "cate_pinned" => $value_p->cate_pinned,
                                "nodes" => array()
                            ];
                            array_push($itemChild["nodes"], $itemChild2);  
                        }
                    }
                    // add con 1
                    array_push($itemParent["nodes"], $itemChild);
                }
            }
            //  add cha
            array_push($listCategories, $itemParent);
        }
        // ===============Thẻ Sản Phẩm==================
        $listTags = DB::table('products_tags_detail')
                          ->select('tag_slug',
                                    'tag_id',
                                    'tag_name',
                                    'tag_description',
                                    'created_date',
                                    'created_user',
                                    'updated_date',
                                    'updated_user',
                                    'status')
                          ->orderBy('created_date', 'asc')
                          ->get();
        // ======================San Pham Ban Chay===================
        $listQuickSales = DB::select("
            select
            pr.product_slug,
            pr.product_id,
            pr.product_name,
            pr.product_thumbnail,
            pr.product_imported_prices,
            pr.product_retail_prices,
            pr.product_description,
            pr.updated_date
            FROM products_detail AS pr
            WHERE pr.status != 0 AND pr.product_quick_pinned=1
            ORDER BY pr.updated_date DESC
            LIMIT 10
        ");     
        // ============list limit product_saleoff_pinned: 4==============
        $listLimitSaleOff = DB::select("
            select 
            pr.product_slug,
            pr.product_id,
            pr.product_name,
            pr.product_thumbnail,
            pr.product_imported_prices,
            pr.product_retail_prices,
            pr.product_description,
            pr.product_saleoff_pinned,
            pr.product_new_pinned
            FROM products_detail AS pr
            WHERE pr.product_saleoff_pinned=1
            AND pr.status != 0
            ORDER BY pr.updated_date DESC
            LIMIT 4
        "); 
        //======================= get list cart=====================
        
        // ========================= get company info =============================
        $listInfo = DB::table('company_information')
                          ->select('info_id',
                                    'info_key', 
                                   'info_values')
                          ->get();
        
        // =============language=============
        Carbon\Carbon::setLocale('vi');
        return view('front_end.cart',
        [
            'listCategories' => $listCategories,
            'listTags' => $listTags,
            'listQuickSales' => $listQuickSales,
            'listLimitSaleOff' => $listLimitSaleOff,
            'listInfo' => $listInfo
        ]);
    }
    // indexHistoryBill
    protected function indexHistoryBill(Request $request)
    {
        if(Session::get('customerInfo')==null)
        {   
            return Redirect::to('trang-chu'); 
        }
        // ===============products_categories_detail==================
        $listCategories = array();
        $whereFunctions = array(
            ['cate_parent_id', '=', 0],
            ['status', '<>', 0]
        );
        $listParent = DB::table('products_categories_detail')
                          ->select('cate_id', 
                                   'cate_parent_id',
                                   'cate_slug',
                                   'cate_name', 
                                   'cate_description',
                                   'cate_thumbnail',
                                   'cate_orders',
                                   'cate_pinned')
                          ->where($whereFunctions)
                          ->orderBy('cate_orders', 'asc')
                          ->get();
        $whereFunctions = array(
            ['cate_parent_id', '<>', 0],
            ['status', '<>', 0]
        );
        $listChild = DB::table('products_categories_detail')
                          ->select('cate_id', 
                                   'cate_parent_id',
                                   'cate_slug',
                                   'cate_name', 
                                   'cate_description',
                                   'cate_thumbnail',
                                   'cate_orders',
                                   'cate_pinned')
                          ->where($whereFunctions)
                          ->orderBy('cate_orders', 'asc')
                          ->get();
        foreach ($listParent as $value_p) {
            $itemParent = [
                "cate_id" => $value_p->cate_id,
                "cate_parent_id" => $value_p->cate_parent_id,
                "cate_slug" => $value_p->cate_slug,
                "cate_name" => $value_p->cate_name,
                "cate_description" => $value_p->cate_description,
                "cate_thumbnail" => $value_p->cate_thumbnail,
                "cate_orders" => $value_p->cate_orders,
                "cate_pinned" => $value_p->cate_pinned,
                "nodes" => array()
            ];
            // foreach add con 1
            foreach ($listChild as $value_c) {
                // nếu cate_parent_id = cate_id
                if($value_c->cate_parent_id == $value_p->cate_id)
                {
                    $itemChild = [
                        "cate_id" => $value_c->cate_id,
                        "cate_parent_id" => $value_p->cate_id,
                        "cate_slug" => $value_c->cate_slug,
                        "cate_name" => $value_c->cate_name,
                        "cate_description" => $value_c->cate_description,
                        "cate_thumbnail" => $value_c->cate_thumbnail,
                        "cate_orders" => $value_c->cate_orders,
                        "cate_pinned" => $value_p->cate_pinned,
                        "nodes" => array()
                    ];
                    // add con 2
                    foreach ($listChild as $value_c2) {
                        if($value_c2->cate_parent_id == $value_c->cate_id){
                            $itemChild2 = [
                                "cate_id" => $value_c2->cate_id,
                                "cate_parent_id" => $value_c->cate_id,
                                "cate_slug" => $value_c2->cate_slug,
                                "cate_name" => $value_c2->cate_name,
                                "cate_description" => $value_c2->cate_description,
                                "cate_thumbnail" => $value_c2->cate_thumbnail,
                                "cate_orders" => $value_c2->cate_orders,
                                "cate_pinned" => $value_p->cate_pinned,
                                "nodes" => array()
                            ];
                            array_push($itemChild["nodes"], $itemChild2);  
                        }
                    }
                    // add con 1
                    array_push($itemParent["nodes"], $itemChild);
                }
            }
            //  add cha
            array_push($listCategories, $itemParent);
        }
        // ===============Thẻ Sản Phẩm==================
        $listTags = DB::table('products_tags_detail')
                          ->select('tag_slug',
                                    'tag_id',
                                    'tag_name',
                                    'tag_description',
                                    'created_date',
                                    'created_user',
                                    'updated_date',
                                    'updated_user',
                                    'status')
                          ->orderBy('created_date', 'asc')
                          ->get();
        // ======================San Pham Ban Chay===================
        $listQuickSales = DB::select("
            select
            pr.product_slug,
            pr.product_id,
            pr.product_name,
            pr.product_thumbnail,
            pr.product_imported_prices,
            pr.product_retail_prices,
            pr.product_description,
            pr.updated_date
            FROM products_detail AS pr
            WHERE pr.status != 0 AND pr.product_quick_pinned=1
            ORDER BY pr.updated_date DESC
            LIMIT 10
        ");     
        // ============list limit product_saleoff_pinned: 4==============
        $listLimitSaleOff = DB::select("
            select 
            pr.product_slug,
            pr.product_id,
            pr.product_name,
            pr.product_thumbnail,
            pr.product_imported_prices,
            pr.product_retail_prices,
            pr.product_description,
            pr.product_saleoff_pinned,
            pr.product_new_pinned
            FROM products_detail AS pr
            WHERE pr.product_saleoff_pinned=1
            AND pr.status != 0
            ORDER BY pr.updated_date DESC
            LIMIT 4
        "); 
        //======================= get list cart=====================
        
        // ========================= get company info =============================
        $listInfo = DB::table('company_information')
                          ->select('info_id',
                                    'info_key', 
                                   'info_values')
                          ->get();
     
        // =============language=============
        Carbon\Carbon::setLocale('vi');
        return view('front_end.historybill',
        [
            'listCategories' => $listCategories,
            'listTags' => $listTags,
            'listQuickSales' => $listQuickSales,
            'listLimitSaleOff' => $listLimitSaleOff,
            'listInfo' => $listInfo
        ]);
    }
    // indexListNews
    protected function indexListNews(Request $request)
    {
      
        // ===============products_categories_detail==================
        $listCategories = array();
        $whereFunctions = array(
            ['cate_parent_id', '=', 0],
            ['status', '<>', 0]
        );
        $listParent = DB::table('products_categories_detail')
                          ->select('cate_id', 
                                   'cate_parent_id',
                                   'cate_slug',
                                   'cate_name', 
                                   'cate_description',
                                   'cate_thumbnail',
                                   'cate_orders',
                                   'cate_pinned')
                          ->where($whereFunctions)
                          ->orderBy('cate_orders', 'asc')
                          ->get();
        $whereFunctions = array(
            ['cate_parent_id', '<>', 0],
            ['status', '<>', 0]
        );
        $listChild = DB::table('products_categories_detail')
                          ->select('cate_id', 
                                   'cate_parent_id',
                                   'cate_slug',
                                   'cate_name', 
                                   'cate_description',
                                   'cate_thumbnail',
                                   'cate_orders',
                                   'cate_pinned')
                          ->where($whereFunctions)
                          ->orderBy('cate_orders', 'asc')
                          ->get();
        foreach ($listParent as $value_p) {
            $itemParent = [
                "cate_id" => $value_p->cate_id,
                "cate_parent_id" => $value_p->cate_parent_id,
                "cate_slug" => $value_p->cate_slug,
                "cate_name" => $value_p->cate_name,
                "cate_description" => $value_p->cate_description,
                "cate_thumbnail" => $value_p->cate_thumbnail,
                "cate_orders" => $value_p->cate_orders,
                "cate_pinned" => $value_p->cate_pinned,
                "nodes" => array()
            ];
            // foreach add con 1
            foreach ($listChild as $value_c) {
                // nếu cate_parent_id = cate_id
                if($value_c->cate_parent_id == $value_p->cate_id)
                {
                    $itemChild = [
                        "cate_id" => $value_c->cate_id,
                        "cate_parent_id" => $value_p->cate_id,
                        "cate_slug" => $value_c->cate_slug,
                        "cate_name" => $value_c->cate_name,
                        "cate_description" => $value_c->cate_description,
                        "cate_thumbnail" => $value_c->cate_thumbnail,
                        "cate_orders" => $value_c->cate_orders,
                        "cate_pinned" => $value_p->cate_pinned,
                        "nodes" => array()
                    ];
                    // add con 2
                    foreach ($listChild as $value_c2) {
                        if($value_c2->cate_parent_id == $value_c->cate_id){
                            $itemChild2 = [
                                "cate_id" => $value_c2->cate_id,
                                "cate_parent_id" => $value_c->cate_id,
                                "cate_slug" => $value_c2->cate_slug,
                                "cate_name" => $value_c2->cate_name,
                                "cate_description" => $value_c2->cate_description,
                                "cate_thumbnail" => $value_c2->cate_thumbnail,
                                "cate_orders" => $value_c2->cate_orders,
                                "cate_pinned" => $value_p->cate_pinned,
                                "nodes" => array()
                            ];
                            array_push($itemChild["nodes"], $itemChild2);  
                        }
                    }
                    // add con 1
                    array_push($itemParent["nodes"], $itemChild);
                }
            }
            //  add cha
            array_push($listCategories, $itemParent);
        }
           
        // ============list limit product_saleoff_pinned: 4==============
        $listLimitSaleOff = DB::select("
            select 
            pr.product_slug,
            pr.product_id,
            pr.product_name,
            pr.product_thumbnail,
            pr.product_imported_prices,
            pr.product_retail_prices,
            pr.product_description,
            pr.product_saleoff_pinned,
            pr.product_new_pinned
            FROM products_detail AS pr
            WHERE pr.product_saleoff_pinned=1
            AND pr.status != 0
            ORDER BY pr.updated_date DESC
            LIMIT 4
        "); 
        //======================= get list news=====================
        $listAllNews = DB::select("
            select *,
            DAY(nd.created_date) as day_created_date,
            MONTH(nd.created_date) as month_created_date,
            YEAR(nd.created_date) as year_created_date,

            DAY(nd.updated_date) as day_updated_date,
            MONTH(nd.updated_date) as month_updated_date,
            YEAR(nd.updated_date) as year_updated_date

            FROM news_detail AS nd
            WHERE nd.news_pinned != 1 
            AND nd.status = 1
            AND nd.cate_id != 1
            AND nd.status != 0
            AND (nd.cate_id=2 OR nd.cate_id=3)
            ORDER BY nd.updated_date DESC
        "); 
        $listPinnedNews = DB::select("
            select *,
            DAY(nd.created_date) as day_created_date,
            MONTH(nd.created_date) as month_created_date,
            YEAR(nd.created_date) as year_created_date,

            DAY(nd.updated_date) as day_updated_date,
            MONTH(nd.updated_date) as month_updated_date,
            YEAR(nd.updated_date) as year_updated_date

            FROM news_detail AS nd
            WHERE nd.news_pinned=1 
            AND nd.status = 1
            AND nd.cate_id != 1
            AND nd.status != 0
            AND (nd.cate_id=2 OR nd.cate_id=3)
            ORDER BY nd.updated_date DESC
        "); 
        // ========================= get company info =============================
        $listInfo = DB::table('company_information')
                          ->select('info_id',
                                    'info_key', 
                                   'info_values')
                          ->get();
        // =============listLimitPinnedNews=============
        $listLimitPinnedNews = DB::select("
            select *,
            DAY(nd.created_date) as day_created_date,
            MONTH(nd.created_date) as month_created_date,
            YEAR(nd.created_date) as year_created_date 
            FROM news_detail AS nd
            WHERE nd.news_pinned=1 
            AND nd.status = 1
            AND nd.cate_id != 1
            AND nd.status != 0
            ORDER BY nd.updated_date ASC
            LIMIT 3
        ");
        // =============language=============
        Carbon\Carbon::setLocale('vi');
        return view('front_end.listnews',
        [
            'listCategories' => $listCategories,
            'listLimitSaleOff' => $listLimitSaleOff,
            'listAllNews' => $listAllNews,
            'listPinnedNews' => $listPinnedNews,
            'listLimitPinnedNews' => $listLimitPinnedNews,
            'listInfo' => $listInfo
        ]);
    }
    // indexNewsDetail
    protected function indexNewsDetail(Request $request, $slug)
    {
      
        // ===============products_categories_detail==================
        $listCategories = array();
        $whereFunctions = array(
            ['cate_parent_id', '=', 0],
            ['status', '<>', 0]
        );
        $listParent = DB::table('products_categories_detail')
                          ->select('cate_id', 
                                   'cate_parent_id',
                                   'cate_slug',
                                   'cate_name', 
                                   'cate_description',
                                   'cate_thumbnail',
                                   'cate_orders',
                                   'cate_pinned')
                          ->where($whereFunctions)
                          ->orderBy('cate_orders', 'asc')
                          ->get();
        $whereFunctions = array(
            ['cate_parent_id', '<>', 0],
            ['status', '<>', 0]
        );
        $listChild = DB::table('products_categories_detail')
                          ->select('cate_id', 
                                   'cate_parent_id',
                                   'cate_slug',
                                   'cate_name', 
                                   'cate_description',
                                   'cate_thumbnail',
                                   'cate_orders',
                                   'cate_pinned')
                          ->where($whereFunctions)
                          ->orderBy('cate_orders', 'asc')
                          ->get();
        foreach ($listParent as $value_p) {
            $itemParent = [
                "cate_id" => $value_p->cate_id,
                "cate_parent_id" => $value_p->cate_parent_id,
                "cate_slug" => $value_p->cate_slug,
                "cate_name" => $value_p->cate_name,
                "cate_description" => $value_p->cate_description,
                "cate_thumbnail" => $value_p->cate_thumbnail,
                "cate_orders" => $value_p->cate_orders,
                "cate_pinned" => $value_p->cate_pinned,
                "nodes" => array()
            ];
            // foreach add con 1
            foreach ($listChild as $value_c) {
                // nếu cate_parent_id = cate_id
                if($value_c->cate_parent_id == $value_p->cate_id)
                {
                    $itemChild = [
                        "cate_id" => $value_c->cate_id,
                        "cate_parent_id" => $value_p->cate_id,
                        "cate_slug" => $value_c->cate_slug,
                        "cate_name" => $value_c->cate_name,
                        "cate_description" => $value_c->cate_description,
                        "cate_thumbnail" => $value_c->cate_thumbnail,
                        "cate_orders" => $value_c->cate_orders,
                        "cate_pinned" => $value_p->cate_pinned,
                        "nodes" => array()
                    ];
                    // add con 2
                    foreach ($listChild as $value_c2) {
                        if($value_c2->cate_parent_id == $value_c->cate_id){
                            $itemChild2 = [
                                "cate_id" => $value_c2->cate_id,
                                "cate_parent_id" => $value_c->cate_id,
                                "cate_slug" => $value_c2->cate_slug,
                                "cate_name" => $value_c2->cate_name,
                                "cate_description" => $value_c2->cate_description,
                                "cate_thumbnail" => $value_c2->cate_thumbnail,
                                "cate_orders" => $value_c2->cate_orders,
                                "cate_pinned" => $value_p->cate_pinned,
                                "nodes" => array()
                            ];
                            array_push($itemChild["nodes"], $itemChild2);  
                        }
                    }
                    // add con 1
                    array_push($itemParent["nodes"], $itemChild);
                }
            }
            //  add cha
            array_push($listCategories, $itemParent);
        }
        // ===============Thẻ Sản Phẩm==================
        $listTags = DB::table('products_tags_detail')
                          ->select('tag_slug',
                                    'tag_id',
                                    'tag_name',
                                    'tag_description',
                                    'created_date',
                                    'created_user',
                                    'updated_date',
                                    'updated_user',
                                    'status')
                          ->orderBy('created_date', 'asc')
                          ->get();
        // ======================San Pham Ban Chay===================
        $listQuickSales = DB::select("
            select
            pr.product_slug,
            pr.product_id,
            pr.product_name,
            pr.product_thumbnail,
            pr.product_imported_prices,
            pr.product_retail_prices,
            pr.product_description,
            pr.updated_date
            FROM products_detail AS pr
            WHERE pr.status != 0 AND pr.product_quick_pinned=1
            ORDER BY pr.updated_date DESC
            LIMIT 10
        ");  
        // ============list limit product_saleoff_pinned: 4==============
        $listLimitSaleOff = DB::select("
            select 
            pr.product_slug,
            pr.product_id,
            pr.product_name,
            pr.product_thumbnail,
            pr.product_imported_prices,
            pr.product_retail_prices,
            pr.product_description,
            pr.product_saleoff_pinned,
            pr.product_new_pinned
            FROM products_detail AS pr
            WHERE pr.product_saleoff_pinned=1
            AND pr.status != 0
            ORDER BY pr.updated_date DESC
            LIMIT 4
        "); 
        //======================= get news detail=====================
        $newsDetail = DB::table('news_detail AS nd')
                ->select('nd.news_id', 
                         'nd.cate_id', 
                         'nd.news_slug', 
                         'nd.news_title', 
                         'nd.news_description', 
                         'nd.news_content', 
                         'nd.news_ratings',
                         'nd.news_thumbnail',
                         DB::raw("DAY(nd.created_date) AS day_created_date_format"),
                         DB::raw("MONTH(nd.created_date) AS month_created_date_format"),
                         DB::raw("YEAR(nd.created_date) AS year_created_date_format"),
                         'nd.news_pinned')
                ->where('nd.news_slug','=', $slug)
                ->orderBy('nd.updated_date', 'asc')
                ->first();
    
        // ========================= get company info =============================
        $listInfo = DB::table('company_information')
                          ->select('info_id',
                                    'info_key', 
                                   'info_values')
                          ->get();
      
        // =============language=============
        Carbon\Carbon::setLocale('vi');
        return view('front_end.newsdetail',
        [
            'listCategories' => $listCategories,
            'listTags' => $listTags,
            'listQuickSales' => $listQuickSales,
            'listLimitSaleOff' => $listLimitSaleOff,
            'newsDetail' => $newsDetail,
            'listInfo' => $listInfo
        ]);
    }
    // indexRecruitmentNews
    protected function indexRecruitmentNews(Request $request)
    {
      
        // ===============products_categories_detail==================
        $listCategories = array();
        $whereFunctions = array(
            ['cate_parent_id', '=', 0],
            ['status', '<>', 0]
        );
        $listParent = DB::table('products_categories_detail')
                          ->select('cate_id', 
                                   'cate_parent_id',
                                   'cate_slug',
                                   'cate_name', 
                                   'cate_description',
                                   'cate_thumbnail',
                                   'cate_orders',
                                   'cate_pinned')
                          ->where($whereFunctions)
                          ->orderBy('cate_orders', 'asc')
                          ->get();
        $whereFunctions = array(
            ['cate_parent_id', '<>', 0],
            ['status', '<>', 0]
        );
        $listChild = DB::table('products_categories_detail')
                          ->select('cate_id', 
                                   'cate_parent_id',
                                   'cate_slug',
                                   'cate_name', 
                                   'cate_description',
                                   'cate_thumbnail',
                                   'cate_orders',
                                   'cate_pinned')
                          ->where($whereFunctions)
                          ->orderBy('cate_orders', 'asc')
                          ->get();
        foreach ($listParent as $value_p) {
            $itemParent = [
                "cate_id" => $value_p->cate_id,
                "cate_parent_id" => $value_p->cate_parent_id,
                "cate_slug" => $value_p->cate_slug,
                "cate_name" => $value_p->cate_name,
                "cate_description" => $value_p->cate_description,
                "cate_thumbnail" => $value_p->cate_thumbnail,
                "cate_orders" => $value_p->cate_orders,
                "cate_pinned" => $value_p->cate_pinned,
                "nodes" => array()
            ];
            // foreach add con 1
            foreach ($listChild as $value_c) {
                // nếu cate_parent_id = cate_id
                if($value_c->cate_parent_id == $value_p->cate_id)
                {
                    $itemChild = [
                        "cate_id" => $value_c->cate_id,
                        "cate_parent_id" => $value_p->cate_id,
                        "cate_slug" => $value_c->cate_slug,
                        "cate_name" => $value_c->cate_name,
                        "cate_description" => $value_c->cate_description,
                        "cate_thumbnail" => $value_c->cate_thumbnail,
                        "cate_orders" => $value_c->cate_orders,
                        "cate_pinned" => $value_p->cate_pinned,
                        "nodes" => array()
                    ];
                    // add con 2
                    foreach ($listChild as $value_c2) {
                        if($value_c2->cate_parent_id == $value_c->cate_id){
                            $itemChild2 = [
                                "cate_id" => $value_c2->cate_id,
                                "cate_parent_id" => $value_c->cate_id,
                                "cate_slug" => $value_c2->cate_slug,
                                "cate_name" => $value_c2->cate_name,
                                "cate_description" => $value_c2->cate_description,
                                "cate_thumbnail" => $value_c2->cate_thumbnail,
                                "cate_orders" => $value_c2->cate_orders,
                                "cate_pinned" => $value_p->cate_pinned,
                                "nodes" => array()
                            ];
                            array_push($itemChild["nodes"], $itemChild2);  
                        }
                    }
                    // add con 1
                    array_push($itemParent["nodes"], $itemChild);
                }
            }
            //  add cha
            array_push($listCategories, $itemParent);
        }
        // ===============Thẻ Sản Phẩm==================
        $listTags = DB::table('products_tags_detail')
                          ->select('tag_slug',
                                    'tag_id',
                                    'tag_name',
                                    'tag_description',
                                    'created_date',
                                    'created_user',
                                    'updated_date',
                                    'updated_user',
                                    'status')
                          ->orderBy('created_date', 'asc')
                          ->get();
        // ======================San Pham Ban Chay===================
        $listQuickSales = DB::select("
            select
            pr.product_slug,
            pr.product_id,
            pr.product_name,
            pr.product_thumbnail,
            pr.product_imported_prices,
            pr.product_retail_prices,
            pr.product_description,
            pr.updated_date
            FROM products_detail AS pr
            WHERE pr.status != 0 AND pr.product_quick_pinned=1
            ORDER BY pr.updated_date DESC
            LIMIT 10
        ");  
        // ============list limit product_saleoff_pinned: 4==============
        $listLimitSaleOff = DB::select("
            select 
            pr.product_slug,
            pr.product_id,
            pr.product_name,
            pr.product_thumbnail,
            pr.product_imported_prices,
            pr.product_retail_prices,
            pr.product_description,
            pr.product_saleoff_pinned,
            pr.product_new_pinned
            FROM products_detail AS pr
            WHERE pr.product_saleoff_pinned=1
            AND pr.status != 0
            ORDER BY pr.updated_date DESC
            LIMIT 4
        "); 
        //======================= get recruitment news=====================
        $newsDetail = DB::table('news_detail AS nd')
                ->select('nd.news_id', 
                         'nd.cate_id', 
                         'nd.news_slug', 
                         'nd.news_title', 
                         'nd.news_description', 
                         'nd.news_content', 
                         'nd.news_ratings',
                         'nd.news_thumbnail',
                         DB::raw("DAY(nd.created_date) AS day_created_date_format"),
                         DB::raw("MONTH(nd.created_date) AS month_created_date_format"),
                         DB::raw("YEAR(nd.created_date) AS year_created_date_format"),
                         'nd.news_pinned')
                ->where([
                    ['nd.cate_id','=',1],
                    ['nd.status','=',1]
                    ])
                ->orderBy('nd.updated_date', 'asc')
                ->first();
    
        // ========================= get company info =============================
        $listInfo = DB::table('company_information')
                          ->select('info_id',
                                    'info_key', 
                                   'info_values')
                          ->get();
       
        // =============language=============
        Carbon\Carbon::setLocale('vi');
        return view('front_end.recruitmentnews',
        [
            'listCategories' => $listCategories,
            'listTags' => $listTags,
            'listQuickSales' => $listQuickSales,
            'listLimitSaleOff' => $listLimitSaleOff,
            'newsDetail' => $newsDetail,
            'listInfo' => $listInfo
        ]);
    }
    // indexSearch
    protected function indexSearch(Request $request)
    {
        // ===============products_categories_detail==================
        $listCategories = array();
        $whereFunctions = array(
            ['cate_parent_id', '=', 0],
            ['status', '<>', 0]
        );
        $listParent = DB::table('products_categories_detail')
                          ->select('cate_id', 
                                   'cate_parent_id',
                                   'cate_slug',
                                   'cate_name', 
                                   'cate_description',
                                   'cate_thumbnail',
                                   'cate_orders',
                                   'cate_pinned')
                          ->where($whereFunctions)
                          ->orderBy('cate_orders', 'asc')
                          ->get();
        $whereFunctions = array(
            ['cate_parent_id', '<>', 0],
            ['status', '<>', 0]
        );
        $listChild = DB::table('products_categories_detail')
                          ->select('cate_id', 
                                   'cate_parent_id',
                                   'cate_slug',
                                   'cate_name', 
                                   'cate_description',
                                   'cate_thumbnail',
                                   'cate_orders',
                                   'cate_pinned')
                          ->where($whereFunctions)
                          ->orderBy('cate_orders', 'asc')
                          ->get();
        foreach ($listParent as $value_p) {
            $itemParent = [
                "cate_id" => $value_p->cate_id,
                "cate_parent_id" => $value_p->cate_parent_id,
                "cate_slug" => $value_p->cate_slug,
                "cate_name" => $value_p->cate_name,
                "cate_description" => $value_p->cate_description,
                "cate_thumbnail" => $value_p->cate_thumbnail,
                "cate_orders" => $value_p->cate_orders,
                "cate_pinned" => $value_p->cate_pinned,
                "nodes" => array()
            ];
            // foreach add con 1
            foreach ($listChild as $value_c) {
                // nếu cate_parent_id = cate_id
                if($value_c->cate_parent_id == $value_p->cate_id)
                {
                    $itemChild = [
                        "cate_id" => $value_c->cate_id,
                        "cate_parent_id" => $value_p->cate_id,
                        "cate_slug" => $value_c->cate_slug,
                        "cate_name" => $value_c->cate_name,
                        "cate_description" => $value_c->cate_description,
                        "cate_thumbnail" => $value_c->cate_thumbnail,
                        "cate_orders" => $value_c->cate_orders,
                        "cate_pinned" => $value_p->cate_pinned,
                        "nodes" => array()
                    ];
                    // add con 2
                    foreach ($listChild as $value_c2) {
                        if($value_c2->cate_parent_id == $value_c->cate_id){
                            $itemChild2 = [
                                "cate_id" => $value_c2->cate_id,
                                "cate_parent_id" => $value_c->cate_id,
                                "cate_slug" => $value_c2->cate_slug,
                                "cate_name" => $value_c2->cate_name,
                                "cate_description" => $value_c2->cate_description,
                                "cate_thumbnail" => $value_c2->cate_thumbnail,
                                "cate_orders" => $value_c2->cate_orders,
                                "cate_pinned" => $value_p->cate_pinned,
                                "nodes" => array()
                            ];
                            array_push($itemChild["nodes"], $itemChild2);  
                        }
                    }
                    // add con 1
                    array_push($itemParent["nodes"], $itemChild);
                }
            }
            //  add cha
            array_push($listCategories, $itemParent);
        }
        // ===============Thẻ Sản Phẩm==================
        $listTags = DB::table('products_tags_detail')
                          ->select('tag_slug',
                                    'tag_id',
                                    'tag_name',
                                    'tag_description',
                                    'created_date',
                                    'created_user',
                                    'updated_date',
                                    'updated_user',
                                    'status')
                          ->orderBy('created_date', 'asc')
                          ->get();
        // ======================San Pham Ban Chay===================
        $listQuickSales = DB::select("
            select
            pr.product_slug,
            pr.product_id,
            pr.product_name,
            pr.product_thumbnail,
            pr.product_imported_prices,
            pr.product_retail_prices,
            pr.product_description,
            pr.updated_date
            FROM products_detail AS pr
            WHERE pr.status != 0 AND pr.product_quick_pinned=1
            ORDER BY pr.updated_date DESC
            LIMIT 10
        ");    
        // ============list limit product_saleoff_pinned: 4==============
        $listLimitSaleOff = DB::select("
            select 
            pr.product_slug,
            pr.product_id,
            pr.product_name,
            pr.product_thumbnail,
            pr.product_imported_prices,
            pr.product_retail_prices,
            pr.product_description,
            pr.product_saleoff_pinned,
            pr.product_new_pinned
            FROM products_detail AS pr
            WHERE pr.product_saleoff_pinned=1
            AND pr.status != 0
            ORDER BY pr.updated_date DESC
            LIMIT 4
        "); 
         //======================= get list  product by cate=====================
        $listCatePro = DB::select("
            select * FROM products_detail AS pr 
            WHERE pr.status != 0
            AND pr.cate_id IN
            (
                SELECT * FROM(
                (
                    SELECT 
                    ca.cate_id
                    FROM products_categories_detail AS ca
                    )
                    UNION
                    (
                    SELECT 
                    ca1.cate_id
                    FROM products_categories_detail AS ca
                    LEFT JOIN products_categories_detail AS ca1 ON ca1.cate_parent_id=ca.cate_id
                    )
                    UNION
                    (
                    SELECT 
                    ca2.cate_id 
                    FROM products_categories_detail AS ca
                    LEFT JOIN products_categories_detail AS ca1 ON ca1.cate_parent_id=ca.cate_id
                    LEFT JOIN products_categories_detail AS ca2 ON ca2.cate_parent_id=ca1.cate_id
                    )
                ) as abc
                

            )
        ");
        // ========================= get company info =============================
        $listInfo = DB::table('company_information')
                          ->select('info_id',
                                    'info_key', 
                                   'info_values')
                          ->get();
       
        // ====================LIST Product search===============
        $keyword = $request->input('kw');
        $product_slug = $this->commonCtl->slugify($keyword);
        $whereFunctions = array(
            ['pd.product_slug', 'LIKE', '%'.str_replace("-","%",$product_slug).'%'],
            ['pd.status', '<>', 0]
        );
        $listSearch = DB::table('products_detail AS pd')
                ->where($whereFunctions)
                ->paginate(12)
                ->setPath('/tim-kiem?kw='.$keyword);

        Carbon\Carbon::setLocale('vi');

        return view('front_end.search',
            [
                'keyword' => $keyword,
                'listSearch' => $listSearch,
                'listCategories' => $listCategories,
                'listTags' => $listTags,
                'listQuickSales' => $listQuickSales,
                'listLimitSaleOff' => $listLimitSaleOff,
                'listCatePro' => $listCatePro,
                'listInfo' => $listInfo
            ]);
    }
    // indexLogin
    protected function indexLogin()
    {
   
        // ===============listCategories==================
        $listCategories = array();
        $whereFunctions = array(
            ['cate_parent_id', '=', 0],
            ['status', '<>', 0]
        );
        $listParent = DB::table('products_categories_detail')
                          ->select('cate_id', 
                                   'cate_parent_id',
                                   'cate_slug',
                                   'cate_name', 
                                   'cate_description',
                                   'cate_thumbnail',
                                   'cate_orders',
                                   'cate_pinned')
                          ->where($whereFunctions)
                          ->orderBy('cate_orders', 'asc')
                          ->get();
        $listProducts = DB::table('products_detail')
                          ->select('cate_id',
                                    'product_slug', 
                                   'product_id',
                                   'product_name',
                                   'product_thumbnail', 
                                   'product_imported_prices',
                                   'product_retail_prices',
                                   'product_description',
                                   'product_saleoff_pinned',
                                   'product_new_pinned')
                          ->where('status', '<>', 0)
                          ->get();
        $whereFunctions = array(
            ['cate_parent_id', '<>', 0],
            ['status', '<>', 0]
        );
        $listChild = DB::table('products_categories_detail')
                          ->select('cate_id', 
                                   'cate_parent_id',
                                   'cate_slug',
                                   'cate_name', 
                                   'cate_description',
                                   'cate_thumbnail',
                                   'cate_orders',
                                   'cate_pinned')
                          ->where($whereFunctions)
                          ->orderBy('cate_orders', 'asc')
                          ->get();

        foreach ($listParent as $value_p) {

            $itemParent = [
                "cate_id" => $value_p->cate_id,
                "cate_parent_id" => $value_p->cate_parent_id,
                "cate_slug" => $value_p->cate_slug,
                "cate_name" => $value_p->cate_name,
                "cate_description" => $value_p->cate_description,
                "cate_thumbnail" => $value_p->cate_thumbnail,
                "cate_orders" => $value_p->cate_orders,
                "cate_pinned" => $value_p->cate_pinned,
                "nodes" => array(),
                "product_list" => array()
            ];
            // foreach add con 1
            foreach ($listChild as $value_c) {
                // nếu cate_parent_id = cate_id
                if($value_c->cate_parent_id == $value_p->cate_id)
                {
                    $itemChild = [
                        "cate_id" => $value_c->cate_id,
                        "cate_parent_id" => $value_p->cate_id,
                        "cate_slug" => $value_c->cate_slug,
                        "cate_name" => $value_c->cate_name,
                        "cate_description" => $value_c->cate_description,
                        "cate_thumbnail" => $value_c->cate_thumbnail,
                        "cate_orders" => $value_c->cate_orders,
                        "cate_pinned" => $value_p->cate_pinned,
                        "nodes" => array()
                    ];
                    // find product con 1
                    // find product con value_p ------------1-------------
                    foreach ($listProducts as $pr) {
                        if($value_c->cate_id == $pr->cate_id)
                        {
                            $itemPro = [
                                "product_slug" => $pr->product_slug,
                                "product_id" => $pr->product_id,
                                "product_name" => $pr->product_name,
                                "product_thumbnail" => $pr->product_thumbnail,
                                "product_imported_prices" => $pr->product_imported_prices,
                                "product_retail_prices" => $pr->product_retail_prices,
                                "product_description" => $pr->product_description,
                                "product_saleoff_pinned" => $pr->product_saleoff_pinned,
                                "product_new_pinned" => $pr->product_new_pinned
                            ];
                            // push item
                            array_push($itemParent["product_list"], $itemPro);  
                        }
                        
                    }
                    //  end each ------------1-------------
                    // add con 2
                    foreach ($listChild as $value_c2) {
                        if($value_c2->cate_parent_id == $value_c->cate_id){
                            $itemChild2 = [
                                "cate_id" => $value_c2->cate_id,
                                "cate_parent_id" => $value_c->cate_id,
                                "cate_slug" => $value_c2->cate_slug,
                                "cate_name" => $value_c2->cate_name,
                                "cate_description" => $value_c2->cate_description,
                                "cate_thumbnail" => $value_c2->cate_thumbnail,
                                "cate_orders" => $value_c2->cate_orders,
                                "cate_pinned" => $value_p->cate_pinned,
                                "nodes" => array()
                            ];
                            array_push($itemChild["nodes"], $itemChild2); 
                            // find product con 2 
                            // find product con value_p ------------2-------------
                            foreach ($listProducts as $pr) {
                                if($value_c2->cate_id == $pr->cate_id)
                                {
                                    $itemPro = [
                                        "product_slug" => $pr->product_slug,
                                        "product_id" => $pr->product_id,
                                        "product_name" => $pr->product_name,
                                        "product_thumbnail" => $pr->product_thumbnail,
                                        "product_imported_prices" => $pr->product_imported_prices,
                                        "product_retail_prices" => $pr->product_retail_prices,
                                        "product_description" => $pr->product_description,
                                        "product_saleoff_pinned" => $pr->product_saleoff_pinned,
                                        "product_new_pinned" => $pr->product_new_pinned
                                    ];
                                    // push item
                                    array_push($itemParent["product_list"], $itemPro);  
                                }
                                
                            }
                            //  end each ------------2-------------
                        }
                    }
                    // add con 1
                    array_push($itemParent["nodes"], $itemChild);
                    
                }
            }
            // find product con value_p ------------0-------------
            foreach ($listProducts as $pr) {
                if($value_p->cate_id == $pr->cate_id)
                {
                    $itemPro = [
                        "product_slug" => $pr->product_slug,
                        "product_id" => $pr->product_id,
                        "product_name" => $pr->product_name,
                        "product_thumbnail" => $pr->product_thumbnail,
                        "product_imported_prices" => $pr->product_imported_prices,
                        "product_retail_prices" => $pr->product_retail_prices,
                        "product_description" => $pr->product_description,
                        "product_saleoff_pinned" => $pr->product_saleoff_pinned,
                        "product_new_pinned" => $pr->product_new_pinned
                    ];
                    // push item
                    array_push($itemParent["product_list"], $itemPro);  
                }
                
            }
            //  end each ------------0-------------
            //  add cha
            array_push($listCategories, $itemParent);
        }
        // ===============Thẻ Sản Phẩm==================
        $listTags = DB::table('products_tags_detail')
                          ->select('tag_slug',
                                    'tag_id',
                                    'tag_name',
                                    'tag_description',
                                    'created_date',
                                    'created_user',
                                    'updated_date',
                                    'updated_user',
                                    'status')
                          ->orderBy('created_date', 'asc')
                          ->get();
        // ======================San Pham Ban Chay===================
        $listQuickSales = DB::select("
            select
            pr.product_slug,
            pr.product_id,
            pr.product_name,
            pr.product_thumbnail,
            pr.product_imported_prices,
            pr.product_retail_prices,
            pr.product_description,
            pr.updated_date
            FROM products_detail AS pr
            WHERE pr.status != 0 AND pr.product_quick_pinned=1
            ORDER BY pr.updated_date DESC
            LIMIT 10
        ");    
        // ============list limit product_saleoff_pinned: 4==============
        $listLimitSaleOff = DB::select("
            select 
            pr.product_slug,
            pr.product_id,
            pr.product_name,
            pr.product_thumbnail,
            pr.product_imported_prices,
            pr.product_retail_prices,
            pr.product_description,
            pr.product_saleoff_pinned,
            pr.product_new_pinned
            FROM products_detail AS pr
            WHERE pr.product_saleoff_pinned=1
            AND pr.status != 0
            ORDER BY pr.updated_date DESC
            LIMIT 4
        "); 
       
        // ========================= get company info =============================
        $listInfo = DB::table('company_information')
                          ->select('info_id',
                                    'info_key', 
                                   'info_values')
                          ->get();
       
       
        // =============language=============
        Carbon\Carbon::setLocale('vi');

        return view('front_end.login',
        [
            'listCategories' => $listCategories,
            'listTags' => $listTags,
            'listQuickSales' => $listQuickSales,
            'listLimitSaleOff' => $listLimitSaleOff,
            'listInfo' => $listInfo
        ]);
    }
    // indexRegister
    protected function indexRegister()
    {
      
        // ===============listCategories==================
        $listCategories = array();
        $whereFunctions = array(
            ['cate_parent_id', '=', 0],
            ['status', '<>', 0]
        );
        $listParent = DB::table('products_categories_detail')
                          ->select('cate_id', 
                                   'cate_parent_id',
                                   'cate_slug',
                                   'cate_name', 
                                   'cate_description',
                                   'cate_thumbnail',
                                   'cate_orders',
                                   'cate_pinned')
                          ->where($whereFunctions)
                          ->orderBy('cate_orders', 'asc')
                          ->get();
        $listProducts = DB::table('products_detail')
                          ->select('cate_id',
                                    'product_slug', 
                                   'product_id',
                                   'product_name',
                                   'product_thumbnail', 
                                   'product_imported_prices',
                                   'product_retail_prices',
                                   'product_description',
                                   'product_saleoff_pinned',
                                   'product_new_pinned')
                          ->where('status', '<>', 0)
                          ->get();
        $whereFunctions = array(
            ['cate_parent_id', '<>', 0],
            ['status', '<>', 0]
        );
        $listChild = DB::table('products_categories_detail')
                          ->select('cate_id', 
                                   'cate_parent_id',
                                   'cate_slug',
                                   'cate_name', 
                                   'cate_description',
                                   'cate_thumbnail',
                                   'cate_orders',
                                   'cate_pinned')
                          ->where($whereFunctions)
                          ->orderBy('cate_orders', 'asc')
                          ->get();

        foreach ($listParent as $value_p) {

            $itemParent = [
                "cate_id" => $value_p->cate_id,
                "cate_parent_id" => $value_p->cate_parent_id,
                "cate_slug" => $value_p->cate_slug,
                "cate_name" => $value_p->cate_name,
                "cate_description" => $value_p->cate_description,
                "cate_thumbnail" => $value_p->cate_thumbnail,
                "cate_orders" => $value_p->cate_orders,
                "cate_pinned" => $value_p->cate_pinned,
                "nodes" => array(),
                "product_list" => array()
            ];
            // foreach add con 1
            foreach ($listChild as $value_c) {
                // nếu cate_parent_id = cate_id
                if($value_c->cate_parent_id == $value_p->cate_id)
                {
                    $itemChild = [
                        "cate_id" => $value_c->cate_id,
                        "cate_parent_id" => $value_p->cate_id,
                        "cate_slug" => $value_c->cate_slug,
                        "cate_name" => $value_c->cate_name,
                        "cate_description" => $value_c->cate_description,
                        "cate_thumbnail" => $value_c->cate_thumbnail,
                        "cate_orders" => $value_c->cate_orders,
                        "cate_pinned" => $value_p->cate_pinned,
                        "nodes" => array()
                    ];
                    // find product con 1
                    // find product con value_p ------------1-------------
                    foreach ($listProducts as $pr) {
                        if($value_c->cate_id == $pr->cate_id)
                        {
                            $itemPro = [
                                "product_slug" => $pr->product_slug,
                                "product_id" => $pr->product_id,
                                "product_name" => $pr->product_name,
                                "product_thumbnail" => $pr->product_thumbnail,
                                "product_imported_prices" => $pr->product_imported_prices,
                                "product_retail_prices" => $pr->product_retail_prices,
                                "product_description" => $pr->product_description,
                                "product_saleoff_pinned" => $pr->product_saleoff_pinned,
                                "product_new_pinned" => $pr->product_new_pinned
                            ];
                            // push item
                            array_push($itemParent["product_list"], $itemPro);  
                        }
                        
                    }
                    //  end each ------------1-------------
                    // add con 2
                    foreach ($listChild as $value_c2) {
                        if($value_c2->cate_parent_id == $value_c->cate_id){
                            $itemChild2 = [
                                "cate_id" => $value_c2->cate_id,
                                "cate_parent_id" => $value_c->cate_id,
                                "cate_slug" => $value_c2->cate_slug,
                                "cate_name" => $value_c2->cate_name,
                                "cate_description" => $value_c2->cate_description,
                                "cate_thumbnail" => $value_c2->cate_thumbnail,
                                "cate_orders" => $value_c2->cate_orders,
                                "cate_pinned" => $value_p->cate_pinned,
                                "nodes" => array()
                            ];
                            array_push($itemChild["nodes"], $itemChild2); 
                            // find product con 2 
                            // find product con value_p ------------2-------------
                            foreach ($listProducts as $pr) {
                                if($value_c2->cate_id == $pr->cate_id)
                                {
                                    $itemPro = [
                                        "product_slug" => $pr->product_slug,
                                        "product_id" => $pr->product_id,
                                        "product_name" => $pr->product_name,
                                        "product_thumbnail" => $pr->product_thumbnail,
                                        "product_imported_prices" => $pr->product_imported_prices,
                                        "product_retail_prices" => $pr->product_retail_prices,
                                        "product_description" => $pr->product_description,
                                        "product_saleoff_pinned" => $pr->product_saleoff_pinned,
                                        "product_new_pinned" => $pr->product_new_pinned
                                    ];
                                    // push item
                                    array_push($itemParent["product_list"], $itemPro);  
                                }
                                
                            }
                            //  end each ------------2-------------
                        }
                    }
                    // add con 1
                    array_push($itemParent["nodes"], $itemChild);
                    
                }
            }
            // find product con value_p ------------0-------------
            foreach ($listProducts as $pr) {
                if($value_p->cate_id == $pr->cate_id)
                {
                    $itemPro = [
                        "product_slug" => $pr->product_slug,
                        "product_id" => $pr->product_id,
                        "product_name" => $pr->product_name,
                        "product_thumbnail" => $pr->product_thumbnail,
                        "product_imported_prices" => $pr->product_imported_prices,
                        "product_retail_prices" => $pr->product_retail_prices,
                        "product_description" => $pr->product_description,
                        "product_saleoff_pinned" => $pr->product_saleoff_pinned,
                        "product_new_pinned" => $pr->product_new_pinned
                    ];
                    // push item
                    array_push($itemParent["product_list"], $itemPro);  
                }
                
            }
            //  end each ------------0-------------
            //  add cha
            array_push($listCategories, $itemParent);
        }
        // ===============Thẻ Sản Phẩm==================
        $listTags = DB::table('products_tags_detail')
                          ->select('tag_slug',
                                    'tag_id',
                                    'tag_name',
                                    'tag_description',
                                    'created_date',
                                    'created_user',
                                    'updated_date',
                                    'updated_user',
                                    'status')
                          ->orderBy('created_date', 'asc')
                          ->get();
        // ======================San Pham Ban Chay===================
        $listQuickSales = DB::select("
            select
            pr.product_slug,
            pr.product_id,
            pr.product_name,
            pr.product_thumbnail,
            pr.product_imported_prices,
            pr.product_retail_prices,
            pr.product_description,
            pr.updated_date
            FROM products_detail AS pr
            WHERE pr.status != 0 AND pr.product_quick_pinned=1
            ORDER BY pr.updated_date DESC
            LIMIT 10
        ");  
        // ============list limit product_saleoff_pinned: 4==============
        $listLimitSaleOff = DB::select("
            select 
            pr.product_slug,
            pr.product_id,
            pr.product_name,
            pr.product_thumbnail,
            pr.product_imported_prices,
            pr.product_retail_prices,
            pr.product_description,
            pr.product_saleoff_pinned,
            pr.product_new_pinned
            FROM products_detail AS pr
            WHERE pr.product_saleoff_pinned=1
            AND pr.status != 0
            ORDER BY pr.updated_date DESC
            LIMIT 4
        "); 
     
        // ========================= get company info =============================
        $listInfo = DB::table('company_information')
                          ->select('info_id',
                                    'info_key', 
                                   'info_values')
                          ->get();

        // =============language=============
        Carbon\Carbon::setLocale('vi');

        return view('front_end.register',
        [
            'listCategories' => $listCategories,
            'listTags' => $listTags,
            'listQuickSales' => $listQuickSales,
            'listLimitSaleOff' => $listLimitSaleOff,
            'listInfo' => $listInfo
        ]);
    }
    // indexCustomerInfo
    protected function indexCustomerInfo()
    {
      
        // ===============listCategories==================
        $listCategories = array();
        $whereFunctions = array(
            ['cate_parent_id', '=', 0],
            ['status', '<>', 0]
        );
        $listParent = DB::table('products_categories_detail')
                          ->select('cate_id', 
                                   'cate_parent_id',
                                   'cate_slug',
                                   'cate_name', 
                                   'cate_description',
                                   'cate_thumbnail',
                                   'cate_orders',
                                   'cate_pinned')
                          ->where($whereFunctions)
                          ->orderBy('cate_orders', 'asc')
                          ->get();
        $listProducts = DB::table('products_detail')
                          ->select('cate_id',
                                    'product_slug', 
                                   'product_id',
                                   'product_name',
                                   'product_thumbnail', 
                                   'product_imported_prices',
                                   'product_retail_prices',
                                   'product_description',
                                   'product_saleoff_pinned',
                                   'product_new_pinned')
                          ->where('status', '<>', 0)
                          ->get();
        $whereFunctions = array(
            ['cate_parent_id', '<>', 0],
            ['status', '<>', 0]
        );
        $listChild = DB::table('products_categories_detail')
                          ->select('cate_id', 
                                   'cate_parent_id',
                                   'cate_slug',
                                   'cate_name', 
                                   'cate_description',
                                   'cate_thumbnail',
                                   'cate_orders',
                                   'cate_pinned')
                          ->where($whereFunctions)
                          ->orderBy('cate_orders', 'asc')
                          ->get();

        foreach ($listParent as $value_p) {

            $itemParent = [
                "cate_id" => $value_p->cate_id,
                "cate_parent_id" => $value_p->cate_parent_id,
                "cate_slug" => $value_p->cate_slug,
                "cate_name" => $value_p->cate_name,
                "cate_description" => $value_p->cate_description,
                "cate_thumbnail" => $value_p->cate_thumbnail,
                "cate_orders" => $value_p->cate_orders,
                "cate_pinned" => $value_p->cate_pinned,
                "nodes" => array(),
                "product_list" => array()
            ];
            // foreach add con 1
            foreach ($listChild as $value_c) {
                // nếu cate_parent_id = cate_id
                if($value_c->cate_parent_id == $value_p->cate_id)
                {
                    $itemChild = [
                        "cate_id" => $value_c->cate_id,
                        "cate_parent_id" => $value_p->cate_id,
                        "cate_slug" => $value_c->cate_slug,
                        "cate_name" => $value_c->cate_name,
                        "cate_description" => $value_c->cate_description,
                        "cate_thumbnail" => $value_c->cate_thumbnail,
                        "cate_orders" => $value_c->cate_orders,
                        "cate_pinned" => $value_p->cate_pinned,
                        "nodes" => array()
                    ];
                    // find product con 1
                    // find product con value_p ------------1-------------
                    foreach ($listProducts as $pr) {
                        if($value_c->cate_id == $pr->cate_id)
                        {
                            $itemPro = [
                                "product_slug" => $pr->product_slug,
                                "product_id" => $pr->product_id,
                                "product_name" => $pr->product_name,
                                "product_thumbnail" => $pr->product_thumbnail,
                                "product_imported_prices" => $pr->product_imported_prices,
                                "product_retail_prices" => $pr->product_retail_prices,
                                "product_description" => $pr->product_description,
                                "product_saleoff_pinned" => $pr->product_saleoff_pinned,
                                "product_new_pinned" => $pr->product_new_pinned
                            ];
                            // push item
                            array_push($itemParent["product_list"], $itemPro);  
                        }
                        
                    }
                    //  end each ------------1-------------
                    // add con 2
                    foreach ($listChild as $value_c2) {
                        if($value_c2->cate_parent_id == $value_c->cate_id){
                            $itemChild2 = [
                                "cate_id" => $value_c2->cate_id,
                                "cate_parent_id" => $value_c->cate_id,
                                "cate_slug" => $value_c2->cate_slug,
                                "cate_name" => $value_c2->cate_name,
                                "cate_description" => $value_c2->cate_description,
                                "cate_thumbnail" => $value_c2->cate_thumbnail,
                                "cate_orders" => $value_c2->cate_orders,
                                "cate_pinned" => $value_p->cate_pinned,
                                "nodes" => array()
                            ];
                            array_push($itemChild["nodes"], $itemChild2); 
                            // find product con 2 
                            // find product con value_p ------------2-------------
                            foreach ($listProducts as $pr) {
                                if($value_c2->cate_id == $pr->cate_id)
                                {
                                    $itemPro = [
                                        "product_slug" => $pr->product_slug,
                                        "product_id" => $pr->product_id,
                                        "product_name" => $pr->product_name,
                                        "product_thumbnail" => $pr->product_thumbnail,
                                        "product_imported_prices" => $pr->product_imported_prices,
                                        "product_retail_prices" => $pr->product_retail_prices,
                                        "product_description" => $pr->product_description,
                                        "product_saleoff_pinned" => $pr->product_saleoff_pinned,
                                        "product_new_pinned" => $pr->product_new_pinned
                                    ];
                                    // push item
                                    array_push($itemParent["product_list"], $itemPro);  
                                }
                                
                            }
                            //  end each ------------2-------------
                        }
                    }
                    // add con 1
                    array_push($itemParent["nodes"], $itemChild);
                    
                }
            }
            // find product con value_p ------------0-------------
            foreach ($listProducts as $pr) {
                if($value_p->cate_id == $pr->cate_id)
                {
                    $itemPro = [
                        "product_slug" => $pr->product_slug,
                        "product_id" => $pr->product_id,
                        "product_name" => $pr->product_name,
                        "product_thumbnail" => $pr->product_thumbnail,
                        "product_imported_prices" => $pr->product_imported_prices,
                        "product_retail_prices" => $pr->product_retail_prices,
                        "product_description" => $pr->product_description,
                        "product_saleoff_pinned" => $pr->product_saleoff_pinned,
                        "product_new_pinned" => $pr->product_new_pinned
                    ];
                    // push item
                    array_push($itemParent["product_list"], $itemPro);  
                }
                
            }
            //  end each ------------0-------------
            //  add cha
            array_push($listCategories, $itemParent);
        }
        // ===============Thẻ Sản Phẩm==================
        $listTags = DB::table('products_tags_detail')
                          ->select('tag_slug',
                                    'tag_id',
                                    'tag_name',
                                    'tag_description',
                                    'created_date',
                                    'created_user',
                                    'updated_date',
                                    'updated_user',
                                    'status')
                          ->orderBy('created_date', 'asc')
                          ->get();
        // ======================San Pham Ban Chay===================
        $listQuickSales = DB::select("
            select
            pr.product_slug,
            pr.product_id,
            pr.product_name,
            pr.product_thumbnail,
            pr.product_imported_prices,
            pr.product_retail_prices,
            pr.product_description,
            pr.updated_date
            FROM products_detail AS pr
            WHERE pr.status != 0 AND pr.product_quick_pinned=1
            ORDER BY pr.updated_date DESC
            LIMIT 10
        "); 
        // ============list limit product_saleoff_pinned: 4==============
        $listLimitSaleOff = DB::select("
            select 
            pr.product_slug,
            pr.product_id,
            pr.product_name,
            pr.product_thumbnail,
            pr.product_imported_prices,
            pr.product_retail_prices,
            pr.product_description,
            pr.product_saleoff_pinned,
            pr.product_new_pinned
            FROM products_detail AS pr
            WHERE pr.product_saleoff_pinned=1
            AND pr.status != 0
            ORDER BY pr.updated_date DESC
            LIMIT 4
        "); 
     
        // ========================= get company info =============================
        $listInfo = DB::table('company_information')
                          ->select('info_id',
                                    'info_key', 
                                   'info_values')
                          ->get();

        // =============language=============
        Carbon\Carbon::setLocale('vi');

        return view('front_end.customerinfo',
        [
            'listCategories' => $listCategories,
            'listTags' => $listTags,
            'listQuickSales' => $listQuickSales,
            'listLimitSaleOff' => $listLimitSaleOff,
            'listInfo' => $listInfo
        ]);
    }
    // indexContact return view('front_end.contact');
    protected function indexContact()
    {
      
        // ===============listCategories==================
        $listCategories = array();
        $whereFunctions = array(
            ['cate_parent_id', '=', 0],
            ['status', '<>', 0]
        );
        $listParent = DB::table('products_categories_detail')
                          ->select('cate_id', 
                                   'cate_parent_id',
                                   'cate_slug',
                                   'cate_name', 
                                   'cate_description',
                                   'cate_thumbnail',
                                   'cate_orders',
                                   'cate_pinned')
                          ->where($whereFunctions)
                          ->orderBy('cate_orders', 'asc')
                          ->get();
        $listProducts = DB::table('products_detail')
                          ->select('cate_id',
                                    'product_slug', 
                                   'product_id',
                                   'product_name',
                                   'product_thumbnail', 
                                   'product_imported_prices',
                                   'product_retail_prices',
                                   'product_description',
                                   'product_saleoff_pinned',
                                   'product_new_pinned')
                          ->where('status', '<>', 0)
                          ->get();
        $whereFunctions = array(
            ['cate_parent_id', '<>', 0],
            ['status', '<>', 0]
        );
        $listChild = DB::table('products_categories_detail')
                          ->select('cate_id', 
                                   'cate_parent_id',
                                   'cate_slug',
                                   'cate_name', 
                                   'cate_description',
                                   'cate_thumbnail',
                                   'cate_orders',
                                   'cate_pinned')
                          ->where($whereFunctions)
                          ->orderBy('cate_orders', 'asc')
                          ->get();

        foreach ($listParent as $value_p) {

            $itemParent = [
                "cate_id" => $value_p->cate_id,
                "cate_parent_id" => $value_p->cate_parent_id,
                "cate_slug" => $value_p->cate_slug,
                "cate_name" => $value_p->cate_name,
                "cate_description" => $value_p->cate_description,
                "cate_thumbnail" => $value_p->cate_thumbnail,
                "cate_orders" => $value_p->cate_orders,
                "cate_pinned" => $value_p->cate_pinned,
                "nodes" => array(),
                "product_list" => array()
            ];
            // foreach add con 1
            foreach ($listChild as $value_c) {
                // nếu cate_parent_id = cate_id
                if($value_c->cate_parent_id == $value_p->cate_id)
                {
                    $itemChild = [
                        "cate_id" => $value_c->cate_id,
                        "cate_parent_id" => $value_p->cate_id,
                        "cate_slug" => $value_c->cate_slug,
                        "cate_name" => $value_c->cate_name,
                        "cate_description" => $value_c->cate_description,
                        "cate_thumbnail" => $value_c->cate_thumbnail,
                        "cate_orders" => $value_c->cate_orders,
                        "cate_pinned" => $value_p->cate_pinned,
                        "nodes" => array()
                    ];
                    // find product con 1
                    // find product con value_p ------------1-------------
                    foreach ($listProducts as $pr) {
                        if($value_c->cate_id == $pr->cate_id)
                        {
                            $itemPro = [
                                "product_slug" => $pr->product_slug,
                                "product_id" => $pr->product_id,
                                "product_name" => $pr->product_name,
                                "product_thumbnail" => $pr->product_thumbnail,
                                "product_imported_prices" => $pr->product_imported_prices,
                                "product_retail_prices" => $pr->product_retail_prices,
                                "product_description" => $pr->product_description,
                                "product_saleoff_pinned" => $pr->product_saleoff_pinned,
                                "product_new_pinned" => $pr->product_new_pinned
                            ];
                            // push item
                            array_push($itemParent["product_list"], $itemPro);  
                        }
                        
                    }
                    //  end each ------------1-------------
                    // add con 2
                    foreach ($listChild as $value_c2) {
                        if($value_c2->cate_parent_id == $value_c->cate_id){
                            $itemChild2 = [
                                "cate_id" => $value_c2->cate_id,
                                "cate_parent_id" => $value_c->cate_id,
                                "cate_slug" => $value_c2->cate_slug,
                                "cate_name" => $value_c2->cate_name,
                                "cate_description" => $value_c2->cate_description,
                                "cate_thumbnail" => $value_c2->cate_thumbnail,
                                "cate_orders" => $value_c2->cate_orders,
                                "cate_pinned" => $value_p->cate_pinned,
                                "nodes" => array()
                            ];
                            array_push($itemChild["nodes"], $itemChild2); 
                            // find product con 2 
                            // find product con value_p ------------2-------------
                            foreach ($listProducts as $pr) {
                                if($value_c2->cate_id == $pr->cate_id)
                                {
                                    $itemPro = [
                                        "product_slug" => $pr->product_slug,
                                        "product_id" => $pr->product_id,
                                        "product_name" => $pr->product_name,
                                        "product_thumbnail" => $pr->product_thumbnail,
                                        "product_imported_prices" => $pr->product_imported_prices,
                                        "product_retail_prices" => $pr->product_retail_prices,
                                        "product_description" => $pr->product_description,
                                        "product_saleoff_pinned" => $pr->product_saleoff_pinned,
                                        "product_new_pinned" => $pr->product_new_pinned
                                    ];
                                    // push item
                                    array_push($itemParent["product_list"], $itemPro);  
                                }
                                
                            }
                            //  end each ------------2-------------
                        }
                    }
                    // add con 1
                    array_push($itemParent["nodes"], $itemChild);
                    
                }
            }
            // find product con value_p ------------0-------------
            foreach ($listProducts as $pr) {
                if($value_p->cate_id == $pr->cate_id)
                {
                    $itemPro = [
                        "product_slug" => $pr->product_slug,
                        "product_id" => $pr->product_id,
                        "product_name" => $pr->product_name,
                        "product_thumbnail" => $pr->product_thumbnail,
                        "product_imported_prices" => $pr->product_imported_prices,
                        "product_retail_prices" => $pr->product_retail_prices,
                        "product_description" => $pr->product_description,
                        "product_saleoff_pinned" => $pr->product_saleoff_pinned,
                        "product_new_pinned" => $pr->product_new_pinned
                    ];
                    // push item
                    array_push($itemParent["product_list"], $itemPro);  
                }
                
            }
            //  end each ------------0-------------
            //  add cha
            array_push($listCategories, $itemParent);
        }
        // ===============Thẻ Sản Phẩm==================
        $listTags = DB::table('products_tags_detail')
                          ->select('tag_slug',
                                    'tag_id',
                                    'tag_name',
                                    'tag_description',
                                    'created_date',
                                    'created_user',
                                    'updated_date',
                                    'updated_user',
                                    'status')
                          ->orderBy('created_date', 'asc')
                          ->get();
        // ======================San Pham Ban Chay===================
        $listQuickSales = DB::select("
            select
            pr.product_slug,
            pr.product_id,
            pr.product_name,
            pr.product_thumbnail,
            pr.product_imported_prices,
            pr.product_retail_prices,
            pr.product_description,
            pr.updated_date
            FROM products_detail AS pr
            WHERE pr.status != 0 AND pr.product_quick_pinned=1
            ORDER BY pr.updated_date DESC
            LIMIT 10
        "); 
        // ============list limit product_saleoff_pinned: 4==============
        $listLimitSaleOff = DB::select("
            select 
            pr.product_slug,
            pr.product_id,
            pr.product_name,
            pr.product_thumbnail,
            pr.product_imported_prices,
            pr.product_retail_prices,
            pr.product_description,
            pr.product_saleoff_pinned,
            pr.product_new_pinned
            FROM products_detail AS pr
            WHERE pr.product_saleoff_pinned=1
            AND pr.status != 0
            ORDER BY pr.updated_date DESC
            LIMIT 4
        "); 
     
        // ========================= get company info =============================
        $listInfo = DB::table('company_information')
                          ->select('info_id',
                                    'info_key', 
                                   'info_values')
                          ->get();

        // =============language=============
        Carbon\Carbon::setLocale('vi');

        return view('front_end.contact',
        [
            'listCategories' => $listCategories,
            'listTags' => $listTags,
            'listQuickSales' => $listQuickSales,
            'listLimitSaleOff' => $listLimitSaleOff,
            'listInfo' => $listInfo
        ]);
    }
    protected function indexAbout(Request $request)
    {
        return view('front_end.about');
    }
    protected function indexLogout(Request $request)
    {   
        $request->session()->forget('customerInfo');
        return Redirect::to(''); 
    }
    protected function indexPageDetail(Request $request,$slug)
    {
        
        $whereFunctions = array(
            ['pd.page_slug', '=', $slug],
            ['pd.status', '<>', 0]
        );

        $pageDetail = DB::table('system_pages_detail AS pd')
                ->leftJoin('system_pages_categories_detail AS cd', 'cd.cate_id', '=', 'pd.cate_id')
                ->select('pd.page_id', 
                         'pd.page_slug', 
                         'pd.page_title', 
                         'pd.page_content', 
                         'pd.updated_date', 
                         'pd.status', 
                         'cd.cate_id',
                         'cd.cate_slug',
                         'cd.cate_name')
                ->where($whereFunctions)
                ->first();

        if($pageDetail === null){
            return view('front_end.errors.404');
        }
        Carbon\Carbon::setLocale('vi');

        return view('front_end.pagedetail',
        [
            'pageDetail' => $pageDetail,
        ]);
    }
 
}
