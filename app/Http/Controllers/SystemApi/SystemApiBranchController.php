<?php

namespace App\Http\Controllers\SystemApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use DateTime;
use Validator;
use Mail;
use Lang;

use App\Http\Controllers\SystemApi\SystemCommonController as commonCtl;

use App\Models\BranchesDetail;

class SystemApiBranchController extends Controller
{

    /*** Khởi Tạo Giá Trị ***/
    private $commonCtl;

    private $sessionUser;


    /*** Khởi Tạo Giá Trị ***/
    public function __construct(commonCtl $SystemCommonController){
        $this->commonCtl =  $SystemCommonController;
    }

    //Ajax Post Lấy Danh Sách 
    protected function apiGetList(Request $request){  
        
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }
        // $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $listItem = DB::table('branches_detail as br')
                ->select(
                    'br.branch_id',
                    'br.branch_code',
                    'br.branch_name',
                    'br.branch_address_number',
                    'br.branch_phone',
                    'br.created_date',
                    DB::raw("DATE_FORMAT(br.created_date,'%d/%m/%Y') AS created_date_format"),
                    'br.created_user',
                    'br.updated_date',
                    'br.updated_user',
                    'br.status'
                    )
                ->get();

        if(!$listItem->isEmpty())
        {
            $response["success"] = $listItem;
        }
        else
        {
            $response["warning"] = Lang::get('messages.common_warning_empty_list');
        }

        return response()->json($response);
    } 
    //Ajax Post Thêm Mới
    protected function apiAdd(Request $request){  
        
        //Kiểm tra phân quyền sử dụng
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }
        // $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";


        /** Khởi tạo giá trị **/
        $branch_code = $this->commonCtl->slugify($request->input('branch_name'));
        if(empty($branch_code))
        {
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }

        $existsItem = BranchesDetail::where('branch_code','=',$branch_code)
                        ->first();

        if(!empty($existsItem))
        {
            $response["warning"] = [$this->commonCtl->replaceTitle(Lang::get('messages.common_error_exist_data'), ['Mã', 'Chi Nhánh'])];
            return response()->json($response);
        }                

        $itemAdd = new BranchesDetail;

        $itemAdd->fill($request->all());

        $itemAdd->branch_code = $branch_code;

        $itemAdd->created_user = Session::get('userAuth')->user_id;;
        $itemAdd->created_date = $this->commonCtl->getCarbonNow();
        $itemAdd->updated_user = Session::get('userAuth')->user_id;;
        $itemAdd->updated_date = $this->commonCtl->getCarbonNow();
        $itemAdd->status = 1;

        DB::beginTransaction();

        try {
            $itemAdd->save();

            $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_created'), ['Chi Nhánh']);
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception').$e;
            return response()->json($response);
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception').$e;
            return response()->json($response);
        }
        DB::commit();

        return response()->json($response);
    } 

    //Ajax Post Cập Nhật
    protected function apiUpdate(Request $request){  

        // //Kiểm tra phân quyền sử dụng
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }
        // $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";



        $existsItem = BranchesDetail::where('branch_id','=',$request->input('branch_id'))
                        ->first();

        if(empty($existsItem))
        {
            $response["warning"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' Chi Nhánh']);
            return response()->json($response);
        }

        $branch_code = $this->commonCtl->slugify($request->input('branch_name'));
        if(empty($branch_code))
        {
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }

        $existsSlug = BranchesDetail::where([
            ['branch_code','<>',$existsItem->branch_code],
            ['branch_code','=',$branch_code]
            ])->first();
        if($existsSlug)
        {
            $response["warning"] = [$this->commonCtl->replaceTitle(Lang::get('messages.common_error_exist_data'), ['Mã', $request->input('branch_name')])];
            return response()->json($response);
        }

        $existsItem->branch_code = $branch_code;
        $existsItem->branch_name = $request->input('branch_name');
        $existsItem->branch_address_number = $request->input('branch_address_number');
        $existsItem->branch_phone = $request->input('branch_phone');
        $existsItem->updated_user = Session::get('userAuth')->user_id;;
        $existsItem->updated_date = $this->commonCtl->getCarbonNow();

        DB::beginTransaction();
        try 
        {
            $existsItem->save();

            $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_updated'), ['Chi Nhánh']);
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);

            // return Redirect::to('/form')
            // ->withErrors( $e->getErrors() )
            // ->withInput();
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
            // throw $e;
        }

        DB::commit();

        return response()->json($response);
    } 
    //apiRemove
    protected function apiRemove(Request $request){ 

        //Kiểm tra phân quyền sử dụng
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }
        // $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

  
        //  kiem tra bi xoa
        $itemDelete = BranchesDetail::where('branch_id', '=', $request->input('branch_id'))->first();

        if(empty($itemDelete))
        {
            $response["error"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' Chi Nhánh']);
            return response()->json($response);
        }

        DB::beginTransaction();
        try {
          
                $deletedRows = BranchesDetail::where('branch_id', '=', $itemDelete->branch_id)->delete();

                if($deletedRows > 0){
                    $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_removed'), ['Chi Nhánh']);
                }
                else{
                    $response["warning"] = "Lỗi: Không Xóa Được";
                }
           
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);

            // return Redirect::to('/form')
            // ->withErrors( $e->getErrors() )
            // ->withInput();
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
            // throw $e;
        }
        DB::commit();

        return response()->json($response);
    }
}
