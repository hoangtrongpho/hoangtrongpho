<?php

namespace App\Http\Controllers\SystemApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use DateTime;
use Validator;
use Mail;
use Lang;

use App\Http\Controllers\SystemApi\SystemCommonController as commonCtl;
use App\Models\NewsTagsDetail;
use App\Models\NewsTagsList;
use App\Models\ProductsColorsDetail;
use App\Models\ProductsDetail;

class SystemApiProductColorController extends Controller
{

    /*** Khởi Tạo Giá Trị ***/
    private $commonCtl;

    private $sessionUser;

    private $rulesMess = [
        'color_id.required' => 'Vui Lòng Thêm ID Màu Sắc Sản Phẩm.',
        'color_slug.required' => 'Vui Lòng Thêm Mã Màu Sắc Sản Phẩm.',
        'color_name.required' => 'Vui Lòng Thêm Tên Màu Sắc Sản Phẩm.',
        'color_primary_code.required' => 'Vui Lòng Thêm Mã Màu Sắc Sản Phẩm.',
    ];

    /*** Khởi Tạo Giá Trị ***/
    public function __construct(commonCtl $SystemCommonController){
        $this->commonCtl =  $SystemCommonController;
    }

     //Ajax Post Lấy Danh Sách 
    protected function apiGetList(Request $request){  
        
        $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        if($auth["auth"])
        {
            return response()->json($auth);
        }
        $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $listItem = DB::table('products_colors_detail as cd')
                ->leftJoin('products_detail AS pd', 'pd.color_id', '=', 'pd.color_id')
                ->select('cd.color_id', 
                        'cd.color_slug',
                        'cd.color_name',
                        'cd.color_description',
                        'cd.color_code',
                        'cd.color_duotone',
                        DB::raw('DATE_FORMAT(cd.updated_date,"%d/%m/%Y %H:%m:%s") AS updated_date'),
                        'cd.status',
                        DB::raw('count(pd.color_id) as item_couter'))
                ->where('cd.status','<>',0)
                ->groupBy('cd.color_id', 
                        'cd.color_slug',
                        'cd.color_name',
                        'cd.color_description',
                        'cd.color_code',
                        'cd.color_duotone',
                        'cd.updated_date',
                        'cd.status')
                ->get();

        if(!$listItem->isEmpty())
        {
            $response["success"] = $listItem;
        }
        else
        {
            $response["warning"] = Lang::get('messages.common_warning_empty_list');
        }

        return response()->json($response);
    } 

    //Ajax Post Thêm Mới
    protected function apiAdd(Request $request){  
        
        //Kiểm tra phân quyền sử dụng
        $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        if($auth["auth"])
        {
            return response()->json($auth);
        }
        $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";


        /** Khởi tạo giá trị **/
        $color_slug = $this->commonCtl->slugify($request->input('color_name'));
        if(empty($color_slug))
        {
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }

        $existsItem = ProductsColorsDetail::where('color_slug','=',$color_slug)
                        ->first();
        if(!empty($existsItem))
        {
            $response["warning"] = [$this->commonCtl->replaceTitle(Lang::get('messages.common_error_exist_data'), ['Mã', 'màu'])];
            return response()->json($response);
        }                

        $itemAdd = new ProductsColorsDetail;

        $itemAdd->fill($request->all());

        $itemAdd->color_slug = $color_slug;

        $itemAdd->created_user = $this->sessionUser->user_id;
        $itemAdd->created_date = $this->commonCtl->getCarbonNow();
        $itemAdd->updated_user = $this->sessionUser->user_id;
        $itemAdd->updated_date = $this->commonCtl->getCarbonNow();
        $itemAdd->status = 1;

        DB::beginTransaction();

        try {
            $itemAdd->save();

            $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_created'), ['màu']);
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception').$e;
            return response()->json($response);

            // return Redirect::to('/form')
            // ->withErrors( $e->getErrors() )
            // ->withInput();
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception').$e;
            return response()->json($response);
            // throw $e;
        }
        DB::commit();

        return response()->json($response);
    } 

    //Ajax Post Cập Nhật
    protected function apiUpdate(Request $request){  

        //Kiểm tra phân quyền sử dụng
        $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        if($auth["auth"])
        {
            return response()->json($auth);
        }
        $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $rules = [
            'color_id' => 'required',
            'color_slug' => 'required',
            'color_code' => 'required',
            'color_name' => 'required',
        ];

        $valiData = $this->commonCtl->validRequest($request,$rules,$this->rulesMess);

        /** Kiểm tra giá trị nhập vào **/
        if($valiData->fails())
        {   
            $response["warning"] = $valiData->errors();
            return response()->json($response);
        }

        $existsItem = ProductsColorsDetail::where('color_id','=',$request->input('color_id'))
                        ->first();

        if(empty($existsItem))
        {
            $response["warning"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' màu']);
            return response()->json($response);
        }

        $color_slug = $this->commonCtl->slugify($request->input('color_name'));
        if(empty($color_slug))
        {
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }

        $existsSlug = ProductsColorsDetail::where([
                                                    ['color_slug','=',$color_slug],
                                                    ['color_id','=',$existsItem->color_id],
                                                ])->first();
        if($existsSlug)
        {
            $response["warning"] = [$this->commonCtl->replaceTitle(Lang::get('messages.common_error_exist_data'), ['Mã', $request->input('color_name')])];
            return response()->json($response);
        }

        $existsItem->color_name = $color_slug;
        $existsItem->color_name = $request->input('color_name');
        //color_description
        $existsItem->color_description = $request->input('color_description');
        $existsItem->color_code = $request->input('color_code');
        $existsItem->updated_user = $this->sessionUser->user_id;
        $existsItem->updated_date = $this->commonCtl->getCarbonNow();

        DB::beginTransaction();
        try 
        {
            $existsItem->save();

            $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_updated'), ['màu']);
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);

            // return Redirect::to('/form')
            // ->withErrors( $e->getErrors() )
            // ->withInput();
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
            // throw $e;
        }

        DB::commit();

        return response()->json($response);
    } 

    //Ajax Post Xóa
    protected function apiRemove(Request $request){ 

        //Kiểm tra phân quyền sử dụng
        $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        if($auth["auth"])
        {
            return response()->json($auth);
        }
        $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        /** Kiểm tra giá trị nhập vào **/
        $rules = [
            'color_id' => 'required',
        ];

        $valiData = $this->commonCtl->validRequest($request,$rules,$this->rulesMess);

        if($valiData->fails())
        {   
            $response["warning"] = $valiData->errors();
            return response()->json($response);
        }

        $itemDelete = ProductsColorsDetail::where('color_id', '=', $request->input('color_id'))->first();

        if(empty($itemDelete))
        {
            $response["error"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' màu']);
            return response()->json($response);
        }

        $checkExists = ProductsDetail::where('color_id', '=', $request->input('color_id'))->get();

        DB::beginTransaction();
        try {

            if(!$checkExists->isEmpty())
            {
                $itemDelete->updated_user = $this->sessionUser->user_id;
                $itemDelete->updated_date = $this->commonCtl->getCarbonNow();
                $itemDelete->status = 0;

                $itemDelete->save();

                $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_removed'), ['màu']);
            }
            else
            {
                $deletedRows = ProductsColorsDetail::where('color_id', '=', $itemDelete->color_id)->delete();

                if($deletedRows > 0){
                    $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_removed'), ['màu']);
                }
                else{
                    $response["warning"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_removed'), ['màu']);
                }
            }
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);

            // return Redirect::to('/form')
            // ->withErrors( $e->getErrors() )
            // ->withInput();
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
            // throw $e;
        }
        DB::commit();

        return response()->json($response);
    }
}
