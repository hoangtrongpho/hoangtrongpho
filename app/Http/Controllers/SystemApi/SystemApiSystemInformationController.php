<?php

namespace App\Http\Controllers\SystemApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use DateTime;
use Validator;
use Mail;
use Lang;

use App\Http\Controllers\SystemApi\SystemCommonController as commonCtl;

use App\Models\CompanyInformation;
use App\Models\SlideDetail;

class SystemApiSystemInformationController extends Controller
{

    /*** Khởi Tạo Giá Trị ***/
    private $commonCtl;

    private $sessionUser;

    /*!! Khởi Tạo Giá Trị !!*/

    public function __construct(commonCtl $SystemCommonController){
        $this->commonCtl =  $SystemCommonController;
    }

    /*** API Hàm Chức Năng ***/  

    // apiGetCustomerList
    protected function apiGetList(Request   $request){  

        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $whereFunctions = array();

        $listCustomers = DB::table('company_information AS in')
                ->select( 
                        'in.info_id',
                        'in.info_key',
                        'in.info_values'
                         )
                ->where($whereFunctions)
                ->get();

        if(!$listCustomers->isEmpty())
        {
            $response["success"] = $listCustomers;
        }
        else
        {
            $response["warning"] = Lang::get('messages.common_warning_empty_list');
        }

        return response()->json($response);
    }
    /*------------------------------API Update -----------------------*/
    protected function apiUpdate(Request $request){   

        // $this->sessionUser = Session::get('userAuth'); 
        
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }
        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        DB::beginTransaction();

        try {

            if(count($request->input('dtgListInfo')) > 0)
            {
                foreach ($request->input('dtgListInfo') as $item) {
                    $info = CompanyInformation::where('info_id','=',$item['info_id'])->first();
                    if($info !== null){
                        $info->info_key =  $item['info_key'];
                        $info->info_values = $item['info_values'];
                        $info->save();
                    }
                }
            }
            // return success message
            $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_updated'), ['Thông Tin']);
             //$response["data"] = $item;
            
        } 
        catch(ValidationException $e)
        {
            DB::rollback();

            // $response["error"] = $e->getErrors();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response).$e;
        }

        DB::commit();

        return response()->json($response);
    }
    // ===================apiRemoveSlide=================
    protected function apiRemoveSlide(Request $request){ 

        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";
        // check ton tai to delete
        $deleteItem = SlideDetail::where('slide_id', '=', $request->input('slide_id'))->first();

        if($deleteItem == null)
        {
            $response["error"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' Slide']);
            return response()->json($response);
        }

        // Start transaction!
        DB::beginTransaction();

        try {
            $deleteItem->delete();
            $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_removed'), ['slide']);
           
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }

        DB::commit();

        return response()->json($response);
    }
    // ===================apiRemoveSlide=================
    protected function apiAddSlide(Request $request){ 

        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";
        // // check ton tai to delete
        $currentSLide = new SlideDetail;

        $currentSLide->fill($request->all());

        $currentSLide->created_user = Session::get('userAuth')->user_id; 
        $currentSLide->created_date = $this->commonCtl->getCarbonNow();
        $currentSLide->updated_user = Session::get('userAuth')->user_id; 
        $currentSLide->updated_date = $this->commonCtl->getCarbonNow();
        $currentSLide->status = 1;

        // Start transaction!
        DB::beginTransaction();

        try {
            $currentSLide->save();

            $response["success"] = "Thêm Dữ Liệu Thành Công";
           
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }

        DB::commit();
 
        return response()->json($response);
    }
    // apiLoadSlide
    protected function apiLoadSlide(Request   $request){  

        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $whereFunctions = array();

        $listItem = DB::table('slide_detail AS sl')
                ->select( 
                        'sl.slide_id',
                        'sl.slide_name',
                        'sl.slide_url',
                        'sl.slide_img',
                        'sl.created_date',
                        'sl.created_user',
                        'sl.updated_date',
                        'sl.updated_user',
                        'sl.status'
                         )
                ->where('sl.status','<>',0)
                ->get();

        if(!$listItem->isEmpty())
        {
            $response["success"] = $listItem;
        }
        else
        {
            $response["warning"] = Lang::get('messages.common_warning_empty_list');
        }

        return response()->json($response);
    }
}
