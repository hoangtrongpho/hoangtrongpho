<?php

namespace App\Http\Controllers\SystemApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use DateTime;
use Validator;
use Mail;
use Lang;

use App\Http\Controllers\SystemApi\SystemCommonController as commonCtl;
use App\Models\NewsTagsDetail;
use App\Models\NewsTagsList;

class SystemApiNewsTagsController extends Controller
{

    /*** Khởi Tạo Giá Trị ***/
    private $commonCtl;

    private $sessionUser;

    private $rulesMess = [
        'tag_id.required' => 'Vui Lòng Thêm Mã Thẻ Bài Viết.',
        'tag_name.required' => 'Vui Lòng Thêm Tên Thẻ Bài Viết.',
    ];

    /*** Khởi Tạo Giá Trị ***/
    public function __construct(commonCtl $SystemCommonController){
        $this->commonCtl =  $SystemCommonController;
    }

    //Ajax Post Lấy Danh Sách 
    protected function apiGetList(Request $request){  
        
        $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        if($auth["auth"])
        {
            return response()->json($auth);
        }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $listNewsTags = DB::table('news_tags_detail AS td')
                ->leftJoin('news_tags_list AS tl', 'td.tag_id', '=', 'tl.news_tag_id')
                ->select('td.tag_id', 
                         'td.tag_slug', 
                         'td.tag_name', 
                         'td.updated_date', 
                         'td.status',
                         DB::raw('count(tl.news_tag_id) as news_couter'))
                ->groupBy('td.tag_id', 
                         'td.tag_slug', 
                         'td.tag_name', 
                         'td.updated_date', 
                         'td.status')
                ->get();

        if(!$listNewsTags->isEmpty())
        {
            $response["success"] = $listNewsTags;
        }
        else
        {
            $response["warning"] = Lang::get('messages.common_warning_empty_list');
        }

        return response()->json($response);
    } 

    //Ajax Post Thêm
    protected function apiAdd(Request $request){  
        
        $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        if($auth["auth"])
        {
            return response()->json($auth);
        }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $this->sessionUser =  Session::get('userAuth');

        $rules = [
            'tag_name' => 'required',
        ];

        $valiData = $this->commonCtl->validRequest($request,$rules,$this->rulesMess);

        if($valiData->fails())
        {   
            $response["warning"] = $valiData->errors();
            return response()->json($response);
        }

        $tag_slug = $this->commonCtl->slugify($request->input('tag_name'));

        if($tag_slug === null)
        {
            $response["error"] = Lang::get('messages.system_common_error_exception');
            return response()->json($response);
        }

        $itemNewsTags = new NewsTagsDetail;

        $itemNewsTags->fill($request->all());

        $itemNewsTags->tag_slug = $tag_slug;
        $itemNewsTags->created_user = $this->sessionUser->user_id;
        $itemNewsTags->created_date = $this->commonCtl->getCarbonNow();
        $itemNewsTags->updated_user = $this->sessionUser->user_id;
        $itemNewsTags->updated_date = $this->commonCtl->getCarbonNow();
        $itemNewsTags->status = 1;

        DB::beginTransaction();

        try {
            $itemNewsTags->save();

            $response["success"] = "Thêm Thẻ Thành Công!";
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = $e->getErrors();
            return response()->json($response);

            // return Redirect::to('/form')
            // ->withErrors( $e->getErrors() )
            // ->withInput();
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = "Có Lỗi Exception: ";
            return response()->json($response);
            // throw $e;
        }
        DB::commit();

        return response()->json($response);
    } 

    //Ajax Post Thêm
    protected function apiUpdate(Request $request){  

        $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        if($auth["auth"])
        {
            return response()->json($auth);
        }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $this->sessionUser =  Session::get('userAuth');

        $rules = [
            'tag_id' => 'required',
            'tag_name' => 'required',
        ];

        $valiData = $this->commonCtl->validRequest($request,$rules,$this->rulesMess);

        if($valiData->fails())
        {   
            $response["warning"] = $valiData->errors();
            return response()->json($response);
        }

        $tag_slug = $this->commonCtl->slugify($request->input('tag_name'));

        if($tag_slug === null)
        {
            $response["error"] = Lang::get('messages.system_common_error_exception');
            return response()->json($response);
        }

        $itemNewsTags = NewsTagsDetail::where('tag_id','=',$request->input('tag_id'))
                        ->first();

        $itemNewsTags->tag_name = $request->input('tag_name');
        $itemNewsTags->tag_slug = $tag_slug;
        $itemNewsTags->updated_user = $this->sessionUser->user_id;
        $itemNewsTags->updated_date = $this->commonCtl->getCarbonNow();

        DB::beginTransaction();
        try {
            $itemNewsTags->save();
            $response["success"] = "Cập Nhật Thẻ Thành Công!";
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = $e->getErrors();
            return response()->json($response);

            // return Redirect::to('/form')
            // ->withErrors( $e->getErrors() )
            // ->withInput();
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = "Có Lỗi Exception: ";
            return response()->json($response);
            // throw $e;
        }

        DB::commit();

        return response()->json($response);
    } 

    //Ajax Post Xóa Thẻ
    protected function apiRemove(Request $request){ 

        $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        if($auth["auth"])
        {
            return response()->json($auth);
        }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $rules = [
            'tag_id' => 'required',
        ];

        $valiData = $this->commonCtl->validRequest($request,$rules,$this->rulesMess);

        if($valiData->fails())
        {   
            $response["warning"] = $valiData->errors();
            return response()->json($response);
        }

        $itemDelete = NewsTagsDetail::where('tag_id', '=', $request->input('tag_id'))->first();

        if($itemDelete == null)
        {
            $response["warning"] = [ "Lỗi Dữ Liệu" => "Không Tìm Thấy Dữ Liệu, Hoặc Dữ Liệu Đã Bị Xóa Từ Trước."];
            return response()->json($response);
        }

        $checkNewsExists = NewsTagsList::where('news_tag_id', '=', $request->input('tag_id'))->get();

        if(!$checkNewsExists->isEmpty())
        {
            $response["warning"] = [ "Lỗi Dữ Liệu" => "Thẻ này đã được sử dụng cho bài viết, vui lòng kiểm tra lại."];
            return response()->json($response);
        }

        DB::beginTransaction();
        try {

            $deletedRowsTagsList = NewsTagsList::where('news_tag_id', '=', $itemDelete->tag_id)->delete();

            $deletedRowsTag = NewsTagsDetail::where('tag_id', '=', $itemDelete->tag_id)->delete();

            if($deletedRowsTag > 0){
                $response["success"] = "Xóa Thành Công!";
            }
            else{
                $response["warning"] = "Xóa Không Thành Công!";
            }
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = $e->getErrors();
            return response()->json($response);
            // return Redirect::to('/form')
            // ->withErrors( $e->getErrors() )
            // ->withInput();
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = "Lỗi Exception!";
            return response()->json($response);
            // throw $e;
        }
        DB::commit();

        return response()->json($response);
    }
}
