<?php

namespace App\Http\Controllers\SystemApi;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Cookie;
use Config;
use DB;
use Lang;
use Session;
use Validator;

class SystemCommonController
{

   private $listFunction = array(
      "SystemApi/GetNumeral"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/GetRevenueByYear"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/GetDashboard"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/GetMapVector"  => [
         "roles" => [ 1, 2, 3]
      ],
      //API Thể Loại Tin Tức
      "SystemApi/GetListNewsCategories"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/GetListNewsCategoriesForCreateNews"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/AddNewsCategories"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/ChangeStatusNewsCategory"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/RemoveNewsCategory"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/UpdateNewsCategory"  => [
         "roles" => [ 1, 2, 3]
      ],
      //API Tin Tức
      "SystemApi/GetListNews"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/GetNewsDetails"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/AddNews"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/UpdateNews"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/ChangeStatusNews"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/RemoveNews"  => [
         "roles" => [ 1, 2, 3]
      ],
      //API Thẻ Tag Cho Tin Tức
      "SystemApi/GetListNewsTags"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/AddNewsTags"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/UpdateNewsTags"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/RemoveNewsTags"  => [
         "roles" => [ 1, 2, 3]
      ],
      //API Nhân Viên
      "SystemApi/apiGetListGroups"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/GetListUsers"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/GetUserDetail"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/AddUser"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/UpdateUser"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/UpdateProfile"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/ChangeStatusUser"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/RemoveUser"  => [
         "roles" => [ 1, 2, 3]
      ],


      /*** Sản Phẩm Hàng Hóa ***/
      "SystemApi/GetListProducts"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/GetProductDetails"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/AddProduct"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/UpdateProduct"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/RemoveProduct"  => [
         "roles" => [ 1, 2, 3]
      ],
      

      //Đơn Vị
      "SystemApi/GetListProductUnits"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/AddProductUnit"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/UpdateProductUnit"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/RemoveProductUnit"  => [
         "roles" => [ 1, 2, 3]
      ],

      //Màu Sắc
      "SystemApi/GetListProductColors"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/AddProductColor"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/UpdateProductColor"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/RemoveProductColor"  => [
         "roles" => [ 1, 2, 3]
      ],

      //Xuất Xứ
      "SystemApi/GetListProductOrigins"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/AddProductOrigin"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/UpdateProductOrigin"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/RemoveProductOrigin"  => [
         "roles" => [ 1, 2, 3]
      ],

      //Thẻ Tags
      "SystemApi/GetListProductTags"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/AddProductTag"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/UpdateProductTag"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/RemoveProductTag"  => [
         "roles" => [ 1, 2, 3]
      ],

      //Thẻ Tags
      "SystemApi/GetListProductTags"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/AddProductTag"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/UpdateProductTag"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/RemoveProductTag"  => [
         "roles" => [ 1, 2, 3]
      ],

      //API Khu vực
      "SystemApi/GetListAreas"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/GetListAreasForUpdateArea"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/UpdateStatusArea"  => [
         "roles" => [ 1, 2, 3]
      ],

      //Tỉnh Thành Phố
      "SystemApi/GetListProvinces"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/AddProvince"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/UpdateProvince"  => [
         "roles" => [ 1, 2, 3]
      ],
      
      //Quận Huyện
      "SystemApi/GetListDistrictsByProvince"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/AddDistrict"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/UpdateDistrict"  => [
         "roles" => [ 1, 2, 3]
      ],

      //Phường Xã
      "SystemApi/GetListWardsByDistrict"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/AddWard"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/UpdateWard"  => [
         "roles" => [ 1, 2, 3]
      ],

      //Nhà Sản Xuất
      "SystemApi/GetListProductManufacturers"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/AddProductManufacturer"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/UpdateProductManufacturer"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/RemoveProductManufacturer"  => [
         "roles" => [ 1, 2, 3]
      ],

      //Nhóm Sản Phẩm
      "SystemApi/GetListProductCategories"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/GetListProductCategoriesForChosen"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/AddProductCategory"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/UpdateProductCategory"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/RemoveProductCategory"  => [
         "roles" => [ 1, 2, 3]
      ],

      "SystemApi/GetListPages"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/GetPageDetails"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/AddPage"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/UpdatePage"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/ChangeStatusPage"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/RemovePage"  => [
         "roles" => [ 1, 2, 3]
      ],

      "SystemApi/GetListCustomers"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/GetCustomerDetail"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/AddCustomer"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/UpdateCustomer"  => [
         "roles" => [ 1, 2, 3]
      ],
      "SystemApi/RemoveCustomer"  => [
         "roles" => [ 1, 2, 3]
      ],

   );
   
   //Check token session
   public function checkTokenSession($request)
   {
      //Tạo biến lưu kết quả kiểm tra
      $result["auth"] = false;
      $result["message"] = "";

      if (strcmp($request->session()->token(), $request->header('X-CSRF-TOKEN')) !== 0)
      {  //Trường hợp không có _token
         //Không có quyền truy xuất API
         $result["auth"] = true;
         $result["message"] = Lang::get('messages.common_error_permission_api');
      }

      //Có quyền truy xuất API
      return $result;
   }
   
   //Check quyền truy xuất API
   public function checkRolesApi($request, $postback)
   {

      //Tạo biến lưu kết quả kiểm tra
      $result["auth"] = "";
      $result["redirect"] = "";
      $result["message"] = "";

      if (strcmp($request->session()->token(), $request->header('X-CSRF-TOKEN')) !== 0)
      {  //Trường hợp không có _token
         //Không có quyền truy xuất API
         $result["auth"] = true;
         $result["redirect"] = "/System/Login";
         $result["message"] = Lang::get('messages.common_error_permission_api');
      }
      else
      {  //Trường hợp có _token
         
         $sessionUser = null; 
         if(Cookie::get('userAuth') !== null)
         {
            $sessionUser = Cookie::get('userAuth');
         }
         else if($request->session()->has('userAuth'))
         {
            $sessionUser = $request->session()->get('userAuth'); 
         }

         if($sessionUser)
         {  //Trường hợp đã đăng nhập
            $sessionUser = $request->session()->get('userAuth'); 

            $userInfo = DB::table('system_users')
                      ->leftJoin('system_authorities', 'system_users.auth_id', '=', 'system_authorities.auth_id')
                      // ->leftJoin('branches_detail', 'system_users.branch_id', '=', 'branches_detail.branch_id')
                      ->select('user_id'
                              ,'user_fullname'
                              ,'user_account'
                              ,'user_avatar'
                              ,'system_authorities.auth_id'
                              ,'auth_name'
                              ,'user_avatar'
                              ,'user_sex'
                              ,'system_users.status')
                              // ,'branches_detail.branch_id'
                              // ,'branch_name')
                        ->where(function($query) use ($sessionUser)
                        {
                           $query->where('user_account', '=', $sessionUser->user_account);
                           $query->where('system_users.status', '<>', 0);
                        })
                        ->first();

            if($userInfo)
            {  //Trường hợp tài khoản hợp lệ
               Session::put('lockScreen', false);
               Session::put('userAuth', $userInfo);

               if (array_key_exists($request->path(), $this->listFunction)) 
               {  //Trường hợp chức năng tồn tại trong hệ thống
                  $roles = $this->listFunction[$request->path()]["roles"];

                  if(in_array($userInfo->auth_id, $roles))
                  {  //Trường hợp có quyền sử dụng chức năng
                     //Có quyền truy xuất API
                     $result["auth"] = false;
                  }
                  else
                  {  //Trường hợp không có quyền sử dụng chức năng
                     //Không có quyền truy xuất API
                     $result["auth"] = true;
                     $result["redirect"] = $postback;
                     $result["message"] = Lang::get('messages.common_error_permission_api');
                  }
               }
               else
               {  //Trường hợp chức năng không tồn tại trong hệ thống
                  //Không có quyền truy xuất API
                  $result["auth"] = true;
                  $result["redirect"] = $postback;
                  $result["message"] = Lang::get('messages.common_error_permission_api');
               }
            }
            else
            {  //Trường hợp tài khoản đã bị khóa hoặc tài khoản không tồn tại
               //Không có quyền truy xuất API
               $result["auth"] = true;
               $result["redirect"] = "/System/Login";
               $result["message"] = Lang::get('messages.common_error_user_not_exists');
            }
         }
         else
         {  //Trường hợp chưa đăng nhập
            //Không có quyền truy xuất API
            $result["auth"] = true;
            $result["redirect"] = "/System/Login";
            $result["message"] = Lang::get('messages.common_error_permission_api');
         }
      }

      return $result;
   }

   //Check quyền Truy Cập Của Người Dùng
   public function checkUserAuth($request)
   {
      $sessionUser = null; 
      if(Cookie::get('userAuth') !== null)
      {
         $sessionUser = Cookie::get('userAuth');
      }
      else if($request->session()->has('userAuth'))
      {
         $sessionUser = $request->session()->get('userAuth'); 
      }

      if($sessionUser)
      {
         $userInfo =DB::table('system_users')
                   ->leftJoin('system_authorities', 'system_users.auth_id', '=', 'system_authorities.auth_id')
                   // ->leftJoin('branches_detail', 'system_users.branch_id', '=', 'branches_detail.branch_id')
                   ->select('user_id'
                           ,'user_fullname'
                           ,'user_account'
                           ,'user_avatar'
                           ,'system_authorities.auth_id'
                           ,'auth_name'
                           ,'user_avatar'
                           ,'user_sex'
                           ,'system_users.status')
                           // ,'branches_detail.branch_id'
                           // ,'branch_name')
                     ->where(function($query) use ($sessionUser)
                     {
                        $query->where('user_account', '=', $sessionUser->user_account);
                        $query->where('system_users.status', '<>', 0);
                     })
                     ->first();

         if($userInfo != null)
         {
            Session::put('lockScreen', false);
            Session::put('userAuth', $userInfo);

            // Cookie::make('userAuth', $userInfo);

            return $userInfo;
         }
         else
         {
            return null;
         }
      }
      return null;
   }

   public function replaceTitle($strMessage, $arrParameter) 
   {

		$result = "";

		if(empty($strMessage))
		{
			return $result;
		}

		if (count($arrParameter) === 0) 
		{
			$count = substr_count($strMessage, "$"); 

			for($i = 0; $i < $count; $i++)
			{
				$result = str_replace("$".$i,"",$strMessage);
			}
		}
		else
		{
			$i = 0;
         $result = $strMessage;
			foreach ($arrParameter as $value) 
			{
				if(empty($value))
				{
					$result = str_replace("$".$i,"",$result);
				}
				else
				{
					$result = str_replace("$".$i,$value,$result);
				}

				$i++;
			}
		}
		return $result;
   }

   public function validRequest(Request $request, $customRules, $rulesMess)
   {
		$rule = $customRules;

		if (empty($customRules)) {
			return null;
		}

		return Validator::make($request->all(), $rule, $rulesMess);
   }


   public function formatCarbonDatetime($datetime)
	{   
      $carbonDate = Carbon::now();

      if($datetime !== "" && $datetime !== null)
      {
            try { 
                  $carbonDate = new Carbon($datetime);
            } 
            catch(InvalidArgumentException $x) 
            { 
                  return null;
            }
      }
      else
      {
            return null;
      }
  
      $carbonDate->timezone = Config::get('app.timezone');

      return $carbonDate;
   }

    public function formatCarbonDate($datetime)
   {   
      $carbonDate = Carbon::now();

      if($datetime !== "" && $datetime !== null)
      {
            try 
            { 
                  $carbonDate = new Carbon($datetime);

                  $carbonDate->timezone = Config::get('app.timezone');

                  return $carbonDate->toDateString();
            } 
            catch(InvalidArgumentException $x) 
            { 
                  return null;
            }
      }

      return null;
   }

   public function formatCarbonDatetime_From($datetime)
   {   
      $carbonDate = Carbon::now();

      if($datetime !== "" && $datetime !== null)
      {
            try 
            { 
                  $tempDate = new Carbon($datetime);

                  $carbonDate =  Carbon::create($tempDate->year, $tempDate->month, $tempDate->day, 0, 0, 0);
            } 
            catch(InvalidArgumentException $x) 
            { 
                  return null;
            }
      }
      else
      {
            return null;
      }
  
      $carbonDate->timezone = Config::get('app.timezone');

      return $carbonDate;
   }

   public function formatCarbonDatetime_To($datetime)
   {   
      $carbonDate = Carbon::now();

      if($datetime !== "" && $datetime !== null)
      {
            try 
            { 
                  $tempDate = new Carbon($datetime);

                  $carbonDate =  Carbon::create($tempDate->year, $tempDate->month, $tempDate->day, 23, 59, 59);
            } 
            catch(InvalidArgumentException $x) 
            { 
                  return null;
            }
      }
      else
      {
            return null;
      }
  
      $carbonDate->timezone = Config::get('app.timezone');

      return $carbonDate;
   }

   public function formatCarbonDatetime_ParseDay($datetime)
   {   
      $carbonDate = Carbon::now();

      if($datetime !== "" && $datetime !== null)
      {
            try 
            { 
                  $carbonDate = new Carbon($datetime);

                  $carbonDate->timezone = Config::get('app.timezone');

                  return $carbonDate->day;
            } 
            catch(InvalidArgumentException $x) 
            { 
                  return null;
            }
      }

      return null;
   }

   public function formatCarbonDatetime_ParseMonth($datetime)
   {   
      $carbonDate = Carbon::now();

      if($datetime !== "" && $datetime !== null)
      {
            try 
            { 
                  $carbonDate = new Carbon($datetime);

                  $carbonDate->timezone = Config::get('app.timezone');

                  return $carbonDate->month;
            } 
            catch(InvalidArgumentException $x) 
            { 
                  return null;
            }
      }

      return null;
   }

   public function formatCarbonDatetime_ParseYear($datetime)
   {   
     $carbonDate = Carbon::now();

      if($datetime !== "" && $datetime !== null)
      {
            try 
            { 
                  $carbonDate = new Carbon($datetime);

                  $carbonDate->timezone = Config::get('app.timezone');

                  return $carbonDate->year;
            } 
            catch(InvalidArgumentException $x) 
            { 
                  return null;
            }
      }

      return null;
   }

   public function formatCarbonDatetime_ParseHour($datetime)
   {   
      $carbonDate = Carbon::now();

      if($datetime !== "" && $datetime !== null)
      {
            try 
            { 
                  $carbonDate = new Carbon($datetime);

                  $carbonDate->timezone = Config::get('app.timezone');

                  return $carbonDate->hour;
            } 
            catch(InvalidArgumentException $x) 
            { 
                  return null;
            }
      }

      return null;
   }

   public function formatCarbonDatetime_ParseMinute($datetime)
   {   
      $carbonDate = Carbon::now();

      if($datetime !== "" && $datetime !== null)
      {
            try 
            { 
                  $carbonDate = new Carbon($datetime);

                  $carbonDate->timezone = Config::get('app.timezone');

                  return $carbonDate->minute;
            } 
            catch(InvalidArgumentException $x) 
            { 
                  return null;
            }
      }

      return null;
   }

   public function formatCarbonDatetime_ParseSecond($datetime)
   {   
         $carbonDate = Carbon::now();

         if($datetime !== "" && $datetime !== null)
         {
               try 
               { 
                     $carbonDate = new Carbon($datetime);

                     $carbonDate->timezone = Config::get('app.timezone');

                     return $carbonDate->second;
               } 
               catch(InvalidArgumentException $x) 
               { 
                     return null;
               }
         }

         return null;
   }

   public function formatCarbonDatetime_ParseDayOfWeek($datetime)
   {   
         $carbonDate = Carbon::now();

         if($datetime !== "" && $datetime !== null)
         {
               try 
               { 
                     $carbonDate = new Carbon($datetime);

                     $carbonDate->timezone = Config::get('app.timezone');

                     return $carbonDate->dayOfWeek;
               } 
               catch(InvalidArgumentException $x) 
               { 
                     return null;
               }
         }

         return null;
   }

   public function getCarbonNow()
   {   
      $carbonDate = Carbon::now();

      $carbonDate->timezone = Config::get('app.timezone');

      return $carbonDate;
   }

   /**
   * Create a web friendly URL slug from a string.
   * 
   * Although supported, transliteration is discouraged because
   *     1) most web browsers support UTF-8 characters in URLs
   *     2) transliteration causes a loss of information
   *
   * @param string $str
   * @param array $options
   * @return string
   */
   function slugify($str) {

     if(empty($str))
     {
         return null;
     }

     // Make sure string is in UTF-8 and strip invalid UTF-8 characters
     $str = mb_convert_encoding((string)$str, 'UTF-8', mb_list_encodings());
     
     $options = array(
         'delimiter' => '-',
         'limit' => null,
         'lowercase' => true,
         'replacements' => array(),
         'transliterate' => true,
     );
     
     $char_map = array(
         //arabic
         'أ' => 'a',
         'ب' => 'b',
         'ت' => 't',
         'ث' => 'th',
         'ج' => 'g',
         'ح' => 'h',
         'خ' => 'kh',
         'د' => 'd',
         'ذ' => 'th',
         'ر' => 'r',
         'ز' => 'z',
         'س' => 's',
         'ش' => 'sh',
         'ص' => 's',
         'ض' => 'd',
         'ط' => 't',
         'ظ' => 'th',
         'ع' => 'aa',
         'غ' => 'gh',
         'ف' => 'f',
         'ق' => 'k',
         'ك' => 'k',
         'ل' => 'l',
         'م' => 'm',
         'ن' => 'n',
         'ه' => 'h',
         'و' => 'o',
         'ي' => 'y',
         //austrian
         'Ä' => 'AE',
         'Ö' => 'OE',
         'Ü' => 'UE',
         'ß' => 'sz',
         'ä' => 'ae',
         'ö' => 'oe',
         'ü' => 'ue',
         //azerbaijani
         'Ə' => 'E',
         'Ç' => 'C',
         'Ğ' => 'G',
         'İ' => 'I',
         'Ş' => 'S',
         'Ö' => 'O',
         'Ü' => 'U',
         'ə' => 'e',
         'ç' => 'c',
         'ğ' => 'g',
         'ı' => 'i',
         'ş' => 's',
         'ö' => 'o',
         'ü' => 'u',
         //burmese
         'က' => 'k',
         'ခ' => 'kh',
         'ဂ' => 'g',
         'ဃ' => 'ga',
         'င' => 'ng',
         'စ' => 's',
         'ဆ' => 'sa',
         'ဇ' => 'z',
         'စျ' => 'za',
         'ည' => 'ny',
         'ဋ' => 't',
         'ဌ' => 'ta',
         'ဍ' => 'd',
         'ဎ' => 'da',
         'ဏ' => 'na',
         'တ' => 't',
         'ထ' => 'ta',
         'ဒ' => 'd',
         'ဓ' => 'da',
         'န' => 'n',
         'ပ' => 'p',
         'ဖ' => 'pa',
         'ဗ' => 'b',
         'ဘ' => 'ba',
         'မ' => 'm',
         'ယ' => 'y',
         'ရ' => 'ya',
         'လ' => 'l',
         'ဝ' => 'w',
         'သ' => 'th',
         'ဟ' => 'h',
         'ဠ' => 'la',
         'အ' => 'a',
         'ြ' => 'y',
         'ျ' => 'ya',
         'ွ' => 'w',
         'ြွ' => 'yw',
         'ျွ' => 'ywa',
         'ှ' => 'h',
         'ဧ' => 'e',
         '၏' => '-e',
         'ဣ' => 'i',
         'ဤ' => '-i',
         'ဉ' => 'u',
         'ဦ' => '-u',
         'ဩ' => 'aw',
         'သြော' => 'aw',
         'ဪ' => 'aw',
         '၍' => 'ywae',
         '၌' => 'hnaik',
         '၀' => '0',
         '၁' => '1',
         '၂' => '2',
         '၃' => '3',
         '၄' => '4',
         '၅' => '5',
         '၆' => '6',
         '၇' => '7',
         '၈' => '8',
         '၉' => '9',
         '္' => '',
         '့' => '',
         'း' => '',
         'ာ' => 'a',
         'ါ' => 'a',
         'ေ' => 'e',
         'ဲ' => 'e',
         'ိ' => 'i',
         'ီ' => 'i',
         'ို' => 'o',
         'ု' => 'u',
         'ူ' => 'u',
         'ေါင်' => 'aung',
         'ော' => 'aw',
         'ော်' => 'aw',
         'ေါ' => 'aw',
         'ေါ်' => 'aw',
         '်' => 'at',
         'က်' => 'et',
         'ိုက်' => 'aik',
         'ောက်' => 'auk',
         'င်' => 'in',
         'ိုင်' => 'aing',
         'ောင်' => 'aung',
         'စ်' => 'it',
         'ည်' => 'i',
         'တ်' => 'at',
         'ိတ်' => 'eik',
         'ုတ်' => 'ok',
         'ွတ်' => 'ut',
         'ေတ်' => 'it',
         'ဒ်' => 'd',
         'ိုဒ်' => 'ok',
         'ုဒ်' => 'ait',
         'န်' => 'an',
         'ာန်' => 'an',
         'ိန်' => 'ein',
         'ုန်' => 'on',
         'ွန်' => 'un',
         'ပ်' => 'at',
         'ိပ်' => 'eik',
         'ုပ်' => 'ok',
         'ွပ်' => 'ut',
         'န်ုပ်' => 'nub',
         'မ်' => 'an',
         'ိမ်' => 'ein',
         'ုမ်' => 'on',
         'ွမ်' => 'un',
         'ယ်' => 'e',
         'ိုလ်' => 'ol',
         'ဉ်' => 'in',
         'ံ' => 'an',
         'ိံ' => 'ein',
         'ုံ' => 'on',
         //czech
         'Č' => 'C',
         'Ď' => 'D',
         'Ě' => 'E',
         'Ň' => 'N',
         'Ř' => 'R',
         'Š' => 'S',
         'Ť' => 'T',
         'Ů' => 'U',
         'Ž' => 'Z',
         'č' => 'c',
         'ď' => 'd',
         'ě' => 'e',
         'ň' => 'n',
         'ř' => 'r',
         'š' => 's',
         'ť' => 't',
         'ů' => 'u',
         'ž' => 'z',
         //default
         '0' => '0',
         '1' => '1',
         '2' => '2',
         '3' => '3',
         '4' => '4',
         '5' => '5',
         '6' => '6',
         '7' => '7',
         '8' => '8',
         '9' => '9',
         '°' => '0',
         '¹' => '1',
         '²' => '2',
         '³' => '3',
         '⁴' => '4',
         '⁵' => '5',
         '⁶' => '6',
         '⁷' => '7',
         '⁸' => '8',
         '⁹' => '9',
         '₀' => '0',
         '₁' => '1',
         '₂' => '2',
         '₃' => '3',
         '₄' => '4',
         '₅' => '5',
         '₆' => '6',
         '₇' => '7',
         '₈' => '8',
         '₉' => '9',
         'æ' => 'ae',
         'ǽ' => 'ae',
         'À' => 'A',
         'Á' => 'A',
         'Â' => 'A',
         'Ã' => 'A',
         'Å' => 'AA',
         'Ǻ' => 'A',
         'Ă' => 'A',
         'Ǎ' => 'A',
         'Æ' => 'AE',
         'Ǽ' => 'AE',
         'à' => 'a',
         'á' => 'a',
         'â' => 'a',
         'ã' => 'a',
         'å' => 'aa',
         'ǻ' => 'a',
         'ă' => 'a',
         'ǎ' => 'a',
         'ª' => 'a',
         '@' => 'at',
         'Ĉ' => 'C',
         'Ċ' => 'C',
         'ĉ' => 'c',
         'ċ' => 'c',
         '©' => 'c',
         'Ð' => 'Dj',
         'Đ' => 'D',
         'ð' => 'dj',
         'đ' => 'd',
         'È' => 'E',
         'É' => 'E',
         'Ê' => 'E',
         'Ë' => 'E',
         'Ĕ' => 'E',
         'Ė' => 'E',
         'è' => 'e',
         'é' => 'e',
         'ê' => 'e',
         'ë' => 'e',
         'ĕ' => 'e',
         'ė' => 'e',
         'ƒ' => 'f',
         'Ĝ' => 'G',
         'Ġ' => 'G',
         'ĝ' => 'g',
         'ġ' => 'g',
         'Ĥ' => 'H',
         'Ħ' => 'H',
         'ĥ' => 'h',
         'ħ' => 'h',
         'Ì' => 'I',
         'Í' => 'I',
         'Î' => 'I',
         'Ï' => 'I',
         'Ĩ' => 'I',
         'Ĭ' => 'I',
         'Ǐ' => 'I',
         'Į' => 'I',
         'Ĳ' => 'IJ',
         'ì' => 'i',
         'í' => 'i',
         'î' => 'i',
         'ï' => 'i',
         'ĩ' => 'i',
         'ĭ' => 'i',
         'ǐ' => 'i',
         'į' => 'i',
         'ĳ' => 'ij',
         'Ĵ' => 'J',
         'ĵ' => 'j',
         'Ĺ' => 'L',
         'Ľ' => 'L',
         'Ŀ' => 'L',
         'ĺ' => 'l',
         'ľ' => 'l',
         'ŀ' => 'l',
         'Ñ' => 'N',
         'ñ' => 'n',
         'ŉ' => 'n',
         'Ò' => 'O',
         'Ó' => 'O',
         'Ô' => 'O',
         'Õ' => 'O',
         'Ō' => 'O',
         'Ŏ' => 'O',
         'Ǒ' => 'O',
         'Ő' => 'O',
         'Ơ' => 'O',
         'Ø' => 'OE',
         'Ǿ' => 'O',
         'Œ' => 'OE',
         'ò' => 'o',
         'ó' => 'o',
         'ô' => 'o',
         'õ' => 'o',
         'ō' => 'o',
         'ŏ' => 'o',
         'ǒ' => 'o',
         'ő' => 'o',
         'ơ' => 'o',
         'ø' => 'oe',
         'ǿ' => 'o',
         'º' => 'o',
         'œ' => 'oe',
         'Ŕ' => 'R',
         'Ŗ' => 'R',
         'ŕ' => 'r',
         'ŗ' => 'r',
         'Ŝ' => 'S',
         'Ș' => 'S',
         'ŝ' => 's',
         'ș' => 's',
         'ſ' => 's',
         'Ţ' => 'T',
         'Ț' => 'T',
         'Ŧ' => 'T',
         'Þ' => 'TH',
         'ţ' => 't',
         'ț' => 't',
         'ŧ' => 't',
         'þ' => 'th',
         'Ù' => 'U',
         'Ú' => 'U',
         'Û' => 'U',
         'Ü' => 'U',
         'Ũ' => 'U',
         'Ŭ' => 'U',
         'Ű' => 'U',
         'Ų' => 'U',
         'Ư' => 'U',
         'Ǔ' => 'U',
         'Ǖ' => 'U',
         'Ǘ' => 'U',
         'Ǚ' => 'U',
         'Ǜ' => 'U',
         'ù' => 'u',
         'ú' => 'u',
         'û' => 'u',
         'ü' => 'u',
         'ũ' => 'u',
         'ŭ' => 'u',
         'ű' => 'u',
         'ų' => 'u',
         'ư' => 'u',
         'ǔ' => 'u',
         'ǖ' => 'u',
         'ǘ' => 'u',
         'ǚ' => 'u',
         'ǜ' => 'u',
         'Ŵ' => 'W',
         'ŵ' => 'w',
         'Ý' => 'Y',
         'Ÿ' => 'Y',
         'Ŷ' => 'Y',
         'ý' => 'y',
         'ÿ' => 'y',
         'ŷ' => 'y',
         //esperanto
         'ĉ' => 'cx',
         'ĝ' => 'gx',
         'ĥ' => 'hx',
         'ĵ' => 'jx',
         'ŝ' => 'sx',
         'ŭ' => 'ux',
         'Ĉ' => 'CX',
         'Ĝ' => 'GX',
         'Ĥ' => 'HX',
         'Ĵ' => 'JX',
         'Ŝ' => 'SX',
         'Ŭ' => 'UX',
         //finnish
         'Ä' => 'A',
         'Ö' => 'O',
         'ä' => 'a',
         'ö' => 'o',
         //georgian
         'ა' => 'a',
         'ბ' => 'b',
         'გ' => 'g',
         'დ' => 'd',
         'ე' => 'e',
         'ვ' => 'v',
         'ზ' => 'z',
         'თ' => 't',
         'ი' => 'i',
         'კ' => 'k',
         'ლ' => 'l',
         'მ' => 'm',
         'ნ' => 'n',
         'ო' => 'o',
         'პ' => 'p',
         'ჟ' => 'zh',
         'რ' => 'r',
         'ს' => 's',
         'ტ' => 't',
         'უ' => 'u',
         'ფ' => 'f',
         'ქ' => 'k',
         'ღ' => 'gh',
         'ყ' => 'q',
         'შ' => 'sh',
         'ჩ' => 'ch',
         'ც' => 'ts',
         'ძ' => 'dz',
         'წ' => 'ts',
         'ჭ' => 'ch',
         'ხ' => 'kh',
         'ჯ' => 'j',
         'ჰ' => 'h',
         //german
         'Ä' => 'AE',
         'Ö' => 'OE',
         'Ü' => 'UE',
         'ß' => 'ss',
         'ä' => 'ae',
         'ö' => 'oe',
         'ü' => 'ue',
         //greek
         'ΑΥ' => 'AU',
         'Αυ' => 'Au',
         'ΟΥ' => 'OU',
         'Ου' => 'Ou',
         'ΕΥ' => 'EU',
         'Ευ' => 'Eu',
         'ΕΙ' => 'I',
         'Ει' => 'I',
         'ΟΙ' => 'I',
         'Οι' => 'I',
         'ΥΙ' => 'I',
         'Υι' => 'I',
         'ΑΎ' => 'AU',
         'Αύ' => 'Au',
         'ΟΎ' => 'OU',
         'Ού' => 'Ou',
         'ΕΎ' => 'EU',
         'Εύ' => 'Eu',
         'ΕΊ' => 'I',
         'Εί' => 'I',
         'ΟΊ' => 'I',
         'Οί' => 'I',
         'ΎΙ' => 'I',
         'Ύι' => 'I',
         'ΥΊ' => 'I',
         'Υί' => 'I',
         'αυ' => 'au',
         'ου' => 'ou',
         'ευ' => 'eu',
         'ει' => 'i',
         'οι' => 'i',
         'υι' => 'i',
         'αύ' => 'au',
         'ού' => 'ou',
         'εύ' => 'eu',
         'εί' => 'i',
         'οί' => 'i',
         'ύι' => 'i',
         'υί' => 'i',
         'Α' => 'A',
         'Β' => 'V',
         'Γ' => 'G',
         'Δ' => 'D',
         'Ε' => 'E',
         'Ζ' => 'Z',
         'Η' => 'I',
         'Θ' => 'Th',
         'Ι' => 'I',
         'Κ' => 'K',
         'Λ' => 'L',
         'Μ' => 'M',
         'Ν' => 'N',
         'Ξ' => 'X',
         'Ο' => 'O',
         'Π' => 'P',
         'Ρ' => 'R',
         'Σ' => 'S',
         'Τ' => 'T',
         'Υ' => 'I',
         'Φ' => 'F',
         'Χ' => 'Ch',
         'Ψ' => 'Ps',
         'Ω' => 'O',
         'Ά' => 'A',
         'Έ' => 'E',
         'Ή' => 'I',
         'Ί' => 'I',
         'Ό' => 'O',
         'Ύ' => 'I',
         'Ϊ' => 'I',
         'Ϋ' => 'I',
         'ϒ' => 'I',
         'α' => 'a',
         'β' => 'v',
         'γ' => 'g',
         'δ' => 'd',
         'ε' => 'e',
         'ζ' => 'z',
         'η' => 'i',
         'θ' => 'th',
         'ι' => 'i',
         'κ' => 'k',
         'λ' => 'l',
         'μ' => 'm',
         'ν' => 'n',
         'ξ' => 'x',
         'ο' => 'o',
         'π' => 'p',
         'ρ' => 'r',
         'ς' => 's',
         'σ' => 's',
         'τ' => 't',
         'υ' => 'i',
         'φ' => 'f',
         'χ' => 'ch',
         'ψ' => 'ps',
         'ω' => 'o',
         'ά' => 'a',
         'έ' => 'e',
         'ή' => 'i',
         'ί' => 'i',
         'ό' => 'o',
         'ύ' => 'i',
         'ϊ' => 'i',
         'ϋ' => 'i',
         'ΰ' => 'i',
         'ώ' => 'o',
         'ϐ' => 'v',
         'ϑ' => 'th',
         //hindi
         'अ' => 'a',
         'आ' => 'aa',
         'ए' => 'e',
         'ई' => 'ii',
         'ऍ' => 'ei',
         'ऎ' => 'ऎ',
         'ऐ' => 'ai',
         'इ' => 'i',
         'ओ' => 'o',
         'ऑ' => 'oi',
         'ऒ' => 'oii',
         'ऊ' => 'uu',
         'औ' => 'ou',
         'उ' => 'u',
         'ब' => 'B',
         'भ' => 'Bha',
         'च' => 'Ca',
         'छ' => 'Chha',
         'ड' => 'Da',
         'ढ' => 'Dha',
         'फ' => 'Fa',
         'फ़' => 'Fi',
         'ग' => 'Ga',
         'घ' => 'Gha',
         'ग़' => 'Ghi',
         'ह' => 'Ha',
         'ज' => 'Ja',
         'झ' => 'Jha',
         'क' => 'Ka',
         'ख' => 'Kha',
         'ख़' => 'Khi',
         'ल' => 'L',
         'ळ' => 'Li',
         'ऌ' => 'Li',
         'ऴ' => 'Lii',
         'ॡ' => 'Lii',
         'म' => 'Ma',
         'न' => 'Na',
         'ङ' => 'Na',
         'ञ' => 'Nia',
         'ण' => 'Nae',
         'ऩ' => 'Ni',
         'ॐ' => 'oms',
         'प' => 'Pa',
         'क़' => 'Qi',
         'र' => 'Ra',
         'ऋ' => 'Ri',
         'ॠ' => 'Ri',
         'ऱ' => 'Ri',
         'स' => 'Sa',
         'श' => 'Sha',
         'ष' => 'Shha',
         'ट' => 'Ta',
         'त' => 'Ta',
         'ठ' => 'Tha',
         'द' => 'Tha',
         'थ' => 'Tha',
         'ध' => 'Thha',
         'ड़' => 'ugDha',
         'ढ़' => 'ugDhha',
         'व' => 'Va',
         'य' => 'Ya',
         'य़' => 'Yi',
         'ज़' => 'Za',
         //latvian
         'Ā' => 'A',
         'Ē' => 'E',
         'Ģ' => 'G',
         'Ī' => 'I',
         'Ķ' => 'K',
         'Ļ' => 'L',
         'Ņ' => 'N',
         'Ū' => 'U',
         'ā' => 'a',
         'ē' => 'e',
         'ģ' => 'g',
         'ī' => 'i',
         'ķ' => 'k',
         'ļ' => 'l',
         'ņ' => 'n',
         'ū' => 'u',
         //norwegian
         'Æ' => 'AE',
         'Ø' => 'OE',
         'Å' => 'AA',
         'æ' => 'ae',
         'ø' => 'oe',
         'å' => 'aa',
         //polish
         'Ą' => 'A',
         'Ć' => 'C',
         'Ę' => 'E',
         'Ł' => 'L',
         'Ń' => 'N',
         'Ó' => 'O',
         'Ś' => 'S',
         'Ź' => 'Z',
         'Ż' => 'Z',
         'ą' => 'a',
         'ć' => 'c',
         'ę' => 'e',
         'ł' => 'l',
         'ń' => 'n',
         'ó' => 'o',
         'ś' => 's',
         'ź' => 'z',
         'ż' => 'z',
         //russian
         'Ъ' => '',
         'Ь' => '',
         'А' => 'A',
         'Б' => 'B',
         'Ц' => 'C',
         'Ч' => 'Ch',
         'Д' => 'D',
         'Е' => 'E',
         'Ё' => 'E',
         'Э' => 'E',
         'Ф' => 'F',
         'Г' => 'G',
         'Х' => 'H',
         'И' => 'I',
         'Й' => 'Y',
         'Я' => 'Ya',
         'Ю' => 'Yu',
         'К' => 'K',
         'Л' => 'L',
         'М' => 'M',
         'Н' => 'N',
         'О' => 'O',
         'П' => 'P',
         'Р' => 'R',
         'С' => 'S',
         'Ш' => 'Sh',
         'Щ' => 'Shch',
         'Т' => 'T',
         'У' => 'U',
         'В' => 'V',
         'Ы' => 'Y',
         'З' => 'Z',
         'Ж' => 'Zh',
         'ъ' => '',
         'ь' => '',
         'а' => 'a',
         'б' => 'b',
         'ц' => 'c',
         'ч' => 'ch',
         'д' => 'd',
         'е' => 'e',
         'ё' => 'e',
         'э' => 'e',
         'ф' => 'f',
         'г' => 'g',
         'х' => 'h',
         'и' => 'i',
         'й' => 'y',
         'я' => 'ya',
         'ю' => 'yu',
         'к' => 'k',
         'л' => 'l',
         'м' => 'm',
         'н' => 'n',
         'о' => 'o',
         'п' => 'p',
         'р' => 'r',
         'с' => 's',
         'ш' => 'sh',
         'щ' => 'shch',
         'т' => 't',
         'у' => 'u',
         'в' => 'v',
         'ы' => 'y',
         'з' => 'z',
         'ж' => 'zh',
         //swedish
         'Ä' => 'A',
         'Å' => 'a',
         'Ö' => 'O',
         'ä' => 'a',
         'å' => 'a',
         'ö' => 'o',
         //turkish
         'Ç' => 'C',
         'Ğ' => 'G',
         'İ' => 'I',
         'Ş' => 'S',
         'Ö' => 'O',
         'Ü' => 'U',
         'ç' => 'c',
         'ğ' => 'g',
         'ı' => 'i',
         'ş' => 's',
         'ö' => 'o',
         'ü' => 'u',
         //ukrainian
         'Ґ' => 'G',
         'І' => 'I',
         'Ї' => 'Ji',
         'Є' => 'Ye',
         'ґ' => 'g',
         'і' => 'i',
         'ї' => 'ji',
         'є' => 'ye',
         //vietnamese
         'ạ' => 'a',
         'ả' => 'a',
         'ầ' => 'a',
         'ấ' => 'a',
         'ậ' => 'a',
         'ẩ' => 'a',
         'ẫ' => 'a',
         'ằ' => 'a',
         'ắ' => 'a',
         'ặ' => 'a',
         'ẳ' => 'a',
         'ẵ' => 'a',
         'ẹ' => 'e',
         'ẻ' => 'e',
         'ẽ' => 'e',
         'ề' => 'e',
         'ế' => 'e',
         'ệ' => 'e',
         'ể' => 'e',
         'ễ' => 'e',
         'ị' => 'i',
         'ỉ' => 'i',
         'ọ' => 'o',
         'ỏ' => 'o',
         'ồ' => 'o',
         'ố' => 'o',
         'ộ' => 'o',
         'ổ' => 'o',
         'ỗ' => 'o',
         'ờ' => 'o',
         'ớ' => 'o',
         'ợ' => 'o',
         'ở' => 'o',
         'ỡ' => 'o',
         'ụ' => 'u',
         'ủ' => 'u',
         'ừ' => 'u',
         'ứ' => 'u',
         'ự' => 'u',
         'ử' => 'u',
         'ữ' => 'u',
         'ỳ' => 'y',
         'ỵ' => 'y',
         'ỷ' => 'y',
         'ỹ' => 'y',
         'Ạ' => 'A',
         'Ả' => 'A',
         'Ầ' => 'A',
         'Ấ' => 'A',
         'Ậ' => 'A',
         'Ẩ' => 'A',
         'Ẫ' => 'A',
         'Ằ' => 'A',
         'Ắ' => 'A',
         'Ặ' => 'A',
         'Ẳ' => 'A',
         'Ẵ' => 'A',
         'Ẹ' => 'E',
         'Ẻ' => 'E',
         'Ẽ' => 'E',
         'Ề' => 'E',
         'Ế' => 'E',
         'Ệ' => 'E',
         'Ể' => 'E',
         'Ễ' => 'E',
         'Ị' => 'I',
         'Ỉ' => 'I',
         'Ọ' => 'O',
         'Ỏ' => 'O',
         'Ồ' => 'O',
         'Ố' => 'O',
         'Ộ' => 'O',
         'Ổ' => 'O',
         'Ỗ' => 'O',
         'Ờ' => 'O',
         'Ớ' => 'O',
         'Ợ' => 'O',
         'Ở' => 'O',
         'Ỡ' => 'O',
         'Ụ' => 'U',
         'Ủ' => 'U',
         'Ừ' => 'U',
         'Ứ' => 'U',
         'Ự' => 'U',
         'Ử' => 'U',
         'Ữ' => 'U',
         'Ỳ' => 'Y',
         'Ỵ' => 'Y',
         'Ỷ' => 'Y',
         'Ỹ' => 'Y',

     );
     
     // Make custom replacements
     $str = preg_replace(array_keys($options['replacements']), $options['replacements'], $str);
     
     // Transliterate characters to ASCII
     if ($options['transliterate']) {
         $str = str_replace(array_keys($char_map), $char_map, $str);
     }
     
     // Replace non-alphanumeric characters with our delimiter
     $str = preg_replace('/[^\p{L}\p{Nd}]+/u', $options['delimiter'], $str);
     
     // Remove duplicate delimiters
     $str = preg_replace('/(' . preg_quote($options['delimiter'], '/') . '){2,}/', '$1', $str);
     
     // Truncate slug to max. characters
     $str = mb_substr($str, 0, ($options['limit'] ? $options['limit'] : mb_strlen($str, 'UTF-8')), 'UTF-8');
     
     // Remove delimiter from ends
     $str = trim($str, $options['delimiter']);
     
     return $options['lowercase'] ? mb_strtolower($str, 'UTF-8') : $str;
   }



}
