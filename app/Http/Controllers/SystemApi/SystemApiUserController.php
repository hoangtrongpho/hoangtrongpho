<?php

namespace App\Http\Controllers\SystemApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use DateTime;
use Validator;
use Mail;
use Lang;

use App\Http\Controllers\SystemApi\SystemCommonController as commonCtl;

use App\Models\system_authorities;
use App\Models\SystemUser;

class SystemApiUserController extends Controller
{
	/*** Khởi Tạo Giá Trị ***/
    private $commonCtl;

    private $sessionUser;

    private $rulesMess = [
        // 'news_cate_name.required' => 'We need to know your news_cate_name!',
        // 'news_cate_date_public.required' => 'We need to know your news_cate_date_public!',
        // 'status.required' => 'We need to know your news_cate_status!',
    ];
    /*** Khởi Tạo Giá Trị ***/

    public function __construct(commonCtl $SystemCommonController){
        $this->commonCtl =  $SystemCommonController;
    }

    protected function apiGetListGroups(Request $request){  

        //Kiểm tra phân quyền sử dụng
        $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        if($auth["auth"])
        {
            return response()->json($auth);
        }
        $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $listGroups = DB::table('system_authorities AS sa')
                ->select('sa.auth_id', 
                         'sa.auth_name', 
                         'sa.auth_description')
                ->where('status' , '<>', 0)
                ->get();

        if(!$listGroups->isEmpty())
        {
            $response["success"] = $listGroups;
        }
        else
        {
            $response["warning"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' Nhóm Quyền']);
        }

        return response()->json($response);
    }

    protected function apiGetListUsers(Request $request){  

        //Kiểm tra phân quyền sử dụng
        $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        if($auth["auth"])
        {
            return response()->json($auth);
        }
        $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $whereFunctions = array(
            ['su.user_id', '<>', $this->sessionUser->user_id],
            ['su.status', '<>', 9],
            ['sa.status', '<>', 0]
        );

        $listUsers = DB::table('system_users AS su')
                        ->leftJoin('system_authorities AS sa', 'sa.auth_id', '=', 'su.auth_id')
                        ->select('su.user_id', 
                         'su.user_account', 
                         'su.user_email',
                         'su.user_avatar', 
                         'su.user_fullname', 
                         'su.branch_id',
                         'su.user_sex',
                         'su.user_password', 
                         'su.status',
                         'sa.auth_id', 
                         'sa.auth_name', 
                         'sa.auth_description')
                        ->where($whereFunctions)
                        ->get();

        if(!$listUsers->isEmpty())
        {
            $response["success"] = $listUsers;
            
        }
        else
        {
            $response["warning"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' Người Dùng']);
        }

        return response()->json($response);
    }

    protected function apiGetUserDetail(Request $request){  

        //Kiểm tra phân quyền sử dụng
        $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        if($auth["auth"])
        {
            return response()->json($auth);
        }
        $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $whereFunctions = array(
            ['su.user_id', '=', $request->input('user_id')],
            ['sa.status', '<>', 0]
        );

        $user = DB::table('system_users AS su')
                        ->leftJoin('system_authorities AS sa', 'sa.auth_id', '=', 'su.auth_id')
                        ->select('su.user_id', 
                             'su.user_account', 
                             'su.user_email',
                             'su.user_avatar', 
                             'su.user_fullname', 
                             'su.user_sex',
                             'su.status',
                             'sa.auth_id', 
                             'sa.auth_name', 
                             'sa.auth_description')
                        ->where($whereFunctions)
                        ->first();

        if($user !== null)
        {
            $response["success"] = $user;
            
        }
        else
        {
            $response["warning"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' Người Dùng']);
        }

        return response()->json($response);
    }

    protected function apiAdd(Request $request){   

        //Kiểm tra phân quyền sử dụng
        $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        if($auth["auth"])
        {
            return response()->json($auth);
        }
        $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        if(!empty($request->input('user_password')) && !empty($request->input('user_password_confirm')) )
        {
            if($request->input('user_password') !== $request->input('user_password_confirm'))
            {
                $response["warning"] = ['Mật Khẩu Đã Nhập Không Trùng Khớp.'];
                return response()->json($response);
            }
        }
        
        //Lấy dữ liệu post từ angularjs
        $itemUser = new SystemUser;

        $itemUser->fill($request->all());

        $user_account = $this->commonCtl->slugify($request->input('user_account'));

        if($user_account !== null)
        {
            $itemUser->user_account = $user_account;
        }
        else
        {
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);;
        }

        $itemUser->user_password = md5($request->input('user_password'));

        $itemUser->created_user = $this->sessionUser->user_id;
        $itemUser->created_date = $this->commonCtl->getCarbonNow();
        $itemUser->updated_user = $this->sessionUser->user_id;
        $itemUser->updated_date = $this->commonCtl->getCarbonNow();
        $itemUser->status = $request->input('status');

        // Start transaction!
        DB::beginTransaction();

        try {
            // Validate, then create if valid
            $itemUser->save();
            $response["success"] = "Thêm Người Dùng Mới Thành Công.";
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception').$e;
            return response()->json($response);
        }
        DB::commit();

        return response()->json($response);
    }

    protected function apiRemove(Request $request){ 

        //Kiểm tra phân quyền sử dụng
        $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        if($auth["auth"])
        {
            return response()->json($auth);
        }
        $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $itemDelete = SystemUser::where('user_id', '=', $request->input('user_id'))->first();

        if($itemDelete === null)
        {
            $response["warning"] = [ "Lỗi Dữ Liệu" => "Không Tìm Thấy Dữ Liệu, Hoặc Dữ Liệu Đã Bị Xóa Từ Trước."];
            return response()->json($response);
        }

        // Start transaction!
        DB::beginTransaction();

        try {
            // Validate, then create if valid
            $deletedRows = SystemUser::where('user_id', '=', $itemDelete->user_id)->delete();

            if($deletedRows > 0){
                $response["success"] = "Xóa Thành Công!";
            }
            else{
                $response["error"] = "Xóa Không Thành Công!";
                DB::rollback();
            }
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = $e->getErrors();
            return response()->json($response);
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = $e;
            return response()->json($response);
        }
        DB::commit();

        return response()->json($response);
    }

    protected function apiChangeStatus(Request $request){   

        //Kiểm tra phân quyền sử dụng
        $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        if($auth["auth"])
        {
            return response()->json($auth);
        }
        $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";
        
        //Lấy dữ liệu post từ angularjs
        $itemUpdate = SystemUser::where('user_id', '=', $request->input('user_id'))->first();

        if($itemUpdate === null)
        {
            $response["warning"] = [ "Lỗi Dữ Liệu" => "Không Tìm Thấy Dữ Liệu, Hoặc Dữ Liệu Đã Bị Xóa Từ Trước."];
            return response()->json($response);
        }

        // Start transaction!
        DB::beginTransaction();

        try {

            // Validate, then create if valid
            $updatedRows = SystemUser::where('user_id', '=', $itemUpdate->user_id)
                            ->update([
                                'status' => $request->input('status'),
                                'updated_date' => $this->commonCtl->getCarbonNow(),
                                'updated_user' => $this->sessionUser->user_id
                            ]);

            if($updatedRows > 0)
            {
                $response["success"] = "Cập Nhật Thành Công!";
            }
            else
            {
                $response["error"] = "Cập Nhật Không Thành Công!";
                DB::rollback();
            }
            
        } 
        catch(ValidationException $e)
        {
            // Rollback and then redirect
            // back to form with errors
            DB::rollback();

            $response["error"] = $e->getErrors();
            return response()->json($response);

            // return Redirect::to('/form')
            // ->withErrors( $e->getErrors() )
            // ->withInput();
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = $e;
            return response()->json($response);
            // throw $e;
        }

        // If we reach here, then
        // data is valid and working.
        // Commit the queries!
        DB::commit();

        return response()->json($response);
    }

    protected function apiUpdate(Request $request){   

        $this->sessionUser = Session::get('userAuth'); 

        $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        if($auth["auth"])
        {
            return response()->json($auth);
        }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        // $rules = [
        //     'news_cate_id' => 'required',
        //     'news_cate_name' => 'required',
        //     'news_cate_date_public' => 'required',
        //     'status' => 'required',
        // ];

        // $valiData = $this->commonCtl->validRequest($request,$rules,$this->rulesMess);

        // if($valiData->fails())
        // {   
        //     $response["warning"] = $valiData->errors();
        //     return response()->json($response);
        // }

        if(!empty($request->input('user_password')) && !empty($request->input('user_password_confirm')) )
        {
            if($request->input('user_password') !== $request->input('user_password_confirm'))
            {
                $response["warning"] = ['Mật Khẩu Đã Nhập Không Trùng Khớp.'];
                return response()->json($response);
            }
        }

        $existItem = SystemUser::where('user_id','=',$request->input('user_id'))->first();

        if($existItem == null)
        {
            $response["warning"] = [ "warning" => Lang::get('messages.common_error_not_exist_data')];
            return response()->json($response);
        }

        //Lấy dữ liệu post từ angularjs
        $itemRequest = new SystemUser;
        $itemRequest->fill($request->all());

        // Start transaction!
        DB::beginTransaction();

        try {
            // Validate, then create if valid

            $updatedRows = 0;
            
            if(empty($request->input('user_password')))
            {
                $updatedRows = SystemUser::where('user_id', '=', $existItem->user_id)
                            ->update([
                                'user_account' => $itemRequest->user_account,
                                'auth_id' => $itemRequest->auth_id,
                                'user_email' => $itemRequest->user_email,
                                'user_avatar' => $itemRequest->user_avatar,
                                'user_fullname' => $itemRequest->user_fullname,
                                'user_sex' => $itemRequest->user_sex,
                                'branch_id' => $itemRequest->branch_id,
                                'user_login_count' => 0,
                                'status' => $itemRequest->status,
                                'updated_date' => $this->commonCtl->getCarbonNow(),
                                'updated_user' => $this->sessionUser->user_id
                            ]);
            }
            else
            {
                $updatedRows = SystemUser::where('user_id', '=', $existItem->user_id)
                            ->update([
                                'user_account' => $itemRequest->user_account,
                                'auth_id' => $itemRequest->auth_id,
                                'user_email' => $itemRequest->user_email,
                                'user_avatar' => $itemRequest->user_avatar,
                                'user_fullname' => $itemRequest->user_fullname,
                                'user_sex' => $itemRequest->user_sex,
                                'branch_id' => $itemRequest->branch_id,
                                'user_password' => md5($itemRequest->user_password),
                                'user_login_count' => 0,
                                'status' => $itemRequest->status,
                                'updated_date' => $this->commonCtl->getCarbonNow(),
                                'updated_user' => $this->sessionUser->user_id
                            ]);
            }
            

            if($updatedRows > 0)
            {
                $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_updated'), ['Tài Khoản']);
            }
            else
            {
                $response["error"] = Lang::get('Cập Nhật Không Thành Công');
                DB::rollback();
            }

            // $response["success"] = "Cập Nhật Thể Loại Thành Công!";
        } 
        catch(ValidationException $e)
        {
            // Rollback and then redirect
            // back to form with errors
            DB::rollback();

            $response["error"] = Lang::get('messages.common_error_exception').$e->getErrors();
            return response()->json($response);

            // return Redirect::to('/form')
            // ->withErrors( $e->getErrors() )
            // ->withInput();
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception').$e;
            return response()->json($response);
            // throw $e;
        }

        // If we reach here, then
        // data is valid and working.
        // Commit the queries!
        DB::commit();

        return response()->json($response);
    }

    protected function apiUpdateProfile(Request $request){   

        $this->sessionUser = Session::get('userAuth'); 

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";
        
        $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        if($auth["auth"])
        {
            return response()->json($auth);
        }

        // $rules = [
        //     'news_cate_id' => 'required',
        //     'news_cate_name' => 'required',
        //     'news_cate_date_public' => 'required',
        //     'status' => 'required',
        // ];

        // $valiData = $this->commonCtl->validRequest($request,$rules,$this->rulesMess);

        // if($valiData->fails())
        // {   
        //     $response["warning"] = $valiData->errors();
        //     return response()->json($response);
        // }

        if(!empty($request->input('user_password')) && !empty($request->input('user_password_confirm')) )
        {
            if($request->input('user_password') !== $request->input('user_password_confirm'))
            {
                $response["warning"] = ['Mật Khẩu Đã Nhập Không Trùng Khớp.'];
                return response()->json($response);
            }
        }
        
        $existItem = SystemUser::where('user_id','=',$request->input('user_id'))->first();

        if($existItem == null)
        {
            $response["warning"] = [ "warning" => Lang::get('messages.common_error_not_exist_data')];
            return response()->json($response);
        }

        //Lấy dữ liệu post từ angularjs
        $itemRequest = new SystemUser;
        $itemRequest->fill($request->all());

        // Start transaction!
        DB::beginTransaction();

        try {
            // Validate, then create if valid

            $updatedRows = 0;
            
            if(empty($request->input('user_password')))
            {
                $updatedRows = SystemUser::where('user_id', '=', $existItem->user_id)
                            ->update([
                                'user_email' => $itemRequest->user_email,
                                'user_avatar' => $itemRequest->user_avatar,
                                'user_fullname' => $itemRequest->user_fullname,
                                'user_sex' => $itemRequest->user_sex,
                                'updated_date' => $this->commonCtl->getCarbonNow(),
                                'updated_user' => $this->sessionUser->user_id
                            ]);
            }
            else
            {
                $updatedRows = SystemUser::where('user_id', '=', $existItem->user_id)
                            ->update([
                                'user_email' => $itemRequest->user_email,
                                'user_avatar' => $itemRequest->user_avatar,
                                'user_fullname' => $itemRequest->user_fullname,
                                'user_sex' => $itemRequest->user_sex,
                                'user_password' => md5($itemRequest->user_password),
                                'updated_date' => $this->commonCtl->getCarbonNow(),
                                'updated_user' => $this->sessionUser->user_id
                            ]);
            }
            
            if($updatedRows > 0)
            {

                $userInfo = DB::table('system_users')
                ->leftJoin('system_authorities', 'system_users.auth_id', '=', 'system_authorities.auth_id')
                ->select('user_id', 'user_fullname', 'user_account', 'user_avatar', 'auth_name', 'user_avatar', 'user_sex')
                ->where(function($query) use ($existItem)
                {
                    $query->where('user_account', '=', $existItem->user_account);
                    $query->where('system_users.status', '<>', 0);
                })
                ->first();

                Session::put('lockScreen', false);
                Session::put('userAuth', $userInfo);

                $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_updated'), ['Tài Khoản']);
            }
            else
            {
                $response["error"] = Lang::get('Cập Nhật Không Thành Công');
                DB::rollback();
            }

            // $response["success"] = "Cập Nhật Thể Loại Thành Công!";
        } 
        catch(ValidationException $e)
        {
            // Rollback and then redirect
            // back to form with errors
            DB::rollback();

            $response["error"] = Lang::get('messages.common_error_exception').$e->getErrors();
            return response()->json($response);

            // return Redirect::to('/form')
            // ->withErrors( $e->getErrors() )
            // ->withInput();
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception').$e;
            return response()->json($response);
            // throw $e;
        }

        // If we reach here, then
        // data is valid and working.
        // Commit the queries!
        DB::commit();

        return response()->json($response);
    }
}
