<?php

namespace App\Http\Controllers\SystemApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use DateTime;
use Validator;
use Mail;
use Lang;

use App\Http\Controllers\SystemApi\SystemCommonController as commonCtl;
use App\Models\NewsTagsDetail;
use App\Models\NewsTagsList;
use App\Models\ProductsOriginsDetail;
use App\Models\ProductsDetail;

class SystemApiProductOriginController extends Controller
{

    /*** Khởi Tạo Giá Trị ***/
    private $commonCtl;

    private $sessionUser;

    private $rulesMess = [
        'origin_id.required' => 'Vui Lòng Thêm ID Xuất Xứ Sản Phẩm.',
        'origin_slug.required' => 'Vui Lòng Thêm Mã Xuất Xứ Sản Phẩm.',
        'origin_name.required' => 'Vui Lòng Thêm Tên Xuất Xứ Sản Phẩm.',
    ];

    /*** Khởi Tạo Giá Trị ***/
    public function __construct(commonCtl $SystemCommonController){
        $this->commonCtl =  $SystemCommonController;
    }

    //Ajax Post Lấy Danh Sách 
    protected function apiGetList(Request $request){  
        
        $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        if($auth["auth"])
        {
            return response()->json($auth);
        }
        $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $listItem = DB::table('products_origins_detail as od')
                ->leftJoin('products_detail AS pd', 'od.origin_id', '=', 'pd.origin_id')
                ->select('od.origin_id', 
                        'od.origin_slug',
                        'od.origin_name',
                        'od.origin_description',
                        DB::raw('DATE_FORMAT(od.updated_date,"%d/%m/%Y %H:%m:%s") AS updated_date'),
                        'od.status',
                        DB::raw('count(pd.color_id) as item_couter'))
                ->where('od.status','<>',0)
                ->groupBy('od.origin_id', 
                        'od.origin_slug',
                        'od.origin_name',
                        'od.origin_description',
                        'od.updated_date',
                        'od.status')
                ->get();

        if(!$listItem->isEmpty())
        {
            $response["success"] = $listItem;
        }
        else
        {
            $response["warning"] = Lang::get('messages.common_warning_empty_list');
        }

        return response()->json($response);
    } 

    //Ajax Post Thêm
    protected function apiAdd(Request $request){  
        
        //Kiểm tra phân quyền sử dụng
        $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        if($auth["auth"])
        {
            return response()->json($auth);
        }
        $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        /** Kiểm tra giá trị nhập vào **/
        $rules = [
            'origin_name' => 'required',
        ];

        $valiData = $this->commonCtl->validRequest($request,$rules,$this->rulesMess);

        if($valiData->fails())
        {   
            $response["warning"] = $valiData->errors();
            return response()->json($response);
        }
       

        /** Khởi tạo giá trị **/
        $origin_slug = $this->commonCtl->slugify($request->input('origin_name'));
        if(empty($origin_slug))
        {
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }

        $existsItem = ProductsOriginsDetail::where('origin_slug','=',$origin_slug)
                        ->first();
        if(!empty($existsItem))
        {
            $response["warning"] = [$this->commonCtl->replaceTitle(Lang::get('messages.common_error_exist_data'), ['Xuất', 'xứ'])];
            return response()->json($response);
        }                

        $itemAdd = new ProductsOriginsDetail;

        $itemAdd->fill($request->all());
        $itemAdd->origin_slug = $origin_slug;
        $itemAdd->created_user = $this->sessionUser->user_id;
        $itemAdd->created_date = $this->commonCtl->getCarbonNow();
        $itemAdd->updated_user = $this->sessionUser->user_id;
        $itemAdd->updated_date = $this->commonCtl->getCarbonNow();
        $itemAdd->status = 1;

        DB::beginTransaction();

        try {
            $itemAdd->save();

            $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_created'), ['xuất xứ']);
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);

            // return Redirect::to('/form')
            // ->withErrors( $e->getErrors() )
            // ->withInput();
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
            // throw $e;
        }
        DB::commit();

        return response()->json($response);
    } 

    //Ajax Post Thêm
    protected function apiUpdate(Request $request){  

        //Kiểm tra phân quyền sử dụng
        $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        if($auth["auth"])
        {
            return response()->json($auth);
        }
        $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $rules = [
            'origin_id' => 'required',
            'origin_slug' => 'required',
            'origin_name' => 'required',
        ];

        $valiData = $this->commonCtl->validRequest($request,$rules,$this->rulesMess);

        /** Kiểm tra giá trị nhập vào **/
        if($valiData->fails())
        {   
            $response["warning"] = $valiData->errors();
            return response()->json($response);
        }

        $existsItem = ProductsOriginsDetail::where('origin_slug','=',$request->input('origin_slug'))
                        ->first();

        if(empty($existsItem))
        {
            $response["warning"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' xuất xứ']);
            return response()->json($response);
        }

        $origin_slug = $this->commonCtl->slugify($request->input('origin_name'));
        if(empty($origin_slug))
        {
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }

        $existsSlug = ProductsOriginsDetail::where([
                                    ['origin_slug','=',$origin_slug],
                                    ['origin_id','=',$existsItem->origin_id],
                                ])->first();
        if($existsSlug)
        {
            $response["warning"] = [$this->commonCtl->replaceTitle(Lang::get('messages.common_error_exist_data'), ['Tên', 'xuất xứ'])];
            return response()->json($response);
        }

        $existsItem->origin_slug = $origin_slug;
        $existsItem->origin_name = $request->input('origin_name');
        $existsItem->origin_description = $request->input('origin_description');
        $existsItem->updated_user = $this->sessionUser->user_id;
        $existsItem->updated_date = $this->commonCtl->getCarbonNow();

        DB::beginTransaction();
        try 
        {
            $existsItem->save();

            $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_updated'), ['xuất xứ']);
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);

            // return Redirect::to('/form')
            // ->withErrors( $e->getErrors() )
            // ->withInput();
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
            // throw $e;
        }

        DB::commit();

        return response()->json($response);
    } 

    //Ajax Post Xóa Thẻ
    protected function apiRemove(Request $request){ 

        //Kiểm tra phân quyền sử dụng
        $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        if($auth["auth"])
        {
            return response()->json($auth);
        }
        $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        /** Kiểm tra giá trị nhập vào **/
        $rules = [
            'origin_id' => 'required',
        ];

        $valiData = $this->commonCtl->validRequest($request,$rules,$this->rulesMess);

        if($valiData->fails())
        {   
            $response["warning"] = $valiData->errors();
            return response()->json($response);
        }

        $itemDelete = ProductsOriginsDetail::where('origin_id', '=', $request->input('origin_id'))->first();

        if(empty($itemDelete))
        {
            $response["error"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' xuất xứ']);
            return response()->json($response);
        }

        $checkExists = ProductsDetail::where('origin_id', '=', $request->input('origin_id'))->get();

        DB::beginTransaction();
        try {

            if(!$checkExists->isEmpty())
            {
                $itemDelete->updated_user = $this->sessionUser->user_id;
                $itemDelete->updated_date = $this->commonCtl->getCarbonNow();
                $itemDelete->status = 0;

                $itemDelete->save();

                $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_removed'), ['xuất xứ']);
            }
            else
            {
                $deletedRows = ProductsOriginsDetail::where('origin_id', '=', $itemDelete->origin_id)->delete();

                if($deletedRows > 0){
                    $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_removed'), ['xuất xứ']);
                }
                else{
                    $response["warning"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_removed'), ['xuất xứ']);
                }
            }
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);

            // return Redirect::to('/form')
            // ->withErrors( $e->getErrors() )
            // ->withInput();
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
            // throw $e;
        }
        DB::commit();

        return response()->json($response);
    }
}
