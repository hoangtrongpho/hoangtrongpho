<?php

namespace App\Http\Controllers\SystemApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use DB;

// use App\Models\SystemUser;
use App\Http\Controllers\SystemApi\SystemCommonController as commonCtl;

class SystemApiCommonController extends Controller
{
    private $commonCtl;

    public function __construct(commonCtl $SystemCommonController){
        $this->commonCtl =  $SystemCommonController;
        $this->sessionUser =  Session::get('userAuth');
    }


   //Ajax Post đăng nhập
   protected function apiSendMail(Request $request){   

      $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
      if($auth["auth"])
      {
         return response()->json($auth);
      }

      //Tạo biến kiểm tra kết quả
      $response["success"] = "";
      $response["result"] = "";
      $response["warning"] = "";
      $response["error"] = "";

      Mail::raw('Text to e-mail', function($message)
      {
         $message->from('enjoyvinh@gmail.com', 'Laravel');

         $message->to('vinh.nguyen2303@outlook.com', 'Vinh Nguyễn')->cc('enjoyvinh@yahoo.com','Vinh Yahoo');
      });

      // Mail::send('folder.view', $data, function($message) {
      //     $message->to('vinh.nguyen2303@outlook.com', 'Vinh Nguyễn')->subject('Welcome!');
      // });

      print_r($response);
   }
}
