<?php

namespace App\Http\Controllers\SystemApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use DateTime;
use Validator;
use Mail;
use Lang;

use App\Http\Controllers\SystemApi\SystemCommonController as commonCtl;

use App\Models\NewsDetail;
use App\Models\NewsCategoriesDetail;
use App\Models\NewsTagsDetail;
use App\Models\NewsTagsList;
use App\Models\NewsProvincesList;
use App\Models\NewsCounter;

class SystemApiNewsController extends Controller
{

    /*** Khởi Tạo Giá Trị ***/
    private $commonCtl;

    private $sessionUser;

    private $rulesMess = [
        'news_id.required' => 'Vui Lòng Chọn Mã Bản Tin',
        'news_slug.required' => 'Vui Lòng Nhập Slug Bản Tin',
        'news_title.required' => 'Vui Lòng Nhập Tiêu Đề Bản Tin',
        'news_description.required' => 'Vui Lòng Nhập Mô Tả Bản Tin',
        'news_content.required' => 'Vui Lòng Nhập Nội Dung Bản Tin',
        'news_author_aliases.required' => 'Vui Lòng Nhập Bút Danh Người Viết',
        'news_ratings.required' => 'Vui Lòng Nhập Đánh Giá Bản Tin',
        'news_thumbnail.required' => 'Vui Lòng Nhập Hình Đại Diện Bản Tin',
        'news_release_date.required' => 'Vui Lòng Chọn Ngày Phát Hành Bản Tin ',
        'news_expiration_date.required' => 'Vui Lòng Nhập Kết Thúc Bản Tin',
        'news_format_page.required' => 'Vui Lòng Chọn Loại Bản Tin ',
        'news_comment_status.required' => 'Vui Lòng Chọn Trạng Thái Bình Luận Bản Tin',
        'status.required' => 'Vui Lòng Chọn Trạng Thái Bản Tin',
        'visibility.required' => 'Vui Lòng Chọn Trạng Thái Bản Tin',
    ];
    /*!! Khởi Tạo Giá Trị !!*/

    public function __construct(commonCtl $SystemCommonController){
        $this->commonCtl =  $SystemCommonController;
    }

    /*** API Hàm Chức Năng ***/  

    //Ajax Post Lấy Danh Sách 
    protected function apiGetList(Request $request){  

        $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        if($auth["auth"])
        {
            return response()->json($auth);
        }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $whereFunctions = array(
            // ['nd.news_release_date', '>=', $this->commonCtl->formatCarbonDatetime_From($request->input('news_date_public'))],
            // ['nd.news_release_date', '<=', $this->commonCtl->formatCarbonDatetime_To($request->input('news_date_expires'))]
        );

        if(!empty($request->input('news_title')))
        {
            array_push($whereFunctions, ['nd.news_title', 'LIKE', '%'.$request->input('news_title').'%']);
        }

        $listCateId = array();

        if(!empty($request->input('list_cate_id')) && count($request->input('list_cate_id')) > 0 )
        {
            foreach ($request->input('list_cate_id') as $k => $v) {
                array_push($listCateId, $v['news_cate_id']);
            }
        }

        $listNews = DB::table('news_detail AS nd')
                ->leftJoin('news_categories_detail AS cd', 'nd.cate_id', '=', 'cd.news_cate_id')
                ->leftJoin('news_counter AS ct', 'ct.news_id', '=', 'nd.news_id')
                ->select('nd.news_id', 
                         'nd.news_slug', 
                         'nd.news_title', 
                         'nd.news_description', 
                         'nd.news_content', 
                         'nd.news_author_aliases',
                         'nd.news_ratings',
                         'nd.news_thumbnail',
                         'nd.news_release_date',
                         'nd.news_expiration_date',
                         'nd.news_format_page', 
                         'nd.news_comment_status', 
                         'nd.updated_date',
                         DB::raw("DATE_FORMAT(nd.updated_date,'%d/%m/%Y') AS updated_date_format"), 
                         'nd.status', 
                         'nd.visibility', 
                         'nd.news_pinned',
                         'cd.news_cate_id',
                         'cd.news_cate_name',
                         DB::raw('count(ct.counter_client_ip) as views_couter'))
                ->where($whereFunctions)
                ->where(function($q) use ($listCateId) {

                    if(count($listCateId) > 0)
                    {
                        $q->whereIn('cd.news_cate_id', $listCateId);
                    }
                })
                ->groupBy('nd.news_id', 
                         'nd.news_slug', 
                         'nd.news_title', 
                         'nd.news_description', 
                         'nd.news_content', 
                         'nd.news_author_aliases',
                         'nd.news_ratings',
                         'nd.news_thumbnail',
                         'nd.news_release_date',
                         'nd.news_expiration_date',
                         'nd.news_format_page', 
                         'nd.news_comment_status', 
                         'nd.updated_date', 
                         'nd.status', 
                         'nd.visibility', 
                         'nd.news_pinned',
                         'cd.news_cate_id',
                         'cd.news_cate_name')
                ->orderBy('cd.news_cate_id', 'asc')
                ->orderBy('nd.updated_date', 'desc')

                ->get();

        if(!$listNews->isEmpty())
        {
            $response["success"] = $listNews;
        }
        else
        {
            $response["warning"] = [Lang::get('messages.common_warning_empty_list')];
        }

        return response()->json($response);
    }

    //Ajax Post Lấy Chi Tiết Tin Tức
    protected function apiGetDetail(Request $request){  
        
        $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        if($auth["auth"])
        {
            return response()->json($auth);
        }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $whereFunctions = array(
            ['nd.news_slug', '=', $request->input('news_slug')],
            ['nd.status', '<>', 0]
        );

        $detail = DB::table('news_detail AS nd')
                ->leftJoin('news_categories_detail AS cd', 'nd.cate_id', '=', 'cd.news_cate_id')
                
                ->select('nd.news_id', 
                         'nd.news_slug', 
                         'nd.news_title', 
                         'nd.news_description',
                         'nd.news_content', 
                         'nd.news_author_aliases',
                         'nd.news_ratings',
                         'nd.news_thumbnail',
                         'nd.news_release_date',
                         'nd.news_expiration_date',
                         'nd.news_format_page', 
                         'nd.news_comment_status', 
                         'nd.status', 
                         'nd.news_pinned', 
                         'nd.visibility', 
                         'nd.news_seo_title',
                         'nd.news_seo_description',
                         'cd.news_cate_id',
                         'cd.news_cate_name',
                         'cd.news_cate_slug')
                ->where($whereFunctions)
                ->first();

        $whereFunctions = array();

        array_push($whereFunctions, ['nd.news_id', '=', $detail->news_id]);
        array_push($whereFunctions, ['td.status', '<>', 0]);

        $listTags = DB::table('news_detail AS nd')
                ->leftJoin('news_tags_list AS tl', 'nd.news_id', '=', 'tl.news_id')
                ->leftJoin('news_tags_detail AS td', 'tl.news_tag_id', '=', 'td.tag_id')
                ->select('td.tag_id',
                         'td.tag_slug',
                         'td.tag_name')
                ->where($whereFunctions)
                ->get();

        $whereFunctions = array();

        if($detail !== null)
        {
            $response["success"] = $detail;
            $response["listTags"] = $listTags;
        }
        else
        {
            $response["warning"] = [$this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' Bản Tin'])];
        }

        return response()->json($response);
    }

    //Ajax Post Thêm Bản Tin
    protected function apiAdd(Request $request){   

        $this->sessionUser = Session::get('userAuth'); 

        $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        if($auth["auth"])
        {
            return response()->json($auth);
        }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";



        //Lấy dữ liệu post từ angularjs
        $itemNews = new NewsDetail;

        $itemNews->fill($request->all());

        $news_date_public = $this->commonCtl->formatCarbonDatetime_From($request->input('news_release_date'));
        if($news_date_public !== null)
        {
            $itemNews->news_release_date = $news_date_public;
        }
        else
        {
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }

        $news_date_expires = $this->commonCtl->formatCarbonDatetime_To($request->input('news_expiration_date'));
        if($news_date_expires !== null)
        {
            $itemNews->news_expiration_date = $news_date_expires;
        }
        else
        {
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }

        $news_slug = $this->commonCtl->slugify($request->input('news_title'));

        if($news_slug !== null)
        {
            $itemNews->news_slug = $news_slug;
        }
        else
        {
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }

        $itemNews->created_user = $this->sessionUser->user_id; 
        $itemNews->created_date = $this->commonCtl->getCarbonNow();
        $itemNews->updated_user = $this->sessionUser->user_id; 
        $itemNews->updated_date = $this->commonCtl->getCarbonNow();

        DB::beginTransaction();

        try {
            $itemNews->save();

            if(count($request->input('list_tags')) > 0)
            {
                foreach ($request->input('list_tags') as $tag) {
                    $tag = new NewsTagsList([
                        'news_id' => $itemNews->news_id,
                        'news_tag_id' => $tag['tag_id'],
                    ]);
                    $tag->save();
                }
            }

            $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_created'), ['Bản Tin']);
            
        } 
        catch(ValidationException $e)
        {
            DB::rollback();

            // $response["error"] = $e->getErrors();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }

        DB::commit();

        return response()->json($response);
    }

    //Ajax Post Cập Nhật Bản Tin
    protected function apiUpdate(Request $request){   

        $this->sessionUser = Session::get('userAuth'); 
        
        $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        if($auth["auth"])
        {
            return response()->json($auth);
        }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $rules = [
            'news_slug' => 'required',
            'news_title' => 'required',
            'news_description' => 'required',
            'news_content' => 'required',
            'news_author_aliases' => 'required',
            'news_ratings' => 'required',
            'news_thumbnail' => 'required',
            'news_release_date' => 'required',
            'news_expiration_date' => 'required',
            'news_format_page' => 'required',
            'news_comment_status' => 'required',
            'status' => 'required',
            'visibility' => 'required',
        ];

        $valiData = $this->commonCtl->validRequest($request,$rules,$this->rulesMess);

        if($valiData->fails())
        {   
            $response["warning"] = $valiData->errors();
            return response()->json($response);
        }

        //Kiêm tra thể loại tồn tại
        $itemCategory = NewsCategoriesDetail::find($request->input('news_cate_id'));

        if($itemCategory === null)
        {
            $response["warning"] = [$this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' Danh Mục Bản Tin'])];
            return response()->json($response);
        }

        //Kiêm tra bản tin tồn tại
        $currentNews = NewsDetail::where('news_slug','=',$request->input('news_slug'))
                        ->first();

        if($currentNews === null)
        {
            $response["warning"] = [$this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' Bản Tin'])];
            return response()->json($response);
        }

        if(!empty($request->input('news_title')))
        {
            $currentNews->news_title = $request->input('news_title');
        }

        if(!empty($request->input('news_cate_id')))
        {
            $currentNews->cate_id = $request->input('news_cate_id');
        }

        if(!empty($request->input('news_description')))
        {
            $currentNews->news_description = $request->input('news_description');
        }
        if(!empty($request->input('news_content')))
        {
            $currentNews->news_content = $request->input('news_content');
        }
        if(!empty($request->input('news_author_aliases')))
        {
            $currentNews->news_author_aliases = $request->input('news_author_aliases');
        }
        if(!empty($request->input('news_ratings')))
        {
            $currentNews->news_ratings = $request->input('news_ratings');
        }
        if(!empty($request->input('news_thumbnail')))
        {
            $currentNews->news_thumbnail = $request->input('news_thumbnail');
        }
        if(!empty($request->input('news_format_page')))
        {
            $currentNews->news_format_page = $request->input('news_format_page');
        }
        if(!empty($request->input('status')))
        {
            $currentNews->status = $request->input('status');
        }
        if(!empty($request->input('news_pinned')))
        {
            $currentNews->news_pinned = $request->input('news_pinned');
        }
        if(!empty($request->input('visibility')))
        {
            $currentNews->visibility = $request->input('visibility');
        }
        if(!empty($request->input('news_download')))
        {
            $currentNews->news_download = $request->input('news_download');
        }
        if(!empty($request->input('news_seo_title')))
        {
            $currentNews->news_seo_title = $request->input('news_seo_title');
        }
        if(!empty($request->input('news_seo_description')))
        {
            $currentNews->news_seo_description = $request->input('news_seo_description');
        }
        if(!empty($request->input('news_seo_keywords')))
        {
            $currentNews->news_seo_keywords = $request->input('news_seo_keywords');
        }
        
        $news_date_public = $this->commonCtl->formatCarbonDatetime_From($request->input('news_release_date'));
        if($news_date_public !== null)
        {
            $currentNews->news_release_date = $news_date_public;
        }
        else
        {
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }

        $news_date_expires = $this->commonCtl->formatCarbonDatetime_To($request->input('news_expiration_date'));
        if($news_date_expires !== null)
        {
            $currentNews->news_expiration_date = $news_date_expires;
        }
        else
        {
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }

        $news_slug = $this->commonCtl->slugify($request->input('news_title'));

        if($news_slug !== null)
        {
            $currentNews->news_slug = $news_slug;
        }
        else
        {
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }

        $currentNews->updated_user = $this->sessionUser->user_id;
        $currentNews->updated_date = $this->commonCtl->getCarbonNow();

        DB::beginTransaction();

        try {

            $deletedRowsTags = NewsTagsList::where('news_id', '=', $currentNews->news_id)->delete();

            $currentNews->save();
            
            $response["news_slug"] = $news_slug;

            if(count($request->input('list_tags')) > 0)
            {
                foreach ($request->input('list_tags') as $tag) {
                    $tag = new NewsTagsList([
                        'news_id' => $currentNews->news_id,
                        'news_tag_id' => $tag['tag_id'],
                    ]);
                    $tag->save();
                }
            }

            $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_updated'), ['Bản Tin']);
            
        } 
        catch(ValidationException $e)
        {
            DB::rollback();

            // $response["error"] = $e->getErrors();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }

        DB::commit();

        return response()->json($response);
    }

    //Ajax Post Xóa Tin Tức
    protected function apiRemove(Request $request){ 

        $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        if($auth["auth"])
        {
            return response()->json($auth);
        }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $rules = [
            'news_id' => 'required',
        ];

        $valiData = $this->commonCtl->validRequest($request,$rules,$this->rulesMess);

        if($valiData->fails())
        {   
            $response["warning"] = $valiData->errors();
            return response()->json($response);
        }

        $itemDelete = NewsDetail::where('news_id', '=', $request->input('news_id'))->first();

        if($itemDelete == null)
        {
            $response["error"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' Bản Tin']);
            return response()->json($response);
        }

        // Start transaction!
        DB::beginTransaction();

        try {
            $deletedNewsCounter = NewsCounter::where('news_id', '=', $itemDelete->news_id)->delete();

            $deletedRowsTags = NewsTagsList::where('news_id', '=', $itemDelete->news_id)->delete();

            $deletedRowsNews = NewsDetail::where('news_id', '=', $itemDelete->news_id)->delete();

            if($deletedRowsNews > 0){
                $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_removed'), ['Bản Tin']);
            }
            else{
                $response["warning"] = [$this->commonCtl->replaceTitle(Lang::get('messages.common_error_removed'), ['Bản Tin'])];
                DB::rollback();
            }
        } 
        catch(ValidationException $e)
        {
            // Rollback and then redirect
            // back to form with errors
            DB::rollback();
            // $response["error"] = $e->getErrors();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
            // return Redirect::to('/form')
            // ->withErrors( $e->getErrors() )
            // ->withInput();
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
            // throw $e;
        }

        // If we reach here, then
        // data is valid and working.
        // Commit the queries!
        DB::commit();

        return response()->json($response);
    }

    protected function apiChangeStatus(Request $request){   

        $this->sessionUser = Session::get('userAuth'); 

        $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        if($auth["auth"])
        {
            return response()->json($auth);
        }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $rules = [
            'news_id' => 'required',
        ];

        $valiData = $this->commonCtl->validRequest($request,$rules,$this->rulesMess);

        if($valiData->fails())
        {   
            $response["warning"] = $valiData->errors();
            return response()->json($response);
        }

        $itemUpdate = NewsDetail::where('news_id', '=', $request->input('news_id'))->first();

        if($itemUpdate == null)
        {
            $response["error"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' Bản Tin']);
            return response()->json($response);
        }

        // Start transaction!
        DB::beginTransaction();

        try {
            $updatedRows = NewsDetail::where('news_id', '=', $itemUpdate->news_id)
                            ->update([
                                'status' => $request->input('status'),
                                'updated_date' => $this->commonCtl->getCarbonNow(),
                                'updated_user' => $this->sessionUser->user_id
                            ]);

            if($updatedRows > 0){
                $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_updated'), ['Bản Tin']);
            }
            else{
                $response["warning"] = [$this->commonCtl->replaceTitle(Lang::get('messages.common_error_updated'), ['Bản Tin'])];
                DB::rollback();
            }
            
        } 
        catch(ValidationException $e)
        {
            // Rollback and then redirect
            // back to form with errors
            DB::rollback();
            // $response["error"] = $e->getErrors();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
            // return Redirect::to('/form')
            // ->withErrors( $e->getErrors() )
            // ->withInput();
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
            // throw $e;
        }

        // If we reach here, then
        // data is valid and working.
        // Commit the queries!
        DB::commit();

        return response()->json($response);
    }

    /*!! API Hàm Chức Năng !!*/  
}
