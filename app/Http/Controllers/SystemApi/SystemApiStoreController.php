<?php

namespace App\Http\Controllers\SystemApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use DateTime;
use Validator;
use Mail;
use Lang;

use App\Http\Controllers\SystemApi\SystemCommonController as commonCtl;

use App\Models\BranchesDetail;
use App\Models\ProductsDetail;
use App\Models\StoreDetail;

class SystemApiStoreController extends Controller
{

    /*** Khởi Tạo Giá Trị ***/
    private $commonCtl;

    private $sessionUser;


    /*** Khởi Tạo Giá Trị ***/
    public function __construct(commonCtl $SystemCommonController){
        $this->commonCtl =  $SystemCommonController;
    }
    //apiGetList
    protected function apiGetList(Request $request){  

        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $listProducts = DB::select("
            select pd.product_id,pd.product_name,pd.product_thumbnail,
            br.branch_id,br.branch_name,pd.store_quantity,
            st.branch_store_quantity,
            
            CASE WHEN c.sum_deliver_branch IS null THEN 0 ELSE c.sum_deliver_branch END AS sum_deliver_branch,

            CASE WHEN (pd.store_quantity - IFNULL(c.sum_deliver_branch, 0)) IS null THEN 0 ELSE (pd.store_quantity - IFNULL(c.sum_deliver_branch, 0)) END AS sum_deliver_branch_remain,
            
            CASE WHEN a.sum_sale_product_quantity IS null THEN 0 ELSE a.sum_sale_product_quantity END AS sum_sale_product_quantity,

            CASE WHEN (st.branch_store_quantity- IFNULL(sum_sale_product_quantity, 0)) IS null THEN 0 ELSE (st.branch_store_quantity-IFNULL(sum_sale_product_quantity, 0)) END AS sum_sale_product_quantity_remain_format,

            CASE WHEN b.sum_sale_store_product_quantity IS null THEN 0 ELSE b.sum_sale_store_product_quantity END AS sum_sale_store_product_quantity,
            
            CASE WHEN (pd.store_quantity - IFNULL(b.sum_sale_store_product_quantity, 0)) IS null THEN 0 ELSE (pd.store_quantity - IFNULL(b.sum_sale_store_product_quantity, 0)) END AS sum_sale_store_remain_product_quantity

            FROM store_detail AS st
            INNER JOIN products_detail AS pd ON st.product_id=pd.product_id
            INNER JOIN branches_detail AS br ON br.branch_id=st.branch_id

            LEFT JOIN
            (   SELECT pd.product_id,pd.product_name,iv.branch_id,
                SUM(ivd.product_quantity) AS sum_sale_product_quantity
                FROM products_detail as pd 
                LEFT JOIN invoice_detail AS ivd ON ivd.product_id=pd.product_id 
                LEFT JOIN invoice AS iv ON iv.invoice_id=ivd.invoice_id 
                WHERE iv.status= 4 OR iv.status= 5 OR iv.status= 6
                GROUP BY pd.product_id,pd.product_name,iv.branch_id
            ) AS a ON a.product_id = pd.product_id AND a.branch_id = br.branch_id

            LEFT JOIN (
                SELECT ivd.product_id, SUM(ivd.product_quantity) AS sum_sale_store_product_quantity
                FROM invoice AS iv
                LEFT JOIN invoice_detail AS ivd ON ivd.invoice_id=iv.invoice_id
                LEFT JOIN products_detail AS pd ON pd.product_id=ivd.product_id
                GROUP BY ivd.product_id
            ) AS b ON b.product_id=pd.product_id
            
            LEFT JOIN (
                SELECT st.product_id,SUM(st.branch_store_quantity) AS sum_deliver_branch
                FROM store_detail AS st
                GROUP BY st.product_id
            ) AS c ON c.product_id=pd.product_id
            
            

            ");

        if(count($listProducts)>0)
        {
            $response["success"] = $listProducts;
        }
        else
        {
            $response["warning"] = [Lang::get('messages.common_warning_empty_list')];
        }

        return response()->json($response);
    }
    //apiGetList
    protected function apiGetList2(Request $request){  

        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";
        $searchStr="";
        if($request->input('searchStr') != null){
            $searchStr = $request->input('searchStr');
        }

        $listProducts = DB::select("
            select DISTINCT 
                q.product_id,
                q.product_name,
                q.product_thumbnail,
                q.store_quantity,
                q.sum_deliver_branch_store_quantity, 
                q.sum_deliver_branch_remain
                

            ,CASE WHEN q3.sum_sale_product_quantity IS null THEN 0 
            ELSE q3.sum_sale_product_quantity END 
                AS sum_sale_product_quantity,

            CASE WHEN (q.store_quantity - IFNULL(q3.sum_sale_product_quantity, 0)) IS null 
            THEN 0 ELSE (q.store_quantity - IFNULL(q3.sum_sale_product_quantity, 0)) END 
                AS sum_sale_product_quantity_remain 

            FROM
            (
                select 
                pd.product_id,pd.product_name,pd.product_thumbnail,pd.store_quantity,
                SUM(st.branch_store_quantity) AS sum_deliver_branch_store_quantity,
                CASE WHEN (pd.store_quantity - IFNULL(SUM(st.branch_store_quantity), 0)) IS null THEN 0 ELSE (pd.store_quantity - IFNULL(SUM(st.branch_store_quantity), 0)) END 
                AS sum_deliver_branch_remain 
                FROM store_detail AS st
                INNER JOIN products_detail AS pd ON st.product_id=pd.product_id
                INNER JOIN branches_detail AS br ON br.branch_id=st.branch_id
                GROUP BY pd.product_id,pd.product_name,pd.product_thumbnail,pd.store_quantity
            ) as q
            LEFT JOIN
            (
                SELECT pd.product_id,pd.product_name,
                SUM(ivd.product_quantity) AS sum_sale_product_quantity
                FROM products_detail as pd 
                LEFT JOIN invoice_detail AS ivd ON ivd.product_id=pd.product_id 
                LEFT JOIN invoice AS iv ON iv.invoice_id=ivd.invoice_id 
                WHERE iv.status= 4 OR iv.status= 5 OR iv.status= 6 OR iv.status= 7
                GROUP BY pd.product_id,pd.product_name
            ) AS q3 ON q.product_id=q3.product_id
            WHERE q.product_name LIKE '%".$searchStr."%'
            ");
        

        // if($request->input('queryStr') == null){

        //     $response["error"] = "Lỗi: Không Tìm Thấy Mã Truy Vấn";
        //     return response()->json($response);
        // }
        // $listProducts = DB::select($request->input('queryStr'));

        if(count($listProducts)>0)
        {
            $response["success"] = $listProducts;
        }
        else
        {
            $response["warning"] = [Lang::get('messages.common_warning_empty_list')];
        }

        return response()->json($response);
    }
    // 
    protected function apiGetStoreByBranch(Request $request){  

        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";
        $listBranch = DB::table('branches_detail as br')
                ->select(
                    'br.branch_id',
                    'br.branch_code',
                    'br.branch_name',
                    'br.branch_address_number',
                    'br.branch_phone',
                    'br.created_date',
                    DB::raw("DATE_FORMAT(br.created_date,'%d/%m/%Y') AS created_date_format"),
                    'br.created_user',
                    'br.updated_date',
                    'br.updated_user',
                    'br.status'
                    )
                ->get();

        $AllStore = DB::select("
            select a.product_id,a.branch_id,a.branch_store_quantity,
            CASE WHEN b.sum_sale_product_quantity IS null THEN 0 ELSE b.sum_sale_product_quantity END AS sum_sale_branch_quantity
            FROM
            (
                select pd.product_id,branch_store_quantity,b.branch_id
                FROM products_detail AS pd 
                LEFT JOIN
                (
                    select 
                    pd.product_id,st.branch_store_quantity,st.branch_id
                    FROM store_detail AS st
                    INNER JOIN products_detail AS pd ON st.product_id=pd.product_id
                ) as b 
                ON pd.product_id=b.product_id
            ) AS a
            LEFT JOIN 
            (
                SELECT pd.product_id, a.branch_id,a.sum_sale_product_quantity,
                CASE WHEN a.sum_sale_product_quantity IS null THEN 0 ELSE a.sum_sale_product_quantity END AS sum_sale_branch_quantity
                FROM
                products_detail AS pd
                LEFT JOIN
                (
                    SELECT pd.product_id,pd.product_name,iv.branch_id,
                    SUM(ivd.product_quantity) AS sum_sale_product_quantity
                    FROM products_detail as pd 
                    LEFT JOIN invoice_detail AS ivd ON ivd.product_id=pd.product_id 
                    LEFT JOIN invoice AS iv ON iv.invoice_id=ivd.invoice_id 
                    WHERE (iv.status= 4 OR iv.status= 5 OR iv.status= 6 OR iv.status= 7)
                    GROUP BY pd.product_id,pd.product_name,iv.branch_id
                 ) as a ON pd.product_id=a.product_id
            ) AS b ON a.branch_id=b.branch_id and  a.product_id=b.product_id
            ");

        if(!$listBranch->isEmpty())
        {
            $branch_Items=[];
            foreach ($listBranch as $branch) {
                $branch_Item = [
                "branch_id" => $branch->branch_id,
                "branch_name" => $branch->branch_name,
                "store_items" => array()
                ];
                foreach ($AllStore as $store) {
                    if($branch->branch_id==$store->branch_id){
                        $store_Item = [
                        "product_id" => $store->product_id,
                        "branch_store_quantity" => (int)$store->branch_store_quantity,
                        "sum_sale_branch_quantity" => (int)$store->sum_sale_branch_quantity
                        ];
                        array_push($branch_Item["store_items"], $store_Item); 
                    }
                }
                array_push($branch_Items, $branch_Item);
            }

        }
        
        
        if(count($branch_Items) > 0)
        {
            $response["success"] = $branch_Items;
        }
        else
        {
            $response["warning"] = [Lang::get('messages.common_warning_empty_list')];
        }

        return response()->json($response);
    }
    // insert in to store
    protected function apiInnitStore(Request $request){  

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $products_detail = DB::table('products_detail')->get();
        $branches_detail = DB::table('branches_detail')->get();

        DB::beginTransaction();
        try 
        {   
            foreach ($products_detail as $products_item) {
                foreach ($branches_detail as $branches_item) {
                    $existStoreDetail=DB::table('store_detail')
                    ->where([
                        ['branch_id', '=', $branches_item->branch_id],
                        ['product_id', '=', $products_item->product_id]
                        ])
                    ->get();
                    // neu chau co thi insert moi
                    if($existStoreDetail->isEmpty()){
                        $StoreDetail = new StoreDetail([
                            'branch_id' => $branches_item->branch_id,
                            'product_id' => $products_item->product_id,
                            'branch_store_quantity' => 0
                        ]);
                        $StoreDetail->save();
                    }
                }
            }
            $response["success"] = "Khởi Tạo Danh Sách Kho Thành Công";
        }
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = "Khởi Tạo Danh Sách Kho Thất Bại";
            return response()->json($response);
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = "Khởi Tạo Danh Sách Kho Thất Bại";
            return response()->json($response);
        }

        DB::commit();

        return response()->json($response);
    } 
    //apiUpdateStoreSumQuan
    protected function apiUpdateStoreSumQuan(Request $request){  

        // //Kiểm tra phân quyền sử dụng
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }
        // $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        if($request->input('product_id') == null){

            $response["error"] = "Lỗi: Không Tìm Thấy Mã Sản Phẩm";
            return response()->json($response);
        }

        DB::beginTransaction();
        try 
        {
            // DB::table('store_detail')
            // ->where([
            //     ['product_id','=',$request->input('product_id')],
            //     ['branch_id','=',$request->input('branch_id')]
            //     ])
            // ->update(['branch_store_quantity' => $request->input('branch_store_quantity')]);
            DB::table('products_detail')
            ->where([
                ['product_id','=',$request->input('product_id')]
                ])
            ->update(['store_quantity' => $request->input('store_quantity')]);
            $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_updated'), ['Tổng Kho']);
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }

        DB::commit();

        return response()->json($response);
    } 
    //apiUpdateStoreQuan
    protected function apiUpdateStoreQuan(Request $request){  

        // //Kiểm tra phân quyền sử dụng
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }
        // $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        if($request->input('product_id') == null){

            $response["error"] = "Lỗi: Không Tìm Thấy Mã Sản Phẩm";
            return response()->json($response);
        }

        if($request->input('branch_id') == null){

            $response["error"] = "Lỗi: Không Tìm Thấy Mã Chi Nhánh";
            return response()->json($response);
        }

        DB::beginTransaction();
        try 
        {
            DB::table('store_detail')
            ->where([
                ['product_id','=',$request->input('product_id')],
                ['branch_id','=',$request->input('branch_id')]
                ])
            ->update(['branch_store_quantity' => $request->input('branch_store_quantity')]);
           
            $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_updated'), ['Tổng Kho Chi Nhánh']);
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }

        DB::commit();

        return response()->json($response);
    } 
    // 
    protected function apiGetRemainAndSaleByBranch(Request $request){  

        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        if(Session::get('userAuth2')==null){
            $response["error"] = "Hết Phiên Làm Việc, Vui Lòng Đăng Nhập";
            return response()->json($response);
        }
        if(Session::get('userAuth2')->branch_id==null){
            $response["error"] = "User Chưa Thuộc Chi Nhánh Nào ! Vui Lòng Liên Hệ Quản Trị";
            return response()->json($response);
        }
        $listProducts = DB::select("
            select pd.product_id,st.branch_store_quantity,pd.product_thumbnail,pd.product_name,
                CASE WHEN a.sum_sale_product_quantity IS null THEN 0 ELSE a.sum_sale_product_quantity END AS sum_sale_branch_quantity
                FROM
                products_detail AS pd
                LEFT JOIN
                (
                    SELECT pd.product_id,pd.product_name,iv.branch_id,
                    SUM(ivd.product_quantity) AS sum_sale_product_quantity
                    FROM products_detail as pd 
                    LEFT JOIN invoice_detail AS ivd ON ivd.product_id=pd.product_id 
                    LEFT JOIN invoice AS iv ON iv.invoice_id=ivd.invoice_id 
                    WHERE (iv.status= 4 OR iv.status= 5 OR iv.status= 6)
                    AND iv.branch_id=".Session::get('userAuth2')->branch_id."
                    GROUP BY pd.product_id,pd.product_name,iv.branch_id
                ) as a 
                ON pd.product_id=a.product_id
                LEFT JOIN store_detail AS st ON st.product_id=pd.product_id AND st.branch_id=".Session::get('userAuth2')->branch_id."
            
            ");

        if(count($listProducts)>0)
        {
            $response["success"] = $listProducts;
        }
        else
        {
            $response["warning"] = [Lang::get('messages.common_warning_empty_list')];
        }

        return response()->json($response);
    }
}
