<?php

namespace App\Http\Controllers\SystemApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use DateTime;
use Validator;
use Mail;
use Lang;

use App\Http\Controllers\SystemApi\SystemCommonController as commonCtl;

use App\Models\Invoice;
use App\Models\InvoiceDetail;
use App\Models\ProductsDetail;

class SystemApiInvoiceController extends Controller
{

    /*** Khởi Tạo Giá Trị ***/
    private $commonCtl;

    private $sessionUser;

    
    /*!! Khởi Tạo Giá Trị !!*/

    public function __construct(commonCtl $SystemCommonController){
        $this->commonCtl =  $SystemCommonController;
    }

    /*** API Hàm Chức Năng ***/  
    // apiUpdateInvoiceStatus
    protected function apiUpdateInvoiceStatus(){  

        $listInvoice = DB::table('invoice AS in')
                ->select( 
                    'in.invoice_id',
                    'in.status',
                    DB::raw("DATEDIFF(NOW(),in.created_date) AS invoice_datediff")
                         )
                ->get();
        // cập nhật lại trạng thái nếu chưa thanh toán
        DB::beginTransaction();
        
        try {
            if(!$listInvoice->isEmpty())
            {
                foreach ($listInvoice as $item) {
                    // nếu sau 48 tiếng Nhận Hàng (status=4) mà ko thanh toán (status=5)
                    // thi chuyen sang status = 6 (cảnh báo)
                    if($item->invoice_datediff >= 2 && $item->status==4)
                    {
                        $updatedRows = Invoice::where('invoice_id', '=', $item->invoice_id)
                            ->update([
                                'status' => 6,
                                'updated_date' => $this->commonCtl->getCarbonNow()
                            ]);
                    }
                    // nếu sau 48 tiếng từ lúc Đang Xử Lý (status=3) mà ko đến Nhận hàng (status=4)
                    // thi chuyen sang status = 1 (hủy)
                    if($item->invoice_datediff >= 2 && $item->status==3)
                    {
                        $updatedRows = Invoice::where('invoice_id', '=', $item->invoice_id)
                            ->update([
                                'status' => 1,
                                'updated_date' => $this->commonCtl->getCarbonNow()
                            ]);
                    }
                }
            }
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            return "no";
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            return "no";
        }

        DB::commit();
        return "yes";
    }
    // apiGetCustomerList
    protected function apiGetList(Request $request){  

        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";
        $this->apiUpdateInvoiceStatus();

        $whereFunctions = array();
        if($request->input('branch_id')!=null){
            $whereFunctions = array(['br.branch_id','=',$request->input('branch_id')]);
        }

        $listInvoice = DB::table('invoice AS in')
                ->leftJoin('customers_detail AS cu', 'in.customer_id', '=', 'cu.customer_id')
                ->leftJoin('branches_detail AS br', 'br.branch_id', '=', 'in.branch_id')
                ->select( 
                    'in.invoice_id',
                    'in.invoice_number',
                    'in.invoice_id',
                    'in.receiver_name',
                    'in.receiver_phone',
                    'in.receiver_address',
                    'in.total_sum',
                    'in.branch_id',
                    'in.delivery_date',
                    'in.invoice_note',
                    'in.export_invoice_status',
                    'in.payment_method',
                    'in.status',
                    'in.created_date',
                    DB::raw("DATE_FORMAT(in.created_date,'%d/%m/%Y') AS created_date_format"),
                    'cu.customer_id',
                    'cu.customer_fullname',
                    'br.branch_id',
                    'br.branch_name'
                         )
                ->where($whereFunctions)
                ->orderBy('in.updated_date', 'desc')
                ->get();

        if(!$listInvoice->isEmpty())
        {
            $response["success"] = $listInvoice;
        }
        else
        {
            $response["warning"] = Lang::get('messages.common_warning_empty_list');
        }

        return response()->json($response);
    }
    /*------------------------------API Add invoice-----------------------*/
    protected function apiAdd(Request $request){   

        // $this->sessionUser = Session::get('userAuth'); 

        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $itemInvoice = new Invoice;

        $itemInvoice->fill($request->all());
        $itemInvoice->delivery_date = $this->commonCtl->formatCarbonDatetime($request->input('delivery_date'));
        $itemInvoice->created_user = Session::get('userAuth')->user_id; 
        $itemInvoice->created_date = $this->commonCtl->getCarbonNow();
        $itemInvoice->updated_user = Session::get('userAuth')->user_id; 
        $itemInvoice->updated_date = $this->commonCtl->getCarbonNow();

        DB::beginTransaction();

        try {

            $itemInvoice->save();

            if(count($request->input('list_product')) > 0)
            {
                foreach ($request->input('list_product') as $itemInvoiceDetail) {
                    $itemInvoiceDetail = new InvoiceDetail([
                        'invoice_id' => $itemInvoice->invoice_id,
                        'product_id' => $itemInvoiceDetail['product_id'],
                        'product_quantity' => $itemInvoiceDetail['quantity'],
                        'product_retail_prices' => $itemInvoiceDetail['product_retail_prices'],
                        'total_price' => $itemInvoiceDetail['total_price'],
                    ]);
                    $itemInvoiceDetail->save();
                }
            }

            $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_created'), ['Đơn Hàng']);
            
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response).$e;
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response).$e;
        }

        DB::commit();

        return response()->json($response);
    }
    /*------------------------------API Get  detail-----------------------*/
    protected function apiGetDetail(Request $request){  
        
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $whereFunctions = array(
            ['in.invoice_id', '=', $request->input('invoice_id')]
        );

        $listInvoice = DB::table('invoice AS in')
                ->leftJoin('customers_detail AS cu', 'in.customer_id', '=', 'cu.customer_id')
                ->leftJoin('system_areas_districts AS sy', 'cu.area_id', '=', 'sy.district_id')
                ->leftJoin('branches_detail AS br', 'br.branch_id', '=', 'in.branch_id')
                ->select( 
                    'in.invoice_id',
                    'in.invoice_number',
                    'in.receiver_name',
                    'in.receiver_phone',
                    'in.receiver_address',
                    'in.total_sum',
                    'in.delivery_date',
                    'in.invoice_note',
                    'in.export_invoice_status',
                    'in.payment_method',
                    'in.status',
                    'in.created_date',
                    'br.branch_id',
                    'br.branch_name',
                    'br.branch_address_number',
                    'br.branch_phone',
                    DB::raw("DATE_FORMAT(in.updated_date,'%d/%m/%Y') AS updated_date_format"),
                    'cu.customer_id',
                    'cu.customer_fullname',
                    'cu.customer_account',
                    'cu.customer_email',
                    'cu.customer_address',
                    'cu.area_id',
                    'cu.customer_phone',

                    'cu.customer_identify',
                    'cu.customer_student_card',
                    'cu.customer_university',
                    
                    'sy.district_name'
                         )
                ->where($whereFunctions)
                ->first();
        //check null > error
        if($listInvoice !== null)
        {
            $response["success"] = $listInvoice;
            //$response["product_list"] = [];
        }
        else
        {
            $response["warning"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' Đơn Hàng']);
        }
 
        // =============================Invoice detail==================
        
        // $listInvoiceDetail = DB::table('invoice_detail AS in')
        //         ->leftJoin('products_detail AS pro', 'in.product_id', '=', 'pro.product_id')
        //         ->leftJoin('products_units_detail AS un', 'pro.unit_id', '=', 'un.unit_id')
        //         ->leftJoin('products_categories_detail AS ca', 'pro.cate_id', '=', 'ca.cate_id')
        //         ->leftJoin('products_origins_detail AS or', 'pro.origin_id', '=', 'or.origin_id')
        //         ->leftJoin('products_manufacturers_detail AS ma', 'pro.manufacturer_id', '=', 'ma.manufacturer_id')
        //         ->leftJoin('products_colors_detail AS co', 'pro.color_id', '=', 'co.color_id')
        //         ->select( 
        //             'in.invoice_id', 
        //             //phai theo thứ tự (product_id > quantity >product_retail_prices)
        //             'pro.product_id',
        //             'in.product_quantity as quantity',
        //             'in.product_retail_prices',
        //             'in.total_price',
        //             'pro.product_slug',
        //             'pro.unit_id',
        //             'pro.cate_id',
        //             'pro.origin_id',
        //             'pro.manufacturer_id',
        //             'pro.color_id',
        //             'pro.product_name',
        //             'pro.product_description',
        //             'pro.product_specifications',
        //             'pro.product_imported_prices',
        //             'pro.product_retail_prices',
        //             'pro.product_deals',
        //             'pro.product_ratings',
        //             'pro.product_thumbnail',
        //             'pro.product_images_list',
        //             'pro.product_release_date',
        //             'pro.product_expiration_date',
        //             'pro.product_comment_status',
        //             'pro.news_seo_title',
        //             'pro.news_seo_description',
        //             'pro.news_seo_keywords',
        //             'pro.status',
                    
        //             'un.unit_name',
        //             'ca.cate_name',
        //             'or.origin_name',
        //             'ma.manufacturer_name',
        //             'co.color_name'
        //                  )
        //         ->where($whereFunctions)
        //         ->get();
        $listInvoiceDetail=DB::select("
            select ivd.invoice_id,ivd.product_id,ivd.product_quantity as quantity,ivd.product_retail_prices,ivd.total_price,
            pd.product_slug,pd.unit_id,pd.product_name,pd.product_imported_prices,pd.product_retail_prices,pd.product_thumbnail,pd.status,
            un.unit_name,
            iv.branch_id,iv.invoice_id,st.branch_store_quantity,
            a.sum_sale_product_quantity,st.branch_store_quantity-a.sum_sale_product_quantity AS branch_store_quantity_remain
            FROM invoice_detail AS ivd
            LEFT JOIN invoice AS iv ON iv.invoice_id=ivd.invoice_id
            LEFT JOIN products_detail AS pd ON pd.product_id=ivd.product_id
            LEFT JOIN products_units_detail AS un ON un.unit_id=pd.unit_id
            LEFT JOIN store_detail AS st ON st.branch_id=iv.branch_id AND st.product_id=ivd.product_id
            LEFT JOIN 
            (
                SELECT pd.product_id,iv.branch_id,SUM(ivd.product_quantity) AS sum_sale_product_quantity
                FROM products_detail as pd 
                LEFT JOIN invoice_detail AS ivd ON ivd.product_id=pd.product_id 
                LEFT JOIN invoice AS iv ON iv.invoice_id=ivd.invoice_id 
                WHERE iv.status= 4 OR iv.status= 5 OR iv.status= 6
                GROUP BY pd.product_id,pd.product_name,iv.branch_id
            ) AS a ON a.branch_id=iv.branch_id AND a.product_id=ivd.product_id
            WHERE iv.invoice_id=".$request->input('invoice_id')."

        ");
        //check null > error
        if($listInvoiceDetail !== null)
        {
            $response["success"] = $listInvoice;
            $response["product_list"] = $listInvoiceDetail;
        }
        else
        {
            $response["product_list"] =[];
        }     
        // return 
        return response()->json($response);
    }
    /*------------------------------API Update -----------------------*/
    protected function apiUpdate(Request $request){   

        // $this->sessionUser = Session::get('userAuth'); 
        
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        //Kiêm tra San Pham tồn tại (thông qua slug)
        $currentInvoice = Invoice::where('invoice_id','=',$request->input('invoice_id'))->first();

        if($currentInvoice == null)
        {
            $response["warning"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' Đơn Hàng']);
            return response()->json($response);
        }
        // update detail
        $currentInvoice->customer_id = $request->input('customer_id');
        $currentInvoice->receiver_name = $request->input('receiver_name');
        $currentInvoice->receiver_phone = $request->input('receiver_phone');
        $currentInvoice->receiver_address = $request->input('receiver_address');
        $currentInvoice->total_sum = $request->input('total_sum');
        $currentInvoice->delivery_date = $this->commonCtl->formatCarbonDatetime($request->input('delivery_date'));
        $currentInvoice->invoice_note = $request->input('invoice_note');
        $currentInvoice->export_invoice_status = $request->input('export_invoice_status');
        $currentInvoice->payment_method = $request->input('payment_method');
        $currentInvoice->status = $request->input('status');

        $currentInvoice->updated_user = Session::get('userAuth')->user_id;
        $currentInvoice->updated_date = $this->commonCtl->getCarbonNow();

        DB::beginTransaction();

        try {
            // delete old invoice detail
            $deletedRowsInvoiceDetail = InvoiceDetail::where('invoice_id', '=', $currentInvoice->invoice_id)->delete();
            // update customer
            $currentInvoice->save();
            //insert new invoice detail
            if(count($request->input('list_product')) > 0)
            {
                foreach ($request->input('list_product') as $itemInvoiceDetail) {
                    $itemInvoiceDetail = new InvoiceDetail([
                        'invoice_id' => $currentInvoice->invoice_id,
                        'product_id' => $itemInvoiceDetail['product_id'],
                        'product_quantity' => $itemInvoiceDetail['quantity'],
                        'product_retail_prices' => $itemInvoiceDetail['product_retail_prices'],
                        'total_price' => $itemInvoiceDetail['total_price'],
                    ]);
                    $itemInvoiceDetail->save();
                }
            }
            // return success message
            $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_updated'), ['Đơn Hàng']);
            
        } 
        catch(ValidationException $e)
        {
            DB::rollback();

            // $response["error"] = $e->getErrors();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response).$e;
        }

        DB::commit();

        return response()->json($response);
    }
    /*------------------------------API remove -----------------------*/
    protected function apiRemove(Request $request){ 

        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";
        // check ton tai to delete
        $itemDelete = Invoice::where('invoice_id', '=', $request->input('invoice_id'))->first();

        if($itemDelete == null)
        {
            $response["error"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' Đơn Hàng']);
            return response()->json($response);
        }

        // Start transaction!
        DB::beginTransaction();

        try {
            $deletedRowsInvoiceDetail = InvoiceDetail::where('invoice_id', '=', $itemDelete->invoice_id)->delete();
            $deletedRows = Invoice::where('invoice_id', '=', $itemDelete->invoice_id)->delete();

            if($deletedRows > 0){
                $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_removed'), ['Đơn Hàng']);
            }
            else{
                $response["warning"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_removed'), ['Đơn Hàng']);
                DB::rollback();
            }
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }

        DB::commit();

        return response()->json($response);
    }
    // --------------apiChangeStatus----------------
    protected function apiChangeStatus(Request $request){   

        // $this->sessionUser = Session::get('userAuth'); 

        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $itemUpdate = Invoice::where('invoice_id', '=', $request->input('invoice_id'))->first();

        if($itemUpdate == null)
        {
            $response["error"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), ['Đơn Hàng']);
            return response()->json($response);
        }

        // Start transaction!
        DB::beginTransaction();

        try {
            $updatedRows = Invoice::where('invoice_id', '=', $itemUpdate->invoice_id)
                            ->update([
                                'status' => $request->input('status'),
                                'updated_date' => $this->commonCtl->getCarbonNow(),
                                'updated_user' => Session::get('userAuth')->user_id
                            ]);
            // nếu status=7 thì giảm 1 % vào tổng tiền của Hóa đơn
            if($request->input('status')==7){
                $reduce1percent=$itemUpdate->total_sum*99/100;

                $updatedReduce1Percent = Invoice::where('invoice_id', '=', $itemUpdate->invoice_id)
                            ->update([
                                'total_sum' => $reduce1percent
                            ]);
            }

            if($updatedRows > 0 ){
                $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_updated'), ['Đơn Hàng']);
            }
            else{
                $response["warning"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_updated'), ['Đơn Hàng']);
                DB::rollback();
            }
            
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response).$e;
        }
        DB::commit();

        return response()->json($response);
    }
    // apiGetProductByBranch
    protected function apiGetProductByBranch(Request $request){  
        
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        if($request->input('branch_id')==null)
        {
            // $response["warning"] = "Không Tìm Thấy Chi Nhánh";
            // return response()->json($response);
            $branch_id = Session::get('userAuth2')->branch_id;
        }
        else
        {
            $branch_id = $request->input('branch_id');
        }
        $listItem=DB::select("
            select pd.product_id,st.branch_id,pd.product_slug,pd.product_name,pd.product_imported_prices,pd.product_retail_prices,pd.product_thumbnail,pd.status,
            st.branch_store_quantity,ca.cate_name,
            CASE WHEN a.sum_sale_product_quantity IS null THEN 0 ELSE a.sum_sale_product_quantity END AS sum_sale_product_quantity,
            st.branch_store_quantity-(CASE WHEN a.sum_sale_product_quantity IS null THEN 0 ELSE a.sum_sale_product_quantity END) AS branch_store_quantity_remain
            FROM products_detail AS pd
            LEFT JOIN products_categories_detail AS ca ON ca.cate_id=pd.cate_id
            LEFT JOIN store_detail AS st ON st.product_id=pd.product_id AND st.branch_id=".$branch_id."
            LEFT JOIN (
                SELECT pd.product_id,iv.branch_id,SUM(ivd.product_quantity) AS sum_sale_product_quantity
                FROM products_detail as pd 
                LEFT JOIN invoice_detail AS ivd ON ivd.product_id=pd.product_id 
                LEFT JOIN invoice AS iv ON iv.invoice_id=ivd.invoice_id 
                WHERE iv.status= 4 OR iv.status= 5 OR iv.status= 6
                GROUP BY pd.product_id,pd.product_name,iv.branch_id
            ) AS a ON a.branch_id=".$branch_id." AND a.product_id=pd.product_id

        ");
        //check null > error
        if($listItem !== null)
        {
            $response["success"] = $listItem;
        }
        else
        {
            $response["warning"] = Lang::get('messages.common_warning_empty_list');
        }     
        // return 
        return response()->json($response);
    }
}
