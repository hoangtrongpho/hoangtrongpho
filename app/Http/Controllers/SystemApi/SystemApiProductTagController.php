<?php

namespace App\Http\Controllers\SystemApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use DateTime;
use Validator;
use Mail;
use Lang;

use App\Http\Controllers\SystemApi\SystemCommonController as commonCtl;
use App\Models\ProductsTagsDetail;
use App\Models\ProductsDetail;

class SystemApiProductTagController extends Controller
{

    /*** Khởi Tạo Giá Trị ***/
    private $commonCtl;

    private $sessionUser;

    private $rulesMess = [
        'tag_id.required' => 'Vui Lòng Thêm ID Thẻ Sản Phẩm.',
        'tag_slug.required' => 'Vui Lòng Thêm Mã Thẻ Sản Phẩm.',
        'tag_name.required' => 'Vui Lòng Thêm Tên Thẻ Sản Phẩm.',
    ];

    /*** Khởi Tạo Giá Trị ***/
    public function __construct(commonCtl $SystemCommonController){
        $this->commonCtl =  $SystemCommonController;
    }

    //Ajax Post Lấy Danh Sách 
    protected function apiGetList(Request $request){  
        
        $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        if($auth["auth"])
        {
            return response()->json($auth);
        }
        $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $listItem = DB::table('products_tags_detail as td')
                ->leftJoin('products_tags_list AS tl', 'td.tag_id', '=', 'tl.tag_id')
                ->select('td.tag_id', 
                        'td.tag_slug',
                        'td.tag_name',
                        'td.tag_description',
                        DB::raw('DATE_FORMAT(td.updated_date,"%d/%m/%Y %H:%m:%s") AS updated_date'),
                        'td.status',
                        DB::raw('count(tl.tag_id) AS item_couter'))
                ->where('td.status','<>',0)
                ->groupBy('td.tag_id', 
                        'td.tag_slug',
                        'td.tag_name',
                        'td.tag_description',
                        'td.updated_date',
                        'td.status')
                ->get();

        if(!$listItem->isEmpty())
        {
            $response["success"] = $listItem;
        }
        else
        {
            $response["warning"] = Lang::get('messages.common_warning_empty_list');
        }

        return response()->json($response);
    } 

    //Ajax Post Thêm
    protected function apiAdd(Request $request){  
        
        //Kiểm tra phân quyền sử dụng
        $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        if($auth["auth"])
        {
            return response()->json($auth);
        }
        $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        /** Kiểm tra giá trị nhập vào **/
        $rules = [
            'tag_name' => 'required',
        ];

        $valiData = $this->commonCtl->validRequest($request,$rules,$this->rulesMess);

        if($valiData->fails())
        {   
            $response["warning"] = $valiData->errors();
            return response()->json($response);
        }
       

        /** Khởi tạo giá trị **/
        $tag_slug = $this->commonCtl->slugify($request->input('tag_name'));
        if(empty($tag_slug))
        {
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }

        $existsItem = ProductsTagsDetail::where('tag_slug','=',$tag_slug)
                        ->first();
        if(!empty($existsItem))
        {
            $response["warning"] = [$this->commonCtl->replaceTitle(Lang::get('messages.common_error_exist_data'), ['Thẻ', '"'.$request->input('tag_name').'"'])];
            return response()->json($response);
        }                

        $itemAdd = new ProductsTagsDetail;

        $itemAdd->fill($request->all());
        $itemAdd->tag_slug = $tag_slug;
        $itemAdd->created_user = $this->sessionUser->user_id;
        $itemAdd->created_date = $this->commonCtl->getCarbonNow();
        $itemAdd->updated_user = $this->sessionUser->user_id;
        $itemAdd->updated_date = $this->commonCtl->getCarbonNow();
        $itemAdd->status = 1;

        DB::beginTransaction();

        try {
            $itemAdd->save();

            $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_created'), ['thẻ']);
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);

            // return Redirect::to('/form')
            // ->withErrors( $e->getErrors() )
            // ->withInput();
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
            // throw $e;
        }
        DB::commit();

        return response()->json($response);
    } 

    //Ajax Post Thêm
    protected function apiUpdate(Request $request){  

        //Kiểm tra phân quyền sử dụng
        $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        if($auth["auth"])
        {
            return response()->json($auth);
        }
        $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $rules = [
            'tag_id' => 'required',
            'tag_slug' => 'required',
            'tag_name' => 'required',
        ];

        $valiData = $this->commonCtl->validRequest($request,$rules,$this->rulesMess);

        /** Kiểm tra giá trị nhập vào **/
        if($valiData->fails())
        {   
            $response["warning"] = $valiData->errors();
            return response()->json($response);
        }

        $existsItem = ProductsTagsDetail::where('tag_slug','=',$request->input('tag_slug'))
                        ->first();

        if(empty($existsItem))
        {
            $response["warning"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' thẻ']);
            return response()->json($response);
        }

        $tag_slug = $this->commonCtl->slugify($request->input('tag_name'));
        if(empty($tag_slug))
        {
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }

        $existsSlug = ProductsTagsDetail::where([
                                    ['tag_slug','=',$tag_slug],
                                    ['tag_id','<>',$existsItem->tag_id],
                                ])->first();

        if(!empty($existsSlug))
        {
            $response["warning"] = [$this->commonCtl->replaceTitle(Lang::get('messages.common_error_exist_data'), ['Tên', 'thẻ'])];
            return response()->json($response);
        }

        $existsItem->tag_slug = $tag_slug;
        $existsItem->tag_name = $request->input('tag_name');
        $existsItem->tag_description = $request->input('tag_description');
        $existsItem->updated_user = $this->sessionUser->user_id;
        $existsItem->updated_date = $this->commonCtl->getCarbonNow();

        DB::beginTransaction();
        try 
        {
            $existsItem->save();

            $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_updated'), ['thẻ']);
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);

            // return Redirect::to('/form')
            // ->withErrors( $e->getErrors() )
            // ->withInput();
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
            // throw $e;
        }

        DB::commit();

        return response()->json($response);
    } 

    //Ajax Post Xóa Thẻ
    protected function apiRemove(Request $request){ 

        //Kiểm tra phân quyền sử dụng
        $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        if($auth["auth"])
        {
            return response()->json($auth);
        }
        $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        /** Kiểm tra giá trị nhập vào **/
        $rules = [
            'tag_id' => 'required',
        ];

        $valiData = $this->commonCtl->validRequest($request,$rules,$this->rulesMess);

        if($valiData->fails())
        {   
            $response["warning"] = $valiData->errors();
            return response()->json($response);
        }

        $itemDelete = ProductsTagsDetail::where('tag_id', '=', $request->input('tag_id'))->first();

        if(empty($itemDelete))
        {
            $response["error"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' thẻ']);
            return response()->json($response);
        }

        $checkExists = DB::table('products_tags_list')
                        ->select('tag_id')
                        ->where('tag_id','=',$request->input('tag_id'))
                        ->get();

        DB::beginTransaction();
        try {

            if(!$checkExists->isEmpty())
            {
                $itemDelete->updated_user = $this->sessionUser->user_id;
                $itemDelete->updated_date = $this->commonCtl->getCarbonNow();
                $itemDelete->status = 0;

                $itemDelete->save();

                $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_removed'), ['thẻ']);
            }
            else
            {
                $deletedRows = ProductsTagsDetail::where('tag_id', '=', $itemDelete->tag_id)->delete();

                if($deletedRows > 0){
                    $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_removed'), ['thẻ']);
                }
                else{
                    $response["warning"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_removed'), ['thẻ']);
                }
            }
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);

            // return Redirect::to('/form')
            // ->withErrors( $e->getErrors() )
            // ->withInput();
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
            // throw $e;
        }
        DB::commit();

        return response()->json($response);
    }
}
