<?php

namespace App\Http\Controllers\SystemApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use DateTime;
use Validator;
use Mail;
use Lang;

use App\Http\Controllers\SystemApi\SystemCommonController as commonCtl;

use App\Models\CustomersDetail;
use App\Models\CustomersGroup;

class SystemApiCustomerController extends Controller
{

    /*** Khởi Tạo Giá Trị ***/
    private $commonCtl;

    private $sessionUser;

    private $rulesMess = [
        'customer_id.required' => 'Vui Lòng Chọn Mã Nhân Viên'
    ];
    /*!! Khởi Tạo Giá Trị !!*/

    public function __construct(commonCtl $SystemCommonController){
        $this->commonCtl =  $SystemCommonController;
    }
    // apiUpdateCustomerStatus
    protected function apiUpdateCustomerStatus(){  

        $whereFunctions = array();

        $listCustomers = DB::table('customers_detail AS cu')
                ->select( 'cu.group_id',
                    'cu.customer_id',
                    'cu.customer_account',
                    'cu.customer_email',
                    'cu.customer_fullname',
                    'cu.customer_address',
                    'cu.area_id',
                    'cu.customer_phone',
                    'cu.customer_birthday',
                    'cu.customer_password',
                    'cu.customer_login_count',
                    'cu.customer_secure_code',
                    'cu.customer_avatar',
                    'cu.customer_identify',
                    'cu.customer_student_card',
                    'cu.customer_university',
                    DB::raw("DATEDIFF(NOW(),cu.created_date) AS customer_datediff"),
                    'cu.created_date',
                    'cu.created_user',
                    'cu.updated_date',
                    'cu.updated_user',
                    'cu.status'
                         )
                ->where($whereFunctions)
                ->orderBy('cu.updated_date', 'desc')
                ->get();
        // cập nhật lại trạng thái nếu chưa thanh toán
        DB::beginTransaction();
        
        try {
            if(!$listCustomers->isEmpty())
            {
                foreach ($listCustomers as $item) {
                    if($item->customer_datediff >= 2 && $item->status==1)
                    {
                        $updatedRows = CustomersDetail::where('customer_id', '=', $item->customer_id)
                            ->update([
                                'status' => 3,
                                'updated_date' => $this->commonCtl->getCarbonNow()
                            ]);
                    }
                }
            }
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            return "no";
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            return "no";
        }

        DB::commit();
        return "yes";
    }
    // apiGetCustomerList
    protected function apiGetList(Request $request){  

        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";
        $this->apiUpdateCustomerStatus();
        //  get list again to see upadated list
        $listCustomers = DB::table('customers_detail AS cu')
                ->select( 'cu.group_id',
                    'cu.customer_id',
                    'cu.customer_account',
                    'cu.customer_email',
                    'cu.customer_fullname',
                    'cu.customer_address',
                    'cu.area_id',
                    'cu.customer_phone',
                    'cu.customer_birthday',
                    'cu.customer_password',
                    'cu.customer_login_count',
                    'cu.customer_secure_code',
                    'cu.customer_avatar',
                    'cu.customer_identify',
                    'cu.customer_student_card',
                    'cu.customer_university',
                    DB::raw("DATEDIFF(NOW(),cu.created_date) AS customer_datediff"),
                    'cu.created_date',
                    'cu.created_user',
                    'cu.updated_date',
                    'cu.updated_user',
                    'cu.status'
                         )
                ->orderBy('cu.updated_date', 'desc')
                ->get();
        if(!$listCustomers->isEmpty())
        {
            $response["success"] = $listCustomers;
        }
        else
        {
            $response["warning"] = Lang::get('messages.common_warning_empty_list');
        }

        return response()->json($response);
    }
    // apiGetDetail
    protected function apiGetDetail(Request $request){  
        
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $whereFunctions = array(
            ['cu.customer_id', '=', $request->input('customer_id')]
        );

        $listCustomers = DB::table('customers_detail AS cu')
                ->leftJoin('customers_group AS gr', 'cu.group_id', '=', 'gr.group_id')
                ->leftJoin('system_areas_districts AS di', 'cu.area_id', '=', 'di.district_id')
                ->select( 'cu.group_id',
                    'cu.customer_id',
                    'cu.customer_account',
                    'cu.customer_email',
                    'cu.customer_fullname',
                    'cu.customer_address',
                    'cu.area_id',
                    'cu.customer_phone',
                    'cu.customer_birthday',

                    'cu.customer_avatar',
                    'cu.customer_identify',
                    'cu.customer_student_card',
                    'cu.customer_university',

                    'cu.customer_login_count',
                    'cu.customer_secure_code',
                    'cu.created_date',
                    'cu.created_user',
                    'cu.updated_date',
                    'cu.updated_user',
                    'cu.status',

                    'di.district_name',
                    'gr.group_name'
                         )
                ->where($whereFunctions)
                ->first();
   
        //check null > error
        if($listCustomers !== null)
        {
            $response["success"] = $listCustomers;
        }
        else
        {
            $response["warning"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' Khách Hàng']);
        }

        return response()->json($response);
    }
    // apiUpdate
    protected function apiUpdate(Request $request){   

        // $this->sessionUser = Session::get('userAuth'); 
        
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        //Kiêm tra San Pham tồn tại (thông qua slug)
        $currentCustomer = CustomersDetail::where('customer_id','=',$request->input('customer_id'))->first();

        if($currentCustomer === null)
        {
            $response["warning"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' Khách Hàng']);
            return response()->json($response);
        }
        // kiem Tra trùng CMND thì khong cho DK
        $existIdentify = DB::table('customers_detail')->where([
            ['customer_identify','<>', $currentCustomer->customer_identify],
            ['customer_identify','=', $request->input('customer_identify')]
            ])->first();
        if($existIdentify!=null){
            $response["warning"] = "Số CMND: ".$request->input('customer_identify')." đã tồn tại, hoặc đã bị khóa";
            return response()->json($response);
        }

        // update detail
        $currentCustomer->customer_id = $request->input('customer_id');
        $currentCustomer->customer_email = $request->input('customer_email');
        $currentCustomer->customer_fullname = $request->input('customer_fullname');
        $currentCustomer->customer_address = $request->input('customer_address');
        $currentCustomer->area_id = $request->input('area_id');
        $currentCustomer->group_id = $request->input('group_id');
        $currentCustomer->customer_phone = $request->input('customer_phone');

        $currentCustomer->customer_avatar = $request->input('customer_avatar');
        $currentCustomer->customer_identify = $request->input('customer_identify');
        $currentCustomer->customer_student_card = $request->input('customer_student_card');
        $currentCustomer->customer_university = $request->input('customer_university');

        $currentCustomer->updated_user = Session::get('userAuth')->user_id;
        $currentCustomer->updated_date = $this->commonCtl->getCarbonNow();

        DB::beginTransaction();

        try {
            // update customer
            $currentCustomer->save();

            // return success message
            $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_updated'), ['Khách Hàng']);
            
        } 
        catch(ValidationException $e)
        {
            DB::rollback();

            // $response["error"] = $e->getErrors();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response).$e;
        }

        DB::commit();

        return response()->json($response);
    }
    // apiRemove
    protected function apiRemove(Request $request){ 

        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";
        // check ton tai to delete
        $itemDelete = CustomersDetail::where('customer_id', '=', $request->input('customer_id'))->first();

        if($itemDelete == null)
        {
            $response["error"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' Khách Hàng']);
            return response()->json($response);
        }

        // Start transaction!
        DB::beginTransaction();

        try {
            $deletedRows = CustomersDetail::where('customer_id', '=', $itemDelete->customer_id)->delete();

            if($deletedRows > 0){
                $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_removed'), ['Khách Hàng']);
            }
            else{
                $response["warning"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_removed'), ['Khách Hàng']);
                DB::rollback();
            }
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }

        DB::commit();

        return response()->json($response);
    }
    // apiChangeStatus
    protected function apiChangeStatus(Request $request){   

        $this->sessionUser = Session::get('userAuth'); 

        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

       
        $itemUpdate = CustomersDetail::where('customer_id', '=', $request->input('customer_id'))->first();

        if($itemUpdate == null)
        {
            $response["error"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' Khách Hàng']);
            return response()->json($response);
        }

        // Start transaction!
        DB::beginTransaction();

        try {
            $updatedRows = CustomersDetail::where('customer_id', '=', $itemUpdate->customer_id)
                            ->update([
                                'status' => $request->input('status'),
                                'updated_date' => $this->commonCtl->getCarbonNow(),
                                'updated_user' => $this->sessionUser->user_id
                            ]);

            if($updatedRows > 0){
                $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_updated'), ['Khách Hàng']);
            }
            else{
                $response["warning"] = [$this->commonCtl->replaceTitle(Lang::get('messages.common_error_updated'), ['Khách Hàng'])];
                DB::rollback();
            }
            
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }
        DB::commit();

        return response()->json($response);
    }
}
