<?php

namespace App\Http\Controllers\SystemApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Validator;
use Lang;

use App\Http\Controllers\SystemApi\SystemCommonController as commonCtl;

use App\Models\ProductsCategoriesDetail;
use App\Models\ProductsDetail;

class SystemApiProductCategoriesController extends Controller
{

    /*** Khởi Tạo Giá Trị ***/
    private $commonCtl;

    private $sessionUser;

 

    /*** Khởi Tạo Giá Trị ***/
    public function __construct(commonCtl $SystemCommonController){
        $this->commonCtl =  $SystemCommonController;
    }

    protected function apiGetListForChosen(Request $request){

        $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        if($auth["auth"])
        {
            return response()->json($auth);
        }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $whereFunctions = array(
            ['cate_parent_id', '=', 0],
            ['status', '<>', 0]
        );

        $listCategories = DB::table('products_categories_detail')
                          ->select('cate_id', 
                                   'cate_parent_id',
                                   'cate_slug',
                                   'cate_name', 
                                   'cate_description',
                                   'cate_thumbnail',
                                   'cate_orders')
                          ->where($whereFunctions)
                          ->orderBy('cate_orders', 'asc')
                          ->get();

        if(count($listCategories) > 0)
        {
            $response["success"] = $listCategories;
        }
        else
        {
            $response["warning"] = Lang::get('messages.common_warning_empty_list');
        }

        return response()->json($response);
    }

    protected function apiGetList(Request $request){

        $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        if($auth["auth"])
        {
            return response()->json($auth);
        }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $listCategories = array();

        $whereFunctions = array(
            ['cate_parent_id', '=', 0],
            ['status', '<>', 0]
        );

        $listParent = DB::table('products_categories_detail')
                          ->select('cate_id', 
                                   'cate_parent_id',
                                   'cate_slug',
                                   'cate_name', 
                                   'cate_description',
                                   'cate_thumbnail',
                                   'cate_orders')
                          ->where($whereFunctions)
                          ->orderBy('cate_orders', 'asc')
                          ->get();

        $whereFunctions = array(
            ['cate_parent_id', '<>', 0],
            ['status', '<>', 0]
        );

        $listChild = DB::table('products_categories_detail')
                          ->select('cate_id', 
                                   'cate_parent_id',
                                   'cate_slug',
                                   'cate_name', 
                                   'cate_description',
                                   'cate_thumbnail',
                                   'cate_orders')
                          ->where($whereFunctions)
                          ->orderBy('cate_orders', 'asc')
                          ->get();

        foreach ($listParent as $value_p) {

            $itemParent = [
                "cate_id" => $value_p->cate_id,
                "cate_parent_id" => $value_p->cate_parent_id,
                "cate_slug" => $value_p->cate_slug,
                "cate_name" => $value_p->cate_name,
                "cate_description" => $value_p->cate_description,
                "cate_thumbnail" => $value_p->cate_thumbnail,
                "cate_orders" => $value_p->cate_orders,
                "nodes" => array()
            ];
            // foreach add con 1
            foreach ($listChild as $value_c) {
                // nếu cate_parent_id = cate_id
                if($value_c->cate_parent_id == $value_p->cate_id)
                {
                    $itemChild = [
                        "cate_id" => $value_c->cate_id,
                        "cate_parent_id" => $value_p->cate_id,
                        "cate_slug" => $value_c->cate_slug,
                        "cate_name" => $value_c->cate_name,
                        "cate_description" => $value_c->cate_description,
                        "cate_thumbnail" => $value_c->cate_thumbnail,
                        "cate_orders" => $value_c->cate_orders,
                        "nodes" => array()
                    ];
                    // add con 2
                    foreach ($listChild as $value_c2) {
                        if($value_c2->cate_parent_id == $value_c->cate_id){
                            $itemChild2 = [
                                "cate_id" => $value_c2->cate_id,
                                "cate_parent_id" => $value_c->cate_id,
                                "cate_slug" => $value_c2->cate_slug,
                                "cate_name" => $value_c2->cate_name,
                                "cate_description" => $value_c2->cate_description,
                                "cate_thumbnail" => $value_c2->cate_thumbnail,
                                "cate_orders" => $value_c2->cate_orders,
                                "nodes" => array()
                            ];
                            array_push($itemChild["nodes"], $itemChild2);  
                        }
                    }
                    // add con 1
                    array_push($itemParent["nodes"], $itemChild);
                }
            }
            //  add cha
            array_push($listCategories, $itemParent);
        }



        if(count($listCategories) > 0)
        {
            $response["success"] = $listCategories;
        }
        else
        {
            $response["warning"] = Lang::get('messages.common_warning_empty_list');
        }

        return response()->json($response);
    }
     // Ajax Post Thêm
    protected function apiAdd(Request $request){  

        //Kiểm tra phân quyền sử dụng
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }
        // $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

    
        $itemAdd = new ProductsCategoriesDetail;

        $cate_slug = $this->commonCtl->slugify($request->input('cate_name'));
        if(empty($cate_slug))
        {
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }

        $whereFunctions = array(
            ['cate_slug', '=', $cate_slug]
        );
        // nếu trùng slug
        $existItem = ProductsCategoriesDetail::where($whereFunctions)->first();
        // nếu có cate thì báo lỗi trùng
        if(!empty($existItem))
        {
            $response["warning"] = [$this->commonCtl->replaceTitle(Lang::get('messages.common_error_exist_data'), ['Nhóm', 'sản phẩm'])];
            return response()->json($response);
        }   
        

        
        // nếu có Cate có giá trị > tiến hành INSERT
        if(!empty($itemAdd))
        {
            // Start transaction!
            DB::beginTransaction();

            try {
                
                // map all feild
                $itemAdd->fill($request->all());
                // add new slug
                if($request->input('cate_parent_id') == 0)
                {
                    //select count to get NEW orderINDEX
                    $cat_count = DB::select('select COUNT(*) as catCount FROM products_categories_detail where cate_parent_id=0');
                    $itemAdd->cate_orders = $cat_count[0]->catCount;
                }
                else{
                    $itemAdd->cate_orders = 0;
                }
                $itemAdd->cate_slug = $cate_slug;
                $itemAdd->cate_date_public = $this->commonCtl->getCarbonNow();
                $itemAdd->created_user =Session::get('userAuth')->user_id;
                $itemAdd->created_date = $this->commonCtl->getCarbonNow();
                $itemAdd->updated_user = Session::get('userAuth')->user_id;
                $itemAdd->updated_date = $this->commonCtl->getCarbonNow();
                $itemAdd->status = 1;
                $itemAdd->save();
                $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_created'), ['nhóm sản phẩm']);
                
            } 
            catch(ValidationException $e)
            {
                DB::rollback();
                $response["error"] = Lang::get('messages.common_error_exception');
                return response()->json($response);
            } 
            catch(\Exception $e)
            {
                DB::rollback();
                $response["error"] = Lang::get('messages.common_error_exception');
                return response()->json($response);
            }

            DB::commit();
        }
        else
        {
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }

        return response()->json($response);
    } 
    protected function apiRemove(Request $request){  

        //Kiểm tra phân quyền sử dụng
        $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        if($auth["auth"])
        {
            return response()->json($auth);
        }
        $this->sessionUser = $this->commonCtl->checkUserAuth($request);


        DB::beginTransaction();
        try {

            $whereFunctions = array(
                ['cate_id', '=', $request->input('cate_id')]
            );

            $itemDelete = ProductsCategoriesDetail::where($whereFunctions)->first();

            if(empty($itemDelete))
            {
                $response["error"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' nhóm sản phẩm']);
                return response()->json($response);
            }   
            // nếu có sản phẩm thì không cho xóa
            if($this->CheckValidDeleteCat($request->input('cate_id')) == 1)
            {
                $response["warning"] = "Danh Mục Có Sản Phẩm, Không Thể Xóa !";
            }
            else
            {
                $deletedRows = ProductsCategoriesDetail::where($whereFunctions)->delete();

                if($deletedRows > 0){
                    $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_removed'), ['nhóm sản phẩm']);
                }
                else{
                    $response["warning"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_removed'), ['nhóm sản phẩm']);
                }
            }
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }
        DB::commit();

        return response()->json($response);
    } 
    protected function apiUpdate(Request $request){  

        //Kiểm tra phân quyền sử dụng
        $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        if($auth["auth"])
        {
            return response()->json($auth);
        }
        $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        // tao slug moi
        $cate_slug = $this->commonCtl->slugify($request->input('cate_name'));
        if(empty($cate_slug))
        {
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }

        $whereFunctions = array(
            ['cate_slug', '=', $cate_slug],
            ['cate_id', '<>', $request->input('cate_id')]
        );
        // check trung slug
        $existItem = ProductsCategoriesDetail::where($whereFunctions)->first();
        if(!empty($existItem))
        {
            $response["warning"] = [$this->commonCtl->replaceTitle(Lang::get('messages.common_error_exist_data'), ['Nhóm', 'sản phẩm'])];
            return response()->json($response);
        }   
        // cap nhat
        $whereFunctions = array(
            ['cate_id', '=', $request->input('cate_id')]
        );
        $itemUpdate = ProductsCategoriesDetail::where($whereFunctions)->first();
        // check ton tai truoc khi cap nhat
        if(empty($itemUpdate))
        {
            $response["warning"] = [$this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' nhóm sản phẩm'])];
            return response()->json($response);
        }  
        // cap nhat feild moi
        $itemUpdate->cate_slug = $cate_slug;
        $itemUpdate->cate_parent_id = $request->input('cate_parent_id');
        $itemUpdate->cate_name = $request->input('cate_name');
        $itemUpdate->cate_description = $request->input('cate_description');
        $itemUpdate->cate_thumbnail = $request->input('cate_thumbnail');
        $itemUpdate->updated_user = Session::get('userAuth')->user_id;
        $itemUpdate->updated_date = $this->commonCtl->getCarbonNow();

        if(!empty($itemUpdate))
        {
            DB::beginTransaction();
            try {
                $itemUpdate->save();
                $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_updated'), ['nhóm sản phẩm']);
            } 
            catch(ValidationException $e)
            {
                DB::rollback();
                $response["error"] = Lang::get('messages.common_error_exception');
                return response()->json($response);
            } 
            catch(\Exception $e)
            {
                DB::rollback();
                $response["error"] = Lang::get('messages.common_error_exception');
                return response()->json($response).$e;
            }

            DB::commit();
        }
        else
        {
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }

        return response()->json($response);
    }
    // CheckValidDeleteCat
    protected function CheckValidDeleteCat($cate_id){  
        $listParent = DB::select("
            select ca.cate_id,
            ca1.cate_id AS cate_id1,
            ca2.cate_id AS cate_id2 
            FROM products_categories_detail AS ca
            LEFT JOIN products_categories_detail AS ca1 ON ca1.cate_id=ca.cate_parent_id
            LEFT JOIN products_categories_detail AS ca2 ON ca2.cate_id=ca1.cate_parent_id
            WHERE ca.cate_id=".$cate_id."
            ");

        if(count($listParent) > 0)
        {
            if($listParent[0]->cate_id!=null){
                $productCount = DB::table('products_detail')
                ->where('cate_id','=',$listParent[0]->cate_id)
                ->count();
                if($productCount>0){
                    return 1;
                }
            }
            // if($listParent[0]->cate_id1!=null){
            //     $productCount = DB::table('products_detail')
            //     ->where('cate_id','=',$listParent[0]->cate_id1)
            //     ->count();
            //     if($productCount>0){
            //         return 1;
            //     }
            // }
            // if($listParent[0]->cate_id2!=null){
            //     $productCount = DB::table('products_detail')
            //     ->where('cate_id','=',$listParent[0]->cate_id2)
            //     ->count();
            //     if($productCount>0){
            //         return 1;
            //     }
            // }
            return 0;
        }
        else
        {
            return 0;
        }   
    }
    // apiUpdatePtoPOrder
    protected function apiUpdatePtoPOrder(Request $request){  

        //Kiểm tra phân quyền sử dụng
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }
        // $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";
        //
        $listParentCat = DB::table('products_categories_detail')
                          ->select('cate_id','cate_parent_id')
                          ->where('cate_parent_id','=',0)
                          ->get();

        $CatCount = count($listParentCat);
       
        //==================================itemSource========================
        $itemSource = ProductsCategoriesDetail::where('cate_id','=',$request->input('source_cate_id'))->first();
        // cap nhat feild moi
        $itemSource->cate_orders = $request->input('dest_index');

        DB::beginTransaction();
        try {
            
            //when move C to P (sorce có Parent, DEST_cate_id=="")
            if($request->input('source_cate_parent_id')!=0 
                && $request->input('dest_cate_id')=="" )
            {
                // $mem="mem:";
                $obj_array=[];
                for ($i = $request->input('dest_index'); $i < $CatCount; $i++) {
                    $itemUpdate = ProductsCategoriesDetail::where('cate_orders','=',$i)->first();
                    $itemUpdate->cate_orders = $i+1;
                    // $mem=$mem."/".$i;
                    array_push($obj_array, $itemUpdate); 
                }
                // update array
                foreach ($obj_array as $item) {
                    $item->save();
                }
                $itemSource->cate_parent_id = 0;
                $itemSource->save();
                $response["success"] ="Cập Nhật Thành Công !";
                // $response["success"] ="move C to P:".$mem;

                // $response["success"] ="when move C to P !";
                // return response()->json($response);
            }
            //when move P to P (sorce ko có Parent, DEST_cate_id=="")
            else if($request->input('source_cate_parent_id')==0 
                && $request->input('dest_cate_id')=="" )
            {
                //when mmove left=> right: source < dest
                if($request->input('source_index') < $request->input('dest_index'))
                {
                    $mem="mem:";
                    $obj_array=[];
                    for ($i = $request->input('source_index')+1; $i <= $request->input('dest_index'); $i++) {
                        $itemUpdate = ProductsCategoriesDetail::where('cate_orders','=',$i)->first();
                        $itemUpdate->cate_orders = $i-1;
                        // $mem=$mem.",".$itemUpdate->cate_name."_ord:".$itemUpdate->cate_orders;
                        array_push($obj_array, $itemUpdate); 
                    }
                    // update array
                    foreach ($obj_array as $item) {
                        $item->save();
                    }
                    $itemSource->save();
                    $response["success"] ="Cập Nhật Thành Công";
                }
                //when move right=> left: source > dest
                else{
                    $mem="mem:";
                    $obj_array=[];
                    for ($i = $request->input('dest_index'); $i <= $request->input('source_index')-1; $i++) {
                        $itemUpdate = ProductsCategoriesDetail::where('cate_orders','=',$i)->first();
                        $itemUpdate->cate_orders = $i+1;
                        // $mem=$mem.",".$itemUpdate->cate_name."_ord:".$itemUpdate->cate_orders;
                        array_push($obj_array, $itemUpdate);  
                    }
                    // update array
                    foreach ($obj_array as $item) {
                        $item->save();
                    }
                    $mem=$mem.",".$itemSource->cate_name."_ord".$itemSource->cate_orders;
                    $itemSource->save();
                    $response["success"] ="Cập Nhật Thành Công";
                }

                // $response["success"] ="when move P to P !";
                // return response()->json($response);
            }
            // when move C to C (source có parent, des có parent)
            else if($request->input('source_cate_parent_id')!=0
            &&  $request->input('dest_cate_id')!=0){
                if($request->input('source_index') < $request->input('dest_index'))
                {
                    $mem="mem:";
                    $obj_array=[];
                    for ($i = $request->input('source_index')+1; $i <= $request->input('dest_index'); $i++) {
                        $itemUpdate = ProductsCategoriesDetail::where('cate_orders','=',$i)->first();
                        $itemUpdate->cate_orders = $i-1;
                        // $mem=$mem.",".$itemUpdate->cate_name."_ord:".$itemUpdate->cate_orders;
                        array_push($obj_array, $itemUpdate); 
                    }
                    // update array
                    foreach ($obj_array as $item) {
                        $item->save();
                    }
                    $itemSource->cate_parent_id = $request->input('dest_cate_id');
                    $itemSource->save();
                    $response["success"] ="Cập Nhật Thành Công";
                }
                //when move right=> left: source > dest
                else{
                    $mem="mem:";
                    $obj_array=[];
                    for ($i = $request->input('dest_index'); $i <= $request->input('source_index')-1; $i++) {
                        $itemUpdate = ProductsCategoriesDetail::where('cate_orders','=',$i)->first();
                        $itemUpdate->cate_orders = $i+1;
                        // $mem=$mem.",".$itemUpdate->cate_name."_ord:".$itemUpdate->cate_orders;
                        array_push($obj_array, $itemUpdate);  
                    }
                    // update array
                    foreach ($obj_array as $item) {
                        $item->save();
                    }
                    $mem=$mem.",".$itemSource->cate_name."_ord".$itemSource->cate_orders;
                    $itemSource->cate_parent_id = $request->input('dest_cate_id');
                    $itemSource->save();
                    $response["success"] ="Cập Nhật Thành Công";
                }
                // $itemSource->cate_parent_id = $request->input('dest_cate_id');
                // $itemSource->cate_orders = $request->input('dest_index');
                // $itemSource->save(); 

                $response["success"] ="when move C to C !";
                // return response()->json($response);
            }
            //when move P to C (source ko có parent, des có parent)
            //gán source_parent_id = dest_id
            else{
                // $mem="mem:";
                $obj_array=[];
                for ($i = $request->input('source_index')+1; $i < $CatCount; $i++) {
                    $itemUpdate = ProductsCategoriesDetail::where('cate_orders','=',$i)->first();
                    $itemUpdate->cate_orders = $i-1;
                    // $mem=$mem."/".$i;
                    array_push($obj_array, $itemUpdate); 
                }
                // update array
                foreach ($obj_array as $item) {
                    $item->save();
                }
                $itemSource->cate_parent_id = $request->input('dest_cate_id');
                $itemSource->cate_orders = 100;
                $itemSource->save();
                $response["success"] ="Cập Nhật Thành Công !";
                // $response["success"] ="when move P to C !".$mem;
                // return response()->json($response);
            }
            
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response).$e;
        }

        DB::commit();
       
        return response()->json($response);
    } 
    // apiUpdateOrder
    protected function apiUpdateOrder(Request $request){  

        //Kiểm tra phân quyền sử dụng
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }
        // $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $whereFunctions = array(
            ['cate_parent_id', '=', 0],
            ['status', '<>', 0]
        );

        $listParent = DB::table('products_categories_detail')
                          ->select('cate_id', 
                                   'cate_parent_id',
                                   'cate_slug',
                                   'cate_name', 
                                   'cate_description',
                                   'cate_thumbnail',
                                   'cate_orders')
                          ->where($whereFunctions)
                          ->orderBy('cate_orders', 'asc')
                          ->get();

        
        DB::beginTransaction();
        try {
            $listStr="";
            foreach ($listParent as $indexKey =>$cate) {
                // $listStr=$listStr."/".$indexKey;
                ProductsCategoriesDetail::where('cate_id', $cate->cate_id)
                  ->update(['cate_orders' => $indexKey]);

            }
            $response["success"] = "ok:".$listStr;
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }

        DB::commit();
       

        return response()->json($response);
    } 
    // apiGetCatParent
    protected function apiGetCatParent(Request $request){

        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");

        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";



        $listParent = DB::select("
            select * from products_categories_detail AS ca
            WHERE ca.cate_id NOT IN 
            (
                SELECT ca3.cate_id FROM products_categories_detail AS ca
                LEFT JOIN products_categories_detail AS ca2 ON ca2.cate_parent_id = ca.cate_id
                LEFT JOIN products_categories_detail AS ca3 ON ca3.cate_parent_id = ca2.cate_id
                WHERE ca3.cate_id is NOT null
            )
            ");

        if(count($listParent) > 0)
        {
            $response["success"] = $listParent;
        }
        else
        {
            $response["warning"] = Lang::get('messages.common_warning_empty_list');
        }

        return response()->json($response);
    }

}
