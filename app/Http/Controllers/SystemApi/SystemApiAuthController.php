<?php

namespace App\Http\Controllers\SystemApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Cookie;
use Session;

use App\Http\Controllers\SystemApi\SystemCommonController as commonCtl;

class SystemApiAuthController extends Controller
{
    private $commonCtl;

    public function __construct(commonCtl $SystemCommonController){
        $this->commonCtl = $SystemCommonController;
    }

	//Ajax Post Kiểm Tra Tài Khoản
    protected function apiCheckLogin(Request $request)
    {   
        $auth = $this->commonCtl->checkTokenSession($request);
        if($auth["auth"])
        {
            return response()->json($auth);
        }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

    	//Lấy dữ liệu post từ angularjs
    	$user_account = strtolower($request->input('user_account'));
    	$user_password = $request->input('user_password');
    	
    	if(empty($user_account))
        {
            $response["warning"] = "Tài khoản hoặc mật khẩu không chính xác.";
            return response()->json($response);
        }

        if(empty($user_password))
        {
            $response["warning"] = "Tài khoản hoặc mật khẩu không chính xác.";
            return response()->json($response);
        }

        $userLockExists = DB::table('system_users')
                ->select('user_id')
                ->where(function($query) use ($user_account,$user_password)
                {
                    $query->where('user_account', '=', $user_account);
                    $query->where('user_password', '=', md5($user_password));
                    $query->where('status', '=', 0);
                })
                ->first();

        if($userLockExists)
        {
            $response["error"] = "Tài khoản đã bị khóa. Vui lòng liên hệ quản trị viên.";
            return response()->json($response);
        }

        $userInfo = DB::table('system_users')
                ->leftJoin('system_authorities', 'system_users.auth_id', '=', 'system_authorities.auth_id')
                ->leftJoin('branches_detail', 'branches_detail.branch_id', '=', 'system_users.branch_id')
                ->select('user_id'
                        ,'user_fullname'
                        ,'user_account'
                        ,'user_avatar'
                        ,'system_authorities.auth_id'
                        ,'auth_name'
                        ,'user_avatar'
                        ,'user_sex'
                        ,'system_users.branch_id'
                        ,'branches_detail.branch_name'
                        ,'system_users.status')
                ->where(function($query) use ($user_account,$user_password)
                {
                    $query->where('user_account', '=', $user_account);
                    $query->where('user_password', '=', md5($user_password));
                    $query->where('system_users.status', '<>', 0);
                })
                ->first();
        if($userInfo)
        {
            Session::put('lockScreen', false);
            Session::put('userAuth', $userInfo);
            Session::put('userAuth2', $userInfo);
            // $response["success"] = "Đăng Nhập Thành Công";
            $response["success"] = $userInfo;
        }
        else
        {
            $response["warning"] = "Tài khoản hoặc mật khẩu không chính xác.";
        }

        if(!empty($request->input('remembered')) && $request->input('remembered') == 1)
        {
            //make(string $name, string $value, int $minutes, string $path = null, string $domain = null, bool $secure = false, bool $httpOnly = true)
            //Create a new cookie instance.
            return response()->json($response)->withCookie(Cookie::make('userAuth', $userInfo, 86400));
        }
        else
        {
            return response()->json($response);
        }
    }
}
