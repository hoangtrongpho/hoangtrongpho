<?php

namespace App\Http\Controllers\SystemApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use DateTime;
use Validator;
use Mail;
use Lang;

use App\Http\Controllers\SystemApi\SystemCommonController as commonCtl;

use App\Models\ProductsDetail;
use App\Models\ProductsCategoriesDetail;
use App\Models\ProductsGalleriesDetail;
use App\Models\ProductsTagsDetail;
use App\Models\ProductsTagsList;
use App\Models\ProductTransaction;


class SystemApiProductsController extends Controller
{

    /*** Khởi Tạo Giá Trị ***/
    private $commonCtl;

    private $sessionUser;

    /*!! Khởi Tạo Giá Trị !!*/

    public function __construct(commonCtl $SystemCommonController){
        $this->commonCtl =  $SystemCommonController;
    }

    /*** API Hàm Chức Năng ***/  

    //Ajax Post Lấy Danh Sách 
    protected function apiGetList(Request $request){  

        $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        if($auth["auth"])
        {
            return response()->json($auth);
        }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $whereFunctions = array(
   
        );

        $listProducts = DB::table('products_detail AS pd')
                ->leftJoin('products_categories_detail AS cd', 'cd.cate_id', '=', 'pd.cate_id')
                ->leftJoin('products_counter AS pc', 'pc.product_id', '=', 'pd.product_id')
                ->select('pd.product_id', 
                         'pd.product_slug', 
                         'pd.unit_id', 
                         'pd.cate_id', 
                         'pd.origin_id', 
                         'pd.manufacturer_id',
                         'pd.color_id',
                         'pd.product_name',
                         'pd.product_description',
                         'pd.product_specifications',
                         'pd.product_imported_prices',
                         'pd.product_retail_prices',
                         'pd.product_ratings', 
                         'pd.product_thumbnail', 
                         'pd.product_comment_status', 
                         'pd.updated_date', 
                         'pd.store_quantity', 
                         'pd.status', 
                         'cd.cate_id',
                         'cd.cate_slug',
                         'cd.cate_name',
                         DB::raw('count(pc.counter_client_ip) as views_couter'))
                ->where($whereFunctions)
                ->groupBy('pd.product_id', 
                         'pd.product_slug', 
                         'pd.unit_id', 
                         'pd.cate_id', 
                         'pd.origin_id', 
                         'pd.manufacturer_id',
                         'pd.color_id',
                         'pd.product_name',
                         'pd.product_description',
                         'pd.product_specifications',
                         'pd.product_imported_prices',
                         'pd.product_retail_prices',
                         'pd.product_ratings', 
                         'pd.product_thumbnail', 
                         'pd.product_comment_status', 
                         'pd.updated_date', 
                         'pd.store_quantity',
                         'pd.status', 
                         'cd.cate_id',
                         'cd.cate_slug',
                         'cd.cate_name'
                         )
                ->orderBy('pd.updated_date', 'desc')
                ->get();

        if(count($listProducts)>0)
        {
            $response["success"] = $listProducts;
        }
        else
        {
            $response["warning"] = [Lang::get('messages.common_warning_empty_list')];
        }

        return response()->json($response);
    }
    // apiGetListCat
    protected function apiGetListCat(Request $request){
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $listCategories = array();

        $whereFunctions = array(
            ['cate_parent_id', '=', 0],
            ['status', '<>', 0]
        );

        $listParent = DB::table('products_categories_detail')
                          ->select('cate_id', 
                                   'cate_parent_id',
                                   'cate_slug',
                                   'cate_name', 
                                   'cate_description',
                                   'cate_thumbnail',
                                   'cate_orders',
                                   'cate_pinned')
                          ->where($whereFunctions)
                          ->orderBy('cate_orders', 'asc')
                          ->get();

        $whereFunctions = array(
            ['cate_parent_id', '<>', 0],
            ['status', '<>', 0]
        );

        $listChild = DB::table('products_categories_detail')
                          ->select('cate_id', 
                                   'cate_parent_id',
                                   'cate_slug',
                                   'cate_name', 
                                   'cate_description',
                                   'cate_thumbnail',
                                   'cate_orders',
                                   'cate_pinned')
                          ->where($whereFunctions)
                          ->orderBy('cate_orders', 'asc')
                          ->get();
        // loop to get ALL
        foreach ($listParent as $value_p) {

            $itemParent = [
                "cate_id" => $value_p->cate_id,
                "cate_parent_id" => $value_p->cate_parent_id,
                "cate_slug" => $value_p->cate_slug,
                "cate_name" => $value_p->cate_name,
                "cate_description" => $value_p->cate_description,
                "cate_thumbnail" => $value_p->cate_thumbnail,
                "cate_orders" => $value_p->cate_orders,
                "cate_pinned" => $value_p->cate_pinned,
                "nodes" => array()
            ];
            array_push($listCategories, $itemParent);

            // foreach add con 1
            foreach ($listChild as $value_c) {
                // nếu cate_parent_id = cate_id
                if($value_c->cate_parent_id == $value_p->cate_id)
                {
                    $newc1_name = "--".$value_c->cate_name;
                    $itemChild = [
                        "cate_id" => $value_c->cate_id,
                        "cate_parent_id" => $value_p->cate_id,
                        "cate_slug" => $value_c->cate_slug,
                        "cate_name" => $newc1_name,
                        "cate_description" => $value_c->cate_description,
                        "cate_thumbnail" => $value_c->cate_thumbnail,
                        "cate_orders" => $value_c->cate_orders,
                        "cate_pinned" => $value_p->cate_pinned,
                        "nodes" => array()
                    ];
                    array_push($listCategories, $itemChild);
                    // add con 2
                    foreach ($listChild as $value_c2) {
                        if($value_c2->cate_parent_id == $value_c->cate_id){
                            $newc2_name = "----".$value_c2->cate_name;
                            $itemChild2 = [
                                "cate_id" => $value_c2->cate_id,
                                "cate_parent_id" => $value_c->cate_id,
                                "cate_slug" => $value_c2->cate_slug,
                                "cate_name" => $newc2_name,
                                "cate_description" => $value_c2->cate_description,
                                "cate_thumbnail" => $value_c2->cate_thumbnail,
                                "cate_orders" => $value_c2->cate_orders,
                                "cate_pinned" => $value_p->cate_pinned,
                                "nodes" => array()
                            ];
                            array_push($listCategories, $itemChild2);
                        }
                    }
                }
            }
            //  add cha
            // array_push($listCategories, $itemParent);
        }

        if(count($listCategories)>0)
        {
            $response["success"] = $listCategories;
        }
        else
        {
            $response["warning"] = [Lang::get('messages.common_warning_empty_list')];
        }

        return response()->json($response);
    }
    // apiGetListUnit
    protected function apiGetListUnit(Request $request){
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

   
        $listItem = DB::table('products_units_detail as ud')
                ->leftJoin('products_detail AS pd', 'ud.unit_id', '=', 'pd.unit_id')
                ->select('ud.unit_id', 
                        'ud.unit_slug',
                        'ud.unit_name',
                        'ud.unit_description',
                        DB::raw('DATE_FORMAT(ud.updated_date,"%d/%m/%Y %H:%m:%s") AS updated_date'),
                        'ud.status',
                        DB::raw('count(pd.color_id) as item_couter'))
                ->where('ud.status','<>',0)
                ->groupBy('ud.unit_id', 
                        'ud.unit_slug',
                        'ud.unit_name',
                        'ud.unit_description',
                        'ud.updated_date',
                        'ud.status')
                ->get();

        if(!$listItem->isEmpty())
        {
            $response["success"] = $listItem;
        }
        else
        {
            $response["warning"] = Lang::get('messages.common_warning_empty_list');
        }

        return response()->json($response);
    }
    // apiGetListOrigin
    protected function apiGetListOrigin(Request $request){
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

   
        $listItem = DB::table('products_origins_detail as od')
                ->select('od.origin_id', 
                        'od.origin_slug',
                        'od.origin_name',
                        'od.origin_description',
                        'od.status'
                        )
                ->where('od.status','<>',0)
                ->get();

        if(!$listItem->isEmpty())
        {
            $response["success"] = $listItem;
        }
        else
        {
            $response["warning"] = Lang::get('messages.common_warning_empty_list');
        }

        return response()->json($response);
    }
    // apiGetListManufacturer
    protected function apiGetListManufacturer(Request $request){
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

   
        $listItem = DB::table('products_manufacturers_detail as md')
                ->select('md.manufacturer_id', 
                        'md.manufacturer_slug',
                        'md.manufacturer_name',
                        'md.manufacturer_description',
                        DB::raw('DATE_FORMAT(md.updated_date,"%d/%m/%Y %H:%m:%s") AS updated_date'),
                        'md.status')
                ->where('md.status','<>',0)
                ->get();

        if(!$listItem->isEmpty())
        {
            $response["success"] = $listItem;
        }
        else
        {
            $response["warning"] = Lang::get('messages.common_warning_empty_list');
        }

        return response()->json($response);
    }
    // apiGetListColor
    protected function apiGetListColor(Request $request){
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";
   
        $listItem = DB::table('products_colors_detail as cd')
                ->leftJoin('products_detail AS pd', 'pd.color_id', '=', 'pd.color_id')
                ->select('cd.color_id', 
                        'cd.color_slug',
                        'cd.color_name',
                        'cd.color_description',
                        'cd.color_code',
                        'cd.color_duotone',
                        DB::raw('DATE_FORMAT(cd.updated_date,"%d/%m/%Y %H:%m:%s") AS updated_date'),
                        'cd.status',
                        DB::raw('count(pd.color_id) as item_couter'))
                ->where('cd.status','<>',0)
                ->groupBy('cd.color_id', 
                        'cd.color_slug',
                        'cd.color_name',
                        'cd.color_description',
                        'cd.color_code',
                        'cd.color_duotone',
                        'cd.updated_date',
                        'cd.status')
                ->get();

        if(!$listItem->isEmpty())
        {
            $response["success"] = $listItem;
        }
        else
        {
            $response["warning"] = Lang::get('messages.common_warning_empty_list');
        }

        return response()->json($response);
    }
    // apiGetListTag
    protected function apiGetListTag(Request $request){
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";
   
        $listItem = DB::table('products_tags_detail as td')
                ->leftJoin('products_tags_list AS tl', 'td.tag_id', '=', 'tl.tag_id')
                ->select('td.tag_id', 
                        'td.tag_slug',
                        'td.tag_name',
                        'td.tag_description',
                        DB::raw('DATE_FORMAT(td.updated_date,"%d/%m/%Y %H:%m:%s") AS updated_date'),
                        'td.status',
                        DB::raw('count(tl.tag_id) AS item_couter'))
                ->where('td.status','<>',0)
                ->groupBy('td.tag_id', 
                        'td.tag_slug',
                        'td.tag_name',
                        'td.tag_description',
                        'td.updated_date',
                        'td.status')
                ->get();

        if(!$listItem->isEmpty())
        {
            $response["success"] = $listItem;
        }
        else
        {
            $response["warning"] = Lang::get('messages.common_warning_empty_list');
        }

        return response()->json($response);
    }
    /*------------------------------API Get Product detail-----------------------*/
    protected function apiGetDetails(Request $request){  
        
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $whereFunctions = array(
            ['nd.product_slug', '=', $request->input('product_slug')]
        );

        $detail = DB::table('products_detail AS nd')
        ->leftJoin('products_units_detail AS un', 'nd.unit_id', '=', 'un.unit_id')
        ->leftJoin('products_categories_detail AS ca', 'nd.cate_id', '=', 'ca.cate_id')
        ->leftJoin('products_origins_detail AS or', 'nd.origin_id', '=', 'or.origin_id')
        ->leftJoin('products_manufacturers_detail AS ma', 'nd.manufacturer_id', '=', 'ma.manufacturer_id')
        ->leftJoin('products_colors_detail AS co', 'nd.color_id', '=', 'co.color_id')
                ->select('nd.product_id',
                    'nd.product_slug',
                    'nd.unit_id',
                    'nd.cate_id',
                    'nd.origin_id',
                    'nd.manufacturer_id',
                    'nd.color_id',
                    'nd.product_name',
                    'nd.product_description',
                    'nd.product_specifications',
                    'nd.product_imported_prices',
                    'nd.product_retail_prices',
                    'nd.product_deals',
                    'nd.product_ratings',
                    'nd.product_thumbnail',
                    'nd.product_images_list',
                    'nd.product_release_date',
                    'nd.product_expiration_date',
                    'nd.product_comment_status',
                    'nd.product_saleoff_pinned',
                    'nd.product_new_pinned',
                    'nd.product_quick_pinned',
                    'nd.news_seo_title',
                    'nd.news_seo_description',
                    'nd.news_seo_keywords',
                    'nd.status',

                    'un.unit_name',
                    'ca.cate_name',
                    'or.origin_name',
                    'ma.manufacturer_name',
                    'co.color_name'
                         )
                ->where($whereFunctions)
                ->first();
        // get list tag / by product ID in ($detail)
        $whereFunctions = array();

        array_push($whereFunctions, ['nd.product_id', '=', $detail->product_id]);

        $listTags = DB::table('products_detail AS nd')
                ->leftJoin('products_tags_list AS tl', 'nd.product_id', '=', 'tl.product_id')
                ->leftJoin('products_tags_detail AS td', 'tl.tag_id', '=', 'td.tag_id')
                ->select('td.tag_id',
                         'td.tag_slug',
                         'td.tag_name')
                ->where($whereFunctions)
                ->get();
        //check null > error
        if($detail !== null)
        {
            $response["success"] = $detail;
            $response["listTags"] = $listTags;
        }
        else
        {
            $response["warning"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' Sản Phẩm']);
        }

        return response()->json($response);
    }
    // apiChangeStatus
    protected function apiChangeStatus(Request $request){   

        $this->sessionUser = Session::get('userAuth'); 

        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

       
        $itemUpdate = ProductsDetail::where('product_id', '=', $request->input('product_id'))->first();

        if($itemUpdate == null)
        {
            $response["error"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' Sản Phẩm']);
            return response()->json($response);
        }

        // Start transaction!
        DB::beginTransaction();

        try {
            $updatedRows = ProductsDetail::where('product_id', '=', $itemUpdate->product_id)
                            ->update([
                                'status' => $request->input('status'),
                                'updated_date' => $this->commonCtl->getCarbonNow(),
                                'updated_user' => $this->sessionUser->user_id
                            ]);

            if($updatedRows > 0){
                $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_updated'), ['Sản Phẩm']);
            }
            else{
                $response["warning"] = [$this->commonCtl->replaceTitle(Lang::get('messages.common_error_updated'), ['Sản Phẩm'])];
                DB::rollback();
            }
            
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }
        DB::commit();

        return response()->json($response);
    }
    //==============================Ajax Post Thêm========================
    protected function apiAdd(Request $request){   

        $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        if($auth["auth"])
        {
            return response()->json($auth);
        }

        $this->sessionUser = Session::get('userAuth'); 

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

       
        //Lấy dữ liệu post từ angularjs
        $itemProduct = new ProductsDetail;

        $itemProduct->fill($request->all());

        $product_release_date = $this->commonCtl->formatCarbonDatetime_From($request->input('product_release_date'));
        if($product_release_date !== null)
        {
            $itemProduct->product_release_date = $product_release_date;
        }
        else
        {
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }

        $product_expiration_date = $this->commonCtl->formatCarbonDatetime_To($request->input('product_expiration_date'));
        if($product_expiration_date !== null)
        {
            $itemProduct->product_expiration_date = $product_expiration_date;
        }
        else
        {
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }

        $product_slug = $this->commonCtl->slugify($request->input('product_name'));
        // kiem tra slug ton tai trong DB
        if($product_slug !== null)
        {
            $itemProduct->product_slug = $product_slug;
        }
        else
        {
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }
        $itemProduct->store_quantity = 1000;
        $itemProduct->created_user = Session::get('userAuth')->user_id; 
        $itemProduct->created_date = $this->commonCtl->getCarbonNow();
        $itemProduct->updated_user = Session::get('userAuth')->user_id; 
        $itemProduct->updated_date = $this->commonCtl->getCarbonNow();

        DB::beginTransaction();

        try {
            $itemProduct->save();

            if(count($request->input('list_tags')) > 0)
            {
                foreach ($request->input('list_tags') as $tag) {
                    $tag = new ProductsTagsList([
                        'product_id' => $itemProduct->product_id,
                        'tag_id' => $tag['tag_id'],
                    ]);
                    $tag->save();
                }
            }


            $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_created'), ['Sản Phẩm']);
            
        } 
        catch(ValidationException $e)
        {
            DB::rollback();

            // $response["error"] = $e->getErrors();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response).$e;
        }

        DB::commit();

        return response()->json($response);
    }
    /*------------------------------API Udate Product detail-----------------------*/
    protected function apiUpdate(Request $request){   

        // $this->sessionUser = Session::get('userAuth'); 
        
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        //Kiêm tra San Pham tồn tại (thông qua slug)
        $currentProduct = ProductsDetail::where('product_slug','=',$request->input('product_slug'))
                        ->first();

        // if($currentProduct === null)
        // {
        //     $response["warning"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' Sản Phẩm']);
        //     return response()->json($response);
        // }
        // check date time > format > update
        $product_release_date = $this->commonCtl->formatCarbonDatetime_From($request->input('product_release_date'));
        if($product_release_date !== null)
        {
            $currentProduct->product_release_date = $product_release_date;
        }
        else
        {
            $response["error"] = "product_release_date";
            return response()->json($response);
        }

        $product_expiration_date = $this->commonCtl->formatCarbonDatetime_To($request->input('product_expiration_date'));
        if($product_expiration_date !== null)
        {
            $currentProduct->product_expiration_date = $product_expiration_date;
        }
        else
        {
            $response["error"] = "product_expiration_date";
            return response()->json($response);
        }

        // create new slug from name
        if($request->input('product_name') == null){

            $response["error"] = "product_name";
            return response()->json($response);
        }
        $product_slug = $this->commonCtl->slugify($request->input('product_name'));

        if($product_slug !== null)
        {
            $currentProduct->product_slug = $product_slug;
        }
        else
        {
            $response["error"] = "product_slug";
            return response()->json($response);
        }
        // update detail
        $currentProduct->cate_id = $request->input('cate_id');
        $currentProduct->unit_id = $request->input('unit_id');
        $currentProduct->origin_id = $request->input('origin_id');
        $currentProduct->manufacturer_id = $request->input('manufacturer_id');
        $currentProduct->color_id = $request->input('color_id');

        $currentProduct->product_name = $request->input('product_name');
        $currentProduct->product_description = $request->input('product_description');
        $currentProduct->product_specifications = $request->input('product_specifications');
        $currentProduct->product_imported_prices = $request->input('product_imported_prices');
        $currentProduct->product_retail_prices = $request->input('product_retail_prices');
        $currentProduct->product_ratings = 5;
        $currentProduct->product_thumbnail = $request->input('product_thumbnail');
        $currentProduct->product_images_list = $request->input('product_images_list');
        $currentProduct->product_comment_status = $request->input('product_comment_status');

        $currentProduct->product_saleoff_pinned = $request->input('product_saleoff_pinned');
        $currentProduct->product_new_pinned = $request->input('product_new_pinned');
        $currentProduct->product_quick_pinned = $request->input('product_quick_pinned');

        $currentProduct->news_seo_title = $request->input('news_seo_title');
        $currentProduct->news_seo_description = $request->input('news_seo_description');
        $currentProduct->news_seo_keywords = $request->input('news_seo_keywords');
        $currentProduct->status = $request->input('status');
        $currentProduct->updated_user = Session::get('userAuth')->user_id;
        $currentProduct->updated_date = $this->commonCtl->getCarbonNow();

        DB::beginTransaction();

        try {
            // delete old product tag list
            $deletedRowsTags = ProductsTagsList::where('product_id', '=', $currentProduct->product_id)->delete();
            // update product
            $currentProduct->save();

            // save new product tag list
            if(count($request->input('list_tags')) > 0)
            {
                foreach ($request->input('list_tags') as $tag) {
                    $tag = new ProductsTagsList([
                        'product_id' => $currentProduct->product_id,
                        'tag_id' => $tag['tag_id'],
                    ]);
                    $tag->save();
                }
            }

            // return success message
            $response["success"] = "ok";
            
        } 
        catch(ValidationException $e)
        {
            DB::rollback();

            // $response["error"] = $e->getErrors();
            $response["error"] = Lang::get('messages.common_error_exception').$e;
            return response()->json($response);
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception').$e;
            return response()->json($response);
        }

        DB::commit();

        return response()->json($response);
    }
    /*------------------------------API remove Product-----------------------*/
    protected function apiRemove(Request $request){ 

        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $itemDelete = ProductsDetail::where('product_id', '=', $request->input('product_id'))->first();

        if($itemDelete == null)
        {
            $response["error"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' Sản Phẩm']);
            return response()->json($response);
        }

        // Start transaction!
        DB::beginTransaction();

        try {

            $deletedRowsTags = ProductsTagsList::where('product_id', '=', $itemDelete->product_id)->delete();

            $deletedRowsNews = ProductsDetail::where('product_id', '=', $itemDelete->product_id)->delete();

            if($deletedRowsNews > 0){
                $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_removed'), ['Sản Phẩm']);
            }
            else{
                $response["warning"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_removed'), ['Sản Phẩm']);
                DB::rollback();
            }
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
  
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);

        }
        DB::commit();

        return response()->json($response);
    }

    /*!! API Hàm Chức Năng !!*/  
}
