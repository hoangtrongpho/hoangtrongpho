<?php

namespace App\Http\Controllers\SystemApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use DateTime;
use Validator;
use Mail;
use Lang;

use App\Http\Controllers\SystemApi\SystemCommonController as commonCtl;

use App\Models\CustomersDetail;
use App\Models\CustomersGroup;

class SystemApiCustomerGroupController extends Controller
{

    /*** Khởi Tạo Giá Trị ***/
    private $commonCtl;

    private $sessionUser;

    private $rulesMess = [
        'group_id.required' => 'Vui Lòng Chọn Mã Nhóm'
    ];
    /*!! Khởi Tạo Giá Trị !!*/

    public function __construct(commonCtl $SystemCommonController){
        $this->commonCtl =  $SystemCommonController;
    }

    /*** API Hàm Chức Năng ***/  

    //Ajax Post Lấy Danh Sách 
    protected function apiGetList(Request $request){  

        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $whereFunctions = array();

        $listGroups = DB::table('customers_group AS gr')
                ->select(
                    'gr.group_id',
                    'gr.group_name',
                    'gr.group_description',
                    'gr.status'
                         )
                ->where($whereFunctions)
                ->orderBy('gr.updated_date', 'desc')
                ->get();

        if(!$listGroups->isEmpty())
        {
            $response["success"] = $listGroups;
        }
        else
        {
            $response["warning"] = Lang::get('messages.common_warning_empty_list');
        }

        return response()->json($response);
    }
    //Ajax Post Thêm
    protected function apiAdd(Request $request){  
        
        // //Kiểm tra phân quyền sử dụng
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }
        // $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";           

        $itemAdd = new CustomersGroup;

        $itemAdd->fill($request->all());
        $itemAdd->created_user = 1;
        $itemAdd->created_date = $this->commonCtl->getCarbonNow();
        $itemAdd->updated_user = 1;
        $itemAdd->updated_date = $this->commonCtl->getCarbonNow();
        $itemAdd->status = 1;

        DB::beginTransaction();

        try {
            $itemAdd->save();

            $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_created'), ['Nhóm']);
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);

        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
            // throw $e;
        }
        DB::commit();

        return response()->json($response);
    } 

    //Ajax Post Thêm
    protected function apiUpdate(Request $request){  

        //Kiểm tra phân quyền sử dụng
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }
        // $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $existsItem = CustomersGroup::where('group_id','=',$request->input('group_id'))
                        ->first();

        if(empty($existsItem))
        {
            $response["warning"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' Nhóm']);
            return response()->json($response);
        }
        
        $existsItem->group_name = $request->input('group_name');
        $existsItem->group_description = $request->input('group_description');
        $existsItem->updated_user = 1;
        $existsItem->updated_date = $this->commonCtl->getCarbonNow();

        DB::beginTransaction();
        try 
        {
            $existsItem->save();

            $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_updated'), ['Nhóm']);
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);

        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
            // throw $e;
        }

        DB::commit();

        return response()->json($response);
    } 

    //Ajax Post Xóa Thẻ
    protected function apiRemove(Request $request){ 

        //Kiểm tra phân quyền sử dụng
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }
        // $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";


        $itemDelete = CustomersGroup::where('group_id','=',$request->input('group_id'))
                        ->first();
        // nếu ai đó lỡ xóa trước > báo lỗi
        if(empty($itemDelete))
        {
            $response["error"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' Nhóm']);
            return response()->json($response);
        }

        DB::beginTransaction();
        try {

                $deletedRows = CustomersGroup::where('group_id','=',$request->input('group_id'))->delete();

                if($deletedRows > 0){
                    $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_removed'), ['Nhóm']);
                }
                else{
                    $response["warning"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_removed'), ['Nhóm']);
                }
            
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }
        DB::commit();

        return response()->json($response);
    }

}
