<?php

namespace App\Http\Controllers\SystemApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use DateTime;
use Validator;
use Mail;
use Lang;

use App\Http\Controllers\SystemApi\SystemCommonController as commonCtl;
use App\Models\NewsTagsDetail;
use App\Models\NewsTagsList;
use App\Models\ProductsUnitsDetail;
use App\Models\ProductsDetail;

class SystemApiProductUnitController extends Controller
{

    /*** Khởi Tạo Giá Trị ***/
    private $commonCtl;

    private $sessionUser;

    private $rulesMess = [
        'unit_id.required' => 'Vui Lòng Thêm ID Đơn Vị Sản Phẩm.',
        'unit_slug.required' => 'Vui Lòng Thêm Mã Đơn Vị Sản Phẩm.',
        'unit_name.required' => 'Vui Lòng Thêm Tên Đơn Vị Sản Phẩm.',
    ];

    /*** Khởi Tạo Giá Trị ***/
    public function __construct(commonCtl $SystemCommonController){
        $this->commonCtl =  $SystemCommonController;
    }

    //Ajax Post Lấy Danh Sách 
    protected function apiGetList(Request $request){  
        
        $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        if($auth["auth"])
        {
            return response()->json($auth);
        }
        $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $listItem = DB::table('products_units_detail as ud')
                ->leftJoin('products_detail AS pd', 'ud.unit_id', '=', 'pd.unit_id')
                ->select('ud.unit_id', 
                        'ud.unit_slug',
                        'ud.unit_name',
                        'ud.unit_description',
                        DB::raw('DATE_FORMAT(ud.updated_date,"%d/%m/%Y %H:%m:%s") AS updated_date'),
                        'ud.status',
                        DB::raw('count(pd.color_id) as item_couter'))
                ->where('ud.status','<>',0)
                ->groupBy('ud.unit_id', 
                        'ud.unit_slug',
                        'ud.unit_name',
                        'ud.unit_description',
                        'ud.updated_date',
                        'ud.status')
                ->get();

        if(!$listItem->isEmpty())
        {
            $response["success"] = $listItem;
        }
        else
        {
            $response["warning"] = Lang::get('messages.common_warning_empty_list');
        }

        return response()->json($response);
    } 

    //Ajax Post Thêm
    protected function apiAdd(Request $request){  
        
        //Kiểm tra phân quyền sử dụng
        $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        if($auth["auth"])
        {
            return response()->json($auth);
        }
        $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        /** Kiểm tra giá trị nhập vào **/
        $rules = [
            'unit_name' => 'required',
        ];

        $valiData = $this->commonCtl->validRequest($request,$rules,$this->rulesMess);

        if($valiData->fails())
        {   
            $response["warning"] = $valiData->errors();
            return response()->json($response);
        }
       

        /** Khởi tạo giá trị **/
        $unit_slug = $this->commonCtl->slugify($request->input('unit_name'));
        if(empty($unit_slug))
        {
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }

        $existsItem = ProductsUnitsDetail::where('unit_slug','=',$unit_slug)
                        ->first();
        if(!empty($existsItem))
        {
            $response["warning"] = [$this->commonCtl->replaceTitle(Lang::get('messages.common_error_exist_data'), ['Tên', 'đơn vị'])];
            return response()->json($response);
        }                

        $itemAdd = new ProductsUnitsDetail;

        $itemAdd->fill($request->all());
        $itemAdd->unit_slug = $unit_slug;
        $itemAdd->created_user = $this->sessionUser->user_id;
        $itemAdd->created_date = $this->commonCtl->getCarbonNow();
        $itemAdd->updated_user = $this->sessionUser->user_id;
        $itemAdd->updated_date = $this->commonCtl->getCarbonNow();
        $itemAdd->status = 1;

        DB::beginTransaction();

        try {
            $itemAdd->save();

            $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_created'), ['đơn vị']);
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);

            // return Redirect::to('/form')
            // ->withErrors( $e->getErrors() )
            // ->withInput();
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
            // throw $e;
        }
        DB::commit();

        return response()->json($response);
    } 

    //Ajax Post Thêm
    protected function apiUpdate(Request $request){  

        //Kiểm tra phân quyền sử dụng
        $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        if($auth["auth"])
        {
            return response()->json($auth);
        }
        $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $rules = [
            'unit_id' => 'required',
            'unit_slug' => 'required',
            'unit_name' => 'required',
        ];

        $valiData = $this->commonCtl->validRequest($request,$rules,$this->rulesMess);

        /** Kiểm tra giá trị nhập vào **/
        if($valiData->fails())
        {   
            $response["warning"] = $valiData->errors();
            return response()->json($response);
        }

        $existsItem = ProductsUnitsDetail::where('unit_slug','=',$request->input('unit_slug'))
                        ->first();

        if(empty($existsItem))
        {
            $response["warning"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' đơn vị']);
            return response()->json($response);
        }

        $unit_slug = $this->commonCtl->slugify($request->input('unit_name'));
        if(empty($unit_slug))
        {
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }

        $existsSlug = ProductsUnitsDetail::where([
                                    ['unit_slug','=',$unit_slug],
                                    ['unit_id','<>',$existsItem->unit_id],
                                ])->first();
        if($existsSlug)
        {
            $response["warning"] = [$this->commonCtl->replaceTitle(Lang::get('messages.common_error_exist_data'), ['Tên', 'đơn vị'])];
            return response()->json($response);
        }

        $existsItem->unit_slug = $unit_slug;
        $existsItem->unit_name = $request->input('unit_name');
        $existsItem->unit_description = $request->input('unit_description');
        $existsItem->updated_user = $this->sessionUser->user_id;
        $existsItem->updated_date = $this->commonCtl->getCarbonNow();

        DB::beginTransaction();
        try 
        {
            $existsItem->save();

            $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_updated'), ['đơn vị']);
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);

            // return Redirect::to('/form')
            // ->withErrors( $e->getErrors() )
            // ->withInput();
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
            // throw $e;
        }

        DB::commit();

        return response()->json($response);
    } 

    //Ajax Post Xóa Thẻ
    protected function apiRemove(Request $request){ 

        //Kiểm tra phân quyền sử dụng
        $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        if($auth["auth"])
        {
            return response()->json($auth);
        }
        $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        /** Kiểm tra giá trị nhập vào **/
        $rules = [
            'unit_id' => 'required',
        ];

        $valiData = $this->commonCtl->validRequest($request,$rules,$this->rulesMess);

        if($valiData->fails())
        {   
            $response["warning"] = $valiData->errors();
            return response()->json($response);
        }

        $itemDelete = ProductsUnitsDetail::where('unit_id', '=', $request->input('unit_id'))->first();

        if(empty($itemDelete))
        {
            $response["error"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' đơn vị']);
            return response()->json($response);
        }

        $checkExists = ProductsDetail::where('unit_id', '=', $request->input('unit_id'))->get();

        DB::beginTransaction();
        try {

            if(!$checkExists->isEmpty())
            {
                $itemDelete->updated_user = $this->sessionUser->user_id;
                $itemDelete->updated_date = $this->commonCtl->getCarbonNow();
                $itemDelete->status = 0;

                $itemDelete->save();

                $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_removed'), ['đơn vị']);
            }
            else
            {
                $deletedRows = ProductsUnitsDetail::where('unit_id', '=', $itemDelete->unit_id)->delete();

                if($deletedRows > 0){
                    $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_removed'), ['đơn vị']);
                }
                else{
                    $response["warning"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_removed'), ['đơn vị']);
                }
            }
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);

            // return Redirect::to('/form')
            // ->withErrors( $e->getErrors() )
            // ->withInput();
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
            // throw $e;
        }
        DB::commit();

        return response()->json($response);
    }
}
