<?php

namespace App\Http\Controllers\SystemApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Validator;
use Lang;

use App\Http\Controllers\SystemApi\SystemCommonController as commonCtl;

use App\Models\SystemAreasDistrict;
use App\Models\SystemAreasProvince;
use App\Models\SystemAreasWard;
use App\Models\AgenciesDetail;

use App\Models\SystemArea;

class SystemApiAreasController extends Controller
{

    /*** Khởi Tạo Giá Trị ***/
    private $commonCtl;

    private $sessionUser;

    private $rulesMessProvince = [
        'area_id.required' => 'Vui Lòng Nhập Mã Tỉnh Thành Phố',
        'area_name.required' => 'Vui Lòng Nhập Tên Tỉnh Thành Phố',
    ];

    private $rulesMessDistrict = [
        'area_id.required' => 'Vui Lòng Nhập Mã Quận Huyện',
        'area_name.required' => 'Vui Lòng Nhập Tên Quận Huyện',
        'area_parent_id.required' => 'Vui Lòng Nhập Mã Trực Thuộc',
        'area_type.required' => 'Vui Lòng Nhập Loại Quận Huyện',
    ];

    private $rulesMessWard = [
        'area_id.required' => 'Vui Lòng Nhập Mã Phường Xã',
        'area_name.required' => 'Vui Lòng Nhập Tên Phường Xã',
        'area_parent_id.required' => 'Vui Lòng Nhập Mã Trực Thuộc',
        'area_type.required' => 'Vui Lòng Nhập Loại Phường Xã',
    ];
    /*** Khởi Tạo Giá Trị ***/

    public function __construct(commonCtl $SystemCommonController){
        $this->commonCtl =  $SystemCommonController;
    }

    protected function apiGetListAreasForUpdateArea(Request $request){

        $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        if($auth["auth"])
        {
            return response()->json($auth);
        }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $whereFunctions = array(
            ['area_type', '<>', 3],
            ['area_id', '<>', $request->input('area_id')],
            ['status', '<>', 0]
        );

        $listAreas = DB::table('system_areas')
                          ->select('area_id', 
                                   'area_parent_id',
                                   'area_name', 
                                   'area_order',
                                   'area_type')
                          ->where($whereFunctions)
                          ->orderBy('area_name', 'asc')
                          ->get();

        if(count($listAreas) > 0)
        {
            $response["success"] = $listAreas;
        }
        else
        {
            $response["warning"] = Lang::get('messages.common_warning_empty_list');
        }

        return response()->json($response);
    }

    protected function apiGetListAreas(Request $request){

        $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        if($auth["auth"])
        {
            return response()->json($auth);
        }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $listAreas = array();

        $whereFunctions = array(
            ['area_type', '=', 1],
            ['area_parent_id', '=', 0],
            ['status', '<>', 0]
        );

        $listProvinces = DB::table('system_areas')
                          ->select('area_id', 
                                   'area_parent_id',
                                   'area_name', 
                                   'area_order',
                                   'area_type')
                          ->where($whereFunctions)
                          ->orderBy('area_name', 'asc')
                          ->get();

        $whereFunctions = array(
            ['area_type', '=', 2],
            ['area_parent_id', '<>', 0],
            ['status', '<>', 0]
        );

        $listDistricts = DB::table('system_areas')
                          ->select('area_id', 
                                   'area_parent_id',
                                   'area_name', 
                                   'area_order',
                                   'area_type')
                          ->where($whereFunctions)
                          ->orderBy('area_order', 'asc')
                          ->get();

        $whereFunctions = array(
            ['area_type', '=', 3],
            ['area_parent_id', '<>', 0],
            ['status', '<>', 0]
        );

        $listWards = DB::table('system_areas')
                          ->select('area_id', 
                                   'area_parent_id',
                                   'area_name',
                                   'area_order',
                                   'area_type')
                          ->where($whereFunctions)
                          ->orderBy('area_order', 'asc')
                          ->get();

        foreach ($listProvinces as $value_p) {

            $itemProvince = [
                "id" => $value_p->area_id,
                "parent_id" => $value_p->area_parent_id,
                "name" => $value_p->area_name,
                "type" => $value_p->area_type,
                "order" => $value_p->area_order,
                "nodes" => array(),
            ];

            foreach ($listDistricts as $value_d) {

                if($value_d->area_parent_id == $value_p->area_id)
                {
                    $itemDistrict = [
                        "id" => $value_d->area_id,
                        "parent_id" => $value_d->area_parent_id,
                        "name" => $value_d->area_name,
                        "type" => $value_d->area_type,
                        "order" => $value_d->area_order,
                        "nodes" => array(),
                    ];

                    foreach ($listWards as $value_w) {

                       if($value_w->area_parent_id == $value_d->area_id)
                        {
                            $itemWard = [
                                "id" => $value_w->area_id,
                                "parent_id" => $value_w->area_parent_id,
                                "name" => $value_w->area_name,
                                "type" => $value_w->area_type,
                                "order" => $value_w->area_order,
                            ]; 

                            array_push($itemDistrict["nodes"], $itemWard);
                        }

                    }

                    array_push($itemProvince["nodes"], $itemDistrict);
                }
            }
            array_push($listAreas, $itemProvince);
        }

        if(count($listAreas) > 0)
        {
            $response["success"] = $listAreas;
        }
        else
        {
            $response["warning"] = Lang::get('messages.common_warning_empty_list');
        }

        return response()->json($response);
    }

    protected function apiGetListProvinces(Request $request){

        $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        if($auth["auth"])
        {
            return response()->json($auth);
        }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $listProvinces = DB::table('system_areas AS ap')
                          ->select('ap.area_id as province_id', 
                                   'ap.area_name as province_name', 
                                   'ap.area_type as province_type')
                          ->where('ap.area_type','=',1)
                          ->orderBy('ap.area_order', 'asc')
                          ->get();

        if(count($listProvinces) > 0)
        {
            $response["success"] = $listProvinces;
        }
        else
        {
            $response["warning"] = Lang::get('messages.common_warning_empty_list');
        }

        return response()->json($response);
    }

    protected function apiGetListDistricts(Request $request){

        $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        if($auth["auth"])
        {
            return response()->json($auth);
        }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $listDistricts = DB::table('system_areas AS ap')
                          ->select('ap.area_id as district_id', 
                                   'ap.area_name as district_name', 
                                   'ap.area_type as district_type')
                          ->where('ap.area_type','=',2)
                          ->orderBy('ap.area_order', 'asc')
                          ->get();

        if(count($listDistricts) > 0)
        {
            $response["success"] = $listDistricts;
        }
        else
        {
            $response["warning"] = Lang::get('messages.common_warning_empty_list');
        }

        return response()->json($response);
    }

    protected function apiGetListWards(){

        $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        if($auth["auth"])
        {
            return response()->json($auth);
        }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $listWards = DB::table('system_areas_wards AS aw')
                      ->select('aw.ward_id', 
                               'aw.district_id', 
                               'aw.ward_name', 
                               'aw.ward_orders', 
                               'aw.status')
                      ->where('aw.status','<>',0)
                      ->get();

        if(count($listWards) > 0)
        {
            $response["success"] = $listWards;
        }
        else
        {
            $response["warning"] = Lang::get('messages.common_warning_empty_list');
        }

        return response()->json($response);
    }

    protected function apiGetListDistrictsByProvince(Request $request){

        $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        if($auth["auth"])
        {
            return response()->json($auth);
        }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        if(empty($request->input('list_province_id')))
        {
            $response["warning"] = "Không có mã tỉnh thành.";
            return response()->json($response);
        }

        $list_province_id = $request->input('list_province_id');

        $listDistricts = DB::table('system_areas_districts AS ad')
                        ->leftJoin('system_areas_provinces AS ap', 'ad.province_id', '=', 'ap.province_id')
                        ->select('ad.district_id', 
                                   'ad.province_id', 
                                   'ad.district_name', 
                                   'ad.district_orders', 
                                   'ad.status',
                                   'ap.province_name')
                        ->whereIn('ad.province_id', $list_province_id)
                        ->where('ad.status','<>',0)
                        ->get();

        if(count($listDistricts) > 0)
        {
            $response["success"] = $listDistricts;
        }
        else
        {
            $response["warning"] = Lang::get('messages.common_warning_empty_list');
        }

        return response()->json($response);
    }

    protected function apiGetListWardsByDistrict(Request $request){

        $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        if($auth["auth"])
        {
            return response()->json($auth);
        }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        if(empty($request->input('list_district_id')))
        {
            $response["warning"] = "Không có mã tỉnh thành.";
            return response()->json($response);
        }

        $list_district_id = $request->input('list_district_id');

        $district_id = $request->input('district_id');

        $listWards = DB::table('system_areas_wards AS aw')
                    ->leftJoin('system_areas_districts AS ad', 'aw.district_id', '=', 'ad.district_id')
                    ->leftJoin('system_areas_provinces AS ap', 'ad.province_id', '=', 'ap.province_id')
                    ->select('aw.ward_id', 
                               'aw.district_id', 
                               'aw.ward_name', 
                               'aw.ward_orders', 
                               'aw.status',
                               'ad.district_name',
                               'ap.province_name')
                    ->whereIn('ad.district_id', $list_district_id)
                    ->where('aw.status','<>',0)
                    ->get();

        if(count($listWards) > 0)
        {
            $response["success"] = $listWards;
        }
        else
        {
            $response["warning"] = Lang::get('messages.common_warning_empty_list');
        }

        return response()->json($response);
    }

    //Ajax Post Thêm
    protected function apiAdd(Request $request){  

        //Kiểm tra phân quyền sử dụng
        $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        if($auth["auth"])
        {
            return response()->json($auth);
        }
        $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        if(empty($request->input('area_type')))
        {
            $response["warning"] = ["Vui lòng chọn loại khu vực cần thêm mới."];
            return response()->json($response);
        }

        $itemAdd = null;

        if($request->input('area_type') === 1)
        {
            $rules = [
                'area_name' => 'required',
                'area_parent_id' => 'required',
                'area_type' => 'required',
            ];

            $valiData = $this->commonCtl->validRequest($request,$rules,$this->rulesMessProvince);

            if($valiData->fails())
            {   
                $response["warning"] = $valiData->errors();
                return response()->json($response);
            }

            $itemAdd = new SystemArea;

            $area_slug = $this->commonCtl->slugify($request->input('area_name'));
            if(empty($area_slug))
            {
                $response["error"] = Lang::get('messages.common_error_exception');
                return response()->json($response);
            }

            $whereFunctions = array(
                ['area_type', '=', 1],
                ['area_parent_id', '=', 0]
            );

            $listItem = DB::table('system_areas')
                              ->select('area_id')
                              ->where($whereFunctions)
                              ->get();

            $nextOrder = $listItem->count() + 1;

            $itemAdd->fill($request->all());

            $itemAdd->area_slug = $area_slug;
            $itemAdd->area_order = $nextOrder;

            $itemAdd->created_user = $this->sessionUser->user_id;
            $itemAdd->created_date = $this->commonCtl->getCarbonNow();
            $itemAdd->updated_user = $this->sessionUser->user_id;
            $itemAdd->updated_date = $this->commonCtl->getCarbonNow();

            $itemAdd->status = 1;
        }

        if($request->input('area_type') === 2)
        {
            $rules = [
                'area_name' => 'required',
                'area_parent_id' => 'required',
                'area_type' => 'required',
            ];

            $valiData = $this->commonCtl->validRequest($request,$rules,$this->rulesMessProvince);

            if($valiData->fails())
            {   
                $response["warning"] = $valiData->errors();
                return response()->json($response);
            }

            $itemAdd = new SystemArea;

            $area_slug = $this->commonCtl->slugify($request->input('area_name'));
            if(empty($area_slug))
            {
                $response["error"] = Lang::get('messages.common_error_exception');
                return response()->json($response);
            }

            $whereFunctions = array(
                ['area_type', '=', 2],
                ['area_parent_id', '<>', 0]
            );

            $listItem = DB::table('system_areas')
                              ->select('area_id')
                              ->where($whereFunctions)
                              ->get();

            $nextOrder = $listItem->count() + 1;

            $itemAdd->fill($request->all());

            $itemAdd->area_slug = $area_slug;
            $itemAdd->area_order = $nextOrder;

            $itemAdd->created_user = $this->sessionUser->user_id;
            $itemAdd->created_date = $this->commonCtl->getCarbonNow();
            $itemAdd->updated_user = $this->sessionUser->user_id;
            $itemAdd->updated_date = $this->commonCtl->getCarbonNow();

            $itemAdd->status = 1;
        }

        if($request->input('area_type') === 3)
        {
            $rules = [
                'area_name' => 'required',
                'area_parent_id' => 'required',
                'area_type' => 'required',
            ];

            $valiData = $this->commonCtl->validRequest($request,$rules,$this->rulesMessProvince);

            if($valiData->fails())
            {   
                $response["warning"] = $valiData->errors();
                return response()->json($response);
            }

            $itemAdd = new SystemArea;

            $area_slug = $this->commonCtl->slugify($request->input('area_name'));
            if(empty($area_slug))
            {
                $response["error"] = Lang::get('messages.common_error_exception');
                return response()->json($response);
            }

            $whereFunctions = array(
                ['area_type', '=', 3],
                ['area_parent_id', '<>', 0]
            );

            $listItem = DB::table('system_areas')
                              ->select('area_id')
                              ->where($whereFunctions)
                              ->get();

            $nextOrder = $listItem->count() + 1;

            $itemAdd->fill($request->all());

            $itemAdd->area_slug = $area_slug;
            $itemAdd->area_order = $nextOrder;

            $itemAdd->created_user = $this->sessionUser->user_id;
            $itemAdd->created_date = $this->commonCtl->getCarbonNow();
            $itemAdd->updated_user = $this->sessionUser->user_id;
            $itemAdd->updated_date = $this->commonCtl->getCarbonNow();

            $itemAdd->status = 1;
        }

        if(!empty($itemAdd))
        {
            // Start transaction!
            DB::beginTransaction();

            try {
                $itemAdd->save();

                $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_created'), ['Tỉnh Thành']);
                
            } 
            catch(ValidationException $e)
            {
                DB::rollback();

                // $response["error"] = $e->getErrors();
                $response["error"] = Lang::get('messages.common_error_exception');
                return response()->json($response);
            } 
            catch(\Exception $e)
            {
                DB::rollback();
                $response["error"] = Lang::get('messages.common_error_exception').$e;
                return response()->json($response);
            }

            DB::commit();
        }
        else
        {
            $response["error"] = Lang::get('messages.common_error_exception').$e;
            return response()->json($response);
        }

        return response()->json($response);
    } 

    protected function apiUpdate(Request $request){  

        //Kiểm tra phân quyền sử dụng
        $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        if($auth["auth"])
        {
            return response()->json($auth);
        }
        $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        if(empty($request->input('area_type')))
        {
            $response["warning"] = ["Vui lòng chọn loại khu vực cần cập nhật mới."];
            return response()->json($response);
        }

        $itemUpdate = null;

        $rules = [
            'area_id' => 'required',
            'area_name' => 'required',
            'area_parent_id' => 'required',
            'area_type' => 'required',
        ];
        
        $valiData = null;
        if($request->input('area_type') === 1)
        {
            $valiData = $this->commonCtl->validRequest($request,$rules,$this->rulesMessProvince);
        }
        else if($request->input('area_type') === 2)
        {
            $valiData = $this->commonCtl->validRequest($request,$rules,$this->rulesMessDistrict);
        }
        else
        {
            $valiData = $this->commonCtl->validRequest($request,$rules,$this->rulesMessWard);
        }

        if($valiData->fails())
        {   
            $response["warning"] = $valiData->errors();
            return response()->json($response);
        }

        $whereFunctions = array(
            ['area_id', '=', $request->input('area_id')],
            ['area_type', '=', $request->input('area_type')],
            ['status', '<>', 0]
        );

        $itemUpdate = SystemArea::where($whereFunctions)->first();

        if($request->input('area_parent_id') !== $itemUpdate->area_parent_id)
        {
            $whereFunctions = array(
                ['area_id', '=', $request->input('area_parent_id')],
                ['status', '<>', 0]
            );

            $parentItem = DB::table('system_areas')
                            ->select('area_id','area_type')
                            ->where($whereFunctions)
                            ->first();


            if($parentItem !== null)
            {
                if($parentItem->area_type !== 3)
                {
                    $itemUpdate->area_type = $parentItem->area_type + 1;
                }
                else
                {
                    $itemUpdate->area_type = $parentItem->area_type;
                }

                $itemUpdate->area_parent_id = $parentItem->area_id;
            }
            else
            {
                $itemUpdate->area_type = 1;
                $itemUpdate->area_parent_id = 0;
            }
        }

        $itemUpdate->area_name = $request->input('area_name');

        $itemUpdate->updated_user = $this->sessionUser->user_id;
        $itemUpdate->updated_date = $this->commonCtl->getCarbonNow();

        $itemUpdate->status = 1;
        
        if(!empty($itemUpdate))
        {
            // Start transaction!
            DB::beginTransaction();

            try {

                $itemUpdate->save();


                $whereFunctions = array(
                    ['area_parent_id', '=', $itemUpdate->area_id]
                );

                $childItems = DB::table('system_areas')
                                ->select('area_id')
                                ->where($whereFunctions)
                                ->get();

                if(count($childItems) > 0)
                {
                    $whereFunctions = array(
                        ['area_parent_id', '=', $itemUpdate->area_id]
                    );

                    if($itemUpdate->area_type !== 3)
                    {
                        $updatedRows = DB::table('system_areas')
                            ->where($whereFunctions)
                            ->update(
                                array(
                                    'area_type' => $itemUpdate->area_type + 1,
                                    'updated_user' => $this->sessionUser->user_id,
                                    'updated_date' => $this->commonCtl->getCarbonNow()
                                )
                            );
                    }
                    else
                    {
                        $deletedRows = DB::table('system_areas')
                            ->where($whereFunctions)
                            ->update(
                                array(
                                    'status' => 0,
                                    'updated_user' => $this->sessionUser->user_id,
                                    'updated_date' => $this->commonCtl->getCarbonNow()
                                )
                            );
                    }
                }

                $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_updated'), [$itemUpdate->update]);
                
            } 
            catch(ValidationException $e)
            {
                DB::rollback();

                // $response["error"] = $e->getErrors();
                $response["error"] = Lang::get('messages.common_error_exception');
                return response()->json($response);
            } 
            catch(\Exception $e)
            {
                DB::rollback();
                $response["error"] = Lang::get('messages.common_error_exception');
                return response()->json($response);
            }

            DB::commit();
        }
        else
        {
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }

        return response()->json($response);
    } 

    protected function apiChangeStatus(Request $request){ 

        //Kiểm tra phân quyền sử dụng
        $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        if($auth["auth"])
        {
            return response()->json($auth);
        }
        $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        if(empty($request->input('area_type')))
        {
            $response["warning"] = ["Vui lòng chọn loại khu vực."];
            return response()->json($response);
        }

        $rules = [
            'area_id' => 'required',
        ];

        $valiData = null;

        $area_type_name = "";

        if($request->input('area_type') == 1)
        {
            $valiData = $this->commonCtl->validRequest($request,$rules,$this->rulesMessProvince);
            $area_type_name = "Tỉnh Thành Phố";
        }
        else if($request->input('area_type') == 2)
        {
            $valiData = $this->commonCtl->validRequest($request,$rules,$this->rulesMessDistrict);
            $area_type_name = "Quận Huyện";
        }
        else
        {
            $valiData = $this->commonCtl->validRequest($request,$rules,$this->rulesMessWard);
            $area_type_name = "Phường Xã";
        }
        
        if($valiData->fails())
        {   
            $response["warning"] = $valiData->errors();
            return response()->json($response);
        }

        $whereFunctions = array(
            ['area_id', '=', $request->input('area_id')],
            ['area_type', '=', $request->input('area_type')]
        );

        $itemDelete = SystemArea::where($whereFunctions)->first();
        if($itemDelete == null)
        {
            $response["error"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [" ".$area_type_name]);
            return response()->json($response);
        }


        $listItem = array();
        if($request->input('area_type') == 1)
        {
            $where = array(
                ['area_type', '=', 2],
                ['area_parent_id', '=', $itemDelete->area_id],
                ['status', '<>', 0],
            );

            $listItem = DB::table('system_areas')
                            ->select('area_id')
                            ->where($where)
                            ->get();
        }
        else if($request->input('area_type') == 2)
        {
            $where = array(
                ['area_type', '=', 3],
                ['area_parent_id', '=', $itemDelete->area_id],
                ['status', '<>', 0],
            );

            $listItem = DB::table('system_areas')
                            ->select('area_id')
                            ->where($where)
                            ->get();
        }

        if(count($listItem) > 0)
        {
            $response["warning"] = ["Vui lòng xóa khu vực của ".$itemDelete->area_name." trước."];
            return response()->json($response);
        }

        // Start transaction!
        DB::beginTransaction();

        try {

            $updatedRows = DB::table('system_areas')
                        ->where($whereFunctions)
                        ->update(
                            array(
                                'status' => 0,
                                'updated_user' => $this->sessionUser->user_id,
                                'updated_date' => $this->commonCtl->getCarbonNow()
                            )
                        );

            if($updatedRows > 0){
                $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_removed'), [$area_type_name]);
            }
            else{
                $response["warning"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_removed'), [$area_type_name]);
                DB::rollback();
            }
        } 
        catch(ValidationException $e)
        {
            // Rollback and then redirect
            // back to form with errors
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
            // throw $e;
        }

        // Commit the queries!
        DB::commit();

        return response()->json($response);
    }
}
