<?php

namespace App\Http\Controllers\SystemApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use DateTime;
use Validator;
use Mail;
use Lang;

use Debugbar;

use App\Http\Controllers\SystemApi\SystemCommonController as commonCtl;
use App\Models\NewsCategoriesDetail;
use App\Models\NewsDetail;


class SystemApiNewsCategoriesController extends Controller
{

   /*** Khởi Tạo Giá Trị ***/
   private $commonCtl;

   private $sessionUser;

   // private $sessionUser;

   // private $rulesMess = [
   //     'news_cate_name.required' => 'We need to know your news_cate_name!',
   //     'news_cate_date_public.required' => 'We need to know your news_cate_date_public!',
   //     'status.required' => 'We need to know your news_cate_status!',
   // ];
   /*** Khởi Tạo Giá Trị ***/

   public function __construct(commonCtl $SystemCommonController){
      $this->commonCtl =  $SystemCommonController;
      // $this->sessionUser =  Session::get('userAuth');
   }
   //Ajax Post Lấy Danh Sách Thể Loại
   protected function apiGetList(Request $request){  

      $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
      if($auth["auth"])
      {
         return response()->json($auth);
      }

      //Tạo biến kiểm tra kết quả
      $response["success"] = "";
      $response["warning"] = "";
      $response["error"] = "";

      $listCategories = DB::table('news_categories_detail AS A')
                  ->select('A.news_cate_id',
                           'A.news_cate_slug', 
                           'A.news_cate_thumbnail',  
                           'A.news_cate_name', 
                           'A.news_cate_description', 
                           'A.status')
                  ->where([
                      ['A.status','<>',0],
                      ['A.news_cate_id','<>',1]
                    ])
                  ->get();

      if(!$listCategories->isEmpty())
      {
         $response["success"] = $listCategories;
      }
      else
      {
         $response["warning"] = Lang::get('messages.common_warning_empty_list');
      }

      return response()->json($response);
   }
   //Ajax Post Thêm
   protected function apiAdd(Request $request){  
        
        //Kiểm tra phân quyền sử dụng
        $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        if($auth["auth"])
        {
            return response()->json($auth);
        }
        $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        /** Khởi tạo giá trị **/
        $news_cate_slug = $this->commonCtl->slugify($request->input('news_cate_name'));
        if(empty($news_cate_slug))
        {
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }
        // $response["error"] = "$news_cate_slug";
        //  return response()->json($response);


        $existsItem = NewsCategoriesDetail::where('news_cate_slug','=',$news_cate_slug)
                        ->first();
        if(!empty($existsItem))
        {
            $response["warning"] = [$this->commonCtl->replaceTitle(Lang::get('messages.common_error_exist_data'), ['Tên', 'nhóm tin'])];
            return response()->json($response);
        }                

        $itemAdd = new NewsCategoriesDetail;

        $itemAdd->fill($request->all());
        $itemAdd->news_cate_slug = $news_cate_slug;
        $itemAdd->created_user = $this->sessionUser->user_id;
        $itemAdd->created_date = $this->commonCtl->getCarbonNow();
        $itemAdd->updated_user = $this->sessionUser->user_id;
        $itemAdd->updated_date = $this->commonCtl->getCarbonNow();
        $itemAdd->status = 1;

        DB::beginTransaction();

        try {
            $itemAdd->save();

            $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_created'), ['nhóm tin']);
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);

        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);

        }
        DB::commit();

        return response()->json($response);
    } 

    //Ajax Post Thêm
    protected function apiUpdate(Request $request){  

        //Kiểm tra phân quyền sử dụng
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }
        // $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";


        $existsItem = NewsCategoriesDetail::where('news_cate_slug','=',$request->input('news_cate_slug'))
                        ->first();

        if(empty($existsItem))
        {
            $response["warning"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' nhóm tin']);
            return response()->json($response);
        }

        $news_cate_slug = $this->commonCtl->slugify($request->input('news_cate_name'));
        if(empty($news_cate_slug))
        {
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }

        $existsSlug = NewsCategoriesDetail::where([
                                    ['news_cate_slug','=',$news_cate_slug],
                                    ['news_cate_id','<>',$existsItem->news_cate_id],
                                ])->first();
        if($existsSlug)
        {
            $response["warning"] = [$this->commonCtl->replaceTitle(Lang::get('messages.common_error_exist_data'), ['Tên', 'nhóm tin'])];
            return response()->json($response);
        }

        $existsItem->news_cate_slug = $news_cate_slug;
        $existsItem->news_cate_name = $request->input('news_cate_name');
        $existsItem->news_cate_description = $request->input('news_cate_description');
        $existsItem->updated_user = Session::get('userAuth')->user_id;
        $existsItem->updated_date = $this->commonCtl->getCarbonNow();

        DB::beginTransaction();
        try 
        {
            $existsItem->save();

            $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_updated'), ['nhóm tin']);
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }

        DB::commit();

        return response()->json($response);
    } 

    //Ajax Post Xóa Thẻ
    protected function apiRemove(Request $request){ 

        // //Kiểm tra phân quyền sử dụng
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }
        // $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";


        $itemDelete = NewsCategoriesDetail::where('news_cate_id', '=', $request->input('news_cate_id'))->first();

        if(empty($itemDelete))
        {
            $response["error"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' nhóm tin']);
            return response()->json($response);
        }

        $checkExists = NewsDetail::where('cate_id', '=', $request->input('news_cate_id'))->get();

        DB::beginTransaction();
        try {

            if(!$checkExists->isEmpty())
            {
                $itemDelete->updated_user = Session::get('userAuth')->user_id;
                $itemDelete->updated_date = $this->commonCtl->getCarbonNow();
                $itemDelete->status = 0;

                $itemDelete->save();

                $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_removed'), ['nhóm tin']);
            }
            else
            {
                $deletedRows = NewsCategoriesDetail::where('news_cate_id', '=', $itemDelete->news_cate_id)->delete();

                if($deletedRows > 0){
                    $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_removed'), ['nhóm tin']);
                }
                else{
                    $response["warning"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_removed'), ['nhóm tin']);
                }
            }
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }
        DB::commit();

        return response()->json($response);
    }
}
