<?php

namespace App\Http\Controllers\SystemApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use DateTime;
use Validator;
use Mail;
use Lang;

use App\Http\Controllers\SystemApi\SystemCommonController as commonCtl;
use App\Models\CustomersDetail;

class SystemApiStatisticalController extends Controller
{

    /*** Khởi Tạo Giá Trị ***/
    private $commonCtl;

    // private $sessionUser;


    public function __construct(commonCtl $SystemCommonController){
        $this->commonCtl =  $SystemCommonController;
        // $this->sessionUser =  Session::get('userAuth');
    }

    /*** API Hàm Chức Năng ***/  
    // apiUpdateCustomerStatus
    protected function apiUpdateCustomerStatus(){  

        $whereFunctions = array();

        $listCustomers = DB::table('customers_detail AS cu')
                ->select( 'cu.group_id',
                    'cu.customer_id',
                    'cu.customer_account',
                    'cu.customer_email',
                    'cu.customer_fullname',
                    'cu.customer_address',
                    'cu.area_id',
                    'cu.customer_phone',
                    'cu.customer_birthday',
                    'cu.customer_password',
                    'cu.customer_login_count',
                    'cu.customer_secure_code',
                    'cu.customer_avatar',
                    'cu.customer_identify',
                    'cu.customer_student_card',
                    'cu.customer_university',
                    DB::raw("DATEDIFF(NOW(),cu.created_date) AS customer_datediff"),
                    'cu.created_date',
                    'cu.created_user',
                    'cu.updated_date',
                    'cu.updated_user',
                    'cu.status'
                         )
                ->where($whereFunctions)
                ->orderBy('cu.updated_date', 'desc')
                ->get();
        // cập nhật lại trạng thái nếu chưa thanh toán
        DB::beginTransaction();
        
        try {
            if(!$listCustomers->isEmpty())
            {
                foreach ($listCustomers as $item) {
                    if($item->customer_datediff >= 2 && $item->status==1)
                    {
                        $updatedRows = CustomersDetail::where('customer_id', '=', $item->customer_id)
                            ->update([
                                'status' => 3,
                                'updated_date' => $this->commonCtl->getCarbonNow()
                            ]);
                    }
                }
            }
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            return "no";
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            return "no";
        }

        DB::commit();
        return "yes";
    }
     //Ajax Post Tính Chỉ Sô Dashboard
    protected function apiGetNumeral(Request $request){  


        // if($this->commonCtl->checkAuthAPI($request->header('X-CSRF-TOKEN')))
        // {
        //     $response["error"] = Lang::get('messages.common_error_permission_api');
        //     return response()->json($response);
        // }

        $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        if($auth["auth"])
        {
            return response()->json($auth);
        }

        //Tạo biến kiểm tra kết quả
        $response["success"] = array();
        $response["warning"] = "";
        $response["error"] = "";

        $type = $request->input('type');

        if($type == "m")
        {
            $firstDay   = $request->input('firstDay');
            $lastDay    = $request->input('lastDay');
            $month      = $request->input('month');
            $year       = $request->input('year');

            for ($i = $firstDay; $i <= $lastDay; $i++) {

                $whereFunctions = array(
                    [DB::raw('MONTH(created_date)'), '=', $month],
                    [DB::raw('YEAR(created_date)'), '=', $year],
                    [DB::raw('DAY(created_date)'), '=', $i],
                    ['status', '=', '5']
                );

                $index = DB::table('invoice')
                    ->select(DB::raw('count(invoice_id) as total_invoice_price'))
                    ->where($whereFunctions)
                    ->first()->total_invoice_price;

                array_push($response["success"], $index);
            }
        }

        return response()->json($response);
    }
    //Ajax Post Thống Kê Dashboard
    protected function apiGetDashboard(Request $request){  
        // if($this->commonCtl->checkAuthAPI($request->header('X-CSRF-TOKEN')))
        // {
        //     $response["error"] = Lang::get('messages.common_error_permission_api');
        //     return response()->json($response);
        // }
        $this->apiUpdateCustomerStatus();
        $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        if($auth["auth"])
        {
            return response()->json($auth);
        }
        //Tạo biến kiểm tra kết quả
        $response["success"] = array("totalNews" => 0, "totalView" => 0, "totalComments" => 0, "totalRevenue" => 0);
        $response["warning"] = "";
        // Tong Hóa Dơn
        $response["success"]["totalInvoice"]= DB::table('invoice')
            ->select(DB::raw('count(invoice_id) as invoice_couter'))
            ->first()->invoice_couter;
        // tong doanh thu : don hang đã xử lý
        $whereFunctions = array(['status', '=', '5']);
        $response["success"]["totalInvoicePrice"]= DB::table('invoice')
            ->select(DB::raw('SUM(total_sum) as total_invoice_price'))
            ->where($whereFunctions)
            ->first()->total_invoice_price;
        // tong  : don hang Chưa xử lý
        $whereFunctions = array(['status', '=', '2']);
        $response["success"]["uncompletedInvoice"]= DB::table('invoice')
            ->select(DB::raw('count(invoice_id) as uncompleted_invoice'))
            ->where($whereFunctions)
            ->first()->uncompleted_invoice;
        // tổng lượt View
        $response["success"]["totalView"]= DB::table('system_counter')
            ->select(DB::raw('count(counter_id) as views_couter'))
            ->first()->views_couter;
        // tổng lượt View
        $response["success"]["totalProduct"]= DB::table('products_detail')
            ->select(DB::raw('count(product_id) as product_couter'))
            ->first()->product_couter;
        // tổng Tin tuc
        $response["success"]["totalNews"]= DB::table('news_detail')
            ->select(DB::raw('count(news_id) as news_couter'))
            ->first()->news_couter;
        // lượt view theo ngày
        $response["success"]["totalViewByDay"]=DB::select("
            select COUNT(*) AS view_count,
            DATE_FORMAT(sy.counter_date, '%d/%m/%Y')
            FROM system_counter AS sy
            WHERE DATE_FORMAT(sy.counter_date, '%d/%m/%Y') = DATE_FORMAT(NOW(), '%d/%m/%Y')
            GROUP BY DATE_FORMAT(sy.counter_date, '%d/%m/%Y')
            ");
        // lượt view theo tháng
        $response["success"]["totalViewByMonth"]=DB::select("
            select COUNT(*) AS view_count,
            DATE_FORMAT(sy.counter_date, '%m/%Y')
            FROM system_counter AS sy
            WHERE DATE_FORMAT(sy.counter_date, '%m/%Y') = DATE_FORMAT(NOW(), '%m/%Y')
            GROUP BY DATE_FORMAT(sy.counter_date, '%m/%Y')
            ");
        // lượt view theo năm
        $response["success"]["totalViewByYear"]=DB::select("
            select COUNT(*) AS view_count,
            DATE_FORMAT(sy.counter_date, '%Y')
            FROM system_counter AS sy
            WHERE DATE_FORMAT(sy.counter_date, '%Y') = DATE_FORMAT(NOW(), '%Y')
            GROUP BY DATE_FORMAT(sy.counter_date, '%Y')
            ");

        return response()->json($response);
    }  
    //Ajax Post Thống Kê Truy Cập
    protected function apiGetMapVector(Request $request){  

        //Kiểm tra phân quyền sử dụng
        $auth = $this->commonCtl->checkRolesApi($request, "/System/Dashboard");
        if($auth["auth"])
        {
            return response()->json($auth);
        }
        $this->sessionUser = $this->commonCtl->checkUserAuth($request);

        //Tạo biến kiểm tra kết quả
        $response["success"] = array();
        $response["warning"] = "";

        $index = DB::table('system_counter')
            ->select('counter_id',
                     'counter_client_ip',
                     'counter_date',
                     'counter_browser_name_pattern',
                     'counter_browser',
                     'counter_browser_version',
                     'counter_platform',
                     'counter_country_code',
                     'counter_country_name',
                     'counter_region_code',
                     'counter_region_name',
                     'counter_city',
                     'counter_zip_code',
                     'counter_latitude',
                     'counter_longitude',
                     'counter_isp',
                     'counter_org',
                     'counter_as',
                     'counter_time_zone')
            ->orderBy('counter_date','desc')
            ->limit(5000)
            ->get();

        if(count($index)>0)
        {
            array_push($response["success"],$index);
        }
        else
        {
            $response["warning"] = "Không có dữ liệu thống kê";
        }
        

        return response()->json($response);
    }  
    // apiGetCustomerSale
    protected function apiGetCustomerSale(Request $request){  
        
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $strQuery = "
            select
            cu.customer_fullname, 
            cu.customer_phone,
            cu.customer_address,
            cu.customer_identify,
            cu.customer_student_card,
            cu.customer_email,
            cu.customer_university,
            CASE WHEN (SUM(iv.total_sum) IS NOT NULL) THEN SUM(iv.total_sum) ELSE 0 END AS total_sum_format
            FROM customers_detail AS cu
            LEFT JOIN invoice AS iv ON iv.customer_id = cu.customer_id
            WHERE iv.status=5 OR iv.status=7
            GROUP BY
            cu.customer_fullname, 
            cu.customer_phone,
            cu.customer_address,
            cu.customer_identify,
            cu.customer_student_card,
            cu.customer_email,
            cu.customer_university
        ";
        $listItem = DB::select($strQuery);
   
        if(count($listItem)>0)
        {
            $response["success"] =  $listItem;
        }
        else
        {
            $response["warning"] = "Không có dữ liệu thống kê";
        }

        return response()->json($response);
    }
    // apiGetCustomerDebtSale
    protected function apiGetCustomerDebtSale(Request $request){  
        
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";
        if(Session::get('userAuth')==null){
            $response["error"] = "Hết Phiên Làm Việc, Vui Lòng Đăng Nhập";
            return response()->json($response);
        }
        $strQuery = "
            select
            cu.customer_fullname, 
            cu.customer_phone,
            cu.customer_address,
            cu.customer_identify,
            cu.customer_student_card,
            cu.customer_email,
            cu.customer_university,
            CASE WHEN (SUM(iv.total_sum) IS NOT NULL) THEN SUM(iv.total_sum) ELSE 0 END AS total_sum_format
            FROM customers_detail AS cu
            LEFT JOIN invoice AS iv ON iv.customer_id = cu.customer_id
            WHERE iv.status=4 OR iv.status=6
            GROUP BY
            cu.customer_fullname, 
            cu.customer_phone,
            cu.customer_address,
            cu.customer_identify,
            cu.customer_student_card,
            cu.customer_email,
            cu.customer_university
        ";
        $listItem = DB::select($strQuery);
   
        if(count($listItem)>0)
        {
            $response["success"] =  $listItem;
        }
        else
        {
            $response["warning"] = "Không có dữ liệu thống kê";
        }

        return response()->json($response);
    }
    // apiGetProductSale
    protected function apiGetProductSale(Request $request){  
        
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $strQuery = "
            select ca.cate_name,
            pd.product_name,
            pd.product_thumbnail, 
            pd.product_retail_prices,
            SUM(id.total_price) AS total_sum_format
            FROM invoice AS iv
            LEFT JOIN invoice_detail AS id ON id.invoice_id = iv.invoice_id
            LEFT JOIN products_detail as pd ON pd.product_id=id.product_id
            LEFT JOIN products_categories_detail AS ca ON ca.cate_id=pd.cate_id
            WHERE iv.status=5 OR iv.status=7
            GROUP BY
            ca.cate_name,
            pd.product_name,
            pd.product_thumbnail, 
            pd.product_retail_prices
        ";
        $listItem = DB::select($strQuery);
   
        if(count($listItem)>0)
        {
            $response["success"] =  $listItem;
        }
        else
        {
            $response["warning"] = "Không có dữ liệu thống kê";
        }

        return response()->json($response);
    }
    // apiGetProductDebtSale
    protected function apiGetProductDebtSale(Request $request){  
        
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";
        if(Session::get('userAuth')==null){
            $response["error"] = "Hết Phiên Làm Việc, Vui Lòng Đăng Nhập";
            return response()->json($response);
        }

        $strQuery = "
            select ca.cate_name,
            pd.product_name,
            pd.product_thumbnail, 
            pd.product_retail_prices,
            SUM(id.total_price) AS total_sum_format
            FROM invoice AS iv
            LEFT JOIN invoice_detail AS id ON id.invoice_id = iv.invoice_id
            LEFT JOIN products_detail as pd ON pd.product_id=id.product_id
            LEFT JOIN products_categories_detail AS ca ON ca.cate_id=pd.cate_id
            WHERE iv.status=4 OR iv.status=6
            GROUP BY
            ca.cate_name,
            pd.product_name,
            pd.product_thumbnail, 
            pd.product_retail_prices
        ";
        $listItem = DB::select($strQuery);
   
        if(count($listItem)>0)
        {
            $response["success"] =  $listItem;
        }
        else
        {
            $response["warning"] = "Không có dữ liệu thống kê";
        }

        return response()->json($response);
    }
    // apiGetBranchSale
    protected function apiGetBranchSale(Request $request){  
        
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $strQuery = "
            select 
            br.branch_name,
            br.branch_address_number,
            br.branch_phone,
            SUM(iv.total_sum) AS total_sum_format
            FROM invoice AS iv
            LEFT JOIN branches_detail AS br ON br.branch_id = iv.branch_id
            WHERE iv.status=5 OR iv.status=7
            GROUP BY 
            br.branch_name,
            br.branch_address_number,
            br.branch_phone
        ";
        $listItem = DB::select($strQuery);
   
        if(count($listItem)>0)
        {
            $response["success"] =  $listItem;
        }
        else
        {
            $response["warning"] = "Không có dữ liệu thống kê";
        }

        return response()->json($response);
    }
    // apiGetBranchDebtSale
    protected function apiGetBranchDebtSale(Request $request){  
        
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $strQuery = "
            select 
            br.branch_name,
            br.branch_address_number,
            br.branch_phone,
            SUM(iv.total_sum) AS total_sum_format
            FROM invoice AS iv
            LEFT JOIN branches_detail AS br ON br.branch_id = iv.branch_id
            WHERE iv.status=4 OR iv.status=6
            GROUP BY 
            br.branch_name,
            br.branch_address_number,
            br.branch_phone
        ";
        $listItem = DB::select($strQuery);
   
        if(count($listItem)>0)
        {
            $response["success"] =  $listItem;
        }
        else
        {
            $response["warning"] = "Không có dữ liệu thống kê";
        }

        return response()->json($response);
    }
    // apiGetSubadminSale
    protected function apiGetSubadminSale(Request $request){  
        
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $strQuery = "
            select 
            iv.updated_user,
            us.user_avatar,
            us.user_account,
            us.user_fullname,
            us.user_phone,
            us.user_email,
            us.auth_id,
            SUM(iv.total_sum) AS total_sum_format
            FROM invoice AS iv
            LEFT JOIN system_users AS us ON us.user_id=iv.updated_user
            WHERE iv.status=5 OR iv.status=7
            GROUP BY iv.updated_user,
            us.user_avatar,
            us.user_account,
            us.user_fullname,
            us.user_phone,
            us.user_email,
            us.auth_id
        ";
        $listItem = DB::select($strQuery);
   
        if(count($listItem)>0)
        {
            $response["success"] =  $listItem;
        }
        else
        {
            $response["warning"] = "Không có dữ liệu thống kê";
        }

        return response()->json($response);
    }
    // apiGetSubadminDebtSale
    protected function apiGetSubadminDebtSale(Request $request){  
        
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $strQuery = "
            select 
            iv.updated_user,
            us.user_avatar,
            us.user_account,
            us.user_fullname,
            us.user_phone,
            us.user_email,
            us.auth_id,
            SUM(iv.total_sum) AS total_sum_format
            FROM invoice AS iv
            LEFT JOIN system_users AS us ON us.user_id=iv.updated_user
            WHERE iv.status=4 OR iv.status=6
            GROUP BY iv.updated_user,
            us.user_avatar,
            us.user_account,
            us.user_fullname,
            us.user_phone,
            us.user_email,
            us.auth_id
        ";
        $listItem = DB::select($strQuery);
   
        if(count($listItem)>0)
        {
            $response["success"] =  $listItem;
        }
        else
        {
            $response["warning"] = "Không có dữ liệu thống kê";
        }

        return response()->json($response);
    }
    // ===========================Thống Kê Cá Nhân==========================
    // apiGetPrivateCustomerSale
    protected function apiGetPrivateCustomerSale(Request $request){  
        
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";
        if(Session::get('userAuth')==null){
            $response["error"] = "Hết Phiên Làm Việc, Vui Lòng Đăng Nhập";
            return response()->json($response);
        }
        $strQuery = "
            select
            cu.customer_fullname, 
            cu.customer_phone,
            cu.customer_address,
            cu.customer_identify,
            cu.customer_student_card,
            cu.customer_email,
            cu.customer_university,
            CASE WHEN (SUM(iv.total_sum) IS NOT NULL) THEN SUM(iv.total_sum) ELSE 0 END AS total_sum_format
            FROM customers_detail AS cu
            LEFT JOIN invoice AS iv ON iv.customer_id = cu.customer_id
            WHERE iv.status=5 OR iv.status=7
            AND iv.updated_user=".Session::get('userAuth')->user_id."
            GROUP BY
            cu.customer_fullname, 
            cu.customer_phone,
            cu.customer_address,
            cu.customer_identify,
            cu.customer_student_card,
            cu.customer_email,
            cu.customer_university
        ";
        $listItem = DB::select($strQuery);
   
        if(count($listItem)>0)
        {
            $response["success"] =  $listItem;
        }
        else
        {
            $response["warning"] = "Không có dữ liệu thống kê";
        }

        return response()->json($response);
    }
    // apiGetPrivateCustomerDebtSale
    protected function apiGetPrivateCustomerDebtSale(Request $request){  
        
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";
        if(Session::get('userAuth')==null){
            $response["error"] = "Hết Phiên Làm Việc, Vui Lòng Đăng Nhập";
            return response()->json($response);
        }
        $strQuery = "
            select
            cu.customer_fullname, 
            cu.customer_phone,
            cu.customer_address,
            cu.customer_identify,
            cu.customer_student_card,
            cu.customer_email,
            cu.customer_university,
            CASE WHEN (SUM(iv.total_sum) IS NOT NULL) THEN SUM(iv.total_sum) ELSE 0 END AS total_sum_format
            FROM customers_detail AS cu
            LEFT JOIN invoice AS iv ON iv.customer_id = cu.customer_id
            WHERE (iv.status=4 OR iv.status=6)
            AND iv.updated_user=".Session::get('userAuth')->user_id."
            GROUP BY
            cu.customer_fullname, 
            cu.customer_phone,
            cu.customer_address,
            cu.customer_identify,
            cu.customer_student_card,
            cu.customer_email,
            cu.customer_university
        ";
        $listItem = DB::select($strQuery);
   
        if(count($listItem)>0)
        {
            $response["success"] =  $listItem;
        }
        else
        {
            $response["warning"] = "Không có dữ liệu thống kê";
        }

        return response()->json($response);
    }
    // apiGetPrivateProductSale
    protected function apiGetPrivateProductSale(Request $request){  
        
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";
        if(Session::get('userAuth')==null){
            $response["error"] = "Hết Phiên Làm Việc, Vui Lòng Đăng Nhập";
            return response()->json($response);
        }

        $strQuery = "
            select ca.cate_name,
            pd.product_name,
            pd.product_thumbnail, 
            pd.product_retail_prices,
            SUM(id.total_price) AS total_sum_format
            FROM invoice AS iv
            LEFT JOIN invoice_detail AS id ON id.invoice_id = iv.invoice_id
            LEFT JOIN products_detail as pd ON pd.product_id=id.product_id
            LEFT JOIN products_categories_detail AS ca ON ca.cate_id=pd.cate_id
            WHERE (iv.status=5 OR iv.status=7)
            AND iv.updated_user=".Session::get('userAuth')->user_id."
            GROUP BY
            ca.cate_name,
            pd.product_name,
            pd.product_thumbnail, 
            pd.product_retail_prices
        ";
        $listItem = DB::select($strQuery);
   
        if(count($listItem)>0)
        {
            $response["success"] =  $listItem;
        }
        else
        {
            $response["warning"] = "Không có dữ liệu thống kê";
        }

        return response()->json($response);
    }
    // apiGetPrivateProductDebtSale
    protected function apiGetPrivateProductDebtSale(Request $request){  
        
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";
        if(Session::get('userAuth')==null){
            $response["error"] = "Hết Phiên Làm Việc, Vui Lòng Đăng Nhập";
            return response()->json($response);
        }

        $strQuery = "
            select ca.cate_name,
            pd.product_name,
            pd.product_thumbnail, 
            pd.product_retail_prices,
            SUM(id.total_price) AS total_sum_format
            FROM invoice AS iv
            LEFT JOIN invoice_detail AS id ON id.invoice_id = iv.invoice_id
            LEFT JOIN products_detail as pd ON pd.product_id=id.product_id
            LEFT JOIN products_categories_detail AS ca ON ca.cate_id=pd.cate_id
            WHERE (iv.status=4 OR iv.status=6)
            AND iv.updated_user=".Session::get('userAuth')->user_id."
            GROUP BY
            ca.cate_name,
            pd.product_name,
            pd.product_thumbnail, 
            pd.product_retail_prices
        ";
        $listItem = DB::select($strQuery);
   
        if(count($listItem)>0)
        {
            $response["success"] =  $listItem;
        }
        else
        {
            $response["warning"] = "Không có dữ liệu thống kê";
        }

        return response()->json($response);
    }
    // apiGetPrivateSubadminSale
    protected function apiGetPrivateSubadminSale(Request $request){  
        
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";
        if(Session::get('userAuth')==null){
            $response["error"] = "Hết Phiên Làm Việc, Vui Lòng Đăng Nhập";
            return response()->json($response);
        }

        $strQuery = "
            select 
            iv.updated_user,
            us.user_avatar,
            us.user_account,
            us.user_fullname,
            us.user_phone,
            us.user_email,
            us.auth_id,
            SUM(iv.total_sum) AS total_sum_format
            FROM invoice AS iv
            LEFT JOIN system_users AS us ON us.user_id=iv.updated_user
            WHERE (iv.status=5 OR iv.status=7)
            AND us.user_id=".Session::get('userAuth')->user_id."
            GROUP BY iv.updated_user,
            us.user_avatar,
            us.user_account,
            us.user_fullname,
            us.user_phone,
            us.user_email,
            us.auth_id
        ";
        $listItem = DB::select($strQuery);
   
        if(count($listItem)>0)
        {
            $response["success"] =  $listItem;
        }
        else
        {
            $response["warning"] = "Không có dữ liệu thống kê";
        }

        return response()->json($response);
    }
    // apiGetPrivateSubadminDebtSale
    protected function apiGetPrivateSubadminDebtSale(Request $request){  
        
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";
        if(Session::get('userAuth')==null){
            $response["error"] = "Hết Phiên Làm Việc, Vui Lòng Đăng Nhập";
            return response()->json($response);
        }

        $strQuery = "
            select 
            iv.updated_user,
            us.user_avatar,
            us.user_account,
            us.user_fullname,
            us.user_phone,
            us.user_email,
            us.auth_id,
            SUM(iv.total_sum) AS total_sum_format
            FROM invoice AS iv
            LEFT JOIN system_users AS us ON us.user_id=iv.updated_user
            WHERE (iv.status=4 OR iv.status=6)
            AND us.user_id=".Session::get('userAuth')->user_id."
            GROUP BY iv.updated_user,
            us.user_avatar,
            us.user_account,
            us.user_fullname,
            us.user_phone,
            us.user_email,
            us.auth_id
        ";
        $listItem = DB::select($strQuery);
   
        if(count($listItem)>0)
        {
            $response["success"] =  $listItem;
        }
        else
        {
            $response["warning"] = "Không có dữ liệu thống kê";
        }

        return response()->json($response);
    }
    // Get total sale by datetime
    protected function apiGetTotalSaleByDateTime(Request $request){  
        
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $strQuery = "
            select  MONTH(iv.updated_date) as month,
            YEAR(iv.updated_date) as year,
            SUM(id.total_price) AS total_sum
            FROM invoice AS iv
            LEFT JOIN invoice_detail AS id ON id.invoice_id = iv.invoice_id
            WHERE iv.status=5 OR iv.status=7
            GROUP BY MONTH(iv.updated_date),YEAR(iv.updated_date)
        ";
        $listItem = DB::select($strQuery);
   
        if(count($listItem)>0)
        {
            $response["success"] =  $listItem;
        }
        else
        {
            $response["warning"] = "Không có dữ liệu thống kê";
        }

        return response()->json($response);
    }
    // Get total sale by datetime and Branch
    protected function apiGetTotalSaleByDateTimeBranch(Request $request){  
        
        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";
        if($request->input('branch_id')==null){
            $response["error"] = "Không Tìm Thấy Mã Chy Nhánh";
        }
        
        $strQuery = "
            select  MONTH(iv.updated_date) as month,
            YEAR(iv.updated_date) as year,
            SUM(id.total_price) AS total_sum
            FROM invoice AS iv
            LEFT JOIN invoice_detail AS id ON id.invoice_id = iv.invoice_id
            WHERE (iv.status=5 OR iv.status=7)
            AND iv.branch_id=".$request->input('branch_id')."
            GROUP BY MONTH(iv.updated_date),YEAR(iv.updated_date)
        ";
        $listItem = DB::select($strQuery);
   
        if(count($listItem)>0)
        {
            $response["success"] =  $listItem;
        }
        else
        {
            $response["warning"] = "Không có dữ liệu thống kê";
        }

        return response()->json($response);
    }
    // =========================dashboard 2================
    
}
