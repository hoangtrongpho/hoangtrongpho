<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Cookie;
use DB;
use Lang;
use Redirect;
use Session;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    //Check đăng nhập
    public function checkSystemAuth(){

        if(Session::has('userAuth'))
        {
            if(Session::has('lockScreen') && Session::get('lockScreen') == false)
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }
        else
        {
            return 0;
        }
    }

    //Check đăng nhập
    public function checkWebsiteAuth($request)
    {
        //Tạo biến lưu kết quả kiểm tra
      $result["auth"] = "";
      // $result["redirect"] = "";
      $result["message"] = "";

      $sessionUser = null; 
      if(Cookie::get('agencyAuth') !== null)
      {
         $sessionUser = Cookie::get('agencyAuth');
      }
      else if($request->session()->has('agencyAuth'))
      {
         $sessionUser = $request->session()->get('agencyAuth'); 
      }

      if($sessionUser)
      {  //Trường hợp đã đăng nhập

         $agencyInfo = DB::table('agencies_detail')
                ->select('agency_id', 
                         'agency_code',
                         'agency_name', 
                         'agency_address_number', 
                         'agency_provinces',
                         'agency_districts',  
                         'agency_wards',
                         'agency_phone', 
                         'agency_owner_full_name', 
                         'agency_owner_birthday',
                         'agency_owner_email')
                ->where(function($query) use ($sessionUser)
                {
                    $query->where('agency_id', '=', $sessionUser->agency_id);
                    $query->where('status', '<>', 0);
                })
                ->first();

         if($agencyInfo)
         {  //Trường hợp tài khoản hợp lệ
            //Có quyền truy xuất API
            Session::put('agencyAuth', $agencyInfo);
             $result["auth"] = false;
         }
         else
         {  //Trường hợp tài khoản đã bị khóa hoặc tài khoản không tồn tại
            //Không có quyền truy xuất API
            $result["auth"] = true;
            // $result["redirect"] = "/Login";
            $result["message"] = Lang::get('messages.common_error_user_not_exists');
         }
      }
      else
      {  //Trường hợp chưa đăng nhập
         //Không có quyền truy xuất API
         $result["auth"] = true;
         // $result["redirect"] = "/Login";
         $result["message"] = "Bạn chưa đăng nhập, hãy đăng nhập để sử dụng hệ thống.";
      }

      return $result;
   }

    // //Check đăng nhập
    // public function checkWebsiteAuth(){

    //     if(Session::has('agencyAuth'))
    //     {
    //         return false;
    //     }
    //     else
    //     {
    //         return true;
    //     }
    // }

    //Check đăng nhập
    public function requestLogout($request)
    {
        $request->session()->forget('agencyAuth');

        $request->session()->forget('changePassword');
        $request->session()->forget('userForChange');

        $cookieUserAuth = Cookie::forget('agencyAuth');

        return Redirect::to('dang-nhap')->withCookie($cookieUserAuth); 
    }
}
