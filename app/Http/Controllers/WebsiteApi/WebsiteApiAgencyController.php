<?php

namespace App\Http\Controllers\WebsiteApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use DateTime;
use Validator;
use Mail;
use Debugbar;
use Lang;

use App\Http\Controllers\WebsiteApi\WebsiteCommonController as commonCtl;

use App\Models\AgenciesDetail;
use App\Models\AgenciesNotificationBirthday;
use App\Models\AgenciesStatistic;
use App\Models\SystemAreasProvince;

class WebsiteApiAgencyController extends Controller
{

    /*** Khởi Tạo Giá Trị ***/
    private $commonCtl;

    private $sessionUser;

    /*** Array Validation Message ***/
    private $rulesMess = [
        'agency_id.required' => 'Vui Lòng Thêm Mã Đại Lý.',
        'agency_code.required' => 'Vui Lòng Thêm Mã Đăng Nhập Đại Lý.',
        'agency_name.required' => 'Vui Lòng Thêm Tên Đại Lý.',
        'agency_address_number.required' => 'Vui Lòng Thêm Địa Chỉ Đại Lý.',
        'agency_provinces.required' => 'Vui Lòng Chọn Tỉnh Thành Đại Lý.',
        'agency_phone.required' => 'Vui Lòng Thêm Số Điện Thoại Đại Lý.',
        'agency_promotion_type.required' => 'Vui Lòng Chọn Cách Thức Tích Lũy Điểm Đại Lý.',
        'agency_owner_full_name.required' => 'Vui Lòng Thêm Tên Chủ Đại Lý.',
        'agency_owner_email.required' => 'Vui Lòng Thêm Email Đại Lý.',
        'status.required' => 'Vui Lòng Chọn Trạng Thái Đại Lý.',
        'agency_owner_birthday.required' => 'Vui Lòng Chọn Ngày Sinh Chủ Đại Lý.',
        'agency_joined_date.required' => 'Vui Lòng Chọn Ngày Tham Gia Của Đại Lý.'
    ];

    /*** Khởi Tạo Giá Trị ***/
    public function __construct(commonCtl $SystemCommonController){
        $this->commonCtl =  $SystemCommonController;
    }

    /*** API Hàm Chức Năng ***/  
    protected function apiGetDetail(Request $request){  

        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $whereFunctions = array(['ad.status', '<>', 0]);

        if(!empty($request->input('agency_id')))
        {
            array_push($whereFunctions, ['ad.agency_id', '=', $request->input('agency_id')]);
        }

        $agency = DB::table('agencies_detail AS ad')
                ->leftJoin('system_areas_provinces AS ap', 'ad.agency_provinces', '=', 'ap.province_id')
                ->leftJoin('system_areas_districts AS sd', 'ad.agency_districts', '=', 'sd.district_id')
                ->leftJoin('system_areas_wards AS aw', 'ad.agency_wards', '=', 'aw.ward_id')
                ->select('ad.agency_id', 
                         'ad.agency_code', 
                         'ad.agency_password', 
                         'ad.agency_login_status', 
                         'ad.agency_security_code', 
                         'ad.agency_name',
                         'ad.agency_address_number',
                         'ad.agency_provinces',
                         'ad.agency_districts',
                         'ad.agency_wards',
                         'ad.agency_phone', 
                         'ad.agency_joined_date', 
                         'ad.agency_owner_full_name', 
                         'ad.agency_owner_birthday', 
                         'ad.agency_owner_email',
                         'ad.agency_promotion_type',
                         'ad.status',
                         'ap.province_name',
                         'sd.district_name',
                         'aw.ward_name')
                ->where($whereFunctions)
                ->first();

        $whereFunctions = array(
            ['as.agency_id', '=', $request->input('agency_id')],
            ['as.status', '<>', 0]
        );

        $listAgencyStatistic = DB::table('agencies_statistics AS as')
                ->select('as.statistic_id', 
                         'as.agency_id', 
                         'as.statistic_year', 
                         'as.statistic_month', 
                         'as.statistic_turnover', 
                         'as.statistic_turnover_points',
                         'as.statistic_points_display_reached',
                         'as.statistic_points_display_nonreached')
                ->where($whereFunctions)
                ->orderBy('as.statistic_year', 'desc')
                ->orderBy(DB::raw('LENGTH(as.statistic_month)'), 'asc')
                ->get();

        if($agency)
        {
            $response["success"] = $agency;
            $response["listAgencyStatistic"] = $listAgencyStatistic;
        }
        else
        {
            $response["warning"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' Đại Lý']);
        }

        return response()->json($response);
    }

    //Ajax Post Cập Nhật
    protected function apiChangePassword(Request $request){   

        $auth = $this->checkWebsiteAuth($request);
        if($auth["auth"])
        {
            return response()->json($auth);
        }

        $this->sessionUser = Session::get('agencyAuth'); 

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";
        
        if(!empty($request->input('agency_password')) && !empty($request->input('agency_password_confirm')) )
        {
            if($request->input('agency_password') !== $request->input('agency_password_confirm'))
            {
                $response["warning"] = ['Mật Khẩu Đã Nhập Không Trùng Khớp.'];
                return response()->json($response);
            }
        }
        else
        {
            $response["warning"] = ['Vui lòng nhập mật khẩu.'];
            return response()->json($response);
        }

        //Kiêm tra tồn tại
        $updateItem = AgenciesDetail::where('agency_id','=',$request->input('agency_id'))
                        ->first();
        if($updateItem === null)
        {
            $response["error"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' Đại Lý']);
            return response()->json($response);
        }

        $updateItem->agency_password = md5($request->input('agency_password'));

        $updateItem->updated_user = $this->sessionUser->agency_id;
        $updateItem->updated_date = $this->commonCtl->getCarbonNow();

        DB::beginTransaction();

        try {

            $updateItem->save();

            DB::commit();
            $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_updated'), ['Mật Khẩu']);
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            // $response["error"] = $e->getErrors();
            return response()->json($response);
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }

        return response()->json($response);
    }

    //Ajax Post Cập Nhật
    protected function apiUpdate(Request $request){   

        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        $this->sessionUser = Session::get('agencyAuth'); 

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";
        
        $rules = [
            'agency_id' => 'required',
            'agency_name' => 'required',
            'agency_address_number' => 'required',
            'agency_provinces' => 'required',
            'agency_phone' => 'required',
            'agency_promotion_type' => 'required',
            'agency_owner_full_name' => 'required',
            'agency_owner_email' => 'required',
            'agency_owner_birthday' => 'required',
        ];

        $valiData = $this->commonCtl->validRequest($request,$rules,$this->rulesMess);

        if($valiData->fails()){   
            $response["warning"] = $valiData->errors();
            return response()->json($response);
        }

        //Kiêm tra tồn tại
        $itemProvincesExists = SystemAreasProvince::where('province_id','=',$request->input('agency_provinces'))
                                ->first();
        if($itemProvincesExists === null)
        {
            $response["error"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' Tỉnh Thành Phố']);
            return response()->json($response);
        }

        //Kiêm tra tồn tại
        $updateItem = AgenciesDetail::where('agency_id','=',$request->input('agency_id'))
                        ->first();
        if($updateItem === null)
        {
            $response["error"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' Đại Lý']);
            return response()->json($response);
        }

        //Kiêm tra tồn tại
        $whereFunctions = array(
            ['agency_owner_email','=',$request->input('agency_owner_email')],
            ['agency_id', '<>', $request->input('agency_id')]
        );
        $emailExists = AgenciesDetail::where($whereFunctions)
                        ->first();
        if($emailExists !== null)
        {
            $response["warning"] = [$this->commonCtl->replaceTitle(Lang::get('messages.common_error_exist_data'), ['Email', 'Đại Lý'])];
            return response()->json($response);
        }

        $updateItem->agency_name = $request->input('agency_name');
        $updateItem->agency_address_number = $request->input('agency_address_number');
        $updateItem->agency_provinces = $request->input('agency_provinces');
        $updateItem->agency_phone = $request->input('agency_phone');
        $updateItem->agency_promotion_type = $request->input('agency_promotion_type');
        $updateItem->agency_owner_full_name = $request->input('agency_owner_full_name');
        $updateItem->agency_owner_email = $request->input('agency_owner_email');

        $agency_owner_birthday = $this->commonCtl->formatCarbonDatetime($request->input('agency_owner_birthday'));
        if($agency_owner_birthday !== null)
        {
            $updateItem->agency_owner_birthday = $agency_owner_birthday;
        }
        else
        {
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }


        $updateItem->updated_user = $this->sessionUser->agency_id;
        $updateItem->updated_date = $this->commonCtl->getCarbonNow();

        DB::beginTransaction();

        try {

            $updateItem->save();

            DB::commit();
            $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_updated'), ['thông tin đại lý '.$updateItem->agency_name]);
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception').$e->getErrors();
            // $response["error"] = $e->getErrors();
            return response()->json($response);
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception').$e;
            return response()->json($response);
        }

        return response()->json($response);
    }

    //Ajax Post Cập Nhật
    protected function apiGenerateSecureCode(Request $request){   

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $method = $request->input('method');
        $account = $request->input('account');

        if(empty($method))
        {
            $response["warning"] = "Vui lòng chọn cách khôi phục tài khoản của bạn.";
            return response()->json($response);
        }

        if(empty($account))
        {
            $response["warning"] = "Vui lòng chọn cách khôi phục tài khoản của bạn.";
            return response()->json($response);
        }

        $existAgency = AgenciesDetail::where('agency_owner_email','=',$account)
                        ->first();

        if($method == "phone")
        {
            $existAgency = AgenciesDetail::where('agency_phone','=',$account)
                        ->first();
        }

        if(!$existAgency)
        {
            $response["error"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' Đại Lý']);
            return response()->json($response);
        }

        $characters = '9512036874';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i <= 5; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        $timeCode = $this->commonCtl->getCarbonNow();
        $timeCode = $timeCode->addMinutes(10);
      
        $existAgency->agency_security_code = $randomString;
        $existAgency->agency_security_code_time = $timeCode;
        $existAgency->updated_date = $this->commonCtl->getCarbonNow();

        DB::beginTransaction();

        try {

            $existAgency->save();

            DB::commit();
            
            if($method == "phone")
            {
                $data   = array(
                    'submission' => array(
                    'api_key' => env("SMS_KEY"),
                    'api_secret' => env("SMS_SECRECT"),
                    'sms' => array(
                        0 => array(
                          'brandname' => env("SMS_BRAND_NAME"),
                          'text' => "Ban vua yeu cau thay doi mat khau cho tai khoan cau lac bo dien quang. Ma bao mat cua ban la ".$existAgency->agency_security_code.". Chi tiet lien he 19001257",
                          'to' => $existAgency->agency_phone,
                        ),
                      ),
                    ),
                );

                $url = env("SMS_API");

                $dataString = json_encode($data);
                $ch         = curl_init(env("SMS_API"));
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt(
                    $ch,
                    CURLOPT_HTTPHEADER,
                    array(
                      'Content-Type: application/json',
                      'Content-Length: ' . strlen($dataString)
                    )
                );
                curl_exec($ch);
                curl_close($ch);

                Session::put("changePassword",true);
                Session::put("userForChange",$existAgency->agency_id);
                $response["success"] = "Xác nhận thành công. Vui lòng kiểm tra email để đổi mật khẩu.";
            }
            else
            {
                $app_url = DB::table('system_settings')
                            ->select('setting_values')
                            ->where('setting_key', '=', 'app_url')
                            ->first();

                $emailcontent = array (
                    'agency_id' => $existAgency->agency_id, 
                    'agency_code' => $existAgency->agency_code, 
                    'agency_login_status' => $existAgency->agency_login_status, 
                    'agency_security_code' => $existAgency->agency_security_code, 
                    'agency_name' => $existAgency->agency_name,
                    'agency_address_number' => $existAgency->agency_address_number,
                    'agency_provinces' => $existAgency->agency_provinces,
                    'agency_districts' => $existAgency->agency_districts,
                    'agency_wards' => $existAgency->agency_wards,
                    'agency_phone' => $existAgency->agency_phone, 
                    'agency_joined_date' => $existAgency->agency_joined_date, 
                    'agency_owner_full_name' => $existAgency->agency_owner_full_name, 
                    'agency_owner_birthday' => $existAgency->agency_owner_birthday, 
                    'agency_owner_email' => $existAgency->agency_owner_email,
                    'agency_promotion_type' => $existAgency->agency_promotion_type,
                    'province_name' => $existAgency->province_name,
                    'district_name' => $existAgency->district_name,
                    'ward_name' => $existAgency->ward_name,
                    'app_url' => $app_url
                );

                Mail::send('system.mailtemplate.agencystepsecuritycodetemplate', $emailcontent, 
                    function ($message) use ($existAgency)  {

                    $message->from(env("MAIL_FROM_ADDRESS","support@dienquang.com"), "Skyfire Solution");
                    $message->to($existAgency->agency_owner_email, $existAgency->agency_owner_full_name);
                    $message->cc("vinh.nguyen2303@outlook.com","Vinh Nguyễn");
                    $message->subject("Mã bảo mật đổi mật khẩu - Skyfire Solution");

                });

                if(count(Mail::failures()) > 0) 
                {
                    $response["error"] = Lang::get('messages.common_error_send_mail');
                } 
                else 
                {
                    Session::put("changePassword",true);
                    Session::put("userForChange",$existAgency->agency_id);
                    $response["success"] = "Xác nhận thành công. Vui lòng kiểm tra email để đổi mật khẩu.";
                }
            }
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception').$e->getErrors();
            // $response["error"] = $e->getErrors();
            return response()->json($response);
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception').$e;
            return response()->json($response);
        }

        return response()->json($response);
    }

    protected function apiSecureCodeConfirm(Request $request){  

        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $whereFunctions = array(['status', '<>', 0]);


        if(!empty(Session::get("userForChange")))
        {
            array_push($whereFunctions, ['agency_id', '=', Session::get("userForChange")]);
            array_push($whereFunctions, ['agency_security_code', '=', $request->input('security_code')]);
            array_push($whereFunctions, ['agency_security_code_time', '>', $this->commonCtl->getCarbonNow()]);
        }
        else
        {
           $response["error"] = "Bạn không có quyền sử dụng chức năng này."; 
           return response()->json($response);
        }

        $agency = DB::table('agencies_detail')
                ->select('agency_id')
                ->where($whereFunctions)
                ->first();

        if($agency)
        {
            $response["success"] = $request->input('security_code');
        }
        else
        {
            $response["warning"] = "Mã bảo mật không trùng khớp hoặc đã quá thời gian sử dụng. Vui lòng thử lại hoặc liên hệ với quản trị viên.";
        }

        return response()->json($response);
    }

    //Ajax Post Cập Nhật
    protected function apiFinishChangePassword(Request $request){   

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $whereFunctions = array(['status', '<>', 0]);


        if(!empty(Session::get("userForChange")))
        {
            array_push($whereFunctions, ['agency_id', '=', Session::get("userForChange")]);
            array_push($whereFunctions, ['agency_security_code', '=', $request->input('security_code')]);
            array_push($whereFunctions, ['agency_security_code_time', '>', $this->commonCtl->getCarbonNow()]);

        }
        else
        {
           $response["error"] = "Bạn không có quyền sử dụng chức năng này."; 
           return response()->json($response);
        }
        
        if(!empty($request->input('agency_password')) && !empty($request->input('agency_password_confirm')) )
        {
            if($request->input('agency_password') !== $request->input('agency_password_confirm'))
            {
                $response["warning"] = 'Mật Khẩu Đã Nhập Không Trùng Khớp.';
                return response()->json($response);
            }
        }
        else
        {
            $response["warning"] = 'Vui lòng nhập mật khẩu.';
            return response()->json($response);
        }

        //Kiêm tra tồn tại
        $updateItem = AgenciesDetail::where($whereFunctions)
                        ->first();

        if($updateItem === null)
        {
            $response["error"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_error_not_exist_data'), [' Đại Lý']);
            return response()->json($response);
        }

        $updateItem->agency_password = md5($request->input('agency_password'));
        $updateItem->agency_security_code = null;
        $updateItem->agency_security_code_time = $this->commonCtl->getCarbonNow();

        $updateItem->updated_date = $this->commonCtl->getCarbonNow();

        DB::beginTransaction();

        try {

            $updateItem->save();

            DB::commit();
            $response["success"] = $this->commonCtl->replaceTitle(Lang::get('messages.common_success_updated'), ['Mật Khẩu']);
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            // $response["error"] = $e->getErrors();
            return response()->json($response);
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = Lang::get('messages.common_error_exception');
            return response()->json($response);
        }

        return response()->json($response);
    }
    
}
