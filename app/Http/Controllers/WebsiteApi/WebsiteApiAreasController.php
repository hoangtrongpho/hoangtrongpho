<?php

namespace App\Http\Controllers\WebsiteApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Validator;
use Lang;

use App\Http\Controllers\SystemApi\SystemCommonController as commonCtl;

use App\Models\SystemAreasDistrict;
use App\Models\SystemAreasProvince;
use App\Models\SystemAreasWard;
use App\Models\AgenciesDetail;


class WebsiteApiAreasController extends Controller
{

    /*** Khởi Tạo Giá Trị ***/
    private $commonCtl;

    private $sessionUser;

    private $rulesMess = [
        'province_id.required' => 'Vui Lòng Nhập Mã Tỉnh Thành',
        'province_name.required' => 'Vui Lòng Nhập Tên Tỉnh Thành',
        // 'news_cate_date_public.required' => 'We need to know your news_cate_date_public!',
        // 'status.required' => 'We need to know your news_cate_status!',
    ];
    /*** Khởi Tạo Giá Trị ***/

    public function __construct(commonCtl $SystemCommonController){
        $this->commonCtl =  $SystemCommonController;
    }

    protected function apiGetListProvinces(Request $request){

        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $listProvinces = DB::table('system_areas_provinces AS ap')
                          ->select('ap.province_id', 
                                   'ap.province_name', 
                                   'ap.province_orders', 
                                   'ap.status')
                          ->where('ap.status','<>',0)
                          ->orderBy('ap.province_name', 'desc')
                          ->get();

        if(!$listProvinces->isEmpty())
        {
            $response["success"] = $listProvinces;
        }
        else
        {
            $response["warning"] = Lang::get('messages.common_warning_empty_list');
        }

        return response()->json($response);
    }

    protected function apiGetListDistricts(Request $request){

        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $listDistricts = DB::table('system_areas_districts AS ad')
                          ->select('ad.district_id', 
                                   'ad.province_id', 
                                   'ad.district_name', 
                                   'ad.district_orders', 
                                   'ad.status')
                          ->where('ad.status','<>',0)
                          ->get();

        if(!$listDistricts->isEmpty())
        {
            $response["success"] = $listDistricts;
        }
        else
        {
            $response["warning"] = Lang::get('messages.common_warning_empty_list');
        }

        return response()->json($response);
    }

    protected function apiGetListWards(){

        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        $listWards = DB::table('system_areas_wards AS aw')
                      ->select('aw.ward_id', 
                               'aw.district_id', 
                               'aw.ward_name', 
                               'aw.ward_orders', 
                               'aw.status')
                      ->where('aw.status','<>',0)
                      ->get();

        if(!$listWards->isEmpty())
        {
            $response["success"] = $listWards;
        }
        else
        {
            $response["warning"] = Lang::get('messages.common_warning_empty_list');
        }

        return response()->json($response);
    }

    protected function apiGetListDistrictsByProvince(Request $request){

        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        if(empty($request->input('list_province_id')))
        {
            $response["warning"] = "Không có mã tỉnh thành.";
            return response()->json($response);
        }

        $list_province_id = $request->input('list_province_id');

        $listDistricts = DB::table('system_areas_districts AS ad')
                        ->leftJoin('system_areas_provinces AS ap', 'ad.province_id', '=', 'ap.province_id')
                        ->select('ad.district_id', 
                                   'ad.province_id', 
                                   'ad.district_name', 
                                   'ad.district_orders', 
                                   'ad.status',
                                   'ap.province_name')
                        ->whereIn('ad.province_id', $list_province_id)
                        ->where('ad.status','<>',0)
                        ->get();

        if(!$listDistricts->isEmpty())
        {
            $response["success"] = $listDistricts;
        }
        else
        {
            $response["warning"] = Lang::get('messages.common_warning_empty_list');
        }

        return response()->json($response);
    }

    protected function apiGetListWardsByDistrict(Request $request){

        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        if(empty($request->input('list_district_id')))
        {
            $response["warning"] = "Không có mã tỉnh thành.";
            return response()->json($response);
        }

        $list_district_id = $request->input('list_district_id');

        $district_id = $request->input('district_id');

        $listWards = DB::table('system_areas_wards AS aw')
                    ->leftJoin('system_areas_districts AS ad', 'aw.district_id', '=', 'ad.district_id')
                    ->leftJoin('system_areas_provinces AS ap', 'ad.province_id', '=', 'ap.province_id')
                    ->select('aw.ward_id', 
                               'aw.district_id', 
                               'aw.ward_name', 
                               'aw.ward_orders', 
                               'aw.status',
                               'ad.district_name',
                               'ap.province_name')
                    ->whereIn('ad.district_id', $list_district_id)
                    ->where('aw.status','<>',0)
                    ->get();

        if(!$listWards->isEmpty())
        {
            $response["success"] = $listWards;
        }
        else
        {
            $response["warning"] = Lang::get('messages.common_warning_empty_list');
        }

        return response()->json($response);
    }

  
}
