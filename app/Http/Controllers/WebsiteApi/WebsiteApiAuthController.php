<?php

namespace App\Http\Controllers\WebsiteApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cookie;
use DateTime;
use DB;
use Debugbar;
use Mail;
use Redirect;
use Session;

use App\Http\Controllers\SystemApi\SystemCommonController as commonCtl;

use App\Models\SystemCounter;
use App\Models\AgenciesDetail;

class WebsiteApiAuthController extends Controller
{
    private $commonCtl;

    public function __construct(commonCtl $SystemCommonController){
        $this->commonCtl = $SystemCommonController;
    }

	//Ajax Post Kiểm Tra Tài Khoản
    protected function apiCheckLogin(Request $request)
    {   

        $auth = $this->commonCtl->checkTokenSession($request);
        if($auth["auth"])
        {
            return response()->json($auth);
        }
        
        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        //Lấy dữ liệu post từ angularjs
        $user_account = strtoupper($request->input('user_account'));
        $user_password = $request->input('user_password');
        
        if(empty($user_account))
        {
            $response["warning"] = "Tài khoản hoặc mật khẩu không chính xác.";
            return response()->json($response);
        }

        if(empty($user_password))
        {
            $response["warning"] = "Tài khoản hoặc mật khẩu không chính xác.";
            return response()->json($response);
        }

        $customerInfo = DB::table('customers_detail')
                ->select('customer_id', 
                         'group_id',
                         'customer_account', 
                         'customer_email', 
                         'customer_fullname',
                         'customer_address',  
                         'customer_phone',
                         'customer_birthday', 

                         'customer_identify', 
                         'customer_student_card', 
                         'customer_university', 
                         'status')
                ->where(function($query) use ($user_account,$user_password)
                {
                    $query->where('customer_account', '=', $user_account);
                    $query->where('customer_password', '=', md5($user_password));
                    $query->where([
                        ['status', '<>', 0],
                        ['status', '<>', 3]
                        ]);
                })
                ->first();
        
        if($customerInfo != null)
        {
            Session::flush();
            Session::put('customerInfo', $customerInfo);
            $this->viewCounter($request);
            $response["success"] = "Đăng Nhập Thành Công";

        }
        else
        {
            $response["warning"] = "Thông Tin không chính xác/ Hoặc Chưa Được Active.";
        }

        if(!empty($request->input('remembered')) && $request->input('remembered') == 1)
        {
           return response()->json($response)->withCookie(Cookie::make('agencyAuth', $agencyInfo, 86400));
        }
        else
        {
            return response()->json($response);
        }
    }

    protected function apiRequestRegister(Request $request)
    {   

        $auth = $this->commonCtl->checkTokenSession($request);
        if($auth["auth"])
        {
            return response()->json($auth);
        }
        
        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        //Lấy dữ liệu post từ angularjs
        $email = $request->input('email');
        $name = $request->input('name');
        
        if(empty($email))
        {
            $response["warning"] = "Vui lòng nhập email của bạn.";
            return response()->json($response);
        }

        if(empty($name))
        {
            $response["warning"] = "Vui lòng nhập họ và tên đầy đủ của bạn.";
            return response()->json($response);
        }

        $agencyInfo = DB::table('agencies_detail')
                ->select('agency_owner_email')
                ->where(function($query) use ($email)
                {
                    $query->where('agency_owner_email', '=', $email);
                })
                ->first();
        
        if($agencyInfo != null)
        {
            $response["warning"] = "Email đã tồn tại trong hệ thống. Vui lòng chọn một email khác.";
        }
        else
        {
            $emailcontent = array (
                'email' => $email, 
                'name' => $name
            );

            Mail::send('system.mailtemplate.requestregisteragencytemplate', $emailcontent, 
                function ($message) use ($email, $name)  {

                $message->to(env("MAIL_FROM_ADDRESS","support@dienquang.com"), "Skyfire Solution");
                $message->from($email, $name);
                $message->cc("vinh.nguyen2303@outlook.com","Vinh Nguyễn");
                $message->subject("Yêu cầu đăng ký tài khoản từ câu lạc bộ điện quang - Skyfire Solution");

            });

            if(count(Mail::failures()) > 0) 
            {
                $response["error"] = Lang::get('messages.common_error_send_mail');
            } 
            else 
            {
                $response["success"] = "Gửi yêu cầu thành công. Chúng tôi sẽ liên lạc với bạn trong thời gian sớm nhất.";
            }
        }


        return response()->json($response);
    }

    //Thêm Lượt Truy Cập
    protected function viewCounter($request){

        //Khởi Tạo Dữ Liệu
        $counterData = new SystemCounter();

        //Get IP máy client
        // if(($request->ip !== "::1" || $request->ip !== "127.0.0.1" || $request->ip !== "localhost") && !empty($request->ip))
        // {
        // $counterData->counter_client_ip = $request->ip;
        $counterData->counter_client_ip = $_SERVER['REMOTE_ADDR'];
        // }
        // else
        // {
        //     $counterData->counter_client_ip = "127.0.0.1"; 
        // }

        //Get Ngày Tháng Năm Hiện Tại
        $counterData->counter_date = $this->commonCtl->formatCarbonDatetime_From($this->commonCtl->getCarbonNow());

        //$browser = get_browser(null, true);

        // $counterData->counter_browser_name_pattern = $browser["browser_name_pattern"];
        // $counterData->counter_browser = $browser["browser"];
        // $counterData->counter_browser_version = $browser["version"];
        // $counterData->counter_platform = $browser["platform"];

        $counterData->counter_browser_name_pattern = "";
        $counterData->counter_browser = "";
        $counterData->counter_browser_version = "";
        $counterData->counter_platform = "";

        // $counterData->agency_id = Session::get('agencyAuth')->agency_id;        

        // //$url="http://freegeoip.net/json/".$counterData->counter_client_ip;
        // $url="http://ip-api.com/json/".$counterData->counter_client_ip;
        // //  Initiate curl
        // $ch = curl_init();
        // // Disable SSL verification
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // // Will return the response, if false it print the response
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // // Set the url
        // curl_setopt($ch, CURLOPT_URL,$url);
        // // Execute
        // $data=curl_exec($ch);
        // // Closing
        // curl_close($ch);

        // $result = json_decode($data,true);

        /*if($result["status"] == "success")
        {
            $counterData->counter_country_code = $result["countryCode"];
            $counterData->counter_country_name = $result["country"];
            $counterData->counter_region_code = $result["region"];
            $counterData->counter_region_name = $result["regionName"];
            $counterData->counter_city = $result["city"];
            $counterData->counter_zip_code = $result["zip"];
            $counterData->counter_latitude = $result["lat"];
            $counterData->counter_longitude = $result["lon"];
            $counterData->counter_time_zone = $result["timezone"];

            $counterData->counter_isp = $result["isp"];
            $counterData->counter_org = $result["org"];
            $counterData->counter_as = $result["as"];*/

            // Start transaction!
            DB::beginTransaction();

            try 
            {
                $counterData->save();
            } 
            catch(\Exception $e)
            {
                DB::rollback();
            }

            DB::commit();
        /*}*/
    }
}
