<?php

namespace App\Http\Controllers\WebsiteApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use DateTime;
use Validator;
use Mail;
use Debugbar;
use Lang;

use App\Http\Controllers\WebsiteApi\WebsiteCommonController as commonCtl;


use App\Models\ProductsDetail;
use App\Models\InvoiceDetail;
use App\Models\Invoice;
use App\Models\CustomersDetail;

class WebsiteApiUserInfoController extends Controller
{

    /*** Khởi Tạo Giá Trị ***/
    private $commonCtl;

    private $sessionUser;



    /*** Khởi Tạo Giá Trị ***/
    public function __construct(commonCtl $SystemCommonController){
        $this->commonCtl =  $SystemCommonController;
    }
     /*** API Hàm Chức Năng ***/  
    //apigetInvoiceByUser
    protected function apigetInvoiceByUser(Request $request){  

        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";
        if(Session::get('customerInfo')==null)
        {
            $response["error"] = "Vui Lòng Đăng Nhập";
            return response()->json($response);
        }

        $customerSession = Session::get('customerInfo');
        $response["customer_fullname"] = $customerSession->customer_fullname;
        if($customerSession->customer_id==null)
        {
            $response["error"] = "customer_id null";
            return response()->json($response);
        }
        //=================== whereFunctions ===================
        try {
            $whereFunctions = array(
                    ['cu.customer_id', '=', $customerSession->customer_id]
                );

            $listInvoice = DB::table('invoice AS in')
                    ->leftJoin('customers_detail AS cu', 'in.customer_id', '=', 'cu.customer_id')
                    ->select( 
                        'in.invoice_id',
                        'in.invoice_number',
                        'in.invoice_id',
                        'in.receiver_name',
                        'in.receiver_phone',
                        'in.receiver_address',
                        'in.total_sum',
                        'in.delivery_date',
                        'in.invoice_note',
                        DB::raw("DATE_FORMAT(in.updated_date,'%d/%m/%Y') AS updated_date_format"),
                        'in.export_invoice_status',
                        'in.payment_method',
                        'in.status',

                        'cu.customer_id',
                        'cu.customer_fullname'
                             )
                    ->where($whereFunctions)
                    ->orderBy('in.updated_date', 'desc')
                    ->get();

            if(!$listInvoice->isEmpty())
            {
                $response["success"] = $listInvoice;
            }
            else
            {
                $response["warning"] = "listInvoice null";
            }
        } 
        catch(ValidationException $e)
        {
            //DB::rollback();
            $response["error"] = "Lỗi Hệ Thống ValidationException";
            return response()->json($response);
        } 
        catch(\Exception $e)
        {
            //DB::rollback();
            $response["error"] = "Lỗi Hệ Thống Exception";
            return response()->json($response);
        }

        return response()->json($response);
    }
    //apiGetUserDetail
    protected function apiGetUserDetail(Request $request){  
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";
        if(Session::get('customerInfo')==null)
        {
            $response["error"] = "customerInfo null";
            return response()->json($response);
        }
        $userInfo=Session::get('customerInfo');

        $response["success"] = $userInfo;
   

        return response()->json($response);
    }
    //apiupdatePass
    protected function apiupdatePass(Request $request){  
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";
        
        if($request->input('txtCustomerId')==null || 
            $request->input('txtCustomerPass') == null || 
            $request->input('txtCustomerNewPass') ==null)
        {
            $response["error"] = "txtCustomerId/txtCustomerPass/txtCustomerNewPass null";
            return response()->json($response);
        }

        $users = CustomersDetail::where('customer_id', '=', $request->input('txtCustomerId'))->first();
        // check exist user
        if($users == null)
        {
            $response["error"] = "user not exist";
            return response()->json($response);
        }
        if($users->customer_password != md5($request->input('txtCustomerPass')) )
        {
            $response["error"] = "Pass not right";
            return response()->json($response);
        }

        $users->customer_password = md5($request->input('txtCustomerNewPass'));
        $users->save();
        Session::put('customerInfo', $users);
        
        $response["success"] = "Thay Đổi Thành Công Mật Khẩu";
        return response()->json($response);
    }
    //apiregisterUser
    protected function apiregisterUser(Request $request){
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";
        // CHECK TRUNG ACC
        $whereFunctions = array(
            ['customer_account', '=', $request->input('registerAccount')]
        );
        $checkCustomer = DB::table('customers_detail')
                        ->where($whereFunctions)
                        ->first();
        if(count($checkCustomer) > 0){
             $response["warning"] = "Tài Khoản: ".$request->input('registerAccount')." đã tồn tại, hoặc đã bị khóa";
            return response()->json($response);
        }
        // kiem Tra trùng CMND thì khong cho DK
        $existIdentify = DB::table('customers_detail')->where('customer_identify', $request->input('registerIdentify'))->first();
        if($existIdentify!=null){
            $response["warning"] = "Số CMND: ".$request->input('registerIdentify')." đã tồn tại, hoặc đã bị khóa";
            return response()->json($response);
        }
        // insert code
        $itemCustomer = new CustomersDetail;
        $itemCustomer->group_id         =0;
        $itemCustomer->customer_account =$request->input('registerAccount');
        $itemCustomer->customer_email   =$request->input('registerEmail');
        $itemCustomer->customer_fullname=$request->input('registerFullName');
        $itemCustomer->customer_address=$request->input('registerAddress');
        $itemCustomer->area_id          =1;
        $itemCustomer->customer_phone   =$request->input('registerPhone');
        $itemCustomer->customer_password=md5($request->input('registerPassword'));
         // txtRegisterIdentify
        // txtRegisterStudentCard
        // txtRegisterStudentUniversity
        $itemCustomer->customer_identify=$request->input('registerIdentify');
        $itemCustomer->customer_student_card=$request->input('registerStudentCard');
        $itemCustomer->customer_university=$request->input('registerStudentUniversity');

        $itemCustomer->created_user     = 1; 
        $itemCustomer->created_date     = $this->commonCtl->getCarbonNow();
        $itemCustomer->updated_user     = 1; 
        $itemCustomer->updated_date     = $this->commonCtl->getCarbonNow();


        DB::beginTransaction();

        try {
            $itemCustomer->save();
            Session::put('customerInfo', $itemCustomer);
            $response["success"] = "Đăng Ký Thành Công";
            
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            Session::flush();
            $response["error"] = "Lỗi Hệ Thống";
            return response()->json($response);
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            Session::flush();
            $response["error"] = "Lỗi Hệ Thống";
            return response()->json($response);
        }

        DB::commit();

        return response()->json($response);
    }
    //apiUpdateUserDetail
    protected function apiUpdateUserDetail(Request $request){  
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        if(Session::get('customerInfo')==null)
        {
            $response["error"] = "session custominfo null";
            return response()->json($response);
        }
        if($request->input('customer_fullname')==null )
        {
            $response["error"] = "Vui Lòng Điền Họ Tên !";
            return response()->json($response);
        }
        if($request->input('customer_email')==null )
        {
            $response["error"] = "Vui Lòng Điền Email !";
            return response()->json($response);
        }
        if($request->input('customer_phone')==null )
        {
            $response["error"] = "Vui Lòng Điền SĐT !";
            return response()->json($response);
        }
        if($request->input('customer_address')==null )
        {
            $response["error"] = "Vui Lòng Điền Địa Chỉ !";
            return response()->json($response);
        }
        if($request->input('customer_identify')==null )
        {
            $response["error"] = "Vui Lòng Điền CMND !";
            return response()->json($response);
        }
        if($request->input('customer_student_card')==null )
        {
            $response["error"] = "Vui Lòng Điền Mã Sinh Viên !";
            return response()->json($response);
        }
        if($request->input('customer_university')==null )
        {
            $response["error"] = "Vui Lòng Điền Trường Học !";
            return response()->json($response);
        }
        $users = CustomersDetail::where('customer_id', '=', Session::get('customerInfo')->customer_id)->first();

        // check exist user
        if($users == null)
        {
            $response["error"] = "User Không Tồn Tại/ Hoặc đã Bị Xóa";
            return response()->json($response);
        }

        DB::beginTransaction();

        try {
            $users->fill($request->all()); 
            $users->updated_user     = Session::get('customerInfo')->customer_id; 
            $users->updated_date     = $this->commonCtl->getCarbonNow();
            $users->save();
            
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = "Lỗi Hệ Thống";
            return response()->json($response);
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = "Lỗi Hệ Thống";
            return response()->json($response);
        }

        DB::commit();
        
        Session::put('customerInfo', $users);        
        $response["success"] = "Thay Đổi Thông Tin Thành Công";
        return response()->json($response);
    }
}
