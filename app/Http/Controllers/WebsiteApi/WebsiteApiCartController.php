<?php

namespace App\Http\Controllers\WebsiteApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use DateTime;
use Validator;
use Mail;
use Debugbar;
use Lang;

use App\Http\Controllers\WebsiteApi\WebsiteCommonController as commonCtl;


use App\Models\ProductsDetail;
use App\Models\InvoiceDetail;
use App\Models\Invoice;
use App\Models\CustomersDetail;

class WebsiteApiCartController extends Controller
{

    /*** Khởi Tạo Giá Trị ***/
    private $commonCtl;

    private $sessionUser;



    /*** Khởi Tạo Giá Trị ***/
    public function __construct(commonCtl $SystemCommonController){
        $this->commonCtl =  $SystemCommonController;
    }
     /*** API Hàm Chức Năng ***/ 
    //apiGetListCart
    protected function apiGetListCart(Request $request){  

        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }
        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        // create new list obj to push date
        $list_invoiceDetail = ProductsDetail::where('product_id', '=', 0)->get();
        $list_Cart = ProductsDetail::where('product_id', '=', 0)->get();
        $total_Sum = 0;
        $quantity_Sum = 0;
        $cart_Count = 0;
        //nếu session có san pham > check id trùng
        if(Session::get('cart')!=null){
            $list_invoiceDetail = Session::get('cart');
            foreach ($list_invoiceDetail as $item) {
                //get detail
                $whereFunctions = array(
                    ['nd.product_id', '=', $item['product_id']]
                );
                $cartDetail = DB::table('products_detail AS nd')
                ->leftJoin('products_units_detail AS un', 'nd.unit_id', '=', 'un.unit_id')
                ->leftJoin('products_categories_detail AS ca', 'nd.cate_id', '=', 'ca.cate_id')
                ->leftJoin('products_origins_detail AS or', 'nd.origin_id', '=', 'or.origin_id')
                ->leftJoin('products_manufacturers_detail AS ma', 'nd.manufacturer_id', '=', 'ma.manufacturer_id')
                ->leftJoin('products_colors_detail AS co', 'nd.color_id', '=', 'co.color_id')
                //phai theo thứ tự (product_id > quantity >product_imported_prices)
                ->select('nd.product_id',
                    'nd.product_slug',
                    'nd.unit_id',
                    'nd.cate_id',
                    'nd.origin_id',
                    'nd.manufacturer_id',
                    'nd.color_id',
                    'nd.product_name',
                    'nd.product_description',
                    'nd.product_specifications',
                    'nd.product_imported_prices as product_imported_prices1',//gia ban
                    'nd.product_retail_prices',//gia khuyen mai
                    'nd.product_deals',
                    'nd.product_ratings',
                    'nd.product_thumbnail',
                    'nd.product_images_list',
                    'nd.product_release_date',
                    'nd.product_expiration_date',
                    'nd.product_comment_status',
                    'nd.news_seo_title',
                    'nd.news_seo_description',
                    'nd.news_seo_keywords',
                    'nd.status',
                    'un.unit_name',
                    'ca.cate_name',
                    'or.origin_name',
                    'ma.manufacturer_name',
                    'co.color_name'
                         )
                ->where($whereFunctions)
                ->first();
                $cartDetail->quantity= $item['product_quantity'];
                $cartDetail->product_imported_prices= $cartDetail->product_imported_prices1;
                // product_quantity < 3 : giam 20%
                if($item['product_quantity']<3){
                    $cartDetail->sum= $item['product_quantity'] * $cartDetail->product_retail_prices *80/100;
                    $cartDetail->discount="20 %";
                }
                // product_quantity < 5 : giam 22%
                else if($item['product_quantity']<5){
                    $cartDetail->sum= $item['product_quantity'] * $cartDetail->product_retail_prices *78/100;
                    $cartDetail->discount="22 %";
                }
                // product_quantity < 10 : giam 24%
                else if($item['product_quantity']<10){
                    $cartDetail->sum= $item['product_quantity'] * $cartDetail->product_retail_prices *76/100;
                    $cartDetail->discount="24 %";
                }
                // product_quantity < 20 : giam 26%
                else if($item['product_quantity']<20){
                    $cartDetail->sum= $item['product_quantity'] * $cartDetail->product_retail_prices *74/100;
                    $cartDetail->discount="26 %";
                }
                // product_quantity < 50 : giam 28%
                else if($item['product_quantity']<50){
                    $cartDetail->sum= $item['product_quantity'] * $cartDetail->product_retail_prices *72/100;
                    $cartDetail->discount="28 %";
                }
                // product_quantity < 100 : giam 30%
                else if($item['product_quantity']<100){
                    $cartDetail->sum= $item['product_quantity'] * $cartDetail->product_retail_prices *70/100;
                    $cartDetail->discount="30 %";
                }
                // product_quantity >= 100 : giam 35%
                else {
                    $cartDetail->sum= $item['product_quantity'] * $cartDetail->product_retail_prices *65/100;
                    $cartDetail->discount="35 %";
                }
                $cartDetail->root_sum= $item['product_quantity'] * $cartDetail->product_retail_prices;
                $total_Sum +=$cartDetail->sum;
                $quantity_Sum +=$cartDetail->quantity;
                $cart_Count +=1;
                // $response["success"] = $listProduct;
                // return response()->json($response);
                $list_Cart->push($cartDetail);
            }
            $list_invoiceDetail=$list_Cart;
        }
        // nếu session null thì thêm mới
        else{
            $response["warning"] = "cart null";
            return response()->json($response);
        }
        if($list_invoiceDetail->count()==0)
        {
            $response["warning"] = "cart null";
            return response()->json($response);
        }
        // lay sesssion 
        if(Session::get('customerInfo')==null){
             $response["customerInfo_status"]="0";
        }
        else
        {
            $response["customerInfo_status"]="1";
        }
        $response["total_sum"]=$total_Sum;
        $response["quantity_sum"] = $quantity_Sum;
        $response["cart_count"] = $cart_Count;
        $response["success"] = $list_invoiceDetail;

        //Lưu tổng giá trị hóa đơn;
        Session::put('total_sum', $total_Sum);
        return response()->json($response);
    }
    // apiAddCart
    protected function apiAddCart(Request $request){  

        // $auth = $this->commonCtl->checkRolesApi($request, "/System/Login");
        // if($auth["auth"])
        // {
        //     return response()->json($auth);
        // }

        //Tạo biến kiểm tra kết quả
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";
        $product = ProductsDetail::find($request->input('product_id'));
        if($product == null)
        {
            $response["warning"] = "không tìm thấy SP";
            return response()->json($response);
        }

        $invoiceDetail = new InvoiceDetail;
        $invoiceDetail->product_id= $request->input('product_id');
        $invoiceDetail->product_quantity= $request->input('quantity');
        $invoiceDetail->product_retail_prices= $product->product_retail_prices;
       
        // create new list obj to push date
        $list_invoiceDetail = InvoiceDetail::where('product_id', '=', 0)->get();

        //nếu session có san pham > check id trùng
        if(Session::get('cart')!=null){
            $list_invoiceDetail = Session::get('cart');
            //nếu trùng id > ++số lượng
            $exist_flag=0;
            $changeItem;
            $i = 0;
            $position = 0;
            
            foreach ($list_invoiceDetail as $item) {
                if($item['product_id']==$request->input('product_id'))
                {
                    $exist_flag=1;
                    $position = $i;
                    //tạo obj mới , có SL mới
                    $changeItem = new InvoiceDetail([
                        'product_id' => $item['product_id'],
                        'product_quantity' => $item['product_quantity']+$request->input('quantity'),
                        'product_retail_prices' => $item['product_retail_prices'],
                    ]);
                }
                $i +=1;
            }
            //nếu  trùng id> replace tại vi tri $position =$i ( $changeItem)
            if($exist_flag==1)
            {
                $list_invoiceDetail->splice($position,1);
                $list_invoiceDetail->push($changeItem);
            }
            //nếu không trùng id> thêm mới
            else
            {
                $list_invoiceDetail->push($invoiceDetail);
            }
        }
        // nếu session null thì thêm mới
        else{
            $list_invoiceDetail->push($invoiceDetail);
        }
        
        Session::put('cart', $list_invoiceDetail);
        $response["success"] = "Đã Thêm Vào Giỏ Hàng";

        return response()->json($response);
    }
    // apiUpdateCart
    protected function apiUpdateCart(Request $request){  

        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";

        // create new list obj to push date
        $list_invoiceDetail = InvoiceDetail::where('product_id', '=', 0)->get();

        foreach ($request->input('dtgListCart') as $item) {


            $cartItem = new InvoiceDetail([
                'product_id' => $item['product_id'],
                'product_quantity' => $item['quantity'],
                'product_retail_prices' => $item['product_retail_prices'],
            ]);
            $list_invoiceDetail->push($cartItem);
        }

        Session::put('cart', $list_invoiceDetail);

        $response["list_invoiceDetail"] = $list_invoiceDetail;
        $response["success"] = "Đã Cập Nhật Giỏ Hàng";

        return response()->json($response);
    }
    //apiSaveInvoice
    protected function apiSaveInvoiceSession(Request $request){
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";
        $currentInvoice = new Invoice;

        // kiem tra session khach hang
        if(Session::get('customerInfo')==null){
            $response["error"] = "Vui Lòng Đăng Nhập";
            return response()->json($response);
        }
        if($request->input('branch_id')==null){
            $response["error"] = "Vui Lòng Chọn Chi Nhánh";
            return response()->json($response);
        }
        // check active/ un-active account
        $existIdentify = DB::table('customers_detail')
        ->where([
            ['customer_id', Session::get('customerInfo')->customer_id],
            ['status', 3],
            ])
        ->first();
        if($existIdentify!=null){
            $response["warning"] = "Tài Khoản Này Đã Bị Khóa, Vui Lòng Liên Hệ Quản Trị Viên !";
            return response()->json($response);
        }
        //  save data
        $currentInvoice->branch_id = $request->input('branch_id');
        $currentInvoice->invoice_number = $request->input('invoice_number');
        $currentInvoice->customer_id = Session::get('customerInfo')->customer_id;

        $currentInvoice->receiver_name = Session::get('customerInfo')->customer_fullname;
        $currentInvoice->receiver_phone = Session::get('customerInfo')->customer_phone;
        $currentInvoice->receiver_address = Session::get('customerInfo')->customer_address;
        $currentInvoice->status = 2;
        // lấy tổng tiền hóa đơn từ Session
        $currentInvoice->total_sum = Session::get('total_sum');
        $currentInvoice->created_user = Session::get('customerInfo')->customer_id;
        $currentInvoice->created_date = $this->commonCtl->getCarbonNow();
        $currentInvoice->updated_user = Session::get('customerInfo')->customer_id;
        $currentInvoice->updated_date = $this->commonCtl->getCarbonNow();
        DB::beginTransaction();

        try {
       
            $currentInvoice->save();

            $list_invoiceDetail = Session::get('cart');
            $response["list_invoiceDetail"] =$list_invoiceDetail;
            if(count($list_invoiceDetail) > 0)
            {
                foreach ($list_invoiceDetail as $itemInvoiceDetail) {
                    // $total_price=$itemInvoiceDetail['product_quantity']*$itemInvoiceDetail['product_retail_prices'];
                    // check sl để lưu vào CTHD: $itemInvoiceDetail['product_quantity']
                    // product_quantity < 3 : giam 20%
                    if($itemInvoiceDetail['product_quantity']<3){
                        $total_price= $itemInvoiceDetail['product_quantity'] * $itemInvoiceDetail['product_retail_prices'] *80/100;
                     
                    }
                    // product_quantity < 5 : giam 22%
                    else if($itemInvoiceDetail['product_quantity']<5){
                        $total_price= $itemInvoiceDetail['product_quantity'] * $itemInvoiceDetail['product_retail_prices'] *78/100;
                  
                    }
                    // product_quantity < 10 : giam 24%
                    else if($itemInvoiceDetail['product_quantity']<10){
                        $total_price= $itemInvoiceDetail['product_quantity'] * $itemInvoiceDetail['product_retail_prices'] *76/100;
                
                    }
                    // product_quantity < 20 : giam 26%
                    else if($itemInvoiceDetail['product_quantity']<20){
                        $total_price= $itemInvoiceDetail['product_quantity'] * $itemInvoiceDetail['product_retail_prices'] *74/100;
                  
                    }
                    // product_quantity < 50 : giam 28%
                    else if($itemInvoiceDetail['product_quantity']<50){
                        $total_price= $itemInvoiceDetail['product_quantity'] * $itemInvoiceDetail['product_retail_prices'] *72/100;
                  
                    }
                    // product_quantity < 100 : giam 30%
                    else if($itemInvoiceDetail['product_quantity']<100){
                        $total_price= $itemInvoiceDetail['product_quantity'] * $itemInvoiceDetail['product_retail_prices'] *70/100;
                   
                    }
                    // product_quantity >= 100 : giam 35%
                    else {
                        $total_price= $itemInvoiceDetail['product_quantity'] * $itemInvoiceDetail['product_retail_prices'] *65/100;
                
                    }

                    $itemInvoiceDetail = new InvoiceDetail([
                        'invoice_id' => $currentInvoice->invoice_id,
                        'product_id' => $itemInvoiceDetail['product_id'],
                        'product_quantity' => $itemInvoiceDetail['product_quantity'],
                        'product_retail_prices' => $itemInvoiceDetail['product_retail_prices'],
                        'total_price' => $total_price
                    ]);
                    $itemInvoiceDetail->save();
                }
            }
            
        } 
        catch(ValidationException $e)
        {
            DB::rollback();
            $response["error"] = "Lỗi Hệ Thống Exception_";
            return response()->json($response).$e;
        } 
        catch(\Exception $e)
        {
            DB::rollback();
            $response["error"] = "Lỗi Hệ Thống Exception";
            return response()->json($response).$e;
        }

        DB::commit();
        $response["success"] = "Hoàn Tất Đặt Hàng";
        $request->session()->forget('invoice');
        $request->session()->forget('cart');
        $request->session()->forget('total_sum');
        return response()->json($response);
    }
    //apigetCustomerDetail
    protected function apigetCustomerDetail(Request $request){
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";
        if(Session::get('cart')==null){
            $response["error"] = "Product null";
            return response()->json($response);
        }
        if(Session::get('customerInfo')==null)
        {
            //$response["error"] = "customerInfo null";
            $response["success"] = "null";
            return response()->json($response);
        }

        $customerSession = Session::get('customerInfo');
        $response["Customer"] = $customerSession;
        $response["success"] = "ok";

        return response()->json($response);
    }
    


    //apigetSuccessOrder
    protected function apigetSuccessOrder(Request $request){
        $response["success"] = "";
        $response["warning"] = "";
        $response["error"] = "";
         if(Session::get('payment_method')==null){
            $response["error"] = "payment null";
            return response()->json($response);
        }
        if(Session::get('invoice')==null){
            $response["error"] = "Invoice Null";
            return response()->json($response);
        }
        if(Session::get('cart')==null){
            $response["error"] = "Product null";
            return response()->json($response);
        }
        if(Session::get('total_sum')==null){
            $response["error"] = "total_sum null";
            return response()->json($response);
        }
        //invoice_number delivery_date receiver_name
        $invoiceSession = Session::get('invoice');
        $response["invoice_number"] = $invoiceSession['invoice_number'];
        $response["delivery_date"] = $invoiceSession['delivery_date'];
        $response["receiver_name"] = $invoiceSession['receiver_name'];

        $response["success"] = "Hoàn Tất Đặt Hàng";
        // clear session
        $request->session()->forget('invoice');
        $request->session()->forget('cart');
        $request->session()->forget('total_sum');
        return response()->json($response);
    }

}
