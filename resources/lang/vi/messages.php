<?php
use Illuminate\Support\Facades\DB;

/*
    |--------------------------------------------------------------------------
    | Ngôn Ngữ Tiếng Việt
    |--------------------------------------------------------------------------
*/

$languages = DB::select('SELECT lang_key,lang_value FROM system_languages WHERE lang_type = ? OR lang_type = ?', ['vi','all']);

$infoCompany = DB::select('SELECT info_key,info_values FROM company_information');

$result_lang = array();

foreach ($languages as $row) 
{
    $result_lang[$row->lang_key] = $row->lang_value;
}

foreach ($infoCompany as $row) 
{
    $result_lang['infoCompany'][$row->info_key] = $row->info_values;
}

return $result_lang;
