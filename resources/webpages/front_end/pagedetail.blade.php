@extends('front_end.layouts.index')

@section('headTitle')
   <title>{{$pageDetail->page_title}} - {{ Lang::get('messages.infoCompany.company_name') }}</title>
@stop

@section('headCss')
<style type="text/css">

</style>
@stop

@section('headJs')

@stop

@section('container')
<div class="breadcrumbs">
   <div class="container">
      <div class="row">
         <ul>
            <li class="home"> <a href="{{URL::to('/trang-chu')}}" title="Trang chủ">Trang chủ</a><span>|</span></li>
            <li><strong>Dịch Vụ {{$pageDetail->page_title}}</strong></li>
         </ul>
      </div>
   </div>
</div>
<div class="container">
   <div class="content_page_info">
      <h1>{{$pageDetail->page_title}}</h1>
      <div class="sidebar-line"><span></span></div>
      <div class="rte">
         {!!$pageDetail->page_content!!}
      </div>
   </div>
</div>


@stop