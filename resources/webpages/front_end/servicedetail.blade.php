@extends('front_end.layouts.index')

@section('headTitle')
   <title>{{$bikeDetail->product_name}} - {{ Lang::get('messages.infoCompany.company_name') }}</title>

   <meta property="og:url"           content="{{URL::to('/xe/'.$bikeDetail->product_slug)}}" />
   <meta property="og:type"          content="website" />
   <meta property="og:title"         content="{{$bikeDetail->product_name}} - {{ Lang::get('messages.infoCompany.company_name') }}" />
   <meta property="og:description"   content="{{$bikeDetail->product_description}}" />
   <meta property="og:image"         content="{{$bikeDetail->product_thumbnail}}" />
   <meta property="fb:app_id"        content="1402826736415176" />


@stop

@section('headCss')
<style type="text/css">

   a.product-image img {
      -moz-transition: all 0.5s;
      -webkit-transition: all 0.5s;
      transition: all 0.5s;
   }
   a.product-image:hover img {
     -moz-transform: scale(1.3);
     -webkit-transform: scale(1.3);
     transform: scale(1.3);
   }
</style>
@stop

@section('headJs')
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.8&appId=1402826736415176";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
@stop

@section('container')
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>

<div class="breadcrumbs">
   <div class="container">
      <div class="row">
         <ul>
            <li class="home"> <a href="{{URL::to('/trang-chu')}}" title="Trang chủ">Trang chủ</a><span>|</span></li>
            <li class="home"> <a href="{{URL::to('/hang-xe/'.$bikeDetail->cate_slug)}}" title="{{$bikeDetail->cate_name}}">{{$bikeDetail->cate_name}}</a><span>|</span></li>
            <li><strong>{{$bikeDetail->product_name}}</strong></li>
         </ul>
      </div>
   </div>
</div>
<div class="main-container col2-left-layout">
   <div class="main container">
      <div class="row">
         <section class="col-main col-sm-9 col-sm-push-3">

            <div class="">
               <div class="col-main">
                  <div class="row">
                     <div class="product-view">
                        <div class="product-essential">
                              <div class="product-img-box col-lg-5 col-sm-5 col-xs-12">
                                 <div class="large-image"> 
                                    <ul class="moreview" id="moreview">
                                       
                                       @if(count($listGalleries) <= 0)
                                          <li class="moreview_thumb">
                                             <img class="moreview_thumb_image" src="{{$bikeDetail->product_thumbnail}}" alt="{{$bikeDetail->product_slug}}" title="{{$bikeDetail->product_name}}"> 
                                             <img class="moreview_small_image_fake" style="display:none;" src="{{$bikeDetail->product_thumbnail}}" alt="{{$bikeDetail->product_slug}}" title="{{$bikeDetail->product_name}}"> 
                                             <img class="moreview_source_image" src="{{$bikeDetail->product_thumbnail}}" alt="{{$bikeDetail->product_slug}}" title="{{$bikeDetail->product_name}}"> 
                                             <span class="roll-over">Di chuột vào ảnh để xem chi tiết</span> 
                                             <img  class="zoomImg"  src="{{$bikeDetail->product_thumbnail}}" alt="{{$bikeDetail->product_slug}}" title="{{$bikeDetail->product_name}}">
                                          </li>
                                       @else
                                          @foreach ($listGalleries as $photo)
                                             <li class="moreview_thumb">
                                                <img class="moreview_thumb_image" src="{{$photo->photo_url}}" alt="{{$bikeDetail->product_slug}}" title="{{$bikeDetail->product_name}}"> 
                                                <img class="moreview_small_image_fake" style="display:none;" src="{{$photo->photo_url}}" alt="{{$bikeDetail->product_slug}}" title="{{$bikeDetail->product_name}}"> 
                                                <img class="moreview_source_image" src="{{$photo->photo_url}}" alt="{{$bikeDetail->product_slug}}" title="{{$bikeDetail->product_name}}"> 
                                                <span class="roll-over">Di chuột vào ảnh để xem chi tiết</span> 
                                                <img  class="zoomImg"  src="{{$photo->photo_url}}" alt="{{$bikeDetail->product_slug}}" title="{{$bikeDetail->product_name}}">
                                             </li>
                                          @endforeach
                                       @endif
                                    </ul>
                                 </div>
                                 <div class="moreview-control"> 
                                    <a href="javascript:void(0)" class="moreview-prev"></a>
                                    <a href="javascript:void(0)" class="moreview-next"></a>
                                 </div>
                              </div>

                              <div class="product-shop col-lg-7 col-sm-7 col-xs-12">
                                 <div class="product-name">
                                    <h1 itemprop="name">{{$bikeDetail->product_name}}</h1>
                                 </div>
                                 <div class="price-block">
                                    <div class="price-box">
                                       <p class="special-price" itemprop="price"> <span id="product-price-48" class="price" itemprop="price">{{number_format($bikeDetail->product_retail_prices, 0)}} VNĐ</span> </p>
                                       <!-- <p class="old-price"> <span id="old-price-48" class="price">0</span> </p> -->
                                    </div>
                                 </div>
                                 <div class="short-description" style="border-bottom: 1px solid #ddd;">
                                    <!-- <p>{!! $bikeDetail->product_description !!}</p> -->
                                 </div>
                                 <div class="short-description">
                                    <h4>Thẻ Sản Phẩm</h4>
                                    <div class="box-collateral box-tags">
                                       <div class="tags">
                                          <ul>
                                             <li><a href="/the/" title="xe-dap-the-thao"><i class="fa fa-fw fa-tag" aria-hidden="true"></i>&nbsp;Xe đạp thể thao</a></li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- <div class="add-to-box">
                                    <div class="add-to-cart" style=" float: left; width: 100%;">
                                       <div class="pull-left" style=" float: left; width: 100%;">
                                          <div class="selector-wrapper" style="text-align: left; margin-bottom: 15px;">
                                             <label>Màu sắc</label>
                                             <select class="single-option-selector" data-option="option1" id="product-selectors-option-0">
                                                <option value="Trắng">Trắng</option>
                                             </select>
                                          </div>
                                          <select class="select" id="product-selectors" name="variantId" style="display: none;">
                                             <option selected="selected" value="1882915">Trắng - 3.100.000₫</option>
                                          </select>
                                       </div>
                                    </div>
                                    
                                 </div> -->
                                 <div class="social">
                                    <button class="button btn-cart btn-lg add_to_cart" title="Thêm vào giỏ hàng" type="button"> ĐẶT MUA XE</button>
                                 </div>
                                 <div class="social" style="border-top:none;text-align: right;">
                                    <!-- <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5269ddad0d5cefd6" async="async"></script>
                                    <div class="addthis_native_toolbox"></div> -->
                                    <div class="fb-like" data-href="{{URL::to('/xe/'.$bikeDetail->product_slug)}}" data-layout="box_count" data-action="like" data-size="large" data-show-faces="true" data-share="false"></div>
                                    <div class="fb-share-button" data-href="{{URL::to('/xe/'.$bikeDetail->product_slug)}}" data-layout="box_count" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="{{URL::to('/xe/'.$bikeDetail->product_slug)}}">Chia sẻ</a></div>
                                 </div>
                              </div>
                        </div>
                        <div class="product-collateral">
                           <div class="col-sm-12">
                              <ul id="product-detail-tab" class="nav nav-tabs product-tabs">
                                 <li class="active"> <a href="#product_tabs_description" data-toggle="tab"> Chi Tiết Xe</a> </li>
                                 <li class=""><a href="#product_tabs_content" data-toggle="tab">Hướng Dẫn Mua Hàng</a></li>
                              </ul>
                              <div id="productTabContent" class="tab-content">
                                 <div class="tab-pane fade active in" id="product_tabs_description">
                                    <div class="std">{!! $bikeDetail->product_description !!}</div>
                                 </div>

                                 <div class="tab-pane fade" id="product_tabs_content">
                                    <div class="box-collateral box-tags">
                                       <div class="tags">
                                          <p>Với mong muốn mang lại sự hài lòng cho quý khách khi mua hàng, chúng tôi có những quy định trong vận chuyển, nhằm đảm bảo rằng những sản phẩm quý khách mua là sản phẩm mà vừa ý nhất.</p>
                                          <p>1. Chúng tôi sẽ được thực hiện và chuyển phát dựa trên mẫu khách hàng đã chọn. Trường hợp không có đúng sản phẩm Quý khách yêu cầu chúng tôi sẽ gọi điện xác nhận gửi sản phẩm tương tự thay thế.</p>
                                          <p>2. Thời gian chuyển phát tiêu chuẩn cho một đơn hàng là 12 giờ kể từ lúc đặt hàng. Chuyển phát sản phẩm đến các khu vực nội thành thành phố trên toàn quốc từ 4 giờ kể từ khi nhận hàng, chuyển phát ngay trong ngày đến các vùng lân cận (bán kính từ 10km – 50km).</p>
                                          <p>3. Các đơn hàng gửi đi quốc tế: không đảm bảo thời gian được chính xác như yêu cầu, không đảm bảo thời gian nếu thời điểm chuyển phát trùng với các ngày lễ, tết và chủ nhật tại khu vực nơi đến.</p>
                                          <p>4. Trường hợp không liên lạc được với người nhận, người nhận đi vắng:</p>
                                          <p>- Nếu chưa rõ địa chỉ chúng tôi sẽ lưu lại trong vòng 6 tiếng và liên lạc lại với người nhận, trong trường hợp ko liên lạc được đơn hàng sẽ bị hủy và không được hoàn lại thanh toán.</p>
                                          <p>- Nếu địa chỉ là công ty, văn phòng, nhà ở… Chúng tôi sẽ gửi đồng nghiệp, người thân nhận hộ và ký xác nhận</p>
                                          <p>- Để tại một nơi an toàn người nhận dễ nhận thấy tại nhà, văn phòng, công ty… Trường hợp này không có ký nhận.</p>
                                          <p>5. Trường hợp người nhận không nhận đơn hàng:</p>
                                          <p>- Chúng tôi sẽ hủy bỏ đơn hàng. Trường hợp này sẽ không được hoàn trả thanh toán.</p>
                                          <p>6. Trường hợp không đúng địa chỉ, thay đổi địa chỉ:</p>
                                          <p>- Không đúng địa chỉ: trường hợp sai địa chỉ chúng tôi sẽ lưu lại 6 tiếng và liên lạc với người gửi và người nhận để thỏa thuận về địa điểm, thời gian, nếu địa chỉ mới không quá 3km sẽ phát miễn phí. Trường hợp địa chỉ mới xa hơn 3km sẽ tính thêm phí theo quy định chuyển phát.</p>
                                          <p>7. Trường hợp không tồn tại người nhận tại địa chỉ yêu cầu: đơn hàng sẽ được hủy và không được hoàn lại thanh toán.</p>
                                          <p>8. Chúng tôi không vận chuyển sản phẩm đến các địa chỉ trên tàu hỏa, máy bay, tàu thủy, khu vực nguy hiểm, các khu vực cấm…</p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>

                           @if($bikeDetail->product_comment_status == 1)
                           <div class="col-sm-12">
                              <div class="fb-comments" data-href="{{URL::to('/xe/'.$bikeDetail->product_slug)}}" data-numposts="5" data-order-by="reverse_time" data-width="100%"></div>
                           </div>
                           @endif

                           @if(count($listBikesRelative) > 0)
                              <div class="col-sm-12">
                                 <div class="box-additional">
                                    <div class="related-pro">
                                       <div class="slider-items-products">
                                          <div class="new_title center">
                                             <h2>Sản phẩm liên quan</h2>
                                          </div>
                                          <div class="clearfix">&nbsp;</div>
                                          <div id="related-products-slider" >
                                             <div class="slider-items slider-width-col4 hidden_btn_cart owl-carousel owl-theme" style="opacity: 1; display: block;">
                                                <div class="owl-wrapper-outer">
                                                   <div class="owl-wrapper" style="width: 5400px; left: 0px; display: block; transition: all 0ms ease; transform: translate3d(0px, 0px, 0px);">

                                                      @foreach ($listBikesRelative as $bike)
                                                         <div class="owl-item" style="width: 300px;">
                                                            <div class="item">
                                                               <div class="col-item">
                                                                  <div class="sale-label sale-top-right">MỚI</div>
                                                                  <div class="product-image-area"> 
                                                                     <a class="product-image" title="{{$bike->product_name}}" href="{{URL::to('xe/'.$bike->product_slug)}}"> 
                                                                        <img src="{{$bike->product_thumbnail}}" class="img-responsive" alt="{{$bike->product_slug}}" title="{{$bike->product_name}}" style="width: 100%;"> 
                                                                     </a>
                                                                  </div>
                                                     
                                                                  <div class="info">
                                                                     <div class="info-inner">
                                                                        <div class="item-title">
                                                                           <strong><a title="{{$bike->product_name}}" href="{{URL::to('xe/'.$bike->product_slug)}}">{{$bike->product_name}}</a></strong>
                                                                        </div>
                                                                        <!--item-title-->
                                                                        <div class="item-content">
                                                                           <div class="price-box">
                                                                              <p class="special-price"> 
                                                                                 <span class="price">{{number_format($bike->product_retail_prices, 0)}} VNĐ</span> 
                                                                              </p>
                                                                              <p class="old-price" style="display: none;"> 
                                                                                 <span class="price-sep">-</span> 
                                                                                 <span class="price">0</span> 
                                                                              </p>
                                                                           </div>
                                                                        </div>
                                                                        <!--item-content--> 
                                                                     </div>
                                                                     <!--info-inner-->
                                                                     <div class="clearfix"> </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      @endforeach
                     
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           @endif


                        </div>
                     </div>
                  </div>
               </div>
         </section>
         <aside class="col-left sidebar col-sm-3 col-xs-12 col-sm-pull-9">
            <div class="box_collection_pr">
               <div class="title_st">
                  <h2>Danh Mục Xe Máy Cũ</h2>
                  <span class="arrow_title"></span>
               </div>
               <div class="list_item">
                  <ul>
                     <li class=""> <a href="{{URL::to('/xe-moi-nhap')}}" title="Xe Mới Nhập">Xe Mới Nhập</a> </li>
                     <li class=""> <a href="{{URL::to('/xe-khuyen-mai')}}" title="Xe Khuyến Mãi">Xe Khuyến Mãi</a> </li>
                     @foreach ($listCategories as $cate)
                        @if($bikeDetail->cate_slug == $cate->cate_slug)
                           <li class="active"> 
                        @else
                           <li class=""> 
                        @endif
                           <a href="{{URL::to('hang-xe/'.$cate->cate_slug)}}" title="{{ !empty($cate->cate_description) ? $cate->cate_description : $cate->cate_name  }}" >
                              {{ $cate->cate_name }} ({{$cate->products_couter}})
                           </a> 
                        </li>
                     @endforeach
                  </ul>
               </div>
            </div>
            <div class="block block-banner"><a href="#"><img alt="alt" src="//bizweb.dktcdn.net/100/049/382/themes/146759/assets/block-banner.png?1477704421120"></a></div>
         </aside>
      </div>
   </div>
</div>


@stop