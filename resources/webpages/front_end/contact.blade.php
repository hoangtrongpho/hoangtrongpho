@extends('front_end.layouts.index')

@section('headTitle')
   <title>Trang Chủ - Sinh Viên Kinh Doanh</title>
@stop

@section('headCss')

@stop

@section('headJs')
  


    <!-- UI Bootstrap Modal --> 
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-bootstrap/ui-bootstrap-tpls-2.0.0.min.js')}}"></script>

    <!-- Angular Toastr -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-toastr/angular-toastr.tpls.js')}}"></script>

  
     <!-- JS -->
  <script type="text/javascript" src="{{asset('public/system/common/system.common.angurlar.js')}}"></script>

  <script type="text/javascript" src="{{asset('public/front_end/common/login.angurlar.js')}}"></script>
  <script src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyBVsgL6svCXyS9meenVc_ZlUW2RXOFYuRQ"></script>
  <script>  
 
    function init_map() {
      // map1
      var var_location = new google.maps.LatLng(10.799328,106.737451);
      var var_mapoptions = {
          center: var_location,
          zoom: 16
      };
      var var_marker = new google.maps.Marker({
          position: var_location,
          map: var_map,
          mapTypeId: google.maps.MapTypeId.HYBRID,
          title:"Vtech IDC"
      });
      var var_map = new google.maps.Map(document.getElementById("map-container"),var_mapoptions);
      var_marker.setMap(var_map); 

      // map2
      var var_location2 = new google.maps.LatLng(10.75267176,106.65376469);
      var var_mapoptions2 = {
          center: var_location2,
          zoom: 16
      };
      var var_marker2 = new google.maps.Marker({
          position: var_location2,
          map: var_map2,
          mapTypeId: google.maps.MapTypeId.HYBRID,
          title:"Vtech IDC"
      });
      var var_map2 = new google.maps.Map(document.getElementById("map-container2"),var_mapoptions2);
      var_marker2.setMap(var_map2);
      // map 3
      var var_location3 = new google.maps.LatLng(10.76788257,106.66799985);
      var var_mapoptions3 = {
          center: var_location3,
          zoom: 16
      };
      var var_marker3 = new google.maps.Marker({
          position: var_location3,
          map: var_map3,
          mapTypeId: google.maps.MapTypeId.HYBRID,
          title:"Vtech IDC"
      });
      var var_map3 = new google.maps.Map(document.getElementById("map-container3"),var_mapoptions3);
      var_marker3.setMap(var_map3);
    }
 
    google.maps.event.addDomListener(window, 'load', init_map);
 
    </script>
@stop

@section('container')
<style>

  .map-container { height: 30% }

  .map-outer {  height: 440px; 
        padding: 20px; 
      border: 2px solid #CCC; 
      margin-bottom: 20px; 
      background-color:#FFF }
  .map-container { height: 400px }
  @media all and (max-width: 991px) {
  .map-outer  { height: 650px }
  }
</style>
  <!-- MAIN CONTENT -->
   <div id="main" role="main" ng-app="loginApp" ng-controller="loginController" ng-init="pageInit('')">
      <div class="container">
         <div class="row">       
            <div class="main_content  col-sm-9 col-sm-push-3">          
               <!-- =================BREADCRUMBS =========-->
               <div class="collection_listing_main">
                  <div class="row">
                     <div class="main_content  col-sm-12">
                      
                        <div class="breadcrumb_wrap">
                           <ul class="breadcrumb">
                              <li><a href="/" class="homepage-link" title="Back to the frontpage">Trang Chủ</a></li>
                              <li><a href="" title="">Liên Hệ</a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
                <!-- contentent -->
                <div class="">
                  <div  class="col-md-12 map-outer">
                    <div  class="col-md-4 address">
                      <h2>Trụ Sở Chính</h2>
                      <address>
                        <strong>Số 74, đường số 7,</strong><br>
                        KP. 5, P. An Phú,<br>
                        Quận 2, TP. Hồ Chí Minh<br>
                        <abbr>SĐT:</abbr> 028 38 380 380<br>
                        <abbr>SĐT:</abbr> 091 789 3625
                      </address>
                    </div>
                    <div id="map-container" class="col-md-8 map-container"></div>
                  </div> 
                 
                </div>
                <!-- end content -->
                <!-- contentent -->
                <div class="">
                  <div  class="col-md-12 map-outer">
                    <div  class="col-md-4 address">
                      <h2>Trụ Sở 2</h2>
                      <address>
                        <strong>Số 938, Nguyễn Trãi,</strong><br>
                        Phường 14, Quận 5, TP. Hồ Chí Minh<br>
                        <abbr>SĐT:</abbr> 028 38 380 380<br>
                        <abbr>SĐT:</abbr> 091 789 3625
                      </address>
                    </div>
                    <div id="map-container2" class="col-md-8 map-container"></div>
                  </div> 
                </div>
                <!-- end content -->
                <!-- contentent -->
                <div class="">
                  <div  class="col-md-12 map-outer">
                    <div  class="col-md-4 address">
                      <h2>Trụ Sở 3</h2>
                      <address>
                        <strong>Số 497, Lý Thái Tổ,</strong><br>
                        Phường 9, Quận 10, TP. Hồ Chí Minh<br>
                        <abbr>SĐT:</abbr> 028 38 380 380<br>
                        <abbr>SĐT:</abbr> 091 789 3625
                      </address>
                    </div>
                    <div id="map-container3" class="col-md-8 map-container"></div>
                  </div> 
                </div>
                <!-- end content -->
              </div>
            
            <!-- ============================= side bar ==================== -->
            @include('front_end.layouts.menu',[ 
              'listCategoriesuserAuth' => $listCategories,
              'listTags' => $listTags,
              'listQuickSales' => $listQuickSales,
            ])  
            </div>         
         </div>
      </div>
   </div>
@stop
