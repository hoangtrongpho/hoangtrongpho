<meta charset="utf-8">

<meta name="auth_token" content="{{ csrf_token() }}">
<!-- <meta http-equiv="REFRESH" CONTENT="5"> -->
<?php
    $metas = DB::select('SELECT setting_key, setting_values FROM system_settings WHERE setting_type = ? OR autoload = ?', ['meta',1]);
    $echoMetas = "";
    foreach ($metas as $item) 
    {
        $echoMetas .= '<meta name="'.$item->setting_key.'" content="'.$item->setting_values.'">';
    }
    echo $echoMetas;
?>
<meta property="fb:app_id" content="1863962563863649" />
<meta system_id="skyfire_management_system" system_code="_AJYKd+gci@#_+tuvwfw&*gh6kXAWETJCeiohohBt!@#uvwxQR+ghijEN" system_content="{{ csrf_token() }}" system_key="CD78&^&zABCOPQR+ghijENOPQR$s_+tuvwfw&*gh6kXeiohohBt!@#uvwxy$%^&zABCD78&^&zABC*OPQR+ghijENOPQR$^YZ" name="system-secret-code" >
<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<!-- Mobile viewport optimization h5bp.com/ad -->
<meta name="HandheldFriendly" content="True" />
<meta name="MobileOptimized" content="320" />
<!-- Mobile IE allows us to activate ClearType technology for smoothing fonts for easy reading -->
<meta http-equiv="cleartype" content="on" />
<!-- Shortcut Icon -->
<link rel="shortcut icon" type="images/vnd.microsoft.icon" href="{{ asset('favicon.png')}}" />
<!-- For third generation iPad Retina Display -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('favicon.png')}}" />
<!-- For iPhone 4 with high-resolution Retina display: -->
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('favicon.png')}}" />
<!-- For first-generation iPad: -->
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('favicon.png')}}" />
<!-- For non-Retina iPhone, iPod Touch, and Android 2.1+ devices: -->
<link rel="apple-touch-icon-precomposed" href="{{ asset('favicon.png')}}" />

<link rel="stylesheet" type="text/css" href="{{asset('public/front_end/css/bootstrap.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('public/front_end/css/font-awesome.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('public/front_end/css/assets.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('public/front_end/css/style.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('public/front_end/css/responsive.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('public/front_end/css/side_menu.css')}}"/>
{{-- <link rel="stylesheet" type="text/css" href="{{asset('public/front_end/css/css.css')}}"/> --}}
<!--[if IE 9]>
<link href="css/ie9.css" rel="stylesheet" type="text/css" media="all" />
<![endif]-->
<!-- JS -->
<style type="text/css">
	.loader-wrapper {
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0px;
        left: 0px;
        background-color: rgba(255,255,255,0.7);
        transition: all 0.5s ease-in-out;
        -moz-transition: all 0.5s ease-in-out;
        -o-transition: all 0.5s ease-in-out;
        -webkit-transition: all 0.5s ease-in-out;
        overflow: hidden;
        z-index: 110;
    }

    #divloader {
        display: block;
        position: relative;
        left: 52%;
        top: 15%;
        width: 100px;
        height: 100px;
        margin: -75px 0 0 -75px;
        border-radius: 50%;
        border: 3px solid transparent;
        border-top-color: #7D3C8E;
        -webkit-animation: spin 1.5s linear infinite; /* Chrome, Opera 15+, Safari 5+ */
        animation: spin 1.5s linear infinite; /* Chrome, Firefox 16+, IE 10+, Opera */
    }
     
    #divloader:before {
        content: "";
        position: absolute;
        top: 5px;
        left: 5px;
        right: 5px;
        bottom: 5px;
        border-radius: 50%;
        border: 3px solid transparent;
        border-top-color: #7D3C8E;
        -webkit-animation: spin 2s linear infinite; /* Chrome, Opera 15+, Safari 5+ */
          animation: spin 2s linear infinite; /* Chrome, Firefox 16+, IE 10+, Opera */
    }
     
    #divloader:after {
        content: "";
        position: absolute;
        top: 15px;
        left: 15px;
        right: 15px;
        bottom: 15px;
        border-radius: 50%;
        border: 3px solid transparent;
        border-top-color: #7D3C8E;
        -webkit-animation: spin 1s linear infinite; /* Chrome, Opera 15+, Safari 5+ */
          animation: spin 1s linear infinite; /* Chrome, Firefox 16+, IE 10+, Opera */
    }

   
     
    @-webkit-keyframes spin {
        0%   {
            -webkit-transform: rotate(0deg);  /* Chrome, Opera 15+, Safari 3.1+ */
            -ms-transform: rotate(0deg);  /* IE 9 */
            transform: rotate(0deg);  /* Firefox 16+, IE 10+, Opera */
        }
        25%   {
            -webkit-transform: rotate(90deg);  /* Chrome, Opera 15+, Safari 3.1+ */
            -ms-transform: rotate(90deg);  /* IE 9 */
            transform: rotate(90deg);  /* Firefox 16+, IE 10+, Opera */
        }
        50%   {
            -webkit-transform: rotate(180deg);  /* Chrome, Opera 15+, Safari 3.1+ */
            -ms-transform: rotate(180deg);  /* IE 9 */
            transform: rotate(180deg);  /* Firefox 16+, IE 10+, Opera */
        }
        75%   {
            -webkit-transform: rotate(270deg);  /* Chrome, Opera 15+, Safari 3.1+ */
            -ms-transform: rotate(270deg);  /* IE 9 */
            transform: rotate(270deg);  /* Firefox 16+, IE 10+, Opera */
        }
        100% {
            -webkit-transform: rotate(360deg);  /* Chrome, Opera 15+, Safari 3.1+ */
            -ms-transform: rotate(360deg);  /* IE 9 */
            transform: rotate(360deg);  /* Firefox 16+, IE 10+, Opera */
        }
    }
    @keyframes spin {
        0%   {
            -webkit-transform: rotate(0deg);  /* Chrome, Opera 15+, Safari 3.1+ */
            -ms-transform: rotate(0deg);  /* IE 9 */
            transform: rotate(0deg);  /* Firefox 16+, IE 10+, Opera */
        }
        25%   {
            -webkit-transform: rotate(90deg);  /* Chrome, Opera 15+, Safari 3.1+ */
            -ms-transform: rotate(90deg);  /* IE 9 */
            transform: rotate(90deg);  /* Firefox 16+, IE 10+, Opera */
        }
        50%   {
            -webkit-transform: rotate(180deg);  /* Chrome, Opera 15+, Safari 3.1+ */
            -ms-transform: rotate(180deg);  /* IE 9 */
            transform: rotate(180deg);  /* Firefox 16+, IE 10+, Opera */
        }
        75%   {
            -webkit-transform: rotate(270deg);  /* Chrome, Opera 15+, Safari 3.1+ */
            -ms-transform: rotate(270deg);  /* IE 9 */
            transform: rotate(270deg);  /* Firefox 16+, IE 10+, Opera */
        }
        100% {
            -webkit-transform: rotate(360deg);  /* Chrome, Opera 15+, Safari 3.1+ */
            -ms-transform: rotate(360deg);  /* IE 9 */
            transform: rotate(360deg);  /* Firefox 16+, IE 10+, Opera */
        }
    }
    /*** CSS Preloading ***/

    .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control{
        color: #01489A !important;
        background-color: #d2d2d2 !important;
    }

    .btn.focus, .btn:focus, .btn:hover{
        color: #01489A !important;    
        border: 1px solid #01489A !important;    
        text-decoration: none !important;    
        background-color: #ffffff !important;    
    }

    .main_content{
        padding-left: 8px !important;
        padding-right: 0px !important;
    }

    @media (min-width: 768px)
    {
        .main_content{
            padding-left: 8px !important;
            padding-right: 0px !important;
        }
    }

    .collection_listing_main .col-xs-12 {
        width: 97% !important;
    }

    @media (min-width: 992px)
    {
        .collection_listing_main .col-md-4 {
            width: 31.333333% !important;
        }
    }

    @media (min-width: 768px)
    {
        .collection_listing_main .col-sm-6 {
            width: 47%;
        }

    }

    .featured_products .fa-fw,
    .collection_listing_main .fa-fw{
        padding: 1px 0px 0px 9px !important;
    }

    .product_links{
        margin-top: 15px !important;
    }
</style>