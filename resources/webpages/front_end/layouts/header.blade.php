<!-- HEADER -->
<header>
   <div class="container" >
      <div class="header_row__1">
         <!-- USER MENU -->
         <ul class="header_user">
         @if(Session::has('customerInfo'))
            <li><a href="{{URL::to('/thong-tin-khach-hang')}}" id="customer_login_link">Chào Mừng {{Session::get('customerInfo')->customer_fullname}}</a></li>
            <li><a href="{{URL::to('/dang-xuat')}}" id="customer_login_link">Thoát</a></li>
            <li><a href="{{URL::to('/lich-su-don-hang')}}" title="Đơn Hàng">Đơn Hàng</a></li>
         @else
            <li><a href="{{URL::to('/dang-nhap')}}" id="customer_login_link">Đăng Nhập</a></li>
            <li><a href="{{URL::to('/dang-ky')}}" id="customer_register_link">Đăng Ký</a></li>
         @endif 
            <li class="checkout"><a href="{{URL::to('/gio-hang')}}">Giỏ Hàng</a></li>
         </ul>
         <ul class="header_social hidden-xs">
            <li><a href="//www.facebook.com/TemplateMonster"><i class="fa fa-facebook"></i></a></li>
         </ul> 
         <div class="clearfix"></div>
      </div>
      <div class="row header_row__2">
          <img width="100%" class="img-responsive" src="{{asset('public/UserPhotos/manager/logo-banner/Untitled-3.png')}}" alt="LogoSinhVienKinhDoanh" title="SinhVienKinhDoanh" />
      </div>
      <div class="row header_row__2">
         <div class="col-sm-3 header_left" style="text-align:center;">
          
         </div>
         <div class="col-sm-6 header_center">
            <!-- HEADER SEARCH -->
            <div class="header_search">
               <form action="{{URL::to('/tim-kiem')}}" method="get" id="frmSearch" name="frmSearch" class="search-form" role="search">
                  <input style="color: black" id="search-field" name="kw" id="kw" type="text" placeholder="" class="hint" />
                  <button id="search-submit" type="submit">Tìm Kiếm</button>
               </form>
            </div>
         </div>
         <div class="col-sm-3 header_right">       
      
         </div>
      </div>
      <div class="row header_row__3">
         <div class=" col-sm-9  col-sm-offset-3" style="padding-right: 8px;">
            <!-- MEGAMENU -->
            <div id="megamenu">
               <!-- megamenu_desktop -->
               <ul class="sf-menu megamenu_desktop visible-lg">
                  <!-- ===========Trang Chu========== -->
                  <li class="megamenu_item_1">
                     <a href="{{URL::to('/')}}" class="active">Trang Chủ</a>
                  </li>
                  <!-- ===========Danh Muc========== -->
                  <li class="megamenu_item_2">
                     <a href="#">Danh Mục<span class="menu_badge"><span style="display: none;"></span></span></a>       
                     <ul>
                        <li>
                           <div class="submenu submenu_2">
                              <div class="row submenu_2__categories">
                                 @if(count($listCategories)<=0)
                                    <div>
                                       <h4 lass="col-sm-12">
                                          <a href="#">Danh Mục Trống</a>
                                       </h4>
                                    </div>
                                 @else
                                    @foreach($listCategories as $c)
                                       <?php
                                          $c1 = array_get($c,'nodes');
                                          $c_count=count($c1);
                                          $c_pinned=array_get($c,'cate_pinned');
                                       ?>
                                    
                                          <div class="col-sm-4" >
                                            <a href="{{URL::to('chi-tiet-danh-muc/'.array_get($c, 'cate_slug'))}}">
                                              <img style="width: 100%;height: 200px" src="{{array_get($c, 'cate_thumbnail')}}"  alt="hinh danh muc" class="blog_img" />
                                            </a>
                                            
                                             <h4>
                                                <a href="{{URL::to('chi-tiet-danh-muc/'.array_get($c, 'cate_slug'))}}">
                                                   <b>{{array_get($c, 'cate_name')}}</b>
                                                   @if($c_count>0)
                                                      <span class="badge">{{$c_count}}</span>
                                                   @endif
                                                </a>
                                             </h4>
                                             <ul>
                                                @foreach($c1 as $c1)
                                                   <?php
                                                      $c2 = array_get($c1, 'nodes');
                                                      $c1_count=count($c2);
                                                   ?>
                                                   <li>
                                                      <a href="{{URL::to('chi-tiet-danh-muc/'.array_get($c1, 'cate_slug'))}}" title="{{array_get($c1,'cate_name')}}" style="color: #00c7ff!important">
                                                      <i class="fa fa-th-large" aria-hidden="true"></i>
                                                      {{array_get($c1, 'cate_name')}}
                                                   </a>
                                                   </li>
                                                @endforeach
                                             </ul>
                                          </div>
                                       
                                          
                                    @endforeach
                                 @endif
                              </div>
                              <hr>
                              <!-- view all menu -->
                              <div class="submenu_2__all" style="display: none;">
                                 <a href="/collections/">View all collections</a>
                              </div>         
                           </div>
                        </li>
                     </ul>       
                  </li>
                  <!-- ===========giam gia========== -->
                    @if(count($listLimitSaleOff)>0)
                        <li class="megamenu_item_4">
                            <a href="{{URL::to('/san-pham-giam-gia/')}}">Giảm Giá
                                <span style="display: none" class="menu_badge"><span>30%</span></span>
                            </a>       
                            <ul>
                                <li>
                                    <div class="submenu submenu_4">
                                        <div class="row">  
                                                @foreach($listLimitSaleOff as $s)
                                                <div class="col-sm-3">
                                                    <div class="product_item" style="background-color:rgba(255,255,255,.1);">
                                                        <div class="product_img">  
                                                            <a href="{{URL::to('chi-tiet-san-pham/'.$s->product_slug)}}" style="padding:0px;">
                                                                <img src="{{$s->product_thumbnail}}" 
                                                                    alt="{{$s->product_slug}}" 
                                                                    title="{{$s->product_name}}" 
                                                                    style="width:100%; height:270px; object-fit:cover;"> 
                                                            </a>
                                                        </div>
                                                        <div class="product_price">
                                                            <span style="font-size: 16px" class="money">
                                                                {{ number_format(($s->product_retail_prices), 0) }} VNĐ
                                                            </span><br>
                                                            <span style="display: none;" class="money compare-at-price">
                                                                {{ number_format(($s->product_imported_prices), 0) }} VNĐ
                                                            </span>
                                                        </div>
                                                        <div class="product_name">
                                                            <a href="{{URL::to('chi-tiet-san-pham/'.$s->product_slug)}}">{{$s->product_name}}</a>
                                                        </div>
                                                        <div class="product_desc">
                                                            {!! $s->product_description !!}
                                                        </div>
                                                    </div>
                                                </div> 
                                                @endforeach
                                        </div>
                                    </div>
                                </li>
                            </ul>       
                        </li>
                    @endif 
                  <!-- ===========Tin Tuc========== -->
                  <li class="megamenu_item_3">
                    <a href="{{URL::to('/tin-tuc/')}}">Tin Tức</a> 
                  </li>
                  
                  <!-- ===========tuyen dung========== -->
                  <li class="megamenu_item_5">
                        <a href="{{URL::to('/tuyen-dung/')}}">Tuyển Dụng</a>
                  </li>
                  <!-- ===========Lien He========== -->
                  <li class="megamenu_item_6">
                     <a href="{{URL::to('/lien-he/')}}">Liên Hệ
                     <span style="display: none" class="menu_badge"><span>012.3456.789</span></span>
                     </a>
                  </li>
               </ul>
               <!-- megamenu_mobile -->
               <div class="megamenu_mobile visible-xs visible-sm hidden-lg">
                  <h2>SinhVienKinhDoanh.Com<i></i></h2>
                  <ul class="level_1">
                     <li>
                        <a href="{{URL::to('/')}}">Trang Chủ</a>
                     </li>
                     <li>
                        <a href="#">Danh Mục<i class="level_1_trigger"></i></a>            
                        <ul class="level_2">
                           @if(count($listCategories)<=0)
                              <li>
                                 <a href="/collections/cameras-camcorders">Danh Mục Trống<i class="level_2_trigger"></a>
                              </li>
                           @else
                              @foreach($listCategories as $c)
                                 <li>
                                    <?php
                                       $c1 = array_get($c, 'nodes');
                                       $c_count=count($c1);
                                    ?>
                                    <a href="{{URL::to('/chi-tiet-danh-muc/'.array_get($c, 'cate_slug'))}}">
                                       {{array_get($c, 'cate_name')}}
                                       @if($c_count>0)
                                          <i class="level_2_trigger"></i>
                                       @endif
                                    </a>
                                    <ul class="level_3">
                                       @foreach($c1 as $c1)
                                          <?php
                                             $c2 = array_get($c1, 'nodes');
                                          ?>
                                          <li>
                                             <a href="{{URL::to('/chi-tiet-danh-muc/'.array_get($c1, 'cate_slug'))}}">
                                                {{array_get($c1, 'cate_name')}}
                                             </a>
                                          </li>
                                       @endforeach
                                    </ul>
                                 </li>
                              @endforeach
                           @endif
                        </ul>          
                     </li>
                     <li>
                        <a href="{{URL::to('/tin-tuc/')}}">Tin Tức</a>
                     </li>
                     <li>
                        <a href="{{URL::to('/san-pham-giam-gia/')}}">Giảm Giá</a>
                     </li>
                     <li>
                        <a href="{{URL::to('/tuyen-dung/')}}">Tuyển Dụng</a>
                     </li>
                     <li>
                        <a href="{{URL::to('/lien-he/')}}">Liên Hệ</a>
                     </li>
                  </ul>
               </div>
               <!-- HEADER CART -->
               <div class="header_cart text-center">
                  <a href="{{URL::to('/gio-hang/')}}"><i class="fa fa-shopping-cart"></i><span id="cart_items"></span></a>
               </div>
            </div>
         </div>
      </div>
   </div>
</header>