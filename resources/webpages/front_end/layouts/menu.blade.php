<!-- ============================= side bar ==================== -->
<div class="sidebar col-sm-3 sidebar_left col-sm-pull-9">
    <!-- ================================side menu==================== -->
    <div class="sidebar_widget sidebar_widget__collections hidden-xs">
        <h3 class="widget_header">Danh Mục</h3>
        <div class="widget_content">
            <ul class="list_links mainmenu0"> 
            @if(count($listCategories)<=0)
                <li></li>
            @else
                @foreach($listCategories as $c)
                    <li>
                        <?php
                        $c1 = array_get($c, 'nodes');
                        $c_count=count($c1);
                        $p1 = array_get($c, 'product_list');
                        ?>
                    
                        <a href="{{URL::to('chi-tiet-danh-muc/'.array_get($c, 'cate_slug'))}}" title="{{array_get($c,'cate_name')}}">
                        <b >{{array_get($c, 'cate_name')}}</b>
                        @if($c_count>0)
                            <span class="badge">{{$c_count}}</span>
                        @endif
                        
                        </a>

                        <ul class="submenu1">
                        @foreach($c1 as $c1)
                        <li>
                            <?php
                                $c2 = array_get($c1, 'nodes');
                                $c1_count=count($c2);
                            ?>
                            <a href="{{URL::to('chi-tiet-danh-muc/'.array_get($c1, 'cate_slug'))}}" title="{{array_get($c1,'cate_name')}}" style="color: #01489A!important">
                                {{array_get($c1, 'cate_name')}}
                                @if($c1_count>0)
                                    <span class="badge">{{$c1_count}}</span>
                                @endif
                                
                            </a>
                            
                            <ul class="submenu2">
                                @foreach($c2 as $c2)
                                <li>
                                    <a href="{{URL::to('chi-tiet-danh-muc/'.array_get($c2, 'cate_slug'))}}" title="{{array_get($c2,'cate_name')}}" style="color: #01489A!important">
                                    {{array_get($c2, 'cate_name')}}
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        </li>
                        @endforeach
                        </ul>
                    </li>
                @endforeach
            @endif
            
            </ul>
        </div>
    </div>
    <!-- ===========================Hỗ Trợ Trực Tuyến================= -->
    <div class="sidebar_widget sidebar_widget__types hidden-xs">
        <h3 class="widget_header">Hỗ Trợ Trực Tuyến</h3>
        <div class="widget_content">
            <ul class="list_links">
                <li >
                    <script type="text/javascript" src="https://secure.skypeassets.com/i/scom/js/skype-uri.js"></script>
                    <div id="SkypeButton_Call_hiepvtech_1">
                     <script type="text/javascript">
                     Skype.ui({
                     "name": "chat",
                     "element": "SkypeButton_Call_hiepvtech_1",
                     "participants": ["hiepvtech"]
                     });
                     </script>
                    </div>
                </li>
              
            </ul>
        </div>
    </div>
    <!-- ===========================Chính Sách Mua Hàng================= -->
    <div class="sidebar_widget sidebar_widget__types hidden-xs">
        <h3 class="widget_header">Hệ Thống</h3>
        <div class="widget_content">
            <ul class="list_links">
                <li class="Audio/Video">
                    <a href="{{URL::to('/chi-tiet-ban-tin/gioi-thieu')}}" title="Giới Thiệu Tuyển Dụng"><i style="color: 
                    blue" class="fa fa-group" aria-hidden="true"></i>&nbsp;Giới Thiệu</a>
                </li>
                <li class="Audio/Video">
                    <a href="{{URL::to('/chi-tiet-ban-tin/chinh-sach-chiet-khau')}}" title="Chính Sách Khách Hàng"><i style="color: 
                    blue" class="fa fa-book" aria-hidden="true"></i>&nbsp;Chính Sách Khách Hàng</a>
                </li>
                <li class="Audio/Video">
                    <a href="{{URL::to('/chi-tiet-ban-tin/cach-thuc-hoat-dong')}}" title="Hướng Dẫn Mua Hàng"><i style="color: 
                    blue" class="fa fa-question" aria-hidden="true"></i>&nbsp;Hướng Dẫn Mua Hàng</a>
                </li>
                <li class="Audio/Video">
                    <a href="{{URL::to('/chi-tiet-ban-tin/download')}}" title="download"><i style="color: 
                    blue" class="fa fa-download" aria-hidden="true"></i>&nbsp;Download</a>
                </li>
                <li class="Audio/Video">
                    <a href="#" title="Liên Kết WEBSITE"><i style="color: 
                    blue" class="fa fa-link" aria-hidden="true"></i>&nbsp;Liên Kết WEBSITE</a>
                </li>
                <li class="Audio/Video">
                    <a href="http://www.vtech.vn/" title="vtech">&nbsp;&nbsp;&nbsp;&nbsp;
                        <i style="color: blue" class="fa fa-lightbulb-o" aria-hidden="true"></i>
                        &nbsp;www.vtech.vn
                    </a>
                </li>
                <li class="Audio/Video">
                    <a href="http://www.pravis.vn/" title="pravis">&nbsp;&nbsp;&nbsp;&nbsp;
                        <i style="color: blue" class="fa fa-star" aria-hidden="true"></i>
                        &nbsp;www.pravis.vn
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <!-- ============================product tag==================== -->
    <div class="sidebar_widget sidebar_widget__types hidden-xs">
        <h3 class="widget_header">Loại Sản Phẩm (Tags)</h3>
        <div class="widget_content">
            <ul class="list_links">
            @if(count($listTags)<=0)
                <li></li>
            @else
                @foreach($listTags as $t)
                    <li class="Audio/Video"><a href="{{URL::to('chi-tiet-the/'.$t->tag_slug)}}" title="Audio/Video"><i style="color: 
                    blue" class="fa fa-tags" aria-hidden="true"></i>&nbsp;{{$t->tag_name}}</a></li>
                @endforeach
            @endif
            </ul>
        </div>
    </div>
    <!-- ============================San Pham Ban Chay==================== -->
    @if(count($listQuickSales)>0)
    <div class="sidebar_widget sidebar_widget__products">
        <h3 class="widget_header">Sản Phẩm Bán Chạy</h3>
        <div class="widget_content">
            <ul class="list_products">   
                
                <li></li>
            
                @foreach($listQuickSales as $s)
                    <li class="product">
                        <div class="product_img">  
                            <a href="{{URL::to('chi-tiet-san-pham/'.$s->product_slug)}}">
                            <img  src="{{$s->product_thumbnail}}" alt="{{$s->product_slug}}" title="{{$s->product_name}}"
                            style="width:100%; height:75px; object-fit:cover;"> 
                            </a>
                        </div>
                        <div class="product_info">
                            <div class="product_price">
                            <span class="money">
                            {{ number_format(($s->product_retail_prices), 0) }} VND
                            </span>
                            </div>
                            <div class="product_price">
                            <span style="display: none;" class="money compare-at-price">
                            {{ number_format(($s->product_imported_prices), 0) }} VND
                            </span>   
                            </div>
                            <div class="product_name">
                            <strong><a style="color: #2f2f2f" href="{{URL::to('chi-tiet-san-pham/'.$s->product_slug)}}">{{$s->product_name}}</a></strong><br>
                            {!! $s->product_description !!}
                            </div>
                        </div>
                    </li>  
                @endforeach
            </ul>
        </div>
    </div>
    @endif

</div> 