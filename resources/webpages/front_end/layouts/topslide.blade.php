 <div class="slider_wrap hidden-xs">
   <div class="nivoSlider">
     @if(!$listSlide->isEmpty())
       @foreach($listSlide as $s)
         <a href="{{$s->slide_url}}">
           <img src="{{$s->slide_img}}" alt="{{$s->slide_img}}">
         </a> 
       @endforeach
     @endif
   </div>
</div>