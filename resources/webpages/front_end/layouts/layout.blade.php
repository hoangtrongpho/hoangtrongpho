<!DOCTYPE html>
<html lang="vi" class="cmsmasters_html">
    <head>
        @yield('headTitle')

        @include('front_end.layouts.head')

        @yield('headCss')
    </head>
    <body>

        <!-- Angurlarjs 1.5.8 -->
        <script type="text/javascript" src="{{asset('public/js/angularjs/angular.min.js').'?v='.Carbon\Carbon::now()->timestamp}}"></script>
        <script type="text/javascript" src="{{asset('public/js/angularjs/angular-sanitize.min.js').'?v='.Carbon\Carbon::now()->timestamp}}"></script>
        <script type="text/javascript" src="{{asset('public/js/angularjs/angular-route.min.js').'?v='.Carbon\Carbon::now()->timestamp}}"></script>
        <script type="text/javascript" src="{{asset('public/js/angularjs/angular-cookies.min.js').'?v='.Carbon\Carbon::now()->timestamp}}"></script>
        <script type="text/javascript" src="{{asset('public/js/angularjs/angular-animate.min.js').'?v='.Carbon\Carbon::now()->timestamp}}"></script>
        <!-- Momentjs -->
        <script type="text/javascript" src="{{asset('public/system/plugins/momentjs/moment.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/system/plugins/momentjs/vi.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/front_end/js/jquery.nivoslider.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/front_end/js/jquery.bxslider.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/front_end/js/jquery.fancybox.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/front_end/js/jquery.api.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/front_end/js/shop.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/front_end/js/home_common.js')}}"></script>

    </body>
</html>