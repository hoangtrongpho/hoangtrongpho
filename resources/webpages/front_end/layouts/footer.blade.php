<style type="text/css">
  #SkypeButton_Call_harold_1_paraElement img{
    width: auto !important;
  }
  #SkypeButton_Call_hiepvtech_1_paraElement img{
    width: auto !important;
  }
</style>

<!-- FOOTER -->
<footer>
   <div class="container">
      <div class="footer_row__1">
         <div class="row">
            <div class="col-sm-6 footer_block footer_block__1" style="color: #ffffff">
               <h2 style="color:#ffffff;"><b>Thông Tin Hệ Thống</b></h2>
               <p  style="font-size: 14px">
               @if(count($listInfo)<=0)
                  <div class="col-sm-3"></div>
               @else
                  @foreach($listInfo as $i)
                     @if($i->info_key=='company_intro')
                        {{$i->info_values}}
                     @endif
                     
                  @endforeach
               @endif
              
               </p>
            </div> 
            <div class="col-sm-3 footer_block footer_block__2" style="color: #ffffff">
               <h2><b>Hệ Thống</b></h2>
               <ul style="font-size: 14px">
                  <li class="Audio/Video">
                     <a href="#">
                        <script type="text/javascript" src="https://secure.skypeassets.com/i/scom/js/skype-uri.js"></script>
                        <div id="SkypeButton_Call_harold_1">
                         Liên Hệ Trực Tuyến:
                         <script type="text/javascript">
                         Skype.ui({
                         "name": "chat",
                         "element": "SkypeButton_Call_harold_1",
                         "participants": ["harold"]
                         });
                         </script>
                        </div>
                     </a>
                  </li>
                  <li class="Audio/Video">
                    <a href="{{URL::to('/chi-tiet-ban-tin/gioi-thieu')}}" title="Giới Thiệu Tuyển Dụng"><i class="fa fa-group" aria-hidden="true"></i>&nbsp;Giới Thiệu</a>
                </li>
                <li class="Audio/Video">
                    <a href="{{URL::to('/chi-tiet-ban-tin/chinh-sach-chiet-khau')}}" title="Chính Sách Khách Hàng"><i class="fa fa-book" aria-hidden="true"></i>&nbsp;Chính Sách Khách Hàng</a>
                </li>
                <li class="Audio/Video">
                    <a href="{{URL::to('/chi-tiet-ban-tin/cach-thuc-hoat-dong')}}" title="Hướng Dẫn Mua Hàng"><i class="fa fa-question" aria-hidden="true"></i>&nbsp;Hướng Dẫn Mua Hàng</a>
                </li>
                <li class="Audio/Video">
                    <a href="{{URL::to('/chi-tiet-ban-tin/download')}}" title="download"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;Download</a>
                </li>
                <li class="Audio/Video">
                    <a href="#" title="Liên Kết WEBSITE"><i  class="fa fa-link" aria-hidden="true"></i>&nbsp;Liên Kết WEBSITE</a>
                </li>
                <li class="Audio/Video">
                    <a href="http://www.vtech.vn/" title="vtech">&nbsp;&nbsp;&nbsp;&nbsp;
                        <i  class="fa fa-lightbulb-o" aria-hidden="true"></i>
                        &nbsp;www.vtech.vn
                    </a>
                </li>
                <li class="Audio/Video">
                    <a href="http://www.pravis.vn/" title="pravis">&nbsp;&nbsp;&nbsp;&nbsp;
                        <i  class="fa fa-star" aria-hidden="true"></i>
                        &nbsp;www.pravis.vn
                    </a>
                </li>         
               </ul>
            </div>
            <div class="col-sm-3 footer_block footer_block__2" style="color: #ffffff">
               <h2><b>Phương Thức Hoạt Động</b></h2>
               <ul style="font-size: 14px">
                  <li>
                     <a href="#">
                        <i class="fa fa-user"></i> <span>Đăng Ký Tài Khoản.</span> 
                     </a>
                  </li>
                  <li>
                     <a href="#">
                        <i class="fa fa-cart-plus"></i> <span>Đặt Hàng.</span> 
                     </a>
                  </li>
                  <li>
                     <a href="#">
                        <i class="fa fa-check"></i> <span>Xác Nhận Đặt Hàng.</span> 
                     </a>
                  </li> 
                  <li>
                     <a href="#">
                        <i class="fa fa-cubes"></i> <span>Nhận Hàng.</span> 
                     </a>
                  </li> 
                  <li>
                     <a href="#">
                        <i class="fa fa-diamond"></i> <span>Bán Hàng.</span> 
                     </a>
                  </li>   
                  <li>
                     <a href="#">
                        <i class="fa fa-money"></i> <span>Thanh Toán.</span> 
                     </a>
                  </li>          
               </ul>
            </div>
         </div>
      </div>

      <div class="footer_row__2" style="font-size: 14px">
         <div class="row">
            <div class="col-xs-6 col-sm-3 footer_block footer_block__3 hidden-xs">
               <h2 style="font-weight: bold;">Thông Tin</h2>
               <ul class="footer_links">              
                  <li class="active"><a href="{{URL::to('/')}}" title="">Trang Chủ</a></li>             
                  <li ><a href="#" title="">Danh Mục</a></li>             
                  <li ><a href="{{URL::to('/tin-tuc/')}}" title="">Tin Tức</a></li>               
                  <li ><a href="{{URL::to('/san-pham-giam-gia/')}}" title="">Giảm Giá</a></li>              
                  <li ><a href="{{URL::to('/tuyen-dung/')}}" title="">Tuyển Dụng</a></li>              
                  <li ><a href="{{URL::to('/lien-he/')}}" title="">Liên Hệ</a></li> 
               </ul>
            </div>
            <div class="col-xs-6 col-sm-3 footer_block footer_block__4 hidden-xs">
               <h2 style="font-weight: bold;">Danh Mục</h2>
               <ul class="footer_links">              
                  @if(count($listCategories)<=0)
                     <li>
                        <a href="#">Danh Mục Trống<i class="level_2_trigger"></a>
                     </li>
                  @else
                     @foreach($listCategories as $c)
                        <li>
                           <?php
                              $c1 = array_get($c, 'nodes');
                              $c_count=count($c1);
                           ?>
                           <a href="{{URL::to('chi-tiet-danh-muc/'.array_get($c, 'cate_slug'))}}">
                              {{array_get($c, 'cate_name')}}
                           </a>
                      
                        </li>
                     @endforeach
                  @endif            
               </ul>
            </div>
            <div class="col-xs-6 col-sm-3 footer_block footer_block__5 hidden-xs">
               <h2 style="font-weight: bold;">Thông Tin Tài Khoản</h2>
               <ul class="footer_links">              
                  <li ><a href="/account" title="">Tài Khoản</a></li>                          
                  <li ><a href="/cart" title="">Giở Hàng</a></li>              
               </ul>
            </div>         
            <div class="col-xs-6 col-sm-3 footer_block footer_block__6">
               <h2 style="font-weight: bold;">Hảy Chia Sẻ</h2>
               <ul class="footer_social">
                  {{-- <li><a href="//twitter.com/templatemonster"><i class="fa fa-twitter"></i><span>Twitter</span></a></li> --}}
                  <li><a href="//www.facebook.com/TemplateMonster"><i class="fa fa-facebook"></i><span>Facebook</span></a></li>
                  <li><a href="//www.youtube.com/user/TemplateMonsterCo"><i class="fa fa-youtube"></i><span>Youtube</span></a></li>                
                  {{-- <li><a href="//www.pinterest.com/templatemonster/"><i class="fa fa-pinterest"></i><span>Pinterest</span></a></li> --}}
                  {{-- <li><a href="//google.com/+templatemonster"><i class="fa fa-google-plus"></i><span>Google+</span></a></li>                  --}}
               </ul>
            </div>      
         </div>
      </div>
      <div class="copyright">
         <p role="contentinfo">SinhVienKinhDoang.com &copy; 2017-2020. All Rights Reserved.<br>
         Development by <a href="https://skyfiresolution.com">skyfiresolution.com</a>.<br>
         <a target="_blank" rel="nofollow" href="http://www.vtech.vn/">Powered by Vtech</a>.</p><!-- Design by skyfiresolution.com -->
      </div>
   </div>
</footer>