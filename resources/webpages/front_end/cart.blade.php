@extends('front_end.layouts.index_fullwidth')

@section('headTitle')
   <title>Trang Chủ - Sinh Viên Kinh Doanh</title>
@stop

@section('headCss')
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-bootstrap-datetimepicker/css/datetimepicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/daterangepicker/daterangepicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-ui-switch/bootstrap-switch.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-multi-select/isteven-multi-select.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-toastr/angular-toastr.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/jquery.dataTables.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/dataTables.bootstrap.css')}}" />
@stop

@section('headJs')
   <!-- Main JS -->
   <!-- Date Time Picker -->
   <script type="text/javascript" src="{{asset('public/system/plugins/daterangepicker/daterangepicker.js')}}"></script>
   <script type="text/javascript" src="{{asset('public/system/plugins/angular-daterangepicker/angular-daterangepicker.min.js')}}"></script>

   
   <!-- Angular Multi Select -->
   <script type="text/javascript" src="{{asset('public/system/plugins/angular-multi-select/isteven-multi-select.js')}}"></script>

   <!-- UI Bootstrap Modal --> 
   <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-bootstrap/ui-bootstrap-tpls-2.0.0.min.js')}}"></script>

   <!-- Angular Toastr -->
   <script type="text/javascript" src="{{asset('public/system/plugins/angular-toastr/angular-toastr.tpls.js')}}"></script>
   <!-- Angular Datatables-->
   <script type="text/javascript" src="{{asset('public/system/plugins/datatables/jquery.dataTables.js')}}"></script>
   <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/angular-datatables.min.js')}}"></script>
   <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
   <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js')}}"></script>
   <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/colreorder/angular-datatables.colreorder.min.js')}}"></script>
   <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/bootstrap/angular-datatables.bootstrap.min.js')}}"></script>

    <!-- JS -->
   <script type="text/javascript" src="{{asset('public/system/common/system.common.angurlar.js')}}"></script>

    <script type="text/javascript" src="{{asset('public/front_end/common/front_end.cartindex.angurlar.js')}}"></script>
@stop
@section('container')
   <!-- MAIN CONTENT -->
   <div ng-app="cartApp" ng-controller="cartController" ng-init="pageInit()" id="main" role="main" >
      <div class="container">
         <!-- loader -->
         <div class="loader-wrapper" id="loader">
            <div id="divloader"></div>
         </div>
         <!-- content -->
         <div class="row">       
            <div class="main_content  col-sm-12">          
               <div class="collection_listing_main">
                  <div class="row">
                     <div class="main_content  col-sm-12">
                        <div class="breadcrumb_wrap">
                           <ul class="breadcrumb">
                              <li><a href="{{URL::to('')}}" class="homepage-link" title="Back to the frontpage">Trang Chủ</a></li>
                              <li><a href="{{URL::to('gio-hang')}}"><span class="page-title">Giở Hàng</span></a></li>
                           </ul>
                        </div>
                        <div class="breadcrumb_wrap">
                        <!-- info tab -->
                          <div class="alert alert-info">
                            click vào đây để tìm hiểu về: 
                            <strong>
                            <a  href="{{asset('chi-tiet-ban-tin/cach-thuc-hoat-dong')}}">
                              Phương Thức Hoặt Động Của Hệ Thống
                            </a>
                            </strong>
                          </div>
                        <!-- end infotab -->
                        </div>
                        &nbsp;
                        <div class="col-md-12" ng-show="model.datainit.cartCount==0">
                           <div class="alert alert-warning">
                             <strong>Giỏ Hàng Trống. Vui Lòng Chọn Sản Phẩm:</strong> 
                             <a  href="{{URL::to('')}}">Tiếp Tục Mua Hàng</a>
                           </div>
                         </div>
                        <div class="cart_page " ng-show="model.datainit.cartCount!=0">
                           <div id="cart_content" class="">
                              
                                 <table class="cart_list table">
                                    <thead>
                                       <tr>
                                          <th style="width: auto;">Sản Phẩm</th>
                                          <th style="width: auto;">Giá</th>
                                          <th style="width: auto;">Số Lượng</th>
                                          <th style="width: auto;">Thành Tiền</th>
                                          <th style="width: auto;">Chiết Khấu</th>
                                          <th style="width: auto;">Còn Lại</th>
                                       </tr>
                                    </thead>
                                    <tbody>

                                       <tr class="cart_item" ng-repeat="pro in model.datainit.dtgListCart">
                                          <td class="cell_1" style="width: auto;">
                                             <div class="cart_item__img">
                                                <a href="">  
                                                   <img ng-src="[[pro.product_thumbnail]]" alt="[[pro.product_name]]" style="width: 100%;height: 130px; object-fit: cover;" />
                                                </a>
                                             </div>
                                             <div class="cart_item__info" style="width: auto;">
                                                <h3 class="cart_item__name product_name">
                                                   <a href="#">
                                                      [[pro.product_name]]&nbsp;&nbsp;|&nbsp;&nbsp;<a style="margin-top: -2px" ng-click="updateQuantity([[pro.product_id]],1)" class="cart_item__remove" title="1" href="#"><i class="fa fa-trash"></i></a>
                                                   </a>
                                                </h3>

                                                <div class="cart_item__details" >
                                                   <!-- <p class="item_type"><span>Màu:</span> [[pro.color_name]]</p> -->
                                                   <p class="item_type"><span>Loại:</span> [[pro.cate_name]]</p>
                                                   <p class="item_vendor"><span>Xuất Xứ:</span> [[pro.origin_name]]</p>
                                                   <!-- <p class="item_vendor"><span>Nhà Sản Xuất:</span> [[pro.manufacturer_name]]</p> -->
                                                   <p class="item_weight"><span>Đơn Vị Tính:</span> [[pro.unit_name]]</p>
                                                </div>
                                             </div>
                                          </td>

                                          <td class="cell_2 cart_price text-center">
                                            <span class="money text-center">
                                              [[pro.product_retail_prices | number]] VNĐ
                                            </span>
                                          </td>

                                          <td class="cell_3">
                                             <input 
                                                style="width: 80px;"
                                                type="number"
                                                id="pro_[[pro.product_id]]" 
                                                name="[[pro.product_id]]"
                                                autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                                ng-minlength="1"
                                                ng-maxlength="99"
                                                maxlength="99"
                                                ng-value = "[[pro.quantity]]"
                                                min = "1">
                                             <button ng-click="updateQuantity([[pro.product_id]],0)" class="btn cart_update">Cập Nhật</button>
                                          </td>
                                          <td class="cell_4 cart_price text-center">
                                             <span class="money text-center">
                                                [[pro.root_sum | number]] VNĐ
                                             </span>
                                          </td>
                                          <td class="cell_4 cart_price text-center">
                                             <span class="money text-center">
                                                [[pro.discount]]
                                             </span>
                                          </td>
                                          <td class="cell_4 cart_price text-center">
                                             <span class="money text-center">
                                                [[pro.sum | number]] VNĐ
                                             </span>
                                          </td>

                                       </tr>

                                    </tbody>

                                    <tfoot>
                                       <tr class="cart_buttons">
                                          <td colspan="7">
                                             <a class="btn btn-alt cart_continue" href="{{URL::to('')}}">Tiếp Tục Mua Hàng</a>
                                             <a style="display: none;" class="btn" id="cart_clear" href="#">Xóa Giỏ Hàng</a>
                                          </td>
                                       </tr>

                                       <tr class="cart_summary">
                                          <td colspan="5">

                                             <p class="cart_summary__row">Tổng Thành Tiền <span class="money">[[model.datainit.totalSum | number]] VNĐ</span></p>
                                             <div class="input-group">
                                                <isteven-multi-select
                                                  tabindex="2"
                                                  name="slbBranch"
                                                  input-model="model.datainit.slbBranch"
                                                  output-model="model.request.slbBranch"
                                                  helper-elements="filter"
                                                  button-label="branch_name"
                                                  item-label="branch_name"
                                                  output-properties="branch_id"
                                                  search-property="branch_name"
                                                  tick-property="ticked"
                                                  orientation="vertical"
                                                  disable-property="false"
                                                  is-disabled="false"
                                                  selection-mode="single"
                                                  min-search-length="10"
                                                  translation="model.localLang"
                                                  max-labels="1"  
                                                  directive-id="slbBranch"
                                                  on-item-click="slbItemClick(data,'txtBranchId')">
                                                </isteven-multi-select>

                                                <input  
                                                    type="text"
                                                    class="form-control" 
                                                    id="txtBranchId" 
                                                    name="txtBranchId" 
                                                    ng-model="model.request.txtBranchId"
                                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false""
                                                    style="display: none;"
                                                   >
                                             </div>
                                          </td>
                                          <td colspan="2">
                                             <div class="cart_summary__checkout">
                                                
                                                @if(Session::has('customerInfo'))
                                                   <button 
                                                   ng-disabled="model.request.txtBranchId==''"
                                                   ng-click="saveInvoiceSession()" 
                                                   class="btn btn-alt">Gởi Đơn Hàng</button>
                                                @else
                                                   <a href="{{URL::to('/dang-nhap')}}" class="btn btn-alt">Đăng Nhập Để Gởi Đơn Hàng 
                                                   </a>
                                                @endif
                                             </div>
                                          </td>
                                       </tr>
                                       <tr class="cart_summary">
                                          <td colspan="7">
                                             <div ng-show="model.request.txtBranchId!=''">
                                                <div class="form-group">
                                                   <label class="control-label " for="email">Địa Chỉ:</label>
                                                   <div class="">
                                                      [[model.request.txtBranchAddress]]
                                                   </div>
                                                </div>
                                                <div class="form-group">
                                                   <label class="control-label" for="email">SĐT:</label>
                                                   <div class="">
                                                      [[model.request.txtBranchPhone]]
                                                   </div>
                                                </div>
                                             </div>
                                             @if(Session::has('customerInfo'))
                                             @else
                                                <p class="alert alert-warning cart_summary__notification">Bạn Cần Đăng Nhập Để Nhận Thông Tin Đơn Hàng
                                                </p>
                                             @endif
                                          </td>
                                       </tr>
                                    </tfoot>
                                 </table>
                           
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
@stop