@extends('front_end.layouts.index')

@section('headTitle')
   <title>Trang Chủ - Sinh Viên Kinh Doanh</title>
@stop

@section('headCss')
  <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-toastr/angular-toastr.css')}}" />
@stop

@section('headJs')
  


    <!-- UI Bootstrap Modal --> 
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-bootstrap/ui-bootstrap-tpls-2.0.0.min.js')}}"></script>

    <!-- Angular Toastr -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-toastr/angular-toastr.tpls.js')}}"></script>

  
     <!-- JS -->
  <script type="text/javascript" src="{{asset('public/system/common/system.common.angurlar.js')}}"></script>

  <script type="text/javascript" src="{{asset('public/front_end/common/login.angurlar.js')}}"></script>
@stop

@section('container')
  <!-- MAIN CONTENT -->
   <div id="main" role="main" ng-app="loginApp" ng-controller="loginController" ng-init="pageInit('')">
      <div class="container">
         <div class="row">       
            <div class="main_content  col-sm-9 col-sm-push-3">
              <div class="loader-wrapper" id="loader">
                <div id="divloader"></div>
              </div>
              <div class="main_content  col-sm-12">
            
                <!-- BREADCRUMBS -->
                <div class="breadcrumb_wrap">
                  <ul class="breadcrumb">
                    <li><a href="/" class="homepage-link" title="Back to the frontpage">Trang Chủ</a></li>
                    <li><span class="page-title">Đăng Ký</span></li>
                  </ul>
                </div>
                <h2 class="page_heading">Đăng Ký</h2>
                <!-- info tab -->
                <div class="alert alert-info">
                  click vào đây để tìm hiểu về: 
                  <strong>
                  <a  href="{{asset('chi-tiet-ban-tin/cach-thuc-hoat-dong')}}">
                    Phương Thức Hoặt Động Của Hệ Thống
                  </a>
                  </strong>
                </div>
                <!-- end infotab -->
                <div id="account_register" class="account_section">
                  <h4>Tạo Tài Khoản</h4>
                  <div class="form-horizontal">
                    <form id="frmRegister" name="frmRegister" ng-submit="submitForm(frmRegister.$valid)" novalidate>
                      <input type="hidden" name="utf8" value="✓" />
                      <!--  txtRegisterAccount -->
                      <div class="clearfix form-group">
                        <label for="first_name" class="login control-label col-sm-4">Tên Tài Khoản</label>
                        <div class="col-sm-4">
                          <input class="form-control" 
                             type="text" 
                             placeholder="Tên đăng nhập" 
                             id="txtRegisterAccount" 
                             name="txtRegisterAccount"
                             ng-model="registerModel.registerAccount" 
                             ng-minlength="6" 
                             ng-maxlength="20"
                             maxlength="20" required>
                          <span style="color: red" class="help-block" ng-show="frmRegister.txtRegisterAccount.$error.required && frmRegister.txtRegisterAccount.$invalid && !frmRegister.txtRegisterAccount.$pristine">Vui Lòng Nhập.</span>
                            <span style="color: red" class="help-block" ng-show="frmRegister.txtRegisterAccount.$error.minlength">Tên đăng nhập Không Được Ngắn Hơn 6 Ký Tự.</span>
                            <span style="color: red" class="help-block" ng-show="frmRegister.txtRegisterAccount.$error.maxlength">Tên đăng nhập Không Được Dài Hơn 20 Ký Tự.</span>
                        </div>
                      </div>
                      <!--  txtRegisterFullName -->
                      <div class="clearfix form-group">
                        <label for="email" class="login control-label col-sm-4">Họ Tên</label>
                        <div class="col-sm-4">
                          <input class="form-control" 
                             type="text" 
                             placeholder="Họ Tên" 
                             id="txtRegisterFullName" 
                             name="txtRegisterFullName"
                             ng-model="registerModel.registerFullName" 
                             ng-minlength="2" 
                             ng-maxlength="100"
                             maxlength="100" required>
                          <span style="color: red" class="help-block" ng-show="frmRegister.txtRegisterFullName.$error.required && frmRegister.txtRegisterFullName.$invalid && !frmRegister.txtRegisterFullName.$pristine">Vui Lòng Nhập.</span>
                            <span style="color: red" class="help-block" ng-show="frmRegister.txtRegisterFullName.$error.minlength">Họ Tên Không Được Ngắn Hơn 2 Ký Tự.</span>
                            <span style="color: red" class="help-block" ng-show="frmRegister.txtRegisterFullName.$error.maxlength">Họ Tên Không Được Dài Hơn 100 Ký Tự.</span>
                        </div>
                      </div>
                      <!--  txtRegisterEmail -->
                      <div class="clearfix form-group">
                        <label for="last_name" class="login control-label col-sm-4">Email</label>
                        <div class="col-sm-4">
                          <input class="form-control" 
                             type="email" 
                             placeholder="Email" 
                             id="txtRegisterEmail" 
                             name="txtRegisterEmail"
                             ng-model="registerModel.registerEmail" 
                             ng-minlength="6" 
                             ng-maxlength="100"
                             maxlength="100" required>
                          <span style="color: red" class="help-block" ng-show="frmRegister.txtRegisterEmail.$error.required && frmRegister.txtRegisterEmail.$invalid && !frmRegister.txtRegisterEmail.$pristine">Vui Lòng Nhập.</span>
                            <span style="color: red" class="help-block" ng-show="frmRegister.txtRegisterEmail.$error.minlength">Email Không Được Ngắn Hơn 6 Ký Tự.</span>
                            <span style="color: red" class="help-block" ng-show="frmRegister.txtRegisterEmail.$error.maxlength">Email Không Được Dài Hơn 100 Ký Tự.</span>
                           <span style="color: red" class="help-block" ng-show="frmRegister.txtRegisterEmail.$error.email">Định Dạng Email Không Hợp Lệ.</span>
                        </div>
                      </div>
                      <!--  txtRegisterPhone -->
                      <div class="clearfix form-group">
                        <label for="last_name" class="login control-label col-sm-4">Số điện thoại</label>
                        <div class="col-sm-4">
                          <input class="form-control" 
                             type="text" 
                             placeholder="Số điện thoại" 
                             id="txtRegisterPhone" 
                             name="txtRegisterPhone"
                             ng-model="registerModel.registerPhone" 
                             ng-minlength="10" 
                             ng-maxlength="11"
                             maxlength="11" required>
                           <span style="color: red" class="help-block" ng-show="frmRegister.txtRegisterPhone.$error.required && frmRegister.txtRegisterPhone.$invalid && !frmRegister.txtRegisterPhone.$pristine">Vui Lòng Nhập.</span>
                            <span style="color: red" class="help-block" ng-show="frmRegister.txtRegisterPhone.$error.minlength">Số điện thoại Không Được Ngắn Hơn 10 Ký Tự.</span>
                            <span style="color: red" class="help-block" ng-show="frmRegister.txtRegisterPhone.$error.maxlength">Số điện thoại Không Được Dài Hơn 11 Ký Tự.</span>
                        </div>
                      </div>
                      <!--  txtRegisterAddress -->
                      <div class="clearfix form-group">
                        <label for="email" class="login control-label col-sm-4">Địa Chỉ</label>
                        <div class="col-sm-4">
                          <input class="form-control" 
                             type="text" 
                             placeholder="Địa Chỉ" 
                             id="txtRegisterAddress" 
                             name="txtRegisterAddress"
                             ng-model="registerModel.registerAddress" 
                             ng-minlength="2" 
                             ng-maxlength="200"
                             maxlength="200" required>
                           <span style="color: red" class="help-block" ng-show="frmRegister.txtRegisterAddress.$error.required && frmRegister.txtRegisterAddress.$invalid && !frmRegister.txtRegisterAddress.$pristine">Vui Lòng Nhập.</span>
                            <span style="color: red" class="help-block" ng-show="frmRegister.txtRegisterAddress.$error.minlength">Địa Chỉ Không Được Ngắn Hơn 2 Ký Tự.</span>
                            <span style="color: red" class="help-block" ng-show="frmRegister.txtRegisterAddress.$error.maxlength">Địa Chỉ Không Được Dài Hơn 200 Ký Tự.</span>
                        </div>
                      </div>
                      <!--  txtRegisterIdentify -->
                      <div class="clearfix form-group">
                        <label for="email" class="login control-label col-sm-4">CMND</label>
                        <div class="col-sm-4">
                          <input class="form-control" 
                             type="text" 
                             placeholder="CMND" 
                             id="txtRegisterIdentify" 
                             name="txtRegisterIdentify"
                             ng-model="registerModel.registerIdentify" 
                             ng-minlength="8" 
                             ng-maxlength="11"
                             maxlength="11" required>
                          <span style="color: red" class="help-block" ng-show="frmRegister.txtRegisterIdentify.$error.required && frmRegister.txtRegisterIdentify.$invalid && !frmRegister.txtRegisterIdentify.$pristine">Vui Lòng Nhập.</span>
                            <span style="color: red" class="help-block" ng-show="frmRegister.txtRegisterIdentify.$error.minlength">CMND Không Được Ngắn Hơn 8 Ký Tự.</span>
                            <span style="color: red" class="help-block" ng-show="frmRegister.txtRegisterIdentify.$error.maxlength">CMND Không Được Dài Hơn 11 Ký Tự.</span>
                        </div>
                      </div>
                      <!--  txtRegisterStudentCard -->
                      <div class="clearfix form-group">
                        <label for="email" class="login control-label col-sm-4">Mã Sinh Viên</label>
                        <div class="col-sm-4">
                          <input class="form-control" 
                             type="text" 
                             placeholder="Mã Sinh Viên" 
                             id="txtRegisterStudentCard" 
                             name="txtRegisterStudentCard"
                             ng-model="registerModel.registerStudentCard" 
                             ng-minlength="2" 
                             ng-maxlength="20"
                             maxlength="20" required>
                          <span style="color: red" class="help-block" ng-show="frmRegister.txtRegisterStudentCard.$error.required && frmRegister.txtRegisterStudentCard.$invalid && !frmRegister.txtRegisterStudentCard.$pristine">Vui Lòng Nhập.</span>
                            <span style="color: red" class="help-block" ng-show="frmRegister.txtRegisterStudentCard.$error.minlength">Mã Sinh Viên Không Được Ngắn Hơn 2 Ký Tự.</span>
                            <span style="color: red" class="help-block" ng-show="frmRegister.txtRegisterStudentCard.$error.maxlength">Mã Sinh Viên Không Được Dài Hơn 20 Ký Tự.</span>
                        </div>
                      </div>
                      <!--  txtRegisterStudentUniversity -->
                      <div class="clearfix form-group">
                        <label for="email" class="login control-label col-sm-4">Trường Học</label>
                        <div class="col-sm-4">
                          <input class="form-control" 
                             type="text" 
                             placeholder="Trường Học" 
                             id="txtRegisterStudentUniversity" 
                             name="txtRegisterStudentUniversity"
                             ng-model="registerModel.registerStudentUniversity" 
                             ng-minlength="2" 
                             ng-maxlength="50"
                             maxlength="50" required>
                             <span style="color: red" class="help-block" ng-show="frmRegister.txtRegisterStudentUniversity.$error.required && frmRegister.txtRegisterStudentUniversity.$invalid && !frmRegister.txtRegisterStudentUniversity.$pristine">Vui Lòng Nhập.</span>
                            <span style="color: red" class="help-block" ng-show="frmRegister.txtRegisterStudentUniversity.$error.minlength">Trường Không Được Ngắn Hơn 2 Ký Tự.</span>
                            <span style="color: red" class="help-block" ng-show="frmRegister.txtRegisterStudentUniversity.$error.maxlength">Trường Không Được Dài Hơn 50 Ký Tự.</span>
                        </div>
                      </div>
                      <!--  txtRegisterPassword -->
                      <div class="clearfix form-group">
                        <label for="password" class="login control-label col-sm-4">Mật Khẩu</label>
                        <div class="col-sm-4">
                          <input   class="form-control" 
                           type="Password" 
                           placeholder="Mật khẩu" 
                           id="txtRegisterPassword" 
                           name="txtRegisterPassword"
                           ng-model="registerModel.registerPassword" 
                           ng-minlength="6" 
                           ng-maxlength="30"
                           maxlength="30" required>
                            <span style="color: red" class="help-block" ng-show="frmRegister.txtRegisterPassword.$error.required && frmRegister.txtRegisterPassword.$invalid && !frmRegister.txtRegisterPassword.$pristine">Vui Lòng Nhập.</span>
                            <span style="color: red" class="help-block" ng-show="frmRegister.txtRegisterPassword.$error.minlength">Mật Khẩu Không Được Ngắn Hơn 6 Ký Tự.</span>
                            <span style="color: red" class="help-block" ng-show="frmRegister.txtRegisterPassword.$error.maxlength">Mật Khẩu Không Được Dài Hơn 30 Ký Tự.</span>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-4">
                          <input 
                            ng-disabled="frmRegister.$invalid" 
                            ng-click="registerUser()" 
                            type="button" 
                            value="Đăng ký" 
                            class="btn"/>
                          <a class="account_register__cancel" href="#">Hủy</a>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
               
              </div>
            </div>
            <!-- ============================= side bar ==================== -->
            @include('front_end.layouts.menu',[ 
              'listCategoriesuserAuth' => $listCategories,
              'listTags' => $listTags,
              'listQuickSales' => $listQuickSales,
            ])           
         </div>
      </div>
   </div>
@stop
