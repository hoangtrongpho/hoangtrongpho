@extends('front_end.layouts.index')

@section('headTitle')
   <title>Danh Mục Sản Phẩm- Sinh Viên Kinh Doanh</title>
@stop

@section('headCss')
  <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-toastr/angular-toastr.css')}}" />
@stop

@section('headJs')
 <!-- UI Bootstrap Modal --> 
  <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-bootstrap/ui-bootstrap-tpls-2.0.0.min.js')}}"></script>
  <!-- Angular Toastr -->
  <script type="text/javascript" src="{{asset('public/system/plugins/angular-toastr/angular-toastr.tpls.js')}}"></script>
  <!-- JS -->
  <script type="text/javascript" src="{{asset('public/system/common/system.common.angurlar.js')}}"></script>
  <script type="text/javascript" src="{{asset('public/front_end/common/front_end.categorydetail.angurlar.js')}}"></script>
@stop
@section('container')
   <!-- MAIN CONTENT -->
   <div id="main" role="main" ng-app="productDetailApp" ng-controller="productDetailController" ng-init="pageInit()">
      <div class="container">
        
        <div class="row">       
            <div class="main_content  col-sm-9 col-sm-push-3">          

               <div class="collection_listing_main">
                    <?php
                         $name= "Danh Mục Trống";
                      ?>
                    @if(count($listCatePro)>0)
                        @foreach($listCatePro as $index => $c)  
                            <?php
                                 $name= $c->cate_name;
                                  break;
                              ?>
                        @endforeach
                    @endif
                     <h3 class="page_heading slider_wrap" style="margin: 10px 7px;height: 60px;padding: 0 20px;background: #01489A;font-size: 1.5em;line-height: 60px;color: #ffffff;-webkit-border-radius: 5px 5px 0 0;border-radius: 5px 5px 0 0;">{{$name}}</h3>
                     @if(count($listCatePro)<=0)
                     <div></div>
                     @else
                        @foreach($listCatePro as $index => $c)  

                           <div class="col-xs-12 col-sm-6 col-md-4" style="border: 1px solid #dddddd;padding: 15px; margin: 7px 7px;">
                               <div class="product_wrapper">
                                   <div class="product_img">
                                     <a class="img_change" href="{{URL::to('chi-tiet-san-pham/'.$c->product_slug)}}">
                                         <img src="{{$c->product_thumbnail}}" title="{{$c->product_name}}" alt="{{$c->product_slug}}"
                                           style="width:100%; height:270px; object-fit:cover;"> 
                                         <span class="product_badge new">Mới</span>
                                     </a>
                                   </div>
                                   <div class="product_info">
                                     <div class="product_price">
                                         <span class="money">
                                         {{ number_format(($c->product_retail_prices), 0) }} VNĐ
                                         </span><br>
                                         <span style="display: none;" class="money compare-at-price">
                                         {{ number_format(($c->product_retail_prices), 0) }} VNĐ
                                         </span>             
                                     </div>
                                     <div class="product_name" style="height: 38px;">
                                         <a href="{{URL::to('chi-tiet-san-pham/'.$c->product_slug)}}">{{mb_strimwidth($c->product_name,0,50,"...")}}</a>
                                     </div>
                                     <div class="product_desc product_desc_short" style="height: 38px;">{!!mb_strimwidth($c->product_description,0,70,"...")!!}</div>
                                     <div class="product_desc product_desc_long"></div>
                                     <div class="product_links">
                                        <a ng-click="addCart('{{$c->product_id}}')" class="btn" href="#" title="Cart"><i class="fa fa-fw fa-cart-plus"></i>&nbsp;&nbsp;Mua Ngay</a>
                                        <a class="btn pull-right" href="{{URL::to('chi-tiet-san-pham/'.$c->product_slug)}}" title="Xem Chi Tiết"><i class="fa fa-fw fa-eye"></i>&nbsp;&nbsp;Chi Tiết</a>
                                     </div>
                                   </div>
                                   <div class="clearfix"></div>
                               </div>
                             </div> 
                              
                        @endforeach
                     @endif
                     
               </div>
            </div>
            <!-- ============================= side bar ==================== -->
            @include('front_end.layouts.menu',[ 
              'listCategoriesuserAuth' => $listCategories,
              'listTags' => $listTags,
              'listQuickSales' => $listQuickSales,
            ])
        </div>
      </div>
   </div>
@stop