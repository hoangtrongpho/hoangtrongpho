@if ($paginator->hasPages())
      <ul class="pagination">
         {{-- Previous Page Link --}}
         @if ($paginator->onFirstPage())
            <!-- <li class=" disabled"><span class="">&laquo;&nbsp;Trước</span></li> -->
         @else
            <li class=""><a class="" href="{{ $paginator->previousPageUrl() }}" rel="prev">&laquo;&nbsp;Trước</a></li>
         @endif

         {{-- Pagination Elements --}}
         @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class=" disabled"><span class="">{{ $element }}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class=" active">
                            <a href="" style="pointer-events:none">{{ $page }}</a>
                        </li>
                    @else
                        <li class=""><a class="" href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
         @endforeach

         {{-- Next Page Link --}}
         @if ($paginator->hasMorePages())
            <li class=""><a class="" href="{{ $paginator->nextPageUrl() }}" rel="next">Tiếp&nbsp;&raquo;</a></li>
         @else
            <!-- <li class=" disabled"><span class="">Tiếp&nbsp;&raquo;</span></li> -->
         @endif
      </ul>
@endif
