@extends('front_end.layouts.index')

@section('headTitle')
   <title>Trang Chủ - Sinh Viên Kinh Doanh</title>
@stop

@section('headCss')
    <script type="text/javascript" src="{{asset('public/js/jquery/jquery-3.1.1.min.js').'?v='.Carbon\Carbon::now()->timestamp}}"></script> 
    <script type="text/javascript" src="{{asset('public/js/bootstrap/bootstrap.min.js').'?v='.Carbon\Carbon::now()->timestamp}}"></script> 
   <style type="text/css">
      .modal-backdrop {z-index: 0 !important}
   </style>
      <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-bootstrap-datetimepicker/css/datetimepicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/daterangepicker/daterangepicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-ui-switch/bootstrap-switch.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-multi-select/isteven-multi-select.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-toastr/angular-toastr.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/iCheck/flat/blue.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/jquery.dataTables.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/dataTables.bootstrap.css')}}" />
    <style type="text/css">
       .modal-lg{
          width:900px;
      }
    </style>t
@stop

@section('headJs')

    <!-- Date Time Picker -->
    <script type="text/javascript" src="{{asset('public/system/plugins/daterangepicker/daterangepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-daterangepicker/angular-daterangepicker.min.js')}}"></script>

    <script src="{{asset('public/system/plugins/angular-ui-switch/angular-ui-switch.js')}}"></script>
    <script src="{{asset('public/system/plugins/angular-ui-switch/bootstrap-switch.js')}}"></script>
    <script src="{{asset('public/system/plugins/angular-ui-switch/angular-bootstrap-switch.js')}}"></script>

    <!-- iCheck -->
    <script src="{{asset('public/system/plugins/angular_bootstrap_checkbox/angular-bootstrap-checkbox.js')}}"></script>
    <!-- Input Currency -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-ng-currency/ng-currency.directive.js')}}"></script>
    <!-- File Manager -->
    <!-- Angular Multi Select -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-multi-select/isteven-multi-select.js')}}"></script>

    <!-- UI Bootstrap Modal --> 
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-bootstrap/ui-bootstrap-tpls-2.0.0.min.js')}}"></script>

    <!-- Angular Toastr -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-toastr/angular-toastr.tpls.js')}}"></script>

    <!-- Angular Confirm Dialog -->
    <script type="text/javascript" src="{{asset('public/system/plugins/bootbox/bootbox.min.js')}}"></script>

    <!-- Angular Datatables-->
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/angular-datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/colreorder/angular-datatables.colreorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/bootstrap/angular-datatables.bootstrap.min.js')}}"></script>
    <!-- JS -->
   <script type="text/javascript" src="{{asset('public/system/common/system.common.angurlar.js')}}"></script>

    <script type="text/javascript" src="{{asset('public/front_end/common/front_end.historybill.angurlar.js')}}"></script>

@stop
@section('container')

   <!-- MAIN CONTENT -->
   <div ng-app="cartApp1" ng-controller="cart1Controller" ng-init="pageInit()" id="main" role="main" >
      <div class="container">

         <!-- Modal -->
         <div id="modalAdd" class="modal fade   " role="dialog">
            <div class="modal-dialog modal-lg">
               <form class="form" class="form-inline" name="frmAdd" id="frmAdd" novalidate>
                  <!-- Modal content-->
                  <div class="modal-content">
                     <div class="modal-header bg-blue">
                       
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        
                     </div>
                     <div class="modal-body">
                        <!--  Group Thông Tin hóa Đơn--> 
                        <div class="panel-group">
                          <div class="panel panel-primary">
                            <div class="panel-heading">Thông Tin Chung</div>
                            <div class="panel-body">
                              <!--  Group Thông Tin hóa Đơn-->
                              <div class="row"> 
                                 <!--   ===== Số HD ======= -->
                                 <div class="col-md-6">
                                    <label for="txtInvoiceNumber" class="control-label display-block-xs">Mã Đơn Hàng:</label>  
                                    <div class="input-group">
                                       <span class="input-group-addon">
                                          <i class="fa fa-fw fa-credit-card" aria-hidden="true"></i>
                                       </span>
                                       <input  
                                            tabindex="1"
                                            type="text"
                                            class="form-control" 
                                            id="txtInvoiceNumber" 
                                            name="txtInvoiceNumber" 
                                            placeholder="Số Hóa Đơn" 
                                            ng-model="model.request.txtInvoiceNumber"
                                            autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                            ng-minlength="2"
                                            ng-maxlength="20"
                                            maxlength="20"
                                            ng-disabled=true
                                             >
                                    </div>
                                 </div>
                                 <!--   ===== Ngày Đặt ======= -->
                                 <div class="col-md-6">
                                    <label for="txtInvoiceNumber" class="control-label display-block-xs">Ngày Đặt:</label>  
                                    <div class="input-group">
                                       <span class="input-group-addon">
                                          <i class="fa fa-fw fa-credit-card" aria-hidden="true"></i>
                                       </span>
                                       <input  
                                            tabindex="1"
                                            type="text"
                                            class="form-control" 
                                            id="txtUpdatedDate" 
                                            name="txtUpdatedDate" 
                                            placeholder="Ngày Đặt" 
                                            ng-model="model.request.txtUpdatedDate"
                                            autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                            ng-minlength="0"
                                            ng-maxlength="50"
                                            maxlength="20"
                                            ng-disabled=true
                                             >
                                    </div>
                                 </div>
                                 <!--   ========= Customer name ========== -->
                                 <div class="col-md-6" >
                                    <label for="txtCustomerName" class="control-label display-block-xs">Tên Khách Hàng:</label>  
                                    <div class="input-group">
                                       <span class="input-group-addon">
                                            <i class="fa fa-fw fa-database" aria-hidden="true"></i>
                                       </span>
                                       <input  
                                            tabindex="1"
                                            type="text"
                                            class="form-control" 
                                            id="txtCustomerName" 
                                            name="txtCustomerName" 
                                            placeholder="Tên Khách Hàng" 
                                            ng-model="model.request.txtCustomerName"
                                            autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                            ng-minlength="2"
                                            ng-maxlength="50"
                                            maxlength="50"
                                            ng-disabled=true
                                             >
                                    </div>
                                 </div>
                                 <!--  ======== Customer Email ========= -->
                                 <div class="col-md-6">
                                    <label for="txtCustomerEmail" class="control-label display-block-xs">Email Khách Hàng:</label>  
                                    <div class="input-group">
                                       <span class="input-group-addon">
                                          <i class="fa fa-fw fa-bookmark-o" aria-hidden="true"></i>
                                       </span>
                                       <input  
                                            tabindex="1"
                                            type="text"
                                            class="form-control" 
                                            id="txtCustomerEmail" 
                                            name="txtCustomerEmail" 
                                            placeholder="Email Khách Hàng" 
                                            ng-model="model.request.txtCustomerEmail"
                                            autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                            ng-minlength="2"
                                            ng-maxlength="150"
                                            maxlength="150"
                                            ng-disabled=true
                                             >
                                    </div>
                                 </div>
                                 <!--   ==== Customer Phone number ======= -->
                                 <div class="col-md-6">
                                    <label for="txtCustomerPhone" class="control-label display-block-xs">Số Điện Thoại:</label>  
                                    <div class="input-group">
                                       <span class="input-group-addon">
                                            <i class="fa fa-fw fa-info" aria-hidden="true"></i>
                                       </span>
                                       <input  
                                            tabindex="1"
                                            type="text"
                                            class="form-control" 
                                            id="txtCustomerPhone" 
                                            name="txtCustomerPhone" 
                                            placeholder="Số Điện Thoại" 
                                            ng-model="model.request.txtCustomerPhone"
                                            autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                            ng-minlength="10"
                                            ng-maxlength="11"
                                            maxlength="11"
                                            ng-disabled=true
                                             >
                                    </div>
                                 </div>
                                 <!--   ====== Customer address ====== -->
                                 <div class="col-md-6">
                                    <label for="txtCustomerAddress" class="control-label display-block-xs">Địa Chỉ Khách Hàng:</label>  
                                    <div class="input-group">
                                       <span class="input-group-addon">
                                          <i class="fa fa-fw fa-street-view" aria-hidden="true"></i>
                                       </span>
                                       <input  
                                            tabindex="1"
                                            type="text"
                                            class="form-control" 
                                            id="txtCustomerAddress" 
                                            name="txtCustomerAddress" 
                                            placeholder="Địa Chỉ Khách Hàng" 
                                            ng-model="model.request.txtCustomerAddress"
                                            autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                            ng-minlength="2"
                                            ng-maxlength="150"
                                            maxlength="150"
                                            ng-disabled=true
                                            required >
                                    </div>
                                 </div>
                                 <!--   ============== ghi chu============= -->
                                 <div class="col-md-12" style="display: none;">
                                    <label for="txtInvoiceId" class="control-label display-block-xs">Ghi Chú:</label>  
                                    <div class="input-group">
                                       <span class="input-group-addon">
                                          <i class="fa fa-fw fa-indent" aria-hidden="true"></i>
                                       </span>
                                       <input  
                                          tabindex="1"
                                          type="text"
                                          class="form-control" 
                                          id="txtInvoiceNote" 
                                          name="txtInvoiceNote" 
                                          placeholder="Ghi Chú" 
                                          ng-model="model.request.txtInvoiceNote"
                                          autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                          ng-minlength="2"
                                          ng-maxlength="200"
                                          maxlength="200"
                                          ng-disabled=true
                                           >
                                    </div>
                                 </div>
                              </div>
                              <!--  END Group Thông Tin hóa Đơn-->
                            </div>
                          </div>
                        </div>
                        <!--  END Group Thông Tin hóa Đơn-->
                        <!-- GROUP Thông Tin Nhận Hàng--> 
                        <div class="panel-group">
                          <div class="panel panel-primary">
                            <div class="panel-heading">Thông Tin Địa Chỉ Nhận Hàng</div>
                            <div class="panel-body">
                              <!-- GROUP Thông Tin Nhận Hàng-->
                              <div class="row"> 
                                 <!--   ============== txtBranchName============ -->
                                 <div class="col-md-6" >
                                    <label for="txtInvoiceId" class="control-label display-block-xs">Tên Chi Nhánh Nhận Hàng:</label>  
                                    <div class="input-group">
                                       <span class="input-group-addon">
                                          <i class="fa fa-fw fa-indent" aria-hidden="true"></i>
                                       </span>
                                       <input  
                                          tabindex="1"
                                          type="text"
                                          class="form-control" 
                                          id="txtBranchName" 
                                          name="txtBranchName" 
                                          placeholder="Tên Chi Nhánh" 
                                          ng-model="model.request.txtBranchName"
                                          autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                          ng-disabled=true
                                           >
                                    </div>
                                 </div>
                                 <!--   ============== txtBranchPhone============= -->
                                 <div class="col-md-6" >
                                    <label for="txtInvoiceId" class="control-label display-block-xs">SĐT Chi Nhánh:</label>  
                                    <div class="input-group">
                                       <span class="input-group-addon">
                                          <i class="fa fa-fw fa-indent" aria-hidden="true"></i>
                                       </span>
                                       <input  
                                          tabindex="1"
                                          type="text"
                                          class="form-control" 
                                          id="txtBranchPhone" 
                                          name="txtBranchPhone" 
                                          placeholder="Ghi Chú" 
                                          ng-model="model.request.txtBranchPhone"
                                          autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                          ng-disabled=true
                                           >
                                    </div>
                                 </div>
                                 <!--   ============== txtBranchAddress============= -->
                                 <div class="col-md-12" >
                                    <label for="txtInvoiceId" class="control-label display-block-xs">Địa Chỉ Chi Nhánh Nhận Hàng:</label>  
                                    <div class="input-group">
                                       <span class="input-group-addon">
                                          <i class="fa fa-fw fa-indent" aria-hidden="true"></i>
                                       </span>
                                       <input  
                                          tabindex="1"
                                          type="text"
                                          class="form-control" 
                                          id="txtBranchAddress" 
                                          name="txtBranchAddress" 
                                          placeholder="Địa Chỉ" 
                                          ng-model="model.request.txtBranchAddress"
                                          autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                          ng-disabled=true
                                           >
                                    </div>
                                 </div>
                              </div>
                              <!-- END GROUP Thông Tin Nhận Hàng-->
                            </div>
                          </div>
                        </div>
                        <!-- END GROUP Thông Tin Nhận Hàng-->
                        <!--  Group Thông Tin Đơn Hàng--> 
                        <div class="panel-group">
                          <div class="panel panel-primary">
                            <div class="panel-heading">Chi Tiết Đơn Hàng</div>
                            <div class="panel-body">
                              <!--   total sum -->
                              <div class="row"> 
                                 <div class="input-group col-sm-6">
                                    <span class="input-group-addon">
                                       Tổng Thành Tiền:
                                    </span>
                                    <span class="input-group-addon">
                                       [[model.request.txtTotalSum | number]]
                                    </span>
                                    <span class="input-group-addon">
                                       VNĐ
                                    </span>
                                 </div>
                              </div>
                              <!--   pro list -->
                              <div class="row"> 
                                <div class="col-sm-12">
                                    <table  
                                          style="color: darkblue" 
                                          name="listResult" 
                                          id="listResult" 
                                          style="width: 100%;"
                                          dt-options="dtOptions"
                                          dt-column-defs="dtColumnDefs"
                                          class="table table-bordered table-striped table-condensed">
                                        <thead>
                                            <tr>
                                                <th class="text-center" data-column-index="1" style="width: 100px;">Hình Ảnh</th>
                                                <th class="text-center" data-column-index="2" style="width: auto;">Tên Sản Phẩm</th>
                                                <th class="text-center" data-column-index="3" style="width: auto;">Số Lượng</th>
                                                <th class="text-center" data-column-index="4" style="width: auto;">Giá Bán</th>
                                                <th class="text-center" data-column-index="5" style="width: auto;">Chiết Khấu</th>
                                                <th class="text-center" data-column-index="6" style="width: auto;">Thành Tiền</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr role="row" ng-repeat="pro in model.request.dtgListProductInvoice">
                                                <td>
                                                    <img ng-src="[[pro.product_thumbnail]]"
                                                     onError="this.src='{{asset('public/images/default-image.png')}}';"
                                                     id="holder1" 
                                                     class="img-responsive img-thumbnail" 
                                                     alt="Ảnh Đại Diện" 
                                                     title="Ảnh Đại Diện" 
                                                     height="50px">
                                                </td>
                                                <td>
                                                  [[pro.product_name | limitTo:50]]
                                                </td>
                                                <td>
                                                    [[pro.quantity]]
                                                </td>
                                                <td>
                                                  [[pro.product_retail_prices | number]] VNĐ
                                                </td>
                                                <td>
                                                  [[100- (pro.total_price *100/(pro.product_retail_prices*pro.quantity) )]] %
                                                </td>
                                                <td>
                                                  [[pro.total_price | number]] VNĐ
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- END Group Thông Tin Đơn Hàng--> 
                     </div>
                  </div>
               </form>
            </div>
         </div>
         <!-- loader -->
         <div class="loader-wrapper" id="loader">
            <div id="divloader"></div>
         </div>

         <!-- content -->
         <div class="row">       
            <div class="main_content  col-sm-9 col-sm-push-3">          
               <!--==================== NIVOSLIDER TOP==============-->                 
              
               <!--=============== bill list =================-->

               <hr style="display: none;">
               <div class="collection_listing_main">
                  <div class="row">
                     <div class="main_content  col-sm-12">
                        <div class="breadcrumb_wrap">
                           <ul class="breadcrumb">
                              <li><a href="{{URL::to('')}}" class="homepage-link" title="Back to the frontpage">Trang Chủ</a></li>
                          
                              <li><a href="{{URL::to('lich-su-don-hang')}}"><span class="page-title">Đơn Hàng</span></a></li>
                           </ul>
                        </div>
                        <div class="col-md-12" ng-show="model.datainit.dtgListInvoice.length==0">
                          <div class="alert alert-warning">
                            <strong>Đơn Hàng Trống. Vui Lòng Đặt Hàng:</strong> 
                            <a  href="{{URL::to('')}}">Tiếp Tục Mua Hàng</a>
                          </div>
                        </div>
                        <div class="cart_page">
                           <div id="cart_content" class="table-responsive">
                                 <table class="cart_list table" style="font-size: 16px">
                                    <thead>
                                       <tr>
                                          <th style="width: auto;">Tùy Chọn</th>
                                          <th style="width: auto;">Mã Đơn Hàng</th>
                                          <th style="width: auto;">Ngày Lập</th>
                                          <th style="width: auto;">Tổng Tiền</th>
                                          <th style="width: auto;">Trạng Thái</th>
                                       </tr>
                                    </thead>
                                    <tbody>

                                       <tr class="cart_item" ng-repeat="inv in model.datainit.dtgListInvoice">
                                          <td class="cell_1" style="width: auto;">
                                             <button ng-click="loadInvoiceDetail(inv.invoice_id)" data-toggle="modal" data-target="#modalAdd"  class="label label-primary"><i class="fa fa-eye" aria-hidden="true"></i> Chi Tiết</button>
                                             <button style="display: none" ng-click="removeItem(inv.invoice_id)" class="label label-danger"><i class="fa fa-trash" aria-hidden="true"></i> Hủy</button>
                                          </td>

                                          <td class="cell_2" style="width: auto;">
                                             <a ng-click="loadInvoiceDetail(inv.invoice_id)" data-toggle="modal" data-target="#modalAdd" > [[inv.invoice_number]]</a >
                                          </td>

                                          <td class="cell_3" style="width: auto;">
                                             [[inv.updated_date_format]]
                                          </td>

                                          <td class="cell_4" style="width: auto;">
                                             [[inv.total_sum |  number]] VNĐ
                                          </td>
                                          <td class="cell_4" style="width: auto;">
                                             <button ng-show="inv.status==1" class="label label-default"><i class="fa fa-window-close" aria-hidden="true"></i> Hủy</button>
                                             <button ng-show="inv.status==2" class="label label-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Chưa Xử Lý</button>
                                             <button ng-show="inv.status==3" class="label label-warning"><i class="fa fa-play" aria-hidden="true"></i> Đang Xử Lý</button>
                                             <button ng-show="inv.status==4" class="label label-primary"><i class="fa fa-gift" aria-hidden="true"></i> Đã Nhận Hàng</button>
                                             <button ng-show="inv.status==5" class="label label-success"><i class="fa fa-check" aria-hidden="true"></i> Hoàn Thành</button>
                                          </td>
                                       </tr>

                                    </tbody>

                                    <tfoot>
                                       <tr class="cart_buttons">
                                          <td colspan="5">
                                             <a class="btn btn-alt cart_continue" href="{{URL::to('')}}">Tiếp Tục Mua Hàng</a>
                                          </td>
                                       </tr>
                                    </tfoot>
                                 </table>
                           
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- ============================= side bar ==================== -->
            @include('front_end.layouts.menu',[ 
              'listCategoriesuserAuth' => $listCategories,
              'listTags' => $listTags,
              'listQuickSales' => $listQuickSales,
            ])       
         </div>
      </div>
   </div>
@stop