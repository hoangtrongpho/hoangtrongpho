@extends('front_end.layouts.index')

@section('headTitle')
   <title>Trang Chủ - Sinh Viên Kinh Doanh</title>
@stop

@section('headCss')
  <meta property="fb:app_id" content="1863962563863649" />
  <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-toastr/angular-toastr.css')}}" />
@stop

@section('headJs')
 <!-- UI Bootstrap Modal --> 
  <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-bootstrap/ui-bootstrap-tpls-2.0.0.min.js')}}"></script>
  <!-- Angular Toastr -->
  <script type="text/javascript" src="{{asset('public/system/plugins/angular-toastr/angular-toastr.tpls.js')}}"></script>
  <!-- JS -->
  <script type="text/javascript" src="{{asset('public/system/common/system.common.angurlar.js')}}"></script>
  <script type="text/javascript" src="{{asset('public/front_end/common/front_end.categorydetail.angurlar.js')}}"></script>
   <script>
      $(document).ready(function(){ 
            $('.article_content img').addClass('img-responsive');
      });
   </script>
@stop
@section('container')
   <!-- MAIN CONTENT -->
   <div id="main" role="main" ng-app="productDetailApp" ng-controller="productDetailController" ng-init="pageInit()">
      <div class="container">
         <div class="row">       
            <div class="main_content  col-sm-9 col-sm-push-3">          
               <!--=============== NEWS list =================-->
               <div class="collection_listing_main">
                  <div class="row ">
                    <div class="main_content col-sm-12">
                      <div class="">
                        <!-- BREADCRUMBS -->
                        <div class="">
                          <ul class="breadcrumb">
                              <li><a href="#" class="" title="Trang Chủ">Trang Chủ</a></li>
                              <li><a href="#" title="homepage-link">Tin</a></li>
                          </ul>
                        </div>
                        <!-- END BREADCRUMBS-->
                        <div class="row">
                          <div class="main_content  col-sm-12">
                              <div class="blog_article" style="padding: 15px !important;border: 1px solid #dddddd;">

                                 <!-- body -->
                                 <div class="article_body" style="margin-left: 0px; width: 100%;">
                                  @if($newsDetail !== null)
                                    <div class="article_content rte" style="margin-top: 0px !important;">
                                      <a href="{{URL::to('chi-tiet-ban-tin/'.$newsDetail->news_slug)}}"><img src="{{$newsDetail->news_thumbnail}}" alt="{{$newsDetail->news_slug}}" class="img-responsive pull-right" style="width: 100%; height: 320px; object-fit: cover;"></a>
                                    </div>

                                    <div class="article_header">
                                      <h3 class="article_header__title product_name">
                                         <b>{{$newsDetail->news_title}}</b>  
                                      </h3>
                                      
                                      <h3 style="padding-top: 10px" class="article_header__title product_name">
                                         <b>{{$newsDetail->news_description}}</b>
                                      </h3>
                                    </div>
                                    <div class="article_content">
                                      {!! $newsDetail->news_content !!}
                                    </div>
                                    <p>
                                      <span class="article_header__author">Admin</span> | <span class="article_header__author">Ngày Đăng: {{$newsDetail->day_created_date_format}}/{{$newsDetail->month_created_date_format}}/{{$newsDetail->year_created_date_format}}</span>
                                    </p>
                                  @endif
                                    
                                </div>
    
                                 <!-- commnent -->
                                 <div id="comments">
                                    <div class="article_comments__form">
                                        <div class="fb-comments" data-href="{{URL::to('chi-tiet-ban-tin/'.$newsDetail->news_slug)}}" data-width="100%" data-numposts="10"></div>
                                    </div>
                                 </div>
                                <!--end commnent -->
                             </div>
                          </div>
                        </div>
                        <!-- end ROW -->
                      </div>
                    </div>
                  </div>
               </div>
               <!-- end colection -->
            </div>
            <!-- ============================= side bar ==================== -->
            @include('front_end.layouts.menu',[ 
              'listCategoriesuserAuth' => $listCategories,
              'listTags' => $listTags,
              'listQuickSales' => $listQuickSales,
            ])  
         </div>
      </div>
   </div>
@stop