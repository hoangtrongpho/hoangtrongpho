@extends('front_end.layouts.index')

@section('headTitle')
   <title>Trang Chủ - Sinh Viên Kinh Doanh</title>
@stop

@section('headCss')

@stop

@section('headJs')
    <!-- UI Bootstrap Modal --> 
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-bootstrap/ui-bootstrap-tpls-2.0.0.min.js')}}"></script>

    <!-- Angular Toastr -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-toastr/angular-toastr.tpls.js')}}"></script>

  
     <!-- JS -->
  <script type="text/javascript" src="{{asset('public/system/common/system.common.angurlar.js')}}"></script>

  <script type="text/javascript" src="{{asset('public/front_end/common/front_end.customerinfo.angurlar.js')}}"></script>
@stop

@section('container')
  <!-- MAIN CONTENT -->
   <div id="main" role="main" ng-app="loginApp" ng-controller="loginController" ng-init="pageInit()">
      <div class="container">
         <div class="row">       
            <div class="main_content  col-sm-9 col-sm-push-3">
              <div class="loader-wrapper" id="loader">
                <div id="divloader"></div>
              </div>
              <div class="main_content  col-sm-12">
            
                <!-- BREADCRUMBS -->
                <div class="breadcrumb_wrap">
                  <ul class="breadcrumb">
                    <li><a href="/" class="homepage-link" title="Back to the frontpage">Trang Chủ</a></li>
                    <li><span class="page-title">Đăng Ký</span></li>
                  </ul>
                </div>
                @if(Session::has('customerInfo'))
                  <div id="account_register" class="account_section">
                    <h4>Thông Tin Tài Khoản</h4>
                    <div class="form-horizontal">
                      <form id="frmAdd" name="frmAdd" ng-submit="submitForm(frmAdd.$valid)" novalidate>
                        <input type="hidden" name="utf8" value="✓" />
                        <!--  txtRegisterAccount -->
                        <div class="clearfix form-group">
                          <label for="first_name" class="login control-label col-sm-4">Tên Tài Khoản </label>
                          <div class="col-sm-4">
                            <input class="form-control" 
                               type="text" 
                               placeholder="Tên đăng nhập" 
                               id="txtRegisterAccount" 
                               name="txtRegisterAccount"
                               ng-model="request.registerAccount" 
                               value="" 
                               ng-disabled=true
                               ng-minlength="6" 
                               ng-maxlength="20"
                               maxlength="20" required>
                            <span style="color: red" class="help-block" ng-show="frmAdd.txtRegisterAccount.$error.required && frmAdd.txtRegisterAccount.$invalid && !frmAdd.txtRegisterAccount.$pristine">Vui Lòng Nhập.</span>
                              <span style="color: red" class="help-block" ng-show="frmAdd.txtRegisterAccount.$error.minlength">Tên đăng nhập Không Được Ngắn Hơn 6 Ký Tự.</span>
                              <span style="color: red" class="help-block" ng-show="frmAdd.txtRegisterAccount.$error.maxlength">Tên đăng nhập Không Được Dài Hơn 20 Ký Tự.</span>
                          </div>
                        </div>
                        <!--  txtRegisterFullName -->
                        <div class="clearfix form-group">
                          <label for="email" class="login control-label col-sm-4">Họ Tên</label>
                          <div class="col-sm-4">
                            <input class="form-control" 
                               type="text" 
                               placeholder="Họ Tên" 
                               id="txtRegisterFullName" 
                               name="txtRegisterFullName"
                               ng-model="request.registerFullName" 
                               ng-minlength="2" 
                               ng-maxlength="100"
                               maxlength="100" required>
                            <span style="color: red" class="help-block" ng-show="frmAdd.txtRegisterFullName.$error.required && frmAdd.txtRegisterFullName.$invalid && !frmAdd.txtRegisterFullName.$pristine">Vui Lòng Nhập.</span>
                              <span style="color: red" class="help-block" ng-show="frmAdd.txtRegisterFullName.$error.minlength">Họ Tên Không Được Ngắn Hơn 2 Ký Tự.</span>
                              <span style="color: red" class="help-block" ng-show="frmAdd.txtRegisterFullName.$error.maxlength">Họ Tên Không Được Dài Hơn 100 Ký Tự.</span>
                          </div>
                        </div>
                        <!--  txtRegisterEmail -->
                        <div class="clearfix form-group">
                          <label for="last_name" class="login control-label col-sm-4">Email</label>
                          <div class="col-sm-4">
                            <input class="form-control" 
                               type="email" 
                               placeholder="Email" 
                               id="txtRegisterEmail" 
                               name="txtRegisterEmail"
                               ng-model="request.registerEmail" 
                               ng-minlength="6" 
                               ng-maxlength="100"
                               maxlength="100" required>
                            <span style="color: red" class="help-block" ng-show="frmAdd.txtRegisterEmail.$error.required && frmAdd.txtRegisterEmail.$invalid && !frmAdd.txtRegisterEmail.$pristine">Vui Lòng Nhập.</span>
                              <span style="color: red" class="help-block" ng-show="frmAdd.txtRegisterEmail.$error.minlength">Email Không Được Ngắn Hơn 6 Ký Tự.</span>
                              <span style="color: red" class="help-block" ng-show="frmAdd.txtRegisterEmail.$error.maxlength">Email Không Được Dài Hơn 100 Ký Tự.</span>
                          </div>
                        </div>
                        <!--  txtRegisterPhone -->
                        <div class="clearfix form-group">
                          <label for="last_name" class="login control-label col-sm-4">Số điện thoại</label>
                          <div class="col-sm-4">
                            <input class="form-control" 
                               type="text" 
                               placeholder="Số điện thoại" 
                               id="txtRegisterPhone" 
                               name="txtRegisterPhone"
                               ng-model="request.registerPhone" 
                               ng-minlength="10" 
                               ng-maxlength="11"
                               maxlength="11" required>
                             <span style="color: red" class="help-block" ng-show="frmAdd.txtRegisterPhone.$error.required && frmAdd.txtRegisterPhone.$invalid && !frmAdd.txtRegisterPhone.$pristine">Vui Lòng Nhập.</span>
                              <span style="color: red" class="help-block" ng-show="frmAdd.txtRegisterPhone.$error.minlength">Số điện thoại Không Được Ngắn Hơn 10 Ký Tự.</span>
                              <span style="color: red" class="help-block" ng-show="frmAdd.txtRegisterPhone.$error.maxlength">Số điện thoại Không Được Dài Hơn 11 Ký Tự.</span>
                          </div>
                        </div>
                        <!--  txtRegisterAddress -->
                        <div class="clearfix form-group">
                          <label for="email" class="login control-label col-sm-4">Địa Chỉ</label>
                          <div class="col-sm-4">
                            <input class="form-control" 
                               type="text" 
                               placeholder="Địa Chỉ" 
                               id="txtRegisterAddress" 
                               name="txtRegisterAddress"
                               ng-model="request.registerAddress" 
                               ng-minlength="2" 
                               ng-maxlength="200"
                               maxlength="200" required>
                             <span style="color: red" class="help-block" ng-show="frmAdd.txtRegisterAddress.$error.required && frmAdd.txtRegisterAddress.$invalid && !frmAdd.txtRegisterAddress.$pristine">Vui Lòng Nhập.</span>
                              <span style="color: red" class="help-block" ng-show="frmAdd.txtRegisterAddress.$error.minlength">Địa Chỉ Không Được Ngắn Hơn 2 Ký Tự.</span>
                              <span style="color: red" class="help-block" ng-show="frmAdd.txtRegisterAddress.$error.maxlength">Địa Chỉ Không Được Dài Hơn 200 Ký Tự.</span>
                          </div>
                        </div>
                        <!--  txtRegisterIdentify -->
                        <div class="clearfix form-group">
                          <label for="email" class="login control-label col-sm-4">CMND</label>
                          <div class="col-sm-4">
                            <input class="form-control" 
                               type="text" 
                               placeholder="CMND" 
                               id="txtRegisterIdentify" 
                               name="txtRegisterIdentify"
                               ng-model="request.registerIdentify" 
                               ng-minlength="8" 
                               ng-maxlength="11"
                               maxlength="11" required>
                            <span style="color: red" class="help-block" ng-show="frmAdd.txtRegisterIdentify.$error.required && frmAdd.txtRegisterIdentify.$invalid && !frmAdd.txtRegisterIdentify.$pristine">Vui Lòng Nhập.</span>
                              <span style="color: red" class="help-block" ng-show="frmAdd.txtRegisterIdentify.$error.minlength">CMND Không Được Ngắn Hơn 8 Ký Tự.</span>
                              <span style="color: red" class="help-block" ng-show="frmAdd.txtRegisterIdentify.$error.maxlength">CMND Không Được Dài Hơn 11 Ký Tự.</span>
                          </div>
                        </div>
                        <!--  txtRegisterStudentCard -->
                        <div class="clearfix form-group">
                          <label for="email" class="login control-label col-sm-4">Mã Sinh Viên</label>
                          <div class="col-sm-4">
                            <input class="form-control" 
                               type="text" 
                               placeholder="Mã Sinh Viên" 
                               id="txtRegisterStudentCard" 
                               name="txtRegisterStudentCard"
                               ng-model="request.registerStudentCard" 
                               ng-minlength="2" 
                               ng-maxlength="20"
                               maxlength="20" required>
                            <span style="color: red" class="help-block" ng-show="frmAdd.txtRegisterStudentCard.$error.required && frmAdd.txtRegisterStudentCard.$invalid && !frmAdd.txtRegisterStudentCard.$pristine">Vui Lòng Nhập.</span>
                              <span style="color: red" class="help-block" ng-show="frmAdd.txtRegisterStudentCard.$error.minlength">Mã Sinh Viên Không Được Ngắn Hơn 2 Ký Tự.</span>
                              <span style="color: red" class="help-block" ng-show="frmAdd.txtRegisterStudentCard.$error.maxlength">Mã Sinh Viên Không Được Dài Hơn 20 Ký Tự.</span>
                          </div>
                        </div>
                        <!--  txtRegisterStudentUniversity -->
                        <div class="clearfix form-group">
                          <label for="email" class="login control-label col-sm-4">Trường Học</label>
                          <div class="col-sm-4">
                            <input class="form-control" 
                               type="text" 
                               placeholder="Trường Học" 
                               id="txtRegisterStudentUniversity" 
                               name="txtRegisterStudentUniversity"
                               ng-model="request.registerStudentUniversity" 
                               ng-minlength="2" 
                               ng-maxlength="50"
                               maxlength="50" required>
                               <span style="color: red" class="help-block" ng-show="frmAdd.txtRegisterStudentUniversity.$error.required && frmAdd.txtRegisterStudentUniversity.$invalid && !frmAdd.txtRegisterStudentUniversity.$pristine">Vui Lòng Nhập.</span>
                              <span style="color: red" class="help-block" ng-show="frmAdd.txtRegisterStudentUniversity.$error.minlength">Trường Không Được Ngắn Hơn 2 Ký Tự.</span>
                              <span style="color: red" class="help-block" ng-show="frmAdd.txtRegisterStudentUniversity.$error.maxlength">Trường Không Được Dài Hơn 50 Ký Tự.</span>
                          </div>
                        </div>
                       
                        <div class="form-group">
                          <div class="col-sm-offset-4 col-sm-4">
                            <input 
                              ng-disabled="frmAdd.$invalid" 
                              ng-click="UpdateUserDetail()" 
                              type="button" 
                              value="Cập Nhật" 
                              class="btn"/>
                              <a 
                              style="background-color: white!important; color: black!important" 
                              href="{{URL::to('')}}" 
                              class="btn btn-default">Trở Về</a>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                @endif 
                  
               
              </div>
            </div>
            <!-- ============================= side bar ==================== -->
            @include('front_end.layouts.menu',[ 
              'listCategoriesuserAuth' => $listCategories,
              'listTags' => $listTags,
              'listQuickSales' => $listQuickSales,
            ])           
         </div>
      </div>
   </div>
@stop
