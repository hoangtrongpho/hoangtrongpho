@extends('front_end.layouts.index')

@section('headTitle')
   <title>Trang Chủ - Sinh Viên Kinh Doanh</title>
@stop

@section('headCss')


  <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-toastr/angular-toastr.css')}}" />

@stop

@section('headJs')
 

    <!-- Angular Multi Select -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-multi-select/isteven-multi-select.js')}}"></script>

    <!-- UI Bootstrap Modal --> 
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-bootstrap/ui-bootstrap-tpls-2.0.0.min.js')}}"></script>

    <!-- Angular Toastr -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-toastr/angular-toastr.tpls.js')}}"></script>
  <!-- JS -->
  <script type="text/javascript" src="{{asset('public/system/common/system.common.angurlar.js')}}"></script>
  <script type="text/javascript" src="{{asset('public/front_end/common/login.angurlar.js')}}"></script>
@stop

@section('container')

  <!-- MAIN CONTENT -->
  <div id="main" role="main" ng-app="loginApp" ng-controller="loginController" ng-init="pageInit('')">
    <!--  divloader -->
    <div class="loader-wrapper" id="loader">
      <div id="divloader"></div>
    </div>
    <div class="container">
      
       <div class="row">       
          <div class="main_content  col-sm-9 col-sm-push-3">
            <div class="main_content  col-sm-12">
          
              <!-- BREADCRUMBS -->
              <div class="breadcrumb_wrap">
                <ul class="breadcrumb">
                  <li><a href="/" class="homepage-link" title="Back to the frontpage">Trang Chủ</a></li>
                  <li><span class="page-title">Tài Khoản</span></li>
                </ul>
              </div>
              <h2 class="page_heading">Đăng Nhập</h2>
              <!-- info tab -->
                <div class="alert alert-info">
                  click vào đây để tìm hiểu về: 
                  <strong>
                  <a  href="{{asset('chi-tiet-ban-tin/cach-thuc-hoat-dong')}}">
                    Phương Thức Hoặt Động Của Hệ Thống
                  </a>
                  </strong>
                </div>
              <!-- end infotab -->
              <div id="account_login">
                <div class="account_wrapper">
                  <div class="account_left">
                    <div class="account_section">
                      <h4>Các Bước Hoàn Tất Thủ Tục Đặt Hàng</h4>
                      <p class="note">Đăng Ký Nhanh Chóng Và Dễ Dàng</p>
                      <ul>
                        <li>
                           <a href="#">
                              <i class="fa fa-user"></i> <span>Đăng Ký Tài Khoản.</span> 
                           </a>
                        </li>
                        <li>
                           <a href="#">
                              <i class="fa fa-cart-plus"></i> <span>Đặt Hàng.</span> 
                           </a>
                        </li>
                        <li>
                           <a href="#">
                              <i class="fa fa-check"></i> <span>Xác Nhận Đặt Hàng.</span> 
                           </a>
                        </li> 
                        <li>
                           <a href="#">
                              <i class="fa fa-cubes"></i> <span>Nhận Hàng.</span> 
                           </a>
                        </li> 
                        <li>
                           <a href="#">
                              <i class="fa fa-diamond"></i> <span>Bán Hàng.</span> 
                           </a>
                        </li>   
                        <li>
                           <a href="#">
                              <i class="fa fa-money"></i> <span>Thanh Toán.</span> 
                           </a>
                        </li>  
                      </ul>

                      <a id="account_register__link" class="btn" href="{{URL::to('/dang-ky')}}">Tạo Tài Khoản</a>
                    </div>
                  </div>
                  <div class="account_right">
                    <div class="account_section">
                      <form id="frmLogin" name="frmLogin" ng-submit="submitForm(frmLogin.$valid)" novalidate>
                        <input type="hidden" name="utf8" value="✓" />
                        <h4>Bạn Đã Đăng Ký Chưa?</h4>
                        <div class="form-group">
                          <label for="customer_email" class="control-label">Tên Tài Khoản</label>
                          <input class="form-control" 
                              type="text" 
                              placeholder="Nhập Tài Khoản" 
                              id="txtUserName" 
                              name="txtUserName"
                              ng-model="loginModel.userName" 
                              ng-minlength="6" 
                              ng-maxlength="20"
                              maxlength="20" 
                              required />
                              <span style="color: red" class="help-block" ng-show="frmLogin.txtUserName.$error.required && frmLogin.txtUserName.$invalid && !frmLogin.txtUserName.$pristine">Vui Lòng Nhập.</span>
                              <span style="color: red" class="help-block" ng-show="frmLogin.txtUserName.$error.minlength">Tài Khoản Không Được Ngắn Hơn 6 Ký Tự.</span>
                              <span  style="color: red" class="help-block" ng-show="frmLogin.txtUserName.$error.maxlength">Tài Khoản Không Được Dài Hơn 20 Ký Tự.</span>
                        </div>
                        <div class="form-group">
                          <label for="customer_password" class="control-label">Mật Khẩu</label>
                          <input class="form-control" 
                            type="Password" 
                            placeholder="Nhập mật khẩu" 
                            id="txtPass" 
                            name="txtPass"
                            ng-model="loginModel.passWord" 
                            ng-minlength="6" 
                            ng-maxlength="20"
                            maxlength="20" required/>
                            <span style="color: red" class="help-block" ng-show="frmLogin.txtPass.$error.required && frmLogin.txtPass.$invalid && !frmLogin.txtPass.$pristine">Vui Lòng Nhập.</span>
                            <span style="color: red" class="help-block" ng-show="frmLogin.txtPass.$error.minlength">Mật Khẩu Không Được Ngắn Hơn 6 Ký Tự.</span>
                            <span  style="color: red" class="help-block" ng-show="frmLogin.txtPass.$error.maxlength">Mật Khẩu Không Được Dài Hơn 20 Ký Tự.</span>
                        </div>            
                        <div class="form-group">
                          <input type="submit" id="btnLogin" ng-disabled="frmLogin.$invalid" value="Đăng nhập" class="btn" />
                        </div>
                      </form>
                    </div>
                  </div >
                </div>
              </div>
              <!-- end form login -->
            </div>
          </div>
          <!-- ============================= side bar ==================== -->
          <div class="sidebar col-sm-3 sidebar_left col-sm-pull-9">
             <!-- ================================side menu==================== -->
             <div class="sidebar_widget sidebar_widget__collections">
                <h3 class="widget_header">Danh Mục</h3>
                <div class="widget_content">
                   <ul class="list_links mainmenu0"> 
                      @if(count($listCategories)<=0)
                         <li></li>
                      @else
                         @foreach($listCategories as $c)
                            <li>
                               <?php
                                  $c1 = array_get($c, 'nodes');
                                  $c_count=count($c1);
                                  $p1 = array_get($c, 'product_list');
                               ?>
                            
                               <a href="{{URL::to('chi-tiet-danh-muc/'.array_get($c, 'cate_slug'))}}" title="{{array_get($c,'cate_name')}}">
                                  <b >{{array_get($c, 'cate_name')}}</b>
                                  @if($c_count>0)
                                     <span class="badge">{{$c_count}}</span>
                                  @endif
                                  
                               </a>

                               <ul class="submenu1">
                                  @foreach($c1 as $c1)
                                  <li>
                                     <?php
                                        $c2 = array_get($c1, 'nodes');
                                        $c1_count=count($c2);
                                     ?>
                                     <a href="{{URL::to('chi-tiet-danh-muc/'.array_get($c1, 'cate_slug'))}}" title="{{array_get($c1,'cate_name')}}" style="color: #01489A!important">
                                        &nbsp;&nbsp;<i class="fa fa-th-large" aria-hidden="true"></i>
                                        {{array_get($c1, 'cate_name')}}
                                        @if($c1_count>0)
                                           <span class="badge">{{$c1_count}}</span>
                                        @endif
                                        
                                     </a>
                                     
                                     <ul class="submenu2">
                                        @foreach($c2 as $c2)
                                        <li>
                                           <a href="{{URL::to('chi-tiet-danh-muc/'.array_get($c2, 'cate_slug'))}}" title="{{array_get($c2,'cate_name')}}" style="color: #01489A!important">
                                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-th-large" aria-hidden="true"></i>
                                              {{array_get($c2, 'cate_name')}}
                                           </a>
                                        </li>
                                        @endforeach
                                     </ul>
                                  </li>
                                  @endforeach
                               </ul>
                            </li>
                         @endforeach
                      @endif
                     
                     
                   </ul>
                </div>
             </div>
             <!-- ============================product tag==================== -->
             <div class="sidebar_widget sidebar_widget__types">
                <h3 class="widget_header" style="color: #01489A">Loại Sản Phẩm (Tags)</h3>
                <div class="widget_content">
                   <ul class="list_links">
                      @if(count($listTags)<=0)
                         <li></li>
                      @else
                         @foreach($listTags as $t)
                            <li class="Audio/Video"><a href="{{URL::to('chi-tiet-the/'.$t->tag_slug)}}" title="Audio/Video">{{$t->tag_name}}</a></li>
                         @endforeach
                      @endif
                   </ul>
                </div>
             </div>
             <!-- ============================San Pham Ban Chay==================== -->
             <div class="sidebar_widget sidebar_widget__products">
                <h3 class="widget_header" style="color: #01489A">Sản Phẩm Bán Chạy</h3>
                <div class="widget_content">
                   <ul class="list_products">   
                      @if(count($listQuickSales)<=0)
                         <li></li>
                      @else
                         @foreach($listQuickSales as $s)
                            <li class="product">
                               <div class="product_img">  
                                  <a href="{{URL::to('chi-tiet-san-pham/'.$s->product_slug)}}">
                                     <img  src="{{$s->product_thumbnail}}" alt="{{$s->product_slug}}" title="{{$s->product_name}}"> 
                                  </a>
                               </div>
                               <div class="product_info">
                                  <div class="product_price">
                                     {{$s->product_name}}             
                                  </div>
                                  <div class="product_price">
                                     <span class="money">
                                     {{ number_format(($s->product_retail_prices), 0) }} VND
                                     </span>
                                  </div>
                                  <div class="product_price">
                                     <span class="money compare-at-price">
                                     {{ number_format(($s->product_imported_prices), 0) }} VND
                                     </span>   
                                  </div>
                                  <div class="product_name">
                                     <a href="{{URL::to('chi-tiet-san-pham/'.$s->product_slug)}}">{!! $s->product_description !!}</a>
                                  </div>
                               </div>
                            </li>  
                         @endforeach
                      @endif     
                      
                   </ul>
                </div>
             </div>
          </div>          
       </div>
    </div>
  </div>
@stop
