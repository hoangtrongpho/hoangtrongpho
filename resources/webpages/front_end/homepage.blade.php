@extends('front_end.layouts.index')

@section('headTitle')
   <title>Trang Chủ - Sinh Viên Kinh Doanh</title>
@stop

@section('headCss')
  <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-toastr/angular-toastr.css')}}" />
@stop

@section('headJs')
  <!-- UI Bootstrap Modal --> 
  <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-bootstrap/ui-bootstrap-tpls-2.0.0.min.js')}}"></script>
  <!-- Angular Toastr -->
  <script type="text/javascript" src="{{asset('public/system/plugins/angular-toastr/angular-toastr.tpls.js')}}"></script>
  <!-- JS -->
  <script type="text/javascript" src="{{asset('public/system/common/system.common.angurlar.js')}}"></script>
  <script type="text/javascript" src="{{asset('public/front_end/common/front_end.homepage.angurlar.js')}}"></script>
@stop

@section('container')

   <!-- MAIN CONTENT -->
   <div id="main" role="main" ng-app="productDetailApp" ng-controller="productDetailController" ng-init="pageInit()">
      <div class="container">
            <div class="main_content  col-sm-9 col-sm-push-3">          
               <!--==================== NIVOSLIDER TOP==============-->                 
               @include('front_end.layouts.topslide',[
                  'listSlide' => $listSlide,
                ]) 
               <!-- =================== Sản Phẩm Mới ================ -->
               <div class="featured_products">
                  <h3 class="page_heading slider_wrap" style="margin: 10px 7px;height: 60px;padding: 0 20px;background: #01489A;font-size: 1.5em;line-height: 60px;color: #ffffff;-webkit-border-radius: 5px 5px 0 0;border-radius: 5px 5px 0 0;">Sản Phẩm Mới</h3>

                  @if(count($listLimitNew)>0)
                    <div class="product_listing_main homepage_carousel row homepage_carousel__common">
                      @foreach($listLimitNew as $c1)
                          <div class="product col-sm-3 product_homepage item_1">
                            <div class="product_wrapper">
                                <div class="product_img">
                                  <a class="img_change" href="{{URL::to('chi-tiet-san-pham/'.$c1->product_slug)}}">
                                      <img src="{{$c1->product_thumbnail}}" alt="{{$c1->product_name}}" title="{{$c1->product_name}}" alt="{{$c1->product_slug}}"
                                        style="width:100%; height:270px; object-fit:cover;"> 
                                      <span class="product_badge new">Mới</span>
                                  </a>
                                </div>
                                <div class="product_info">
                                  <div class="product_price">
                                      <span class="money">
                                      {{ number_format(($c1->product_retail_prices), 0) }} VNĐ
                                      </span><br>
                                      <span style="display: none;" class="money compare-at-price">
                                      {{ number_format(($c1->product_imported_prices), 0) }} VNĐ
                                      </span>             
                                  </div>
                                  <div class="product_name" style="height: 38px;">
                                      <a href="{{URL::to('chi-tiet-san-pham/'.$c1->product_slug)}}">{{mb_strimwidth($c1->product_name,0,50,"...")}}</a>
                                  </div>
                                  <div class="product_desc product_desc_short" style="height: 38px;">{!! mb_strimwidth($c1->product_description,0,70,"...") !!}</div>
                                  <div class="product_desc product_desc_long"></div>
                                  <div class="product_links">
                                      <a ng-click="addCart('{{$c1->product_id}}')" class="btn" title="Mua Ngay"><i class="fa fa-fw fa-cart-plus"></i>&nbsp;&nbsp;Mua Ngay</a>
                                      <a class="btn pull-right" href="{{URL::to('chi-tiet-san-pham/'.$c1->product_slug)}}" title="Xem Chi Tiết"><i class="fa fa-fw fa-eye"></i>&nbsp;&nbsp;Chi Tiết</a>
                                  </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                          </div> 
                      @endforeach
                    </div>
                  @endif
               </div>
               <!--=============== product list =================-->
               <hr>
               <div class="featured_products">
                  @if(count($listCategories)<=0)
                     <div></div>
                  @else
                     @foreach($listCategories as $c)
                        <?php
                           $c1 = array_get($c, 'product_list');
                           $c_count=count($c1);
                        ?>
                        @if($c_count > 0)
                          <h2 class="page_heading slider_wrap" 
                              style="height: 60px;padding: 0 20px;background: #01489A;font-size: 1.5em;line-height: 60px;color: #ffffff;-webkit-border-radius: 5px 5px 0 0;border-radius: 5px 5px 0 0;">
                              {{array_get($c, 'cate_name')}}</h2>
                          <div class="product_listing_main homepage_carousel row homepage_carousel__common">
                            @foreach($c1 as $c1)
                                <div class="product col-sm-3 product_homepage item_1">
                                  <div class="product_wrapper">
                                      <div class="product_img">
                                        <a class="img_change" href="{{URL::to('chi-tiet-san-pham/'.array_get($c1, 'product_slug'))}}">
                                            <img src="{{array_get($c1, 'product_thumbnail')}}" alt="{{array_get($c1, 'product_name')}}" title="{{array_get($c1, 'product_name')}}" alt="{{array_get($c1, 'product_slug')}}"
                                              style="width:100%; height:270px; object-fit:cover;"> 
                                            <span class="product_badge new">Mới</span>
                                        </a>
                                      </div>
                                      <div class="product_info">
                                        <div class="product_price">
                                            <span class="money">
                                            {{ number_format((array_get($c1, 'product_retail_prices')), 0) }} VNĐ
                                            </span><br>
                                            <span style="display: none;" class="money compare-at-price">
                                            {{ number_format((array_get($c1, 'product_imported_prices')), 0) }} VNĐ
                                            </span>             
                                        </div>
                                        <div class="product_name" style="height: 38px;">
                                            <a href="{{URL::to('chi-tiet-san-pham/'.array_get($c1, 'product_slug'))}}">{{mb_strimwidth(array_get($c1, 'product_name'),0,50,"...")}}</a>
                                        </div>
                                        <div class="product_desc product_desc_short" style="height: 38px;">{!!mb_strimwidth(array_get($c1, 'product_description'),0,70,"...")!!}</div>
                                        <div class="product_desc product_desc_long"></div>
                                        <div class="product_links">
                                            <a ng-click="addCart('{{array_get($c1, 'product_id')}}')" class="btn" title="Mua Ngay"><i class="fa fa-fw fa-cart-plus"></i>&nbsp;&nbsp;Mua Ngay</a>
                                            <a class="btn pull-right" href="{{URL::to('chi-tiet-san-pham/'.array_get($c1, 'product_slug'))}}" title="Xem Chi Tiết"><i class="fa fa-fw fa-eye"></i>&nbsp;&nbsp;Chi Tiết</a>
                                        </div>
                                      </div>
                                      <div class="clearfix"></div>
                                  </div>
                                </div> 
                            @endforeach
                          </div>
                        @endif
                     @endforeach
                  @endif
                  
               </div>
            </div>
            @include('front_end.layouts.menu',[ 
              'listCategoriesuserAuth' => $listCategories,
              'listTags' => $listTags,
              'listQuickSales' => $listQuickSales,
            ])

      </div>
   </div>
@stop
