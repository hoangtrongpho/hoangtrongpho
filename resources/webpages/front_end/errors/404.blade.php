@extends('front_end.layouts.index')

@section('headTitle')
   <title>Lỗi 404! Không Tìm Thấy Trang Yêu Cầu- {{ Lang::get('messages.infoCompany.company_name') }}</title>
@stop

@section('headCss')
<style type="text/css"></style>
@stop

@section('headJs')

@stop

@section('container')
<div class="breadcrumbs">
  <div class="container">
    <div class="row">
      <ul>
        <li class="home"> <a href="{{URL::to('/trang-chu')}}" title="Trang chủ">Trang chủ</a></li>
      </ul>
    </div>
  </div>
</div>
<section class="latest-blog container">
      <div class="col-xs-12">
        <h3><i class="fa fa-fw fa-warning text-red"></i> Lỗi 404! Không Tìm Thấy Trang Yêu Cầu.</h3>
        <br>
        <p>
        Không Tìm Thấy Trang Yêu Cầu
        <br>Bây giờ, hãy nhấn vào <a href="{{URL::to('/trang-chu')}}">đây</a> để trở về.
        </p>
        <strong>Hoặc bấm vào <a href="mailto:{{env('MAIL_FROM_ADDRESS')}}" title="Quản Trị Viên">đây</a> để liên hệ với quản trị viên </strong>
      </div>
</section>
<!-- End Latest Blog -->

@stop