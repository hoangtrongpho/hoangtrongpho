<!DOCTYPE html>
<html lang="vi">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Bảo trì hệ thống - Hạt Giống F1508</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="{{asset('public/system/css/bootstrap/bootstrap.min.css')}}">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{asset('public/system/css/AdminLTE/AdminLTE.min.css')}}">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="hold-transition lockscreen" style="background-color: #ee0000;">
        <div class="error-page">
            <h2 class="headline text-red">Bảo trì hệ thống</h2>
            <div class="error-content">
                <h2><i class="fa fa-fw fa-warning text-red"></i> Hệ .</h2>
                <br>
                <p>
                Không tìm thấy trang bạn cần trong hệ thống quản trị website.
                <br>Bây giờ, hãy nhấn vào <a href="{{URL::to('/tin-tuc')}}">đây</a> để trở về.
                </p>

                <strong>Hoặc bấm vào <a href="mailto:{{env('MAIL_FROM_ADDRESS')}}" title="Quản Trị Viên">đây</a> để liên hệ với quản trị viên </strong>

                <!-- <form class="search-form">
                    <div class="input-group">
                        <input type="text" name="search" class="form-control" placeholder="Search">

                        <div class="input-group-btn">
                            <button type="submit" name="submit" class="btn btn-warning btn-flat"><i class="fa fa-fw fa-search"></i>
                            </button>
                        </div>
                    </div>
                </form> -->
            </div>
            <!-- /.error-content -->
        </div>
        <!-- /.error-page -->
    </body>
</html>
