@extends('front_end.layouts.index')

@section('headTitle')
   <title>{{$productDetail->product_name}} - Sinh Viên Kinh Doanh</title>
@stop

@section('headCss')

 <link rel="stylesheet" type="text/css" href="{{asset('public/front_end/css/product-detail.css')}}"/>
 <link rel="stylesheet" type="text/css" href="{{asset('public/front_end/css/product-detail-style.css')}}"/>
 <link rel="stylesheet" type="text/css" href="{{asset('public/front_end/css/flexslider.css')}}"/>

 <style type="text/css">
    .main_content{
      padding-right: 8px!important;
    }
    .product_specifications img{
      width: auto;
    }
 </style>
@stop

@section('headJs')
   <!-- UI Bootstrap Modal --> 
   <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-bootstrap/ui-bootstrap-tpls-2.0.0.min.js')}}"></script>
   <!-- Angular Toastr -->
   <script type="text/javascript" src="{{asset('public/system/plugins/angular-toastr/angular-toastr.tpls.js')}}"></script>
   <!-- JS -->
   <script type="text/javascript" src="{{asset('public/system/common/system.common.angurlar.js')}}"></script>
   <script type="text/javascript" src="{{asset('public/front_end/common/front_end.productdetail.angurlar.js')}}"></script>


   <script type="text/javascript" src="{{asset('public/front_end/js/jquery.flexslider.js')}}"></script>
 <!--   <script type="text/javascript" src="{{asset('public/system/plugins/elevatezoom-master/jquery.elevatezoom.js')}}"></script> -->

   <script>
      $(window).load(function() {
            $('.flexslider').flexslider({
               animation: "slide",
               controlNav: "thumbnails"
            });
      });
      $(document).ready(function(){ 
            $('.product_specifications img').addClass('img-responsive');
      });
       
     
   </script>
   
@stop
@section('container')

   <!-- MAIN CONTENT -->
   <div ng-app="productDetailApp" ng-controller="productDetailController" ng-init="pageInit()" id="main" role="main" >
      <div class="container">
         <!-- loader -->
         <div class="loader-wrapper" id="loader">
            <div id="divloader"></div>
         </div>
         <div class="row">       
            <div class="main_content  col-sm-9 col-sm-push-3">          
               <!-- =================BREADCRUMBS =========-->
               <div class="collection_listing_main">
                  <div class="row">
                     <div class="main_content  col-sm-12">
                      
                        <div class="">
                           <ul class="breadcrumb">
                              <li><a href="/" class="homepage-link" title="Back to the frontpage">Trang Chủ</a></li>
                              <li><a href="/collections/cameras-camcorders" title="">Danh Muc</a></li>
                              <li><span class="page-title">{{$productDetail->product_name}}</span></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
         
               <!-- ===================Product Detail =======-->
               <div class="collection_listing_main">
                  <div class="card">
                     <div class="container-fliud">
                        <div class="wrapper row">
                           @if($productDetail != null)
                              <?php
                                 $imgs="";
                                 if($productDetail->product_images_list !==null && $productDetail->product_images_list!=="")
                                 {
                                    $imgs = explode("@", $productDetail->product_images_list);
                                 }  
                              ?>
                              <!--=================== new ===============-->
                              <div class="col-md-6 single-top-left col-sm-push-1">
                                 <div class="flexslider">
                                    <ul class="slides">
                                       <li data-thumb="{{$productDetail->product_thumbnail}}">
                                          <div class="thumb-image detail_images"> 
                                             <img 
                                             id="pro_{{$productDetail->product_id}}"
                                             src="{{$productDetail->product_thumbnail}}"
                                             class="img-responsive" 
                                             alt="Groovy Apparel">
                                          </div>
                                       </li>
                                       @if($imgs!=="" )
                                          @foreach($imgs as $key=>$img)
                                             @if($img!=="")
                                                <li data-thumb="{{$img}}">
                                                    <div class="thumb-image"> 
                                                      <img 
                                                      src="{{$img}}"
                                                      class="img-responsive" 
                                                      alt="{{$img}}">
                                                    </div>
                                                </li>
                                             @endif
                                          @endforeach
                                       @endif
                                    </ul>
                                 </div>
                              </div>
                              <!-- add cart group -->
                              <div class="details col-md-5 col-sm-push-1">
                                 <h3 class="product-title">{{$productDetail->product_name}}</h3>
                                 <div class="rating" >
                                    <div class="stars">
                                       <span class="fa fa-star checked"></span>
                                       <span class="fa fa-star checked"></span>
                                       <span class="fa fa-star checked"></span>
                                       <span class="fa fa-star checked"></span>
                                       <span class="fa fa-star"></span>
                                    </div>
                                    <span class="review-no" style="display: none;">41 reviews</span>
                                 </div>
                                 <p class="product-description">{!! $productDetail->product_description !!}</p>
                                 <h4 class="price">Giá: 

                                       {{ number_format(($productDetail->product_retail_prices), 0) }} VND
                                 </h4>

                                 <div class="price">
                                    <span style="display: none;" class="money compare-at-price">
                                    {{ number_format(($productDetail->product_imported_prices), 0) }} VND
                                    </span>   
                                 </div>
                                 
                                 <div class="product_details">
                                    <p class="product_details__item product_weight">
                                       <h5 class="sizes">Loại: {{$productDetail->cate_name}}</h5>
                                    </p>
                                    @if($productDetail->unit_id!=0)
                                       <p style="display: none" class="product_details__item product_weight">
                                          <label for="">Đơn Vị Tính: </label> 
                                          {{$productDetail->unit_name}}
                                       </p>
                                    @endif
                                    @if($productDetail->origin_id!=0)
                                       <p style="display: none" class="product_details__item product_weight">
                                          <label for="">Xuất Xứ: </label> 
                                          {{$productDetail->origin_name}}
                                       </p>
                                    @endif
                                    @if($productDetail->manufacturer_id!=0)
                                       <p style="display: none" class="product_details__item product_weight">
                                          <label for="">Nhà Sản Xuất:</label> 
                                          {{$productDetail->manufacturer_name}}
                                       </p>
                                    @endif
                                    @if($productDetail->color_id!=0)
                                       <p style="display: none" class="product_details__item product_weight">
                                          <label for="">Màu: </label> 
                                          {{$productDetail->color_name}}
                                       </p>
                                    @endif
                                 </div>
                                 <div class="action">
                                    <div id="purchase">
                                       <h4 class="price">Chọn Số Lượng:</h4>
                                       <div class="col-md-4">
                                          <input  
                                               type="number"
                                               class="form-control" 
                                               id="txtQuantity" 
                                               name="txtQuantity" 
                                               ng-model="model.request.txtQuantity"
                                               autocomplete="off" autocorrect="off" 
                                               autocapitalize="off" spellcheck="false"
                                               min="1">
                                       </div>
                                       <div class="col-md-6">
                                          <a href="#" ng-click="addCart({{$productDetail->product_id}})" class="btn btn-cart" >
                                            <i class="fa fa-shopping-bag fa-lg" aria-hidden="true"></i> &nbsp; Thêm Giỏ Hàng
                                          </a>
                                       </div>
                                    </div>
                                 </div>
                              </div> 
                              
                           @endif
                        </div>
                        <!-- Chi Tiet Ky Thuat -->
                        <hr style="border-top: 1px solid #c9c9c9;">
                        <div class="wrapper row">
                           <div class="details product_specifications col-md-12">
                              <h3 class="product-title">Chi Tiết Kỹ Thuật</h3>
                              {!!$productDetail->product_specifications!!}
                           </div>
                        </div>
                        <!-- commnent -->
                        <hr style="border-top: 1px solid #c9c9c9;">
                        <div id="comments">
                           <div class="article_comments__form">
                               <div class="fb-comments" data-href="{{URL::to('chi-tiet-ban-tin/'.$productDetail->product_slug)}}" data-width="100%" data-numposts="10"></div>
                           </div>
                        </div>
                       <!--end commnent -->
                     </div>
                  </div>
               </div>
               <!-- ============pro new ============-->
               <div class="featured_products">
                  @if(count($listNew)<=0)
                     <div></div>
                  @else
                     <h3 class="page_heading slider_wrap" style="height: 60px;padding: 0 20px;background: #01489A;font-size: 19px;line-height: 60px;color: #ffffff;-webkit-border-radius: 5px 5px 0 0;border-radius: 5px 5px 0 0;">Sản Phẩm Mới</h3>

                     <div class="product_listing_main homepage_carousel row homepage_carousel__common">
                        @foreach($listNew as $s)
                        <div class="product col-sm-3 product_homepage item_1">
                           <div class="product_wrapper">
                              <div class="product_img">
                                 <a class="img_change" href="{{URL::to('chi-tiet-san-pham/'.$s->product_slug)}}">
                                    <img style="width:100%; height:270px; object-fit:cover;"  src="{{$s->product_thumbnail}}" alt="Img" title="Img"> 
                                    <img style="width:100%; height:270px; object-fit:cover;" class="img__2" src="{{$s->product_thumbnail}}" alt="Img" title="Img">
                                    <span class="product_badge new">Mới</span>
                                 </a>
                              </div>
                              <div class="product_info">
                                 <div class="product_price">
                                    <span class="money">
                                     {{ number_format( $s->product_retail_prices, 0) }} VND
                                    </span><br>
                                    <span style="display: none;" class="money compare-at-price">
                                     {{ number_format( $s->product_imported_prices, 0) }} VND
                                    </span>             
                                 </div>
                                 <div class="product_name" style="height: 38px;">
                                    <a href="{{URL::to('chi-tiet-san-pham/'.$s->product_slug)}}">{{mb_strimwidth($s->product_name,0,50,"...")}}</a>
                                 </div>
                                 <div class="product_desc product_desc_short" style="height: 38px;">{!!mb_strimwidth($s->product_description,0,70,"...")!!}</div>
                                 <div class="product_desc product_desc_long"></div>
                                 <div class="product_links">
                                    <a ng-click="addCart('{{$s->product_id}}')" class="btn" href="#" title="Cart"><i class="fa fa-cart-plus"></i></a>
                                    <a class="btn " href="{{URL::to('chi-tiet-san-pham/'.$s->product_slug)}}" title="Quick view"><i class="fa fa-eye"></i></a>
                                 </div>
                              </div>
                              <div class="clearfix"></div>
                           </div>
                        </div> 
                        @endforeach
                     </div>
                  @endif
               </div>
               <!-- =================pro Sale OFF ================-->
               <div class="featured_products">
                  @if(count($listSaleOff)<=0)
                     <div></div>
                  @else
                     <h3 class="page_heading slider_wrap" style="height: 60px;padding: 0 20px;background: #01489A;font-size: 19px;line-height: 60px;color: #ffffff;-webkit-border-radius: 5px 5px 0 0;border-radius: 5px 5px 0 0;">Sản Giảm Giá</h3>

                     <div class="product_listing_main homepage_carousel row homepage_carousel__common">
                        @foreach($listSaleOff as $s)
                        <div class="product col-sm-3 product_homepage item_1">
                           <div class="product_wrapper">
                              <div class="product_img">
                                 <a class="img_change" href="{{URL::to('chi-tiet-san-pham/'.$s->product_slug)}}">
                                    <img style="width:100%; height:270px; object-fit:cover;" src="{{$s->product_thumbnail}}" alt="Img" title="Img"> 
                                    <img style="width:100%; height:270px; object-fit:cover;" class="img__2" src="{{$s->product_thumbnail}}" alt="Img" title="Img">
                                    <span class="product_badge new">Giảm Giá</span>
                                 </a>
                              </div>
                              <div class="product_info">
                                 <div class="product_price">
                                    <span class="money">
                                     {{ number_format( $s->product_retail_prices, 0) }} VND
                                    </span><br>
                                    <span style="display: none;" class="money compare-at-price">
                                     {{ number_format( $s->product_imported_prices, 0) }} VND
                                    </span>             
                                 </div>
                                 <div class="product_name" style="height: 38px;">
                                    <a href="{{URL::to('chi-tiet-san-pham/'.$s->product_slug)}}">{{mb_strimwidth($s->product_name,0,50,"...")}}</a>
                                 </div>
                                 <div class="product_desc product_desc_short" style="height: 38px;">{!!mb_strimwidth($s->product_description,0,70,"...")!!}</div>
                                 <div class="product_desc product_desc_long"></div>
                                 <div class="product_links">
                                    <a ng-click="addCart('{{$s->product_id}}')" class="btn" href="#" title="Cart"><i class="fa fa-cart-plus"></i></a>
                                    <a class="btn " href="{{URL::to('chi-tiet-san-pham/'.$s->product_slug)}}" title="Quick view"><i class="fa fa-eye"></i></a>
                                 </div>
                              </div>
                              <div class="clearfix"></div>
                           </div>
                        </div> 
                        @endforeach
                     </div>
                  @endif
               </div>
            </div>

            <!-- ============================= side bar ==================== -->
            @include('front_end.layouts.menu',[ 
              'listCategoriesuserAuth' => $listCategories,
              'listTags' => $listTags,
              'listQuickSales' => $listQuickSales,
            ])        
         </div>
      </div>
   </div>

@stop