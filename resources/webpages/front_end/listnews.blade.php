@extends('front_end.layouts.index_fullwidth')

@section('headTitle')
   <title>Trang Chủ - Sinh Viên Kinh Doanh</title>
@stop

@section('headCss')
  <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-toastr/angular-toastr.css')}}" />
@stop

@section('headJs')
 <!-- UI Bootstrap Modal --> 
  <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-bootstrap/ui-bootstrap-tpls-2.0.0.min.js')}}"></script>
  <!-- Angular Toastr -->
  <script type="text/javascript" src="{{asset('public/system/plugins/angular-toastr/angular-toastr.tpls.js')}}"></script>
  <!-- JS -->
  <script type="text/javascript" src="{{asset('public/system/common/system.common.angurlar.js')}}"></script>
  <script type="text/javascript" src="{{asset('public/front_end/common/front_end.categorydetail.angurlar.js')}}"></script>
@stop
@section('container')
   <!-- MAIN CONTENT -->
   <div id="main" role="main" ng-app="productDetailApp" ng-controller="productDetailController" ng-init="pageInit()">
      <div class="container">
         <div class="row">       
            <div class="main_content col-sm-12">          
               <!--=============== NEWS list =================-->
               <hr>
               <div class="collection_listing_main">
                  <div class="row ">
                    <!-- RIGHT side -->
                    <div class="main_content col-sm-8 col-sm-push-4">
                      <div class="blog_listing">
                        <h1 style="margin: 0; color: #ffffff;-webkit-border-radius: 5px 5px 0 0;border-radius: 5px 5px 0 0;background: #01489A;font-size: 1.5em;" class="page_heading">Tin Tức<a href="/blogs/blog.atom" target="_blank"></a></h1>
                        @if(count($listAllNews)>0)
                          @foreach($listAllNews as $n)
                            <!-- main content -->
                            <div class="blog_listing__article" style="padding: 15px; border: 1px solid #dddddd;margin-top: 0px!important;">
                              <!-- Ngay Dang -->
                                <div class="article_date">
                                    <time datetime="2015-07-20">
                                      <span class="date_2">Ngày Đăng</span>
                                      <span class="date_1">{{$n->day_updated_date}}</span>
                                      <span class="date_2">{{$n->month_updated_date}}/{{$n->year_updated_date}}</span>
                                    </time>
                                </div>
                                <!-- body -->
                                <div class="article_body">
                                    <div class="article_content rte" style="margin-top: 0px !important;">
                                      <a href="{{URL::to('chi-tiet-ban-tin/'.$n->news_slug)}}"><img src="{{$n->news_thumbnail}}" alt="{{$n->news_slug}}" class="img-responsive pull-right" style="width: 100%; height: 320px; object-fit: cover;"></a>
                                      
                                    </div>
                                    <div class="article_header">
                                      <h3 class="article_header__title product_name">
                                          <a href="{{URL::to('chi-tiet-ban-tin/'.$n->news_slug)}}">{{$n->news_title}}</a>
                                      </h3>
                                      <p style="height: 38px;">{!!mb_strimwidth($n->news_description,0,200,"...")!!}</p>
                                      <p>
                                        <span class="article_header__author">admin</span>
                                          <!-- <a class="article_comments__link" href="{{URL::to('chi-tiet-ban-tin/'.$n->news_slug)}}">0 Bình Luận</a> -->
                                      </p>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                          @endforeach
                        @endif
                      </div>
                    </div>
                    <!-- end RIGHT side -->
                    <!-- LEFT side -->
                    <div class="sidebar col-sm-4 sidebar_left col-sm-pull-8">
                      <div class="sidebar_widget sidebar_widget__articles">
                        <h3 class="widget_header">Bài Viết Mới</h3>
                        <div class="widget_content">
                          <ul class="list_articles" style="border: 1px solid #dddddd;padding: 10px">
                            <!-- MAIN CONTENT -->
                            @if(count($listPinnedNews)>0)
                              @foreach($listPinnedNews as $n)
                                <li>
                                  <div class="article_date">
                                    <time datetime="2015-07-20">
                                      <span class="date_2">Ngày Đăng</span>
                                      <span class="date_1">{{$n->day_updated_date}}</span>
                                      <span class="date_2">{{$n->month_updated_date}}/{{$n->year_updated_date}}</span>
                                    </time>
                                  </div>
                                    <div class="article_body">
                                      <a href="{{URL::to('chi-tiet-ban-tin/'.$n->news_slug)}}"><img src="{{$n->news_thumbnail}}" alt="{{$n->news_slug}}" style="width: 100%; height: 200px; object-fit: cover;"></a>
                                      <p class="item_title product_name">
                                        <a href="{{URL::to('chi-tiet-ban-tin/'.$n->news_slug)}}">{{$n->news_title}}</a>
                                      </p>
                                      <p class="item_content" style="height: 38px;">{!!mb_strimwidth($n->news_description,0,100,"...")!!}</p>
                                     <!--  <a class="article_comments__link" href="#">
                                        0 comments
                                      </a> -->
                                    </div>
                                  <div class="clearfix"></div>
                                </li>
                              @endforeach
                            @endif
                            <!-- END MAIN CONTENT -->
                          </ul>
                        </div>
                      </div>
                    </div>
                    <!-- END LEFT side --> 
                  </div>
               </div>
            </div>
            <!-- ============================= side bar ==================== -->
            
         </div>
      </div>
   </div>
@stop