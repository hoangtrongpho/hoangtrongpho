@extends('system.layouts.index')

@section('headTitle')
   <title>{{ Lang::get('messages.system_heade_title') }} - Skyfire System @ 2017</title>
@stop

@section('pageTitle')
   <h1>
        <i class="fa fa-fw fa-bar-chart" aria-hidden="true"></i> Thống Kê Hệ Thống
        <small>{{ Lang::get('messages.system_verion') }}</small>
    </h1>
    <ol class="breadcrumb">
         <li>
            <a href="{{URL::to('/System/Dashboard')}}"><i class="fa fa-fw fa-dashboard"></i> Hệ Thống Quản Trị Website</a>
         </li>
         <li class="active">
            <i class="fa fa-fw fa-bar-chart" aria-hidden="true"></i> Thống Kê Lượt Truy Cập
         </li>
    </ol>
@stop

@section('headCss')
   <!-- jvectormap -->
   <link rel="stylesheet" href="{{asset('public/system/plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
   <!-- Angular Toastr -->
   <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-toastr/angular-toastr.css')}}" />
   <!-- UI Bootstrap Modal --> 
   <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-ui-switch/bootstrap-switch.css')}}"/>

   <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/jquery.dataTables.css')}}" />
   <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css')}}">
   <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.css')}}">
   <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/dataTables.bootstrap.css')}}" />

   <style type="text/css">
      .pagination{
         margin: 0px;
      }
   </style>
@stop

@section('headJs')
   <!-- ChartJS 1.0.1 -->
   <script src="{{asset('public/system/plugins/chartjs/Chart.min.js')}}"></script>
   <!-- Angular Toastr -->
   <script type="text/javascript" src="{{asset('public/system/plugins/angular-toastr/angular-toastr.tpls.js')}}"></script>
   <!-- UI Bootstrap Modal --> 
   <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-bootstrap/ui-bootstrap-tpls-2.0.0.min.js')}}"></script>
   <!-- Sparkline -->
   <script src="{{asset('public/system/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
   <!-- jvectormap -->
   <script src="{{asset('public/system/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
   <script src="{{asset('public/system/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>

   
   <!-- Angular Datatables-->
   <script type="text/javascript" src="{{asset('public/system/plugins/datatables/jquery.dataTables.js')}}"></script>
   <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/angular-datatables.min.js')}}"></script>
   <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
   <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js')}}"></script>
   <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/colreorder/angular-datatables.colreorder.min.js')}}"></script>
   <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/bootstrap/angular-datatables.bootstrap.min.js')}}"></script>
   <!-- JS -->
   <script type="text/javascript" src="{{asset('public/system/common/system.common.angurlar.js')}}"></script>
   <script type="text/javascript" src="{{asset('public/system/common/system.statisticaccess.angurlar.js')}}"></script>

   <script type="text/javascript">
     
    $(document).ready(function() { 
        $(window).on('load', function() {  
            //insert all your ajax callback code here. 
            //Which will run only after page is fully loaded in background.
        });

        //Minify Left Menu
        //$("body").addClass("sidebar-collapse");
    });
</script>
@stop

@section('container')
<div ng-app="statisticAccessApp" ng-controller="statisticAccessController" ng-init="pageInit()">
   <!-- Main row -->
   <div class="row">
      <div class="col-md-12">
         <!-- MAP & BOX PANE -->
         <div class="box box-primary">
            <div class="box-header with-border">
               <h3 class="box-title"><i class="fa fa-fw fa-street-view" aria-hidden="true"></i> Bản Đồ Truy Cập</h3>
               <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse">
                     <i class="fa fa-fw fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" ng-click="loadMapVector()">
                     <i class="fa fa-fw fa-refresh"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove">
                     <i class="fa fa-fw fa-times"></i>
                  </button>
               </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
               <div class="row">
                  <div class="col-md-12">
                     <div class="pad" id="parrentWorldMap">
                        <!-- Map will be created here -->
                        <div id="world-map-markers" style="height: 480px;"></div>
                     </div>
                  </div>
               </div>
               <!-- /.row -->
            </div>
            <!-- /.box-body -->

             <!-- Loading (remove the following to stop the loading)-->
            <div ng-show="model.datainit.mapPreLoading" class="overlay">
                <i class="fa fa-fw fa-refresh fa-spin"></i>
            </div>
            <!-- end loading -->
         </div>
         <!-- /.box -->
      </div>
      <!-- /.col -->

      <div class="col-md-12">
         <div class="box box-success">
            <div class="box-header with-border">
               <h3 class="box-title"><i class="fa fa-fw fa-bar-chart" aria-hidden="true"></i> Thống Kê Lượt Truy Cập</h3>
               <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse">
                     <i class="fa fa-fw fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" ng-click="loadChartByMonth()">
                     <i class="fa fa-fw fa-refresh"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove">
                     <i class="fa fa-fw fa-times"></i>
                  </button>
               </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               <div class="row">
                  <div class="col-md-12">
                   
                     <div class="row">

                        <div class="col-md-6 col-xs-12 pull-left">
                           <strong id="leftChartTitle">&nbsp;</strong>
                           <strong id="rightChartTitle">&nbsp;</strong>
                        </div>

                        <div class="col-md-6 col-xs-12 pull-right text-right">
                           <div class="dataTables_paginate paging_simple_numbers" id="example1_paginate">
                              <ul class="pagination">
                                 <li class="paginate_button previous" id="example1_previous">
                                    <a href="" ng-click="previousMonth()" aria-controls="example1" data-dt-idx="0" tabindex="0"><i class="fa fa-fw fa-chevron-left" aria-hidden="true"></i> Tháng Trước</a>
                                 </li>
                                 <li class="paginate_button">
                                    <a href="" ng-click="reloadMonth()" aria-controls="example1" data-dt-idx="1" tabindex="0"><i class="fa fa-fw fa-refresh" aria-hidden="true"></i> Tháng Hiện Tại</a>
                                 </li>
                                 <li class="paginate_button next" id="example1_next">
                                    <a href="" ng-click="nextMonth()" aria-controls="example1" data-dt-idx="7" tabindex="0">Tháng Tiếp <i class="fa fa-fw fa-chevron-right" aria-hidden="true"></i></a>
                                 </li>
                              </ul>
                           </div>
                        </div>

                     </div>

                     <div class="chart" id="chartContent" style="min-height: 485px;">
                        <!-- Sales Chart Canvas -->
                        <canvas id="charByMonth" style="height: 480px;"></canvas>
                     </div>
                     <!-- /.chart-responsive -->
                  </div>
                  <!-- /.col -->
               </div>
               <!-- /.row -->
            </div>
            <!-- ./box-body -->

            <!-- Loading (remove the following to stop the loading)-->
            <div ng-show="model.datainit.accessPreLoading" class="overlay">
                <i class="fa fa-fw fa-refresh fa-spin"></i>
            </div>
            <!-- end loading -->
         </div>
         <!-- /.box -->
      </div>
      <!-- /.col -->
   </div>
   <!-- /.row -->

   <!-- /.row -->
   <div class="row" >
      <!-- Left col -->
      <div class="col-md-12">
         <!-- MAP & BOX PANE -->
         <div class="box box-primary">
            <div class="box-header with-border">
               <h3 class="box-title">Danh Sách Tổng Hợp</h3>
               <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-fw fa-minus"></i>
                  </button>
               </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                     <table  name="listResult" 
                             datatable="ng" 
                             dt-options="dtOptions"
                             width="100%" 
                             class="table table-bordered table-striped table-condensed">
                         <thead>
                             <tr>
                                 <th width="10%" class="text-center" data-column-index="0" >Đại Chỉ IP</th>
                                 <th width="10%" class="text-center" data-column-index="1" >Nước</th>
                                 <th width="30%" class="text-center" data-column-index="2" >Thành Phố</th>
                                 <th width="10%" class="text-center" data-column-index="3" >Nhà Cung Cấp ISP</th>
                                 <th width="10%" class="text-center" data-column-index="4" >Nền Tảng</th>
                                 <th width="10%" class="text-center" data-column-index="5" >Trình Duyệt</th>
                                 <th width="30%" class="text-center" data-column-index="6" >Ngày Truy Cập</th>
                             </tr>
                         </thead>
                         <tbody>
                             <tr role="row" 
                                 ng-repeat="counter in model.datainit.dtgListIP">
                                 <td class="text-center">[[counter.counter_client_ip]]</td>
                                 <td class="text-center">[[counter.counter_country_name]]</td>
                                 <td class="text-center">[[counter.counter_city]]</td>
                                 <td class="text-center">[[counter.counter_org]]</td>
                                 <td class="text-center">[[counter.counter_platform]]</td>
                                 <td class="text-center">[[counter.counter_browser]]</td>
                                 <td class="text-center">[[counter.counter_date]]</td>
                             </tr>
                         </tbody>
                     </table>
                  </div>
               </div>
               <!-- /.row -->
            </div>
            <!-- /.box-body -->
            <!-- Loading (remove the following to stop the loading)-->
            <div ng-show="model.datainit.accessPreLoading" class="overlay">
                <i class="fa fa-fw fa-refresh fa-spin"></i>
            </div>
            <!-- end loading -->
         </div>
         <!-- /.box -->
      </div>
      
   </div>
   <!-- /.row -->
</div>

@stop