@extends('system.layouts.index')

@section('headTitle')
   <title>{{ Lang::get('messages.system_heade_title') }} - Skyfire System @ 2017</title>
@stop

@section('pageTitle')
   <h1>
        <i class="fa fa-fw fa-tachometer" aria-hidden="true"></i> Dashboard
        <small>{{ Lang::get('messages.system_verion') }}</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{URL::to('/System/Dashboard')}}"><i class="fa fa-fw fa-dashboard"></i> Hệ Thống Quản Trị Website</a>
        </li>
    </ol>
@stop

@section('headCss')
   <!-- jvectormap -->
   <link rel="stylesheet" href="{{asset('public/system/plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
   <!-- Angular Toastr -->
   <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-toastr/angular-toastr.css')}}" />
   <!-- UI Bootstrap Modal --> 
   <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-ui-switch/bootstrap-switch.css')}}"/>
   <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/jquery.dataTables.css')}}" />
   <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css')}}">
   <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.css')}}">
   <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/dataTables.bootstrap.css')}}" />
   <style type="text/css">
      .pagination{
         margin: 0px;
      }
      .small-box h3
      {
         font-size: 24px !important;
         font-weight: bolder !important;
      }
      #myChart3-svg{
        padding-left: 6px;
      }
   </style>
@stop

@section('headJs')
    <script src = "https://cdn.zingchart.com/zingchart.min.js" ></script>  
    <script src = "https://cdn.zingchart.com/angular/zingchart-angularjs.js" ></script>  
   <!-- Angular Toastr -->
   <script type="text/javascript" src="{{asset('public/system/plugins/angular-toastr/angular-toastr.tpls.js')}}"></script>
   <!-- UI Bootstrap Modal --> 
   <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-bootstrap/ui-bootstrap-tpls-2.0.0.min.js')}}"></script>
   <!-- Angular Datatables-->
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/angular-datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/colreorder/angular-datatables.colreorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/bootstrap/angular-datatables.bootstrap.min.js')}}"></script>
   <!-- JS -->
   <script type="text/javascript" src="{{asset('public/system/common/system.common.angurlar.js')}}"></script>
   <script type="text/javascript" src="{{asset('public/system/common/system.zingcharttest.angurlar.js')}}"></script>

   <script type="text/javascript">
        $(document).ready(function() { 
            $(window).on('load', function() {  
                //Which will run only after page is fully loaded in background.
            });

            //Minify Left Menu
            // //$("body").addClass("sidebar-collapse");
        });
    </script>
@stop

@section('container')
<div ng-app="dashboardApp" ng-controller="dashboardController" ng-init="pageInit()">
    <div class="row">
 <!--        [[model.datainit.dataList]]
        <br>
        [[model.chart3.ChartData.series[0].values]] -->
        <div class="col-lg-12">
            <div zingchart id="myChart" zc-render="model.chart1.myRender" zc-json="model.chart1.ChartData" ></div>
        </div>
        <div class="col-lg-12">
            <div zingchart id="myChart2" zc-render="model.chart1.myRender" zc-json="model.chart2.ChartData" ></div>
        </div>
        <div class="col-lg-12" >
          <div class="col-lg-2 pull-right" >
            <div class="input-group">
              <span class="input-group-btn">
                <button ng-click="backYear()" class="btn btn-default" type="button"><i class="fa fa-backward" aria-hidden="true"></i></button>
              </span>

              <input 
                  type="text" 
                  class="form-control"
                  ng-disabled=true
                  ng-model="model.datainit.currentYear">

              <span class="input-group-btn">
                <button ng-click="nextYear()" class="btn btn-default" type="button"><i class="fa fa-forward" aria-hidden="true"></i></button>
              </span>
            </div>
          </div>
          
        </div>
        <br><br><br>
        <div class="col-lg-12" >
            <div  zingchart id="myChart3" zc-render="model.chart1.myRender" zc-json="model.chart3.ChartData" ></div>
        </div>

     
    </div>
</div>

@stop