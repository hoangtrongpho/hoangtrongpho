@extends('system.layouts.index')

@section('headTitle')
   <title>{{ Lang::get('messages.system_heade_title') }} - Skyfire System @ 2017</title>
@stop

@section('pageTitle')
   <h1>
        <i class="fa fa-fw fa-tachometer" aria-hidden="true"></i> Dashboard
        <small>{{ Lang::get('messages.system_verion') }}</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{URL::to('/System/Dashboard')}}"><i class="fa fa-fw fa-dashboard"></i> Hệ Thống Quản Trị Website</a>
        </li>
    </ol>
@stop

@section('headCss')
   <!-- jvectormap -->
   <link rel="stylesheet" href="{{asset('public/system/plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
   <!-- Angular Toastr -->
   <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-toastr/angular-toastr.css')}}" />
   <!-- UI Bootstrap Modal --> 
   <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-ui-switch/bootstrap-switch.css')}}"/>
   <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/jquery.dataTables.css')}}" />
   <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css')}}">
   <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.css')}}">
   <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/dataTables.bootstrap.css')}}" />
   <style type="text/css">
      .pagination{
         margin: 0px;
      }
      .small-box h3
      {
         font-size: 24px !important;
         font-weight: bolder !important;
      }
   </style>
@stop

@section('headJs')
  <script src = "https://cdn.zingchart.com/zingchart.min.js" ></script>  
    <script src = "https://cdn.zingchart.com/angular/zingchart-angularjs.js" ></script>
   <!-- ChartJS 1.0.1 -->
   <script src="{{asset('public/system/plugins/chartjs/Chart.min.js')}}"></script>
   <!-- Angular Toastr -->
   <script type="text/javascript" src="{{asset('public/system/plugins/angular-toastr/angular-toastr.tpls.js')}}"></script>
   <!-- UI Bootstrap Modal --> 
   <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-bootstrap/ui-bootstrap-tpls-2.0.0.min.js')}}"></script>
   <!-- Angular Datatables-->
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/angular-datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/colreorder/angular-datatables.colreorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/bootstrap/angular-datatables.bootstrap.min.js')}}"></script>
   <!-- JS -->
   <script type="text/javascript" src="{{asset('public/system/common/system.common.angurlar.js')}}"></script>
   <script type="text/javascript" src="{{asset('public/system/common/system.salereport.angurlar.js')}}"></script>

   <script type="text/javascript">
        $(document).ready(function() { 
            $(window).on('load', function() {  
                //Which will run only after page is fully loaded in background.
            });

            //Minify Left Menu
            // //$("body").addClass("sidebar-collapse");
        });
    </script>
@stop

@section('container')
  @if(Session::has('userAuth'))
      <?php
         $auth_id = Session::get('userAuth')->auth_id;
      ?> 
  @else
      <?php
         $auth_id = 0;
      ?> 
  @endif
<div ng-app="dashboardApp" ng-controller="dashboardController" ng-init="pageInit()">
   <!-- Info boxes -->
   <div class="row">
      <!-- fix for small devices only -->
      <div class="clearfix visible-sm-block"></div>

      <div class="col-md-3 col-sm-6 col-xs-12">
         <div class="small-box bg-blue">
            <div class="inner">
               <h3>[[model.datainit.totalProduct | number]]</h3>
               <strong>Sản Phẩm</strong>
               <h5>Tổng Sản Phẩm</h5>
            </div>
            <div class="icon">
               <i class="fa fa-archive" aria-hidden="true"></i>
            </div>
               <a href="{{URL::to('System/ListProducts')}}" class="small-box-footer">Danh Sách SP <i class="fa fa-fw fa-arrow-circle-right"></i>
            </a>
         </div>
      </div>

      <!-- box 1 #66ffff-->
      <div class="col-md-3 col-sm-6 col-xs-12">
         <div class="small-box bg-red">
            <div class="inner">
               <h3>[[model.datainit.totalInvoice | number]]</h3>
               <strong>Hóa Đơn</strong>
               <h5>Khối Lượng Giao Dịch</h5>
            </div>
            <div class="icon">
               <i class="fa fa-balance-scale" aria-hidden="true"></i>
            </div>
               <a href="{{URL::to('System/ListInvoices')}}" class="small-box-footer">Quản Lý Bán Hàng <i class="fa fa-fw fa-arrow-circle-right"></i>
            </a>
         </div>
      </div>
      <!-- box 2 #d2ff4d-->
      <div class="col-md-3 col-sm-6 col-xs-12">
         <div class="small-box bg-yellow">
            <div class="inner">
               <h3>[[model.datainit.totalInvoicePrice | number]]</h3>
               <strong>VNĐ</strong>
               <h5>Tổng Doanh Thu</h5>
            </div>
            <div class="icon">
               <i class="fa fa-fw fa-usd" aria-hidden="true"></i>
            </div>
               <a href="{{URL::to('System/ListInvoices')}}" class="small-box-footer">Doanh Thu <i class="fa fa-fw fa-arrow-circle-right"></i>
            </a>
         </div>
      </div>

      <!-- box 4 #ffcc00-->
      <div class="col-md-3 col-sm-6 col-xs-12" ng-show="{{$auth_id}}==1">
         <div class="small-box bg-green">
            <div class="inner">
               <h5><b>Tổng Lượt Xem: [[model.datainit.totalView | number]]</b></h5>
               <h5>Lượt Xem Trong Ngày : [[model.datainit.totalViewByDay | number]]</h5>
               <h5>Lượt Xem Trong Tháng : [[model.datainit.totalViewByMonth | number]]</h5>
               <h5>Lượt Xem Trong Năm : [[model.datainit.totalViewByYear | number]]</h5>
            </div>
            <div class="icon">
               <i class="fa fa-users" aria-hidden="true"></i>
            </div>
               {{-- <a href="{{URL::to('System/StatisticAccess')}}" class="small-box-footer">View Map <i class="fa fa-fw fa-arrow-circle-right"></i> --}}
               <a href="#" class="small-box-footer">Lượt Xem <i class="fa fa-fw fa-arrow-circle-right"></i>
            </a>
         </div>
      </div>
      <!-- box 5 -->
      
      <!-- box 6 #00ff99-->
   </div>
   <!-- sale system chart -->
  <div class="row">
        <div class="col-lg-12" >
          <div class="col-lg-4 pull-right" >
            <div class="input-group">
              <span style="background-color: lightblue" class="input-group-addon " id="basic-addon3">
              <i class="fa fa-calendar" aria-hidden="true"></i>
              Chọn Năm Thống Kê:
              </span>

              <span class="input-group-btn">
                <button ng-click="backYear()" class="btn btn-primary" type="button"><i class="fa fa-backward" aria-hidden="true"></i></button>
              </span>

              <input 
                  style="text-align:center; background-color: lightcyan"
                  type="text" 
                  class="form-control"
                  ng-disabled=true
                  ng-model="model.datainit.currentYear">

              <span class="input-group-btn">
                <button ng-click="nextYear()" class="btn btn-primary" type="button"><i class="fa fa-forward" aria-hidden="true"></i></button>
              </span>
            </div>
          </div>
          
        </div><br><br>
        <div class="col-lg-12" >
            <div  zingchart id="chartSaleSystem" zc-render="model.datainit.myRender" zc-json="model.chartSaleSystem" ></div>
        </div>
  </div>
  <!-- sale branch chart -->
    <div class="row">
        <div class="col-lg-12" >
          <div class="col-lg-4 pull-right" >
            <div class="input-group">
              <span style="background-color: lightblue" class="input-group-addon " id="basic-addon3">
              <i class="fa fa-calendar" aria-hidden="true"></i>
              Chọn Năm Thống Kê:
              </span>

              <span class="input-group-btn">
                <button ng-click="backYear()" class="btn btn-primary" type="button"><i class="fa fa-backward" aria-hidden="true"></i></button>
              </span>

              <input 
                  style="text-align:center; background-color: lightcyan"
                  type="text" 
                  class="form-control"
                  ng-disabled=true
                  ng-model="model.datainit.currentYear">

              <span class="input-group-btn">
                <button ng-click="nextYear()" class="btn btn-primary" type="button"><i class="fa fa-forward" aria-hidden="true"></i></button>
              </span>
            </div>
          </div>
          
        </div><br><br>
        <div class="col-lg-12" >
            <div  zingchart id="chartSaleBranch" zc-render="model.datainit.myRender" zc-json="model.chartSaleBranch" ></div>
        </div>
    </div>
   <!-- thong ke doanh thu theo từng khách hàng -->
   <div class="row">
      <div class="col-xs-12 col-md-12">
         <div class="box box-danger collapsed-box">
            <div class="box-header with-border" data-widget="collapse">
                <div class="alert alert-success" style="background-color: white !important">
                    <label class="btn" style="background-color: #0198ba"><i class="fa fa-fw fa-line-chart" aria-hidden="true"></i>Thống Kê Doanh Thu Theo Khách Hàng <span class="badge"></span></label>
                    <label class="btn" style="background-color: #0198ba">Số Khách Hàng: <span class="badge">[[(model.datainit.slbSaleCustomer).length]]</span></label>
                    <label class="btn" style="background-color: #0198ba">Tổng Doanh Số: <span class="badge">[[
                          model.datainit.slbSaleCustomerSum | number]] VNĐ</span></label>
                </div>
                
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse">
                     <i style="color: blue;font-size: 18px" class="fa fa-fw fa-plus"></i>
                  </button>
               </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               <div class="row">
                  <div class="col-sm-12">
                        <table  name="listResult" 
                                id="listResult" 
                                style="width: 100%;" 
                                datatable="ng" 
                                dt-options="dtOptions"
                                dt-column-defs="dtColumnDefs"
                                class="table table-bordered table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th class="text-center" data-column-index="0" style="width: auto;background-color: #0198ba;">Họ Tên</th>
                                    <th class="text-center" data-column-index="1" style="width: auto;background-color: #0198ba;"> SDT</th>
                                    <th class="text-center" data-column-index="2" style="width: auto;background-color: #0198ba;">Email</th>
                                    <th class="text-center" data-column-index="3" style="width: auto;background-color: #0198ba;">Địa Chỉ</th>
                                    <th class="text-center" data-column-index="4" style="width: auto;background-color: #0198ba;">CMND</th>
                                    <th class="text-center" data-column-index="5" style="width: auto;background-color: #0198ba;">Mã Sinh Viên</th>
                                    <th class="text-center" data-column-index="6" style="width: auto;background-color: #0198ba;">Tổng Doanh Thu</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr role="row" ng-repeat="cu in model.datainit.slbSaleCustomer">
                                    <td>[[cu.customer_fullname]]</td>
                                    <td>[[cu.customer_phone]]</td>
                                    <td>[[cu.customer_email]]</td>
                                    <td>[[cu.customer_address]]</td>
                                    <td>[[cu.customer_identify]]</td>
                                    <td>[[cu.customer_student_card]]</td>
                                    <td>[[cu.total_sum_format | currency:"":0]] VNĐ</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                  <!-- /.row -->
               </div>
               <!-- /.row -->
            </div>
            <!-- ./box-body -->
            <!-- Loading (remove the following to stop the loading)-->
            <div ng-show="model.datainit.preLoading" class="overlay">
                <i class="fa fa-fw fa-refresh fa-spin"></i>
            </div>
            <!-- end loading -->
         </div>
         <!-- /.box -->
      </div>
      <!-- /.col -->
   </div>
   <!-- thong ke công nợ theo từng khách hàng -->
   <div class="row">
      <div class="col-xs-12 col-md-12">
         <div class="box box-danger collapsed-box">
            <div class="box-header with-border" data-widget="collapse">
                <div class="alert alert-success" style="background-color: white !important">
                    <label class="btn" style="background-color: #e50031"><i class="fa fa-fw fa-line-chart" aria-hidden="true"></i>Thống Kê Công Nợ Theo Khách Hàng <span class="badge"></span></label>
                    <label class="btn" style="background-color: #e50031">Số Khách Hàng: <span class="badge">[[(model.datainit.slbDebtSaleCustomer).length]]</span></label>
                    <label class="btn" style="background-color: #e50031">Tổng Công Nợ: <span class="badge">[[
                          model.datainit.slbDebtSaleCustomerSum | number]] VNĐ</span></label>
                </div>
                
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse">
                     <i style="color: blue;font-size: 18px" class="fa fa-fw fa-plus"></i>
                  </button>
               </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               <div class="row">
                  <div class="col-sm-12">
                        <table  name="listResult" 
                                id="listResult" 
                                style="width: 100%;" 
                                datatable="ng" 
                                dt-options="dtOptions"
                                dt-column-defs="dtColumnDefs"
                                class="table table-bordered table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th class="text-center" data-column-index="0" style="width: auto;background-color: #e50031;">Họ Tên</th>
                                    <th class="text-center" data-column-index="1" style="width: auto;background-color: #e50031;"> SDT</th>
                                    <th class="text-center" data-column-index="2" style="width: auto;background-color: #e50031;">Email</th>
                                    <th class="text-center" data-column-index="3" style="width: auto;background-color: #e50031;">Địa Chỉ</th>
                                    <th class="text-center" data-column-index="4" style="width: auto;background-color: #e50031;">CMND</th>
                                    <th class="text-center" data-column-index="5" style="width: auto;background-color: #e50031;">Mã Sinh Viên</th>
                                    <th class="text-center" data-column-index="6" style="width: auto;background-color: #e50031;">Tổng Công Nợ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr role="row" ng-repeat="cu in model.datainit.slbDebtSaleCustomer">
                                    <td>[[cu.customer_fullname]]</td>
                                    <td>[[cu.customer_phone]]</td>
                                    <td>[[cu.customer_email]]</td>
                                    <td>[[cu.customer_address]]</td>
                                    <td>[[cu.customer_identify]]</td>
                                    <td>[[cu.customer_student_card]]</td>
                                    <td>[[cu.total_sum_format | currency:"":0]] VNĐ</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                  <!-- /.row -->
               </div>
               <!-- /.row -->
            </div>
            <!-- ./box-body -->
            <!-- Loading (remove the following to stop the loading)-->
            <div ng-show="model.datainit.preLoading" class="overlay">
                <i class="fa fa-fw fa-refresh fa-spin"></i>
            </div>
            <!-- end loading -->
         </div>
         <!-- /.box -->
      </div>
      <!-- /.col -->
   </div>
   <!-- thong ke doanh thu theo từng san phẩm-->
   <div class="row">
      <div class="col-xs-12 col-md-12">
         <div class="box box-danger collapsed-box">
            <div class="box-header with-border" data-widget="collapse">
               <div class="alert alert-success" style="background-color: white !important">
                    <label class="btn" style="background-color: #0ea000"><i class="fa fa-fw fa-line-chart" aria-hidden="true"></i>Thống Kê Doanh Thu Theo Sản Phẩm <span class="badge"></span></label>
                    <label class="btn" style="background-color: #0ea000">Số Sản Phẩm: <span class="badge">[[(model.datainit.slbSaleProduct).length]]</span></label>
                    <label class="btn" style="background-color: #0ea000">Tổng Doanh Thu: <span class="badge">[[
                          model.datainit.slbSaleProductSum | number]] VNĐ</span></label>
                </div>
                
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse">
                     <i style="color: blue;font-size: 18px" class="fa fa-fw fa-plus"></i>
                  </button>
               </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               <div class="row">
                  <div class="col-sm-12">
                        <table  name="listResult" 
                                id="listResult" 
                                style="width: 100%;" 
                                datatable="ng" 
                                dt-options="dtOptions"
                                dt-column-defs="dtColumnDefs"
                                class="table table-bordered table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th class="text-center" data-column-index="1" style="width: auto;background-color: #0ea000">Ảnh</th>
                                    <th class="text-center" data-column-index="2" style="width: auto;background-color: #0ea000">Danh Mục</th>
                                    <th class="text-center" data-column-index="2" style="width: auto;background-color: #0ea000">Tên SP</th>
                                    <th class="text-center" data-column-index="3" style="width: auto;background-color: #0ea000">Giá Niêm Yết</th>
                                    <th class="text-center" data-column-index="4" style="width: auto;background-color: #0ea000">Tổng Doanh Thu</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr role="row" ng-repeat="cu in model.datainit.slbSaleProduct">
                                    <td>
                                     <img ng-src="[[cu.product_thumbnail]]" src="" style="width: 150px;" title="[[cu.product_name]]" alt="[[cu.product_name]]">
                                    </td>
                                    <td>[[cu.cate_name]]</td>
                                    <td>[[cu.product_name]]</td>
                                    <td>[[cu.product_retail_prices]]</td>
                                    <td>[[cu.total_sum_format | currency:"":0]] VNĐ</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                  <!-- /.row -->
               </div>
               <!-- /.row -->
            </div>
            <!-- ./box-body -->
            <!-- Loading (remove the following to stop the loading)-->
            <div ng-show="model.datainit.preLoading" class="overlay">
                <i class="fa fa-fw fa-refresh fa-spin"></i>
            </div>
            <!-- end loading -->
         </div>
         <!-- /.box -->
      </div>
      <!-- /.col -->
   </div>
   <!-- thong ke Công Nợ theo từng San Pham-->
   <div class="row">
      <div class="col-xs-12 col-md-12">
         <div class="box box-danger  collapsed-box">
            <div class="box-header with-border" data-widget="collapse">
               <div class="alert alert-success" style="background-color: white !important">
                    <label class="btn" style="background-color: #f92900"><i class="fa fa-fw fa-line-chart" aria-hidden="true"></i>Thống Kê Công Nợ Theo Sản Phẩm <span class="badge"></span></label>
                    <label class="btn" style="background-color: #f92900">Số Sản Phẩm: <span class="badge">[[(model.datainit.slbDebtSaleProduct).length]]</span></label>
                    <label class="btn" style="background-color: #f92900">Tổng Công Nợ: <span class="badge">[[
                          model.datainit.slbDebtSaleProductSum | number]] VNĐ</span></label>
                </div>
                
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse">
                     <i style="color: blue;font-size: 18px" class="fa fa-fw fa-plus"></i>
                  </button>
               </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               <div class="row">
                  <div class="col-sm-12">
                        <table  name="listResult" 
                                id="listResult" 
                                style="width: 100%;" 
                                datatable="ng" 
                                dt-options="dtOptions"
                                dt-column-defs="dtColumnDefs"
                                class="table table-bordered table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th class="text-center" data-column-index="1" style="width: auto;background-color: #f92900">Ảnh</th>
                                    <th class="text-center" data-column-index="2" style="width: auto;background-color: #f92900">Danh Mục</th>
                                    <th class="text-center" data-column-index="2" style="width: auto;background-color: #f92900">Tên SP</th>
                                    <th class="text-center" data-column-index="3" style="width: auto;background-color: #f92900">Giá Niêm Yết</th>
                                    <th class="text-center" data-column-index="4" style="width: auto;background-color: #f92900">Tổng Công Nợ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr role="row" ng-repeat="cu in model.datainit.slbDebtSaleProduct">
                                    <td>
                                     <img ng-src="[[cu.product_thumbnail]]" src="" style="width: 150px;" title="[[cu.product_name]]" alt="[[cu.product_name]]">
                                    </td>
                                    <td>[[cu.cate_name]]</td>
                                    <td>[[cu.product_name]]</td>
                                    <td>[[cu.product_retail_prices | number]]</td>
                                    <td>[[cu.total_sum_format | currency:"":0]] VNĐ</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                  <!-- /.row -->
               </div>
               <!-- /.row -->
            </div>
            <!-- ./box-body -->
            <!-- Loading (remove the following to stop the loading)-->
            <div ng-show="model.datainit.preLoading" class="overlay">
                <i class="fa fa-fw fa-refresh fa-spin"></i>
            </div>
            <!-- end loading -->
         </div>
         <!-- /.box -->
      </div>
      <!-- /.col -->
   </div>
   <!-- thong ke doanh thu theo từng chi nhánh -->
   <div class="row">
      <div class="col-xs-12 col-md-12">
         <div class="box box-success collapsed-box">
            <div class="box-header with-border" data-widget="collapse">
                <div class="alert alert-success" style="background-color: white !important">
                    <label class="btn" style="background-color: #db6600"><i class="fa fa-fw fa-line-chart" aria-hidden="true"></i>Thống Kê Doanh Thu Theo Chi Nhánh <span class="badge"></span></label>
                    <label class="btn" style="background-color: #db6600">Số Chi Nhánh: <span class="badge">[[(model.datainit.slbSaleBranch).length]]</span></label>
                    <label class="btn" style="background-color: #db6600">Tổng Doanh Thu: <span class="badge">[[
                          model.datainit.slbSaleBranchSum | number]] VNĐ</span></label>
                </div>
                
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse">
                     <i style="color: blue;font-size: 18px" class="fa fa-fw fa-plus"></i>
                  </button>
               </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               <div class="row">
                  <div class="col-sm-12">
                        <table  name="listResult" 
                                id="listResult" 
                                style="width: 100%;" 
                                datatable="ng" 
                                dt-options="dtOptions"
                                dt-column-defs="dtColumnDefs"
                                class="table table-bordered table-striped table-condensed">
                            <thead >
                                <tr >
                                    <th  class="text-center" data-column-index="1" style="width: auto; background-color: #db6600">Tên Chi Nhánh</th>
                                    <th class="text-center" data-column-index="2" style="width: auto;background-color: #db6600">Địa Chỉ</th>
                                    <th class="text-center" data-column-index="3" style="width: auto;background-color: #db6600">SĐT</th>
                                    <th class="text-center" data-column-index="4" style="width: auto;background-color: #db6600">Tổng Doanh Thu</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr role="row" ng-repeat="cu in model.datainit.slbSaleBranch">
                                    <td>[[cu.branch_name]]</td>
                                    <td>[[cu.branch_address_number]]</td>
                                    <td>[[cu.branch_phone]]</td>
                                    <td>[[cu.total_sum_format | currency:"":0]] VNĐ</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                  <!-- /.row -->
               </div>
               <!-- /.row -->
            </div>
            <!-- ./box-body -->
            <!-- Loading (remove the following to stop the loading)-->
            <div ng-show="model.datainit.preLoading" class="overlay">
                <i class="fa fa-fw fa-refresh fa-spin"></i>
            </div>
            <!-- end loading -->
         </div>
         <!-- /.box -->
      </div>
      <!-- /.col -->
   </div>
   <!-- thong ke cong no theo từng chi nhánh -->
   <div class="row">
      <div class="col-xs-12 col-md-12">
         <div class="box box-danger collapsed-box">
            <div class="box-header with-border" data-widget="collapse">
                <div class="alert alert-success" style="background-color: white !important">
                    <label class="btn" style="background-color: #dd4b39"><i class="fa fa-fw fa-line-chart" aria-hidden="true"></i>Thống Kê Công Nợ Theo Chi Nhánh <span class="badge"></span></label>
                    <label class="btn" style="background-color: #dd4b39">Số Chi Nhánh: <span class="badge">[[(model.datainit.slbDebtSaleBranch).length]]</span></label>
                    <label class="btn" style="background-color: #dd4b39">Tổng Công Nợ: <span class="badge">[[
                          model.datainit.slbDebtSaleBranchSum | number]] VNĐ</span></label>
                </div>
                
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse">
                     <i style="color: blue;font-size: 18px" class="fa fa-fw fa-plus"></i>
                  </button>
               </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               <div class="row">
                  <div class="col-sm-12">
                        <table  ng-init="total = 0"
                                name="listResult" 
                                id="listResult" 
                                style="width: 100%;" 
                                datatable="ng" 
                                dt-options="dtOptions"
                                dt-column-defs="dtColumnDefs"
                                class="table table-bordered table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th class="text-center" data-column-index="1" style="width: auto;background-color: #dd4b39">Tên Chi Nhánh</th>
                                    <th class="text-center" data-column-index="2" style="width: auto;background-color: #dd4b39">Địa Chỉ</th>
                                    <th class="text-center" data-column-index="2" style="width: auto;background-color: #dd4b39">SĐT</th>
                                    <th class="text-center" data-column-index="4" style="width: auto;background-color: #dd4b39">Tổng Doanh Thu</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr role="row" ng-repeat="cu in model.datainit.slbDebtSaleBranch">
                                    <td>[[cu.branch_name]]</td>
                                    <td>[[cu.branch_address_number]]</td>
                                    <td>[[cu.branch_phone]]</td>
                                    <td >[[cu.total_sum_format | currency:"":0]] VNĐ</td>
                                </tr>
                               
                            </tbody>
                        </table>
                    </div>
                  <!-- /.row -->
               </div>
               <!-- /.row -->
            </div>
            <!-- ./box-body -->
            <!-- Loading (remove the following to stop the loading)-->
            <div ng-show="model.datainit.preLoading" class="overlay">
                <i class="fa fa-fw fa-refresh fa-spin"></i>
            </div>
            <!-- end loading -->
         </div>
         <!-- /.box -->
      </div>
      <!-- /.col -->
   </div>
   <!-- thong ke doanh thu theo từng slbSaleSubadmin -->
   <div class="row">
      <div class="col-xs-12 col-md-12">
         <div class="box box-success collapsed-box">
            <div class="box-header with-border" data-widget="collapse">
                <div class="alert alert-success" style="background-color: white !important">
                    <label class="btn" style="background-color: #bec100"><i class="fa fa-fw fa-line-chart" aria-hidden="true"></i>Thống Kê Doanh Thu Theo SubAdmin <span class="badge"></span></label>
                    <label class="btn" style="background-color: #bec100">Số SubAdmin: <span class="badge">[[(model.datainit.slbSaleSubadmin).length]]</span></label>
                    <label class="btn" style="background-color: #bec100">Tổng Doanh Thu: <span class="badge">[[
                          model.datainit.slbSaleSubadminSum | number]] VNĐ</span></label>
                </div>
                
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse">
                     <i style="color: blue;font-size: 18px" class="fa fa-fw fa-plus"></i>
                  </button>
               </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               <div class="row">
                  <div class="col-sm-12">
                        <table  name="listResult" 
                                id="listResult" 
                                style="width: 100%;" 
                                datatable="ng" 
                                dt-options="dtOptions"
                                dt-column-defs="dtColumnDefs"
                                class="table table-bordered table-striped table-condensed">
                            <thead >
                                <tr >
                                    <th  class="text-center" data-column-index="0" style="width: auto; background-color: #bec100">Hình Ảnh</th>
                                    <th  class="text-center" data-column-index="1" style="width: auto; background-color: #bec100">Tên</th>
                                    <th  class="text-center" data-column-index="2" style="width: auto; background-color: #bec100">Account</th>
                                    <th  class="text-center" data-column-index="3" style="width: auto; background-color: #bec100">SĐT</th>
                                    <th  class="text-center" data-column-index="4" style="width: auto; background-color: #bec100">Email</th>
                                    <th  class="text-center" data-column-index="5" style="width: auto; background-color: #bec100">Quyền</th>
                                    <th class="text-center" data-column-index="6" style="width: auto;background-color: #bec100">Tổng Doanh Thu</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr role="row" ng-repeat="cu in model.datainit.slbSaleSubadmin">
                                    <td>
                                        <img ng-src="[[cu.user_avatar]]" src="" style="width: 100px;" title="user_avatar" alt="user_avatar">
                                    </td>
                                    <td>[[cu.user_fullname]]</td>
                                    <td>[[cu.user_account]]</td>
                                    <td>[[cu.user_phone]]</td>
                                    <td>[[cu.user_email]]</td>
                                    <td>[[cu.auth_id]]</td>
                                    <td>[[cu.total_sum_format | currency:"":0]] VNĐ</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                  <!-- /.row -->
               </div>
               <!-- /.row -->
            </div>
            <!-- ./box-body -->
            <!-- Loading (remove the following to stop the loading)-->
            <div ng-show="model.datainit.preLoading" class="overlay">
                <i class="fa fa-fw fa-refresh fa-spin"></i>
            </div>
            <!-- end loading -->
         </div>
         <!-- /.box -->
      </div>
      <!-- /.col -->
   </div>
   <!-- thong ke cong no theo từng slbSaleSubadmin -->
   <div class="row">
      <div class="col-xs-12 col-md-12">
         <div class="box box-success collapsed-box">
            <div class="box-header with-border" data-widget="collapse">
               <div class="alert alert-success" style="background-color: white !important">
                    <label class="btn" style="background-color: #8004ed"><i class="fa fa-fw fa-line-chart" aria-hidden="true"></i>Thống Kê Công Nợ Theo SubAdmin <span class="badge"></span></label>
                    <label class="btn" style="background-color: #8004ed">Số SubAdmin: <span class="badge">[[(model.datainit.slbDebtSubadmin).length]]</span></label>
                    <label class="btn" style="background-color: #8004ed">Tổng Công Nợ: <span class="badge">[[
                          model.datainit.slbDebtSubadminSum | number]] VNĐ</span></label>
                </div>
                
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse">
                     <i style="color: blue;font-size: 18px" class="fa fa-fw fa-plus"></i>
                  </button>
               </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               <div class="row">
                  <div class="col-sm-12">
                        <table  name="listResult" 
                                id="listResult" 
                                style="width: 100%;" 
                                datatable="ng" 
                                dt-options="dtOptions"
                                dt-column-defs="dtColumnDefs"
                                class="table table-bordered table-striped table-condensed">
                            <thead >
                                <tr >
                                    <th  class="text-center" data-column-index="0" style="width: auto; background-color: #8004ed">Hình Ảnh</th>
                                    <th  class="text-center" data-column-index="1" style="width: auto; background-color: #8004ed">Tên</th>
                                    <th  class="text-center" data-column-index="2" style="width: auto; background-color: #8004ed">Account</th>
                                    <th  class="text-center" data-column-index="3" style="width: auto; background-color: #8004ed">SĐT</th>
                                    <th  class="text-center" data-column-index="4" style="width: auto; background-color: #8004ed">Email</th>
                                    <th  class="text-center" data-column-index="5" style="width: auto; background-color: #8004ed">Quyền</th>
                                    <th class="text-center" data-column-index="6" style="width: auto;background-color: #8004ed">Tổng Doanh Thu</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr role="row" ng-repeat="cu in model.datainit.slbDebtSubadmin">
                                    <td>
                                        <img ng-src="[[cu.user_avatar]]" src="" style="width: 100px;" title="user_avatar" alt="user_avatar">
                                    </td>
                                    <td>[[cu.user_fullname]]</td>
                                    <td>[[cu.user_account]]</td>
                                    <td>[[cu.user_phone]]</td>
                                    <td>[[cu.user_email]]</td>
                                    <td>[[cu.auth_id]]</td>
                                    <td>[[cu.total_sum_format | currency:"":0]] VNĐ</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                  <!-- /.row -->
               </div>
               <!-- /.row -->
            </div>
            <!-- ./box-body -->
            <!-- Loading (remove the following to stop the loading)-->
            <div ng-show="model.datainit.preLoading" class="overlay">
                <i class="fa fa-fw fa-refresh fa-spin"></i>
            </div>
            <!-- end loading -->
         </div>
         <!-- /.box -->
      </div>
      <!-- /.col -->
   </div>

</div>

@stop