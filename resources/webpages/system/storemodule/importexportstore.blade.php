@extends('system.layouts.index')

@section('headTitle')
   <title>{{ Lang::get('messages.system_heade_title') }}</title>
@stop

@section('pageTitle')
   <h1>
        <i class="fa fa-fw fa-database" aria-hidden="true"></i> Danh Sách Quản Trị Xuất Nhập Kho Hàng
        <small>Skyfire Sytem Version 1.0</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{URL::to('/System/Dashboard')}}"><i class="fa fa-fw fa-dashboard"></i> Hệ Thống Quản Trị Website</a>
        </li>
        <li class="active"><i class="fa fa-fw fa-motorcycle" aria-hidden="true"></i>Quản Trị Xuất Nhập Kho Hàng</li>
    </ol>
@stop

@section('headCss')
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-toastr/angular-toastr.css')}}" />

    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/jquery.dataTables.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/dataTables.bootstrap.css')}}" />

    <style type="text/css">
        .control-label{
            padding-top: 5px;
            padding-right: 5px !important;
            padding-left: 5px !important;
            float: left;
        }

        #listResult {
            width: 100% !important;
        }
        .multiSelect .buttonLabel{
            width: auto !important;
        }
    </style>
@stop

@section('headJs')

    <!-- UI Bootstrap Modal --> 
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-bootstrap/ui-bootstrap-tpls-2.0.0.min.js')}}"></script>

    <!-- Angular Toastr -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-toastr/angular-toastr.tpls.js')}}"></script>

    <!-- Angular Confirm Dialog -->
    <script type="text/javascript" src="{{asset('public/system/plugins/bootbox/bootbox.min.js')}}"></script>

    <!-- Angular Datatables-->
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/angular-datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/colreorder/angular-datatables.colreorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/bootstrap/angular-datatables.bootstrap.min.js')}}"></script>

    <!-- JS -->
    <script type="text/javascript" src="{{asset('public/system/common/system.common.angurlar.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/common/system.importexportstore.angurlar.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function() { 
            $(window).on('load', function() {  

            });
            
            //Minify Left Menu
            // //$("body").addClass("sidebar-collapse");
        });

    </script>
@stop

@section('container')
<div class="row" ng-app="bikeApp" ng-controller="bikeController" ng-init="pageInit()">

    <div class="col-xs-12 col-md-12">
         <div class="box box-danger">
            <div class="box-header with-border" data-widget="collapse">
                <div class="alert alert-success" style="background-color: white !important">
                    <label class="btn" style="background-color: #0198ba">
                    <i class="fa fa-fw fa-database" aria-hidden="true"></i>
                    Quản Trị Xuất Nhập Kho Hàng
                    
                    </label>
                </div>
                
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse">
                     <i style="color: blue;font-size: 18px" class="fa fa-fw fa-plus"></i>
                  </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-4 col-md-offset-8">
                        <div class="input-group">
                            <input 
                            ng-model="model.request.searchStr"
                            placeholder="Từ Khóa" 
                            type="text" 
                            class="form-control" aria-label="...">
                            <div class="input-group-btn">
                                <button ng-click="GetBranchList()" class="btn btn-primary"> Tìm Kiếm</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <table  name="listResult1" 
                                id="listResult1" 
                                style="width: 100%;" 
                                class="table table-bordered table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th class="text-center" data-column-index="2" style="width: auto;">Sản Phẩm</th>
                                    <th class="text-center" data-column-index="3" style="width: auto;">Số Lượng<br>Đầu Kỳ</th>
                                    <!-- loop -->
                                    <th ng-repeat="item in model.datainit.dtgListBranch" class="text-center" data-column-index="4" style="width: auto;">[[item.branch_name]]</th>
                                    <th class="text-center" data-column-index="6" style="width: auto;">Tổng Kho Sau<br>Khi Chuyển CN</th>
                               
                                </tr>
                            </thead>
                            <tbody>
                                <tr role="row" ng-repeat="product in model.datainit.dtgList2 ">
                                    <td style="width: 100px">
                                        <img ng-src="[[product.product_thumbnail]]" src="" style="width: 100px;" title="a" alt="a"> <br>
                                        [[product.product_name | limitTo:20]]...
                                    </td>
                                    <!-- tong kho -->
                                    <td style="width: auto;text-align: right;">
                                        <div class="input-group" style="width: 125px">
                                            <span class="input-group-addon" style="color: red">
                                                <span class="badge">[[product.store_quantity]]</span>
                                            </span>
                                            <input 
                                            style="width: 60px"
                                            type="number"
                                            class="form-control" 
                                            id="sldk_[[product.product_id]]"
                                            name="[[product.product_id]]"
                                            autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                            ng-min="0"
                                            ng-max="999"
                                            max="999"
                                            value = "1"
                                            min = "0"
                                            ng-value = 0>
                                            <span 
                                            ng-click="UpdateStoreSumQuan('sldk_',[[product.product_id]],[[product.store_quantity]])" 
                                            class="input-group-addon btn btn-danger">
                                            Nhập
                                            </span>
                                        </div>
                                    </td>
                                    <!-- loop ngay ở đây -->
                                    <td ng-repeat="item in model.datainit.dtgListBranchItems" style="width: 150px">
                                        <span class="input-group-addon" style="color: red;width: 75px"><b>Tồn:</b>
                                            <span class="badge">[[
                                            ( item.store_items | filter: { product_id: product.product_id } )[0].branch_store_quantity
                                            -
                                            ( item.store_items | filter: { product_id: product.product_id } )[0].sum_sale_branch_quantity
                                            ]]</span>
                                        </span>
                                        <span class="input-group-addon" style="color: red;width: 75px"><b>Bán:</b>
                                            <span class="badge">
                                            [[
                                            ( item.store_items | filter: { product_id: product.product_id } )[0].sum_sale_branch_quantity
                                            ]]
                                            </span>
                                        </span>

                                        <div class="input-group" style="width: 150px">
                                            <input 
                                            style="width: 115px"
                                            type="number"
                                            class="form-control" 
                                            id="get_[[item.branch_id]]_[[product.product_id]]"
                                            name="[[product.product_id]]"
                                            autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                            ng-min="0"
                                            ng-max="999"
                                            max="999"
                                            value = "0"
                                            min = "0"
                                            ng-value = 0>

                                            <!--truyen tham so id chi nhanh de cap nhat-->
                                            <span
                                            ng-click="UpdateStoreQuan('get',
                                            [[
                                            (item.store_items | filter: { product_id: product.product_id } )[0].branch_store_quantity
                                            ]],
                                            [[product.product_id]],
                                            [[item.branch_id]],
                                            [[product.sum_deliver_branch_remain]])" 
                                            class="input-group-addon btn btn-primary">
                                            Nhận
                                            </span>

                                        </div>
                                        <div class="input-group" style="width: 150px">
                                            <input 
                                            style="width: 115px"
                                            type="number"
                                            class="form-control" 
                                            id="return_[[item.branch_id]]_[[product.product_id]]"
                                            name="[[product.product_id]]"
                                            autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                            ng-min="0"
                                            ng-max="999"
                                            max="999"
                                            value = "0"
                                            min = "0"
                                            ng-value = 0>
                                            <!--truyen tham so id chi nhanh de cap nhat-->
                                            <span
                                            ng-click="UpdateReturnStoreQuan(
                                            'return',
                                            [[product.product_id]],
                                            [[item.branch_id]],
                                            [[
                                            (item.store_items | filter: { product_id: product.product_id } )[0].branch_store_quantity
                                            -
                                            ( item.store_items | filter: { product_id: product.product_id } )[0].sum_sale_branch_quantity
                                            ]],
                                            [[
                                            (item.store_items | filter: { product_id: product.product_id } )[0].branch_store_quantity
                                            ]]
                                            )" 
                                            class="input-group-addon btn btn-primary">
                                            Trả
                                            </span>
                                        </div>
                                    </td>
                                    <!--  tong tón au khi  chuyen -->
                                    <td style="width: auto;text-align: right;">
                                        <span class="badge" style="font-size:16px!important ">[[product.sum_deliver_branch_remain]]</span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
               <!-- /.row -->
            </div>
            <!-- ./box-body -->
         </div>
         <!-- /.box -->
    </div>

</div>
@stop