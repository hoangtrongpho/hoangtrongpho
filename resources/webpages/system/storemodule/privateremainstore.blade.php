@extends('system.layouts.index')

@section('headTitle')
   <title>{{ Lang::get('messages.system_heade_title') }}</title>
@stop

@section('pageTitle')
   <h1>
        <i class="fa fa-fw fa-database" aria-hidden="true"></i> Danh Sách Tồn Kho
        <small>Skyfire Sytem Version 1.0</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{URL::to('/System/Dashboard')}}"><i class="fa fa-fw fa-dashboard"></i> Hệ Thống Quản Trị Website</a>
        </li>
        <li class="active"><i class="fa fa-fw fa-motorcycle" aria-hidden="true"></i> Quản Lý Kho Hàng Tồn</li>
    </ol>
@stop

@section('headCss')
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-toastr/angular-toastr.css')}}" />

    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/jquery.dataTables.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/dataTables.bootstrap.css')}}" />

    <style type="text/css">
        .control-label{
            padding-top: 5px;
            padding-right: 5px !important;
            padding-left: 5px !important;
            float: left;
        }

        #listResult {
            width: 100% !important;
        }
        .multiSelect .buttonLabel{
            width: auto !important;
        }
    </style>
@stop

@section('headJs')

    <!-- UI Bootstrap Modal --> 
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-bootstrap/ui-bootstrap-tpls-2.0.0.min.js')}}"></script>

    <!-- Angular Toastr -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-toastr/angular-toastr.tpls.js')}}"></script>

    <!-- Angular Confirm Dialog -->
    <script type="text/javascript" src="{{asset('public/system/plugins/bootbox/bootbox.min.js')}}"></script>

    <!-- Angular Datatables-->
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/angular-datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/colreorder/angular-datatables.colreorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/bootstrap/angular-datatables.bootstrap.min.js')}}"></script>

    <!-- JS -->
    <script type="text/javascript" src="{{asset('public/system/common/system.common.angurlar.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/common/system.privateremainstore.angurlar.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function() { 
            $(window).on('load', function() {  
            });

            //Minify Left Menu
            // //$("body").addClass("sidebar-collapse");
        });
    </script>
@stop

@section('container')
<div class="row" ng-app="bikeApp" ng-controller="bikeController" ng-init="pageInit()">

    <!--======================= test================ -->
        <div class="col-xs-12 col-md-12">
             <div class="box box-danger">
                <div class="box-header with-border" data-widget="collapse">
                     <div class="alert alert-success" style="background-color: white !important">
                        <label class="btn" style="background-color: #0198ba">
                        <i class="fa fa-fw fa-database" aria-hidden="true"></i>
                        Quản Trị Kho Hàng Tồn Tại Chi Nhánh
                        </label>

                    </div>
                    
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse">
                         <i style="color: blue;font-size: 18px" class="fa fa-fw fa-plus"></i>
                      </button>
                   </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <table  name="listResult" 
                                    id="listResult" 
                                    style="width: 100%;" 
                                    datatable="ng" 
                                    dt-options="dtOptions"
                                    dt-column-defs="dtColumnDefs"
                                    class="table table-bordered table-striped table-condensed">
                                <thead>
                                    <tr>
                                        <th class="text-center" data-column-index="1" style="width: auto;">Ảnh</th>
                                        <th class="text-center" data-column-index="2" style="width: auto;">Tên SP</th>
                                        <th class="text-center" data-column-index="3" style="width: auto;">Tồn Kho</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr role="row" ng-repeat="product in model.datainit.dtgList ">
                                        <td>
                                            <img ng-src="[[product.product_thumbnail]]" src="" style="width: 100px;" title="a" alt="a">
                                        </td>
                                        <td style="width: auto">
                                            [[product.product_name]]
                                        </td>
                                        <!-- tong kho Khi Chuyển CN-->
                                        <td style="width: auto;text-align: right;">
                                            <span class="badge">[[product.branch_store_quantity-product.sum_sale_branch_quantity]]</span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                   <!-- /.row -->
                </div>
                <!-- ./box-body -->
             </div>
             <!-- /.box -->
        </div>
   
</div>
@stop