@extends('system.layouts.index')

@section('headTitle')
   <title>Quản Trị Trang Thông Tin - {{ Lang::get('messages.system_heade_title') }}</title>
@stop

@section('pageTitle')
   <h1>
        <i class="fa fa-fw fa-newspaper-o" aria-hidden="true"></i> Quản Trị Trang Thông Tin
        <small>Skyfire Sytem Version 1.0</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{URL::to('/System/Dashboard')}}"><i class="fa fa-fw fa-dashboard"></i> Hệ Thống Quản Trị Website</a>
        </li>
        <li class="active"><i class="fa fa-fw fa-newspaper-o" aria-hidden="true"></i> Danh Sách Tin Tức</li>
    </ol>
@stop

@section('headCss')
<!-- <link rel="stylesheet" href="{{asset('public/system/css/datetimepicker.css')}}"/> -->
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/daterangepicker/daterangepicker.css')}}"/>
<!-- <link rel="stylesheet" href="{{asset('public/system/plugins/angular-ui-switch/angular-ui-switch.css')}}"/> -->
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-ui-switch/bootstrap-switch.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-multi-select/isteven-multi-select.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-toastr/angular-toastr.css')}}" />

<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/jquery.dataTables.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/dataTables.bootstrap.css')}}" />

<style type="text/css">
    .control-label{
        padding-top: 5px;
        padding-right: 5px !important;
        padding-left: 5px !important;
        float: left;
    }

    #listResult {
        width: 100% !important;
    }
</style>
@stop

@section('headJs')
    <!-- Date Time Picker -->
    <script type="text/javascript" src="{{asset('public/system/plugins/daterangepicker/daterangepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-daterangepicker/angular-daterangepicker.min.js')}}"></script>
    <!-- <script type="text/javascript" src="{{asset('public/system/plugins/datepicker/locales/bootstrap-datepicker.vi.js')}}"></script> -->
    <!-- <script type="text/javascript" src="{{asset('public/system/js/datetimepicker.js')}}"></script> -->
    <!-- <script type="text/javascript" src="{{asset('public/system/js/datetimepicker.templates.js')}}"></script> -->

    <!-- <script src="{{asset('public/system/plugins/angular-ui-switch/angular-ui-switch.js')}}"></script> -->
    <script src="{{asset('public/system/plugins/angular-ui-switch/bootstrap-switch.js')}}"></script>
    <script src="{{asset('public/system/plugins/angular-ui-switch/angular-bootstrap-switch.js')}}"></script>

    <!-- iCheck -->
    <script src="{{asset('public/system/plugins/angular_bootstrap_checkbox/angular-bootstrap-checkbox.js')}}"></script>

    <!-- File Manager -->
    <!-- Angular Multi Select -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-multi-select/isteven-multi-select.js')}}"></script>

    <!-- UI Bootstrap Modal --> 
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-bootstrap/ui-bootstrap-tpls-2.0.0.min.js')}}"></script>

    <!-- Angular Toastr -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-toastr/angular-toastr.tpls.js')}}"></script>

    <!-- Angular Confirm Dialog -->
    <script type="text/javascript" src="{{asset('public/system/plugins/bootbox/bootbox.min.js')}}"></script>

    <!-- Angular Datatables-->
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/angular-datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/colreorder/angular-datatables.colreorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/bootstrap/angular-datatables.bootstrap.min.js')}}"></script>

    <!-- JS -->
    <script type="text/javascript" src="{{asset('public/system/common/system.common.angurlar.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/common/system.listpages.angurlar.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function() { 
            $(window).on('load', function() {  
            });


            //Minify Left Menu
            //$("body").addClass("sidebar-collapse");
        });
    </script>
@stop

@section('container')
<div class="row" ng-app="pageApp" ng-controller="pageController" ng-init="pageInit()">

    <div class="col-lg-12">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-fw fa-list" aria-hidden="true"></i> Danh Sách Tin Tức</h3>
                <div class="box-tools pull-right">
                    <a class="btn btn-primary" href="{{URL::to('System/CreateNews')}}" title="CreateNews">
                        <i class="fa fa-fw fa-plus" aria-hidden="true"></i>
                        <span>Thêm Mới</span>
                    </a>
                </div>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <table  name="listResult" 
                                id="listResult" 
                                style="width: 100%;" 
                                datatable="ng" 
                                dt-options="dtOptions"
                                dt-column-defs="dtColumnDefs"
                                class="table table-bordered table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th class="text-center" data-column-index="0" style="width: 80px;">Tùy Chọn</th>
                                    <th class="text-center" data-column-index="1" style="width: auto;">Tin Tức</th>
                                    <th class="text-center" data-column-index="2" style="width: 15%;">Thể Loại</th>
                                    <th class="text-center" data-column-index="3" style="width: 10%;">Trạng Thái</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr role="row" ng-repeat="page in model.datainit.dtgList">
                                    <td class="text-center">
                                        <a href="{{URL::to('System/UpdateNews/[[page.news_slug]]')}}" class="btn btn-warning btn-xs" title="[[page.news_title]]"><i class="fa fa-fw fa-edit fa-fw" aria-hidden="true"></i></a>
                                        <button ng-show="page.status == 1" type="button" ng-click="changeStatus(page.news_id, 0)" class="btn btn-danger btn-xs"><i class="fa fa-fw fa-eye-slash fa-fw" aria-hidden="true"></i></button>
                                        <button ng-show="page.status == 0" type="button" ng-click="changeStatus(page.news_id, 1)" class="btn btn-success btn-xs"><i class="fa fa-fw fa-eye fa-fw" aria-hidden="true"></i></button>
                                        <button type="button" ng-click="removeItem(page.news_id)" class="btn btn-danger btn-xs"><i class="fa fa-fw fa-trash-o fa-fw"></i></button>
                                    </td>
                                    <td>[[page.page_title]]</td>
                                    <td>[[page.cate_name]]</td>
                                    <td class="text-center">
                                        <span style="display:none;">[[page.status]]</span>
                                        <a ng-show="page.status == 1" role="button" class="btn btn-success disabled">Hiện</a>
                                        <a ng-show="page.status == 0" role="button" class="btn btn-danger disabled">&nbsp;&nbsp;Ẩn&nbsp;</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <!-- /.box-body -->

            <!-- Loading (remove the following to stop the loading)-->
            <div id="listReLoading" class="overlay">
                <i class="fa fa-fw fa-refresh fa-spin"></i>
            </div>
            <!-- end loading -->

        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
@stop