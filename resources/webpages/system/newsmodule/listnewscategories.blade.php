@extends('system.layouts.index')

@section('headTitle')
   <title>{{ Lang::get('messages.system_heade_title') }}</title>
@stop

@section('pageTitle')
   <h1>
        <i class="fa fa-fw fa-gavel" aria-hidden="true"></i> Nhóm Tin Sản Phẩm
        <small>{{ Lang::get('messages.system_verion') }}</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{URL::to('/System/Dashboard')}}"><i class="fa fa-fw fa-dashboard"></i> Hệ Thống Quản Trị Website</a>
        </li>
        <li class="active"><i class="fa fa-fw fa-gavel" aria-hidden="true"></i> Nhóm Tin Sản Phẩm</li>
    </ol>
@stop

@section('headCss')
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-toastr/angular-toastr.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/jquery.dataTables.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/dataTables.bootstrap.css')}}" />
@stop

@section('headJs')
    <!-- UI Bootstrap Modal --> 
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-bootstrap/ui-bootstrap-tpls-2.0.0.min.js')}}"></script>
    
    <!-- Angular Datatables-->
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/angular-datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/colreorder/angular-datatables.colreorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/bootstrap/angular-datatables.bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/tabletools/angular-datatables.tabletools.min.js')}}"></script>
    <!-- JS -->
    <script type="text/javascript" src="{{asset('public/system/common/system.listnewscategories.angurlar.js')}}"></script>
@stop

@section('container')
<div class="row" ng-app="productUnitApp" ng-controller="productUnitsController" ng-init="pageInit()">

    <!-- Modal -->
    <div id="modalAdd" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <form class="form" class="form-inline" name="frmAdd" id="frmAdd" novalidate>
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-blue">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-fw fa-floppy-o" aria-hidden="true"></i> Thêm Mới</h4>
                </div>
                <div class="modal-body">
                    
                    <div class="col-xs-12">
                        <div class="form-group" ng-class="{ 'has-error' : frmAdd.txtName.$touched && frmAdd.txtName.$invalid && !frmAdd.txtName.$pristine }">
                            <label for="txtName" class="control-label">Tên Nhóm Tin:</label>
                            <input  tabindex="1"
                                    type="text"
                                    class="form-control" 
                                    id="txtName" 
                                    name="txtName" 
                                    placeholder="Nhập Tên Nhóm Tin" 
                                    ng-model="model.request.txtName"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                    ng-minlength="2"
                                    ng-maxlength="50"
                                    maxlength="50"
                                    required >
                            <span class="help-block" ng-show="frmAdd.txtName.$touched && frmAdd.txtName.$error.required && frmAdd.txtTagName.$invalid && !frmAdd.txtTagName.$pristine">Vui Lòng Nhập Tên Nhóm Tin.</span>
                            <span class="help-block" ng-show="frmAdd.txtName.$touched && frmAdd.txtName.$error.minlength">Tên Nhóm Tin Không Được Ngắn Hơn 2 Ký Tự.</span>
                            <span class="help-block" ng-show="frmAdd.txtName.$touched && frmAdd.txtName.$error.maxlength">Tên Nhóm Tin Không Được Dài Hơn 50 Ký Tự.</span>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="txtDescription" class="control-label">Mô Tả Ngắn:</label>
                            <input  tabindex="2"
                                    type="text"
                                    class="form-control" 
                                    id="txtDescription" 
                                    name="txtDescription" 
                                    placeholder="Nhập Mô Tả Ngắn" 
                                    ng-model="model.request.txtDescription"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                    maxlength="50">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal" tabindex="4">
                        <i class="fa fa-fw fa-ban" aria-hidden="true"></i>&nbsp;Đóng
                    </button>
                    <button ng-disabled="frmAdd.txtName.$error.$invalid" 
                            ng-click="addItem()"
                            tabindex="3" 
                            data-loading-text="<i class='fa fa-refresh fa-spin fa-fw'></i> Đang xử lý"
                            type="button" class="btn btn-primary" id="btnAdd" name="btnAdd">
                        <i class="fa fa-fw fa-plus" aria-hidden="true"></i>&nbsp;Thêm Mới
                    </button>
                </div>
            </div>
        </form>
      </div>
    </div>

    <!-- Modal -->
    <div id="modalUpdate" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <form class="form" class="form-inline" name="frmUpdate" id="frmUpdate" novalidate>
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-yellow">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-fw fa-pencil-square-o" aria-hidden="true"></i> Cập Nhật</h4>
                </div>
                <div class="modal-body">
                    <div class="col-xs-12">
                        <div class="form-group" ng-class="{ 'has-error' : frmAdd.txtName.$touched && frmAdd.txtName.$invalid && !frmAdd.txtName.$pristine }">
                            <label for="txtName" class="control-label">Tên Nhóm Tin:</label>
                            <input  tabindex="1"
                                    type="text"
                                    class="form-control" 
                                    id="txtName" 
                                    name="txtName" 
                                    placeholder="Nhập Tên Nhóm Tin" 
                                    ng-model="model.request.txtName"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                    ng-minlength="2"
                                    ng-maxlength="50"
                                    maxlength="50"
                                    required >
                            <span class="help-block" ng-show="frmAdd.txtName.$touched && frmAdd.txtName.$error.required && frmAdd.txtTagName.$invalid && !frmAdd.txtTagName.$pristine">Vui Lòng Nhập Tên Nhóm Tin.</span>
                            <span class="help-block" ng-show="frmAdd.txtName.$touched && frmAdd.txtName.$error.minlength">Tên Nhóm Tin Không Được Ngắn Hơn 2 Ký Tự.</span>
                            <span class="help-block" ng-show="frmAdd.txtName.$touched && frmAdd.txtName.$error.maxlength">Tên Nhóm Tin Không Được Dài Hơn 50 Ký Tự.</span>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="txtDescription" class="control-label">Mô Tả Ngắn:</label>
                            <input  tabindex="2"
                                    type="text"
                                    class="form-control" 
                                    id="txtDescription" 
                                    name="txtDescription" 
                                    placeholder="Nhập Mô Tả Ngắn" 
                                    ng-model="model.request.txtDescription"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                    maxlength="50">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal" tabindex="4">
                        <i class="fa fa-fw fa-ban" aria-hidden="true"></i>&nbsp;Đóng
                    </button>
                    <button ng-disabled="frmUpdate.txtTagName.$error.$invalid" 
                            ng-click="updateItem()"
                            tabindex="3" 
                            data-loading-text="<i class='fa fa-refresh fa-spin fa-fw'></i> Đang xử lý"
                            type="button" class="btn btn-warning" id="btnUpdate" name="btnUpdate">
                        <i class="fa fa-fw fa-pencil-square-o" aria-hidden="true"></i>&nbsp;Cập Nhật
                    </button>
                </div>
            </div>
        </form>
      </div>
    </div>

    <div class="col-lg-12 col-xs-12">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-fw fa-gavel" aria-hidden="true"></i> Danh Sách Nhóm Tin</h3>
                <div class="box-tools pull-right">
                <button id="sendMail" type="button" class="btn btn-primary" tabindex="4" data-toggle="modal" data-target="#modalAdd">
                    <i class="fa fa-fw fa-plus" aria-hidden="true"></i>
                    <span class="hidden-xs"> Thêm Mới</span>
                </button>
               </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                    <div class="row">
                        <div class="col-sm-12">
                            <table  name="listResult" 
                                    datatable="ng" 
                                    dt-options="dtOptions"
                                    dt-column-defs="dtColumnDefs"
                                    width="100%" 
                                    class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center" style="width:75px;" data-column-index="1" >Tùy Chọn</th>
                                        <th class="text-center" style="width:20%;"  data-column-index="2" >Hình Ảnh Nhóm Tin</th>
                                        <th class="text-center" style="width:auto;" data-column-index="3" >Tên Nhóm Tin</th>
                                        <th class="text-center" style="width:20%;" data-column-index="4" >Mô Tả</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr role="row" ng-repeat="item in model.datainit.dtgList">>
                                        <td class="text-center">
                                            <button type="button" 
                                                    ng-click="openUpdateItem(item)"  
                                                    class="btn btn-warning"><i class="fa fa-fw fa-edit"></i></button>
                                            <button type="button"
                                                    ng-click="removeItem(item)"  
                                                    data-loading-text="<i class='fa fa-refresh fa-spin fa-fw'></i>"
                                                    class="btn btn-danger"><i class="fa fa-fw fa-trash-o"></i></button>
                                        </td>
                                        <td>[[item.news_cate_thumbnail]]</td>
                                        <td>[[item.news_cate_name]]</td>
                                        <td>[[item.news_cate_description]]</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
            <!-- /.box-body -->

            <!-- Loading (remove the following to stop the loading)-->
            <div id="listReLoading" class="overlay">
                <i class="fa fa-fw fa-refresh fa-spin"></i>
            </div>
            <!-- end loading -->

        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->




</div>
@stop