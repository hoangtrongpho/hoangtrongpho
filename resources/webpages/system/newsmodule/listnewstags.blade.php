@extends('system.layouts.index')

@section('headTitle')
   <title>{{ Lang::get('messages.system_heade_title') }}</title>
@stop

@section('pageTitle')
   <h1>
        <i class="fa fa-fw fa-newspaper-o" aria-hidden="true"></i> Quản Trị Tin Tức
        <small>{{ Lang::get('messages.system_verion') }}</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{URL::to('/System/Dashboard')}}"><i class="fa fa-fw fa-dashboard"></i> Hệ Thống Quản Trị Website</a>
        </li>
        <li class="active"><i class="fa fa-fw fa-tags" aria-hidden="true"></i> Quản Trị Thẻ Tin Tức</li>
    </ol>
@stop

@section('headCss')
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-ui-switch/bootstrap-switch.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-toastr/angular-toastr.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/jquery.dataTables.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/dataTables.bootstrap.css')}}" />
@stop

@section('headJs')
    <!-- UI Bootstrap Modal --> 
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-bootstrap/ui-bootstrap-tpls-2.0.0.min.js')}}"></script>
    <!-- Angular Toastr -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-toastr/angular-toastr.tpls.js')}}"></script>
    <!-- Angular Confirm Dialog -->
    <script type="text/javascript" src="{{asset('public/system/plugins/bootbox/bootbox.min.js')}}"></script>
    <!-- Angular Datatables-->
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/angular-datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/colreorder/angular-datatables.colreorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/bootstrap/angular-datatables.bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/tabletools/angular-datatables.tabletools.min.js')}}"></script>
    <!-- JS -->
    <script type="text/javascript" src="{{asset('public/system/common/system.listnewstags.angurlar.js')}}"></script>
@stop

@section('container')
<div class="row" ng-app="newsTagsApp" ng-controller="newsTagsController" ng-init="pageInit()">

    <!-- Modal -->
    <div id="modalAdd" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <form class="form" class="form-inline" name="frmAdd" id="frmAdd" novalidate>
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-blue">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-fw fa-floppy-o" aria-hidden="true"></i> Thêm Thẻ Mới.</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="form-group" ng-class="{ 'has-error' : frmAdd.txtTagName.$invalid && !frmAdd.txtTagName.$pristine }">
                            <label for="txtTagName" class="control-label">Thẻ Tin Tức:</label>
                            <input  tabindex="1"
                                    type="text"
                                    class="form-control" 
                                    id="txtTagName" 
                                    name="txtTagName" 
                                    placeholder="Thẻ Tin Tức" 
                                    ng-model="model.request.txtTagName"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                    ng-minlength="2"
                                    ng-maxlength="50"
                                    maxlength="50"
                                    required >
                            <span class="help-block" ng-show="frmAdd.txtTagName.$error.required && frmAdd.txtTagName.$invalid && !frmAdd.txtTagName.$pristine">Vui Lòng Nhập Thẻ Tin Tức.</span>
                            <span class="help-block" ng-show="frmAdd.txtTagName.$error.minlength">Thẻ Tin Tức Không Được Ngắn Hơn 2 Ký Tự.</span>
                            <span class="help-block" ng-show="frmAdd.txtTagName.$error.maxlength">Thẻ Tin Tức Không Được Dài Hơn 50 Ký Tự.</span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal">
                        <i class="fa fa-fw fa-ban" aria-hidden="true"></i>&nbsp;Đóng
                    </button>
                    <button ng-disabled="frmAdd.txtTagName.$error.required 
                            || frmAdd.txtTagName.$error.minlength
                            || frmAdd.txtTagName.$error.maxlength" 
                            ng-click="addItem()"
                            type="submit" class="btn btn-primary" id="btnAdd" name="btnAdd">
                        <i class="fa fa-fw fa-plus" aria-hidden="true"></i>&nbsp;Thêm Mới
                    </button>
                </div>
            </div>
        </form>
      </div>
    </div>

    <!-- Modal -->
    <div id="modalUpdate" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <form class="form" class="form-inline" name="frmUpdate" id="frmUpdate" novalidate>
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-yellow">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-fw fa-pencil-square-o" aria-hidden="true"></i> Cập Nhật Thẻ.</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="form-group" ng-class="{ 'has-error' : frmUpdate.txtTagName.$invalid && !frmUpdate.txtTagName.$pristine }">
                            <label for="txtTagName" class="control-label">Thẻ Tin Tức:</label>
                            <input  tabindex="1"
                                    type="text"
                                    class="form-control" 
                                    id="txtTagName" 
                                    name="txtTagName" 
                                    placeholder="Thẻ Tin Tức" 
                                    ng-model="model.request.txtTagName"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                    ng-minlength="2"
                                    ng-maxlength="50"
                                    maxlength="50"
                                    required >
                            <span class="help-block" ng-show="frmUpdate.txtTagName.$error.required && frmUpdate.txtTagName.$invalid && !frmUpdate.txtTagName.$pristine">Vui Lòng Nhập Thẻ Tin Tức.</span>
                            <span class="help-block" ng-show="frmUpdate.txtTagName.$error.minlength">Thẻ Tin Tức Không Được Ngắn Hơn 2 Ký Tự.</span>
                            <span class="help-block" ng-show="frmUpdate.txtTagName.$error.maxlength">Thẻ Tin Tức Không Được Dài Hơn 50 Ký Tự.</span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal">
                        <i class="fa fa-fw fa-ban" aria-hidden="true"></i>&nbsp;Đóng
                    </button>
                    <button ng-disabled="frmUpdate.txtTagName.$error.required 
                            || frmUpdate.txtTagName.$error.minlength
                            || frmUpdate.txtTagName.$error.maxlength" 
                            ng-click="updateItem()"
                            type="submit" class="btn btn-warning" id="btnUpdate" name="btnUpdate">
                        <i class="fa fa-fw fa-pencil-square-o" aria-hidden="true"></i>&nbsp;Cập Nhật
                    </button>
                </div>
            </div>
        </form>
      </div>
    </div>

    <div class="col-lg-12 col-xs-12">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-fw fa-tags" aria-hidden="true"></i>&nbsp;Danh Sách Thẻ Tin Tức</h3>
                <div class="box-tools pull-right">
                <button id="sendMail" type="button" class="btn btn-primary" tabindex="4" data-toggle="modal" data-target="#modalAdd">
                    <i class="fa fa-fw fa-plus" aria-hidden="true"></i>&nbsp;Thêm Thẻ
                </button>

               </div>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
                <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                    <div class="row">
                        <div class="col-sm-12">
                            <table  name="listResult" 
                                    datatable="ng" 
                                    dt-options="dtOptions"
                                    dt-column-defs="dtColumnDefs"
                                    width="100%" 
                                    class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center" style="width:15%;" data-column-index="0" >Tùy Chọn</th>
                                        <th class="text-center" style="width:45%;" data-column-index="1" >Tên Thẻ</th>
                                        <th class="text-center" style="width:20%;" data-column-index="2" >Số Lượng Bài</th>
                                        <th class="text-center" style="width:20%;" data-column-index="3" >Ngày Cập Nhật</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr role="row" ng-repeat="tags in model.datainit.dtgListTags">
                                        <td class="text-center">
                                            <button type="button" ng-click="openUpdateItem(tags)" class="btn btn-warning"><i class="fa fa-fw fa-edit"></i></button>
                                            <button type="button" ng-click="removeItem(tags.tag_id)" class="btn btn-danger"><i class="fa fa-fw fa-trash-o"></i></button>
                                        </td>
                                        <td>[[tags.tag_name]]</td>
                                        <td class="text-center">[[tags.news_couter]]</td>
                                        <td class="text-center">[[tags.updated_date | date:'dd/MM/yyyy HH:mm:ss']]</td>
                                        
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
            <!-- /.box-body -->

            <!-- Loading (remove the following to stop the loading)-->
            <div id="listReLoading" class="overlay">
                <i class="fa fa-fw fa-refresh fa-spin"></i>
            </div>
            <!-- end loading -->

        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->




</div>
@stop