@extends('system.layouts.index')

@section('headTitle')
   <title>Quản Trị Tin Tức - {{ Lang::get('messages.system_heade_title') }}</title>
@stop

@section('pageTitle')
   <h1>
        <i class="fa fa-fw fa-newspaper-o" aria-hidden="true"></i> Quản Trị Tin Tức
        <small>Skyfire Sytem Version 1.0</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{URL::to('/System/Dashboard')}}"><i class="fa fa-fw fa-dashboard"></i> Hệ Thống Quản Trị Website</a>
        </li>
        <li class="active"><i class="fa fa-fw fa-newspaper-o" aria-hidden="true"></i> Danh Sách Tin Tức</li>
    </ol>
@stop

@section('headCss')
<!-- <link rel="stylesheet" href="{{asset('public/system/css/datetimepicker.css')}}"/> -->
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/daterangepicker/daterangepicker.css')}}"/>
<!-- <link rel="stylesheet" href="{{asset('public/system/plugins/angular-ui-switch/angular-ui-switch.css')}}"/> -->
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-ui-switch/bootstrap-switch.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-multi-select/isteven-multi-select.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-toastr/angular-toastr.css')}}" />

<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/jquery.dataTables.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/dataTables.bootstrap.css')}}" />

<style type="text/css">
    .control-label{
        padding-top: 5px;
        padding-right: 5px !important;
        padding-left: 5px !important;
        float: left;
    }

    #listResult {
        width: 100% !important;
    }
</style>
@stop

@section('headJs')
    <!-- Date Time Picker -->
    <script type="text/javascript" src="{{asset('public/system/plugins/daterangepicker/daterangepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-daterangepicker/angular-daterangepicker.min.js')}}"></script>
    <!-- <script type="text/javascript" src="{{asset('public/system/plugins/datepicker/locales/bootstrap-datepicker.vi.js')}}"></script> -->
    <!-- <script type="text/javascript" src="{{asset('public/system/js/datetimepicker.js')}}"></script> -->
    <!-- <script type="text/javascript" src="{{asset('public/system/js/datetimepicker.templates.js')}}"></script> -->

    <!-- <script src="{{asset('public/system/plugins/angular-ui-switch/angular-ui-switch.js')}}"></script> -->
    <script src="{{asset('public/system/plugins/angular-ui-switch/bootstrap-switch.js')}}"></script>
    <script src="{{asset('public/system/plugins/angular-ui-switch/angular-bootstrap-switch.js')}}"></script>

    <!-- iCheck -->
    <script src="{{asset('public/system/plugins/angular_bootstrap_checkbox/angular-bootstrap-checkbox.js')}}"></script>

    <!-- File Manager -->
    <!-- Angular Multi Select -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-multi-select/isteven-multi-select.js')}}"></script>

    <!-- UI Bootstrap Modal --> 
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-bootstrap/ui-bootstrap-tpls-2.0.0.min.js')}}"></script>

    <!-- Angular Toastr -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-toastr/angular-toastr.tpls.js')}}"></script>

    <!-- Angular Confirm Dialog -->
    <script type="text/javascript" src="{{asset('public/system/plugins/bootbox/bootbox.min.js')}}"></script>

    <!-- Angular Datatables-->
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/angular-datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/colreorder/angular-datatables.colreorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/bootstrap/angular-datatables.bootstrap.min.js')}}"></script>

    <!-- JS -->
    <script type="text/javascript" src="{{asset('public/system/common/system.common.angurlar.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/common/system.listnews.angurlar.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function() { 
            $(window).on('load', function() {  
            });


            //Minify Left Menu
            //$("body").addClass("sidebar-collapse");
        });
    </script>
@stop

@section('container')
<div class="row" ng-app="newsNewsApp" ng-controller="newsNewsController" ng-init="pageInit()">

    <div class="col-lg-12">
        <div class="box box-success collapsed-box">
            <div class="box-header" data-widget="collapse" style="cursor: pointer;">
                <h3 class="box-title"><i class="fa fa-fw fa-sliders" aria-hidden="true"></i> Tìm Kiếm Nâng Cao</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-fw fa-minus"></i>
                  </button>
               </div>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
                <form class="form" name="frmSearch" id="frmSearch" novalidate>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">

                                <div class="form-group" ng-class="{ 'has-error' : frmAddNewsCate.txtNewsName.$invalid && !frmAddNewsCate.txtNewsName.$pristine }">
                                    <label for="txtNewsName" class="control-label col-md-1 text-right">Tiêu đề:</label>
                                    <div class="col-md-3">
                                        <input  type="text"
                                                class="form-control" 
                                                id="txtNewsName" 
                                                name="txtNewsName" 
                                                placeholder="Nhập Tiêu Đề Bản Tin" 
                                                ng-model="model.request.txtNewsName"
                                                autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                                ng-maxlength="100"
                                                maxlength="100"
                                                required >
                                        <span class="help-block" ng-show="frmAddNewsCate.txtNewsName.$error.maxlength">Tiêu Đề Không Được Dài Hơn 100 Ký Tự.</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="slbCateParent" class="control-label col-md-1 text-right">Danh Mục:</label>
                                    <div class="col-md-3">
                                        <isteven-multi-select
                                            name="slbCateParent"
                                            input-model="model.datainit.slbListCategories"
                                            output-model="model.request.slbCateParent"
                                            helper-elements="all filter reset"
                                            button-label="news_cate_name"
                                            item-label="news_cate_name"
                                            output-properties="news_cate_id"
                                            search-property="news_cate_name"
                                            tick-property="ticked"
                                            orientation="vertical"
                                            disable-property="false"
                                            is-disabled="false"
                                            selection-mode="multiple"
                                            min-search-length="10"
                                            translation="model.localLang"
                                            max-labels="1"  
                                            directive-id="slbCateParent"
                                            on-reset="slbClear()"
                                            on-item-click="slbItemClick(data)">
                                        </isteven-multi-select>
                                    </div>
                                    <input  type="text"
                                            class="form-control" 
                                            id="txtCateParent" 
                                            name="txtCateParent" 
                                            ng-model="model.request.txtCateParent"
                                            autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style="display:none;" 
                                            required >
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row" style="margin-top: 3px;">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="form-group pull-right">
                                    <div class="col-md-3">
                                        <button type="button" 
                                                ng-click="loadNewsList()"
                                                class="btn btn-success">
                                            <i class="fa fa-fw fa-search" aria-hidden="true"></i>
                                            <span>Lọc Danh Sách</span>
                                        </button>
                                    </div>
                                </div>
                            
                            </div>
                        </div>
                    </div>

                </form>
                

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->

    <div class="col-lg-12">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-fw fa-list" aria-hidden="true"></i> Danh Sách Tin Tức</h3>
                <div class="box-tools pull-right">
                    <a class="btn btn-primary" href="{{URL::to('System/CreateNews')}}" title="CreateNews">
                        <i class="fa fa-fw fa-plus" aria-hidden="true"></i>
                        <span>Thêm Mới</span>
                    </a>
                </div>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <table  name="listResult" 
                                id="listResult" 
                                style="width: 100%;" 
                                datatable="ng" 
                                dt-options="dtOptions"
                                dt-column-defs="dtColumnDefs"
                                class="table table-bordered table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th class="text-center" data-column-index="0" style="width: 80px;">Tùy Chọn</th>
                                    <th class="text-center" data-column-index="1" style="width: auto;">Hình Ảnh</th>
                                    <th class="text-center" data-column-index="1" style="width: auto;">Tiêu Đề</th>
                                    <th class="text-center" data-column-index="2" style="width: 15%;">Loại</th>
                                    <th class="text-center" data-column-index="3" style="width: 10%;">Ghim</th>
                                    <th class="text-center" data-column-index="3" style="width: 10%;">Trạng Thái</th>
                                    <th class="text-center" data-column-index="4" style="width: 15%;">Lượt Xem</th>
                                    <th class="text-center" data-column-index="5" style="width: 20%;">Ngày Đăng</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr role="row" ng-repeat="news in model.datainit.dtgListNews">
                                    <td class="text-center">
                                        <a href="{{URL::to('System/UpdateNews/[[news.news_slug]]')}}" class="btn btn-warning btn-xs" title="[[news.news_title]]"><i class="fa fa-fw fa-edit fa-fw" aria-hidden="true"></i></a>

                                        <button ng-show="news.status == 1 && news.news_cate_id != 1" type="button" ng-click="changeStatus(news.news_id, 0)" class="btn btn-danger btn-xs"><i class="fa fa-fw fa-eye-slash fa-fw" aria-hidden="true"></i></button>

                                        <button ng-show="news.status == 0 && news.news_cate_id != 1" type="button" ng-click="changeStatus(news.news_id, 1)" class="btn btn-success btn-xs"><i class="fa fa-fw fa-eye fa-fw" aria-hidden="true"></i></button>

                                        <button ng-show="news.news_cate_id != 1 " type="button" ng-click="removeItem(news.news_id)" class="btn btn-danger btn-xs"><i class="fa fa-fw fa-trash-o fa-fw"></i></button>
                                    </td>
                                    <td>
                                        <img ng-src="[[news.news_thumbnail]]"
                                         onError="this.src='{{asset('public/images/default-image.png')}}';"
                                         class="img-responsive img-thumbnail" 
                                         alt="Chọn Ảnh Đại Diện" 
                                         title="Chọn Ảnh Đại Diện" 
                                         height="150px">
                                    </td>
                                    <td>
                                        [[news.news_title | limitTo:50]]
                                    </td>
                                    <td>[[news.news_cate_name]]</td>
                                    <td class="text-center">
                                        <a ng-show="news.news_pinned == 1" role="button" class="btn btn-success disabled">Có</a>
                                        <a ng-show="news.news_pinned == 0" role="button" class="btn btn-danger disabled">&nbsp;&nbsp;Không &nbsp;</a>
                                    </td>
                                    <td class="text-center">
                                        <span style="display:none;">[[news.status]]</span>
                                        <a ng-show="news.status == 1" role="button" class="btn btn-success disabled">Hiện</a>
                                        <a ng-show="news.status == 0" role="button" class="btn btn-danger disabled">&nbsp;&nbsp;Ẩn&nbsp;</a>
                                    </td>
                                    <td class="text-center">[[news.views_couter]] Lượt</td>
                                    <td class="text-center">[[news.updated_date_format]]</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <!-- /.box-body -->

            <!-- Loading (remove the following to stop the loading)-->
            <div id="listReLoading" class="overlay">
                <i class="fa fa-fw fa-refresh fa-spin"></i>
            </div>
            <!-- end loading -->

        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
@stop