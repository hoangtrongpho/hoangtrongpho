<!DOCTYPE html>
<html lang="vi">
    <head>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

        @yield('headTitle')
        <meta charset="utf-8">
        <?php
            $metas = DB::select('SELECT setting_key, setting_values FROM system_settings WHERE setting_type = ? OR autoload = ?', ['meta',1]);
            $echoMetas = "";
            foreach ($metas as $item) 
            {
                $echoMetas .= '<meta name="'.$item->setting_key.'" content="'.$item->setting_values.'">';
            }
            echo $echoMetas;
        ?>
        <meta system_id="skyfire_management_system" system_code="_AJYKd+gci@#_+tuvwfw&*gh6kXAWETJCeiohohBt!@#uvwxQR+ghijEN" system_content="{{ csrf_token() }}" system_key="CD78&^&zABCOPQR+ghijENOPQR$s_+tuvwfw&*gh6kXeiohohBt!@#uvwxy$%^&zABCD78&^&zABC*OPQR+ghijENOPQR$^YZ" name="system-secret-code" >
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <!-- Mobile viewport optimization h5bp.com/ad -->
        <meta name="HandheldFriendly" content="True">
        <meta name="MobileOptimized" content="320">
        <!-- Mobile IE allows us to activate ClearType technology for smoothing fonts for easy reading -->
        <meta http-equiv="cleartype" content="on">
        <!-- Shortcut Icon -->
        <link rel="shortcut icon" type="images/vnd.microsoft.icon" href="{{ asset('favicon.png')}}" />
        <!-- For third generation iPad Retina Display -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('favicon.png')}}" />
        <!-- For iPhone 4 with high-resolution Retina display: -->
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('favicon.png')}}" />
        <!-- For first-generation iPad: -->
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('favicon.png')}}" />
        <!-- For non-Retina iPhone, iPod Touch, and Android 2.1+ devices: -->
        <link rel="apple-touch-icon-precomposed" href="{{ asset('favicon.png')}}" />
        <!-- For nokia devices: -->
        
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="{{asset('public/css/bootstrap/bootstrap.min.css').'?v='.Carbon\Carbon::now()->timestamp}}">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{asset('public/css/fontawesome/font-awesome.min.css').'?v='.Carbon\Carbon::now()->timestamp}}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{asset('public/system/css/AdminLTE/AdminLTE.min.css').'?v='.Carbon\Carbon::now()->timestamp}}">
        <!-- Pace style -->
        <link rel="stylesheet" href="{{asset('public/system/plugins/pace/pace.css').'?v='.Carbon\Carbon::now()->timestamp}}">
        <!-- iCheck -->
        <link rel="stylesheet" href="{{asset('public/system/plugins/iCheck/flat/yellow.css').'?v='.Carbon\Carbon::now()->timestamp}}">

        @yield('headCss')

        <style type="text/css">
            .loader-wrapper {
                position: fixed;
                top: 0px;
                left: 0px;
                width: 100%;
                height: 100%;
                z-index: 999999998;
                background-color: rgba(0,0,0,0.3);
                position: fixed;
                transition: all 0.5s ease-in-out;
                -moz-transition: all 0.5s ease-in-out;
                -o-transition: all 0.5s ease-in-out;
                -webkit-transition: all 0.5s ease-in-out;
                overflow: hidden;
            }
            #loader {
                display: block;
                position: relative;
                left: 52%;
                top: 50%;
                width: 100px;
                height: 100px;
                margin: -75px 0 0 -75px;
                border-radius: 50%;
                border: 4px solid transparent;
                border-top-color: #3498db;
                -webkit-animation: spin 1.5s linear infinite; /* Chrome, Opera 15+, Safari 5+ */
                animation: spin 1.5s linear infinite; /* Chrome, Firefox 16+, IE 10+, Opera */
            }
             
            #loader:before {
                content: "";
                position: absolute;
                top: 5px;
                left: 5px;
                right: 5px;
                bottom: 5px;
                border-radius: 50%;
                border: 4px solid transparent;
                border-top-color: #e74c3c;
                -webkit-animation: spin 2s linear infinite; /* Chrome, Opera 15+, Safari 5+ */
                  animation: spin 2s linear infinite; /* Chrome, Firefox 16+, IE 10+, Opera */
            }
             
            #loader:after {
                content: "";
                position: absolute;
                top: 15px;
                left: 15px;
                right: 15px;
                bottom: 15px;
                border-radius: 50%;
                border: 4px solid transparent;
                border-top-color: #f9c922;
                -webkit-animation: spin 1s linear infinite; /* Chrome, Opera 15+, Safari 5+ */
                  animation: spin 1s linear infinite; /* Chrome, Firefox 16+, IE 10+, Opera */
            }
             
            @-webkit-keyframes spin {
                0%   {
                    -webkit-transform: rotate(0deg);  /* Chrome, Opera 15+, Safari 3.1+ */
                    -ms-transform: rotate(0deg);  /* IE 9 */
                    transform: rotate(0deg);  /* Firefox 16+, IE 10+, Opera */
                }
                25%   {
                    -webkit-transform: rotate(90deg);  /* Chrome, Opera 15+, Safari 3.1+ */
                    -ms-transform: rotate(90deg);  /* IE 9 */
                    transform: rotate(90deg);  /* Firefox 16+, IE 10+, Opera */
                }
                50%   {
                    -webkit-transform: rotate(180deg);  /* Chrome, Opera 15+, Safari 3.1+ */
                    -ms-transform: rotate(180deg);  /* IE 9 */
                    transform: rotate(180deg);  /* Firefox 16+, IE 10+, Opera */
                }
                75%   {
                    -webkit-transform: rotate(270deg);  /* Chrome, Opera 15+, Safari 3.1+ */
                    -ms-transform: rotate(270deg);  /* IE 9 */
                    transform: rotate(270deg);  /* Firefox 16+, IE 10+, Opera */
                }
                100% {
                    -webkit-transform: rotate(360deg);  /* Chrome, Opera 15+, Safari 3.1+ */
                    -ms-transform: rotate(360deg);  /* IE 9 */
                    transform: rotate(360deg);  /* Firefox 16+, IE 10+, Opera */
                }
            }
            @keyframes spin {
                0%   {
                    -webkit-transform: rotate(0deg);  /* Chrome, Opera 15+, Safari 3.1+ */
                    -ms-transform: rotate(0deg);  /* IE 9 */
                    transform: rotate(0deg);  /* Firefox 16+, IE 10+, Opera */
                }
                25%   {
                    -webkit-transform: rotate(90deg);  /* Chrome, Opera 15+, Safari 3.1+ */
                    -ms-transform: rotate(90deg);  /* IE 9 */
                    transform: rotate(90deg);  /* Firefox 16+, IE 10+, Opera */
                }
                50%   {
                    -webkit-transform: rotate(180deg);  /* Chrome, Opera 15+, Safari 3.1+ */
                    -ms-transform: rotate(180deg);  /* IE 9 */
                    transform: rotate(180deg);  /* Firefox 16+, IE 10+, Opera */
                }
                75%   {
                    -webkit-transform: rotate(270deg);  /* Chrome, Opera 15+, Safari 3.1+ */
                    -ms-transform: rotate(270deg);  /* IE 9 */
                    transform: rotate(270deg);  /* Firefox 16+, IE 10+, Opera */
                }
                100% {
                    -webkit-transform: rotate(360deg);  /* Chrome, Opera 15+, Safari 3.1+ */
                    -ms-transform: rotate(360deg);  /* IE 9 */
                    transform: rotate(360deg);  /* Firefox 16+, IE 10+, Opera */
                }
            }
        </style>
    </head>

    <body class="hold-transition login-page" style="overflow-y: hidden;">

        <div class="loader-wrapper" id="pagePreLoading">
            <div id="loader"></div>
        </div>

        @yield('container')

        <!-- jQuery 3.1.1 -->
        <script type="text/javascript" src="{{asset('public/js/jquery/jquery-3.1.1.min.js').'?v='.Carbon\Carbon::now()->timestamp}}"></script>
        <!-- jQuery UI 1.12.1 -->
        <script  type="text/javascript" src="{{asset('public/js/jqueryui/jquery-ui.min.js').'?v='.Carbon\Carbon::now()->timestamp}}"></script>
        
        <?php 
            try
            {
                $fileLangJs = public_path().'/system/lang/'.App::getLocale().'.js';
                $languages = DB::select('SELECT lang_key,lang_value FROM system_languages WHERE lang_type = ? AND lang_kind = ? AND lang_page = ? AND status = ?', 
                                        [App::getLocale(),'js','system',9]);
                $resultLang = '"use strict";var Languages={};$(document).ready(function(){Languages={';
                $i = 0;
                $numItems = count($languages);
                foreach ($languages as $row) 
                {
                    $resultLang .= "'".$row->lang_key."':'".$row->lang_value."',";

                    if(++$i === $numItems) {
                        $resultLang .= "'".$row->lang_key."':'".$row->lang_value."'";
                    }
                }
                $resultLang .= "};});";
                if(!file_exists($fileLangJs))
                {
                    file_put_contents($fileLangJs, '');
                }
                $currentJs = file_get_contents($fileLangJs);
                if($resultLang !== $currentJs)
                {
                    file_put_contents($fileLangJs, $resultLang);
                }
                echo '<!-- Languages Common System -->';
                echo '<script type="text/javascript" src="'.asset('public/system/lang/'.App::getLocale().'.js').'?v='.Carbon\Carbon::now()->timestamp.'"></script>';
            }
            catch(\Exception $e)
            {
                echo '<!-- Default Languages Common System -->';
                echo '<script type="text/javascript" src="'.asset('public/system/lang/default.js').'?v='.Carbon\Carbon::now()->timestamp.'"></script>';
            }
        ?>

        <!-- Bootstrap 3.3.7 -->
        <script type="text/javascript" src="{{asset('public/js/bootstrap/bootstrap.min.js').'?v='.Carbon\Carbon::now()->timestamp}}"></script>

        <!-- Angurlarjs 1.6.2 -->
        <script type="text/javascript" src="{{asset('public/js/angularjs/angular.min.js').'?v='.Carbon\Carbon::now()->timestamp}}"></script>
        <script type="text/javascript" src="{{asset('public/js/angularjs/angular-sanitize.min.js').'?v='.Carbon\Carbon::now()->timestamp}}"></script>
        <script type="text/javascript" src="{{asset('public/js/angularjs/angular-route.min.js').'?v='.Carbon\Carbon::now()->timestamp}}"></script>
        <script type="text/javascript" src="{{asset('public/js/angularjs/angular-cookies.min.js').'?v='.Carbon\Carbon::now()->timestamp}}"></script>
        <script type="text/javascript" src="{{asset('public/js/angularjs/angular-animate.min.js').'?v='.Carbon\Carbon::now()->timestamp}}"></script>

        <!-- iCheck -->
        <script type="text/javascript" src="{{asset('public/system/plugins/angular_bootstrap_checkbox/angular-bootstrap-checkbox.js').'?v='.Carbon\Carbon::now()->timestamp}}"></script>

        <!-- PACE -->
        <script type="text/javascript" data-pace-options='{ "ajax": true }' src="{{asset('public/system/plugins/pace/pace.js').'?v='.Carbon\Carbon::now()->timestamp}}"></script>

        <!-- Special Script For Page -->
        @yield('headJs')

        
    </body>
</html>