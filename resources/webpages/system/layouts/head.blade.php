<meta charset="utf-8">

<meta name="auth_token" content="{{ csrf_token() }}">
<!-- <meta http-equiv="REFRESH" CONTENT="5"> -->
<?php
    $metas = DB::select('SELECT setting_key, setting_values FROM system_settings WHERE setting_type = ? OR autoload = ?', ['meta',1]);
    $echoMetas = "";
    foreach ($metas as $item) 
    {
        $echoMetas .= '<meta name="'.$item->setting_key.'" content="'.$item->setting_values.'">';
    }
    echo $echoMetas;
?>
<meta system_id="skyfire_management_system" system_code="_AJYKd+gci@#_+tuvwfw&*gh6kXAWETJCeiohohBt!@#uvwxQR+ghijEN" system_content="{{ csrf_token() }}" system_key="CD78&^&zABCOPQR+ghijENOPQR$s_+tuvwfw&*gh6kXeiohohBt!@#uvwxy$%^&zABCD78&^&zABC*OPQR+ghijENOPQR$^YZ" name="system-secret-code" >
<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<!-- Mobile viewport optimization h5bp.com/ad -->
<meta name="HandheldFriendly" content="True" />
<meta name="MobileOptimized" content="320" />
<!-- Mobile IE allows us to activate ClearType technology for smoothing fonts for easy reading -->
<meta http-equiv="cleartype" content="on" />
<!-- Shortcut Icon -->
<link rel="shortcut icon" type="images/vnd.microsoft.icon" href="{{ asset('favicon.png')}}" />
<!-- For third generation iPad Retina Display -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('favicon.png')}}" />
<!-- For iPhone 4 with high-resolution Retina display: -->
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('favicon.png')}}" />
<!-- For first-generation iPad: -->
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('favicon.png')}}" />
<!-- For non-Retina iPhone, iPod Touch, and Android 2.1+ devices: -->
<link rel="apple-touch-icon-precomposed" href="{{ asset('favicon.png')}}" />
<!-- For nokia devices: -->

<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet" href="{{asset('public/css/bootstrap/bootstrap.min.css').'?v='.Carbon\Carbon::now()->timestamp}}">
<!-- Font Awesome -->
<link rel="stylesheet" href="{{asset('public/css/fontawesome/font-awesome.min.css').'?v='.Carbon\Carbon::now()->timestamp}}">
<!-- Material Design Icons -->
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<!-- Ionicons -->
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css?v={{Carbon\Carbon::now()->timestamp}}"> -->
<!-- Theme style -->
<link rel="stylesheet" href="{{asset('public/system/css/AdminLTE/AdminLTE.min.css').'?v='.Carbon\Carbon::now()->timestamp}}">
<!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="{{asset('public/system/css/AdminLTE/skins/_all-skins.css').'?v='.Carbon\Carbon::now()->timestamp}}">
<!-- Pace style -->
<link rel="stylesheet" href="{{asset('public/system/plugins/pace/pace.min.css').'?v='.Carbon\Carbon::now()->timestamp}}">

<link rel="stylesheet" href="{{asset('public/system/css/global-system.css').'?v='.Carbon\Carbon::now()->timestamp}}">