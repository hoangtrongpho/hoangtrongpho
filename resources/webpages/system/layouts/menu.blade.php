<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar" id="leftMenuApp" style="background-color: black">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar" ng-controller="leftMenuController">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                @if(!$userAuth->user_avatar)
                    @if($userAuth->user_sex == 0)
                        <img src="{{asset('public\images\account\avatar1.png')}}" class="img-circle" alt="userLogo" title="userMaleLogo">
                    @elseif($userAuth->user_sex == 1)
                        <img src="{{asset('public\images\account\avatar2.png')}}" class="img-circle" alt="userLogo" title="userFemaleLogo">
                    @endif
                @else
                        <img src="{{asset($userAuth->user_avatar)}}" class="img-circle" alt="{{ $userAuth->user_fullname }}" title="{{ $userAuth->user_fullname }}">
                @endif
            </div>
            <div class="pull-left info">
                <a href="{{URL::to('System/Profile')}}" title="{{ $userAuth->user_fullname }} - {{URL::to('System/Profile')}}">
                    <strong>{{ $userAuth->user_fullname }}</strong>
                    <p style="font-weight: normal;margin-top: 1px;">{{ $userAuth->auth_name }}</p>
                    <i class="fa fa-fw fa-circle text-success"></i> Online
                </a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form" style="display: none;">
            <div class="input-group">
                <input  type="text" 
                        name="q" 
                        class="form-control" 
                        placeholder="Tìm Chức Năng..."
                        autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                        ng-model="model.request.txtKeyword">
                <span class="input-group-btn">
                    <button type="button" name="search" id="search-btn" class="btn btn-flat" readonly>
                        <i class="fa fa-fw fa-search"></i>
                    </button>
                    <button type="button" 
                            ng-click="clearKeyWord()"
                            name="clearKeyWord" 
                            id="clearKeyWord" 
                            class="btn btn-flat" 
                            style="display:none;">
                        <i class="fa fa-fw fa-times" aria-hidden="true"></i>
                    </button>
                </span>
            </div>
        </form>
        <ul class="sidebar-menu" id="resultFunctionSearch" style="display:none;">
            <li class="header"><i class="fa fa-fw fa-newspaper-o" aria-hidden="true"></i> Chức Năng Phù Hợp...</li>
            <li ng-repeat="func in model.datainit.listFunctions | searchFunctions:model.request.txtKeyword">
                <a href="{{URL::to($currentLocale)}}[[func.url]]">
                    <i class="fa fa-fw [[func.icon]]"></i>
                    <span>[[func.title]]</span>
                </a>
            </li>
        </ul>
        <!-- /.search form -->

        @if($userAuth->auth_id == 1)
            <ul class="sidebar-menu" id="menuFunction" style="display:block;">
                <!-- ====================Dashboard=============== -->
                <li class="itemMenu {{ $page == 'Dashboard' || $page == 'Dashboard2' ? 'active' : '' }}">
                    <a href="{{URL::to($currentLocale.'/System/Dashboard2')}}">
                        <i class="fa fa-fw fa-tachometer" aria-hidden="true"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <!--  ============Hệ Thống Thương Mại Điện Tử=============== -->
                <li class="header" style="background-color: lightcyan"><i class="fa fa-fw fa-shopping-cart" aria-hidden="true"></i> Chức Năng Chính</li>
                <li class="treeview {{ $page == 'ListProducts' 
                                    || $page == 'CreateProduct' 
                                    || $page == 'UpdateProduct'
                                    || $page == 'ListProductUnits' 
                                    || $page == 'ListProductColors' 
                                    || $page == 'ListProductOrigins' 
                                    || $page == 'ListProductCategories' 
                                    || $page == 'ListProductTags' ? 'active' : '' }}">
                    <a href="#">
                        <i class="fa fa-fw fa-archive" aria-hidden="true"></i>
                        <span>Quản Lý Sản Phẩm</span>
                        <span class="pull-right-container">
                            <i class="fa fa-fw fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul @if($page == 'ListProducts' 
                        || $page == 'CreateProduct' 
                        || $page == 'UpdateProduct'
                        || $page == 'ListProductUnits'
                        || $page == 'ListProductColors' 
                        || $page == 'ListProductOrigins' 
                        || $page == 'ListProductCategories' 
                        || $page == 'ListProductTags'
                        || $page == 'ListManufacturers' )
                            class="treeview-menu menu-open" style="display: block;"
                        @else
                            class="treeview-menu" style="display: none;"
                        @endif
                        >

                        <li class="itemMenu {{ $page == 'ListProducts' ||  $page == 'CreateProduct' ||  $page == 'UpdateProduct' ? 'active' : '' }}">
                            <a href="{{URL::to($currentLocale.'/System/ListProducts')}}">
                                <i class="fa fa-fw fa-star" aria-hidden="true"></i>
                                <span>Quản Lý Sản Phẩm</span>
                            </a>
                        </li>
                        
                        <li class="itemMenu {{ $page == 'ListProductCategories' ? 'active' : '' }}">
                            <a href="{{URL::to($currentLocale.'/System/ListProductCategories')}}">
                                <i class="fa fa-fw fa-cubes" aria-hidden="true"></i>
                                <span>Nhóm Sản Phẩm</span>
                            </a>
                        </li>

                        <li class="itemMenu {{ $page == 'ListManufacturers' ? 'active' : '' }}">
                            <a href="{{URL::to($currentLocale.'/System/ListManufacturers')}}">
                                <i class="fa fa-fw fa-industry" aria-hidden="true"></i>
                                <span>Nhà Sản Xuất</span>
                            </a>
                        </li>

                        <li class="itemMenu {{ $page == 'ListProductColors' ? 'active' : '' }}">
                            <a href="{{URL::to($currentLocale.'/System/ListProductColors')}}">
                                <i class="fa fa-fw fa-hashtag" aria-hidden="true"></i>
                                <span>Màu Sắc</span>
                            </a>
                        </li>
                        
                        <li class="itemMenu {{ $page == 'ListProductOrigins' ? 'active' : '' }}">
                            <a href="{{URL::to($currentLocale.'/System/ListProductOrigins')}}">
                                <i class="fa fa-fw fa-flag" aria-hidden="true"></i>
                                <span>Xuất Xứ</span>
                            </a>
                        </li>
                       
                        <li class="itemMenu {{ $page == 'ListProductTags' ? 'active' : '' }}">
                            <a href="{{URL::to($currentLocale.'/System/ListProductTags')}}">
                                <i class="fa fa-fw fa-tags" aria-hidden="true"></i>
                                <span>Thẻ Sản Phẩm</span>
                            </a>
                        </li>
                        
                        <li class="itemMenu {{ $page == 'ListProductUnits' ? 'active' : '' }}">
                            <a href="{{URL::to($currentLocale.'/System/ListProductUnits')}}">
                                <i class="fa fa-fw fa-gavel" aria-hidden="true"></i>
                                <span>Đơn Vị Sản Phẩm</span>
                            </a>
                        </li>

                    </ul>
                </li>
                <li class="{{ $page == 'ListInvoices' ||  $page == 'CreateInvoice' ||  $page == 'UpdateInvoice' ? 'active' : '' }}">
                    <a href="{{URL::to($currentLocale.'/System/ListInvoices')}}">
                        <i class="fa fa-fw fa-area-chart" aria-hidden="true"></i>
                        <span>Quản Trị Đơn Hàng</span>
                    </a>
                </li>
                <li class="itemMenu {{ $page == 'ListCustomers' ||  $page == 'CreateCustomer' ||  $page == 'UpdateCustomer' ||  $page == 'CreateCustomer' ? 'active' : '' }}">
                    <a href="{{URL::to($currentLocale.'/System/ListCustomers')}}">
                        <i class="fa fa-fw fa-users" aria-hidden="true"></i>
                        <span>Thông Tin Khách Hàng</span>
                    </a>
                </li>
               
                <!-- ===============Quản Trị Tin Tức============ -->
                <li class="header" style="background-color: lightcyan">
                    <i class="fa fa-fw fa-newspaper-o" aria-hidden="true"></i> 
                    Quản Trị Tin Tức
                </li>
                <li class="itemMenu {{ $page == 'ListNewsCategories' ? 'active' : '' }}">
                    <a href="{{URL::to($currentLocale.'/System/ListNewsCategories')}}">
                        <i class="fa fa-fw fa-bookmark" aria-hidden="true"></i>
                        <span>Nhóm Tin</span>
                    </a>
                </li>
                <li class="itemMenu {{ $page == 'ListNews' || $page == 'CreateNews' || $page == 'UpdateNews' ? 'active' : '' }}">
                    <a href="{{URL::to($currentLocale.'/System/ListNews')}}">
                        <i class="fa fa-fw fa-newspaper-o" aria-hidden="true"></i>
                        <span>Tin Tức</span>
                    </a>
                </li>
                 <!-- =================== Thống Kê =================== -->
                <li class="header" style="background-color: lightcyan">
                    <i class="fa fa-fw fa-area-chart" aria-hidden="true"></i> Thống Kê
                </li>
                <li class="itemMenu {{ $page == 'SaleReport' ? 'active' : '' }}">
                    <a href="{{URL::to($currentLocale.'/System/SaleReport')}}">
                        <i class="fa fa-fw fa-pie-chart" aria-hidden="true"></i>
                        <span>Doanh Số Hệ Thống</span>
                    </a>
                </li>
                <li class="itemMenu {{ $page == 'PrivateReport' ? 'active' : '' }}">
                    <a href="{{URL::to($currentLocale.'/System/PrivateReport')}}">
                        <i class="fa fa-fw fa-bar-chart" aria-hidden="true"></i>
                        <span>Doanh Số Cá Nhân</span>
                    </a>
                </li>
                <li class="itemMenu {{ $page == 'CustomerReport' ? 'active' : '' }}">
                    <a href="{{URL::to($currentLocale.'/System/CustomerReport')}}">
                        <i class="fa fa-fw fa-address-card" aria-hidden="true"></i>
                        <span>Khách Hàng </span>
                    </a>
                </li>
                <!-- =================Kho Hàng============== -->
                <li class="header" style="background-color: lightcyan">
                    <i class="fa fa-fw fa-database" aria-hidden="true"></i> Kho Hàng
                </li>
                <li  class="{{ $page == 'ImportExportStore' ? 'active' : '' }}">
                    <a href="{{URL::to($currentLocale.'/System/ImportExportStore')}}">
                        <i class="fa fa-fw fa-database" aria-hidden="true"></i>
                        <span>Xuất Nhập Kho</span>
                    </a>
                </li>
                <li  class="{{ $page == 'RemainStore' ? 'active' : '' }}">
                    <a href="{{URL::to($currentLocale.'/System/RemainStore')}}">
                        <i class="fa fa-fw fa-shopping-bag" aria-hidden="true"></i>
                        <span>Thống Kê Hàng Tồn</span>
                    </a>
                </li>
                <li  class="{{ $page == 'SaleStore' ? 'active' : '' }}">
                    <a href="{{URL::to($currentLocale.'/System/SaleStore')}}">
                        <i class="fa fa-fw fa-send" aria-hidden="true"></i>
                        <span>Thống Kê Hàng Xuất Bán</span>
                    </a>
                </li>
                <!-- =================Hệ Thống============== -->
                <li class="header" style="background-color: lightcyan">
                    <i class="fa fa-fw fa-tasks" aria-hidden="true"></i> Hệ Thống
                </li>
                <li  class="{{ $page == 'ListBranch' ? 'active' : '' }}">
                    <a href="{{URL::to($currentLocale.'/System/ListBranch')}}">
                        <i class="fa fa-fw fa-paw" aria-hidden="true"></i>
                        <span>Quản Trị Chi Nhánh</span>
                    </a>
                </li>
                <li class="{{ $page == 'SystemInformation' ? 'active' : '' }}">
                    <a href="{{URL::to($currentLocale.'/System/SystemInformation')}}">
                        <i class="fa fa-fw fa-ship" aria-hidden="true"></i>
                        <span>Thông Tin Công Ty</span>
                    </a>
                </li>
                <li class="itemMenu {{ $page == 'ListUsers' ? 'active' : '' }}">
                    <a href="{{URL::to($currentLocale.'/System/ListUsers')}}">
                        <i class="fa fa-fw fa-user-circle" aria-hidden="true"></i>
                        <span>Quản Trị Tài Khoản</span>
                    </a>
                </li>

            </ul>
        @else
            <ul class="sidebar-menu" id="menuFunction" style="display:block;">
                <!-- ====================Dashboard=============== -->
                <li class="itemMenu {{ $page == 'Dashboard' ? 'active' : '' }}">
                    <a href="{{URL::to($currentLocale.'/System/Dashboard')}}">
                        <i class="fa fa-fw fa-tachometer" aria-hidden="true"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <!--  ============Hệ Thống Thương Mại Điện Tử=============== -->
                <li class="header" style="background-color: lightcyan"><i class="fa fa-fw fa-shopping-cart" aria-hidden="true"></i> Hệ Thống Thương Mại Điện Tử</li>
                <li class="treeview {{ $page == 'ListProducts' 
                                    || $page == 'CreateProduct' 
                                    || $page == 'UpdateProduct'
                                    || $page == 'ListProductUnits' 
                                    || $page == 'ListProductColors' 
                                    || $page == 'ListProductOrigins' 
                                    || $page == 'ListProductCategories' 
                                    || $page == 'ListProductTags' ? 'active' : '' }}">
                    <a href="#">
                        <i class="fa fa-fw fa-archive" aria-hidden="true"></i>
                        <span>Quản Lý Sản Phẩm</span>
                        <span class="pull-right-container">
                            <i class="fa fa-fw fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul @if($page == 'ListProducts' 
                        || $page == 'CreateProduct' 
                        || $page == 'UpdateProduct'
                        || $page == 'ListProductUnits'
                        || $page == 'ListProductColors' 
                        || $page == 'ListProductOrigins' 
                        || $page == 'ListProductCategories' 
                        || $page == 'ListProductTags'
                        || $page == 'ListManufacturers' )
                            class="treeview-menu menu-open" style="display: block;"
                        @else
                            class="treeview-menu" style="display: none;"
                        @endif
                        >

                        <li class="itemMenu {{ $page == 'ListProducts' ||  $page == 'CreateProduct' ||  $page == 'UpdateProduct' ? 'active' : '' }}">
                            <a href="{{URL::to($currentLocale.'/System/ListProducts')}}">
                                <i class="fa fa-fw fa-star" aria-hidden="true"></i>
                                <span>Quản Lý Sản Phẩm</span>
                            </a>
                        </li>
                        
                        <li class="itemMenu {{ $page == 'ListProductCategories' ? 'active' : '' }}">
                            <a href="{{URL::to($currentLocale.'/System/ListProductCategories')}}">
                                <i class="fa fa-fw fa-cubes" aria-hidden="true"></i>
                                <span>Nhóm Sản Phẩm</span>
                            </a>
                        </li>

                        <li class="itemMenu {{ $page == 'ListManufacturers' ? 'active' : '' }}">
                            <a href="{{URL::to($currentLocale.'/System/ListManufacturers')}}">
                                <i class="fa fa-fw fa-industry" aria-hidden="true"></i>
                                <span>Nhà Sản Xuất</span>
                            </a>
                        </li>

                        <li class="itemMenu {{ $page == 'ListProductColors' ? 'active' : '' }}">
                            <a href="{{URL::to($currentLocale.'/System/ListProductColors')}}">
                                <i class="fa fa-fw fa-hashtag" aria-hidden="true"></i>
                                <span>Màu Sắc</span>
                            </a>
                        </li>
                        
                        <li class="itemMenu {{ $page == 'ListProductOrigins' ? 'active' : '' }}">
                            <a href="{{URL::to($currentLocale.'/System/ListProductOrigins')}}">
                                <i class="fa fa-fw fa-flag" aria-hidden="true"></i>
                                <span>Xuất Xứ</span>
                            </a>
                        </li>
                       
                        <li class="itemMenu {{ $page == 'ListProductTags' ? 'active' : '' }}">
                            <a href="{{URL::to($currentLocale.'/System/ListProductTags')}}">
                                <i class="fa fa-fw fa-tags" aria-hidden="true"></i>
                                <span>Thẻ Sản Phẩm</span>
                            </a>
                        </li>
                        
                        <li class="itemMenu {{ $page == 'ListProductUnits' ? 'active' : '' }}">
                            <a href="{{URL::to($currentLocale.'/System/ListProductUnits')}}">
                                <i class="fa fa-fw fa-gavel" aria-hidden="true"></i>
                                <span>Đơn Vị Sản Phẩm</span>
                            </a>
                        </li>

                    </ul>
                </li>
                <li class="{{ $page == 'ListPrivateInvoices' ||  $page == 'CreateInvoice' ||  $page == 'UpdateInvoice' ? 'active' : '' }}">
                    <a href="{{URL::to($currentLocale.'/System/ListPrivateInvoices')}}">
                        <i class="fa fa-fw fa-area-chart" aria-hidden="true"></i>
                        <span>Đơn Hàng Theo Chi Nhánh</span>
                    </a>
                </li>
                <li class="itemMenu {{ $page == 'ListCustomers' ||  $page == 'CreateCustomer' ||  $page == 'UpdateCustomer'  ||  $page == 'CreateCustomer' ? 'active' : '' }}">
                    <a href="{{URL::to($currentLocale.'/System/ListCustomers')}}">
                        <i class="fa fa-fw fa-users" aria-hidden="true"></i>
                        <span>Thông Tin Khách Hàng</span>
                    </a>
                </li>
                <!-- =================== Thống Kê =================== -->
                <li class="header" style="background-color: lightcyan">
                    <i class="fa fa-fw fa-area-chart" aria-hidden="true"></i> Thống Kê
                </li>
                <li class="itemMenu {{ $page == 'PrivateReport' ? 'active' : '' }}">
                    <a href="{{URL::to($currentLocale.'/System/PrivateReport')}}">
                        <i class="fa fa-fw fa-bar-chart" aria-hidden="true"></i>
                        <span>Doanh Số Cá Nhân</span>
                    </a>
                </li>
                <!-- ===============Quản Trị Nội Dung & Tin Tức============ -->
                <li class="header" style="background-color: lightcyan">
                    <i class="fa fa-fw fa-newspaper-o" aria-hidden="true"></i> 
                    Quản Trị Nội Dung & Tin Tức
                </li>
                <li class="itemMenu {{ $page == 'ListNewsCategories' ? 'active' : '' }}">
                    <a href="{{URL::to($currentLocale.'/System/ListNewsCategories')}}">
                        <i class="fa fa-fw fa-bookmark" aria-hidden="true"></i>
                        <span>Nhóm Thông Tin</span>
                    </a>
                </li>
                <li class="itemMenu {{ $page == 'ListNews' || $page == 'CreateNews' || $page == 'UpdateNews' ? 'active' : '' }}">
                    <a href="{{URL::to($currentLocale.'/System/ListNews')}}">
                        <i class="fa fa-fw fa-newspaper-o" aria-hidden="true"></i>
                        <span>Tin Tức</span>
                    </a>
                </li>
                <!-- =================Kho Hàng Chi Nhánh============== -->
                <li class="header" style="background-color: lightcyan">
                    <i class="fa fa-fw fa-database" aria-hidden="true"></i> Kho Hàng Chi Nhánh
                </li>
                <li  class="{{ $page == 'PrivateRemainStore' ? 'active' : '' }}">
                    <a href="{{URL::to($currentLocale.'/System/PrivateRemainStore')}}">
                        <i class="fa fa-fw fa-shopping-bag" aria-hidden="true"></i>
                        <span>Hàng Tồn Chi Nhánh</span>
                    </a>
                </li>
                <li  class="{{ $page == 'PrivateSaleStore' ? 'active' : '' }}">
                    <a href="{{URL::to($currentLocale.'/System/PrivateSaleStore')}}">
                        <i class="fa fa-fw fa-send" aria-hidden="true"></i>
                        <span>Hàng Xuất Bán Chi Nhánh</span>
                    </a>
                </li>
            </ul>
        @endif
    </section>
    <!-- /.sidebar -->
</aside>