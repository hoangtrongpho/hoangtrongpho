<!DOCTYPE html>
<html lang="vi">
<!--<![endif]-->
    <head>
        @yield('headTitle')

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        @include('system.layouts.head')

        @yield('headCss')
    </head>

    <body class="hold-transition skin-blue sidebar-mini" style="min-height: 700px;">

        <?php
        /*<div class="loader" id="pagePreLoading">
            <div><img src="{{asset('/public/images/loading.gif')}}" title="Loading..." alt="Loading..." /></div>
        </div>*/
        ?>
        

        <div class="wrapper">

            @include('system.layouts.header',['currentLocale' => "", 
                                              'userAuth' => Session::get('userAuth')])

            @include('system.layouts.menu',['currentLocale' => "", 
                                            'page' => Request::segment(1) == 'System' ? Request::segment(2) : Request::segment(3),
                                            'userAuth' => Session::get('userAuth')
                                           ])
        
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper" id="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header divBlur" id="main-content-header">
                    @yield('pageTitle')
                </section>
                <!--  /.Content Header -->
                <!-- Main content -->
                <section class="content divBlur" id="main-content">
                    @yield('container')
                </section>
                <!-- /.Main content -->

                <div class="loader-wrapper" id="pagePreLoading">
                    <div id="loader"></div>
                </div>  
  
            </div>
            <!-- /.content-wrapper -->

            @include('system.layouts.footer')
            
        </div>
        
        

        <!-- jQuery 3.1.1 -->
        <!-- <script type="text/javascript" src="{{asset('public/js/jquery/jquery-3.1.1.min.js').'?v='.Carbon\Carbon::now()->timestamp}}"></script> -->
        <script type="text/javascript" src="{{asset('public/js/jquery/jquery-2.2.4.js').'?v='.Carbon\Carbon::now()->timestamp}}"></script>
        <!-- jQuery UI 1.12.1 -->
        <script  type="text/javascript" src="{{asset('public/js/jqueryui/jquery-ui.min.js').'?v='.Carbon\Carbon::now()->timestamp}}"></script>

        <?php 
            try
            {
                $fileLangJs = public_path().'/system/lang/'.App::getLocale().'.js';
                $languages = DB::select('SELECT lang_key,lang_value FROM system_languages WHERE lang_type = ? AND lang_kind = ? AND lang_page = ? AND status = ?', 
                                        [App::getLocale(),'js','system',9]);
                $resultLang = '"use strict";var Languages={};$(document).ready(function(){Languages={';
                $i = 0;
                $numItems = count($languages);
                foreach ($languages as $row) 
                {
                    $resultLang .= "'".$row->lang_key."':'".$row->lang_value."',";

                    if(++$i === $numItems) {
                        $resultLang .= "'".$row->lang_key."':'".$row->lang_value."'";
                    }
                }
                $resultLang .= "};});";
                if(!file_exists($fileLangJs))
                {
                    file_put_contents($fileLangJs, '');
                }
                $currentJs = file_get_contents($fileLangJs);
                if($resultLang !== $currentJs)
                {
                    file_put_contents($fileLangJs, $resultLang);
                }
                echo '<!-- Languages Common System -->';
                echo '<script type="text/javascript" src="'.asset('public/system/lang/'.App::getLocale().'.js').'?v='.Carbon\Carbon::now()->timestamp.'"></script>';
            }
            catch(\Exception $e)
            {
                echo '<!-- Default Languages Common System -->';
                echo '<script type="text/javascript" src="'.asset('public/system/lang/default.js').'?v='.Carbon\Carbon::now()->timestamp.'"></script>';
            }
        ?>

        <!-- Tether 1.3.3 -->
        <script type="text/javascript" src="{{asset('public/js/tether/tether.min.js').'?v='.Carbon\Carbon::now()->timestamp}}"></script>
        
        <!-- Bootstrap 3.3.7 -->
        <script type="text/javascript" src="{{asset('public/js/bootstrap/bootstrap.min.js').'?v='.Carbon\Carbon::now()->timestamp}}"></script>

        <!-- Angurlarjs 1.6.2 -->
        <script type="text/javascript" src="{{asset('public/js/angularjs/angular.min.js').'?v='.Carbon\Carbon::now()->timestamp}}"></script>
        <script type="text/javascript" src="{{asset('public/js/angularjs/angular-sanitize.min.js').'?v='.Carbon\Carbon::now()->timestamp}}"></script>
        <script type="text/javascript" src="{{asset('public/js/angularjs/angular-route.min.js').'?v='.Carbon\Carbon::now()->timestamp}}"></script>
        <script type="text/javascript" src="{{asset('public/js/angularjs/angular-cookies.min.js').'?v='.Carbon\Carbon::now()->timestamp}}"></script>
        <script type="text/javascript" src="{{asset('public/js/angularjs/angular-animate.min.js').'?v='.Carbon\Carbon::now()->timestamp}}"></script>

        <!-- Momentjs 2.17.1 -->
        <script type="text/javascript" src="{{asset('public/system/plugins/momentjs/moment.js').'?v='.Carbon\Carbon::now()->timestamp}}"></script>
        <script type="text/javascript" src="{{asset('public/system/plugins/momentjs/vi.js').'?v='.Carbon\Carbon::now()->timestamp}}"></script>

        <!-- FastClick -->
        <script type="text/javascript" src="{{asset('public/system/plugins/fastclick/fastclick.js').'?v='.Carbon\Carbon::now()->timestamp}}"></script>

        <!-- PACE -->
        <script type="text/javascript" src="{{asset('public/system/plugins/pace/pace.js').'?v='.Carbon\Carbon::now()->timestamp}}"></script>

        <!-- SlimScroll 1.3.0 -->
        <!-- <script type="text/javascript" src="{{asset('public/system/plugins/slimScroll/jquery.slimscroll.min.js').'?v='.Carbon\Carbon::now()->timestamp}}"></script> -->

        <!-- AdminLTE App -->
        <script type="text/javascript" src="{{asset('public/system/js/AdminLTE/app.js').'?v='.Carbon\Carbon::now()->timestamp}}"></script>

        <!-- Angular Toastr -->
        <script type="text/javascript" src="{{asset('public/system/plugins/angular-toastr/angular-toastr.tpls.js').'?v='.Carbon\Carbon::now()->timestamp}}"></script>

        <!-- Angular Confirm Dialog -->
        <script type="text/javascript" src="{{asset('public/system/plugins/bootbox/bootbox.min.js').'?v='.Carbon\Carbon::now()->timestamp}}"></script>

        <!-- Moduel JS -->
        <script type="text/javascript" src="{{asset('public/system/common/system.common.angurlar.js').'?v='.Carbon\Carbon::now()->timestamp}}"></script>
        <script type="text/javascript" src="{{asset('public/system/common/system.common.leftmenu.angurlar.js').'?v='.Carbon\Carbon::now()->timestamp}}"></script>

        <!-- Special Script For Page -->
        @yield('headJs')
        <script type="text/javascript">
            $(document).ready(function() {

                //After All Content Rendered
                $(window).on('load', function() {  
                   // $("#pagePreLoading").hide();
                });

                // javascript:window.history.forward(1);

                angular.bootstrap($("#leftMenuApp"), ['leftMenuApp']);

                // Handler for .ready() called.
                // Pace.restart();

                

                // var leftWindowsID;

                // $("body").mouseleave(function() {
                //     leftWindowsID = window.setTimeout(function() {
                //         window.location.href = "{{URL::to('System/LockScreen')}}"
                //     }, 300000);
                //     // console.log("Mouse Leave");
                // })

                // $("body").mousemove(function( event ) {
                //     // var pageCoords = "( " + event.pageX + ", " + event.pageY + " )";
                //     // var clientCoords = "( " + event.clientX + ", " + event.clientY + " )";
                //     // console.log( "( event.pageX, event.pageY ) : " + pageCoords );
                //     // console.log( "( event.clientX, event.clientY ) : " + clientCoords );

                //     clearTimeout(leftWindowsID);
                //     // console.log("Mouse Move");
                // });

                // var checkCtrlQ=false;

                // $('*').keydown(function(e){
                //     if(e.keyCode=='17'){
                //         checkCtrlQ=true;
                //     }
                // }).keyup(function(ev){
                //     if(ev.keyCode=='17'){
                //         checkCtrlQ=false;
                //     }
                // }).keydown(function(event){
                //     if(checkCtrlQ){
                //         if(event.keyCode=='81'){
                //             window.location.href = "{{URL::to('System/LockScreen')}}"
                //             checkCtrlQ=false;
                //         }
                //     }
                // });
            });

        </script>
    </body>
</html>