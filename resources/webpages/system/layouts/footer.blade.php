<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>{{ Lang::get('messages.system_signature') }}</b>
    </div>
    <strong>Copyright &copy; 2016-2017 <a href="http://skyfiresolution.com/">Skyfire Team</a>.</strong> All rights reserved.
</footer>