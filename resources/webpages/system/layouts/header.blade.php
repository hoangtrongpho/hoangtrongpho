<header class="main-header">
   <!-- Logo -->
   <a href="{{URL::to($currentLocale.'/System/Dashboard')}}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>SVKD</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">
         <strong>Quản Trị SVKD</strong>
      <!-- <img src="{{asset('public/images/logo_hatgiong_f1508.png')}}" title="Cửa Hàng Chuyên Mua Bán Xe Máy Cũ Xe39" alt="Cửa Hàng Chuyên Mua Bán Xe Máy Cũ Xe39" height="35px"> -->
      </span>
   </a>
   <!-- Header Navbar: style can be found in header.less -->
   <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
         <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
         <ul class="nav navbar-nav">
            <!-- Messages: style can be found in dropdown.less-->
            <li style="display: none;" class="dropdown notifications-menu">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
               <i class="fa fa-fw fa-users" aria-hidden="true"></i>
               <span class="label label-warning">10</span>
               </a>
               <ul class="dropdown-menu">
                  <li class="header"><strong>Có 10 Khách Hàng Cần Chăm Sóc</strong></li>
                  <li>
                     <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 200px;">
                        <ul class="menu" style="overflow: auto; width: 100%; height: 300px;">
                           <li>
                              <a href="#">
                                 <i class="fa fa-fw fa-user text-aqua"></i> Khách Hàng ABC DEF GHL Có 10 Sản Phẩm
                              </a>
                           </li>
                           <li>
                              <a href="#">
                                 <i class="fa fa-fw fa-user text-aqua"></i> Khách Hàng ABC DEF GHL Có 10 Sản Phẩm
                              </a>
                           </li>
                           <li>
                              <a href="#">
                                 <i class="fa fa-fw fa-user text-aqua"></i> Khách Hàng ABC DEF GHL Có 10 Sản Phẩm
                              </a>
                           </li>
                           <li>
                              <a href="#">
                                 <i class="fa fa-fw fa-user text-aqua"></i> Khách Hàng ABC DEF GHL Có 10 Sản Phẩm
                              </a>
                           </li>
                           <li>
                              <a href="#">
                                 <i class="fa fa-fw fa-user text-aqua"></i> Khách Hàng ABC DEF GHL Có 10 Sản Phẩm
                              </a>
                           </li>
                           <li>
                              <a href="#">
                                 <i class="fa fa-fw fa-user text-aqua"></i> Khách Hàng ABC DEF GHL Có 10 Sản Phẩm
                              </a>
                           </li>
                           <li>
                              <a href="#">
                                 <i class="fa fa-fw fa-user text-aqua"></i> Khách Hàng ABC DEF GHL Có 10 Sản Phẩm
                              </a>
                           </li>
                           <li>
                              <a href="#">
                                 <i class="fa fa-fw fa-user text-aqua"></i> Khách Hàng ABC DEF GHL Có 10 Sản Phẩm
                              </a>
                           </li>
                           <li>
                              <a href="#">
                                 <i class="fa fa-fw fa-user text-aqua"></i> Khách Hàng ABC DEF GHL Có 10 Sản Phẩm
                              </a>
                           </li>

                        </ul>
                        <div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 3px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div>
                        <div class="slimScrollRail" style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
                     </div>
                  </li>
                  <li class="footer"><a href="{{URL::to('System/ListInvoices')}}" title="Danh Sách Đơn Hàng Trong Ngày">Xem Tất Cả <i class="fa fa-fw fa-arrow-circle-right"></i></a></li>
               </ul>
            </li>
            <li style="display: none;" class="dropdown notifications-menu">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
               <i class="fa fa-fw fa-cart-plus" aria-hidden="true"></i>
               <span class="label label-danger">10</span>
               </a>
               <ul class="dropdown-menu">
                  <li class="header"><strong>Có 10 đơn hàng đang đợi</strong></li>
                  <li>
                     <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 200px;">
                        <ul class="menu" style="overflow: auto; width: 100%; height: 300px;">
                           <li>
                              <a href="#">
                                 <i class="fa fa-fw fa-shopping-cart text-red"></i> Khách Hàng ABC DEF GHL Có 10 Sản Phẩm
                              </a>
                           </li>
                           <li>
                              <a href="#">
                                 <i class="fa fa-fw fa-shopping-cart text-red"></i> Khách Hàng ABC DEF GHL Có 10 Sản Phẩm
                              </a>
                           </li>
                           <li>
                              <a href="#">
                                 <i class="fa fa-fw fa-shopping-cart text-red"></i> Khách Hàng ABC DEF GHL Có 10 Sản Phẩm
                              </a>
                           </li>
                           <li>
                              <a href="#">
                                 <i class="fa fa-fw fa-shopping-cart text-red"></i> Khách Hàng ABC DEF GHL Có 10 Sản Phẩm
                              </a>
                           </li>
                           <li>
                              <a href="#">
                                 <i class="fa fa-fw fa-shopping-cart text-red"></i> Khách Hàng ABC DEF GHL Có 10 Sản Phẩm
                              </a>
                           </li>
                           <li>
                              <a href="#">
                                 <i class="fa fa-fw fa-shopping-cart text-red"></i> Khách Hàng ABC DEF GHL Có 10 Sản Phẩm
                              </a>
                           </li>
                           <li>
                              <a href="#">
                                 <i class="fa fa-fw fa-shopping-cart text-red"></i> Khách Hàng ABC DEF GHL Có 10 Sản Phẩm
                              </a>
                           </li>
                           <li>
                              <a href="#">
                                 <i class="fa fa-fw fa-shopping-cart text-red"></i> Khách Hàng ABC DEF GHL Có 10 Sản Phẩm
                              </a>
                           </li>
                           <li>
                              <a href="#">
                                 <i class="fa fa-fw fa-shopping-cart text-red"></i> Khách Hàng ABC DEF GHL Có 10 Sản Phẩm
                              </a>
                           </li>
                        </ul>
                        <div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 3px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div>
                        <div class="slimScrollRail" style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
                     </div>
                  </li>
                  <li class="footer"><a href="{{URL::to('System/ListInvoices')}}" title="Danh Sách Đơn Hàng Trong Ngày">Xem Tất Cả <i class="fa fa-fw fa-arrow-circle-right"></i></a></li>
               </ul>
            </li>
            
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown">
               @if(!$userAuth->user_avatar)
                  @if($userAuth->user_sex == 0)
                     <img src="{{asset('public\images\account\avatar1.png')}}" class="user-image" alt="userMaleLogo" title="userMaleLogo">
                  @elseif($userAuth->user_sex == 1)
                     <img src="{{asset('public\images\account\avatar2.png')}}" class="user-image" alt="userFemaleLogo" title="userFemaleLogo">
                  @endif
               @else
                 <img src="{{asset($userAuth->user_avatar)}}" class="user-image" alt="{{ $userAuth->user_fullname }}" title="{{ $userAuth->user_fullname }}">
               @endif
               <span class="hidden-xs">
                  {{ $userAuth->user_account }}
               </span>
               </a>
               <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                     @if(!$userAuth->user_avatar)
                        @if($userAuth->user_sex == 0)
                           <img src="{{asset('public\images\account\avatar1.png')}}" class="img-circle" alt="userMaleLogo" title="userMaleLogo">
                        @elseif($userAuth->user_sex == 1)
                           <img src="{{asset('public\images\account\avatar2.png')}}" class="img-circle" alt="userFemaleLogo" title="userFemaleLogo">
                        @endif
                     @else
                        <img src="{{asset($userAuth->user_avatar)}}" class="img-circle" alt="{{ $userAuth->user_fullname }}" title="{{ $userAuth->user_fullname }}">
                     @endif
                     <p> {{ $userAuth->user_fullname }}
                        <small> Quyền {{ $userAuth->auth_name }}</small>
                     </p>
                  </li>
                  <!-- Menu Body -->
                  <!-- <li class="user-body">
                     <div class="row">
                        <div class="col-xs-4 text-center">
                           <a href="#">Followers</a>
                        </div>
                        <div class="col-xs-4 text-center">
                           <a href="#">Sales</a>
                        </div>
                        <div class="col-xs-4 text-center">
                           @if("vi" == 'vi')
                              <a href="{{URL::to($currentLocale.'/System/Dashboard')}}">Tiếng Anh</a>
                           @else
                               <a href="{{URL::to($currentLocale.'/System/Dashboard')}}">Tiếng Việt</a>
                           @endif
                        </div>
                     </div>
                     <!-- /.row -->
                  <!-- </li> -->
                  <!-- Menu Footer-->
                  <li class="user-footer">
                     <!-- <div class="pull-left">
                        <a href="{{URL::to(Request::segment(1).'/System/Infomation')}}" class="btn btn-primary btn-flat">Thông Tin</a> 
                     </div> -->
                     <div class="pull-left" style="width: 45%">
                        <a href="{{URL::to('System/Profile')}}" class="btn btn-primary btn-flat bg-blue" style="width:100%" title="Thông Tin Tài Khoản"><i class="fa fa-fw fa-id-card-o" aria-hidden="true"></i> Tài Khoản</a>
                     </div>
                     <div class="pull-right" style="width: 45%">
                        <a href="{{URL::to('System/Logout')}}" class="btn btn-danger btn-flat bg-red" style="width:100%" title="Đăng Xuất"><i class="fa fa-sign-out" aria-hidden="true"></i> Đăng Xuất</a>
                     </div>
                  </li>
               </ul>
            </li>
            <!-- Control Sidebar Toggle Button -->
            <!-- <li>
               <a href="#" data-toggle="control-sidebar"><i class="fa fa-fw fa-gears"></i></a>
            </li> -->
         </ul>
      </div>
   </nav>
</header>

