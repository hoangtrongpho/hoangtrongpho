@extends('system.layouts.index')

@section('headTitle')
   <title>Quản Trị Tin Tức - {{ Lang::get('messages.system_heade_title') }}</title>
@stop

@section('pageTitle')
   <h1>
        <i class="fa fa-fw fa-newspaper-o" aria-hidden="true"></i> Quản Trị Tin Tức
        <small>{{ Lang::get('messages.system_verion') }}</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{URL::to('/System/Dashboard')}}"><i class="fa fa-fw fa-dashboard"></i> Hệ Thống Quản Trị Website</a>
        </li>
        <li class="active"><i class="fa fa-fw fa-newspaper-o" aria-hidden="true"></i> Tạo Mới Tin Tức</li>
    </ol>
@stop

@section('headCss')
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/daterangepicker/daterangepicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-ui-switch/bootstrap-switch.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-multi-select/isteven-multi-select.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-toastr/angular-toastr.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/iCheck/flat/blue.css')}}">

    <style type="text/css">
        .has-error>.mce-tinymce{
            border-color: #dd4b39 !important;
            box-shadow: none !important;
        }
        .has-error>.input-group, .has-error>.input-group>.input-group-addon{
            border-color: #dd4b39 !important;
            box-shadow: none !important;
        }

        .has-error button, .has-error button:active{
            border-color: #dd4b39 !important;
        }


        .slbTags .multiSelect .checkboxLayer{
            bottom: 35px !important;
        }
        .slbTags .multiSelect .buttonLabel, .slbProvince .multiSelect .buttonLabel{
            width: auto !important;
        }
    </style>
@stop

@section('headJs')
    <!-- Date Time Picker -->
    <script type="text/javascript" src="{{asset('public/system/plugins/daterangepicker/daterangepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-daterangepicker/angular-daterangepicker.min.js')}}"></script>

    <script src="{{asset('public/system/plugins/angular-ui-switch/bootstrap-switch.js')}}"></script>
    <script src="{{asset('public/system/plugins/angular-ui-switch/angular-bootstrap-switch.js')}}"></script>

    <!-- iCheck -->
    <script type="text/javascript" src="{{asset('public/system/plugins/iCheck/icheck.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-icheck/angular-icheck.js')}}"></script>

    <!-- File Manager -->
    <script src="{{asset('vendor/unisharp/laravel-filemanager/public/js/lfm.js')}}"></script>

    <!-- Angular Multi Select -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-multi-select/isteven-multi-select.js')}}"></script>

    <!-- UI Bootstrap Modal --> 
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-bootstrap/ui-bootstrap-tpls-2.0.0.min.js')}}"></script>

    <!-- Angular Toastr -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-toastr/angular-toastr.tpls.js')}}"></script>

    <!-- Angular Confirm Dialog -->
    <script type="text/javascript" src="{{asset('public/system/plugins/bootbox/bootbox.min.js')}}"></script>

    <script src="{{asset('/public/system/plugins/tinymce/tinymce.min.js')}}"></script>

    <script src="{{asset('/public/system/plugins/angular-tinymce/angular-tinymce.js')}}"></script>
    <script src="{{asset('/public/system/plugins/angular-tinymce/langs/vi_VN.js')}}"></script>

    <!-- JS -->
    <script type="text/javascript" src="{{asset('public/system/common/system.common.angurlar.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/common/system.createnews.angurlar.js')}}"></script>



    <script type="text/javascript">
        $(document).ready(function() { 
            $(window).on('load', function() {  
                //insert all your ajax callback code here. 
                //Which will run only after page is fully loaded in background.
                $("#addReLoading").hide();
            });

            //Laravel File Manager
            $('#selectImages').filemanager('images');

            //Minify Left Menu
            //$("body").addClass("sidebar-collapse");
        });

        // $(window).bind('beforeunload', function(){
        //   alert( 'Are you sure you want to leave?');
        // });

        $( "#dialog-modal" ).on( "dialogopen", function( event, ui ) {
            console.log('Wayhay!!');
            window.open("http://www.google.com");
        } );
    </script>

@stop

@section('container')
<div class="row" ng-app="createNewsApp" ng-controller="createNewsController" ng-init="pageInit()">

<!-- <button type="button" class="btn btn-default" ng-click="openDialog()">Toggle Animation </button> -->


    <form class="form" class="form-vertical" name="frmAdd" novalidate>

        <div class="col-lg-9 col-xs-12">
            <div class="box box-danger">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-fw fa-newspaper-o" aria-hidden="true"></i> Chi Tiết Bài Viết</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-fw fa-minus"></i></button>
                   </div>
                </div>

                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group" ng-show="model.datainit.pageStatus == 'update'">
                        <a class="btn btn-default" href="{{URL::to('System/ListNews')}}" title="CreateNews">
                            <i class="fa fa-fw fa-arrow-left" aria-hidden="true"></i>
                            <span class="hidden-xs">Trở Về</span>
                        </a>
                        <div class="pull-right">&nbsp;</div>
                        <button type="button" 
                                tabindex="101"
                                class="btn btn-danger"
                                ng-click="formReload()"
                                title="Hủy">
                                <i class="fa fa-fwfa-ban" aria-hidden="true"></i>
                                <span class="hidden-xs">Hủy</span>
                        </button>
                        
                       <!--  <button type="button" 
                                tabindex="99"
                                ng-click="clipboardItem(1)"
                                class="btn btn-success pull-right"
                                ng-disabled="frmAdd.dtpPublishes.$invalid
                                          || frmAdd.txtAlias.$invalid
                                          || frmAdd.txtTitle.$invalid
                                          || frmAdd.txtDescription.$invalid">
                            <i class="fa fa-fw fa-eye" aria-hidden="true"></i>
                            <span class="hidden-xs">Xem Trước</span>
                        </button>
                        <div class="pull-right">&nbsp;</div> -->
                        <button type="button" 
                                tabindex="99"
                                ng-click="updateItem(1)"
                                class="btn btn-warning pull-right"
                                ng-disabled="frmAdd.dtpPublishes.$invalid
                                          || frmAdd.txtCateParent.$invalid
                                          || frmAdd.txtAlias.$invalid
                                          || frmAdd.txtTitle.$invalid
                                          || frmAdd.txtDescription.$invalid
                                          || frmAdd.slbRating.$invalid">
                            <i class="fa fa-fw fa-edit" aria-hidden="true"></i>
                            <span class="hidden-xs">Cập Nhật</span>
                        </button>
                        <div class="pull-right">&nbsp;</div>
                        <button type="button" 
                                tabindex="101"
                                class="btn btn-danger pull-right"
                                ng-click="removeItem([[model.request.txtNewsId]])">
                                <i class="fa fa-fw fa-trash-o" aria-hidden="true"></i>
                                <span class="hidden-xs">Xóa</span>
                        </button>
                    </div>

                    <div class="form-group" ng-show="model.datainit.pageStatus == 'add'">
                        <a class="btn btn-info" href="{{URL::to('System/ListNews')}}" title="CreateNews">
                            <i class="fa fa-fw fa-arrow-left" aria-hidden="true"></i>
                            <span class="hidden-xs">Trở Về</span>
                        </a>
                        <div class="pull-right">&nbsp;</div>
                        <button type="button" 
                                tabindex="101"
                                class="btn btn-danger"
                                ng-click="formReset()"
                                title="Hủy">
                                <i class="fa fa-fw fa-ban" aria-hidden="true"></i>
                                <span class="hidden-xs">Hủy</span>
                        </button>
                        
                        <!-- <button type="button" 
                                tabindex="99"
                                ng-click="clipboardItem(1)"
                                class="btn btn-success pull-right"
                                ng-disabled="frmAdd.dtpPublishes.$invalid
                                          || frmAdd.txtAlias.$invalid
                                          || frmAdd.txtTitle.$invalid
                                          || frmAdd.txtDescription.$invalid">
                            <i class="fa fa-fw fa-eye" aria-hidden="true"></i>
                            <span class="hidden-xs">Xem Trước</span>
                        </button>
                        <div class="pull-right">&nbsp;</div> -->
                        <button type="button" 
                                tabindex="99"
                                ng-click="addItem(1)"
                                class="btn btn-primary pull-right"
                                ng-disabled="frmAdd.dtpPublishes.$invalid
                                          || frmAdd.txtCateParent.$invalid
                                          || frmAdd.txtAlias.$invalid
                                          || frmAdd.txtTitle.$invalid
                                          || frmAdd.txtDescription.$invalid
                                          || frmAdd.slbRating.$invalid"
                                title="Thêm Mới">
                            <i class="fa fa-fw fa-plus" aria-hidden="true"></i>
                            <span class="hidden-xs">Thêm Mới</span>
                        </button>
                        <div class="pull-right">&nbsp;</div>
                        <button type="button" 
                                tabindex="100"
                                ng-click="addItem(0)"
                                class="btn btn-info pull-right"
                                title="Lưu Nháp"
                                ng-disabled="frmAdd.dtpPublishes.$invalid
                                          || frmAdd.txtCateParent.$invalid
                                          || frmAdd.txtAlias.$invalid
                                          || frmAdd.txtTitle.$invalid
                                          || frmAdd.txtDescription.$invalid
                                          || frmAdd.slbRating.$invalid">
                            <i class="fa fa-fw fa-clipboard" aria-hidden="true"></i>
                            <span class="hidden-xs">Lưu Nháp</span>
                        </button>
                        
                    </div>

                    <div class="form-group" style="display:none;">
                        <input  
                            type="text"
                            class="form-control" 
                            id="txtSlug" 
                            name="txtSlug" 
                            placeholder="Slug" 
                            autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                            value="{{$slug}}">
                    </div>

                    <div class="form-group" ng-class="{ 'has-error' : frmAdd.txtTitle.$invalid && !frmAdd.txtTitle.$pristine }">
                        <label for="txtTitle" class="control-label display-block-xs">Tiêu Đề Bản Tin:</label>  
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-fw fa-newspaper-o" aria-hidden="true"></i>
                            </span>
                            <input  
                                tabindex="1"
                                type="text"
                                class="form-control" 
                                id="txtTitle" 
                                name="txtTitle" 
                                placeholder="Tiêu Đề Bản Tin" 
                                ng-model="model.request.txtTitle"
                                autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                ng-minlength="2"
                                ng-maxlength="150"
                                maxlength="150"
                                required >
                        </div>
                        <span class="help-block" ng-show="frmAdd.txtTitle.$error.required && frmAdd.txtTitle.$invalid && !frmAdd.txtTitle.$pristine">Vui Lòng Nhập Tiêu Đề.</span>
                        <span class="help-block" ng-show="frmAdd.txtTitle.$error.minlength">Tiêu Đề Không Được Ngắn Hơn 2 Ký Tự.</span>
                        <span class="help-block" ng-show="frmAdd.txtTitle.$error.maxlength">Tiêu Đề Không Được Dài Hơn 150 Ký Tự.</span>
                    </div>

                    <div class="form-group" ng-class="{ 'has-error' : frmAdd.txtDescription.$invalid && !frmAdd.txtDescription.$pristine }">
                        <label for="txtDescription" class="control-label display-block-xs">Mô Tả Ngắn:</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-fw fa-list" aria-hidden="true"></i>
                            </span>
                            <input
                                tabindex="2"
                                type="text"
                                class="form-control" 
                                id="txtDescription" 
                                name="txtDescription" 
                                placeholder="Mô Tả Ngắn" 
                                ng-model="model.request.txtDescription"
                                autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                ng-minlength="2"
                                ng-maxlength="200"
                                maxlength="200"
                                height="50px"
                                required>
                        </div>
                        <span class="help-block" ng-show="frmAdd.txtDescription.$error.required && frmAdd.txtDescription.$invalid && !frmAdd.txtDescription.$pristine">Vui Lòng Nhập Mô Tả Ngắn.</span>
                        <span class="help-block" ng-show="frmAdd.txtDescription.$error.minlength">Mô Tả Ngắn Không Được Ngắn Hơn 2 Ký Tự.</span>
                        <span class="help-block" ng-show="frmAdd.txtDescription.$error.maxlength">Mô Tả Ngắn Không Được Dài Hơn 200 Ký Tự.</span>
                    </div>

                    <div class="form-group" ng-show="model.request.radFormatPage == 1" ng-class="{ 'has-error' : frmAdd.txaContent.$invalid && !frmAdd.txaContent.$pristine }">
                        <label for="txaContent" class="control-label hidden-xs">Nội Dung Chi Tiết:</label>    
                        <textarea 
                            tabindex="3"
                            id="txaContent" 
                            name="txaContent" 
                            ui-tinymce="model.tinymceOptions" 
                            ng-model="model.request.txaContent" 
                            required>
                        </textarea>
                        <span class="help-block" ng-show="frmAdd.txaContent.$error.required && frmAdd.txaContent.$invalid && !frmAdd.txaContent.$pristine">Vui Lòng Nhập Nội Dung Chi Tiết Bài Viết.</span>
                    </div>

                    <div class="form-group" ng-show="model.request.radFormatPage == 2" ng-class="{ 'has-error' : frmAdd.txtContent.$invalid && !frmAdd.txtContent.$pristine }">
                        <label for="txtContent" class="col-sm-12 control-label">Mã Video Clip Từ Youtube: </label>
                        <input  
                            tabindex="6"
                            type="text"
                            class="form-control" 
                            id="txtContent" 
                            name="txtContent" 
                            placeholder="Mã Video Clip Từ Youtube" 
                            ng-model="model.request.txtContent"
                            ng-minlength="2"
                            ng-maxlength="100"
                            maxlength="100"
                            autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                            required>
                        <span class="help-block" ng-show="frmAdd.txtContent.$error.required && frmAdd.txtContent.$invalid && !frmAdd.txtContent.$pristine">Đường Dẫn Video Clip Từ Youtube.</span>
                        <span class="help-block" ng-show="frmAdd.txtContent.$error.minlength">Mã Video Clip Không Được Ngắn Hơn 2 Ký Tự.</span>
                        <span class="help-block" ng-show="frmAdd.txtContent.$error.maxlength">Mã Video Clip Không Được Dài Hơn 100 Ký Tự.</span>
                    </div>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-fw fa-search-plus" aria-hidden="true"></i> Chi Tiết SEO</h3>
                    <div class="box-tools pull-right">
                       <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-fw fa-minus"></i></button>
                   </div>
                </div>

                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="txtTitleSEO" class="col-sm-12 control-label">Tiêu Đề SEO (meta title):</label>
                        <input  
                            type="text"
                            class="form-control" 
                            id="txtTitleSEO" 
                            name="txtTitleSEO" 
                            placeholder="Tiêu Đề SEO" 
                            ng-model="model.request.txtTitleSEO"
                            autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                    </div>

                    <div class="form-group">
                        <label for="txtKeywordSEO" class="col-sm-12 control-label">Từ Khóa SEO (meta keywords):</label>
                        <input  
                            class="form-control" 
                            id="txtKeywordSEO" 
                            name="txtKeywordSEO" 
                            placeholder="Từ Khóa SEO Ví dụ: dienquang, caulacbodienquang, denled" 
                            ng-model="model.request.txtKeywordSEO"
                            autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                    </div>

                    <div class="form-group">
                        <label for="txtDescriptionSEO" class="col-sm-12 control-label">Mô Tả SEO (meta description):</label>
                        <textarea
                            class="form-control" 
                            id="txtDescriptionSEO" 
                            name="txtDescriptionSEO" 
                            placeholder="Mô Tả SEO" 
                            ng-model="model.request.txtDescriptionSEO"
                            style="resize: vertical;"
                            autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"></textarea>
                    </div>

                    
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->

        <div class="col-lg-3 col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-fw fa-info-circle" aria-hidden="true"></i> Thông Tin chung</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-fw fa-minus"></i></button>
                   </div>
                </div>
                <!-- /.box-header -->

                <div class="box-body">
                    
                    <div class="form-group row slbProvince" ng-class="{ 'has-error' : frmAdd.txtProvince.$invalid && !frmAdd.txtProvince.$pristine }">
                        <label for="slbProvince" class="col-sm-12 control-label">Phân Loại Vùng Miền:</label>
                        <div class="col-sm-12">
                            <isteven-multi-select
                                tabindex="2"
                                name="slbProvince"
                                directive-id="slbProvince"
                                input-model="model.datainit.slbListProvinces"
                                output-model="model.request.slbProvince"
                                helper-elements="filter reset"
                                button-label="province_name"
                                item-label="province_name"
                                output-properties="province_id"
                                search-property="province_name"
                                tick-property="ticked"
                                orientation="vertical"
                                disable-property="true"
                                is-disabled="false"
                                selection-mode="multiple"
                                min-search-length="10"
                                translation="model.localLang"
                                max-labels="2"  
                                on-reset="slbClear('txtProvince')"
                                on-item-click="slbItemClick(data,'txtProvince')">
                            </isteven-multi-select>
                            <span class="help-block" ng-show="frmAdd.txtProvince.$error.required && frmAdd.txtProvince.$invalid && !frmAdd.txtProvince.$pristine">Vui Lòng Chọn Vùng Hiển Thị Tin Tức.</span>
                        </div>
                        <input  
                            type="text"
                            class="form-control" 
                            id="txtProvince" 
                            name="txtProvince" 
                            ng-model="model.request.txtProvince"
                            autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style="display:none;" 
                            required >
                    </div>

                    <div class="form-group row" ng-class="{ 'has-error' : frmAdd.dtpPublishes.$invalid && !frmAdd.dtpPublishes.$pristine }">
                        <label for="dtpPublishes" class="col-sm-12 control-label">Thời Gian Phát Hành:</label>
                        <div class="col-md-12">
                            <input  
                                date-range-picker 
                                class="form-control date-picker" 
                                type="text" 
                                ng-model="model.request.dtpPublishes" 
                                options="model.datePickerOpts"
                                clearable="true"
                                id="dtpPublishes"
                                name="dtpPublishes"
                                tabindex="1"
                                placeholder="Thời Gian Phát Hành"
                                required />
                            <span class="help-block" ng-show="frmAdd.dtpPublishes.$error.required && frmAdd.dtpPublishes.$invalid && !frmAdd.dtpPublishes.$pristine">Vui Lòng Chọn Thời Gian Phát Hành.</span>
                        </div>
                    </div>

                    <div class="form-group row" ng-class="{ 'has-error' : frmAdd.slbRating.$invalid && !frmAdd.slbRating.$pristine }" style="display: none;">
                        <label for="slbRating" class="col-sm-12 control-label">Đánh Giá:</label>
                        <div class="col-md-12">
                            <select class="form-control" id="slbRating" name="slbRating" 
                                    ng-model="model.request.slbRating" 
                                    ng-options="option.value as option.name for option in model.datainit.ratingOptions">
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="sblStatus" class="col-sm-12 control-label">Trạng Thái:</label>
                        <div class="col-sm-12">
                            <input
                                tabindex="3"
                                bs-switch
                                name="sblStatus"
                                ng-model="model.request.sblStatus"
                                type="checkbox"
                                switch-active="true"
                                switch-readonly="false"
                                switch-size="normal"
                                switch-animate="true"
                                switch-label=""
                                switch-icon="icon-save"
                                switch-on-text="Hiện"
                                switch-off-text="Ẩn"
                                switch-on-color="success"
                                switch-off-color="danger"
                                switch-radio-off="false"
                                switch-label-width="0px"
                                switch-handle-width="auto"
                                switch-inverse="false"
                                switch-change="onChange()"
                                ng-true-value="1"
                                ng-false-value="0">
                        </div>
                    </div>

                    <div class="form-group row" ng-class="{ 'has-error' : frmAdd.txtCateParent.$invalid && !frmAdd.txtCateParent.$pristine }">
                        <label for="slbCateParent" class="col-sm-12 control-label">Danh Mục:</label>
                        <div class="col-sm-12">
                            <isteven-multi-select
                                tabindex="2"
                                name="slbCateParent"
                                input-model="model.datainit.slbListCategories"
                                output-model="model.request.slbCateParent"
                                helper-elements="filter reset"
                                button-label="news_cate_name"
                                item-label="news_cate_name"
                                output-properties="news_cate_id"
                                search-property="news_cate_name"
                                tick-property="ticked"
                                orientation="vertical"
                                disable-property="true"
                                is-disabled="false"
                                selection-mode="single"
                                min-search-length="10"
                                translation="model.localLang"
                                max-labels="1"  
                                directive-id="slbCateParent"
                                on-reset="slbClear('txtCateParent')"
                                on-item-click="slbItemClick(data,'txtCateParent')">
                            </isteven-multi-select>
                            <span class="help-block" ng-show="frmAdd.txtCateParent.$error.required && frmAdd.txtCateParent.$invalid && !frmAdd.txtCateParent.$pristine">Vui Lòng Chọn Thể Loại Tin Tức.</span>
                        </div>
                        <input  
                            type="text"
                            class="form-control" 
                            id="txtCateParent" 
                            name="txtCateParent" 
                            ng-model="model.request.txtCateParent"
                            autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style="display:none;" 
                            required >
                    </div>

                    <div class="form-group row" ng-class="{ 'has-error' : frmAdd.txtAlias.$error.maxlength }" ng-show="model.request.radFormatPage == 1" >
                        <label for="txtAlias" class="col-sm-12 control-label">Bút Danh:</label>
                        <div class="col-sm-12">
                            <input  
                                tabindex="4"
                                type="text"
                                class="form-control" 
                                id="txtAlias" 
                                name="txtAlias" 
                                placeholder="Bút Danh" 
                                ng-model="model.request.txtAlias"
                                autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                ng-minlength="2"
                                ng-maxlength="30"
                                maxlength="30">
                            <span class="help-block" ng-show="frmAdd.txtAlias.$error.minlength">Bút Danh Không Được Ngắn Hơn 2 Ký Tự.</span>
                            <span class="help-block" ng-show="frmAdd.txtAlias.$error.maxlength">Bút Danh Không Được Dài Hơn 30 Ký Tự.</span>
                        </div>
                    </div>

                    <div class="form-group row" ng-show="model.request.radFormatPage == 1" >
                        <label for="selectImages" class="col-sm-12 control-label">Hình Ảnh:</label>    
                        <div class="form-group col-md-12">
                            <div class="input-group col-md-12">
                                <span class="input-group-btn">
                                    <a  id="selectImages" 
                                        name="selectImages" 
                                        data-input="thumbnail" 
                                        data-preview="holder" 
                                        class="btn btn-primary">
                                        <i class="fa fa-fw fa-picture-o"></i>
                                        <span>Chọn Ảnh</span>
                                    </a>
                                </span>
                                <input  
                                    tabindex="5"
                                    id="thumbnail" 
                                    class="form-control" 
                                    type="text" 
                                    name="filepath" 
                                    placeholder="Chọn Ảnh Đại Diện"
                                    ng-model="model.request.txtImages" 
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                                <i  class="fa fa-fw fa-times pull-right" 
                                    style="position: absolute;right: 1px;z-index: 2;top: 1px; background-color: rgba(255,255,255,0.8);padding: 8px;" 
                                    ng-click="resetChoiceThumbnail()"></i> 
                            </div>
                        </div> 
                        <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-12 "> 
                            <img ng-src="[[model.request.txtImages]]"
                                 onError="this.src='{{asset('public/images/default-image.png')}}';"
                                 id="holder" 
                                 class="img-responsive img-thumbnail" 
                                 alt="Chọn Ảnh Đại Diện" 
                                 title="Chọn Ảnh Đại Diện" 
                                 height="150px">
                        </div>
                    </div>
                </div>
                <!--/ box body-->
                <!-- Loading (remove the following to stop the loading)-->
                <!-- <div class="overlay" id="addReLoading">
                    <i class="fa fa-fw fa-refresh fa-spin"></i>
                </div> -->
                <!-- end loading -->
            </div>

            <div class="box box-warning">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-fw fa-list-alt" aria-hidden="true"></i> Chuyên Trang</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-fw fa-minus"></i></button>
                   </div>
                </div>
                <!-- /.box-header -->

                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="radio icheck">
                                <label>
                                    <input icheck name="radFormatPage" ng-model="model.request.radFormatPage" type="radio" value="1" title="Chuyên Trang Tin Tức">
                                    Chuyên Trang Tin Tức
                                </label>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="radio icheck">
                                <label>
                                    <input icheck name="radFormatPage" ng-model="model.request.radFormatPage" type="radio" value="2" title="Chuyên Trang Video Clip">
                                    Chuyên Trang Video Clip
                                </label>
                            </div>
                        </div>
                    </div>

                </div>
                <!--/ box body-->
            </div>

            <div class="box box-info" ng-show="model.request.radFormatPage == 1" >
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-fw fa-tags" aria-hidden="true"></i> Thẻ Bài Viết</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-fw fa-minus"></i></button>
                   </div>
                </div>
                <!-- /.box-header -->

                <div class="box-body">
                    <div class="row slbTags">
                        <label for="slbTags" class="col-sm-12 control-label">Thẻ Bài Viết:</label>
                        <div class="col-sm-12">
                            <isteven-multi-select
                                tabindex="2"
                                name="slbTags"
                                directive-id="slbTags"
                                input-model="model.datainit.slbListTags"
                                output-model="model.request.slbTags"
                                helper-elements="filter reset"
                                button-label="tag_name"
                                item-label="tag_name"
                                output-properties="tag_id"
                                search-property="tag_name"
                                tick-property="ticked"
                                orientation="vertical"
                                disable-property="true"
                                is-disabled="false"
                                selection-mode="multiple"
                                min-search-length="10"
                                translation="model.localLang"
                                max-labels="2"  
                                on-reset="">
                            </isteven-multi-select>
                        </div>
                    </div>
                </div>
                <!--/ box body-->
            </div>

        </div>
    </form>
    <!--/ form action-->
</div>
@stop