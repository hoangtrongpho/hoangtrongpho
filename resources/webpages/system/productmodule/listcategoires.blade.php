@extends('system.layouts.index')

@section('headTitle')
   <title>{{ Lang::get('messages.system_heade_title') }}</title>
@stop

@section('pageTitle')
   <h1>
        <i class="fa fa-fw fa-cubes" aria-hidden="true"></i> Quản Trị Nhóm Sản Phẩm
        <small>{{ Lang::get('messages.system_verion') }}</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{URL::to('/System/Dashboard')}}"><i class="fa fa-fw fa-dashboard"></i> Hệ Thống Quản Trị Website</a>
        </li>
        <li class="active"><i class="fa fa-fw fa-cubes" aria-hidden="true"></i> Quản Trị Nhóm Sản Phẩm</li>
    </ol>
@stop

@section('headCss')
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-multi-select/isteven-multi-select.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/daterangepicker/daterangepicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/chosen/chosen.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-toastr/angular-toastr.css')}}" />
    <link rel="stylesheet" href="{{asset('public/system/plugins/angular-ui-tree/angular-ui-tree.min.css')}}">

    <style type="text/css">

        #tree-root{
            border: 2px solid #555555; 
            padding: 15px;
            margin-top: 15px;
        }

        .angular-ui-tree-nonhandle {
            background: #f8faff;
            border: 1px solid #dae2ea;
            color: #7c9eb2;
            padding: 10px 0px;
            line-height: 25px !important;
            cursor: auto;
            height: 52px;
        }

        .angular-ui-tree-handle {
            background: #f8faff;
            border: 1px solid #dae2ea;
            color: #7c9eb2;
            padding: 10px 0px;
            line-height: 25px !important;
            height: 52px;    
        }

        .angular-ui-tree-handle:hover {
            color: #438eb9;
            background: #f4f6f7;
            border-color: #dce2e8;
        }

        .angular-ui-tree-placeholder {
            background: #f0f9ff;
            border: 2px dashed #bed2db;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        tr.angular-ui-tree-empty {
            height:100px
        }

        .group-title {
            background-color: #687074 !important;
            color: #FFF !important;
        }


        /* --- Tree --- */
        .tree-node {
            border: 1px solid #dae2ea;
            background: #f8faff;
            color: #7c9eb2;
        }

        .nodrop {
            background-color: #f2dede;
        }

        .tree-node-content {
            margin: 10px;
        }
        .tree-handle {
            padding: 10px;
            background: #428bca;
            color: #FFF;
            margin-right: 10px;
        }

        .angular-ui-tree-handle:hover {
        }

        .angular-ui-tree-placeholder {
            background: #f0f9ff;
            border: 2px dashed #bed2db;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        .angular-ui-tree-nonhandle .btn, .angular-ui-tree-handle .btn {
            margin-right: 3px;
            margin-bottom: 3px;
        }

        .angular-ui-tree-node, .angular-ui-tree-placeholder{
            cursor: none;
        }

        .angular-ui-tree-nodes .angular-ui-tree-nodes{
            cursor: auto !important;
        }

        @media (max-width: 767px)
        {
            .angular-ui-tree-nonhandle {
                height: 82px !important;
            }

            #tree-root{
                border: 1px solid #555555; 
                padding: 5px !important;
                margin-top: 5px;
            }
        }


        .chosen-container{
            width: 100% !important;
        }
        .chosen-container-multi .chosen-choices {
            display: block;
            width: 100%;
            min-height: 34px !important;
            height: auto !important;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            border-radius: 0;
            box-shadow: none;
            border-color: #d2d6de;
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075) !important;
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075) !important;
            -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s !important;
            -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s !important;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s !important;
        }

        .chosen-container-multi .chosen-choices:hover {

        }

        .chosen-container-multi .chosen-choices li.search-choice{
            margin: 0px 5px 5px 0 !important;
        }
        .chosen-container-multi .chosen-choices li.search-field input[type="text"]{
            height: 20px !important;
            width: 100% !important
        }
        /*.chosen-container .search-field{
            width: 1% !important;
            border: none !important;
        }*/

        .chosen-container-single .chosen-single{
            padding: 6px 0px 0 12px !important;
            height: 34px !important;
            border: 1px solid #d2d6de !important;
            border-radius: 0px !important;
            background: none !important;
            background-clip: none !important;
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075) !important;
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075) !important;
            color: #555 !important;
            font-size: 14px !important;
            line-height: 1.42857143 !important;
        }

        .chosen-container-single .chosen-single div{
            top: 5px !important;
        }
        .chosen-container-single .chosen-single abbr{
            top: 10px !important;
        }
    </style>

@stop

@section('headJs')
    <!-- Date Time Picker -->
    <script type="text/javascript" src="{{asset('public/system/plugins/daterangepicker/daterangepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-daterangepicker/angular-daterangepicker.min.js')}}"></script>

    <!-- Angular Chosen -->
    <script type="text/javascript" src="{{asset('public/system/plugins/chosen/chosen.jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-chosen/angular-chosen.js')}}"></script>

    <!-- Angular Tree View -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-tree/angular-ui-tree.js')}}"></script>

    <!-- File Manager -->
    <script src="{{asset('/vendor/unisharp/laravel-filemanager/public/js/lfm.js')}}"></script>

    <!-- UI Bootstrap Modal --> 
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-bootstrap/ui-bootstrap-tpls-2.0.0.min.js')}}"></script>
    <!-- Angular Toastr -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-toastr/angular-toastr.tpls.js')}}"></script>
    <!-- Angular Confirm Dialog -->
    <script type="text/javascript" src="{{asset('public/system/plugins/bootbox/bootbox.min.js')}}"></script>
    <!-- Angular Multi Select -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-multi-select/isteven-multi-select.js')}}"></script>
    <!-- JS -->
    <script type="text/javascript" src="{{asset('public/system/common/system.listproductcategories.angurlar.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function() { 
            $(window).on('load', function() {  
                //Which will run only after page is fully loaded in background.
            });

            //Laravel File Manager
            $('#lfm1').filemanager('image');
            $('#lfm2').filemanager('image');
            
        });
    </script>

@stop

@section('container')
<div class="row" ng-app="categoryApp" ng-controller="categoryController" ng-init="pageInit()">

    <div class="col-lg-12 col-xs-12">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-fw fa-cubes" aria-hidden="true"></i>&nbsp;Danh Sách Nhóm</h3>
                <div class="box-tools pull-right">
                    <button ng-click="openAddItem(null,'add')"
                    id="sendMail" type="button" class="btn btn-primary" tabindex="4" data-toggle="modal" data-target="#modalAdd">
                        <i class="fa fa-fw fa-plus" aria-hidden="true"></i>
                        <span class="hidden-xs">Thêm Nhóm Sản Phẩm</span>
                    </button>               
                </div>
            </div>

            <!-- /.box-header -->
            <div class="box-body">

                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <label class="control-label">Có [[model.datainit.dtgList.length]] Nhóm Sản Phẩm</label>
                    </div>
                    <div class="col-xs-12 col-sm-6 text-right">
                        <label class="control-label">Tìm Kiếm: </label>
                        <input  ng-model="query"
                                ng-change="findNodes()"
                                type="text"
                                class="form-control" 
                                id="txtSearchAreas" 
                                name="txtSearchAreas">
                    </div>
                </div>
           
                <!-- tree main -->
                <div class="row">
                    <div class="col-xs-12">
                        <div  data-ui-tree="treeOptions" id="tree-root" data-max-depth="1">
                            <ol data-ui-tree-nodes ng-model="model.datainit.dtgList">
                                <li data-ng-repeat="item in model.datainit.dtgList" data-ui-tree-node>
                                    <!-- node cha -->
                                    <div  ui-tree-handle class="tree-node tree-node-content">
                                        <!-- content cha -->
                                        <div class="col-xs-12 col-sm-6" >
                                            <a class="btn btn-success btn-sm" ng-if="item.nodes && item.nodes.length > 0" data-nodrag ng-click="toggle(this)">
                                                <span class="glyphicon"
                                                      ng-class="{
                                                        'glyphicon-chevron-right': collapsed,
                                                        'glyphicon-chevron-down': !collapsed
                                                      }"></span>
                                            </a>
                                            <span>
                                                <img ng-src="[[item.cate_thumbnail]]"
                                                 onError="this.src='{{asset('public/images/default-image.png')}}';"
                                                 id="holder1" 
                                                 class="img-responsive img-thumbnail" 
                                                 alt="Ảnh Đại Diện" 
                                                 title="Ảnh Đại Diện" 
                                                 style="height: 35px;width: 50px" 
                                                 >
                                            </span>
                                            <span style="overflow: hidden;white-space: nowrap;text-overflow: ellipsis;width: 150px">[[item.cate_name]] ([[item.nodes.length]]) </span>
                                        </div>
                                        <!-- button cha-->
                                        <div class="col-xs-12 col-sm-6">
                                            <button type="button" 
                                            ng-disabled="item.nodes.length!=0"
                                            ng-click="removeItem(item)" data-nodrag class="pull-right btn btn-danger btn-sm"><i class="fa fa-fw fa-trash-o"></i></button>
                                            <button type="button" ng-click="openAddItem(item,'update')" data-nodrag class="pull-right btn btn-warning btn-sm" ><i class="fa fa-fw fa-edit"></i></button>
                                            <button type="button" ng-click="openAddItem(item,'add')" data-nodrag class="pull-right btn btn-primary btn-sm"><i class="fa fa-fw fa-plus"></i></button>
                                        </div>
                                    </div>
                                    <!-- node con 1-->
                                    <ol ui-tree-nodes="" ng-model="item.nodes" ng-class="{hidden: collapsed}">
                                        <!-- danh sách node con 1-->
                                        <li ng-repeat="node in item.nodes" data-ui-tree-node data-nodrag>
                                            <div ui-tree-handle class="tree-node tree-node-content">
                                                <!-- content con 1-->
                                                <div class="col-xs-12 col-sm-6">
                                                    <a class="btn btn-success btn-sm" ng-if="node.nodes && node.nodes.length > 0" data-nodrag ng-click="toggle(this)">
                                                        <span class="glyphicon"
                                                              ng-class="{
                                                                'glyphicon-chevron-right': collapsed,
                                                                'glyphicon-chevron-down': !collapsed
                                                              }"></span>
                                                    </a>
                                                    <span>
                                                        <img ng-src="[[node.cate_thumbnail]]"
                                                         onError="this.src='{{asset('public/images/default-image.png')}}';"
                                                         id="holder1" 
                                                         class="img-responsive img-thumbnail" 
                                                         alt="Ảnh Đại Diện" 
                                                         title="Ảnh Đại Diện" 
                                                         style="height: 35px;width: 50px" 
                                                         >
                                                    </span>
                                                    <span style="overflow: hidden;white-space: nowrap;text-overflow: ellipsis;width: 150px;">[[node.cate_name]]</span>
                                                </div>
                                                <!-- button con 1-->
                                                <div class="col-xs-12 col-sm-6">
                                                    <button type="button"
                                                    ng-disabled="node.nodes.length!=0"
                                                    ng-click="removeItem(node)" data-nodrag class="pull-right btn btn-danger btn-sm"><i class="fa fa-fw fa-trash-o"></i></button>
                                                    <button type="button" ng-click="openAddItem(node,'update')" data-nodrag class="pull-right btn btn-warning btn-sm" ><i class="fa fa-fw fa-edit"></i></button>
                                                    <button type="button"
                                                    ng-click="openAddItem(node)" data-nodrag class="pull-right btn btn-primary btn-sm"><i class="fa fa-fw fa-plus"></i></button>
                                                </div>
                                            </div>
                                            <!-- danh sách node con 2-->
                                            <ol ui-tree-nodes="" ng-model="item.nodes" ng-class="{hidden: collapsed}">
                                                <li ng-repeat="node in node.nodes" data-ui-tree-node data-nodrag>
                                                    <div ui-tree-handle class="tree-node tree-node-content">
                                                        <!-- content con 1-->
                                                        <div class="col-xs-12 col-sm-6">
                                                            <span>
                                                                <img ng-src="[[node.cate_thumbnail]]"
                                                                 onError="this.src='{{asset('public/images/default-image.png')}}';"
                                                                 id="holder1" 
                                                                 class="img-responsive img-thumbnail" 
                                                                 alt="Ảnh Đại Diện" 
                                                                 title="Ảnh Đại Diện" 
                                                                 style="height: 35px;width: 50px" 
                                                                 >
                                                            </span>
                                                            <span style="overflow: hidden;white-space: nowrap;text-overflow: ellipsis;width: 150px;">[[node.cate_name]] </span>
                                                        </div>
                                                        <!-- button con 1-->
                                                        <div class="col-xs-12 col-sm-6">
                                                            <button type="button" ng-click="removeItem(node)" data-nodrag class="pull-right btn btn-danger btn-sm"><i class="fa fa-fw fa-trash-o"></i></button>
                                                            <button type="button" ng-click="openAddItem(node,'update')" data-nodrag class="pull-right btn btn-warning btn-sm" ><i class="fa fa-fw fa-edit"></i></button>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ol>
                                        </li>
                                    </ol>

                                </li>
                            </ol>
                        </div>
                    </div>
                </div>

            <!-- Loading (remove the following to stop the loading)-->
            <div id="listReLoading" class="overlay">
                <i class="fa fa-fw fa-refresh fa-spin"></i>
            </div>
            <!-- end loading -->

        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->

    <!-- Modal View Add-->
    <div id="modalAdd" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <form class="form" class="form-inline" name="frmAdd" id="frmAdd" novalidate>
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header bg-blue" ng-show="model.datainit.modalStatus=='add'">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">
                            <i class="fa fa-fw fa-floppy-o" aria-hidden="true"></i>
                            <span>Thêm Nhóm Sản Phẩm</span>
                        </h4>
                    </div>
                    <div class="modal-header bg-yellow" ng-show="model.datainit.modalStatus=='update'">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">
                            <i class="fa fa-fw fa-pencil-square-o" aria-hidden="true"></i> 
                            <span>Cập Nhật Danh Mục</span>
                        </h4>
                    </div>
                    <div class="modal-body">
                        <!--  txtName -->
                        <div class="col-md-12">
                            <div class="form-group" ng-class="{ 'has-error' : frmAdd.txtName.$touched && frmAdd.txtName.$invalid && !frmAdd.txtName.$pristine }">
                                <label for="txtName" class="control-label">Tên Nhóm:</label>
                                <input  tabindex="1"
                                        type="text"
                                        class="form-control" 
                                        id="txtName" 
                                        name="txtName" 
                                        placeholder="Nhập Tên Nhóm Sản Phẩm" 
                                        ng-model="model.request.txtName"
                                        autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                        ng-minlength="2"
                                        ng-maxlength="100"
                                        maxlength="100"
                                        required >
                                <span class="help-block" ng-show="frmAdd.txtName.$touched && frmAdd.txtName.$error.required && frmAdd.txtName.$invalid && !frmAdd.txtName.$pristine">Vui Lòng Nhập Tên Nhóm Sản Phẩm.</span>
                                <span class="help-block" ng-show="frmAdd.txtName.$touched && frmAdd.txtName.$error.minlength">Tên Nhóm Sản Phẩm Không Được Ngắn Hơn 2 Ký Tự.</span>
                                <span class="help-block" ng-show="frmAdd.txtName.$touched && frmAdd.txtName.$error.maxlength">Tên Nhóm Sản Phẩm Không Được Dài Hơn 100 Ký Tự.</span>
                            </div>
                        </div>
                        <!--  txtDescription-->
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="txtDescription" class="control-label">Mô Tả Ngắn:</label>
                                <input  tabindex="2"
                                        type="text"
                                        class="form-control" 
                                        id="txtDescription" 
                                        name="txtDescription" 
                                        placeholder="Nhập Mô Tả Ngắn Cho Nhóm Sản Phẩm" 
                                        ng-model="model.request.txtDescription"
                                        autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                        maxlength="50">
                            </div>
                        </div>
                        <!--  txtParent_Cate_Id-->
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="slbParentList" class="control-label">Chọn Danh Mục Cha:</label>
                            </div>
                            <isteven-multi-select
                                tabindex="2"
                                name="slbCateParent"
                                input-model="model.datainit.slbParentList"
                                output-model="model.request.txtParent_Cate_Id"
                                helper-elements="filter"
                                button-label="cate_name"
                                item-label="cate_name"
                                output-properties="cate_id"
                                search-property="cate_name"
                                tick-property="ticked"
                                orientation="vertical"
                                disable-property="false"
                                is-disabled="false"
                                selection-mode="single"
                                min-search-length="10"
                                translation="model.localLang"
                                max-labels="1"  
                                directive-id="slbCateParent"
                                on-item-click="slbItemClick(data,'txtParent_Cate_Id')">
                            </isteven-multi-select>
                            <input  
                                type="text"
                                class="form-control" 
                                id="txtParent_Cate_Id" 
                                name="txtParent_Cate_Id" 
                                ng-model="model.request.txtParent_Cate_Id"
                                autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style="display:none;" 
                                required >
                        </div>
                        <hr>
                        <br>
                        <!-- txtAvatar -->
                        <div class="col-xs-12 col-md-8">
                            <div class="form-group">
                                <label for="lfm1" class="control-label">Ảnh Nhóm Sản Phẩm:</label> 
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <a  id="lfm1" 
                                            name="lfm1" 
                                            data-input="thumbnail1" 
                                            data-preview="holder" 
                                            class="btn btn-primary"
                                            style="margin: 0px -2px 0px 0px !important;">
                                            <i class="fa fa-fw fa-picture-o"></i>
                                            <span> Chọn Ảnh</span>
                                        </a>
                                    </span>
                                    <input  id="thumbnail1" 
                                            tabindex="8"
                                            class="form-control" 
                                            type="text" 
                                            name="filepath" 
                                            placeholder="Ảnh Nhóm Sản Phẩm"
                                            
                                            ng-model="model.request.txtAvatar" 
                                            autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                                </div>
                            </div>
                        </div>
                        <!-- txtAvatar -->
                        <div class="col-xs-12 col-md-4">
                            <img ng-src="[[model.request.txtAvatar]]"
                                 onError="this.src='{{asset('public/images/default-image.png')}}';"
                                 id="holder" 
                                 class="img-responsive img-thumbnail" 
                                 alt="Chọn Ảnh Nhóm Sản Phẩm" 
                                 title="Chọn Ảnh Nhóm Sản Phẩm" 
                                 height="300px">
                        </div>

                        <hr style="margin-top: 5px;">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            <i class="fa fa-fw fa-ban" aria-hidden="true"></i>
                            <span>Đóng</span>
                        </button>
                        <button ng-disabled="frmAdd.$invalid" 
                                ng-show="model.datainit.modalStatus=='add'"
                                ng-click="addItem()"
                                type="submit" class="btn btn-primary" id="btnAdd" name="btnAdd">
                            <i class="fa fa-fw fa-plus" aria-hidden="true"></i>
                            <span>Thêm Mới</span>
                        </button>
                        <button ng-disabled="frmAdd.$invalid" 
                                ng-show="model.datainit.modalStatus=='update'"
                                ng-click="updateItem()"
                                type="submit" class="btn btn-warning" id="btnAdd" name="btnAdd">
                            <i class="fa fa-fw fa-pencil-square-o" aria-hidden="true"></i>
                            <span>Cập Nhật</span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>
@stop