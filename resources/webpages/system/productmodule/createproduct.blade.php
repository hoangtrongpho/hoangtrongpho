@extends('system.layouts.index')

@section('headTitle')
   <title>{{ Lang::get('messages.system_heade_title') }}</title>
@stop

@section('pageTitle')
   <h1>
        <i class="fa fa-fw fa-motorcycle" aria-hidden="true"></i> Nhập Sản Phẩm Mới
        <small>{{ Lang::get('messages.system_verion') }}</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{URL::to('/System/Dashboard')}}"><i class="fa fa-fw fa-dashboard"></i> Hệ Thống Quản Trị Website</a>
        </li>
        <li class="active"><i class="fa fa-fw fa-motorcycle" aria-hidden="true"></i> Quản Lý Nhập Sản Phẩm</li>
    </ol>
@stop

@section('headCss')
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-bootstrap-datetimepicker/css/datetimepicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/daterangepicker/daterangepicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-ui-switch/bootstrap-switch.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-multi-select/isteven-multi-select.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-toastr/angular-toastr.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/iCheck/flat/blue.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/chosen/chosen.css')}}"/>    

    <style type="text/css">
        .has-error>.mce-tinymce{
            border-color: #dd4b39 !important;
            box-shadow: none !important;
        }
        .has-error>.input-group, .has-error>.input-group>.input-group-addon{
            border-color: #dd4b39 !important;
            box-shadow: none !important;
        }

        .has-error button, .has-error button:active{
            border-color: #dd4b39 !important;
        }


        .slbTags .multiSelect .checkboxLayer{
            bottom: 35px !important;
        }
        .slbTags .multiSelect .buttonLabel, .slbProvince .multiSelect .buttonLabel{
            width: auto !important;
        }

        .nav-tabs-custom > .nav-tabs > li.active{
            border-left: 1px solid #3c8dbc !important;
            border-right: 1px solid #3c8dbc !important;
        }
        .nav-tabs-custom > .tab-content{
            border: 1px solid #3c8dbc !important;
        }
        .chosen-container{
            width: 100% !important;
        }
        .chosen-container-multi .chosen-choices {
            display: block;
            width: 100%;
            min-height: 34px !important;
            height: auto !important;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            border-radius: 0;
            box-shadow: none;
            border-color: #d2d6de;
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075) !important;
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075) !important;
            -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s !important;
            -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s !important;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s !important;
        }

        .chosen-container-multi .chosen-choices:hover {

        }

        .chosen-container-multi .chosen-choices li.search-choice{
            margin: 0px 5px 5px 0 !important;
        }
        .chosen-container-multi .chosen-choices li.search-field input[type="text"]{
            height: 20px !important;
            width: 100% !important
        }
        /*.chosen-container .search-field{
            width: 1% !important;
            border: none !important;
        }*/

        .chosen-container-single .chosen-single{
            padding: 6px 0px 0 12px !important;
            height: 34px !important;
            border: 1px solid #d2d6de !important;
            border-radius: 0px !important;
            background: none !important;
            background-clip: none !important;
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075) !important;
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075) !important;
            color: #555 !important;
            font-size: 14px !important;
            line-height: 1.42857143 !important;
        }

        .chosen-container-single .chosen-single div{
            top: 5px !important;
        }
        .chosen-container-single .chosen-single abbr{
            top: 10px !important;
        }
    </style>
@stop

@section('headJs')
    <!-- DateTime Picker -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-bootstrap-datetimepicker/js/datetimepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-bootstrap-datetimepicker/js/datetimepicker.templates.js')}}"></script>

    <!-- Input Currency -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-ng-currency/ng-currency.directive.js')}}"></script>

    <!-- Date Range Picker -->
    <script type="text/javascript" src="{{asset('public/system/plugins/daterangepicker/daterangepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-daterangepicker/angular-daterangepicker.min.js')}}"></script>

    <script src="{{asset('public/system/plugins/angular-ui-switch/bootstrap-switch.js')}}"></script>
    <script src="{{asset('public/system/plugins/angular-ui-switch/angular-bootstrap-switch.js')}}"></script>

    <!-- iCheck -->
    <script type="text/javascript" src="{{asset('public/system/plugins/iCheck/icheck.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-icheck/angular-icheck.js')}}"></script>

    <!-- File Manager -->
    <script src="{{asset('vendor/unisharp/laravel-filemanager/public/js/lfm.js')}}"></script>

    <!-- Angular Multi Select -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-multi-select/isteven-multi-select.js')}}"></script>

    <!-- UI Bootstrap Modal --> 
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-bootstrap/ui-bootstrap-tpls-2.0.0.min.js')}}"></script>

    <!-- Angular Toastr -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-toastr/angular-toastr.tpls.js')}}"></script>

    <!-- Angular Confirm Dialog -->
    <script type="text/javascript" src="{{asset('public/system/plugins/bootbox/bootbox.min.js')}}"></script>

    <script src="{{asset('/public/system/plugins/tinymce/tinymce.min.js')}}"></script>

    <script src="{{asset('/public/system/plugins/angular-tinymce/angular-tinymce.js')}}"></script>
    <script src="{{asset('/public/system/plugins/angular-tinymce/langs/vi_VN.js')}}"></script>

    <!-- Angular Chosen -->
    <script type="text/javascript" src="{{asset('public/system/plugins/chosen/chosen.jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-chosen/angular-chosen.js')}}"></script>

    <!-- JS -->
    <script type="text/javascript" src="{{asset('public/system/common/system.common.angurlar.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/common/system.createproduct.angurlar.js')}}"></script>



    <script type="text/javascript">
        //Minify Left Menu
        // //$("body").addClass("sidebar-collapse");

        $(document).ready(function() { 
            $(window).on('load', function() {  
                //Laravel File Manager
                

                //insert all your ajax callback code here. 
                //Which will run only after page is fully loaded in background.
                $("#addReLoading").hide();
            });
                $('#selectImages').filemanager('images');

                $('#selectImages1').filemanager('images');
                $('#selectImages2').filemanager('images');
                $('#selectImages3').filemanager('images');
                $('#selectImages4').filemanager('images');
            
            

            
        });

        // $(window).bind('beforeunload', function(){
        //   alert( 'Are you sure you want to leave?');
        // });

        // $( "#dialog-modal" ).on( "dialogopen", function( event, ui ) {
        //     console.log('Wayhay!!');
        //     window.open("http://www.google.com");
        // } );
    </script>

@stop

@section('container')
<div class="row" ng-app="createProductApp" ng-controller="createProductController" ng-init="pageInit()">

    <form class="form" class="form-vertical" name="frmAdd" novalidate>
        <div class="col-xs-12">
            <div class="box box-danger">
                <!--=========================== header====================== -->
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-fw fa-motorcycle" aria-hidden="true"></i> Chi Tiết Sản Phẩm</h3>
                    <span> | </span>
                    <input
                        tabindex="0"
                        bs-switch
                        name="sbl_status"
                        ng-model="model.request.sbl_status"
                        type="checkbox"
                        switch-active="true"
                        switch-readonly="false"
                        switch-size="normal"
                        switch-animate="true"
                        switch-label=""
                        switch-icon="icon-save"
                        switch-on-text="Đăng Bán"
                        switch-off-text="Ngưng Bán"
                        switch-on-color="success"
                        switch-off-color="danger"
                        switch-radio-off="false"
                        switch-label-width="0px"
                        switch-handle-width="auto"
                        switch-inverse="false"
                        switch-change="onChange()"
                        ng-true-value="1"
                        ng-false-value="0">
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-fw fa-minus"></i></button>
                   </div>
                </div>
                <!-- ==============.box-content ===========-->
                <div class="box-body">
                    <!--=============== UPDATE BUTTON GROUP=============-->
                    <div class="form-group" ng-show="model.datainit.pageStatus == 'update'">
                        <a class="btn btn-default" href="{{URL::to('System/ListProducts')}}" title="CreateNews">
                            <i class="fa fa-fw fa-arrow-left" aria-hidden="true"></i>
                            <span class="hidden-xs">Trở Về</span>
                        </a>
                        <div class="pull-right">&nbsp;</div>
                        <button type="button" 
                                tabindex="99"
                                ng-click="updateItem(1)"
                                class="btn btn-warning pull-right"
                                ng-disabled="frmAdd.$invalid">
                            <i class="fa fa-fw fa-edit" aria-hidden="true"></i>
                            <span class="hidden-xs">Cập Nhật</span>
                        </button>
                        <div class="pull-right">&nbsp;</div>
                      
                    </div>
                    <!--=============== INSERT BUTTON GROUP==============-->
                    <div class="form-group" ng-show="model.datainit.pageStatus == 'add'">
                        <a class="btn btn-default" href="{{URL::to('System/ListProducts')}}" title="CreateNews">
                            <i class="fa fa-fw fa-arrow-left" aria-hidden="true"></i>
                            <span class="hidden-xs">Trở Về</span>
                        </a>

                        <button type="button" 
                                tabindex="99"
                                ng-click="addItem(1)"
                                class="btn btn-primary pull-right"
                                ng-disabled="frmAdd.$invalid"
                                title="Thêm Mới">
                            <i class="fa fa-fw fa-plus" aria-hidden="true"></i>
                            <span class="hidden-xs">Thêm Mới</span>
                        </button>.
                        <div class="pull-right">&nbsp;</div>
                    </div>
                    <!--=============== SLUG ==============-->
                    <div class="form-group" style="display:none;">
                        <input  
                            type="text"
                            class="form-control" 
                            id="txtSlug" 
                            name="txtSlug" 
                            placeholder="Slug" 
                            autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                            value="{{$slug}}">
                    </div>
                    <!--=================== MAIN CONTENT ================-->
                    <div class="nav-tabs-custom">
                        <!--=========== Ul Tab ==========-->
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tabInfo" data-toggle="tab"><i class="fa fa-fw fa-info-circle" aria-hidden="true"></i>&nbsp;<span class="hidden-xs">Thông Tin Chung</span></a></li>
                            <li><a href="#tabGalleries" data-toggle="tab"><i class="fa fa-fw fa-picture-o" aria-hidden="true"></i>&nbsp;<span class="hidden-xs">Ảnh Trình Chiếu</span></a></li>
                            <li><a href="#tabSEO" data-toggle="tab"><i class="fa fa-fw fa-search" aria-hidden="true"></i>&nbsp;<span class="hidden-xs">Thông Tin SEO</span></a></li>
                        </ul>
                        <!--========== tab Content ===========-->
                        <div class="tab-content">
                            <div class="active tab-pane" id="tabInfo">
                                <div class="row">
                                    <!-- Hinh Anh SP -->
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group"  >
                                            <label for="selectImages" class="col-sm-12 control-label">Hình Ảnh Sản Phẩm:</label>    
                                            <div class="form-group col-xs-12 col-md-12">
                                                <div class="input-group">
                                                    <span class="input-group-btn">
                                                        <a  id="selectImages" 
                                                            name="selectImages" 
                                                            data-input="thumbnail" 
                                                            data-preview="holder" 
                                                            class="btn btn-primary">
                                                            <i class="fa fa-fw fa-picture-o"></i>
                                                            <span>Chọn Ảnh</span>
                                                        </a>
                                                    </span>
                                                    <input  
                                                        tabindex="5"
                                                        id="thumbnail" 
                                                        class="form-control" 
                                                        type="text" 
                                                        name="filepath" 
                                                        placeholder="Chọn Ảnh Đại Diện"
                                                        ng-model="model.request.txtImages" 
                                                        autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                                                    <i  class="fa fa-fw fa-times pull-right" 
                                                        style="position: absolute;right: 7px;z-index: 2;top: 1px; background-color: rgba(255,255,255,0.8);padding: 8px;" 
                                                        ng-click="resetChoiceThumbnail()"></i> 
                                                </div>
                                            </div> 
                                            <div class="form-group col-xs-12 col-sm-6 col-sm-offset-3"> 
                                                <img ng-src="[[model.request.txtImages]]"
                                                     onError="this.src='{{asset('public/images/default-image.png')}}';"
                                                     id="holder" 
                                                     class="img-responsive img-thumbnail" 
                                                     alt="Chọn Ảnh Đại Diện" 
                                                     title="Chọn Ảnh Đại Diện" 
                                                     height="150px">
                                            </div>
                                        </div>

                                    </div>
                                    <!-- Thong Tin SP -->
                                    <div class="col-sm-6">
                                        <!-- =========Chọn txt_cate_id============ -->
                                        <div class="form-group col-sm-12" ng-class="{ 'has-error' : frmAdd.txt_cate_id.$invalid && !frmAdd.txt_cate_id.$pristine }">
                                            <label for="slbListCategories" class="control-label">Chọn Nhóm:</label>
                                            <isteven-multi-select
                                                tabindex="2"
                                                name="slbListCategories"
                                                input-model="model.datainit.slbListCategories"
                                                output-model="model.request.slbListCategories"
                                                helper-elements=""
                                                button-label="cate_name"
                                                item-label="cate_name"
                                                output-properties="cate_id"
                                                search-property="cate_name"
                                                tick-property="ticked"
                                                orientation="vertical"
                                                disable-property="true"
                                                is-disabled="false"
                                                selection-mode="single"
                                                min-search-length="10"
                                                translation="model.localLangCate"
                                                max-labels="1"  
                                                directive-id="slbCate"
                                                on-reset="slbClear('txt_cate_id')"
                                                on-item-click="slbItemClick(data,'txt_cate_id')">
                                            </isteven-multi-select>
                                            <span class="help-block" ng-show="frmAdd.txt_cate_id.$error.required && frmAdd.txt_cate_id.$invalid && !frmAdd.txt_cate_id.$pristine">Vui Lòng Chọn Nhóm.</span>
                                            <input  
                                                type="text"
                                                class="form-control" 
                                                id="txt_cate_id" 
                                                name="txt_cate_id" 
                                                ng-model="model.request.txt_cate_id"
                                                autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style="display:none;" 
                                                required >
                                        </div>
                                        <!-- =========Chọn txt_unit_id============ -->
                                        <div class="form-group col-sm-12" ng-class="{ 'has-error' : frmAdd.txt_unit_id.$invalid && !frmAdd.txt_unit_id.$pristine }">
                                            <label for="slbListUnits" class="control-label">Chọn Đơn Vị Tính:</label>
                                            <isteven-multi-select
                                                tabindex="2"
                                                name="slbListUnits"
                                                input-model="model.datainit.slbListUnits"
                                                output-model="model.request.slbListUnits"
                                                helper-elements=""
                                                button-label="unit_name"
                                                item-label="unit_name"
                                                output-properties="unit_id"
                                                search-property="unit_name"
                                                tick-property="ticked"
                                                orientation="vertical"
                                                disable-property="true"
                                                is-disabled="false"
                                                selection-mode="single"
                                                min-search-length="10"
                                                translation="model.localLangCate"
                                                max-labels="1"  
                                                directive-id="slbCate"
                                                on-reset="slbClear('txt_unit_id')"
                                                on-item-click="slbItemClick(data,'txt_unit_id')">
                                            </isteven-multi-select>
                                            <span class="help-block" ng-show="frmAdd.txt_unit_id.$error.required && frmAdd.txt_unit_id.$invalid && !frmAdd.txt_unit_id.$pristine">Vui Lòng Chọn ĐVT.</span>
                                            <input  
                                                type="text"
                                                class="form-control" 
                                                id="txt_unit_id" 
                                                name="txt_unit_id" 
                                                ng-model="model.request.txt_unit_id"
                                                autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style="display:none;" 
                                                required >
                                        </div>
                                        <!-- =========Chọn txt_origin_id============ -->
                                        <div class="form-group col-sm-12" ng-class="{ 'has-error' : frmAdd.txt_origin_id.$invalid && !frmAdd.txt_origin_id.$pristine }">
                                            <label for="slbListOrigins" class="control-label">Chọn Xuất Xứ:</label>
                                            <isteven-multi-select
                                                tabindex="2"
                                                name="slbListOrigins"
                                                input-model="model.datainit.slbListOrigins"
                                                output-model="model.request.slbListOrigins"
                                                helper-elements=""
                                                button-label="origin_name"
                                                item-label="origin_name"
                                                output-properties="origin_id"
                                                search-property="origin_name"
                                                tick-property="ticked"
                                                orientation="vertical"
                                                disable-property="true"
                                                is-disabled="false"
                                                selection-mode="single"
                                                min-search-length="10"
                                                translation="model.localLangCate"
                                                max-labels="1"  
                                                directive-id="slbListOrigins"
                                                on-reset="slbClear('txt_origin_id')"
                                                on-item-click="slbItemClick(data,'txt_origin_id')">
                                            </isteven-multi-select>
                                            <span class="help-block" ng-show="frmAdd.txt_origin_id.$error.required && frmAdd.txt_origin_id.$invalid && !frmAdd.txt_origin_id.$pristine">Vui Lòng Chọn Xuất Xứ.</span>
                                            <input  
                                                type="text"
                                                class="form-control" 
                                                id="txt_origin_id" 
                                                name="txt_origin_id" 
                                                ng-model="model.request.txt_origin_id"
                                                autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style="display:none;" 
                                                required >
                                        </div>
                                        <!-- =========Chọn txt_manufacturer_id============ -->
                                        <div class="form-group col-sm-12" ng-class="{ 'has-error' : frmAdd.txt_manufacturer_id.$invalid && !frmAdd.txt_manufacturer_id.$pristine }">
                                            <label for="slbListManufacturer" class="control-label">Chọn Nhà Sản Xuất:</label>
                                            <isteven-multi-select
                                                tabindex="2"
                                                name="slbListManufacturer"
                                                input-model="model.datainit.slbListManufacturer"
                                                output-model="model.request.slbListManufacturer"
                                                helper-elements=""
                                                button-label="manufacturer_name"
                                                item-label="manufacturer_name"
                                                output-properties="manufacturer_id"
                                                search-property="manufacturer_name"
                                                tick-property="ticked"
                                                orientation="vertical"
                                                disable-property="true"
                                                is-disabled="false"
                                                selection-mode="single"
                                                min-search-length="10"
                                                translation="model.localLangCate"
                                                max-labels="1"  
                                                directive-id="slbListOrigins"
                                                on-reset="slbClear('txt_manufacturer_id')"
                                                on-item-click="slbItemClick(data,'txt_manufacturer_id')">
                                            </isteven-multi-select>
                                            <span class="help-block" ng-show="frmAdd.txt_manufacturer_id.$error.required && frmAdd.txt_manufacturer_id.$invalid && !frmAdd.txt_manufacturer_id.$pristine">Vui Lòng Chọn NSX.</span>
                                            <input  
                                                type="text"
                                                class="form-control" 
                                                id="txt_manufacturer_id" 
                                                name="txt_manufacturer_id" 
                                                ng-model="model.request.txt_manufacturer_id"
                                                autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style="display:none;" 
                                                required >
                                        </div>
                                        <!-- =========Chọn txt_color_id============ -->
                                        <div class="form-group col-sm-12" ng-class="{ 'has-error' : frmAdd.txt_color_id.$invalid && !frmAdd.txt_color_id.$pristine }">
                                            <label for="slbListColor" class="control-label">Chọn Màu Sắc:</label>
                                            <isteven-multi-select
                                                tabindex="2"
                                                name="slbListColor"
                                                input-model="model.datainit.slbListColor"
                                                output-model="model.request.slbListColor"
                                                helper-elements=""
                                                button-label="color_name"
                                                item-label="color_name"
                                                output-properties="color_id"
                                                search-property="color_name"
                                                tick-property="ticked"
                                                orientation="vertical"
                                                disable-property="true"
                                                is-disabled="false"
                                                selection-mode="single"
                                                min-search-length="10"
                                                translation="model.localLangCate"
                                                max-labels="1"  
                                                directive-id="slbListColor"
                                                on-reset="slbClear('txt_color_id')"
                                                on-item-click="slbItemClick(data,'txt_color_id')">
                                            </isteven-multi-select>
                                            <span class="help-block" ng-show="frmAdd.txt_color_id.$error.required && frmAdd.txt_color_id.$invalid && !frmAdd.txt_color_id.$pristine">Vui Lòng Chọn Màu Sắc.</span>
                                            <input  
                                                type="text"
                                                class="form-control" 
                                                id="txt_color_id" 
                                                name="txt_color_id" 
                                                ng-model="model.request.txt_color_id"
                                                autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style="display:none;" 
                                                required >
                                        </div>
                                        <!-- =========Chọn slb_List_Tag============ -->
                                        <div class="form-group col-sm-12">
                                            <label for="slbListColor" class="control-label">Chọn Thẻ Tag:</label>
                                            <isteven-multi-select
                                                tabindex="2"
                                                name="slbTag"
                                                input-model="model.datainit.slbListTags"
                                                output-model="model.request.slbTag"
                                                helper-elements="filter reset"
                                                button-label="tag_name"
                                                item-label="tag_name"
                                                output-properties="tag_id"
                                                search-property="tag_name"
                                                tick-property="ticked"
                                                orientation="vertical"
                                                disable-property="false"
                                                is-disabled="false"
                                                selection-mode="mutiple"
                                                min-search-length="10"
                                                translation="model.localLangCate"
                                                max-labels="1"  
                                                directive-id="slbTag"
                                                on-reset="slbClear('txt_tag_id')"
                                                on-item-click="slbItemClick(data,'txt_tag_id')">
                                            </isteven-multi-select>
                                            <input  
                                                type="text"
                                                class="form-control" 
                                                id="txt_tag_id" 
                                                name="txt_tag_id" 
                                                ng-model="model.request.txt_tag_id"
                                                autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style="display:none;">
                                        </div>
                                        
                                    </div>
                                </div> 
                                <div class="row">
                                    <!-- ======= txt_product_name ======== -->
                                    <div class="form-group col-sm-12" ng-class="{ 'has-error' : frmAdd.txt_product_name.$touched && frmAdd.txt_product_name.$invalid && !frmAdd.txt_product_name.$pristine }">
                                        <label for="txtTitle" class="control-label display-block-xs">Tên :</label>  
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-fw fa-motorcycle" aria-hidden="true"></i>
                                            </span>
                                            <input  
                                                tabindex="1"
                                                type="text"
                                                class="form-control" 
                                                id="txt_product_name" 
                                                name="txt_product_name" 
                                                placeholder="Nhập Tên " 
                                                ng-model="model.request.txt_product_name"
                                                autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                                ng-minlength="2"
                                                ng-maxlength="100"
                                                maxlength="100"
                                                required >
                                        </div>
                                        <span class="help-block" ng-show="frmAdd.txt_product_name.$touched && frmAdd.txt_product_name.$error.required && frmAdd.txt_product_name.$invalid && !frmAdd.txt_product_name.$pristine">Vui Lòng Nhập Tên .</span>
                                        <span class="help-block" ng-show="frmAdd.txt_product_name.$touched && frmAdd.txt_product_name.$error.minlength">Tên Không Được Ngắn Hơn 2 Ký Tự.</span>
                                        <span class="help-block" ng-show="frmAdd.txt_product_name.$touched && frmAdd.txt_product_name.$error.maxlength">Tên Không Được Dài Hơn 100 Ký Tự.</span>
                                    </div>
                                    <!--txt_product_imported_prices-->
                                    <div style="display: none;" class="form-group col-sm-4" ng-class="{ 'has-error' : frmAdd.txt_product_imported_prices.$touched && frmAdd.txt_product_imported_prices.$invalid && !frmAdd.txt_product_imported_prices.$pristine }">
                                        <label for="txtTitle" class="control-label display-block-xs">Giá Bán :</label>  
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-fw fa-motorcycle" aria-hidden="true"></i>
                                            </span>
                                            <input tabindex="1"
                                                type="text" 
                                                class="form-control" 
                                                ng-model="model.request.txt_product_imported_prices" 
                                                placeholder="Nhập Giá " 
                                                autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                                min="1000" 
                                                max="999999999999" 
                                                ng-required="true"
                                                fraction="0"
                                                currency-symbol="VNĐ "
                                                hard-cap="true"
                                                ng-currency="true"
                                                id="txt_product_imported_prices"
                                                name="txt_product_imported_prices">
                                        </div>
                                    </div>
                                    <!--txt_product_retail_prices-->
                                    <div class="form-group col-sm-4" ng-class="{ 'has-error' : frmAdd.txt_product_retail_prices.$touched && frmAdd.txt_product_retail_prices.$invalid && !frmAdd.txt_product_retail_prices.$pristine }">
                                        <label for="txtTitle" class="control-label display-block-xs">Giá Niêm Yết:</label>  
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-fw fa-motorcycle" aria-hidden="true"></i>
                                            </span>
                                            <input tabindex="1"
                                                type="text" 
                                                class="form-control" 
                                                ng-model="model.request.txt_product_retail_prices" 
                                                placeholder="Nhập Giá " 
                                                autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                                min="1000" 
                                                max="999999999999" 
                                                ng-required="true"
                                                fraction="0"
                                                currency-symbol="VNĐ "
                                                hard-cap="true"
                                                ng-currency="true"
                                                id="txt_product_retail_prices"
                                                name="txt_product_retail_prices">
                                        </div>
                                    </div>
                                    <!-- === Release Date / Expiration Date ============-->
                                    <div class="form-group col-sm-4" ng-class="{ 'has-error' : frmAdd.dtpPublishes.$invalid && !frmAdd.dtpPublishes.$pristine }">
                                        <label for="dtpPublishes" class="control-label">Thời Gian Phát Hành:</label>
                                        <div >
                                            <input  
                                                date-range-picker 
                                                class="form-control date-picker" 
                                                type="text" 
                                                ng-model="model.request.dtpPublishes" 
                                                options="model.datePickerOpts"
                                                clearable="true"
                                                id="dtpPublishes"
                                                name="dtpPublishes"
                                                tabindex="1"
                                                placeholder="Thời Gian Phát Hành"
                                                required />
                                            <span class="help-block" ng-show="frmAdd.dtpPublishes.$error.required && frmAdd.dtpPublishes.$invalid && !frmAdd.dtpPublishes.$pristine">Vui Lòng Chọn Thời Gian Phát Hành.</span>
                                        </div>
                                    </div>
                                    <!--======== Switch btn =======-->
                                    <div class="form-group col-sm-4" ng-class="{ 'has-error' : frmAdd.txt_product_retail_prices.$touched && frmAdd.txt_product_retail_prices.$invalid && !frmAdd.txt_product_retail_prices.$pristine }">
                                        <label for="txtTitle" class="control-label display-block-xs">Giảm Giá :</label>  
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-fw fa-motorcycle" aria-hidden="true"></i>
                                            </span>
                                            <input
                                                tabindex="0"
                                                bs-switch
                                                name="sw_product_saleoff_pinned"
                                                ng-model="model.request.sw_product_saleoff_pinned"
                                                type="checkbox"
                                                switch-active="true"
                                                switch-readonly="false"
                                                switch-size="normal"
                                                switch-animate="true"
                                                switch-label=""
                                                switch-icon="icon-save"
                                                switch-on-text="Có"
                                                switch-off-text="Không"
                                                switch-on-color="success"
                                                switch-off-color="danger"
                                                switch-radio-off="false"
                                                switch-label-width="0px"
                                                switch-handle-width="auto"
                                                switch-inverse="false"
                                                switch-change="onChange()"
                                                ng-true-value="1"
                                                ng-false-value="0">
                                        </div>
                                    </div>
                                    <!--======== sw_product_new_pinned =======-->
                                    <div class="form-group col-sm-4" ng-class="{ 'has-error' : frmAdd.sw_product_new_pinned.$touched && frmAdd.sw_product_new_pinned.$invalid && !frmAdd.sw_product_new_pinned.$pristine }">
                                        <label for="txtTitle" class="control-label display-block-xs">Mới :</label>  
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-fw fa-motorcycle" aria-hidden="true"></i>
                                            </span>
                                            <input
                                                tabindex="0"
                                                bs-switch
                                                name="sw_product_new_pinned"
                                                ng-model="model.request.sw_product_new_pinned"
                                                type="checkbox"
                                                switch-active="true"
                                                switch-readonly="false"
                                                switch-size="normal"
                                                switch-animate="true"
                                                switch-label=""
                                                switch-icon="icon-save"
                                                switch-on-text="Có"
                                                switch-off-text="Không"
                                                switch-on-color="success"
                                                switch-off-color="danger"
                                                switch-radio-off="false"
                                                switch-label-width="0px"
                                                switch-handle-width="auto"
                                                switch-inverse="false"
                                                switch-change="onChange()"
                                                ng-true-value="1"
                                                ng-false-value="0">
                                        </div>
                                    </div>
                                    <!--======== sw_product_quick_pinned =======-->
                                    <div class="form-group col-sm-4" ng-class="{ 'has-error' : frmAdd.sw_product_quick_pinned.$touched && frmAdd.sw_product_quick_pinned.$invalid && !frmAdd.sw_product_quick_pinned.$pristine }">
                                        <label for="txtTitle" class="control-label display-block-xs">Bán Chạy :</label>  
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-fw fa-motorcycle" aria-hidden="true"></i>
                                            </span>
                                            <input
                                                tabindex="0"
                                                bs-switch
                                                name="sw_product_quick_pinned"
                                                ng-model="model.request.sw_product_quick_pinned"
                                                type="checkbox"
                                                switch-active="true"
                                                switch-readonly="false"
                                                switch-size="normal"
                                                switch-animate="true"
                                                switch-label=""
                                                switch-icon="icon-save"
                                                switch-on-text="Có"
                                                switch-off-text="Không"
                                                switch-on-color="success"
                                                switch-off-color="danger"
                                                switch-radio-off="false"
                                                switch-label-width="0px"
                                                switch-handle-width="auto"
                                                switch-inverse="false"
                                                switch-change="onChange()"
                                                ng-true-value="1"
                                                ng-false-value="0">
                                        </div>
                                    </div>
                                    <!-- ======= Mô Tả Ngắn ======== -->
                                    <div class="form-group col-sm-12" ng-class="{ 'has-error' : frmAdd.txt_product_description.$touched && frmAdd.txt_product_description.$invalid && !frmAdd.txt_product_description.$pristine }">
                                        <label for="txtTitle" class="control-label display-block-xs">Mô Tả Ngắn :<request class="txt_product_description"></request></label>  
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-fw fa-motorcycle" aria-hidden="true"></i>
                                            </span>
                                            <input  
                                                tabindex="1"
                                                type="text"
                                                class="form-control" 
                                                id="txt_product_description" 
                                                name="txt_product_description" 
                                                placeholder="Nhập Mô Tả Ngắn " 
                                                ng-model="model.request.txt_product_description"
                                                autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                                ng-minlength="2"
                                                ng-maxlength="100"
                                                maxlength="100"
                                                required >
                                        </div>
                                        <span class="help-block" ng-show="frmAdd.txt_product_description.$touched && frmAdd.txt_product_description.$error.required && frmAdd.txt_product_description.$invalid && !frmAdd.txt_product_description.$pristine">Vui Lòng Nhập Mô Tả Ngắn .</span>
                                        <span class="help-block" ng-show="frmAdd.txt_product_description.$touched && frmAdd.txt_product_description.$error.minlength">Mô Tả Ngắn Không Được Ngắn Hơn 2 Ký Tự.</span>
                                        <span class="help-block" ng-show="frmAdd.txt_product_description.$touched && frmAdd.txt_product_description.$error.maxlength">Mô Tả Ngắn Không Được Dài Hơn 100 Ký Tự.</span>
                                    </div>

                                    <!-- date time picker hiden-->
                                    <div style="display: none;" class="form-group col-sm-12">
                                        <label for="dtpImport" class="control-label">Ngày Nhập:</label>
                                        <div class="dropdown">
                                            <a class="dropdown-toggle" id="dropdownImport" role="button" data-toggle="dropdown">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                    <input tabindex="8" type="text" ng-model="model.request.dtpImport" class="form-control" id="dtpImport" name="dtpImport">
                                                </div>
                                            </a>
                                            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                                <datetimepicker data-ng-model="model.request.dtpImport" data-datetimepicker-config="model.datePickerOptions"/>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- a_product_specifications  -->
                                <div class="row">
                                    <div class="form-group col-sm-12"  ng-class="{ 'has-error' : frmAdd.a_product_specifications.$touched && frmAdd.a_product_specifications.$invalid && !frmAdd.a_product_specifications.$pristine }">
                                        <label for="txaContent" class="control-label hidden-xs">Nội Dung Chi Tiết:</label>    
                                        <textarea 
                                            tabindex="3"
                                            id="a_product_specifications" 
                                            name="a_product_specifications" 
                                            ui-tinymce="model.tinymceOptions" 
                                            ng-model="model.request.a_product_specifications" 
                                            required>
                                        </textarea>
                                        <span class="help-block" ng-show="frmAdd.a_product_specifications.$touched && frmAdd.a_product_specifications.$error.required && frmAdd.a_product_specifications.$invalid && !frmAdd.a_product_specifications.$pristine">Vui Lòng Nhập Nội Dung Chi Tiết Bài Viết.</span>
                                    </div>
                                </div>
                            </div>

                            <!-- tab-tabGalleries -->
                            <div class="tab-pane" id="tabGalleries">

                                <div class="row">
                                    <!-- hinh anh 1 -->
                                    <div class="form-group col-xs-12 col-sm-4" style="display: inline-block;">
                                        <div class="col-xs-12">
                                            <div class="input-group col-md-12">
                                                <span class="input-group-btn">
                                                    <a  id="selectImages1" 
                                                        name="selectImages1" 
                                                        data-input="thumbnail1" 
                                                        data-preview="holder1" 
                                                        class="btn btn-primary">
                                                        <i class="fa fa-fw fa-picture-o"></i>
                                                        <span>Chọn Ảnh</span>
                                                    </a>

                                                </span>
                                                <input  
                                                    id="thumbnail1" 
                                                    class="form-control" 
                                                    type="text" 
                                                    name="filepath" 
                                                    placeholder="Chọn Ảnh"
                                                    ng-model="model.request.txtImages1" 
                                                  
                                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                                            </div>
                                        </div> 
                                        <div class="col-xs-12 col-sm-6 col-sm-offset-3"> 
                                            <img ng-src="[[model.request.txtImages1]]"
                                                 onError="this.src='{{asset('public/images/default-image.png')}}';"
                                                 id="holder1" 
                                                 class="img-responsive img-thumbnail" 
                                                 alt="Chọn Ảnh" 
                                                 title="Chọn Ảnh" 
                                                 height="150px">
                                        </div>
                                    </div>
                                    <!-- hinh anh 2 -->
                                    <div class="form-group col-xs-12 col-sm-4" style="display: inline-block;">
                                        <div class="col-xs-12">
                                            <div class="input-group col-md-12">
                                                <span class="input-group-btn">
                                                    <a  id="selectImages2" 
                                                        name="selectImages2" 
                                                        data-input="thumbnail2" 
                                                        data-preview="holder2" 
                                                        class="btn btn-primary">
                                                        <i class="fa fa-fw fa-picture-o"></i>
                                                        <span>Chọn Ảnh</span>
                                                    </a>
                                                </span>
                                                <input  
                                                    id="thumbnail2" 
                                                    class="form-control" 
                                                    type="text" 
                                                    name="filepath" 
                                                    placeholder="Chọn Ảnh"
                                                    ng-model="model.request.txtImages2" 
                                                  
                                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                                             
                                            </div>
                                        </div> 
                                        <div class="col-xs-12 col-sm-6 col-sm-offset-3"> 
                                            <img ng-src="[[model.request.txtImages2]]"
                                                 onError="this.src='{{asset('public/images/default-image.png')}}';"
                                                 id="holder2" 
                                                 class="img-responsive img-thumbnail" 
                                                 alt="Chọn Ảnh" 
                                                 title="Chọn Ảnh" 
                                                 height="150px">
                                        </div>
                                    </div>
                                    <!-- hinh anh 2 -->
                                    <div class="form-group col-xs-12 col-sm-4" style="display: inline-block;">
                                        <div class="col-xs-12">
                                            <div class="input-group col-md-12">
                                                <span class="input-group-btn">
                                                    <a  id="selectImages3" 
                                                        name="selectImages3" 
                                                        data-input="thumbnail3" 
                                                        data-preview="holder3" 
                                                        class="btn btn-primary">
                                                        <i class="fa fa-fw fa-picture-o"></i>
                                                        <span>Chọn Ảnh</span>
                                                    </a>
                                                </span>
                                                <input  
                                                    id="thumbnail3" 
                                                    class="form-control" 
                                                    type="text" 
                                                    name="filepath" 
                                                    placeholder="Chọn Ảnh"
                                                    ng-model="model.request.txtImages3" 
                                              
                                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                                            </div>
                                        </div> 
                                        <div class="col-xs-12 col-sm-6 col-sm-offset-3"> 
                                            <img ng-src="[[model.request.txtImages3]]"
                                                 onError="this.src='{{asset('public/images/default-image.png')}}';"
                                                 id="holder3" 
                                                 class="img-responsive img-thumbnail" 
                                                 alt="Chọn Ảnh" 
                                                 title="Chọn Ảnh" 
                                                 height="150px">
                                        </div>
                                    </div>
                                </div>
                              
                            </div>

                            <!-- tab-tabtabSEO -->
                            <div class="tab-pane" id="tabSEO">

                                <div class="row">
                                    <label for="slbTags" class="control-label col-md-12">Thẻ Sản Phẩm</label>
                                    <div class="form-group col-md-12">
                                        
                                        <select chosen multiple
                                                ng-model="model.request.slbTags"
                                                ng-options="cate.tag_id as cate.tag_name for cate in model.datainit.slbListTags"
                                                no-results-text="'Không Có Thẻ Sản Phẩm Phù Hợp'"
                                                placeholder-text-multiple="'Chọn Thẻ Sản Phẩm'"
                                                data-placeholder-text-single="'Chọn Thẻ Sản Phẩm'"
                                                data-placeholder="'Chọn Thẻ Sản Phẩm'"
                                                disable-search="false"
                                                allow-single-deselect="false"
                                                max-selected-options="0"
                                                display-disabled-options="true"
                                                display-selected-options="true"
                                                search-contains="true"
                                                include-group-label-in-selected="true"
                                                max-shown-results="10">
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-sm-12" ng-class="{ 'has-error' : frmAdd.txtKeywordSEO.$touched && frmAdd.txtKeywordSEO.$invalid && !frmAdd.txtKeywordSEO.$pristine }">
                                        <label for="txtKeywordSEO" class="control-label display-block-xs">Keyword Sản Phẩm:</label>
                                        <textarea
                                                tabindex="2"
                                                class="form-control" 
                                                id="txtKeywordSEO" 
                                                name="txtKeywordSEO" 
                                                placeholder="Nhập Keyword Sản Phẩm" 
                                                ng-model="model.request.txtKeywordSEO"
                                                autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                                ng-minlength="2"
                                                ng-maxlength="150"
                                                maxlength="150"
                                         ></textarea>
                                        <span class="help-block" ng-show="frmAdd.txtKeywordSEO.$touched && frmAdd.txtKeywordSEO.$touched && frmAdd.txtKeywordSEO.$error.minlength">Keyword Không Được Ngắn Hơn 2 Ký Tự.</span>
                                        <span class="help-block" ng-show="frmAdd.txtKeywordSEO.$touched &&frmAdd.txtKeywordSEO.$touched && frmAdd.txtKeywordSEO.$error.maxlength">Keyword Không Được Dài Hơn 150 Ký Tự.</span>
                                    </div>
                                </div>
                            </div>
                            <!-- /.tab-pane -->

                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- /.nav-tabs-custom -->
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </form>
    <!--/ form action-->
</div>
@stop