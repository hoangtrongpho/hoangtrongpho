@extends('system.layouts.index')

@section('headTitle')
   <title>{{ Lang::get('messages.system_heade_title') }}</title>
@stop

@section('pageTitle')
   <h1>
        <i class="fa fa-fw fa-motorcycle" aria-hidden="true"></i> Danh Sách Sản Phẩm
        <small>Skyfire Sytem Version 1.0</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{URL::to('/System/Dashboard')}}"><i class="fa fa-fw fa-dashboard"></i> Hệ Thống Quản Trị Website</a>
        </li>
        <li class="active"><i class="fa fa-fw fa-motorcycle" aria-hidden="true"></i> Quản Lý Sản Phẩm</li>
    </ol>
@stop

@section('headCss')
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/daterangepicker/daterangepicker.css')}}"/>
<link rel="stylesheet" href="{{asset('public/system/plugins/angular-ui-switch/angular-ui-switch.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-ui-switch/bootstrap-switch.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-multi-select/isteven-multi-select.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-toastr/angular-toastr.css')}}" />

<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/jquery.dataTables.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/dataTables.bootstrap.css')}}" />

<style type="text/css">
    .control-label{
        padding-top: 5px;
        padding-right: 5px !important;
        padding-left: 5px !important;
        float: left;
    }

    #listResult {
        width: 100% !important;
    }
    .multiSelect .buttonLabel{
        width: auto !important;
    }
</style>
@stop

@section('headJs')
    <!-- Date Time Picker -->
    <script type="text/javascript" src="{{asset('public/system/plugins/daterangepicker/daterangepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-daterangepicker/angular-daterangepicker.min.js')}}"></script>

    <script src="{{asset('public/system/plugins/angular-ui-switch/angular-ui-switch.js')}}"></script>
    <script src="{{asset('public/system/plugins/angular-ui-switch/bootstrap-switch.js')}}"></script>
    <script src="{{asset('public/system/plugins/angular-ui-switch/angular-bootstrap-switch.js')}}"></script>

    <!-- iCheck -->
    <script src="{{asset('public/system/plugins/angular_bootstrap_checkbox/angular-bootstrap-checkbox.js')}}"></script>

    <!-- File Manager -->
    <!-- Angular Multi Select -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-multi-select/isteven-multi-select.js')}}"></script>

    <!-- UI Bootstrap Modal --> 
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-bootstrap/ui-bootstrap-tpls-2.0.0.min.js')}}"></script>

    <!-- Angular Toastr -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-toastr/angular-toastr.tpls.js')}}"></script>

    <!-- Angular Confirm Dialog -->
    <script type="text/javascript" src="{{asset('public/system/plugins/bootbox/bootbox.min.js')}}"></script>

    <!-- Angular Datatables-->
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/angular-datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/colreorder/angular-datatables.colreorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/bootstrap/angular-datatables.bootstrap.min.js')}}"></script>

    <!-- JS -->
    <script type="text/javascript" src="{{asset('public/system/common/system.common.angurlar.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/common/system.listproducts.angurlar.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function() { 
            $(window).on('load', function() {  
            });


            //Minify Left Menu
            // //$("body").addClass("sidebar-collapse");
        });
    </script>
@stop

@section('container')
<div class="row" ng-app="bikeApp" ng-controller="bikeController" ng-init="pageInit()">

    <div class="col-lg-12" style="display: none;">
        <div class="box box-success collapsed-box">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-fw fa-sliders" aria-hidden="true"></i> Tìm Kiếm Nâng Cao</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-fw fa-minus"></i>
                  </button>
               </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <form class="form" name="frmSearch" id="frmSearch" novalidate>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">

                                <div class="form-group" ng-class="{ 'has-error' : frmAddNewsCate.txtNewsName.$invalid && !frmAddNewsCate.txtNewsName.$pristine }">
                                    <label for="txtNewsName" class="control-label col-md-1 text-right">Tên Sản Phẩm:</label>
                                    <div class="col-md-4">
                                        <input  type="text"
                                                class="form-control" 
                                                id="txtNewsName" 
                                                name="txtNewsName" 
                                                placeholder="Tên Sản Phẩm" 
                                                ng-model="model.request.txtNewsName"
                                                autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                                ng-maxlength="15"
                                                maxlength="15">
                                        <span class="help-block" ng-show="frmAddNewsCate.txtNewsName.$error.maxlength">Tên Không Được Dài Hơn 15 Ký Tự.</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="slbCateParent" class="control-label col-md-1 text-right">Danh Muc:</label>
                                    <div class="col-md-6">
                                        <isteven-multi-select
                                            name="slbCateParent"
                                            input-model="model.datainit.slbListCategories"
                                            output-model="model.request.slbCateParent"
                                            helper-elements="all filter reset"
                                            button-label="cate_name"
                                            item-label="cate_name"
                                            output-properties="cate_id"
                                            search-property="cate_name"
                                            tick-property="ticked"
                                            orientation="vertical"
                                            disable-property="false"
                                            is-disabled="false"
                                            selection-mode="multiple"
                                            min-search-length="10"
                                            translation="model.localLang"
                                            max-labels="6"  
                                            directive-id="slbCateParent"
                                            on-reset="slbClear()"
                                            on-item-click="slbItemClick(data)">
                                        </isteven-multi-select>
                                    </div>
                                    <input  type="text"
                                            class="form-control" 
                                            id="txtCateParent" 
                                            name="txtCateParent" 
                                            ng-model="model.request.txtCateParent"
                                            autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style="display:none;" 
                                            required >
                                </div>


                            </div>
                        </div>
                    </div>

                    <div class="row" style="margin-top: 3px;">
                        <div class="col-sm-12">
                            <div class="form-group">

                                <div class="form-group pull-right">
                                    <div class="col-md-3">
                                        <button type="button" 
                                                ng-click="loadNewsList()"
                                                class="btn btn-success">
                                            <i class="fa fa-fw fa-search" aria-hidden="true"></i>
                                            <span>Lọc Danh Sách</span>
                                        </button>
                                    </div>
                                </div>
                            
                            </div>
                        </div>
                    </div>

                </form>
                

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->

    <div class="col-lg-12">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-fw fa-list" aria-hidden="true"></i> Danh Sách Sản Phẩm</h3>
                <div class="box-tools pull-right">
                    <a class="btn btn-primary" href="{{URL::to('System/CreateProduct')}}" title="Thêm Mới Sản Phẩm">
                        <i class="fa fa-fw fa-plus" aria-hidden="true"></i>
                        <span>Nhập Sản phẩm</span>
                    </a>
                </div>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <table  name="listResult" 
                                id="listResult" 
                                style="width: 100%;" 
                                datatable="ng" 
                                dt-options="dtOptions"
                                dt-column-defs="dtColumnDefs"
                                class="table table-bordered table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th class="text-center" data-column-index="0" style="width: 120px;">Tùy Chọn</th>
                                    <th class="text-center" data-column-index="1" style="width: auto;">Ảnh</th>
                                    <th class="text-center" data-column-index="2" style="width: auto;">Danh Mục</th>
                                    <th class="text-center" data-column-index="2" style="width: auto;">Tên SP</th>
                               
                                    <th class="text-center" data-column-index="4" style="width: auto;">Giá Niêm Yết</th>
                                   
                                    <th class="text-center" data-column-index="6" style="width: auto;">Trạng Thái</th>
                                    <th class="text-center" data-column-index="7" style="width: auto;">Lượt Xem</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr role="row" ng-repeat="product in model.datainit.dtgList">
                                    <td class="text-center">
                                        <a href="{{URL::to('System/UpdateProduct/[[product.product_slug]]')}}" class="btn btn-warning" title="[[news.news_title]]"><i class="fa fa-fw fa-edit fa-fw" aria-hidden="true"></i></a>

                                        <button ng-show="product.status == 1" type="button" ng-click="changeStatus(product.product_id, 0)" class="btn btn-danger"><i class="fa fa-fw fa-eye-slash fa-fw" aria-hidden="true"></i></button>

                                        <button ng-show="product.status == 0" type="button" ng-click="changeStatus(product.product_id, 1)" class="btn btn-success"><i class="fa fa-fw fa-eye fa-fw" aria-hidden="true"></i></button>

                                        <button type="button" ng-click="removeItem(product.product_id)" class="btn btn-danger"><i class="fa fa-fw fa-trash-o fa-fw"></i></button>

                                        <span style="display:none;">[[product.product_slug]]</span>
                                    </td>
                                    <td>
                                        <img ng-src="[[product.product_thumbnail]]" src="" style="width: 150px;" title="[[product.product_name]]" alt="[[product.product_slug]]">
                                    </td>
                                    <td>
                                        <div style="width: auto">[[product.product_name]]</div>
                                    </td>
                                    <td class="text-center">
                                        <div style="width: auto">[[product.cate_name]]</div>
                                    </td>
                                  
                                    <td class="text-center">
                                        <div style="width: auto">[[product.product_retail_prices | currency:"":0]] VNĐ</div>
                                    </td>
                                    <td class="text-center" style="width: auto">
                                        <span style="display:none;">[[product.status]]</span>
                                        <span ng-show="product.status == 1" role="button" class="btn btn-success">Đang Bán</span>
                                        <span ng-show="product.status == 0" role="button" class="btn btn-danger">Ngưng Bán</span>
                                    </td>
                                    <td class="text-    " style="width: auto">
                                        [[product.views_couter]] Lượt
                                    </td>
                                    
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <!-- /.box-body -->

            <!-- Loading (remove the following to stop the loading)-->
            <div id="listReLoading" class="overlay">
                <i class="fa fa-fw fa-refresh fa-spin"></i>
            </div>
            <!-- end loading -->

        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
@stop