@extends('system.layouts.index')

@section('headTitle')
   <title>{{ Lang::get('messages.system_heade_title') }}</title>
@stop

@section('pageTitle')
   <h1>
        <i class="fa fa-fw fa-hashtag" aria-hidden="true"></i> Màu Sắc Sản Phẩm
        <small>{{ Lang::get('messages.system_verion') }}</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{URL::to('/System/Dashboard')}}"><i class="fa fa-fw fa-dashboard"></i> Hệ Thống Quản Trị Website</a>
        </li>
        <li class="active"><i class="fa fa-fw fa-hashtag" aria-hidden="true"></i> Màu Sắc Sản Phẩm</li>
    </ol>
@stop

@section('headCss')
  <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-toastr/angular-toastr.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/jquery.dataTables.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/dataTables.bootstrap.css')}}" />

  <!-- Angularjs Bootstrap Color Picker -->
  <link rel="stylesheet" href="{{asset('public/system/plugins/angular-bootstrap-color-picker/css/colorpicker.min.css')}}" />
@stop

@section('headJs')
    <!-- UI Bootstrap Modal --> 
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-bootstrap/ui-bootstrap-tpls-2.0.0.min.js')}}"></script>

    <!-- Angularjs Bootstrap Color Picker -->
    <script src="{{asset('public/system/plugins/angular-bootstrap-color-picker/js/bootstrap-colorpicker-module.min.js')}}"></script>
    
    <!-- Angular Datatables-->
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/angular-datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/colreorder/angular-datatables.colreorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/bootstrap/angular-datatables.bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/tabletools/angular-datatables.tabletools.min.js')}}"></script>
    <!-- JS -->
    <script type="text/javascript" src="{{asset('public/system/common/system.listproductcolors.angurlar.js')}}"></script>
@stop

@section('container')
<div class="row" ng-app="productColorApp" ng-controller="productColorsController" ng-init="pageInit()">

    <!-- Modal -->
    <div id="modalAdd" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <form class="form" class="form-inline" name="frmAdd" id="frmAdd" novalidate>
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-blue">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-fw fa-floppy-o" aria-hidden="true"></i> Thêm Mới</h4>
                </div>
                <div class="modal-body">
                    
                    <div class="col-xs-12">
                        <div class="form-group" ng-class="{ 'has-error' : frmAdd.txtName.$touched && frmAdd.txtName.$invalid && !frmAdd.txtName.$pristine }">
                            <label for="txtName" class="control-label">Tên Màu Sắc:</label>
                            <input  tabindex="1"
                                    type="text"
                                    class="form-control" 
                                    id="txtName" 
                                    name="txtName" 
                                    placeholder="Nhập Tên Màu Sắc" 
                                    ng-model="model.request.txtName"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                    ng-minlength="2"
                                    ng-maxlength="50"
                                    maxlength="50"
                                    required >
                            <span class="help-block" ng-show="frmAdd.txtName.$touched && frmAdd.txtName.$error.required && frmAdd.txtName.$invalid && !frmAdd.txtName.$pristine">Vui Lòng Nhập Tên Màu Sắc.</span>
                            <span class="help-block" ng-show="frmAdd.txtName.$touched && frmAdd.txtName.$error.minlength">Tên Màu Sắc Không Được Ngắn Hơn 2 Ký Tự.</span>
                            <span class="help-block" ng-show="frmAdd.txtName.$touched && frmAdd.txtName.$error.maxlength">Tên Màu Sắc Không Được Dài Hơn 50 Ký Tự.</span>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="form-group" ng-class="{ 'has-error' : frmAdd.pckColor.$touched && frmAdd.pckColor.$invalid && !frmAdd.pckColor.$pristine }">
                            <label for="pckColor" class="control-label">Chọn Màu Sắc:</label>
                            <div class="input-group">
                                <input tabindex="2"
                                       class="form-control"
                                       type="text" 
                                       id="pckColor" 
                                       name="pckColor" 
                                       placeholder="Chọn Màu Sắc" 
                                       colorpicker="rgba" 
                                       colorpicker-position="bottom" 
                                       colorpicker-size="200"
                                       ng-model="model.request.pckColor" 
                                       autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                       type="text" 
                                       ng-minlength="4"
                                       ng-maxlength="22"
                                       maxlength="22"
                                       required>
                                <div class="input-group-addon" style="background-color: [[model.request.pckColor]];"></div>
                            </div>
                            <span class="help-block" ng-show="frmAdd.pckColor.$touched && frmAdd.pckColor.$error.required && frmAdd.pckColor.$invalid && !frmAdd.pckColor.$pristine">Vui Lòng Chọn Màu Sắc.</span>
                            <span class="help-block" ng-show="frmAdd.pckColor.$touched && frmAdd.pckColor.$error.minlength">Màu Sắc Không Được Ngắn Hơn 4 Ký Tự.</span>
                            <span class="help-block" ng-show="frmAdd.pckColor.$touched && frmAdd.pckColor.$error.maxlength">Màu Sắc Không Được Dài Hơn 22 Ký Tự.</span>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="txtDescription" class="control-label">Mô Tả Ngắn:</label>
                            <input  tabindex="3"
                                    type="text"
                                    class="form-control" 
                                    id="txtDescription" 
                                    name="txtDescription" 
                                    placeholder="Nhập Mô Tả Ngắn" 
                                    ng-model="model.request.txtDescription"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                    maxlength="50">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal" tabindex="5">
                        <i class="fa fa-fw fa-ban" aria-hidden="true"></i>&nbsp;Đóng
                    </button>
                    <button ng-disabled="frmAdd.$invalid" 
                            ng-click="addItem()"
                            tabindex="4" 
                            data-loading-text="<i class='fa fa-refresh fa-spin fa-fw'></i> Đang xử lý"
                            type="submit" class="btn btn-primary" id="btnAdd" name="btnAdd">
                        <i class="fa fa-fw fa-plus" aria-hidden="true"></i>&nbsp;Thêm Mới
                    </button>
                </div>
            </div>
        </form>
      </div>
    </div>

    <!-- Modal -->
    <div id="modalUpdate" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <form class="form" class="form-inline" name="frmUpdate" id="frmUpdate" novalidate>
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-yellow">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-fw fa-pencil-square-o" aria-hidden="true"></i> Cập Nhật</h4>
                </div>
                <div class="modal-body">

                    <div class="col-xs-12">
                        <div class="form-group" ng-class="{ 'has-error' : frmAdd.txtName.$touched && frmAdd.txtName.$invalid && !frmAdd.txtName.$pristine }">
                            <label for="txtName" class="control-label">Màu Sắc:</label>
                            <input  tabindex="1"
                                    type="text"
                                    class="form-control" 
                                    id="txtName" 
                                    name="txtName" 
                                    placeholder="Nhập Tên Màu Sắc" 
                                    ng-model="model.request.txtName"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                    ng-minlength="2"
                                    ng-maxlength="50"
                                    maxlength="50"
                                    required >
                            <span class="help-block" ng-show="frmAdd.txtName.$touched && frmAdd.txtName.$error.required && frmAdd.txtName.$invalid && !frmAdd.txtName.$pristine">Vui Lòng Nhập Tên Màu Sắc.</span>
                            <span class="help-block" ng-show="frmAdd.txtName.$touched && frmAdd.txtName.$error.minlength">Tên Màu Sắc Không Được Ngắn Hơn 2 Ký Tự.</span>
                            <span class="help-block" ng-show="frmAdd.txtName.$touched && frmAdd.txtName.$error.maxlength">Tên Màu Sắc Không Được Dài Hơn 50 Ký Tự.</span>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="form-group" ng-class="{ 'has-error' : frmAdd.pckColor.$touched && frmAdd.pckColor.$invalid && !frmAdd.pckColor.$pristine }">
                            <label for="pckColor" class="control-label">Chọn Màu Sắc:</label>
                            <div class="input-group">
                                <input tabindex="2"
                                       class="form-control"
                                       type="text" 
                                       id="pckColor" 
                                       name="pckColor" 
                                       placeholder="Chọn Màu Sắc" 
                                       colorpicker="rgba" 
                                       colorpicker-position="bottom" 
                                       colorpicker-size="200"
                                       ng-model="model.request.pckColor" 
                                       autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                       type="text" 
                                       ng-minlength="4"
                                       ng-maxlength="22"
                                       maxlength="22"
                                       required>
                                <div class="input-group-addon" style="background-color: [[model.request.pckColor]];"></div>
                            </div>                            
                            <span class="help-block" ng-show="frmAdd.pckColor.$touched && frmAdd.pckColor.$error.required && frmAdd.pckColor.$invalid && !frmAdd.pckColor.$pristine">Vui Lòng Chọn Màu Sắc.</span>
                            <span class="help-block" ng-show="frmAdd.pckColor.$touched && frmAdd.pckColor.$error.minlength">Màu Sắc Không Được Ngắn Hơn 4 Ký Tự.</span>
                            <span class="help-block" ng-show="frmAdd.pckColor.$touched && frmAdd.pckColor.$error.maxlength">Màu Sắc Không Được Dài Hơn 22 Ký Tự.</span>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="txtDescription" class="control-label">Mô Tả Ngắn:</label>
                            <input  tabindex="3"
                                    type="text"
                                    class="form-control" 
                                    id="txtDescription" 
                                    name="txtDescription" 
                                    placeholder="Nhập Mô Tả Ngắn" 
                                    ng-model="model.request.txtDescription"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                    maxlength="50">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal" tabindex="5">
                        <i class="fa fa-fw fa-ban" aria-hidden="true"></i>&nbsp;Đóng
                    </button>
                    <button ng-disabled="frmUpdate.$invalid" 
                            ng-click="updateItem()"
                            tabindex="4" 
                            data-loading-text="<i class='fa fa-refresh fa-spin fa-fw'></i> Đang xử lý"
                            type="submit" class="btn btn-warning" id="btnUpdate" name="btnUpdate">
                        <i class="fa fa-fw fa-pencil-square-o" aria-hidden="true"></i>&nbsp;Cập Nhật
                    </button>
                </div>
            </div>
        </form>
      </div>
    </div>

    <div class="col-lg-12 col-xs-12">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-fw fa-hashtag" aria-hidden="true"></i> Danh Sách Màu Sắc</h3>
                <div class="box-tools pull-right">
                <button id="sendMail" type="button" class="btn btn-primary" tabindex="4" data-toggle="modal" data-target="#modalAdd">
                    <i class="fa fa-fw fa-plus" aria-hidden="true"></i>
                    <span class="hidden-xs"> Thêm Mới</span>
                </button>
               </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                    <div class="row">
                        <div class="col-sm-12">
                            <table  name="listResult" 
                                    datatable="ng" 
                                    dt-options="dtOptions"
                                    dt-column-defs="dtColumnDefs"
                                    width="100%" 
                                    class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th class="visible-xs" style="width:2px; display: none;" data-column-index="0" ></th>
                                        <th class="text-center" style="width:75px;" data-column-index="1" >Tùy Chọn</th>
                                        <th class="text-center" style="width:70px;" data-column-index="2" >Mã Màu</th>
                                        <th class="text-center" style="width:20%;"  data-column-index="3" >Màu Sắc</th>
                                        <th class="text-center" style="width:auto;" data-column-index="4" >Mô Tả</th>
                                        <th class="text-center" style="width:auto;" data-column-index="5" >Số Sản Phẩm</th>
                                        <th class="text-center" style="width:20%;"  data-column-index="6" >Ngày Cập Nhật</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr role="row" ng-repeat="item in model.datainit.dtgList">
                                        <td class="visible-xs" style="display: none;"></td>
                                        <td class="text-center" >
                                            <span ng-show="item.status !== 9">
                                                <button type="button" 
                                                        ng-click="openUpdateItem(item)" 
                                                        id="update_[[item.color_id]]" 
                                                        name="update_[[item.color_id]]" 
                                                        class="btn btn-warning"><i class="fa fa-fw fa-edit"></i></button>
                                                <button type="button"
                                                        ng-click="removeItem(item)" 
                                                        id="delete_[[item.color_id]]" 
                                                        name="delete_[[item.color_id]]" 
                                                        data-loading-text="<i class='fa fa-refresh fa-spin fa-fw'></i>"
                                                        class="btn btn-danger"><i class="fa fa-fw fa-trash-o"></i></button>
                                            </span>
                                        </td>
                                        <td class="text-center">
                                            <div style="width: 30px; height: 30px; background: [[item.color_code]];margin-left:35%;">&nbsp;</div>
                                        </td>
                                        <td>[[item.color_name]]</td>
                                        <td>[[item.color_description]]</td>
                                        <td class="text-center">
                                            <span ng-show="!item.item_couter">0</span>
                                            <span ng-show="item.item_couter">[[item.item_couter]]</span>
                                        </td>
                                        <td class="text-center">[[item.updated_date | date:'dd/MM/yyyy HH:mm:ss']]</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
            <!-- /.box-body -->

            <!-- Loading (remove the following to stop the loading)-->
            <div id="listReLoading" class="overlay">
                <i class="fa fa-fw fa-refresh fa-spin"></i>
            </div>
            <!-- end loading -->

        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
@stop