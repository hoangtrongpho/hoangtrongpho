@extends('system.layouts.index')

@section('headTitle')
   <title>{{ Lang::get('messages.system_heade_title') }}</title>
@stop

@section('pageTitle')
   <h1>
        <i class="fa fa-fw fa-archive" aria-hidden="true"></i> Quản Trị Đơn Hàng
        <small>Skyfire Sytem Version 1.0</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{URL::to('/System/Dashboard')}}"><i class="fa fa-fw fa-dashboard"></i> Hệ Thống Quản Trị Website</a>
        </li>
        <li class="active"><i class="fa fa-fw fa-archive" aria-hidden="true"></i> Danh Sách Đơn Hàng</li>
    </ol>
@stop

@section('headCss')
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/daterangepicker/daterangepicker.css')}}"/>
    <link rel="stylesheet" href="{{asset('public/system/plugins/angular-ui-switch/angular-ui-switch.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-ui-switch/bootstrap-switch.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-multi-select/isteven-multi-select.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-toastr/angular-toastr.css')}}" />

    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/jquery.dataTables.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/dataTables.bootstrap.css')}}" />

    <style type="text/css">
        .control-label{
            padding-top: 5px;
            padding-right: 5px !important;
            padding-left: 5px !important;
            float: left;
        }

        #listResult {
            width: 100% !important;
        }
    </style>
@stop

@section('headJs')
    <!-- Date Time Picker -->
    <script type="text/javascript" src="{{asset('public/system/plugins/daterangepicker/daterangepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-daterangepicker/angular-daterangepicker.min.js')}}"></script>

    <script src="{{asset('public/system/plugins/angular-ui-switch/angular-ui-switch.js')}}"></script>
    <script src="{{asset('public/system/plugins/angular-ui-switch/bootstrap-switch.js')}}"></script>
    <script src="{{asset('public/system/plugins/angular-ui-switch/angular-bootstrap-switch.js')}}"></script>

    <!-- iCheck -->
    <script src="{{asset('public/system/plugins/angular_bootstrap_checkbox/angular-bootstrap-checkbox.js')}}"></script>
    <!-- Input Currency -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-ng-currency/ng-currency.directive.js')}}"></script>
    <!-- File Manager -->
    <!-- Angular Multi Select -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-multi-select/isteven-multi-select.js')}}"></script>

    <!-- UI Bootstrap Modal --> 
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-bootstrap/ui-bootstrap-tpls-2.0.0.min.js')}}"></script>

    <!-- Angular Toastr -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-toastr/angular-toastr.tpls.js')}}"></script>

    <!-- Angular Confirm Dialog -->
    <script type="text/javascript" src="{{asset('public/system/plugins/bootbox/bootbox.min.js')}}"></script>

    <!-- Angular Datatables-->
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/angular-datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/colreorder/angular-datatables.colreorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/bootstrap/angular-datatables.bootstrap.min.js')}}"></script>

    <!-- JS -->
    <script type="text/javascript" src="{{asset('public/system/common/system.common.angurlar.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/common/system.listinvoices.angurlar.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function() { 
            $(window).on('load', function() {  
            });
            //Minify Left Menu
            //$("body").addClass("sidebar-collapse");
        });
    </script>
@stop

@section('container')
<div class="row" ng-app="invoiceApp" ng-controller="invoiceController"  ng-init="pageInit()">
    <!-- Modal -->
    <div id="modalAdd" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
           <form class="form" class="form-inline" name="frmAdd" id="frmAdd" novalidate>
              <!-- Modal content-->
              <div class="modal-content">
                 <div class="modal-header bg-blue">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                 </div>
                 <div class="modal-body">
                    <!--  Group Thông Tin hóa Đơn--> 
                    <div class="panel-group">
                      <div class="panel panel-primary">
                        <div class="panel-heading">Thông Tin Chung</div>
                        <div class="panel-body">
                          <!--  Group Thông Tin hóa Đơn-->
                          <div class="row"> 
                             <!--   ===== Số HD ======= -->
                             <div class="col-md-6"> 
                                <div class="input-group">
                                   <b>
                                      Mã Đơn Hàng:
                                   </b>
                                   [[model.request.txtInvoiceNumber]]
                                </div>
                             </div>
                             <!--   ===== Ngày Đặt ======= -->
                             <div class="col-md-6"> 
                                <div class="input-group">
                                   <b>
                                      Ngày Đặt:
                                   </b>
                                   [[model.request.txtUpdatedDate]]
                                </div>
                             </div>
                             <!--   ========= Customer name ========== -->
                             <div class="col-md-6" >
                                <div class="input-group">
                                   <b>
                                        Tên Khách Hàng:
                                   </b>
                                   [[model.request.txtCustomerName]]
                                </div>
                             </div>
                             <!--  ======== Customer Email ========= -->
                             <div class="col-md-6"> 
                                <div class="input-group">
                                   <b>
                                      Email Khách Hàng:
                                   </b>
                                   [[model.request.txtCustomerEmail]]
                                </div>
                             </div>
                             <!--   ==== Customer Phone number ======= -->
                             <div class="col-md-6"> 
                                <div class="input-group">
                                   <b>
                                        Số Điện Thoại:
                                   </b>
                                   [[model.request.txtCustomerPhone]]
                                </div>
                             </div>
                             <!--   ====== Customer address ====== -->
                             <div class="col-md-6">
                                <div class="input-group">
                                   <b>
                                      Địa Chỉ Khách Hàng:
                                   </b>
                                   [[model.request.txtCustomerAddress]]
                                </div>
                             </div>
                             <!--   ============== ghi chu============= -->
                             <div class="col-md-12" style="display: none;">
                                <div class="input-group">
                                   <b>
                                      Ghi Chú:
                                   </b>
                                   [[model.request.txtInvoiceNote]]
                                </div>
                             </div>
                          </div>
                          <!--  END Group Thông Tin hóa Đơn-->
                        </div>
                      </div>
                    </div>
                    <!--  END Group Thông Tin hóa Đơn-->
                    <!-- GROUP Thông Tin Nhận Hàng--> 
                    <div class="panel-group">
                      <div class="panel panel-primary">
                        <div class="panel-heading">Thông Tin Địa Chỉ Nhận Hàng</div>
                        <div class="panel-body">
                          <!-- GROUP Thông Tin Nhận Hàng-->
                          <div class="row"> 
                             <!--   ============== txtBranchName============ -->
                             <div class="col-md-6" >
                                <div class="input-group">
                                   <b>
                                      Tên Chi Nhánh Nhận Hàng:
                                   </b>
                                   [[model.request.txtBranchName]]
                                </div>
                             </div>
                             <!--   ============== txtBranchPhone============= -->
                             <div class="col-md-6" >
                                <div class="input-group">
                                   <b>
                                      SĐT Chi Nhánh:
                                   </b>
                                  [[model.request.txtBranchPhone]]
                                </div>
                             </div>
                             <!--   ============== txtBranchAddress============= -->
                             <div class="col-md-12" > 
                                <div class="input-group">
                                   <b>
                                      Địa Chỉ Chi Nhánh Nhận Hàng:
                                   </b>
                                   [[model.request.txtBranchAddress]]
                                </div>
                             </div>
                          </div>
                          <!-- END GROUP Thông Tin Nhận Hàng-->
                        </div>
                      </div>
                    </div>
                    <!-- END GROUP Thông Tin Nhận Hàng-->
                    <!--  Group Thông Tin Đơn Hàng--> 
                    <div class="panel-group">
                      <div class="panel panel-primary">
                        <div class="panel-heading">Chi Tiết Đơn Hàng</div>
                        <div class="panel-body">
                          <!--   total sum -->
                          <div class="row"> 
                             <div class="input-group col-sm-6">
                                <span class="input-group-addon">
                                   Tổng Thành Tiền:
                                </span>
                                <span class="input-group-addon">
                                   [[model.request.txtTotalSum | number]]
                                </span>
                                <span class="input-group-addon">
                                   VNĐ
                                </span>
                             </div>
                          </div>
                          <!--   pro list -->
                          <div class="row"> 
                            <div class="col-sm-12">
                                <table  
                                      style="color: darkblue" 
                                      name="listResult" 
                                      id="listResult" 
                                      style="width: 100%;"
                                      dt-options="dtOptions"
                                      dt-column-defs="dtColumnDefs"
                                      class="table table-bordered table-striped table-condensed">
                                    <thead>
                                        <tr>
                                            <th class="text-center" data-column-index="1" style="width: 100px;">Hình Ảnh</th>
                                            <th class="text-center" data-column-index="2" style="width: auto;">Tên Sản Phẩm</th>
                                            <th class="text-center" data-column-index="3" style="width: auto;">Số Lượng</th>
                                            <th class="text-center" data-column-index="4" style="width: auto;">Giá Bán</th>
                                            <th class="text-center" data-column-index="5" style="width: auto;">Chiết Khấu</th>
                                            <th class="text-center" data-column-index="6" style="width: auto;">Thành Tiền</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr role="row" ng-repeat="pro in model.request.dtgListProductInvoice">
                                            <td>
                                                <img ng-src="[[pro.product_thumbnail]]"
                                                 onError="this.src='{{asset('public/images/default-image.png')}}';"
                                                 id="holder1" 
                                                 class="img-responsive img-thumbnail" 
                                                 alt="Ảnh Đại Diện" 
                                                 title="Ảnh Đại Diện" 
                                                 height="50px">
                                            </td>
                                            <td>
                                              [[pro.product_name | limitTo:50]]
                                            </td>
                                            <td>
                                                [[pro.quantity]]
                                            </td>
                                            <td>
                                              [[pro.product_retail_prices | number]] VNĐ
                                            </td>
                                            <td>
                                              [[100- (pro.total_price *100/(pro.product_retail_prices*pro.quantity) )]] %
                                            </td>
                                            <td>
                                              [[pro.total_price | number]] VNĐ
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- END Group Thông Tin Đơn Hàng--> 
                 </div>
              </div>
           </form>
        </div>
    </div>
    <!-- content -->
    <div class="col-lg-12">
        
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-fw fa-list" aria-hidden="true"></i> Danh Sách Đơn Hàng</h3>
                <!-- buton create -->
                <div class="box-tools pull-right">
                    <a class="btn btn-primary" href="{{URL::to('System/CreateInvoice')}}" title="Thêm Mới">
                        <i class="fa fa-fw fa-plus" aria-hidden="true"></i>
                        <span>Thêm Mới</span>
                    </a>
                    @if(Session::has('userAuth'))
                        <?php
                           $auth_id = Session::get('userAuth')->auth_id;
                        ?> 
                    @else
                        <?php
                           $auth_id = 0;
                        ?> 
                    @endif
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <th><span>Chú Thích:</span></th><th></th>
                                </tr>
                                <tr>
                                    <th>
                                        <button  title="Hủy" type="button" class="btn btn-default btn-xs"><i class="fa fa-window-close" aria-hidden="true"></i></button>
                                        Hủy</th>
                                    <th>
                                        <button  title="Chưa Xử Lý" type="button" class="btn btn-danger btn-xs"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></button>
                                        Chưa Xử Lý</th>
                                    <th>
                                        <button   title="Đang Xử Lý" type="button" class="btn btn-warning btn-xs"><i class="fa fa-play" aria-hidden="true"></i></button>
                                        Đang Xử Lý</th>
                                    <th>
                                        <button  title="Đã Nhận Hàng" type="button" class="btn btn-primary btn-xs"><i class="fa fa-gift" aria-hidden="true"></i></button>
                                        Đã Nhận Hàng</th>
                                    <th>
                                        <button   title="Hoàn Thành" type="button" class="btn btn-success btn-xs"><i class="fa fa-check" aria-hidden="true"></i></button>
                                        Hoàn Thành</th>
                                    <th>
                                        <button   title="Cảnh Báo" type="button" class="btn btn-danger btn-xs"><i class="fa fa-bomb" aria-hidden="true"></i></button>
                                        Cảnh Báo</th>
                                    <th>
                                        <button   title="Cảnh Báo" type="button" class="btn btn-success btn-xs"><i class="fa fa-star" aria-hidden="true"></i></button>
                                        Nhận Hàng Và Thanh Toán</th>
                                </tr>
                                
                            </table>
                        </div>
                    </div>
                        
                </div>
            </div>
        </div>

        <!-- lọc nâng cao -->
        <div class="row">
          <div class="col-xs-12 col-md-12">
             <div class="box box-danger collapsed-box">
                <div class="box-header with-border" >
                    <div class="form-group row">
                          <div class="col-md-6">
                              <label for="slbCustomer" class="control-label display-block-xs">Chọn Chi Nhánh:</label>
                              <div class="input-group">
                                 
                                  <isteven-multi-select
                                      name="slbCustomer"
                                      input-model="model.datainit.dtgListBranch"
                                      output-model="model.request.slbCustomer"
                                      helper-elements="filter"
                                      button-label="branch_name"
                                      item-label="branch_name"
                                      output-properties="branch_kid"
                                      search-property="branch_name"
                                      tick-property="ticked"
                                      orientation="vertical"
                                      disable-property="false"
                                      is-disabled="false"
                                      selection-mode="single"
                                      min-search-length="10"
                                      translation="model.localLang"
                                      max-labels="1"  
                                      directive-id="slbCustomer"
                                      on-item-click="SearchFunction(data)">
                                  </isteven-multi-select>
                              </div>
                          </div>
                    </div>
                </div>
                <div class="box-body">
                </div>
             </div>
          </div>
          <!-- /.col -->
        </div>
        <!-- Số Đơn Hàng Chưa Xử Lý -->
        <div class="row">
          <div class="col-xs-12 col-md-12">
             <div class="box box-danger collapsed-box">
                <div class="box-header with-border" data-widget="collapse">
                    <div class="alert alert-success" style="background-color: white !important">
                        <!-- Số Đơn Hàng Chưa Xử Lý: -->
                        <button type="button" class="btn btn-danger">Số Đơn Hàng Chưa Xử Lý: <span class="badge">[[(model.datainit.dtgListInvoice | filter: { status: 2 }).length]]</span></button>
                        <button type="button" class="btn btn-danger">Tổng Tiền: <span class="badge">[[model.datainit.sumStatus2/2 | number]] VNĐ</span></button>
                    </div>
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse">
                         <i style="color: blue;font-size: 18px" class="fa fa-fw fa-plus"></i>
                      </button>
                   </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <table  name="listResult" 
                                id="listResult" 
                                style="width: 100%;" 
                                datatable="ng" 
                                dt-options="dtOptions"
                                dt-column-defs="dtColumnDefs"
                                class="table table-bordered table-striped table-condensed">
                                <thead>
                                    <tr>
                                        <th class="text-center" data-column-index="0" style="width: 80px; background-color: #dd4b39">Tùy Chọn</th>
                                        <th class="text-center" data-column-index="1" style="width: auto;background-color: #dd4b39">Đổi Trạng Thái</th>
                                        <th class="text-center" data-column-index="2" style="width: auto;background-color: #dd4b39">Số Hóa Đơn</th>
                                        <th class="text-center" data-column-index="3" style="width: auto;background-color: #dd4b39">Khách Hàng</th>
                                        <th class=25ext-center" data-column-index="5" style="width: auto;background-color: #dd4b39">SDT</th>
                                        <th class="text-center" data-column-index="6" style="width: auto;background-color: #dd4b39">Ngày Lập</th>
                                        <th class="text-center" data-column-index="7" style="width: auto;background-color: #dd4b39">Tổng Trị Giá </th>
                                        <th class="text-center" data-column-index="8" style="width: auto;background-color: #dd4b39">Tình Trạng</th>
                                        <th class="text-center" data-column-index="8" style="width: auto;background-color: #dd4b39">Nơi Nhận Hàng</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr role="row" ng-repeat="inv in model.datainit.dtgListInvoice | filter: { status: 2 }">
                                        <td class="text-center">
                                            <a href="#" ng-click="loadInvoiceDetail(inv.invoice_id)" data-toggle="modal" data-target="#modalAdd"  class="label label-primary"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                            <a href="{{URL::to('System/UpdateInvoice/[[inv.invoice_id]]')}}" class="btn btn-warning btn-xs" title="[[inv.invoice_id]]"><i class="fa fa-fw fa-edit fa-fw" aria-hidden="true"></i></a>
                                            <button type="button" 
                                            ng-click="removeItem(inv.invoice_id)" 
                                            ng-show="{{$auth_id}}==1"
                                            class="btn btn-danger btn-xs"><i class="fa fa-fw fa-trash-o fa-fw"></i></button>
                                        </td>
                                        <td style="width: 140px !important">
                                            <button ng-show="inv.status!=1" ng-click="changeInvoiceStatus(inv.invoice_id,1)" title="Hủy" type="button" class="btn btn-default btn-xs"><i class="fa fa-window-close" aria-hidden="true"></i></button>
                                            <button ng-show="inv.status!=2" ng-click="changeInvoiceStatus(inv.invoice_id,2)" title="Chưa Xử Lý" type="button" class="btn btn-danger btn-xs"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></button>
                                            <button ng-show="inv.status!=3" ng-click="changeInvoiceStatus(inv.invoice_id,3)" title="Đang Xử Lý" type="button" class="btn btn-warning btn-xs"><i class="fa fa-play" aria-hidden="true"></i></button>
                                            <button ng-show="inv.status!=4" ng-click="changeInvoiceStatus(inv.invoice_id,4)" title="Đã Nhận Hàng" type="button" class="btn btn-primary btn-xs"><i class="fa fa-gift" aria-hidden="true"></i></button>
                                            <button ng-show="inv.status!=5" ng-click="changeInvoiceStatus(inv.invoice_id,5)" title="Hoàn Thành" type="button" class="btn btn-success btn-xs"><i class="fa fa-check" aria-hidden="true"></i></button>
                                            <button ng-show="inv.status!=7" ng-click="changeInvoiceStatus(inv.invoice_id,7)" title="Hoàn Thành Và Thanh Toán" type="button" class="btn btn-success btn-xs"><i class="fa fa-star" aria-hidden="true"></i></button>
                                        </td>
                                        <td>[[inv.invoice_number]]</td>
                                        <td>[[inv.customer_fullname]]</td>
                                        <td>[[inv.receiver_phone]]</td>
                                        <td>[[inv.created_date_format ]]</td>
                                        <td style="text-align: right;" 
                                        ng-init="model.datainit.sumStatus2 = model.datainit.sumStatus2 + inv.total_sum">
                                        [[inv.total_sum | number]] VNĐ
                                        </td>
                                        <td>
                                            <span ng-show="inv.status==2" class="label label-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp; Chưa Xử Lý</span>
                                        </td>
                                        <td>[[inv.branch_name]]</td>
                                    </tr>
                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                </div>
                <!-- end loading -->
             </div>
             <!-- /.box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- Số Đơn Hàng Đang Xử Lý -->
        <div class="row">
          <div class="col-xs-12 col-md-12">
             <div class="box box-danger collapsed-box">
                <div class="box-header with-border" data-widget="collapse">
                    <div class="alert alert-warning" style="background-color: white !important">
                        <!-- Số Đơn Hàng Chưa Xử Lý: -->
                        <button type="button" class="btn btn-warning">Số Đơn Hàng Đang Xử Lý: <span class="badge">[[(model.datainit.dtgListInvoice | filter: { status: 3 }).length]]</span></button>
                        <button type="button" class="btn btn-warning">Tổng Tiền: <span class="badge">[[model.datainit.sumStatus3/2 | number]] VNĐ</span></button>
                    </div>
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse">
                         <i style="color: blue;font-size: 18px" class="fa fa-fw fa-plus"></i>
                      </button>
                   </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <table  name="listResult" 
                                id="listResult" 
                                style="width: 100%;" 
                                datatable="ng" 
                                dt-options="dtOptions"
                                dt-column-defs="dtColumnDefs"
                                class="table table-bordered table-striped table-condensed">
                                <thead>
                                    <tr>
                                        <th class="text-center" data-column-index="0" style="width: 80px; background-color: #f39c12">Tùy Chọn</th>
                                        <th class="text-center" data-column-index="1" style="width: auto;background-color: #f39c12">Thay Đổi Trạng Thái</th>
                                        <th class="text-center" data-column-index="2" style="width: auto;background-color: #f39c12">Số Hóa Đơn</th>
                                        <th class="text-center" data-column-index="3" style="width: auto;background-color: #f39c12">Khách Hàng</th>
                                        <th class="text-center" data-column-index="5" style="width: auto;background-color: #f39c12">SDT</th>
                                        <th class="text-center" data-column-index="6" style="width: auto;background-color: #f39c12">Ngày Lập</th>
                                        <th class="text-center" data-column-index="7" style="width: auto;background-color: #f39c12">Tổng Trị Giá </th>
                                        <th class="text-center" data-column-index="8" style="width: auto;background-color: #f39c12">Tình Trạng</th>
                                        <th class="text-center" data-column-index="8" style="width: auto;background-color: #f39c12">Nơi Nhận Hàng</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr role="row" ng-repeat="inv in model.datainit.dtgListInvoice | filter: { status: 3 }">
                                        <td class="text-center">
                                            <a href="#" ng-click="loadInvoiceDetail(inv.invoice_id)" data-toggle="modal" data-target="#modalAdd"  class="label label-primary"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                            <a href="{{URL::to('System/UpdateInvoice/[[inv.invoice_id]]')}}" class="btn btn-warning btn-xs" title="[[inv.invoice_id]]"><i class="fa fa-fw fa-edit fa-fw" aria-hidden="true"></i></a>
                                            <button type="button" 
                                            ng-click="removeItem(inv.invoice_id)" 
                                            ng-show="{{$auth_id}}==1"
                                            class="btn btn-danger btn-xs"><i class="fa fa-fw fa-trash-o fa-fw"></i></button>
                                        </td>
                                        <td style="width: 140px !important">
                                            <button ng-show="inv.status!=1" ng-click="changeInvoiceStatus(inv.invoice_id,1)" title="Hủy" type="button" class="btn btn-default btn-xs"><i class="fa fa-window-close" aria-hidden="true"></i></button>
                                            <button ng-show="inv.status!=3" ng-click="changeInvoiceStatus(inv.invoice_id,3)" title="Đang Xử Lý" type="button" class="btn btn-warning btn-xs"><i class="fa fa-play" aria-hidden="true"></i></button>
                                            <button ng-show="inv.status!=4" ng-click="changeInvoiceStatus(inv.invoice_id,4)" title="Đã Nhận Hàng" type="button" class="btn btn-primary btn-xs"><i class="fa fa-gift" aria-hidden="true"></i></button>
                                            <button ng-show="inv.status!=5" ng-click="changeInvoiceStatus(inv.invoice_id,5)" title="Hoàn Thành" type="button" class="btn btn-success btn-xs"><i class="fa fa-check" aria-hidden="true"></i></button>
                                            <button ng-show="inv.status!=7" ng-click="changeInvoiceStatus(inv.invoice_id,7)" title="Hoàn Thành Và Thanh Toán" type="button" class="btn btn-success btn-xs"><i class="fa fa-star" aria-hidden="true"></i></button>
                                        </td>
                                        <td>[[inv.invoice_number]]</td>
                                        <td>[[inv.customer_fullname]]</td>
                                        <td>[[inv.receiver_phone]]</td>
                                        <td>[[inv.created_date_format ]]</td>
                                        <td style="text-align: right;" 
                                        ng-init="model.datainit.sumStatus3 = model.datainit.sumStatus3 + inv.total_sum">
                                        [[inv.total_sum | number]] VNĐ
                                        </td>
                                        <td>
                                            <span ng-show="inv.status==3" class="label label-warning"><i class="fa fa-play" aria-hidden="true"></i>&nbsp; Đang Xử Lý</span>
                                        </td>
                                        <td>[[inv.branch_name ]]</td>
                                    </tr>
                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                </div>
                <!-- end loading -->
             </div>
             <!-- /.box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- Số Đơn Hàng Đã Nhận Hàng -->
        <div class="row">
          <div class="col-xs-12 col-md-12">
             <div class="box box-danger collapsed-box">
                <div class="box-header with-border" data-widget="collapse">
                    <div class="alert alert-success" style="background-color: white !important">
                        <!-- Số Đơn Hàng Chưa Xử Lý: -->
                        <button type="button" class="btn btn-primary">Số Đơn Hàng Đã Nhận Hàng (Ghi Công Nợ): <span class="badge">[[(model.datainit.dtgListInvoice | filter: { status: 4 }).length]]</span></button>
                        <button type="button" class="btn btn-primary">Tổng Tiền: <span class="badge">[[model.datainit.sumStatus4/2 | number]] VNĐ</span></button>
                    </div>
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse">
                         <i style="color: blue;font-size: 18px" class="fa fa-fw fa-plus"></i>
                      </button>
                   </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <table  name="listResult" 
                                id="listResult" 
                                style="width: 100%;" 
                                datatable="ng" 
                                dt-options="dtOptions"
                                dt-column-defs="dtColumnDefs"
                                class="table table-bordered table-striped table-condensed">
                                <thead>
                                    <tr>
                                        <th class="text-center" data-column-index="0" style="width: 80px; ">Tùy Chọn</th>
                                        <th class="text-center" data-column-index="1" style="width: auto;">Thay Đổi Trạng Thái</th>
                                        <th class="text-center" data-column-index="2" style="width: auto;">Số Hóa Đơn</th>
                                        <th class="text-center" data-column-index="3" style="width: auto;">Khách Hàng</th>
                                        <th class="text-center" data-column-index="5" style="width: auto;">SDT</th>
                                        <th class="text-center" data-column-index="6" style="width: auto;">Ngày Lập</th>
                                        <th class="text-center" data-column-index="7" style="width: auto;">Tổng Trị Giá </th>
                                        <th class="text-center" data-column-index="8" style="width: auto;">Tình Trạng</th>
                                        <th class="text-center" data-column-index="8" style="width: auto;">Nơi Nhận Hàng</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr role="row" ng-repeat="inv in model.datainit.dtgListInvoice | filter: { status: 4 }">
                                        <td class="text-center">
                                            <a href="#" ng-click="loadInvoiceDetail(inv.invoice_id)" data-toggle="modal" data-target="#modalAdd"  class="label label-primary"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                        </td>
                                        <td style="width: 140px !important">
                                            <button ng-show="inv.status!=1" ng-click="changeInvoiceStatus(inv.invoice_id,1)" title="Hủy" type="button" class="btn btn-default btn-xs"><i class="fa fa-window-close" aria-hidden="true"></i></button>
                                            <button ng-show="inv.status!=5" ng-click="changeInvoiceStatus(inv.invoice_id,5)" title="Hoàn Thành" type="button" class="btn btn-success btn-xs"><i class="fa fa-check" aria-hidden="true"></i></button>
                                        </td>
                                        <td>[[inv.invoice_number]]</td>
                                        <td>[[inv.customer_fullname]]</td>
                                        <td>[[inv.receiver_phone]]</td>
                                        <td>[[inv.created_date_format ]]</td>
                                        <td style="text-align: right;" 
                                        ng-init="model.datainit.sumStatus4 = model.datainit.sumStatus4 + inv.total_sum">
                                        [[inv.total_sum | number]] VNĐ
                                        </td>
                                        <td>
                                            <span ng-show="inv.status==4" class="label label-primary"><i class="fa fa-gift" aria-hidden="true"></i>&nbsp; Đã Nhận Hàng</span>
                                        </td>
                                        <td>[[inv.branch_name]]</td>
                                    </tr>
                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                </div>
                <!-- end loading -->
             </div>
             <!-- /.box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- Số Đơn Hàng Hoàn  Thành -->
        <div class="row">
          <div class="col-xs-12 col-md-12">
           
             <div class="box box-danger collapsed-box">
                <div class="box-header with-border" data-widget="collapse">
                    <div class="alert alert-success" style="background-color: white !important">

                        <button type="button" class="btn btn-success">Số Đơn Hàng Hoàn Thành: <span class="badge">[[(model.datainit.dtgListInvoice | filter: filterFinishStatus).length]]</span></button>

                        <button type="button" class="btn btn-success">Tổng Tiền: <span class="badge">[[model.datainit.sumStatus57/2 | number]] VNĐ</span></button>
                    </div>
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse">
                         <i style="color: blue;font-size: 18px" class="fa fa-fw fa-plus"></i>
                      </button>
                   </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <table  name="listResult" 
                                id="listResult" 
                                style="width: 100%;" 
                                datatable="ng" 
                                dt-options="dtOptions"
                                dt-column-defs="dtColumnDefs"
                                class="table table-bordered table-striped table-condensed">
                                <thead>
                                    <tr>
                                        <th class="text-center" data-column-index="0" style="width: 80px; background-color: #00a65a">Tùy Chọn</th>
                                        <th class="text-center" data-column-index="2" style="width: auto;background-color: #00a65a">Số Hóa Đơn</th>
                                        <th class="text-center" data-column-index="3" style="width: auto;background-color: #00a65a">Khách Hàng</th>
                                        <th class="text-center" data-column-index="5" style="width: auto;background-color: #00a65a">SDT</th>
                                        <th class="text-center" data-column-index="6" style="width: auto;background-color: #00a65a">Ngày Lập</th>
                                        <th class="text-center" data-column-index="7" style="width: auto;background-color: #00a65a">Tổng Trị Giá </th>
                                        <th class="text-center" data-column-index="8" style="width: auto;background-color: #00a65a">Tình Trạng</th>
                                        <th class="text-center" data-column-index="8" style="width: auto;background-color: #00a65a">Nơi Nhận Hàng</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr role="row" ng-repeat="inv in model.datainit.dtgListInvoice | filter: filterFinishStatus">
                                        <td class="text-center">
                                            <a href="#" ng-click="loadInvoiceDetail(inv.invoice_id)" data-toggle="modal" data-target="#modalAdd"  class="label label-primary"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                        </td>
                                        <td>[[inv.invoice_number]]</td>
                                        <td>[[inv.customer_fullname]]</td>
                                        <td>[[inv.receiver_phone]]</td>
                                        <td>[[inv.created_date_format ]]</td>
                                        <td style="text-align: right;" 
                                        ng-init="model.datainit.sumStatus57 = model.datainit.sumStatus57 + inv.total_sum">
                                        [[inv.total_sum | number]] VNĐ
                                        </td>
                                        <td>
                                            <span ng-show="inv.status==5" class="label 
                                            label-success"><i class="fa fa-check" aria-hidden="true"></i>&nbsp; Hoàn Thành</span>
                                            <span ng-show="inv.status==7" class="label 
                                            label-success"><i class="fa fa-star" aria-hidden="true"></i>&nbsp; Nhận Hàng & Thanh Toán</span>
                                        </td>
                                        <td>[[inv.branch_name]]</td>
                                    </tr>
                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                </div>
                <!-- end loading -->
             </div>
             <!-- /.box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- Số Đơn Hàng Cảnh Báo -->
        <div class="row">
          <div class="col-xs-12 col-md-12">
             <div class="box box-danger collapsed-box">
                <div class="box-header with-border" data-widget="collapse">
                    <div class="alert alert-success" style="background-color: white !important">
                        <!-- Số Đơn Hàng Chưa Xử Lý: -->
                        <button type="button" class="btn btn-danger">Số Đơn Hàng Cảnh Báo (ghi công nợ): <span class="badge">[[(model.datainit.dtgListInvoice | filter: { status: 6 }).length]]</span></button>
                        <button type="button" class="btn btn-danger">Tổng Tiền: <span class="badge">[[model.datainit.sumStatus6/2 | number]] VNĐ</span></button>
                    </div>
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse">
                         <i style="color: blue;font-size: 18px" class="fa fa-fw fa-plus"></i>
                      </button>
                   </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <table  name="listResult" 
                                id="listResult" 
                                style="width: 100%;" 
                                datatable="ng" 
                                dt-options="dtOptions"
                                dt-column-defs="dtColumnDefs"
                                class="table table-bordered table-striped table-condensed">
                                <thead>
                                    <tr>
                                        <th class="text-center" data-column-index="0" style="width: 80px; background-color: #dd4b39">Tùy Chọn</th>
                                        <th class="text-center" data-column-index="1" style="width: auto;background-color: #dd4b39">Đổi Trạng Thái</th>
                                        <th class="text-center" data-column-index="2" style="width: auto;background-color: #dd4b39">Số Hóa Đơn</th>
                                        <th class="text-center" data-column-index="3" style="width: auto;background-color: #dd4b39">Khách Hàng</th>
                                        <th class="text-center" data-column-index="5" style="width: auto;background-color: #dd4b39">SDT</th>
                                        <th class="text-center" data-column-index="6" style="width: auto;background-color: #dd4b39">Ngày Lập</th>
                                        <th class="text-center" data-column-index="7" style="width: auto;background-color: #dd4b39">Tổng Trị Giá </th>
                                        <th class="text-center" data-column-index="8" style="width: auto;background-color: #dd4b39">Tình Trạng</th>
                                        <th class="text-center" data-column-index="8" style="width: auto;background-color: #dd4b39">Nơi Nhận Hàng</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr role="row" ng-repeat="inv in model.datainit.dtgListInvoice | filter: { status: 6 }">
                                        <td class="text-center">
                                            <a href="#" ng-click="loadInvoiceDetail(inv.invoice_id)" data-toggle="modal" data-target="#modalAdd"  class="label label-primary"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                        </td>
                                        <td style="width: 140px !important">
                                            <button ng-show="inv.status!=1" ng-click="changeInvoiceStatus(inv.invoice_id,1)" title="Hủy" type="button" class="btn btn-default btn-xs"><i class="fa fa-window-close" aria-hidden="true"></i></button>
                                            <button ng-show="inv.status!=5" ng-click="changeInvoiceStatus(inv.invoice_id,5)" title="Hoàn Thành" type="button" class="btn btn-success btn-xs"><i class="fa fa-check" aria-hidden="true"></i></button>
                                        </td>
                                        <td>[[inv.invoice_number]]</td>
                                        <td>[[inv.customer_fullname]]</td>
                                        <td>[[inv.receiver_phone]]</td>
                                        <td>[[inv.created_date_format ]]</td>
                                        <td style="text-align: right;" 
                                        ng-init="model.datainit.sumStatus6 = model.datainit.sumStatus6 + inv.total_sum">
                                        [[inv.total_sum | number]] VNĐ
                                        </td>
                                        <td>
                                            <span ng-show="inv.status==5" class="label label-success"><i class="fa fa-check" aria-hidden="true"></i>&nbsp; Hoàn Thành</span>
                                           
                                        </td>
                                        <td>[[inv.branch_name]]</td>
                                    </tr>
                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                </div>
                <!-- end loading -->
             </div>
             <!-- /.box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- Số Đơn Hàng Hủy -->
        <div class="row">
          <div class="col-xs-12 col-md-12">
             <div class="box box-danger collapsed-box">
                <div class="box-header with-border" data-widget="collapse">
                    <div class="alert alert-success" style="background-color: white !important">
                        <!-- Số Đơn Hàng Chưa Xử Lý: -->
                        <button type="button" class="btn btn-default">Số Đơn Hàng Hủy: <span class="badge">[[(model.datainit.dtgListInvoice | filter: { status: 1 }).length]]</span></button>
                        <button type="button" class="btn btn-default">Tổng Tiền: <span class="badge">[[model.datainit.sumStatus1/2 | number]] VNĐ</span></button>
                    </div>
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse">
                         <i style="color: blue;font-size: 18px" class="fa fa-fw fa-plus"></i>
                      </button>
                   </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <table  name="listResult" 
                                id="listResult" 
                                style="width: 100%;" 
                                datatable="ng" 
                                dt-options="dtOptions"
                                dt-column-defs="dtColumnDefs"
                                class="table table-bordered table-striped table-condensed">
                                <thead>
                                    <tr>
                                        <th class="text-center" data-column-index="0" style="width: 80px; background-color: black">Tùy Chọn</th>
                                        <th class="text-center" data-column-index="0" style="width: 80px; background-color: black">Đỗi Trạng Thái</th>
                                        <th class="text-center" data-column-index="2" style="width: auto;background-color: black">Số Hóa Đơn</th>
                                        <th class="text-center" data-column-index="3" style="width: auto;background-color: black">Khách Hàng</th>
                                        <th class="text-center" data-column-index="5" style="width: auto;background-color: black">SDT</th>
                                        <th class="text-center" data-column-index="6" style="width: auto;background-color: black">Ngày Lập</th>
                                        <th class="text-center" data-column-index="7" style="width: auto;background-color: black">Tổng Trị Giá </th>
                                        <th class="text-center" data-column-index="8" style="width: auto;background-color: black">Tình Trạng</th>
                                        <th class="text-center" data-column-index="8" style="width: auto;background-color: black">Nơi Nhận Hàng</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr role="row" ng-repeat="inv in model.datainit.dtgListInvoice | filter: { status: 1 }">
                                        <td class="text-center">
                                            <a href="#" ng-click="loadInvoiceDetail(inv.invoice_id)" data-toggle="modal" data-target="#modalAdd"  class="label label-primary"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                        </td>
                                        <td style="width: 80px !important">
                                           
                                            <button ng-show="inv.status!=4" ng-click="changeInvoiceStatus(inv.invoice_id,4)" title="Đã Nhận Hàng" type="button" class="btn btn-primary btn-xs"><i class="fa fa-gift" aria-hidden="true"></i></button>
                                            <button ng-show="inv.status!=5" ng-click="changeInvoiceStatus(inv.invoice_id,5)" title="Hoàn Thành" type="button" class="btn btn-success btn-xs"><i class="fa fa-check" aria-hidden="true"></i></button>
                                            <button ng-show="inv.status!=7" ng-click="changeInvoiceStatus(inv.invoice_id,7)" title="Hoàn Thành Và Thanh Toán" type="button" class="btn btn-success btn-xs"><i class="fa fa-star" aria-hidden="true"></i></button>
                                        </td>
                                        <td>[[inv.invoice_number]]</td>
                                        <td>[[inv.customer_fullname]]</td>
                                        <td>[[inv.receiver_phone]]</td>
                                        <td>[[inv.created_date_format ]]</td>
                                        <td style="text-align: right;" 
                                        ng-init="model.datainit.sumStatus1 = model.datainit.sumStatus1 + inv.total_sum">
                                        [[inv.total_sum | number]] VNĐ
                                        </td>
                                        <td>
                                            <span ng-show="inv.status==1" class="label label-default"><i class="fa fa-window-close" aria-hidden="true"></i>&nbsp; Đã Hủy</span>
                                            
                                        </td>
                                        <td>[[inv.branch_name]]</td>
                                    </tr>
                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                </div>
                <!-- end loading -->
             </div>
             <!-- /.box -->
          </div>
          <!-- /.col -->
        </div>
    </div>

</div>
@stop