@extends('system.layouts.index')

@section('headTitle')
   <title>{{ Lang::get('messages.system_heade_title') }}</title>
@stop

@section('pageTitle')
   <h1>
        <i class="fa fa-fw fa-archive" aria-hidden="true"></i> Tạo Đơn Hàng
        <small>{{ Lang::get('messages.system_verion') }}</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{URL::to('/System/Dashboard')}}"><i class="fa fa-fw fa-dashboard"></i> Hệ Thống Quản Trị Website</a>
        </li>
        <li class="active"><i class="fa fa-fw fa-archive" aria-hidden="true"></i> Tạo Đơn Hàng</li>
    </ol>
@stop

@section('headCss')
    <!--datetimepicker  -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-bootstrap-datetimepicker/css/datetimepicker.css')}}"/>
    <!--daterangepicker  -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/daterangepicker/daterangepicker.css')}}"/>
    <!--switch  -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-ui-switch/bootstrap-switch.css')}}"/>
    <!--multi  -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-multi-select/isteven-multi-select.css')}}">
    <!-- toastr -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-toastr/angular-toastr.css')}}" />
    <!-- iCheck -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/iCheck/flat/blue.css')}}">
    <!-- datatables -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/jquery.dataTables.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/dataTables.bootstrap.css')}}" />

    <style type="text/css">
        .has-error>.mce-tinymce{
            border-color: #dd4b39 !important;
            box-shadow: none !important;
        }
        .has-error>.input-group, .has-error>.input-group>.input-group-addon{
            border-color: #dd4b39 !important;
            box-shadow: none !important;
        }

        .has-error button, .has-error button:active{
            border-color: #dd4b39 !important;
        }
        .multiSelect .checkboxLayer {
            max-height: 250px; 
            overflow: auto; /* or scroll */
        }
        .modal-content {
            width: 100% !important;
        }
    </style>
    <style type="text/css">
      .checkbox {
        padding-left: 20px!important; }
      .checkbox label {
        display: inline-block!important;
        vertical-align: middle!important;
        position: relative!important;
          padding-left: 5px!important; }
      .checkbox label::before {
        content: ""!important;
        display: inline-block!important;
        position: absolute!important;
        width: 17px!important;
        height: 17px!important;
        left: 0!important;
        margin-left: -20px!important;
        border: 1px solid #cccccc!important;
        border-radius: 3px!important;
        background-color: #fff!important;
        -webkit-transition: border 0.15s ease-in-out, color 0.15s ease-in-out!important;
        -o-transition: border 0.15s ease-in-out, color 0.15s ease-in-out!important;
            transition: border 0.15s ease-in-out, color 0.15s ease-in-out!important; }
      .checkbox label::after {
        display: inline-block!important;
        position: absolute!important;
        width: 16px!important;
        height: 16px!important;
        left: 0!important;
        top: 0!important;
        margin-left: -20px!important;
        padding-left: 3px!important;
        padding-top: 1px!important;
        font-size: 11px!important;
            color: #555555!important; }
      .checkbox input[type="checkbox"],
      .checkbox input[type="radio"] {
        opacity: 0!important;
        z-index: 1!important;
        cursor: pointer!important;
      }
      .checkbox input[type="checkbox"]:focus + label::before,
      .checkbox input[type="radio"]:focus + label::before {
        outline: thin dotted!important;
        outline: 5px auto -webkit-focus-ring-color!important;
            outline-offset: -2px!important; }
      .checkbox input[type="checkbox"]:checked + label::after,
      .checkbox input[type="radio"]:checked + label::after {
        font-family: "FontAwesome"!important;
        content: "\f00c"!important;}
      .checkbox input[type="checkbox"]:indeterminate + label::after,
      .checkbox input[type="radio"]:indeterminate + label::after {
        display: block!important;
        content: ""!important;
        width: 10px!important;
        height: 3px!important;
        background-color: #555555!important;
        border-radius: 2px!important;
        margin-left: -16.5px!important;
        margin-top: 7px!important;
      }
      .checkbox input[type="checkbox"]:disabled,
      .checkbox input[type="radio"]:disabled {
          cursor: not-allowed!important;
      }
      .checkbox input[type="checkbox"]:disabled + label,
      .checkbox input[type="radio"]:disabled + label {
            opacity: 0.65!important; }
      .checkbox input[type="checkbox"]:disabled + label::before,
      .checkbox input[type="radio"]:disabled + label::before {
        background-color: #eeeeee!important;
              cursor: not-allowed!important; }
      .checkbox.checkbox-circle label::before {
          border-radius: 50%!important; }
      .checkbox.checkbox-inline {
          margin-top: 0!important; }

      .checkbox-primary input[type="checkbox"]:checked + label::before,
      .checkbox-primary input[type="radio"]:checked + label::before {
        background-color: #337ab7!important;
        border-color: #337ab7!important; }
      .checkbox-primary input[type="checkbox"]:checked + label::after,
      .checkbox-primary input[type="radio"]:checked + label::after {
        color: #fff!important; }

      .checkbox-danger input[type="checkbox"]:checked + label::before,
      .checkbox-danger input[type="radio"]:checked + label::before {
        background-color: #d9534f!important;
        border-color: #d9534f!important; }
      .checkbox-danger input[type="checkbox"]:checked + label::after,
      .checkbox-danger input[type="radio"]:checked + label::after {
        color: #fff!important; }

      .checkbox-info input[type="checkbox"]:checked + label::before,
      .checkbox-info input[type="radio"]:checked + label::before {
        background-color: #5bc0de!important;
        border-color: #5bc0de!important; }
      .checkbox-info input[type="checkbox"]:checked + label::after,
      .checkbox-info input[type="radio"]:checked + label::after {
        color: #fff!important; }

      .checkbox-warning input[type="checkbox"]:checked + label::before,
      .checkbox-warning input[type="radio"]:checked + label::before {
        background-color: #f0ad4e!important;
        border-color: #f0ad4e!important; }
      .checkbox-warning input[type="checkbox"]:checked + label::after,
      .checkbox-warning input[type="radio"]:checked + label::after {
        color: #fff!important; }

      .checkbox-success input[type="checkbox"]:checked + label::before,
      .checkbox-success input[type="radio"]:checked + label::before {
        background-color: #5cb85c!important;
        border-color: #5cb85c!important; }
      .checkbox-success input[type="checkbox"]:checked + label::after,
      .checkbox-success input[type="radio"]:checked + label::after {
        color: #fff!important;}

      .checkbox-primary input[type="checkbox"]:indeterminate + label::before,
      .checkbox-primary input[type="radio"]:indeterminate + label::before {
        background-color: #337ab7!important;
        border-color: #337ab7!important;
      }

      .checkbox-primary input[type="checkbox"]:indeterminate + label::after,
      .checkbox-primary input[type="radio"]:indeterminate + label::after {
        background-color: #fff!important;
      }

      .checkbox-danger input[type="checkbox"]:indeterminate + label::before,
      .checkbox-danger input[type="radio"]:indeterminate + label::before {
        background-color: #d9534f!important;
        border-color: #d9534f!important;
      }

      .checkbox-danger input[type="checkbox"]:indeterminate + label::after,
      .checkbox-danger input[type="radio"]:indeterminate + label::after {
        background-color: #fff!important;
      }

      .checkbox-info input[type="checkbox"]:indeterminate + label::before,
      .checkbox-info input[type="radio"]:indeterminate + label::before {
        background-color: #5bc0de!important;
        border-color: #5bc0de!important;
      }

      .checkbox-info input[type="checkbox"]:indeterminate + label::after,
      .checkbox-info input[type="radio"]:indeterminate + label::after {
        background-color: #fff!important;
      }

      .checkbox-warning input[type="checkbox"]:indeterminate + label::before,
      .checkbox-warning input[type="radio"]:indeterminate + label::before {
        background-color: #f0ad4e!important;
        border-color: #f0ad4e!important;
      }

      .checkbox-warning input[type="checkbox"]:indeterminate + label::after,
      .checkbox-warning input[type="radio"]:indeterminate + label::after {
        background-color: #fff!important;
      }

      .checkbox-success input[type="checkbox"]:indeterminate + label::before,
      .checkbox-success input[type="radio"]:indeterminate + label::before {
        background-color: #5cb85c!important;
        border-color: #5cb85c!important;
      }

      .checkbox-success input[type="checkbox"]:indeterminate + label::after,
      .checkbox-success input[type="radio"]:indeterminate + label::after {
        background-color: #fff!important;
      }

      .radio {
        padding-left: 20px!important; }
      .radio label {
        display: inline-block!important;
        vertical-align: middle!important;
        position: relative!important;
          padding-left: 5px!important; }
      .radio label::before {
        content: ""!important;
        display: inline-block!important;
        position: absolute!important;
        width: 17px!important;
        height: 17px!important;
        left: 0!important;
        margin-left: -20px!important;
        border: 1px solid #cccccc!important;
        border-radius: 50%!important;
        background-color: #fff!important;
        -webkit-transition: border 0.15s ease-in-out!important;
        -o-transition: border 0.15s ease-in-out!important;
            transition: border 0.15s ease-in-out!important; }
      .radio label::after {
        display: inline-block!important;
        position: absolute!important;
        content: " "!important;
        width: 11px!important;
        height: 11px!important;
        left: 3px!important;
        top: 3px!important;
        margin-left: -20px!important;
        border-radius: 50%!important;
        background-color: #555555!important;
        -webkit-transform: scale(0, 0)!important;
        -ms-transform: scale(0, 0)!important;
        -o-transform: scale(0, 0)!important;
        transform: scale(0, 0)!important;
        -webkit-transition: -webkit-transform 0.1s cubic-bezier(0.8, -0.33, 0.2, 1.33)!important;
        -moz-transition: -moz-transform 0.1s cubic-bezier(0.8, -0.33, 0.2, 1.33)!important;
        -o-transition: -o-transform 0.1s cubic-bezier(0.8, -0.33, 0.2, 1.33)!important;
            transition: transform 0.1s cubic-bezier(0.8, -0.33, 0.2, 1.33)!important; }
      .radio input[type="radio"] {
        opacity: 0!important;
        z-index: 1!important;
        cursor: pointer!important;
      }
      .radio input[type="radio"]:focus + label::before {
        outline: thin dotted!important;
        outline: 5px auto -webkit-focus-ring-color!important;
            outline-offset: -2px!important; }
      .radio input[type="radio"]:checked + label::after {
        -webkit-transform: scale(1, 1)!important;
        -ms-transform: scale(1, 1)!important;
        -o-transform: scale(1, 1)!important;
            transform: scale(1, 1)!important; }
      .radio input[type="radio"]:disabled {
          cursor: not-allowed!important;
      }
      .radio input[type="radio"]:disabled + label {
            opacity: 0.65!important; }
      .radio input[type="radio"]:disabled + label::before {
              cursor: not-allowed!important; }
      .radio.radio-inline {
          margin-top: 0!important; }

      .radio-primary input[type="radio"] + label::after {
        background-color: #337ab7!important; }
      .radio-primary input[type="radio"]:checked + label::before {
        border-color: #337ab7!important; }
      .radio-primary input[type="radio"]:checked + label::after {
        background-color: #337ab7!important; }

      .radio-danger input[type="radio"] + label::after {
        background-color: #d9534f!important; }
      .radio-danger input[type="radio"]:checked + label::before {
        border-color: #d9534f!important; }
      .radio-danger input[type="radio"]:checked + label::after {
        background-color: #d9534f!important; }

      .radio-info input[type="radio"] + label::after {
        background-color: #5bc0de!important; }
      .radio-info input[type="radio"]:checked + label::before {
        border-color: #5bc0de!important; }
      .radio-info input[type="radio"]:checked + label::after {
        background-color: #5bc0de!important; }

      .radio-warning input[type="radio"] + label::after {
        background-color: #f0ad4e!important; }
      .radio-warning input[type="radio"]:checked + label::before {
        border-color: #f0ad4e!important; }
      .radio-warning input[type="radio"]:checked + label::after {
        background-color: #f0ad4e!important; }

      .radio-success input[type="radio"] + label::after {
        background-color: #5cb85c!important; }
      .radio-success input[type="radio"]:checked + label::before {
        border-color: #5cb85c!important; }
      .radio-success input[type="radio"]:checked + label::after {
        background-color: #5cb85c!important; }

      input[type="checkbox"].styled:checked + label:after,
      input[type="radio"].styled:checked + label:after {
        font-family: 'FontAwesome'!important;
        content: "\f00c"!important; }
      input[type="checkbox"] .styled:checked + label::before,
      input[type="radio"] .styled:checked + label::before {
        color: #fff!important; }
      input[type="checkbox"] .styled:checked + label::after,
      input[type="radio"] .styled:checked + label::after {
        color: #fff!important; }
    </style>
@stop

@section('headJs')
    <!-- Date Time Picker -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-bootstrap-datetimepicker/js/datetimepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-bootstrap-datetimepicker/js/datetimepicker.templates.js')}}"></script>
    <!-- Daterange Time Picker -->
    <script type="text/javascript" src="{{asset('public/system/plugins/daterangepicker/daterangepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-daterangepicker/angular-daterangepicker.min.js')}}"></script>
    <!-- switch -->
    <script src="{{asset('public/system/plugins/angular-ui-switch/bootstrap-switch.js')}}"></script>
    <script src="{{asset('public/system/plugins/angular-ui-switch/angular-bootstrap-switch.js')}}"></script>
    <!-- iCheck -->
    <script type="text/javascript" src="{{asset('public/system/plugins/iCheck/icheck.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-icheck/angular-icheck.js')}}"></script>
    <!-- File Manager -->
    <script src="http://localhost:8000/vendor/unisharp/laravel-filemanager/public/js/lfm.js"></script>
    <!-- Angular Multi Select -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-multi-select/isteven-multi-select.js')}}"></script>
    <!-- UI Bootstrap Modal --> 
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-bootstrap/ui-bootstrap-tpls-2.0.0.min.js')}}"></script>
    <!-- Angular Toastr -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-toastr/angular-toastr.tpls.js')}}"></script>
    <!-- Angular Confirm Dialog -->
    <script type="text/javascript" src="{{asset('public/system/plugins/bootbox/bootbox.min.js')}}"></script>
    <!-- Angular Datatables-->
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/angular-datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/colreorder/angular-datatables.colreorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/bootstrap/angular-datatables.bootstrap.min.js')}}"></script>
    <!-- tiny MCE -->
    <script src="{{asset('/public/system/plugins/tinymce/tinymce.min.js')}}"></script>
    <script src="{{asset('/public/system/plugins/angular-tinymce/angular-tinymce.js')}}"></script>
    <script src="{{asset('/public/system/plugins/angular-tinymce/langs/vi_VN.js')}}"></script>
    <!-- JS -->
    <script type="text/javascript" src="{{asset('public/system/common/system.common.angurlar.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/common/system.createinvoice.angurlar.js')}}"></script>



    <script type="text/javascript">
        $(document).ready(function() { 
            $(window).on('load', function() {  
                //insert all your ajax callback code here. 
                //Which will run only after page is fully loaded in background.
                $("#addReLoading").hide();
            });

            //Laravel File Manager
            $('#selectImages').filemanager('images');
            //Minify Left Menu
            //$("body").addClass("sidebar-collapse");
        });
        $( "#dialog-modal" ).on( "dialogopen", function( event, ui ) {
            console.log('Wayhay!!');
            window.open("http://www.google.com");
        } );
    </script>

@stop

@section('container')
<div class="row" ng-app="createInvocieApp" ng-controller="createInvoiceController" ng-init="pageInit()">
    <!-- Modal -->
    <div id="modalAdd" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <form class="form" class="form-inline" name="frmAdd" id="frmAdd" novalidate>
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-blue">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-fw fa-floppy-o" aria-hidden="true"></i> Thêm Sản Phẩm</h4>
                </div>
                <div class="modal-body">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <table  name="listResult" 
                                    id="listResult" 
                                    style="width: 100%;" 
                                datatable="ng" 
                                dt-options="dtOptions"
                                dt-column-defs="dtColumnDefs"
                                class="table table-bordered table-striped table-condensed">
                                <thead>
                                    <tr>
                                        <th class="text-center" data-column-index="0" style="width: auto;">Thêm</th>
                                        <th class="text-center" data-column-index="1" style="width: auto">Hình Ảnh</th>
                                        <th class="text-center" data-column-index="6" style="width: auto;">Loại</th>
                                        <th class="text-center" data-column-index="2" style="width: auto;">Sản Phẩm</th>
                                        <th class="text-center" data-column-index="3" style="width: auto;">Tồn Kho</th>
                                        <th class="text-center" data-column-index="3" style="width: auto;">SL</th>
                                        <th class="text-center" data-column-index="4" style="width: auto;">Giá Bán</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr role="row" ng-repeat="pro in model.datainit.dtgListProduct">
                                        <td class="text-center">
                                            <button type="button" ng-click="addCart([[pro]])" 
                                            data-dismiss="modal"
                                            class="btn btn-success btn-xs">
                                            <i class="fa fa-fw fa-cart-plus fa-fw"></i>
                                            </button>
                                        </td>
                                        <td>
                                            <img ng-src="[[pro.product_thumbnail]]"
                                             onError="this.src='{{asset('public/images/default-image.png')}}';"
                                             id="holder1" 
                                             class="img-responsive img-thumbnail" 
                                             alt="Ảnh Đại Diện" 
                                             title="Ảnh Đại Diện" 
                                             width="80px">
                                        </td>
                                        <td>[[pro.cate_name]]</td>
                                        <td>[[pro.product_name | limitTo:50]]</td>
                                        <td>
                                          <span class="badge">[[pro.branch_store_quantity_remain]]</span>
                                        </td>
                                        <td>
                                            <input 
                                            style="width: 70px"
                                            type="number"
                                            class="form-control" 
                                            id="pro_[[pro.product_id]]" 
                                            name="[[pro.product_id]]"
                                            autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                            ng-minlength="1"
                                            ng-maxlength="99"
                                            maxlength="99"
                                            value = "1"
                                            min = "1">
                                        </td>
                                        <td style="text-align: right">[[pro.product_retail_prices | number]]</td>
                                        
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal" tabindex="4">
                        <i class="fa fa-fw fa-ban" aria-hidden="true"></i>&nbsp;Đóng
                    </button>
                </div>
            </div>
        </form>
      </div>
    </div>

    <form class="form" class="form-vertical" name="frmAdd" novalidate>
        <!--   ================= Hidden feilds ================ -->
        <div class="form-group" style="display:none;">
            <input  
                type="text"
                class="form-control" 
                id="txtSlug" 
                name="txtSlug" 
                placeholder="Slug" 
                autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                value="{{$slug}}">
        </div>
        <div class="col-lg-12 col-xs-12">
            <!-- =====================first row: button======================= -->
            <div class="box box-danger">
                <!-- ================GROUP BUTTON================== -->
                <div class="box-body">
                    <!-- ========== =======UPDATE===================-->
                    <div class="form-group" ng-show="model.datainit.pageStatus == 'update'">
                    
                        <!-- ================= Trở Về ==============-->
                        <a class="btn btn-default" href="{{URL::to('System/ListInvoices')}}" title="trở về">
                            <i class="fa fa-fw fa-arrow-left" aria-hidden="true"></i>
                            <span class="hidden-xs">Trở Về</span>
                        </a>
                        <div class="pull-right">&nbsp;</div>
                         <!-- ================= Cap Nhat ==============-->
                        <button type="button" 
                                tabindex="99"
                                ng-click="addItem(2)"
                                class="btn btn-warning pull-right"
                                ng-disabled="frmAdd.txtReceivedName.$invalid
                                          || frmAdd.txtReceivedPhone.$invalid
                                          || frmAdd.txtReceivedAddress.$invalid
                                          || model.request.txtTotalSum == 0
                                          ">
                            <i class="fa fa-fw fa-edit" aria-hidden="true"></i>
                            <span class="hidden-xs">Cập Nhật</span>
                        </button>
                        <div class="pull-right">&nbsp;</div>
                    </div>
                    <!-- ======= ============INSERT========= ==============-->
                    <div class="form-group" ng-show="model.datainit.pageStatus == 'add'">
                        <!-- ================= Trở Về ==============-->
                        <a class="btn btn-info" href="{{URL::to('System/ListInvoices')}}" title="CreateNews">
                            <i class="fa fa-fw fa-arrow-left" aria-hidden="true"></i>
                            <span class="hidden-xs">Trở Về</span>
                        </a>
                        <div class="pull-right">&nbsp;</div>
                         <!-- ================= Them Moi==============-->
                        <button type="button" 
                                tabindex="99"
                                ng-click="addItem(1)"
                                class="btn btn-primary pull-right"
                                ng-disabled="frmAdd.txtReceivedName.$invalid
                                          || frmAdd.txtReceivedPhone.$invalid
                                          || frmAdd.txtReceivedAddress.$invalid
                                          || model.request.txtTotalSum == 0
                                          "
                                title="Thêm Mới">
                            <i class="fa fa-fw fa-plus" aria-hidden="true"></i>
                            <span class="hidden-xs">Tạo Đơn Hàng</span>
                        </button>
                        <div class="pull-right">&nbsp;</div>
                    </div>
                    <!--   ================= Hidden feilds ================ -->
                    <div class="form-group" style="display:none;">
                        <input  
                            type="text"
                            class="form-control" 
                            id="txtId" 
                            name="txtId" 
                            placeholder="txtId" 
                            autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                            value="{{$slug}}">
                    </div>
                   
                </div>
                <!-- /.box-body -->
            </div>
            <!-- end first row -->
            <!-- ================ second row: Thong tin hoa don ============= -->
            <div class="form-group row">
                <!-- =======Invoice ID/Number======== -->
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title"><i class="fa fa-fw fa-gitlab" aria-hidden="true"></i> Thông Tin Hóa Đơn</h3>
                          
                            <div class="box-tools pull-right">
                               <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-fw fa-minus"></i></button>
                           </div>
                        </div>
                        <div class="box-body">
                            <!--  first line: Invoice ID/Number-->
                            <div class="form-group row"> 
                                <!--   ============== Mã HD ============= -->
                                <div class="col-md-6" >
                                    <label for="txtInvoiceId" class="control-label display-block-xs">Mã Hóa Đơn:</label>  
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-fw fa-indent" aria-hidden="true"></i>
                                        </span>
                                        <input  
                                            tabindex="1"
                                            type="text"
                                            class="form-control" 
                                            id="txtInvoiceId" 
                                            name="txtInvoiceId" 
                                            placeholder="Mã Hóa Đơn" 
                                            ng-model="model.request.txtInvoiceId"
                                            autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                            ng-minlength="2"
                                            ng-maxlength="20"
                                            maxlength="20"
                                            ng-disabled=true
                                             >
                                    </div>
                                </div>
                                <!--   ===== Số HD ======= -->
                                <div class="col-md-6">
                                    <label for="txtInvoiceNumber" class="control-label display-block-xs">Số Hóa Đơn:</label>  
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-fw fa-credit-card" aria-hidden="true"></i>
                                        </span>
                                        <input  
                                            tabindex="1"
                                            type="text"
                                            class="form-control" 
                                            id="txtInvoiceNumber" 
                                            name="txtInvoiceNumber" 
                                            placeholder="Số Hóa Đơn" 
                                            ng-model="model.request.txtInvoiceNumber"
                                            autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                            ng-minlength="2"
                                            ng-maxlength="20"
                                            maxlength="20"
                                            ng-disabled=true
                                             >
                                    </div>
                                </div>
                            </div>
                            <!-- 1.2.3 Trạng thái HD -->
                            <div class="form-group row" ng-show="model.datainit.pageStatus == 'add'"> 
                                <div class="col-xs-12" >
                                <label class="control-label display-block-xs">Trạng Thái Hóa Đơn</label>
                                </div>
                                <!--   ============== Chưa Xử Lý============= -->
                                <div class="col-xs-6 col-sm-3" style="text-align: center;">
                                    <div class="radio radio-warning">
                                        <input type="radio" id="radioStatus2" ng-model="model.request.radioStatus"
                                        value="2">
                                        <label for="radioStatus2" class="control-label display-block-xs"></label>
                                    </div>
                                    <label for="radioStatus2" class="control-label display-block-xs">Chưa Xử Lý</label>
                                </div>

                                <!--   ============== Đang Xử Lý ============= -->
                                <div class="col-xs-6 col-sm-3" style="text-align: center;">
                                    <div class="radio radio-primary">
                                        <input type="radio" id="radioStatus3" ng-model="model.request.radioStatus"
                                        value="3">
                                        <label for="radioStatus3" class="control-label display-block-xs"></label>
                                    </div>
                                    <label for="radioStatus3" class="control-label display-block-xs">Đang Xử Lý</label>
                                </div>

                                <!--   ============== Đã Nhận ============= -->
                                <div class="col-xs-6 col-sm-2" style="text-align: center;">
                                    <div class="radio radio-success" >
                                        <input type="radio" id="radioStatus4" ng-model="model.request.radioStatus"
                                        value="4">
                                        <label for="radioStatus4" class="control-label display-block-xs"></label>
                                    </div>
                                    <label for="radioStatus4" class="control-label display-block-xs">Đã Nhận</label>
                                </div>
                                <!--   ============== Hoàn Tất============= -->
                                <div class="col-xs-6 col-sm-2" style="text-align: center;">
                                    <div class="radio radio-success">
                                        <input type="radio" id="radioStatus4" ng-model="model.request.radioStatus"
                                        value="5">
                                        <label for="radioStatus4" class="control-label display-block-xs"></label>
                                    </div>
                                    <label for="radioStatus4" class="control-label display-block-xs">H.Tất</label>
                                </div>
                                <!--   ============== Hoàn Tất============= -->
                                <div class="col-xs-6 col-sm-2" style="text-align: center;">
                                    <div class="radio radio-success">
                                        <input type="radio" id="radioStatus4" ng-model="model.request.radioStatus"
                                        value="7">
                                        <label for="radioStatus4" class="control-label display-block-xs"></label>
                                    </div>
                                    <label for="radioStatus4" class="control-label display-block-xs">Nhận Hàng & Thanh Toán</label>
                                </div>
                            </div>
                        
                            <!-- Xuat Hoa Don/ ngay giao -->
                            <div class="form-group row">
                                <div class="col-md-4">
                                    <label for="swExportInvoiceStatus" class="control-label display-block-xs">Xuất Hóa Đơn:</label>
                                    <div class= "col-md-2 input-group">
                                        <input
                                            tabindex="3"
                                            bs-switch
                                            name="swExportInvoiceStatus"
                                            ng-model="model.request.swExportInvoiceStatus"
                                            type="checkbox"
                                            switch-active="true"
                                            switch-readonly="false"
                                            switch-size="small"
                                            switch-animate="true"
                                            switch-label=""
                                            switch-icon="icon-save"
                                            switch-on-text="Có"
                                            switch-off-text="Không"
                                            switch-on-color="success"
                                            switch-off-color="danger"
                                            switch-radio-off="false"
                                            switch-label-width="0px"
                                            switch-handle-width="auto"
                                            switch-inverse="false"
                                            switch-change="onChange()"
                                            ng-true-value="1"
                                            ng-false-value="0">
                                    </div>
                                </div>
                                <!-- Ngày Đặt -->
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="dtpCreatedDate" class="control-label">Ngày Đặt:</label>
                                        <div class="dropdown">
                                            <a class="dropdown-toggle" id="dtpCreatedDate_" role="button" data-toggle="dropdown" data-target="#" href="#">
                                                <div class="input-group">
                                                    <input tabindex="8" 
                                                    type="text" 
                                                    ng-model="model.request.dtpCreatedDate" 
                                                    class="form-control" 
                                                    id="dtpCreatedDate" 
                                                    name="dtpCreatedDate" 
                                                    ng-disabled=true
                                                    >
                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                </div>
                                            </a>
                                      <!--       <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                                <datetimepicker data-ng-model="model.request.dtpCreatedDate" data-datetimepicker-config="model.singleDatePickerOpts"/>
                                            </ul> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- ghi chú -->
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label for="txtInvoiceNote" class="control-label display-block-xs">Ghi Chú:</label>  
                                    <div>
                                        <input  
                                            tabindex="1"
                                            type="text"
                                            class="form-control" 
                                            id="txtInvoiceNote" 
                                            name="txtInvoiceNote" 
                                            placeholder="Ghi Chú" 
                                            ng-model="model.request.txtInvoiceNote"
                                            autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                            ng-minlength="2"
                                            ng-maxlength="200"
                                            maxlength="200"
                                             >
                                    </div>
                                </div>
                            </div>
                           

                        </div>
                    </div>
                </div>
                <!--====================== Thong tin Khách Hàng ==============-->
                <div class="col-md-6">
                    <div class="box box-success">
                        <div class="box-header">
                            <h3 class="box-title"><i class="fa fa-fw fa-search-plus" aria-hidden="true"></i> Thông Tin Khách Hàng</h3>
                            <div class="box-tools pull-right">
                               <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-fw fa-minus"></i></button>
                           </div>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                             <!--   =========0 ========== -->
                            <div class="form-group row">
                                <div class="col-md-6" ng-class="{ 'has-error' : frmAdd.txtCustomer.$invalid && !frmAdd.txtCustomer.$pristine }">
                                    <label for="slbCustomer" class="control-label display-block-xs">Chọn Khách Hàng:</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-fw fa-user-circle-o" aria-hidden="true"></i>
                                        </span>
                                        <isteven-multi-select
                                            tabindex="2"
                                            name="slbCustomer"
                                            input-model="model.datainit.slbCustomer"
                                            output-model="model.request.slbCustomer"
                                            helper-elements="filter"
                                            button-label="customer_fullname"
                                            item-label="customer_fullname"
                                            output-properties="customer_id"
                                            search-property="customer_fullname"
                                            tick-property="ticked"
                                            orientation="vertical"
                                            disable-property="false"
                                            is-disabled="false"
                                            selection-mode="single"
                                            min-search-length="10"
                                            translation="model.localLang"
                                            max-labels="1"  
                                            directive-id="slbCustomer"
                                            on-reset="slbClear('txtCustomer')"
                                            on-item-click="slbItemClick(data,'txtCustomer')">
                                        </isteven-multi-select>
                                    </div>
                                    <input  
                                        type="text"
                                        class="form-control" 
                                        id="txtCustomer" 
                                        name="txtCustomer" 
                                        ng-model="model.request.txtCustomer"
                                        autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style="display:none;" 
                                        required >
                                </div>
                                <div class="col-md-6" >
                                    <label for="txtCustomerAccount" class="control-label display-block-xs">Tài Khoản Khách Hàng:</label>  
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-fw fa-pencil" aria-hidden="true"></i>
                                        </span>
                                        <input  
                                            tabindex="1"
                                            type="text"
                                            class="form-control" 
                                            id="txtCustomerAccount" 
                                            name="txtCustomerAccount" 
                                            placeholder="Tài Khoản Khách Hàng" 
                                            ng-model="model.request.txtCustomerAccount"
                                            autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                            ng-minlength="6"
                                            ng-maxlength="20"
                                            maxlength="20"
                                            ng-disabled=true
                                            required >
                                    </div>
                                </div>
                            </div>
                            <!-- 1/ txtCustomerEmail -->
                            <div class="form-group row"> 
                                <!--   ========= Customer ID ========== -->
                                <div class="col-md-6" >
                                    <label for="txtCustomerId" class="control-label display-block-xs">Mã Khách Hàng:</label>  
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-fw fa-address-card" aria-hidden="true"></i>
                                        </span>
                                        <input  
                                            tabindex="1"
                                            type="text"
                                            class="form-control" 
                                            id="txtCustomerId" 
                                            name="txtCustomerId" 
                                            placeholder="Mã Khách Hàng" 
                                            ng-model="model.request.txtCustomerId"
                                            autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                            ng-minlength="2"
                                            ng-maxlength="150"
                                            maxlength="150"
                                            ng-disabled=true
                                             >
                                    </div>
                                </div>
                                <!--  ======== Customer Email ========= -->
                                <div class="col-md-6">
                                    <label for="txtCustomerEmail" class="control-label display-block-xs">Email Khách Hàng:</label>  
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-fw fa-bookmark-o" aria-hidden="true"></i>
                                        </span>
                                        <input  
                                            tabindex="1"
                                            type="text"
                                            class="form-control" 
                                            id="txtCustomerEmail" 
                                            name="txtCustomerEmail" 
                                            placeholder="Email Khách Hàng" 
                                            ng-model="model.request.txtCustomerEmail"
                                            autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                            ng-minlength="2"
                                            ng-maxlength="150"
                                            maxlength="150"
                                            ng-disabled=true
                                             >
                                    </div>
                                </div>
                            </div>
                            <!--   === 3 :Customer name / phone==== -->
                            <div class="form-group row"> 
                                <!--   ========= Customer name ========== -->
                                <div class="col-md-6" >
                                    <label for="txtCustomerName" class="control-label display-block-xs">Tên Khách Hàng:</label>  
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-fw fa-database" aria-hidden="true"></i>
                                        </span>
                                        <input  
                                            tabindex="1"
                                            type="text"
                                            class="form-control" 
                                            id="txtCustomerName" 
                                            name="txtCustomerName" 
                                            placeholder="Tên Khách Hàng" 
                                            ng-model="model.request.txtCustomerName"
                                            autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                            ng-minlength="2"
                                            ng-maxlength="50"
                                            maxlength="50"
                                            ng-disabled=true
                                             >
                                    </div>
                                </div>
                                <!--   ==== Customer Phone number ======= -->
                                <div class="col-md-6">
                                    <label for="txtCustomerPhone" class="control-label display-block-xs">Số Điện Thoại:</label>  
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-fw fa-info" aria-hidden="true"></i>
                                        </span>
                                        <input  
                                            tabindex="1"
                                            type="text"
                                            class="form-control" 
                                            id="txtCustomerPhone" 
                                            name="txtCustomerPhone" 
                                            placeholder="Số Điện Thoại" 
                                            ng-model="model.request.txtCustomerPhone"
                                            autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                            ng-minlength="10"
                                            ng-maxlength="11"
                                            maxlength="11"
                                            ng-disabled=true
                                             >
                                    </div>
                                </div>
                            </div>
                            <!--   ===== 4: Customer address ===== -->
                            <div class="form-group row"> 
                                <!--   ====== Customer address ====== -->
                                <div class="col-md-12">
                                    <label for="txtCustomerAddress" class="control-label display-block-xs">Địa Chỉ Khách Hàng:</label>  
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-fw fa-street-view" aria-hidden="true"></i>
                                        </span>
                                        <input  
                                            tabindex="1"
                                            type="text"
                                            class="form-control" 
                                            id="txtCustomerAddress" 
                                            name="txtCustomerAddress" 
                                            placeholder="Địa Chỉ Khách Hàng" 
                                            ng-model="model.request.txtCustomerAddress"
                                            autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                            ng-minlength="2"
                                            ng-maxlength="150"
                                            maxlength="150"
                                            ng-disabled=true
                                            required >
                                    </div>
                                </div>
                            </div>
                            <!--5:txtCustomerIdentify/txtCustomerStudentCard-->
                            <div class="form-group row"> 
                                <!--   =========txtCustomerIdentify ========== -->
                                <div class="col-md-6" >
                                    <label for="txtCustomerIdentify" class="control-label display-block-xs">CMND:</label>  
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-fw fa-database" aria-hidden="true"></i>
                                        </span>
                                        <input  
                                            tabindex="1"
                                            type="text"
                                            class="form-control" 
                                            id="txtCustomerIdentify" 
                                            name="txtCustomerIdentify" 
                                            placeholder="CMND" 
                                            ng-model="model.request.txtCustomerIdentify"
                                            autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                            ng-minlength="1"
                                            ng-maxlength="20"
                                            maxlength="20"
                                            ng-disabled=true
                                             >
                                    </div>
                                </div>
                                <!--   ==== txtCustomerStudentCard ======= -->
                                <div class="col-md-6">
                                    <label for="txtCustomerStudentCard" class="control-label display-block-xs">Mã Sinh Viên:</label>  
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-fw fa-info" aria-hidden="true"></i>
                                        </span>
                                        <input  
                                            tabindex="1"
                                            type="text"
                                            class="form-control" 
                                            id="txtCustomerStudentCard" 
                                            name="txtCustomerStudentCard" 
                                            placeholder="Mã Sinh Viên" 
                                            ng-model="model.request.txtCustomerStudentCard"
                                            autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                            ng-minlength="1"
                                            ng-maxlength="20"
                                            maxlength="20"
                                            ng-disabled=true
                                             >
                                    </div>
                                </div>
                            </div>
                            <!--6 line:txtCustomerUniversity-->
                            <div class="form-group row"> 
                                <!--   =========txtCustomerUniversity ========== -->
                                <div class="col-md-12" >
                                    <label for="txtCustomerUniversity" class="control-label display-block-xs">Trường Học:</label>  
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-fw fa-database" aria-hidden="true"></i>
                                        </span>
                                        <input  
                                            tabindex="1"
                                            type="text"
                                            class="form-control" 
                                            id="txtCustomerUniversity" 
                                            name="txtCustomerUniversity" 
                                            placeholder="Trường Học" 
                                            ng-model="model.request.txtCustomerUniversity"
                                            autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                            ng-minlength="1"
                                            ng-maxlength="50"
                                            maxlength="50"
                                            ng-disabled=true
                                             >
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
            <!-- end second row -->
            <!-- ======================= third row: DSSP ===================== -->
            <div class="form-group row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header">
                            <h3 class="box-title"><i class="fa fa-fw fa-hand-o-down" aria-hidden="true"></i> Danh Sách Sản Phẩm</h3>
                          
                            <div class="box-tools pull-right">
                               <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-fw fa-minus"></i></button>
                           </div>
                        </div>

                        <div class="box-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="box-tools pull-right">
                                        <span style="font-size: 16px" class="label label-primary">Tổng Thành Tiền: [[model.request.txtTotalSum|number]]&nbsp; VNĐ</span>
                                         &nbsp;&nbsp;&nbsp;
                                        <button id="sendMail" type="button" class="btn btn-primary" tabindex="4" data-toggle="modal" data-target="#modalAdd"
                                        title="thêm sản phẩm" 
                                        ng-click="loadProductsList(model.request.txtBranchId)">
                                            <i class="fa fa-fw fa-plus" aria-hidden="true"></i>
                                            <span class="hidden-xs"> Thêm Sản Phẩm</span>
                                        </button>
                                    </div>
                                    <table  name="listResult" 
                                            id="listResult" 
                                            style="width: 100%;" 
                                            datatable="ng" 
                                            dt-options="dtOptions"
                                            dt-column-defs="dtColumnDefs"
                                            class="table table-bordered table-striped table-condensed">
                                        <thead>
                                            <tr>
                                                <th class="text-center" data-column-index="0" style="width: 80px;">Tùy Chọn</th>
                                                <th class="text-center" data-column-index="3" style="width: 100px;">Hình Ảnh</th>
                                                <th class="text-center" data-column-index="4" style="width: auto;">Tên Sản Phẩm</th>
                                                <th class="text-center" data-column-index="1" style="width: auto;">Tồn Kho</th>
                                                <th class="text-center" data-column-index="1" style="width: 50px;">Số Lượng</th>
                                                <th class="text-center" data-column-index="2" style="width: auto;">Giá Bán</th>
                                                <th class="text-center" data-column-index="2" style="width: auto;">Chiết Khấu</th>
                                                <th class="text-center" data-column-index="8" style="width: auto;">Thành Tiền</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr role="row" ng-repeat="pro in model.request.dtgListProductInvoice">
                                                <td class="text-center">
                                                    <button title="cập nhật" type="button" ng-click="updateInvoiceQuan([[pro.product_id]],0)" class="btn btn-success btn-xs"><i class="fa fa-fw fa-exchange fa-fw"></i></button>
                                                    <button title="xóa" type="button" ng-click="updateInvoiceQuan([[pro.product_id]],1)" class="btn btn-danger btn-xs"><i class="fa fa-fw fa-trash-o fa-fw"></i></button>
                                                </td>
                                                <td>
                                                    <img ng-src="[[pro.product_thumbnail]]"
                                                     onError="this.src='{{asset('public/images/default-image.png')}}';"
                                                     id="holder1" 
                                                     class="img-responsive img-thumbnail" 
                                                     alt="Ảnh Đại Diện" 
                                                     title="Ảnh Đại Diện" 
                                                     height="50px">
                                                </td>
                                                <td>[[pro.product_name | limitTo:50]]</td>
                                                <td style="text-align: center;">
                                                  <span class="badge btn-primary">
                                                    [[pro.branch_store_quantity_remain]]
                                                  </span>
                                                </td>
                                                <td>
                                                    <input 
                                                    type="number"
                                                    class="form-control" 
                                                    id="invoice_[[pro.product_id]]"
                                                    name="[[pro.product_id]]"
                                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                                    ng-min="1"
                                                    ng-max="99"
                                                    max="99"
                                                    value = "1"
                                                    min = "1"
                                                    ng-value = "[[pro.quantity]]">
                                                </td>
                                                <td style="text-align: right;">[[pro.product_retail_prices |number]]</td>
                                                <td style="text-align: center;">[[100- (pro.total_price *100/(pro.product_retail_prices*pro.quantity) )]] %</td>
                                                <td style="text-align: right;">[[pro.total_price | number]]</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
            <!-- end third row -->
        </div>
        <!--  /.end first Column -->  
    </form>
    <!--/ form action-->

</div>
@stop