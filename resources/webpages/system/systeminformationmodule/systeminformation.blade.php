@extends('system.layouts.index')

@section('headTitle')
   <title>{{ Lang::get('messages.system_heade_title') }}</title>
@stop

@section('pageTitle')
   <h1>
        <i class="fa fa-fw fa-archive" aria-hidden="true"></i> Quản Trị Đơn Hàng
        <small>Skyfire Sytem Version 1.0</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{URL::to('/System/Dashboard')}}"><i class="fa fa-fw fa-dashboard"></i> Hệ Thống Quản Trị Website</a>
        </li>
        <li class="active"><i class="fa fa-fw fa-archive" aria-hidden="true"></i> Danh Sách Đơn Hàng</li>
    </ol>
@stop

@section('headCss')
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/daterangepicker/daterangepicker.css')}}"/>
<link rel="stylesheet" href="{{asset('public/system/plugins/angular-ui-switch/angular-ui-switch.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-ui-switch/bootstrap-switch.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-multi-select/isteven-multi-select.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-toastr/angular-toastr.css')}}" />

<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/jquery.dataTables.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/dataTables.bootstrap.css')}}" />

<style type="text/css">
    .control-label{
        padding-top: 5px;
        padding-right: 5px !important;
        padding-left: 5px !important;
        float: left;
    }

    #listResult {
        width: 100% !important;
    }
</style>
@stop

@section('headJs')
    <!-- Date Time Picker -->
    <script type="text/javascript" src="{{asset('public/system/plugins/daterangepicker/daterangepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-daterangepicker/angular-daterangepicker.min.js')}}"></script>

    <script src="{{asset('public/system/plugins/angular-ui-switch/angular-ui-switch.js')}}"></script>
    <script src="{{asset('public/system/plugins/angular-ui-switch/bootstrap-switch.js')}}"></script>
    <script src="{{asset('public/system/plugins/angular-ui-switch/angular-bootstrap-switch.js')}}"></script>

    <!-- iCheck -->
    <script src="{{asset('public/system/plugins/angular_bootstrap_checkbox/angular-bootstrap-checkbox.js')}}"></script>

    <!-- File Manager -->
    <script src="{{asset('vendor/unisharp/laravel-filemanager/public/js/lfm.js')}}"></script>
    <!-- Angular Multi Select -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-multi-select/isteven-multi-select.js')}}"></script>

    <!-- UI Bootstrap Modal --> 
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-bootstrap/ui-bootstrap-tpls-2.0.0.min.js')}}"></script>

    <!-- Angular Toastr -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-toastr/angular-toastr.tpls.js')}}"></script>

    <!-- Angular Confirm Dialog -->
    <script type="text/javascript" src="{{asset('public/system/plugins/bootbox/bootbox.min.js')}}"></script>

    <!-- Angular Datatables-->
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/angular-datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/colreorder/angular-datatables.colreorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/bootstrap/angular-datatables.bootstrap.min.js')}}"></script>

    <!-- JS -->
    <script type="text/javascript" src="{{asset('public/system/common/system.common.angurlar.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/common/system.systeminformation.angurlar.js')}}"></script>

   
    <script type="text/javascript">
        $(document).ready(function() { 
            $(window).on('load', function() {  
                //insert all your ajax callback code here. 
                //Which will run only after page is fully loaded in background.
                $("#addReLoading").hide();
            });

            //Laravel File Manager
            $('#selectImages').filemanager('images');
        });

        $( "#dialog-modal" ).on( "dialogopen", function( event, ui ) {
            console.log('Wayhay!!');
            window.open("http://www.google.com");
        } );
    
    </script>
@stop

@section('container')
<div class="row" ng-app="systemInformationApp" ng-controller="systemInformationController"  ng-init="pageInit()">

    <!-- ============================Infofo list================== -->
    <div class="col-lg-12">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-fw fa-list" aria-hidden="true"></i> Thông Tin Cửa Hàng</h3>
                <!-- buton create -->
                <div class="box-tools pull-right">
                    <button ng-click="saveSystemInfo()" class="btn btn-primary" title="Thêm Mới">
                        <i class="fa fa-fw fa-plus" aria-hidden="true"></i>
                        <span>Lưu Thông Tin</span>
                    </button>
                </div>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <table  name="listResult" 
                                id="listResult" 
                                style="width: 100%;" 
                                datatable="ng" 
                                dt-options="dtOptions"
                                dt-column-defs="dtColumnDefs"
                                class="table table-bordered table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th class="text-center" data-column-index="0" style="width: auto;">info_id</th>
                                    <th class="text-center" data-column-index="1" style="width: auto;">info_key</th>
                                    <th class="text-center" data-column-index="2" style="width: auto;">info_values</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr role="row" ng-repeat="info in model.datainit.dtgListInfo">
                                    <td ng-show="info.info_key!='system_slideshow_detail'">[[info.info_id]]</td>
                                    <td ng-show="info.info_key!='system_slideshow_detail'">[[info.info_key]]</td>
                                    <td ng-show="info.info_key!='system_slideshow_detail'">
                                        <textarea
                                            style="width: 100%;" 
                                            rows="3" 
                                            class="form-control" 
                                            id="txtInvoiceId" 
                                            name="txtInvoiceId" 
                                            placeholder="[[info.info_key]]" 
                                            ng-model="info.info_values"
                                            autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                            ng-minlength="0"
                                            ng-value="info.info_values"

                                                >
                                        </textarea>
                                  
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <!-- /.box-body -->

            <!-- Loading (remove the following to stop the loading)-->
            <div id="listReLoading" class="overlay">
                <i class="fa fa-fw fa-refresh fa-spin"></i>
            </div>
            <!-- end loading -->

        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
    <form class="form" class="form-inline" name="frmAdd" id="frmAdd" novalidate>
        <!-- =============================slide list================== -->
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-fw fa-list" aria-hidden="true"></i> Thông Tin Slide</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <!-- =============== SLide Images ============-->
                    <div class="row" >
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="txtModalGroupId" class="control-label">Slide Url:</label>
                                <input  
                                        type="text"
                                        class="form-control" 
                                        id="txtSlideUrl" 
                                        name="txtSlideUrl" 
                                        ng-model="model.request.txtSlideUrl"
                                        autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                        ng-minlength="2"
                                        ng-maxlength="250"
                                        maxlength="250"
                                        required >
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="col-sm-12 control-label">Chọn Hình Slide:</label>
                            
                            <div class="form-group col-md-12">
                                <div class="input-group col-md-12">
                                    <span class="input-group-btn">
                                        <a  id="selectImages" 
                                            name="selectImages" 
                                            data-input="thumbnail" 
                                            data-preview="holder" 
                                            class="btn btn-primary">
                                            <i class="fa fa-fw fa-picture-o"></i>
                                            <span>Chọn Ảnh</span>
                                        </a>
                                    </span>
                                    <input  
                                        tabindex="5"
                                        name="thumbnail"
                                        id="thumbnail" 
                                        class="form-control" 
                                        type="text" 
                                        name="filepath" 
                                        placeholder="Chọn Ảnh"
                                        ng-model="model.request.txtImages" 
                                        autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                        ng-minlength="2"
                                        ng-maxlength="150"
                                        maxlength="150"
                                        ng-disabled=true
                                        required>
                                        <!--===========btn ADD======  -->
                                    <span class="input-group-btn">
                                        <button 
                                            ng-disabled="frmAdd.txtSlideUrl.$invalid"
                                            ng-click="addSlide()" 
                                            class="btn btn-primary" 
                                            title="Thêm Mới">
                                            <i class="fa fa-fw fa-plus" aria-hidden="true"></i>
                                            <span>Thêm Slide</span>
                                        </button>
                                    </span>
                                </div>
                            </div> 
                            <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-12 ">
                                <img
                                    style="height: 150px !important"  
                                    ng-src="[[model.request.txtImages]]"
                                     onError="this.src='{{asset('public/images/default-image.png')}}';"
                                     id="holder" 
                                     class="img-responsive img-thumbnail" 
                                     alt="Chọn Ảnh" 
                                     title="Chọn Ảnh " 
                                     height="150px">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table  name="listResult2" 
                                    id="listResult2" 
                                    style="width: 100%;" 
                                    datatable="ng" 
                                    dt-options="dtOptions"
                                    dt-column-defs="dtColumnDefs"
                                    class="table table-bordered table-striped table-condensed">
                                <thead>
                                    <tr>
                                        <th class="text-center" data-column-index="0" style="width: auto;">Tùy Chọn</th>
                                        <th class="text-center" data-column-index="0" style="width: auto;">Tên Slide</th>
                                        <th class="text-center" data-column-index="0" style="width: auto;">Đường Dẫn URL</th>
                                        <th class="text-center" data-column-index="1" style="width: auto;">Hình SLide</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr role="row" ng-repeat="sl in model.datainit.dtgListSlide">
                                        <td class="text-center">
                                            <button type="button" ng-click="removeSlide(sl)" class="btn btn-danger btn-xs"><i class="fa fa-fw fa-trash-o fa-fw"></i></button>
                                        </td>
                                        <td>
                                            [[sl.slide_name]]
                                        </td>
                                        <td>
                                            [[sl.slide_url]]
                                        </td>
                                        <td >
                                             <img 
                                             style="height: 100px !important" 
                                             ng-src="[[sl.slide_img]]"
                                             id="holder" 
                                             class="img-responsive img-thumbnail" 
                                             alt="Chọn Ảnh Slide" 
                                             title="Chọn Ảnh Slide">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

    </form>
    
</div>
@stop