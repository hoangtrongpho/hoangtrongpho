@extends('system.layouts.index')

@section('headTitle')
   <title>{{ Lang::get('messages.system_heade_title') }}</title>
@stop

@section('pageTitle')
   <h1>
        <i class="fa fa-fw fa-archive" aria-hidden="true"></i> Quản Trị Chi Nhánh
        <small>Skyfire Sytem Version 1.0</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{URL::to('/System/Dashboard')}}"><i class="fa fa-fw fa-dashboard"></i> Hệ Thống Quản Trị Website</a>
        </li>
        <li class="active"><i class="fa fa-fw fa-archive" aria-hidden="true"></i> Danh Sách Chi Nhánh</li>
    </ol>
@stop

@section('headCss')
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/daterangepicker/daterangepicker.css')}}"/>
<link rel="stylesheet" href="{{asset('public/system/plugins/angular-ui-switch/angular-ui-switch.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-ui-switch/bootstrap-switch.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-multi-select/isteven-multi-select.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-toastr/angular-toastr.css')}}" />

<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/jquery.dataTables.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/dataTables.bootstrap.css')}}" />

<style type="text/css">
    .control-label{
        padding-top: 5px;
        padding-right: 5px !important;
        padding-left: 5px !important;
        float: left;
    }

    #listResult {
        width: 100% !important;
    }
</style>
@stop

@section('headJs')
    <!-- Date Time Picker -->
    <script type="text/javascript" src="{{asset('public/system/plugins/daterangepicker/daterangepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-daterangepicker/angular-daterangepicker.min.js')}}"></script>

    <script src="{{asset('public/system/plugins/angular-ui-switch/angular-ui-switch.js')}}"></script>
    <script src="{{asset('public/system/plugins/angular-ui-switch/bootstrap-switch.js')}}"></script>
    <script src="{{asset('public/system/plugins/angular-ui-switch/angular-bootstrap-switch.js')}}"></script>

    <!-- iCheck -->
    <script src="{{asset('public/system/plugins/angular_bootstrap_checkbox/angular-bootstrap-checkbox.js')}}"></script>

    <!-- File Manager -->
    <!-- Angular Multi Select -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-multi-select/isteven-multi-select.js')}}"></script>

    <!-- UI Bootstrap Modal --> 
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-bootstrap/ui-bootstrap-tpls-2.0.0.min.js')}}"></script>

    <!-- Angular Toastr -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-toastr/angular-toastr.tpls.js')}}"></script>

    <!-- Angular Confirm Dialog -->
    <script type="text/javascript" src="{{asset('public/system/plugins/bootbox/bootbox.min.js')}}"></script>

    <!-- Angular Datatables-->
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/angular-datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/colreorder/angular-datatables.colreorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/bootstrap/angular-datatables.bootstrap.min.js')}}"></script>

    <!-- JS -->
    <script type="text/javascript" src="{{asset('public/system/common/system.common.angurlar.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/common/system.listbranch.angurlar.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function() { 
            $(window).on('load', function() {  
            });
        });
    </script>
@stop

@section('container')
<div class="row" ng-app="newsNewsApp" ng-controller="newsNewsController" ng-init="pageInit()">

    <!-- Modal -->
    <div id="modalAdd" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <form class="form" class="form-inline" name="frmAdd" id="frmAdd" novalidate>
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-blue" ng-show="model.datainit.editStatus=='add'">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-fw fa-floppy-o" aria-hidden="true"></i> Thêm Mới</h4>
                </div>
                <div class="modal-header bg-orange" ng-show="model.datainit.editStatus=='update'">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-fw fa-floppy-o" aria-hidden="true"></i>Cập Nhật</h4>
                </div>
                <div class="modal-body">
                        
                    <div class="col-xs-12">
                        <div class="form-group" ng-class="{ 'has-error' : frmAdd.branch_name.$touched && frmAdd.branch_name.$invalid && !frmAdd.branch_name.$pristine }">
                            <label for="txtName" class="control-label">Tên Chi Nhánh:</label>
                            <input  tabindex="1"
                                    type="text"
                                    class="form-control" 
                                    id="branch_name" 
                                    name="branch_name" 
                                    placeholder="Nhập Tên Chi Nhánh" 
                                    ng-model="model.request.branch_name"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                    ng-minlength="2"
                                    ng-maxlength="100"
                                    maxlength="100"
                                    required >
                            <span class="help-block" ng-show="frmAdd.branch_name.$touched && frmAdd.branch_name.$error.required && frmAdd.branch_name.$invalid && !frmAdd.branch_name.$pristine">Vui Lòng Nhập Tên Chi Nhánh.</span>
                            <span class="help-block" ng-show="frmAdd.branch_name.$touched && frmAdd.branch_name.$error.minlength">Tên Chi Nhánh Không Được Ngắn Hơn 2 Ký Tự.</span>
                            <span class="help-block" ng-show="frmAdd.branch_name.$touched && frmAdd.branch_name.$error.maxlength">Tên Chi Nhánh Không Được Dài Hơn 100 Ký Tự.</span>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group" ng-class="{ 'has-error' : frmAdd.branch_phone.$touched && frmAdd.branch_phone.$invalid && !frmAdd.branch_phone.$pristine }">
                            <label for="txtName" class="control-label">SĐT:</label>
                            <input  tabindex="1"
                                    type="text"
                                    class="form-control" 
                                    id="branch_phone" 
                                    name="branch_phone" 
                                    placeholder="Nhập SĐT" 
                                    ng-model="model.request.branch_phone"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                    ng-minlength="10"
                                    ng-maxlength="20"
                                    maxlength="20"
                                    required >
                            <span class="help-block" ng-show="frmAdd.branch_phone.$touched && frmAdd.branch_phone.$error.required && frmAdd.branch_phone.$invalid && !frmAdd.branch_phone.$pristine">Vui Lòng Nhập SĐT.</span>
                            <span class="help-block" ng-show="frmAdd.branch_phone.$touched && frmAdd.branch_phone.$error.minlength">SĐT Không Được Ngắn Hơn 10 Ký Tự.</span>
                            <span class="help-block" ng-show="frmAdd.branch_phone.$touched && frmAdd.branch_phone.$error.maxlength">SĐT Không Được Dài Hơn 20 Ký Tự.</span>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group" ng-class="{ 'has-error' : frmAdd.branch_address_number.$touched && frmAdd.branch_address_number.$invalid && !frmAdd.branch_address_number.$pristine }">
                            <label for="branch_address_number" class="control-label">Địa Chỉ:</label>
                            <input  tabindex="2"
                                    type="text"
                                    class="form-control" 
                                    id="branch_address_number" 
                                    name="branch_address_number" 
                                    placeholder="Nhập Địa Chỉ" 
                                    ng-model="model.request.branch_address_number"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                    ng-minlength="2"
                                    ng-maxlength="250"
                                    maxlength="250"
                                    required
                                    >
                            <span class="help-block" ng-show="frmAdd.branch_address_number.$touched && frmAdd.branch_address_number.$error.required && frmAdd.branch_address_number.$invalid && !frmAdd.branch_address_number.$pristine">Vui Lòng Nhập Địa Chỉ.</span>
                            <span class="help-block" ng-show="frmAdd.branch_address_number.$touched && frmAdd.branch_address_number.$error.minlength">Địa Chỉ Không Được Ngắn Hơn 2 Ký Tự.</span>
                            <span class="help-block" ng-show="frmAdd.branch_address_number.$touched && frmAdd.branch_address_number.$error.maxlength">Địa Chỉ Không Được Dài Hơn 250 Ký Tự.</span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal" tabindex="4">
                        <i class="fa fa-fw fa-ban" aria-hidden="true"></i>&nbsp;Đóng
                    </button>
                    <button ng-disabled="frmAdd.$invalid" 
                            ng-show="model.datainit.editStatus=='add'"
                            ng-click="addItem()"
                            tabindex="3" 
                            data-loading-text="<i class='fa fa-refresh fa-spin fa-fw'></i> Đang xử lý"
                            type="button" class="btn btn-primary" id="btnAdd" name="btnAdd">
                        <i class="fa fa-fw fa-plus" aria-hidden="true"></i>&nbsp;Thêm Mới
                    </button>
                    <button ng-disabled="frmAdd.$invalid" 
                            ng-show="model.datainit.editStatus=='update'"
                            ng-click="updateItem()"
                            tabindex="3" 
                            data-loading-text="<i class='fa fa-refresh fa-spin fa-fw'></i> Đang xử lý"
                            type="button" class="btn btn-warning" id="btnAdd" name="btnAdd">
                        <i class="fa fa-fw fa-pencil" aria-hidden="true"></i>&nbsp;Cập Nhật
                    </button>
                </div>
            </div>
        </form>
      </div>
    </div>

    <!-- DETAIL -->
    <div class="col-lg-12">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-fw fa-list" aria-hidden="true"></i> Danh Sách Chi Nhánh</h3>
                <!-- buton create -->
                <div class="box-tools pull-right">
                    <button 
                        ng-click= "ChangeStatus('add','')"
                        class="btn btn-primary" 
                        data-toggle="modal" 
                        data-target="#modalAdd" 
                        title="Thêm Mới">
                        <i class="fa fa-fw fa-plus" aria-hidden="true"></i>
                        <span>Thêm Mới</span>
                    </button>
                </div>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <table  name="listResult" 
                                id="listResult" 
                                style="width: 100%;" 
                                datatable="ng" 
                                dt-options="dtOptions"
                                dt-column-defs="dtColumnDefs"
                                class="table table-bordered table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th class="text-center" data-column-index="0" style="width: 80px;">Tùy Chọn</th>
                                    <th class="text-center" data-column-index="1" style="width: auto;">Mã Chi Nhánh</th>
                                    <th class="text-center" data-column-index="2" style="width: auto;">Tên Chi Nhánh</th>
                                    <th class="text-center" data-column-index="3" style="width: auto;">Địa Chỉ</th>
                                    <th class="text-center" data-column-index="4" style="width: auto;">Điện Thoại</th>
                                    <th class="text-center" data-column-index="6" style="width: auto;">Ngày Lập</th>
                                
                                </tr>
                            </thead>
                            <tbody>
                                <tr role="row" ng-repeat="br in model.datainit.dtgListBranch">
                                    <td class="text-center">
                                        <button
                                            ng-click= "ChangeStatus('update',br)"
                                            data-toggle="modal" data-target="#modalAdd"
                                            class="btn btn-warning btn-xs" 
                                            title="edit">
                                            <i class="fa fa-fw fa-edit fa-fw" aria-hidden="true"></i>
                                        </button>

                                        <button style="display: none;" type="button" ng-click="removeItem(br)" class="btn btn-danger btn-xs">
                                            <i class="fa fa-fw fa-trash-o fa-fw"></i>
                                        </button>
                                    </td>
                                    <td>[[br.branch_id]]</td>
                                    <td>[[br.branch_name]]</td>
                                    <td>[[br.branch_address_number]]</td>
                                    <td>[[br.branch_phone]]</td>
                                    <td>[[br.created_date_format]]</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <!-- /.box-body -->

            <!-- Loading (remove the following to stop the loading)-->
            <div id="listReLoading" class="overlay">
                <i class="fa fa-fw fa-refresh fa-spin"></i>
            </div>
            <!-- end loading -->

        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
@stop