@extends('system.layouts.index')

@section('headTitle')
   <title>{{ Lang::get('messages.system_heade_title') }}</title>
@stop

@section('pageTitle')
   <h1>
        <i class="fa fa-fw fa-address-card" aria-hidden="true"></i> Quản Trị Chức Vụ
        <small>{{ Lang::get('messages.system_verion') }}</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{URL::to('/System/Dashboard')}}"><i class="fa fa-fw fa-dashboard"></i> Hệ Thống Quản Trị Website</a>
        </li>
        <li class="active"><i class="fa fa-fw fa-industry" aria-hidden="true"></i> Chức Vụ</li>
    </ol>
@stop

@section('headCss')
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-toastr/angular-toastr.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/jquery.dataTables.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/dataTables.bootstrap.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-multi-select/isteven-multi-select.css')}}">
@stop

@section('headJs')
    <!-- Angular Multi Select -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-multi-select/isteven-multi-select.js')}}"></script>
    <!-- UI Bootstrap Modal --> 
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-bootstrap/ui-bootstrap-tpls-2.0.0.min.js')}}"></script>
    <!-- Angular Toastr -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-toastr/angular-toastr.tpls.js')}}"></script>
    <!-- Angular Confirm Dialog -->
    <script type="text/javascript" src="{{asset('public/system/plugins/bootbox/bootbox.min.js')}}"></script>
    <!-- Angular Datatables-->
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/angular-datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/colreorder/angular-datatables.colreorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/bootstrap/angular-datatables.bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/tabletools/angular-datatables.tabletools.min.js')}}"></script>
    <!-- JS -->
    <script type="text/javascript" src="{{asset('public/system/common/system.listpositions.angurlar.js')}}"></script>
@stop

@section('container')
<div class="row" ng-app="positionsApp" ng-controller="positionsController" ng-init="pageInit()">

    <!-- Modal -->
    <div id="modalAdd" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <form class="form" class="form-inline" name="frmAdd" id="frmAdd" novalidate>
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-blue">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-fw fa-floppy-o" aria-hidden="true"></i>
                    <span class="hidden-xs">Thêm Chức Vụ</span></h4>
                </div>
                <div class="modal-body">
                    <div class="col-xs-12" style="display: none;">
                        <div class="form-group" ng-class="{ 'has-error' : frmAdd.txtBranchId.$invalid && !frmAdd.txtBranchId.$pristine }">
                            <label for="slbBranch" class="control-label">Đơn Vị:</label>
                            <isteven-multi-select
                                name="slbBranch"
                                tabindex="11"
                                input-model="model.datainit.slbListBranches"
                                output-model="model.request.slbBranch"
                                helper-elements="reset"
                                button-label="branch_name"
                                item-label="branch_name"
                                output-properties="branch_id"
                                search-property="branch_name"
                                tick-property="ticked"
                                orientation="vertical"
                                disable-property="false"
                                is-disabled="false"
                                selection-mode="single"
                                min-search-length="10"
                                translation="model.localLangBranches"
                                max-labels="1"  
                                directive-id="slbBranch"
                                on-reset="slbBranchesClear()"
                                on-item-click="slbBranchesClick(data)">
                            </isteven-multi-select>
                            <span class="help-block" ng-show="frmAdd.txtBranchId.$error.required && frmAdd.txtBranchId.$invalid && !frmAdd.txtBranchId.$pristine">Vui Lòng Chọn Đơn Vị Công Tác.</span>
                            <input  type="text"
                                    class="form-control" 
                                    id="txtBranchId" 
                                    name="txtBranchId" 
                                    ng-model="model.request.txtBranchId"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" 
                                    style="display: none;" 
                                    required >
                        </div>
                    </div>
                    <div class="col-xs-12" style="display: none;">
                        <div class="form-group" ng-class="{ 'has-error' : frmAdd.txtDepartmentId.$invalid && !frmAdd.txtDepartmentId.$pristine }">
                            <label for="slbDepartment" class="control-label">Phòng Ban:</label>
                            <isteven-multi-select
                                name="slbDepartment"
                                tabindex="11"
                                input-model="model.datainit.slbListDepartments"
                                output-model="model.request.slbDepartment"
                                helper-elements="reset"
                                button-label="department_name"
                                item-label="department_name"
                                output-properties="department_id"
                                search-property="department_name"
                                tick-property="ticked"
                                orientation="vertical"
                                disable-property="false"
                                is-disabled="false"
                                selection-mode="single"
                                min-search-length="10"
                                translation="model.localLangDepartments"
                                max-labels="1"  
                                directive-id="slbBranch"
                                on-reset="slbDepartmentsClear()"
                                on-item-click="slbDepartmentsClick(data)">
                            </isteven-multi-select>
                            <span class="help-block" ng-show="frmAdd.txtDepartmentId.$error.required && frmAdd.txtDepartmentId.$invalid && !frmAdd.txtDepartmentId.$pristine">Vui Lòng Chọn Phòng Ban.</span>
                            <input  type="text"
                                    class="form-control" 
                                    id="txtDepartmentId" 
                                    name="txtDepartmentId" 
                                    ng-model="model.request.txtDepartmentId"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" 
                                    style="display: none;" 
                                    required >
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group" ng-class="{ 'has-error' : frmAdd.txtName.$invalid && !frmAdd.txtName.$pristine }">
                            <label for="txtName" class="control-label">Tên Chức Vụ:</label>
                            <input  tabindex="1"
                                    type="text"
                                    class="form-control" 
                                    id="txtName" 
                                    name="txtName" 
                                    placeholder="Nhập Tên Chức Vụ" 
                                    ng-model="model.request.txtName"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                    ng-minlength="2"
                                    ng-maxlength="50"
                                    maxlength="50"
                                    required >
                            <span class="help-block" ng-show="frmAdd.txtName.$error.required && frmAdd.txtName.$invalid && !frmAdd.txtName.$pristine">Vui Lòng Nhập Chức Vụ.</span>
                            <span class="help-block" ng-show="frmAdd.txtName.$error.minlength">Chức Vụ Không Được Ngắn Hơn 2 Ký Tự.</span>
                            <span class="help-block" ng-show="frmAdd.txtName.$error.maxlength">Chức Vụ Không Được Dài Hơn 50 Ký Tự.</span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal">
                        <i class="fa fa-fw fa-ban" aria-hidden="true"></i>&nbsp;Đóng
                    </button>
                    <button ng-disabled="frmAdd.$invalid" 
                            ng-click="addItem()"
                            type="submit" class="btn btn-primary" id="btnAdd" name="btnAdd">
                        <i class="fa fa-fw fa-plus" aria-hidden="true"></i>&nbsp;Thêm Mới
                    </button>
                </div>
            </div>
        </form>
      </div>
    </div>

    <!-- Modal -->
    <div id="modalUpdate" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <form class="form" class="form-inline" name="frmUpdate" id="frmUpdate" novalidate>
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-yellow">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-fw fa-pencil-square-o" aria-hidden="true"></i> Cập Nhật Chức Vụ</h4>
                </div>
                <div class="modal-body">
                    <div class="col-xs-12" style="display: none;">
                        <div class="form-group" ng-class="{ 'has-error' : frmAdd.txtBranchId.$invalid && !frmAdd.txtBranchId.$pristine }">
                            <label for="slbBranch" class="control-label">Đơn Vị:</label>
                            <isteven-multi-select
                                name="slbBranch"
                                tabindex="11"
                                input-model="model.datainit.slbListBranches"
                                output-model="model.request.slbBranch"
                                helper-elements="reset"
                                button-label="branch_name"
                                item-label="branch_name"
                                output-properties="branch_id"
                                search-property="branch_name"
                                tick-property="ticked"
                                orientation="vertical"
                                disable-property="false"
                                is-disabled="false"
                                selection-mode="single"
                                min-search-length="10"
                                translation="model.localLangBranches"
                                max-labels="1"  
                                directive-id="slbBranch"
                                on-reset="slbBranchesClear()"
                                on-item-click="slbBranchesClick(data)">
                            </isteven-multi-select>
                            <span class="help-block" ng-show="frmAdd.txtBranchId.$error.required && frmAdd.txtBranchId.$invalid && !frmAdd.txtBranchId.$pristine">Vui Lòng Chọn Đơn Vị Công Tác.</span>
                            <input  type="text"
                                    class="form-control" 
                                    id="txtBranchId" 
                                    name="txtBranchId" 
                                    ng-model="model.request.txtBranchId"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" 
                                    style="display: none;" 
                                    required >
                        </div>
                    </div>
                    <div class="col-xs-12" style="display: none;">
                        <div class="form-group" ng-class="{ 'has-error' : frmAdd.txtDepartmentId.$invalid && !frmAdd.txtDepartmentId.$pristine }">
                            <label for="slbDepartment" class="control-label">Phòng Ban:</label>
                            <isteven-multi-select
                                name="slbDepartment"
                                tabindex="11"
                                input-model="model.datainit.slbListDepartments"
                                output-model="model.request.slbDepartment"
                                helper-elements="reset"
                                button-label="department_name"
                                item-label="department_name"
                                output-properties="department_id"
                                search-property="department_name"
                                tick-property="ticked"
                                orientation="vertical"
                                disable-property="false"
                                is-disabled="false"
                                selection-mode="single"
                                min-search-length="10"
                                translation="model.localLangDepartments"
                                max-labels="1"  
                                directive-id="slbBranch"
                                on-reset="slbDepartmentsClear()"
                                on-item-click="slbDepartmentsClick(data)">
                            </isteven-multi-select>
                            <span class="help-block" ng-show="frmAdd.txtDepartmentId.$error.required && frmAdd.txtDepartmentId.$invalid && !frmAdd.txtDepartmentId.$pristine">Vui Lòng Chọn Phòng Ban.</span>
                            <input  type="text"
                                    class="form-control" 
                                    id="txtDepartmentId" 
                                    name="txtDepartmentId" 
                                    ng-model="model.request.txtDepartmentId"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" 
                                    style="display: none;" 
                                    required >
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group" ng-class="{ 'has-error' : frmUpdate.txtName.$invalid && !frmUpdate.txtName.$pristine }">
                            <label for="txtName" class="control-label">Tên Chức Vụ:</label>
                            <input  tabindex="1"
                                    type="text"
                                    class="form-control" 
                                    id="txtName" 
                                    name="txtName" 
                                    placeholder="Nhập Tên Chức Vụ" 
                                    ng-model="model.request.txtName"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                    ng-minlength="2"
                                    ng-maxlength="50"
                                    maxlength="50"
                                    required >
                            <span class="help-block" ng-show="frmUpdate.txtName.$error.required && frmUpdate.txtName.$invalid && !frmUpdate.txtName.$pristine">Vui Lòng Nhập Chức Vụ.</span>
                            <span class="help-block" ng-show="frmUpdate.txtName.$error.minlength">Chức Vụ Không Được Ngắn Hơn 2 Ký Tự.</span>
                            <span class="help-block" ng-show="frmUpdate.txtName.$error.maxlength">Chức Vụ Không Được Dài Hơn 50 Ký Tự.</span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal">
                        <i class="fa fa-fw fa-ban" aria-hidden="true"></i>&nbsp;Đóng
                    </button>
                    <button ng-disabled="frmUpdate.$invalid" 
                            ng-click="updateItem()"
                            type="submit" class="btn btn-warning" id="btnUpdate" name="btnUpdate">
                        <i class="fa fa-fw fa-pencil-square-o" aria-hidden="true"></i>&nbsp;Cập Nhật
                    </button>
                </div>
            </div>
        </form>
      </div>
    </div>

    <div class="col-lg-12 col-xs-12">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-fw fa-address-card" aria-hidden="true"></i>&nbsp;Danh Sách Chức Vụ</h3>
                <div class="box-tools pull-right">
                <button id="openAddModal" type="button" class="btn btn-primary" tabindex="4" data-toggle="modal" data-target="#modalAdd">
                    <i class="fa fa-fw fa-plus" aria-hidden="true"></i>
                    <span class="hidden-xs">Thêm Chức Vụ</span>
                </button>

               </div>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
                <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                    <div class="row">
                        <div class="col-sm-12">
                            <table  name="listResult" 
                                    datatable="ng" 
                                    dt-options="dtOptions"
                                    dt-column-defs="dtColumnDefs"
                                    width="100%" 
                                    class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center" style="width:80px;" data-column-index="0" >Tùy Chọn</th>
                                        <th class="text-center" style="width:auto;" data-column-index="1" >Chức Vụ</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr role="row" ng-repeat="item in model.datainit.dtgList">
                                        <td class="text-center">
                                            <button type="button" ng-click="openUpdateItem(item)" class="btn btn-warning"><i class="fa fa-fw fa-edit"></i></button>
                                            <button type="button" ng-click="removeItem(item.position_id)" class="btn btn-danger"><i class="fa fa-fw fa-trash-o"></i></button>
                                        </td>
                                        <td class="text-center">[[item.position_name]]</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
            <!-- /.box-body -->

            <!-- Loading (remove the following to stop the loading)-->
            <div id="listReLoading" class="overlay">
                <i class="fa fa-fw fa-refresh fa-spin"></i>
            </div>
            <!-- end loading -->

        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->

</div>
@stop