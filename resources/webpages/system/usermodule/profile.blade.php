@extends('system.layouts.index')

@section('headTitle')
   <title>{{ Lang::get('messages.system_heade_title') }}</title>
@stop

@section('pageTitle')
   <h1>
        <i class="fa fa-fw fa-user" aria-hidden="true"></i> Thông Tin Cá Nhân
        <small>{{ Lang::get('messages.system_verion') }}</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{URL::to('/System/Dashboard')}}"><i class="fa fa-fw fa-dashboard"></i> Hệ Thống Quản Trị Website</a>
        </li>
        <li class="active"><i class="fa fa-fw fa-user" aria-hidden="true"></i> Thông Tin Cá Nhân</li>
    </ol>
@stop

@section('headCss')
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-ui-switch/bootstrap-switch.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-multi-select/isteven-multi-select.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-toastr/angular-toastr.css')}}" />

    <style type="text/css">
        .has-error>.mce-tinymce{
            border-color: #dd4b39;
            box-shadow: none;
        }
        .has-error>.input-group, .has-error>.input-group>.input-group-addon{
            border-color: #dd4b39;
            box-shadow: none;
        }

        .has-error button, .has-error button:active{
            border-color: #dd4b39;
        }

        .sblTags .multiSelect .checkboxLayer{
            bottom: 35px;
        }
        .sblTags .multiSelect .buttonLabel{
            width: auto;
        }
    </style>
@stop

@section('headJs')
    <script src="{{asset('public/system/plugins/angular-ui-switch/bootstrap-switch.js')}}"></script>
    <script src="{{asset('public/system/plugins/angular-ui-switch/angular-bootstrap-switch.js')}}"></script>

    <!-- iCheck -->
    <script src="{{asset('public/system/plugins/angular_bootstrap_checkbox/angular-bootstrap-checkbox.js')}}"></script>

    <!-- File Manager -->
    <script src="{{asset('/vendor/unisharp/laravel-filemanager/public/js/lfm.js')}}"></script>

    <!-- UI Bootstrap Modal --> 
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-bootstrap/ui-bootstrap-tpls-2.0.0.min.js')}}"></script>

    <!-- Angular Toastr -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-toastr/angular-toastr.tpls.js')}}"></script>

    <!-- Angular Confirm Dialog -->
    <script type="text/javascript" src="{{asset('public/system/plugins/bootbox/bootbox.min.js')}}"></script>

    <!-- JS -->
    <script type="text/javascript" src="{{asset('public/system/common/system.common.angurlar.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/common/system.profile.angurlar.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function() { 
            $(window).on('load', function() {  
                //Which will run only after page is fully loaded in background.
                $("#addReLoading").hide();
            });

            //Laravel File Manager
            $('#lfm1').filemanager('image');
        });
    </script>

@stop

@section('container')
<div class="row" ng-app="userApp" ng-controller="profileController">
    <div class="col-xs-12" >
        <div class="box box-danger" ng-class="box-warning">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-fw fa-pencil-square-o" aria-hidden="true"></i> Cập Nhật Người Dùng</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-fw fa-minus"></i></button>
               </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <form class="form" name="frmAddUser" id="frmAddUser" ng-init="pageInit('{{ Session::get('userAuth')->user_id }}')" novalidate>
                    <input  type="text"
                            id="txtUserId" 
                            name="txtUserId" 
                            ng-model="model.request.user_id"
                            style="display:none;" 
                            >
                    <div class="col-xs-8">
                        <div class="form-group row" ng-class="{ 'has-error' : frmAddUser.txtUserAccount.$touched && frmAddUser.txtUserAccount.$invalid && !frmAddUser.txtUserAccount.$pristine }">
                            <label for="txtUserAccount" class="col-sm-12 control-label">Tài Khoản:</label>
                            <div class="col-sm-12">
                                <input  type="text"
                                        tabindex="1"
                                        ng-disabled="true"
                                        class="form-control" 
                                        id="txtUserAccount" 
                                        name="txtUserAccount" 
                                        placeholder="Tài Khoản Đăng Nhập" 
                                        ng-model="model.request.user_account"
                                        autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                        ng-minlength="3"
                                        ng-maxlength="30"
                                        maxlength="30"
                                        ng-pattern="/^[a-zA-Z0-9-]*$/"
                                        ng-disabled="model.datainit.useraccountupdate"
                                        required >
                                <span class="help-block" ng-show="frmAddUser.txtUserAccount.$touched && frmAddUser.txtUserAccount.$error.required && frmAddUser.txtUserAccount.$invalid && !frmAddUser.txtUserAccount.$pristine">Vui Lòng Nhập Tài Khoản.</span>
                                <span class="help-block" ng-show="frmAddUser.txtUserAccount.$touched && frmAddUser.txtUserAccount.$error.minlength">Tài Khoản Không Được Ngắn Hơn 3 Ký Tự.</span>
                                <span class="help-block" ng-show="frmAddUser.txtUserAccount.$touched && frmAddUser.txtUserAccount.$error.maxlength">Tài Khoản Không Được Dài Hơn 30 Ký Tự.</span>
                                <span class="help-block" ng-show="frmAddUser.txtUserAccount.$touched && frmAddUser.txtUserAccount.$error.pattern">Tài Khoản Không Được Có Ký Tự Đặc Biệt.</span>
                            </div>
                        </div>

                        <div class="form-group row" ng-class="{ 'has-error' : frmAddUser.txtUserFullName.$touched && frmAddUser.txtUserFullName.$invalid && !frmAddUser.txtUserFullName.$pristine }">
                            <label for="txtUserFullName" class="col-sm-12 control-label">Họ Và Tên:</label>
                            <div class="col-sm-12">
                                <input  type="text"
                                        tabindex="4"
                                        class="form-control" 
                                        id="txtUserFullName" 
                                        name="txtUserFullName" 
                                        placeholder="Nhập Họ Và Tên" 
                                        ng-model="model.request.user_fullname"
                                        autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                        ng-minlength="2"
                                        ng-maxlength="50"
                                        maxlength="50"
                                        required >
                                <span class="help-block" ng-show="frmAddUser.txtUserFullName.$touched && frmAddUser.txtUserFullName.$error.required && frmAddUser.txtUserFullName.$invalid && !frmAddUser.txtUserFullName.$pristine">Vui Lòng Nhập Họ Và Tên.</span>
                                <span class="help-block" ng-show="frmAddUser.txtUserFullName.$touched && frmAddUser.txtUserFullName.$error.minlength">Họ Và Tên Không Được Ngắn Hơn 2 Ký Tự.</span>
                                <span class="help-block" ng-show="frmAddUser.txtUserFullName.$touched && frmAddUser.txtUserFullName.$error.maxlength">Họ Và Tên Không Được Dài Hơn 50 Ký Tự.</span>
                            </div>
                        </div>

                        <div class="form-group" ng-class="{ 'has-error' : frmAddUser.txtUserPassword.$touched && frmAddUser.txtUserPassword.$invalid && !frmAddUser.txtUserPassword.$pristine }">
                            <label for="txtUserPassword" class="control-label">Mật Khẩu:</label>
                            <input  type="password"
                                    class="form-control" 
                                    tabindex="1"
                                    id="txtUserPassword" 
                                    name="txtUserPassword" 
                                    placeholder="Nhập Mật Khẩu Đăng Nhập" 
                                    ng-model="model.request.user_password"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                    ng-minlength="6"
                                    ng-maxlength="30"
                                    maxlength="30"
                                    ng-trim="false" 
                                    ng-change="model.request.txtUserPassword = model.request.txtUserPassword.split(' ').join('')"
                                    required >
                            <span class="help-block" ng-show="frmAddUser.txtUserPassword.$touched && frmAddUser.txtUserPassword.$error.required && frmAddUser.txtUserPassword.$invalid && !frmAddUser.txtUserPassword.$pristine">Vui Lòng Nhập Mật Khẩu.</span>
                            <span class="help-block" ng-show="frmAddUser.txtUserPassword.$touched && frmAddUser.txtUserPassword.$error.minlength">Mật Khẩu Không Được Ngắn Hơn 6 Ký Tự.</span>
                            <span class="help-block" ng-show="frmAddUser.txtUserPassword.$touched && frmAddUser.txtUserPassword.$error.maxlength">Mật Khẩu Không Được Dài Hơn 30 Ký Tự.</span>
                        </div>

                        <div class="form-group" ng-class="{ 'has-error' : (frmAddUser.txtUserPasswordAgain.$touched && frmAddUser.txtUserPasswordAgain.$invalid && !frmAddUser.txtUserPasswordAgain.$pristine) || (frmAddUser.txtUserPasswordAgain.$valid && frmAddUser.txtUserPassword.$valid && model.request.txtUserPassword != model.request.txtUserPasswordAgain)}">
                            <label for="txtUserPasswordAgain" class="control-label">Xác Nhận Mật Khẩu:</label>
                            <input  type="password"
                                    class="form-control" 
                                    tabindex="2"
                                    id="txtUserPasswordAgain" 
                                    name="txtUserPasswordAgain" 
                                    placeholder="Xác Nhận Mật Khẩu Đăng Nhập" 
                                    ng-model="model.request.user_password_confirm"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                    ng-minlength="6"
                                    ng-maxlength="30"
                                    maxlength="30"
                                    ng-trim="false" 
                                    ng-change="model.request.txtUserPasswordAgain = model.request.txtUserPasswordAgain.split(' ').join('')"
                                    required 
                                    focus>
                            <span class="help-block" ng-show="frmAddUser.txtUserPasswordAgain.$touched && frmAddUser.txtUserPasswordAgain.$error.required && frmAddUser.txtUserPasswordAgain.$invalid && !frmAddUser.txtUserPasswordAgain.$pristine">Vui Lòng Xác Nhận Mật Khẩu.</span>
                            <span class="help-block" ng-show="frmAddUser.txtUserPasswordAgain.$touched && frmAddUser.txtUserPasswordAgain.$error.minlength">Mật Khẩu Không Được Ngắn Hơn 6 Ký Tự.</span>
                            <span class="help-block" ng-show="frmAddUser.txtUserPasswordAgain.$touched && frmAddUser.txtUserPasswordAgain.$error.maxlength">Mật Khẩu Không Được Dài Hơn 30 Ký Tự.</span>
                            <span class="help-block" ng-show="frmAddUser.txtUserPasswordAgain.$valid && frmAddUser.txtUserPassword.$valid && model.request.user_password != model.request.user_password_confirm">Mật Khẩu Không Trùng Khớp</span>
                        </div>

                        <div class="form-group row" ng-class="{ 'has-error' : frmAddUser.txtUserEmail.$touched && frmAddUser.txtUserEmail.$invalid && !frmAddUser.txtUserEmail.$pristine }">
                            <label for="txtUserEmail" class="col-sm-12 control-label">Email:</label>
                            <div class="col-sm-12">
                                <input  type="text"
                                        tabindex="6"
                                        class="form-control" 
                                        id="txtUserEmail" 
                                        name="txtUserEmail" 
                                        placeholder="Nhập Email" 
                                        ng-model="model.request.user_email"
                                        autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                        ng-minlength="3"
                                        ng-maxlength="100"
                                        maxlength="100"
                                        ng-pattern="/^[a-zA-Z0-9!#$%&amp;'*+\/=?^_`{|}~.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*$/"
                                        required >
                                <span class="help-block" ng-show="frmAddUser.txtUserEmail.$touched && frmAddUser.txtUserEmail.$error.required && frmAddUser.txtUserEmail.$invalid && !frmAddUser.txtUserEmail.$pristine">Vui Lòng Nhập Email.</span>
                                <span class="help-block" ng-show="frmAddUser.txtUserEmail.$touched && frmAddUser.txtUserEmail.$error.minlength">Email Không Được Ngắn Hơn 3 Ký Tự.</span>
                                <span class="help-block" ng-show="frmAddUser.txtUserEmail.$touched && frmAddUser.txtUserEmail.$error.maxlength">Email Không Được Dài Hơn 100 Ký Tự.</span>
                                <span class="help-block" ng-show="frmAddUser.txtUserEmail.$touched && frmAddUser.txtUserEmail.$error.pattern">Định Dạng Email Không Chính Xác.</span>
                            </div>
                        </div>

                        <div class="form-group row" ng-show="model.request.status != 9">
                            <label for="sblSex" class="col-sm-3 control-label">Giới Tính:</label>
                        </div>

                        <div class="form-group row" ng-show="model.request.status != 9">
                            <div class="col-sm-12">
                                <input
                                    bs-switch
                                    tabindex="6"
                                    name="sblSex"
                                    ng-model="model.request.user_sex"
                                    type="checkbox"
                                    switch-active="true"
                                    switch-readonly="false"
                                    switch-size="normal"
                                    switch-animate="true"
                                    switch-label=""
                                    switch-icon="icon-save"
                                    switch-on-text="Nam"
                                    switch-off-text="Nữ"
                                    switch-on-color="success"
                                    switch-off-color="danger"
                                    switch-radio-off="false"
                                    switch-label-width="0px"
                                    switch-handle-width="auto"
                                    switch-inverse="false"
                                    switch-change="onChange()"
                                    ng-true-value="1"
                                    ng-false-value="0">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <?php 
                            $sessionAuth = Session::get('userAuth');
                            if($sessionAuth->status !== 9)
                            {
                        ?>
                            <div class="form-group row">
                                <label for="lfm1" class="col-sm-12 control-label">Ảnh Đại Diện:</label> 
                                <div class="form-group col-md-12">
                                    <div class="input-group col-md-12">
                                        <span class="input-group-btn">
                                            <a  id="lfm1" 
                                                name="lfm1" 
                                                data-input="thumbnail" 
                                                data-preview="holder" 
                                                class="btn btn-primary"
                                                style="margin: 0px -2px 0px 0px !important;">
                                                <i class="fa fa-fw fa-picture-o"></i>
                                                <span> Chọn Ảnh</span>
                                            </a>
                                        </span>
                                        <input  id="thumbnail" 
                                                tabindex="8"
                                                class="form-control" 
                                                type="text" 
                                                name="filepath" 
                                                placeholder="Mô Tả Danh Mục"
                                                ng-model="model.request.user_avatar" 
                                                autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                                        <i  class="fa fa-fw fa-times pull-right" 
                                            style="position: absolute;right: 10px;z-index: 2;top: 10px;" 
                                            ng-click="resetChoiceThumbnail()"></i> 
                                    </div>
                                </div> 
                                <div class="form-group col-xs-8 col-xs-offset-2"> 
                                    <img ng-src="{{asset('/')}}[[model.request.user_avatar]]"
                                         onError="this.src='{{asset('public/images/default-image.png')}}';"
                                         id="holder" 
                                         class="img-responsive img-thumbnail" 
                                         alt="Chọn Ảnh Đại Diện" 
                                         title="Chọn Ảnh Đại Diện" 
                                         height="150px">
                                </div>
                            </div>
                        <?php 
                            }
                            else
                            {
                        ?>
                            <div class="form-group row">
                                <label for="lfm1" class="col-sm-12 control-label">Ảnh Đại Diện:</label> 
                                <input  id="thumbnail" 
                                    tabindex="8"
                                    class="form-control" 
                                    type="text" 
                                    name="filepath" 
                                    placeholder="Mô Tả Danh Mục"
                                    ng-model="model.request.user_avatar" 
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                    style="display: none;">
                            </div>
                            <div class="form-group row">
                                <div class="form-group col-xs-8 col-xs-offset-2"> 
                                    <img ng-src="{{asset('/')}}[[model.request.user_avatar]]"
                                         onError="this.src='{{asset('public/images/default-image.png')}}';"
                                         id="holder" 
                                         class="img-responsive img-thumbnail" 
                                         alt="Chọn Ảnh Đại Diện" 
                                         title="Chọn Ảnh Đại Diện" 
                                         height="150px">
                                </div>
                            </div>
                        <?php 
                            }
                        ?>
                       
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" ng-show="model.request.user_id != ''">
                                    <button type="button" 
                                            tabindex="10"
                                            class="btn btn-default pull-left"
                                            ng-click="pageInit('{{ Session::get('userAuth')->user_id }}')">
                                        <i class="fa fa-fw fa-ban" aria-hidden="true"></i>
                                        <span class="hidden-xs">Hủy</span>
                                    </button>
                                    <button type="button" 
                                            ng-click="updateItem()"
                                            tabindex="9"
                                            class="btn btn-warning pull-right"
                                            ng-disabled="frmAddUser.txtUserFullName.$invalid
                                                      || frmAddUser.txtUserAccount.$invalid
                                                      || frmAddUser.txtUserEmail.$invalid
                                                      || frmAddUser.txtAuthId.$invalid
                                                      || (frmAddUser.txtUserPasswordAgain.$touched && frmAddUser.txtUserPassword.$touched && model.request.user_password != model.request.user_password_confirm)">
                                        <i class="fa fa-fw fa-pencil-square-o" aria-hidden="true"></i>
                                        <span class="hidden-xs">Cập Nhật</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <!--/ box body-->
            <!-- Loading (remove the following to stop the loading)-->
            <div class="overlay" id="addReLoading">
                <i class="fa fa-fw fa-refresh fa-spin"></i>
            </div>
            <!-- end loading -->
        </div>
    </div>
</div>
@stop