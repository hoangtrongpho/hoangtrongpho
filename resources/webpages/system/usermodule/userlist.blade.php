@extends('system.layouts.index')

@section('headTitle')
   <title>{{ Lang::get('messages.system_heade_title') }}</title>
@stop

@section('pageTitle')
   <h1>
        <i class="fa fa-fw fa-users" aria-hidden="true"></i> Quản Trị Người Dùng
        <small>{{ Lang::get('messages.system_verion') }}</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{URL::to('/System/Dashboard')}}"><i class="fa fa-fw fa-dashboard"></i> Hệ Thống Quản Trị Website</a>
        </li>
        <li class="active"><i class="fa fa-fw fa-user-circle" aria-hidden="true"></i> Quản Trị Người Dùng</li>
    </ol>
@stop

@section('headCss')
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/daterangepicker/daterangepicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-ui-switch/bootstrap-switch.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-multi-select/isteven-multi-select.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-toastr/angular-toastr.css')}}" />

    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/jquery.dataTables.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/dataTables.bootstrap.css')}}" />
    <style type="text/css">
        .has-error>.mce-tinymce{
            border-color: #dd4b39;
            box-shadow: none;
        }
        .has-error>.input-group, .has-error>.input-group>.input-group-addon{
            border-color: #dd4b39;
            box-shadow: none;
        }

        .has-error button, .has-error button:active{
            border-color: #dd4b39;
        }

        .sblTags .multiSelect .checkboxLayer{
            bottom: 35px;
        }
        .sblTags .multiSelect .buttonLabel{
            width: auto;
        }
    </style>
@stop

@section('headJs')
    <script src="{{asset('public/system/plugins/angular-ui-switch/bootstrap-switch.js')}}"></script>
    <script src="{{asset('public/system/plugins/angular-ui-switch/angular-bootstrap-switch.js')}}"></script>

    <!-- iCheck -->
    <script src="{{asset('public/system/plugins/angular_bootstrap_checkbox/angular-bootstrap-checkbox.js')}}"></script>

    <!-- File Manager -->
    <script src="{{asset('/vendor/unisharp/laravel-filemanager/public/js/lfm.js')}}"></script>

    <!-- Angular Multi Select -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-multi-select/isteven-multi-select.js')}}"></script>

    <!-- UI Bootstrap Modal --> 
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-bootstrap/ui-bootstrap-tpls-2.0.0.min.js')}}"></script>

    <!-- Angular Toastr -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-toastr/angular-toastr.tpls.js')}}"></script>

    <!-- Angular Confirm Dialog -->
    <script type="text/javascript" src="{{asset('public/system/plugins/bootbox/bootbox.min.js')}}"></script>

    <!-- Angular Datatables-->
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/angular-datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/colreorder/angular-datatables.colreorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/bootstrap/angular-datatables.bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/tabletools/angular-datatables.tabletools.min.js')}}"></script>

    <!-- JS -->
    <script type="text/javascript" src="{{asset('public/system/common/system.common.angurlar.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/common/system.listuser.angurlar.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function() { 
            $(window).on('load', function() {  
                //Which will run only after page is fully loaded in background.
                $("#addReLoading").hide();
            });

            //Laravel File Manager
            $('#lfm1').filemanager('image');

            //Minify Left Menu
            //$("body").addClass("sidebar-collapse");
        });
    </script>

@stop

@section('container')
<div class="row" ng-app="userApp">
    <div class="col-md-8 col-xs-12" ng-controller="listUsersController" ng-init="pageInit()">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-fw fa-users" aria-hidden="true"></i> Danh Sách Người Dùng</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-fw fa-minus"></i>
                  </button>
               </div>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
                <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                    <div class="row">
                        <div class="col-sm-12">
                            <table  name="listResult" 
                                    datatable="ng" 
                                    dt-options="dtOptions"
                                    dt-column-defs="dtColumnDefs"
                                    class="table table-bordered table-striped"
                                    style="width: 100%;">
                                <thead>
                                    <tr> 
                                        <th class="text-center" data-column-index="0" >Chọn</th>
                                        <th class="text-center col-md-3" data-column-index="1" >Tài Khoản</th>
                                        <th class="text-center col-md-3" data-column-index="2" >Họ Và Tên</th>
                                        <th class="text-center col-md-3" data-column-index="3" >Nhóm Quyền</th>
                                        <th class="text-center col-md-1" data-column-index="4" >Tr.Thái</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr role="row" ng-repeat="user in model.datainit.dtgListUser">
                                        <td class="text-center">
                                            <button ng-show="user.status != 9" type="button" ng-click="openUpdateItem(user)" class="btn btn-sm btn-warning"><i class="fa fa-fw fa-edit"></i></button>
                                            <button ng-show="user.status != 9" type="button" ng-click="removeItem(user.user_id)" class="btn btn-sm btn-danger"><i class="fa fa-fw fa-trash-o"></i></button>
                                        </td>
                                        <td class="text-left">[[user.user_account]]</td>
                                        <td class="text-left">[[user.user_fullname]]</td>
                                        <td class="text-center">[[user.auth_name]]</td>
                                        <td class="text-center">
                                            <span style="display:none;">[[user.auth_name]]</span>
                                            <button ng-show="user.status == 1" type="button" ng-click="changeStatus(user.user_id, 0)" class="btn btn-sm btn-success" title="Quyền Quản Trị Hệ Thống">&nbsp;Mở&nbsp;</button>
                                            <button ng-show="user.status == 0" type="button" ng-click="changeStatus(user.user_id, 1)" class="btn btn-sm btn-danger" title="Quyền Quản Trị Hệ Thống">Khóa</button>
                                            <button ng-show="user.status == 9" type="button" class="btn btn-danger" disabled title="Quyền Quản Trị Hệ Thống" style="cursor: no-drop;">Manager</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
            <!-- /.box-body -->

            <!-- Loading (remove the following to stop the loading)-->
            <div id="listReLoading" class="overlay">
                <i class="fa fa-fw fa-refresh fa-spin"></i>
            </div>
            <!-- end loading -->

        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->

    <div class="col-md-4 col-xs-12" ng-controller="addUserController">
        <div class="box box-danger" ng-class="{ 'box-warning' : model.request.user_id != '' }">

            <div class="box-header">

                <h3 class="box-title" ng-show="model.request.user_id == ''"><i class="fa fa-fw fa-floppy-o" aria-hidden="true"></i> Thêm Mới Người Dùng</h3>

                <h3 class="box-title" ng-show="model.request.user_id != ''"><i class="fa fa-fw fa-pencil-square-o" aria-hidden="true"></i> Cập Nhật Người Dùng</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-fw fa-minus"></i></button>
               </div>
            </div>
            <!-- /.box-header -->

            <div class="box-body">
                <form class="form" name="frmAddUser" id="frmAddUser" ng-init="pageInit()" novalidate>

                    <input  type="text"
                            id="txtUserId" 
                            name="txtUserId" 
                            ng-model="model.request.user_id"
                            style="display:none;" 
                            >

                    <div class="form-group row" ng-class="{ 'has-error' : frmAddUser.txtAuthId.$invalid && !frmAddUser.txtAuthId.$pristine }">
                        <label for="slbListGroup" class="col-sm-12 control-label">Nhóm Quyền:</label>
                        <div class="col-sm-12">
                            <isteven-multi-select
                                name="slbGroup"
                                input-model="model.datainit.slbListGroup"
                                output-model="model.request.slbGroup"
                                helper-elements="reset"
                                button-label="auth_name"
                                item-label="auth_name"
                                output-properties="auth_id"
                                search-property="auth_name"
                                tick-property="ticked"
                                orientation="vertical"
                                disable-property="true"
                                is-disabled="false"
                                selection-mode="single"
                                min-search-length="1"
                                translation="model.localLang"
                                max-labels="1"  
                                directive-id="slbListGroup"
                                on-reset="slbClear()"
                                on-item-click="slbItemClick(data,'txtAuthId')">
                            </isteven-multi-select>
                            <span class="help-block" ng-show="frmAdd.txtAuthId.$error.required && frmAdd.txtAuthId.$invalid && !frmAdd.txtAuthId.$pristine">Vui Lòng Chọn Nhóm Quyền.</span>
                        </div>
                        <input  type="text"
                                class="form-control" 
                                id="txtAuthId" 
                                name="txtAuthId" 
                                ng-model="model.request.txtAuthId"
                                autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                style="display:none;" 
                                required >
                    </div>
                    <div class="form-group row" ng-class="{ 'has-error' : frmAddUser.txtBranchId.$invalid && !frmAddUser.txtBranchId.$pristine }">
                        <label for="txtBranchId" class="col-sm-12 control-label">Chi Nhánh:</label>
                        <div class="col-sm-12">
                            <isteven-multi-select
                              tabindex="2"
                              name="slbBranch"
                              input-model="model.datainit.slbBranch"
                              output-model="model.request.slbBranch"
                              helper-elements="filter"
                              button-label="branch_name"
                              item-label="branch_name"
                              output-properties="branch_id"
                              search-property="branch_name"
                              tick-property="ticked"
                              orientation="vertical"
                              disable-property="false"
                              is-disabled="false"
                              selection-mode="single"
                              min-search-length="10"
                              translation="model.localLang"
                              max-labels="1"  
                              directive-id="slbBranch"
                              on-item-click="slbItemClick(data,'txtBranchId')">
                            </isteven-multi-select>
                            <span class="help-block" ng-show="frmAdd.txtBranchId.$error.required && frmAdd.txtBranchId.$invalid && !frmAdd.txtBranchId.$pristine">Vui Lòng Chọn Chi Nhánh.</span>
                        </div>
                        <input  
                            type="text"
                            class="form-control" 
                            id="txtBranchId" 
                            name="txtBranchId" 
                            ng-model="model.request.branch_id"
                            autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false""
                            style="display: none;"
                            required 
                           >
                    </div>
                    <div class="form-group row" ng-class="{ 'has-error' : frmAddUser.txtUserAccount.$touched && frmAddUser.txtUserAccount.$invalid && !frmAddUser.txtUserAccount.$pristine }">
                        <label for="txtUserAccount" class="col-sm-12 control-label">Tài Khoản:</label>
                        <div class="col-sm-12">
                            <input  type="text"
                                    class="form-control" 
                                    id="txtUserAccount" 
                                    name="txtUserAccount" 
                                    placeholder="Tài Khoản Đăng Nhập" 
                                    ng-model="model.request.user_account"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                    ng-minlength="2"
                                    ng-maxlength="15"
                                    maxlength="15"
                                    ng-pattern="/^[a-zA-Z0-9-]*$/"
                                    ng-disabled="model.datainit.useraccountupdate"
                                    required >
                            <span class="help-block" ng-show="frmAddUser.txtUserAccount.$touched && frmAddUser.txtUserAccount.$error.required && frmAddUser.txtUserAccount.$invalid && !frmAddUser.txtUserAccount.$pristine">Vui Lòng Nhập Tài Khoản.</span>
                            <span class="help-block" ng-show="frmAddUser.txtUserAccount.$touched && frmAddUser.txtUserAccount.$error.minlength">Tài Khoản Không Được Ngắn Hơn 2 Ký Tự.</span>
                            <span class="help-block" ng-show="frmAddUser.txtUserAccount.$touched && frmAddUser.txtUserAccount.$error.maxlength">Tài Khoản Không Được Dài Hơn 15 Ký Tự.</span>
                            <span class="help-block" ng-show="frmAddUser.txtUserAccount.$touched && frmAddUser.txtUserAccount.$error.pattern">Tài Khoản Không Được Có Ký Tự Đặc Biệt.</span>
                        </div>
                    </div>

                    <div class="form-group" ng-class="{ 'has-error' : frmAddUser.txtUserPassword.$touched && frmAddUser.txtUserPassword.$invalid && !frmAddUser.txtUserPassword.$pristine }">
                        <label for="txtUserPassword" class="control-label">Mật Khẩu:</label>
                        <input  type="password"
                                class="form-control" 
                                tabindex="1"
                                id="txtUserPassword" 
                                name="txtUserPassword" 
                                placeholder="Nhập Mật Khẩu Đăng Nhập" 
                                ng-model="model.request.user_password"
                                autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                ng-minlength="6"
                                ng-maxlength="30"
                                maxlength="30"
                                ng-trim="false" 
                                ng-change="model.request.txtUserPassword = model.request.txtUserPassword.split(' ').join('')"
                                required >
                        <span class="help-block" ng-show="frmAddUser.txtUserPassword.$touched && frmAddUser.txtUserPassword.$error.required && frmAddUser.txtUserPassword.$invalid && !frmAddUser.txtUserPassword.$pristine">Vui Lòng Nhập Mật Khẩu.</span>
                        <span class="help-block" ng-show="frmAddUser.txtUserPassword.$touched && frmAddUser.txtUserPassword.$error.minlength">Mật Khẩu Không Được Ngắn Hơn 6 Ký Tự.</span>
                        <span class="help-block" ng-show="frmAddUser.txtUserPassword.$touched && frmAddUser.txtUserPassword.$error.maxlength">Mật Khẩu Không Được Dài Hơn 30 Ký Tự.</span>
                    </div>

                    <div class="form-group" ng-class="{ 'has-error' : (frmAddUser.txtUserPasswordAgain.$touched && frmAddUser.txtUserPasswordAgain.$invalid && !frmAddUser.txtUserPasswordAgain.$pristine) || (frmAddUser.txtUserPasswordAgain.$valid && frmAddUser.txtUserPassword.$valid && model.request.txtUserPassword != model.request.txtUserPasswordAgain)}">
                        <label for="txtUserPasswordAgain" class="control-label">Xác Nhận Mật Khẩu:</label>
                        <input  type="password"
                                class="form-control" 
                                tabindex="2"
                                id="txtUserPasswordAgain" 
                                name="txtUserPasswordAgain" 
                                placeholder="Xác Nhận Mật Khẩu Đăng Nhập" 
                                ng-model="model.request.user_password_confirm"
                                autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                ng-minlength="6"
                                ng-maxlength="30"
                                maxlength="30"
                                ng-trim="false" 
                                ng-change="model.request.txtUserPasswordAgain = model.request.txtUserPasswordAgain.split(' ').join('')"
                                required 
                                focus>
                        <span class="help-block" ng-show="frmAddUser.txtUserPasswordAgain.$touched && frmAddUser.txtUserPasswordAgain.$error.required && frmAddUser.txtUserPasswordAgain.$invalid && !frmAddUser.txtUserPasswordAgain.$pristine">Vui Lòng Xác Nhận Mật Khẩu.</span>
                        <span class="help-block" ng-show="frmAddUser.txtUserPasswordAgain.$touched && frmAddUser.txtUserPasswordAgain.$error.minlength">Mật Khẩu Không Được Ngắn Hơn 6 Ký Tự.</span>
                        <span class="help-block" ng-show="frmAddUser.txtUserPasswordAgain.$touched && frmAddUser.txtUserPasswordAgain.$error.maxlength">Mật Khẩu Không Được Dài Hơn 30 Ký Tự.</span>
                        <span class="help-block" ng-show="frmAddUser.txtUserPasswordAgain.$valid && frmAddUser.txtUserPassword.$valid && model.request.user_password != model.request.user_password_confirm">Mật Khẩu Không Trùng Khớp</span>
                    </div>

                    <div class="form-group row" ng-class="{ 'has-error' : frmAddUser.txtUserFullName.$touched && frmAddUser.txtUserFullName.$invalid && !frmAddUser.txtUserFullName.$pristine }">
                        <label for="txtUserFullName" class="col-sm-12 control-label">Họ Và Tên:</label>
                        <div class="col-sm-12">
                            <input  type="text"
                                    class="form-control" 
                                    id="txtUserFullName" 
                                    name="txtUserFullName" 
                                    placeholder="Nhập Họ Và Tên" 
                                    ng-model="model.request.user_fullname"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                    ng-minlength="2"
                                    ng-maxlength="50"
                                    maxlength="50"
                                    required >
                            <span class="help-block" ng-show="frmAddUser.txtUserFullName.$touched && frmAddUser.txtUserFullName.$error.required && frmAddUser.txtUserFullName.$invalid && !frmAddUser.txtUserFullName.$pristine">Vui Lòng Nhập Họ Và Tên.</span>
                            <span class="help-block" ng-show="frmAddUser.txtUserFullName.$touched && frmAddUser.txtUserFullName.$error.minlength">Họ Và Tên Không Được Ngắn Hơn 2 Ký Tự.</span>
                            <span class="help-block" ng-show="frmAddUser.txtUserFullName.$touched && frmAddUser.txtUserFullName.$error.maxlength">Họ Và Tên Không Được Dài Hơn 50 Ký Tự.</span>
                        </div>
                    </div>

                    

                    <div class="form-group row" ng-class="{ 'has-error' : frmAddUser.txtUserEmail.$touched && frmAddUser.txtUserEmail.$invalid && !frmAddUser.txtUserEmail.$pristine }">
                        <label for="txtUserEmail" class="col-sm-12 control-label">Email:</label>
                        <div class="col-sm-12">
                            <input  type="text"
                                    class="form-control" 
                                    id="txtUserEmail" 
                                    name="txtUserEmail" 
                                    placeholder="Nhập Email Tài Khoản" 
                                    ng-model="model.request.user_email"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                    ng-minlength="3"
                                    ng-maxlength="100"
                                    maxlength="100"
                                    ng-pattern="/^[a-zA-Z0-9!#$%&amp;'*+\/=?^_`{|}~.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*$/"
                                    required >
                            <span class="help-block" ng-show="frmAddUser.txtUserEmail.$touched && frmAddUser.txtUserEmail.$error.required && frmAddUser.txtUserEmail.$invalid && !frmAddUser.txtUserEmail.$pristine">Vui Lòng Nhập Email.</span>
                            <span class="help-block" ng-show="frmAddUser.txtUserEmail.$touched && frmAddUser.txtUserEmail.$error.minlength">Email Không Được Ngắn Hơn 3 Ký Tự.</span>
                            <span class="help-block" ng-show="frmAddUser.txtUserEmail.$touched && frmAddUser.txtUserEmail.$error.maxlength">Email Không Được Dài Hơn 100 Ký Tự.</span>
                            <span class="help-block" ng-show="frmAddUser.txtUserEmail.$touched && frmAddUser.txtUserEmail.$error.pattern">Định Dạng Email Không Chính Xác.</span>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="sblSex" class="col-xs-6 col-sm-6 control-label">Giới Tính:</label>
                        <label for="sblStatus" class="col-xs-6 col-sm-6 control-label">Trạng Thái:</label>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-6 col-sm-6">
                            <input
                                bs-switch
                                name="sblSex"
                                ng-model="model.request.user_sex"
                                type="checkbox"
                                switch-active="true"
                                switch-readonly="false"
                                switch-size="normal"
                                switch-animate="true"
                                switch-label=""
                                switch-icon="icon-save"
                                switch-on-text="Nam"
                                switch-off-text="Nữ"
                                switch-on-color="success"
                                switch-off-color="danger"
                                switch-radio-off="false"
                                switch-label-width="0px"
                                switch-handle-width="auto"
                                switch-inverse="false"
                                switch-change="onChange()"
                                ng-true-value="1"
                                ng-false-value="0">
                        </div>
                        <div class="col-xs-6 col-sm-6">
                            <input
                                bs-switch
                                name="sblStatus"
                                ng-model="model.request.status"
                                type="checkbox"
                                switch-active="true"
                                switch-readonly="false"
                                switch-size="normal"
                                switch-animate="true"
                                switch-label=""
                                switch-icon="icon-save"
                                switch-on-text="Mở"
                                switch-off-text="Khóa"
                                switch-on-color="success"
                                switch-off-color="danger"
                                switch-radio-off="false"
                                switch-label-width="0px"
                                switch-handle-width="auto"
                                switch-inverse="false"
                                switch-change="onChange()"
                                ng-true-value="1"
                                ng-false-value="0">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="lfm1" class="col-sm-12 control-label">Ảnh Đại Diện:</label>    
                        <div class="form-group col-md-12">
                            <div class="input-group col-md-12">
                                <span class="input-group-btn">
                                    <a  id="lfm1" 
                                        name="lfm1" 
                                        data-input="thumbnail" 
                                        data-preview="holder" 
                                        class="btn btn-primary"
                                        style="margin: 0px -2px 0px 0px !important;">
                                        <i class="fa fa-fw fa-picture-o"></i>
                                        <span> Chọn Ảnh</span>
                                    </a>
                                </span>
                                <input  id="thumbnail" 
                                        class="form-control" 
                                        type="text" 
                                        name="filepath" 
                                        placeholder="Chọn Ảnh Đại Diện"
                                        ng-model="model.request.user_avatar" 
                                        autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                                <i  class="fa fa-fw fa-times pull-right" 
                                    style="position: absolute;right: 10px;z-index: 2;top: 10px;" 
                                    ng-click="resetChoiceThumbnail()"></i> 
                            </div>
                        </div> 
                        <div class="form-group col-md-12"> 
                            <img ng-src="[[model.request.user_avatar]]"
                                 onError="this.src='{{asset('public/images/default-image.png')}}';"
                                 id="holder" 
                                 class="img-responsive img-thumbnail" 
                                 alt="Chọn Ảnh Đại Diện" 
                                 title="Chọn Ảnh Đại Diện" 
                                 height="150px">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group text-right" ng-show="model.request.user_id == ''">
                                <button type="button" 
                                        class="btn btn-default"
                                        ng-click="formReset()">
                                    <i class="fa fa-fw fa-ban" aria-hidden="true"></i>
                                    <span class="hidden-xs">Hủy</span>
                                </button>
                                <button type="button" 
                                        ng-click="addItem()"
                                        class="btn btn-primary"
                                        ng-disabled="frmAddUser.txtUserFullName.$invalid
                                                  || frmAddUser.txtUserAccount.$invalid
                                                  || frmAddUser.txtUserEmail.$invalid
                                                  || frmAddUser.txtUserPassword.$invalid
                                                  || frmAddUser.txtBranchId.$invalid
                                                  || frmAddUser.txtUserPasswordAgain.$invalid
                                                  || frmAddUser.txtAuthId.$invalid
                                                  || model.request.user_password != model.request.user_password_confirm">
                                    <i class="fa fa-fw fa-plus" aria-hidden="true"></i>
                                    <span class="hidden-xs">Thêm</span>
                                </button>
                            </div>
                            <div class="form-group" ng-show="model.request.user_id != ''">
                                <button type="button" 
                                        class="btn btn-default pull-left"
                                        ng-click="formReset()">
                                    <i class="fa fa-fw fa-ban" aria-hidden="true"></i>
                                    <span class="hidden-xs">Hủy</span>
                                </button>
                                <button type="button" 
                                        ng-click="updateItem()"
                                        class="btn btn-warning pull-right"
                                        ng-disabled="frmAddUser.txtUserFullName.$invalid
                                                  || frmAddUser.txtUserAccount.$invalid
                                                  || frmAddUser.txtUserEmail.$invalid
                                                  || frmAddUser.txtAuthId.$invalid
                                                  || frmAddUser.txtBranchId.$invalid
                                                  || (frmAddUser.txtUserPasswordAgain.$touched && frmAddUser.txtUserPassword.$touched && model.request.user_password != model.request.user_password_confirm)">
                                    <i class="fa fa-fw fa-pencil-square-o" aria-hidden="true"></i>
                                    <span class="hidden-xs">Cập Nhật</span>
                                </button>
                                <button type="button" 
                                        class="btn btn-danger pull-right"
                                        style="margin-right: 5px;" 
                                        ng-click="removeItem(model.request.user_id)">
                                    <i class="fa fa-fw fa-trash" aria-hidden="true"></i>
                                    <span class="hidden-xs">Xóa</span>
                                </button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <!--/ box body-->
            <!-- Loading (remove the following to stop the loading)-->
            <div class="overlay" id="addReLoading">
                <i class="fa fa-fw fa-refresh fa-spin"></i>
            </div>
            <!-- end loading -->
        </div>
    </div>

    
</div>

@stop