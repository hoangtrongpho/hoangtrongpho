@extends('system.layouts.index')

@section('headTitle')
   <title>{{ Lang::get('messages.system_heade_title') }}</title>
@stop

@section('headCss')
<!-- <link rel="stylesheet" href="{{asset('public/system/css/datetimepicker.css')}}"/> -->
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/daterangepicker/daterangepicker.css')}}"/>
<!-- <link rel="stylesheet" href="{{asset('public/system/plugins/angular-ui-switch/angular-ui-switch.css')}}"/> -->
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-ui-switch/bootstrap-switch.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-multi-select/isteven-multi-select.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-toastr/angular-toastr.css')}}" />

<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/jquery.dataTables.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/dataTables.bootstrap.css')}}" />
<!-- <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/TableTools/css/dataTables.tableTools.css')}}"> -->

@stop

@section('headJs')
    <!-- Date Time Picker -->
    <script type="text/javascript" src="{{asset('public/system/plugins/daterangepicker/daterangepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-daterangepicker/angular-daterangepicker.min.js')}}"></script>

    <script src="{{asset('public/system/plugins/angular-ui-switch/bootstrap-switch.js')}}"></script>
    <script src="{{asset('public/system/plugins/angular-ui-switch/angular-bootstrap-switch.js')}}"></script>

    <!-- iCheck -->
    <script src="{{asset('public/system/plugins/angular_bootstrap_checkbox/angular-bootstrap-checkbox.js')}}"></script>

    <!-- File Manager -->
    <script src="{{asset('/vendor/unisharp/laravel-filemanager/public/js/lfm.js')}}"></script>

    <!-- Angular Multi Select -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-multi-select/isteven-multi-select.js')}}"></script>

    <!-- UI Bootstrap Modal --> 
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-bootstrap/ui-bootstrap-tpls-2.0.0.min.js')}}"></script>

    <!-- Angular Toastr -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-toastr/angular-toastr.tpls.js')}}"></script>

    <!-- Angular Confirm Dialog -->
    <script type="text/javascript" src="{{asset('public/system/plugins/bootbox/bootbox.min.js')}}"></script>

    <!-- Angular Datatables-->
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/angular-datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/colreorder/angular-datatables.colreorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/bootstrap/angular-datatables.bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/tabletools/angular-datatables.tabletools.min.js')}}"></script>


    <script src="{{asset('/public/system/plugins/angular-tinymce/angular-tinymce.js')}}"></script>
    <script src="{{asset('/public/system/plugins/angular-tinymce/langs/vi_VN.js')}}"></script>

    <!-- JS -->
    <script type="text/javascript" src="{{asset('public/system/common/system.common.angurlar.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/common/system.listnewscategories.angurlar.js')}}"></script>

    <script type="text/javascript">

        // function removeItemEvent() {
        //     angular.element($("table[name=listResult]")).scope().removeItem(id);
        // }
        // function copyItemEvent() {
        //     angular.element($("table[name=listResult]")).scope().copyItem(cate);
        // }
        // function openUpdateItemEvent() {
        //     angular.element($("table[name=listResult]")).scope().openUpdateItem(cate);
        // }

        $('#lfm1').filemanager('image');
    </script>

@stop

@section('container')
<div class="row" ng-app="newsCateApp">

    <div class="col-lg-4 col-xs-12" ng-controller="addCateController" style="display: none;">
        <div class="box box-danger">

            <div class="box-header">

                <h3 class="box-title" ng-show="model.request.txtCateId == ''"><i class="fa fa-fw fa-floppy-o" aria-hidden="true"></i> Thêm Mới Thể Loại</h3>

                <h3 class="box-title" ng-show="model.request.txtCateId != ''"><i class="fa fa-fw fa-pencil-square-o" aria-hidden="true"></i> Cập Nhật Thể Loại</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-fw fa-minus"></i></button>
               </div>
            </div>
            <!-- /.box-header -->

            <div class="box-body">
                <form class="form" name="frmAddNewsCate" ng-init="pageInit()" novalidate>

                    <input  type="text"
                            id="txtCateId" 
                            name="txtCateId" 
                            ng-model="model.request.txtCateId"
                            style="display:none;" 
                            >

                    <div class="form-group row" ng-class="{ 'has-error' : frmAddNewsCate.txtCateName.$invalid && !frmAddNewsCate.txtCateName.$pristine }">
                        <label for="txtCateName" class="col-sm-12 control-label">Tên Danh Mục:</label>
                        <div class="col-sm-12">
                            <input  type="text"
                                    class="form-control" 
                                    id="txtCateName" 
                                    name="txtCateName" 
                                    placeholder="Tên Danh Mục" 
                                    ng-model="model.request.txtCateName"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                    ng-minlength="3"
                                    ng-maxlength="50"
                                    required >
                            <span class="help-block" ng-show="frmAddNewsCate.txtCateName.$error.required && frmAddNewsCate.txtCateName.$invalid && !frmAddNewsCate.txtCateName.$pristine">Vui Lòng Nhập Tên Danh Mục.</span>
                            <span class="help-block" ng-show="frmAddNewsCate.txtCateName.$error.minlength">Tên Thể Loại Không Được Ngắn Hơn 3 Ký Tự.</span>
                            <span class="help-block" ng-show="frmAddNewsCate.txtCateName.$error.maxlength">Tên Thể Loại Không Được Dài Hơn 50 Ký Tự.</span>
                        </div>
                    </div>

                    <div class="form-group row" ng-class="{ 'has-error' : frmAddNewsCate.txtDescription.$error.maxlength }">
                        <label for="txtDescription" class="col-sm-12 control-label">Mô Tả Ngắn:</label>
                        <div class="col-sm-12">
                            <input  type="text"
                                    class="form-control" 
                                    id="txtDescription" 
                                    name="txtDescription" 
                                    placeholder="Tên Danh Mục" 
                                    ng-model="model.request.txtDescription"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                    ng-maxlength="100">
                            <span class="help-block" ng-show="frmAddNewsCate.txtDescription.$error.maxlength">Mô Tả Thể Loại Không Được Dài Hơn 100 Ký Tự.</span>
                        </div>
                    </div>

                    <div class="form-group row" style="display: none;">
                        <label for="slbCateParent" class="col-sm-12 control-label">Thể Loại Cha:</label>
                        <div class="col-sm-12">
                            <isteven-multi-select
                                name="slbCateParent"
                                input-model="model.datainit.slbListCategories"
                                output-model="model.request.slbCateParent"
                                helper-elements="reset"
                                button-label="news_cate_name"
                                item-label="news_cate_name"
                                output-properties="news_cate_id"
                                search-property="news_cate_name"
                                tick-property="ticked"
                                orientation="vertical"
                                disable-property="true"
                                is-disabled="false"
                                selection-mode="single"
                                min-search-length="1"
                                translation="model.localLang"
                                max-labels="1"  
                                directive-id="slbCateParent"
                                on-reset="slbClear()"
                                on-item-click="slbItemClick(data)">
                            </isteven-multi-select>
                        </div>
                        <input  type="text"
                                class="form-control" 
                                id="txtCateParent" 
                                name="txtCateParent" 
                                ng-model="model.request.txtCateParent"
                                autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style="display:none;" 
                                required >
                    </div>

                    <div class="form-group row">
                        <label for="sblStatus" class="col-sm-12 control-label">Trạng Thái:</label>
                        <div class="col-sm-12">
                            <input
                                bs-switch
                                name="sblStatus"
                                ng-model="model.request.sblStatus"
                                type="checkbox"
                                switch-active="true"
                                switch-readonly="false"
                                switch-size="normal"
                                switch-animate="true"
                                switch-label=""
                                switch-icon="icon-save"
                                switch-on-text="Hiện"
                                switch-off-text="Ẩn"
                                switch-on-color="success"
                                switch-off-color="danger"
                                switch-radio-off="false"
                                switch-label-width="0px"
                                switch-handle-width="auto"
                                switch-inverse="false"
                                switch-change="onChange()"
                                ng-true-value="1"
                                ng-false-value="0">
                        </div>
                    </div>

                    <div class="form-group row" ng-class="{ 'has-error' : frmAddNewsCate.dtpPublishes.$invalid && !frmAddNewsCate.dtpPublishes.$pristine }">
                        <label for="dtpPublishes" class="col-sm-12 control-label">Ngày Phát Hành: </label>
                        <div class="col-md-12">
                            <input  date-range-picker 
                                    class="form-control date-picker" 
                                    type="text" 
                                    ng-model="model.request.dtpPublishes" 
                                    options="model.datePickerOpts"
                                    clearable="true"
                                    min="'1900-01-01'" 
                                    max="'2025-12-31'" 
                                    id="dtpPublishes"
                                    name="dtpPublishes"
                                    required />
                            <span class="help-block" ng-show="frmAddNewsCate.dtpPublishes.$error.required && frmAddNewsCate.dtpPublishes.$invalid && !frmAddNewsCate.dtpPublishes.$pristine">Vui Lòng Chọn Ngày Phát Hành.</span>
                        </div>
                    </div>

                    <div class="form-group row" style="display: none;">
                        <label for="lfm1" class="col-sm-12 control-label">Hình Ảnh:</label>    
                        <div class="form-group col-md-12">
                            <div class="input-group col-md-12">
                                <span class="input-group-btn">
                                    <a  id="lfm1" 
                                        name="lfm1" 
                                        data-input="thumbnail" 
                                        data-preview="holder" 
                                        class="btn btn-primary">
                                        <i class="fa fa-fw fa-picture-o"></i>
                                        <span>Chọn Ảnh</span>
                                    </a>
                                </span>
                                <input  id="thumbnail" 
                                        class="form-control" 
                                        type="text" 
                                        name="filepath" 
                                        placeholder="Mô Tả Danh Mục"
                                        ng-model="model.request.txtImages" 
                                        autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                            </div>
                        </div> 
                        <div class="form-group col-md-12"> 
                            <img ng-src="[[model.request.txtImages]]"
                                 onError="this.src='{{asset('public/images/default-image.png')}}';"
                                 id="holder" 
                                 class="img-responsive img-thumbnail" 
                                 alt="Chọn Ảnh Đại Diện" 
                                 title="Chọn Ảnh Đại Diện" 
                                 height="150px">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group text-right" ng-show="model.request.txtCateId == ''">
                                <button type="button" 
                                        class="btn btn-warning"
                                        ng-click="formReset()">
                                    <i class="fa fa-fw fa-ban" aria-hidden="true"></i>
                                    <span>Hủy</span>
                                </button>
                                <button type="button" 
                                        ng-click="addItem()"
                                        class="btn btn-primary"
                                        ng-disabled="frmAddNewsCate.txtCateName.$invalid
                                                  || frmAddNewsCate.txtDescription.$invalid
                                                  || frmAddNewsCate.dtpPublishes.$invalid">
                                    <i class="fa fa-fw fa-plus" aria-hidden="true"></i>
                                    <span>Thêm</span>
                                </button>
                            </div>
                            <div class="form-group text-right" ng-show="model.request.txtCateId != ''">
                                <button type="button" 
                                        class="btn btn-warning"
                                        ng-click="formReset()">
                                    <i class="fa fa-fw fa-ban" aria-hidden="true"></i>
                                    <span>Hủy</span>
                                </button>
                                <button type="button" 
                                        class="btn btn-danger" 
                                        ng-click="removeItem(model.request.txtCateId)">
                                    <i class="fa fa-fw fa-trash" aria-hidden="true"></i>
                                    <span>Xóa</span>
                                </button>
                                <button type="button" 
                                        ng-click="updateItem()"
                                        class="btn btn-primary"
                                        ng-disabled="frmAddNewsCate.txtCateName.$invalid
                                                  || frmAddNewsCate.txtDescription.$invalid
                                                  || frmAddNewsCate.dtpPublishes.$invalid">
                                    <i class="fa fa-fw fa-pencil-square-o" aria-hidden="true"></i>
                                    <span>Cập Nhật</span>
                                </button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <!--/ box body-->
            <!-- Loading (remove the following to stop the loading)-->
            <div class="overlay" id="addReLoading">
                <i class="fa fa-fw fa-refresh fa-spin"></i>
            </div>
            <!-- end loading -->
        </div>
    </div>

    <div class="col-md-12 col-xs-12" ng-controller="listCateController" ng-init="pageInit()">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Danh Sách Thể Loại Tin Tức</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-fw fa-minus"></i>
                  </button>
               </div>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
                <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                    <div class="row">
                        <div class="col-sm-12">
                            <table  name="listResult" 
                                    datatable="ng" 
                                    dt-options="dtOptions"
                                    dt-column-defs="dtColumnDefs"
                                    class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <!-- <th class="text-center" data-column-index="0" >Tùy Chọn</th> -->
                                        <th class="text-center" data-column-index="1" >Tên Thể Loại</th>
                                        <!-- <th class="text-center" data-column-index="2" >Trạng Thái</th> -->
                                        <th class="text-center" data-column-index="3" >Phát Hành</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr role="row" ng-repeat="cate in model.datainit.dtgListCategories">
                                        <!-- td class="btn-group btn-group-sm text-center">
                                            <button type="button" ng-click="copyItem(cate)" class="btn btn-primary"><i class="fa fa-fw fa-files-o" aria-hidden="true"></i></button>
                                            <button ng-disabled="cate.status == 99" type="button" ng-click="openUpdateItem(cate)" class="btn btn-warning"><i class="fa fa-fw fa-edit"></i></button>
                                            <button ng-disabled="cate.status == 99" type="button" ng-click="removeItem(cate.news_cate_id)" class="btn btn-danger"><i class="fa fa-fw fa-trash-o"></i></button>
                                        </td> -->
                                        <td>[[cate.news_cate_name]]</td>
                                        <!-- <td class="text-center">
                                            <span style="display:none;">[[cate.status]]</span>
                                            <button ng-show="cate.status == 1" type="button" ng-click="changeStatus(cate.news_cate_id, 0)" class="btn btn-success">Hiện</button>
                                            <button ng-show="cate.status == 0" type="button" ng-click="changeStatus(cate.news_cate_id, 1)" class="btn btn-danger">&nbsp;&nbsp;Ẩn&nbsp;&nbsp;</button>

                                            <button ng-show="cate.status == 99" type="button" ng-click="changeStatus(cate.news_cate_id, 0)" class="btn btn-success" disabled>Hiện</button>
                                        </td> -->
                                        <td class="text-center">[[cate.news_cate_date_public | date :  "dd/MM/yyyy HH:mm"]]</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <!-- <th>Tùy Chọn</th> -->
                                        <th>Tên Thể Loại</th>
                                        <!-- <th>Trạng Thái</th> -->
                                        <th>Phát Hành</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
            <!-- /.box-body -->

            <!-- Loading (remove the following to stop the loading)-->
            <div id="listReLoading" class="overlay">
                <i class="fa fa-fw fa-refresh fa-spin"></i>
            </div>
            <!-- end loading -->

        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
@stop