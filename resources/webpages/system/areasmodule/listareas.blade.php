@extends('system.layouts.index')

@section('headTitle')
   <title>{{ Lang::get('messages.system_heade_title') }}</title>
@stop

@section('pageTitle')
   <h1>
        <i class="fa fa-fw fa-building-o" aria-hidden="true"></i> Quản Trị Khu Vực
        <small>{{ Lang::get('messages.system_verion') }}</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{URL::to('/System/Dashboard')}}"><i class="fa fa-fw fa-dashboard"></i> Hệ Thống Quản Trị Website</a>
        </li>
        <li class="active"><i class="fa fa-fw fa-building" aria-hidden="true"></i> Tỉnh Thành Phố</li>
    </ol>
@stop

@section('headCss')

<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/chosen/chosen.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-toastr/angular-toastr.css')}}" />
<link rel="stylesheet" href="{{asset('public/system/plugins/angular-ui-tree/angular-ui-tree.min.css')}}">
<style type="text/css">

    #tree-root{
        border: 2px solid #555555; 
        padding: 15px;
        margin-top: 15px;
    }

    .angular-ui-tree-nonhandle {
        background: #f8faff;
        border: 1px solid #dae2ea;
        color: #7c9eb2;
        padding: 10px 0px;
        line-height: 25px !important;
        cursor: auto;
        height: 52px;
    }

    .angular-ui-tree-handle {
        background: #f8faff;
        border: 1px solid #dae2ea;
        color: #7c9eb2;
        padding: 10px 10px;
        line-height: 25px !important;
        height: 43px;
    }

    .angular-ui-tree-handle:hover {
        color: #438eb9;
        background: #f4f6f7;
        border-color: #dce2e8;
    }

    .angular-ui-tree-placeholder {
        background: #f0f9ff;
        border: 2px dashed #bed2db;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }

    tr.angular-ui-tree-empty {
        height:100px
    }

    .group-title {
        background-color: #687074 !important;
        color: #FFF !important;
    }


    /* --- Tree --- */
    .tree-node {
        border: 1px solid #dae2ea;
        background: #f8faff;
        color: #7c9eb2;
    }

    .nodrop {
        background-color: #f2dede;
    }

    .tree-node-content {
        margin: 10px;
    }
    .tree-handle {
        padding: 10px;
        background: #428bca;
        color: #FFF;
        margin-right: 10px;
    }

    .angular-ui-tree-handle:hover {
    }

    .angular-ui-tree-placeholder {
        background: #f0f9ff;
        border: 2px dashed #bed2db;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }

    .angular-ui-tree-nonhandle .btn, .angular-ui-tree-handle .btn {
        margin-right: 3px;
        margin-bottom: 3px;
    }

    .angular-ui-tree-node, .angular-ui-tree-placeholder{
        cursor: none;
    }

    .angular-ui-tree-nodes .angular-ui-tree-nodes{
        cursor: auto !important;
    }

    @media (max-width: 767px)
    {
        .angular-ui-tree-nonhandle {
            height: 82px !important;
        }

        #tree-root{
            border: 1px solid #555555; 
            padding: 5px !important;
            margin-top: 5px;
        }
    }


    .chosen-container{
        width: 100% !important;
    }
    .chosen-container-multi .chosen-choices {
        display: block;
        width: 100%;
        min-height: 34px !important;
        height: auto !important;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        border-radius: 0;
        box-shadow: none;
        border-color: #d2d6de;
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075) !important;
        box-shadow: inset 0 1px 1px rgba(0,0,0,.075) !important;
        -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s !important;
        -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s !important;
        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s !important;
    }

    .chosen-container-multi .chosen-choices:hover {

    }

    .chosen-container-multi .chosen-choices li.search-choice{
        margin: 0px 5px 5px 0 !important;
    }
    .chosen-container-multi .chosen-choices li.search-field input[type="text"]{
        height: 20px !important;
        width: 100% !important
    }
    /*.chosen-container .search-field{
        width: 1% !important;
        border: none !important;
    }*/

    .chosen-container-single .chosen-single{
        padding: 6px 0px 0 12px !important;
        height: 34px !important;
        border: 1px solid #d2d6de !important;
        border-radius: 0px !important;
        background: none !important;
        background-clip: none !important;
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075) !important;
        box-shadow: inset 0 1px 1px rgba(0,0,0,.075) !important;
        color: #555 !important;
        font-size: 14px !important;
        line-height: 1.42857143 !important;
    }

    .chosen-container-single .chosen-single div{
        top: 5px !important;
    }
    .chosen-container-single .chosen-single abbr{
        top: 10px !important;
    }
</style>

@stop

@section('headJs')
    <!-- Angular Chosen -->
    <script type="text/javascript" src="{{asset('public/system/plugins/chosen/chosen.jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-chosen/angular-chosen.js')}}"></script>
    <!-- Angular Tree View -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-tree/angular-ui-tree.js')}}"></script>

    <!-- UI Bootstrap Modal --> 
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-bootstrap/ui-bootstrap-tpls-2.0.0.min.js')}}"></script>
    <!-- Angular Toastr -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-toastr/angular-toastr.tpls.js')}}"></script>
    <!-- Angular Confirm Dialog -->
    <script type="text/javascript" src="{{asset('public/system/plugins/bootbox/bootbox.min.js')}}"></script>
    <!-- Angular Datatables-->
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/angular-datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/colreorder/angular-datatables.colreorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/bootstrap/angular-datatables.bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/tabletools/angular-datatables.tabletools.min.js')}}"></script>

    <!-- JS -->
    <script type="text/javascript" src="{{asset('public/system/common/system.listareas.angurlar.js')}}"></script>


@stop

@section('container')
<div class="row" ng-app="areaApp" ng-controller="provincesController" ng-init="pageInit()">

    <div class="col-lg-12 col-xs-12">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-fw fa-building" aria-hidden="true"></i>&nbsp;Danh Sách Tỉnh Thành Phố</h3>
                <div class="box-tools pull-right">
               </div>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
            

                <!-- Nested node template -->
                <script type="text/ng-template" id="nodes_renderer.html">
                  <div ui-tree-handle class="tree-node tree-node-content">
                    <a class="btn btn-success btn-sm" ng-if="item.nodes && item.nodes.length > 0" data-nodrag ng-click="toggle(this)"><span
                        class="glyphicon"
                        ng-class="{
                          'glyphicon-chevron-right': collapsed,
                          'glyphicon-chevron-down': !collapsed
                        }"></span></a>
                    [[item.name]]
                    <a class="pull-right btn btn-danger btn-sm" data-nodrag ng-click="remove(item)"><span
                        class="glyphicon glyphicon-remove"></span></a>
                    <a class="pull-right btn btn-primary btn-sm" data-nodrag ng-click="newSubItem(item)" style="margin-right: 8px;"><span
                        class="glyphicon glyphicon-plus"></span></a>
                  </div>
                  <ol ui-tree-nodes="" ng-model="item.nodes" ng-class="{hidden: collapsed}">
                    <li ng-repeat="item in item.nodes" ui-tree-node ng-include="'nodes_renderer.html'">
                    </li>
                  </ol>
                </script>

                <div class="row">
                    <div class="col-xs-12 col-sm-6" style="margin-bottom: 15px;">
                        <label class="control-label">Có Tổng [[model.datainit.dtgList.length]] Tỉnh Thành</label>
                        <button id="sendMail" type="button" class="btn btn-primary" tabindex="4" data-toggle="modal" data-target="#modalAddProvince">
                            <i class="fa fa-fw fa-plus" aria-hidden="true"></i>
                            <span class="hidden-xs">Thêm Tỉnh Thành Phố</span>
                        </button>
                    </div>
                    <div class="col-xs-12 col-sm-6 text-right">
                        <label class="control-label col-xs-6">Tìm Kiếm: </label>
                        <input  ng-model="query"
                                ng-change="findNodes()"
                                type="text"
                                class="form-control col-xs-6" 
                                id="txtSearchAreas" 
                                name="txtSearchAreas" 
                                style="width: 50% !important;">
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">

                        <div ui-tree id="tree-root" 
                             data-nodrop-enabled="true" 
                             data-max-depth="3">
                          <ol ui-tree-nodes data-nodrop-enabled="true" ng-model="model.datainit.dtgList">
                            <li ng-repeat="item in model.datainit.dtgList" 
                                ng-show="visible(item)"
                                ui-tree-node
                                onmousedown="event.preventDefault ? event.preventDefault() : event.returnValue = false">

                                <div class="tree-node tree-node-content ng-scope angular-ui-tree-nonhandle">
                                    <div class="col-xs-12 col-sm-6">
                                        <a class="btn btn-success btn-sm" ng-if="item.nodes && item.nodes.length > 0" data-nodrag ng-click="toggle(this)">
                                            <span class="glyphicon"
                                                  ng-class="{
                                                    'glyphicon-chevron-right': collapsed,
                                                    'glyphicon-chevron-down': !collapsed
                                                  }"></span>
                                        </a>
                                        <span style="overflow: hidden;white-space: nowrap;text-overflow: ellipsis;width: 150px">[[item.name]] ([[item.nodes.length]])</span>

                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <button type="button" ng-click="removeItem(item)" data-nodrag class="pull-right btn btn-danger btn-sm"><i class="fa fa-fw fa-trash-o"></i></button>
                                        <button type="button" ng-click="openUpdateItem(item)" data-nodrag class="pull-right btn btn-warning btn-sm"><i class="fa fa-fw fa-edit"></i></button>
                                        <button type="button" ng-click="openAddItem(item)" data-nodrag class="pull-right btn btn-primary btn-sm"><i class="fa fa-fw fa-plus"></i></button>
                                    </div>
                                </div>
                                <ol ui-tree-nodes data-nodrop-enabled="true" ng-model="item.nodes" ng-class="{hidden: collapsed}">
                                    <li ng-repeat="item in item.nodes" 
                                        ui-tree-node
                                        onmousedown="event.preventDefault ? event.preventDefault() : event.returnValue = false">

                                        <div class="tree-node tree-node-content ng-scope angular-ui-tree-nonhandle">
                                            <div class="col-xs-12 col-sm-6">
                                                <a class="btn btn-success btn-sm" ng-if="item.nodes && item.nodes.length > 0" data-nodrag ng-click="toggle(this)">
                                                    <span class="glyphicon"
                                                          ng-class="{
                                                            'glyphicon-chevron-right': collapsed,
                                                            'glyphicon-chevron-down': !collapsed
                                                          }"></span>
                                                </a>
                                                <span style="overflow: hidden;white-space: nowrap;text-overflow: ellipsis;width: 150px;">[[item.name]] ([[item.nodes.length]])</span>

                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <button type="button" ng-click="removeItem(item)" data-nodrag class="pull-right btn btn-danger btn-sm"><i class="fa fa-fw fa-trash-o"></i></button>
                                                <button type="button" ng-click="openUpdateItem(item)" data-nodrag class="pull-right btn btn-warning btn-sm"><i class="fa fa-fw fa-edit"></i></button>
                                                <button type="button" ng-click="openAddItem(item)" data-nodrag class="pull-right btn btn-primary btn-sm"><i class="fa fa-fw fa-plus"></i></button>
                                            </div>
                                        </div>
                                        <ol ui-tree-nodes data-nodrop-enabled="true" ng-model="item.nodes" ng-class="{hidden: collapsed}">
                                            <li ng-repeat="item in item.nodes" 
                                                ui-tree-node
                                                onmousedown="event.preventDefault ? event.preventDefault() : event.returnValue = false">

                                                <div class="tree-node tree-node-content ng-scope angular-ui-tree-nonhandle">
                                                    <div class="col-xs-12 col-sm-6">
                                                        <a class="btn btn-success btn-sm" ng-if="item.nodes && item.nodes.length > 0" data-nodrag ng-click="toggle(this)">
                                                            <span class="glyphicon"
                                                                  ng-class="{
                                                                    'glyphicon-chevron-right': collapsed,
                                                                    'glyphicon-chevron-down': !collapsed
                                                                  }"></span>
                                                        </a>
                                                        <span style="overflow: hidden;white-space: nowrap;text-overflow: ellipsis;width: 150px">[[item.name]]</span>

                                                    </div>
                                                    <div class="col-xs-12 col-sm-6">
                                                        <button type="button" ng-click="removeItem(item)" data-nodrag class="pull-right btn btn-danger btn-sm"><i class="fa fa-fw fa-trash-o"></i></button>
                                                        <button type="button" ng-click="openUpdateItem(item)" data-nodrag class="pull-right btn btn-warning btn-sm"><i class="fa fa-fw fa-edit"></i></button>
                                                    </div>
                                                </div>
                        
                                            </li>
                                        </ol>

                                    </li>
                                </ol>

                            </li>
                          </ol>
                        </div>

                    </div>
                </div>

                <!-- <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                    <div class="row">
                        <div class="col-sm-12">
                            <table  name="listResult" 
                                    datatable="ng" 
                                    dt-options="dtOptions"
                                    dt-column-defs="dtColumnDefs"
                                    width="100%" 
                                    class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center" style="width:15%;" data-column-index="0" >Tùy Chọn</th>
                                        <th class="text-center" style="width:45%;" data-column-index="1" >Tỉnh Thành Phố</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr role="row" ng-repeat="item in model.datainit.dtgList">
                                        <td class="text-center">
                                            <button type="button" ng-click="openUpdateItem(item)" class="btn btn-warning"><i class="fa fa-fw fa-edit"></i></button>
                                            <button type="button" ng-click="removeItem(item.province_id)" class="btn btn-danger"><i class="fa fa-fw fa-trash-o"></i></button>
                                        </td>
                                        <td class="text-center">[[item.province_name]]</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div> -->
            <!-- /.box-body -->

            <!-- Loading (remove the following to stop the loading)-->
            <div id="listReLoading" class="overlay">
                <i class="fa fa-fw fa-refresh fa-spin"></i>
            </div>
            <!-- end loading -->

        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->



    <!-- Modal -->
    <div id="modalAddProvince" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <form class="form" class="form-inline" name="frmAddProvince" id="frmAddProvince" novalidate>
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-blue">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-fw fa-floppy-o" aria-hidden="true"></i>
                    <span class="hidden-xs">Thêm Tỉnh Thành Phố</span></h4>
                </div>
                <div class="modal-body">

                    <div class="col-md-12">
                        <div class="form-group" ng-class="{ 'has-error' : frmAddProvince.txtProvinceName.$touched && frmAddProvince.txtProvinceName.$invalid && !frmAddProvince.txtProvinceName.$pristine }">
                            <label for="txtProvinceName" class="control-label">Tỉnh Thành Phố:</label>
                            <input  tabindex="1"
                                    type="text"
                                    class="form-control" 
                                    id="txtProvinceName" 
                                    name="txtProvinceName" 
                                    placeholder="Nhập Tỉnh Thành Phố" 
                                    ng-model="model.request.txtProvinceName"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                    ng-minlength="2"
                                    ng-maxlength="100"
                                    maxlength="100"
                                    required >
                            <span class="help-block" ng-show="frmAddProvince.txtProvinceName.$touched && frmAddProvince.txtProvinceName.$error.required && frmAddProvince.txtProvinceName.$invalid && !frmAddProvince.txtProvinceName.$pristine">Vui Lòng Nhập Tỉnh Thành Phố.</span>
                            <span class="help-block" ng-show="frmAddProvince.txtProvinceName.$touched && frmAddProvince.txtProvinceName.$error.minlength">Tỉnh Thành Phố Không Được Ngắn Hơn 2 Ký Tự.</span>
                            <span class="help-block" ng-show="frmAddProvince.txtProvinceName.$touched && frmAddProvince.txtProvinceName.$error.maxlength">Tỉnh Thành Phố Không Được Dài Hơn 100 Ký Tự.</span>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <i class="fa fa-fw fa-ban" aria-hidden="true"></i>
                        <span>Đóng</span>
                    </button>
                    <button ng-disabled="frmAddProvince.$invalid" 
                            ng-click="addItemProvince()"
                            type="submit" class="btn btn-primary" id="btnAdd" name="btnAdd">
                        <i class="fa fa-fw fa-plus" aria-hidden="true"></i>
                        <span>Thêm Mới</span>
                    </button>
                </div>
            </div>
        </form>
      </div>
    </div>

    <!-- Modal -->
    <div id="modalAddDistrict" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <form class="form" class="form-inline" name="frmAddDistrict" id="frmAddDistrict" novalidate>
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-blue">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-fw fa-floppy-o" aria-hidden="true"></i>
                    <span class="hidden-xs">Thêm Quận Huyện</span></h4>
                </div>
                <div class="modal-body">

                    <div class="col-md-12">
                        <div class="form-group" ng-class="{ 'has-error' : frmAddDistrict.txtDistrictName.$touched && frmAddDistrict.txtDistrictName.$invalid && !frmAddDistrict.txtDistrictName.$pristine }">
                            <label for="txtDistrictName" class="control-label">Quận Huyện:</label>
                            <input  tabindex="1"
                                    type="text"
                                    class="form-control" 
                                    id="txtDistrictName" 
                                    name="txtDistrictName" 
                                    placeholder="Nhập Quận Huyện" 
                                    ng-model="model.request.txtDistrictName"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                    ng-minlength="2"
                                    ng-maxlength="100"
                                    maxlength="100"
                                    required >
                            <span class="help-block" ng-show="frmAddDistrict.txtDistrictName.$touched && frmAddDistrict.txtDistrictName.$error.required && frmAddDistrict.txtDistrictName.$invalid && !frmAddDistrict.txtDistrictName.$pristine">Vui Lòng Nhập Quận Huyện.</span>
                            <span class="help-block" ng-show="frmAddDistrict.txtDistrictName.$touched && frmAddDistrict.txtDistrictName.$error.minlength">Quận Huyện Không Được Ngắn Hơn 2 Ký Tự.</span>
                            <span class="help-block" ng-show="frmAddDistrict.txtDistrictName.$touched && frmAddDistrict.txtDistrictName.$error.maxlength">Quận Huyện Không Được Dài Hơn 100 Ký Tự.</span>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <i class="fa fa-fw fa-ban" aria-hidden="true"></i>
                        <span>Đóng</span>
                    </button>
                    <button ng-disabled="frmAddDistrict.$invalid" 
                            ng-click="addItemDistrict()"
                            type="submit" class="btn btn-primary" id="btnAdd" name="btnAdd">
                        <i class="fa fa-fw fa-plus" aria-hidden="true"></i>
                        <span>Thêm Mới</span>
                    </button>
                </div>
            </div>
        </form>
      </div>
    </div>

    <!-- Modal -->
    <div id="modalAddWard" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <form class="form" class="form-inline" name="frmAddWard" id="frmAddWard" novalidate>
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-blue">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-fw fa-floppy-o" aria-hidden="true"></i>
                    <span class="hidden-xs">Thêm Phường Xã</span></h4>
                </div>
                <div class="modal-body">

                    <div class="col-md-12">
                        <div class="form-group" ng-class="{ 'has-error' : frmAddWard.txtWardName.$touched && frmAddWard.txtWardName.$invalid && !frmAddWard.txtWardName.$pristine }">
                            <label for="txtWardName" class="control-label">Phường Xã:</label>
                            <input  tabindex="1"
                                    type="text"
                                    class="form-control" 
                                    id="txtWardName" 
                                    name="txtWardName" 
                                    placeholder="Nhập Phường Xã" 
                                    ng-model="model.request.txtWardName"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                    ng-minlength="2"
                                    ng-maxlength="100"
                                    maxlength="100"
                                    required >
                            <span class="help-block" ng-show="frmAddWard.txtWardName.$touched && frmAddWard.txtWardName.$error.required && frmAddWard.txtWardName.$invalid && !frmAddWard.txtWardName.$pristine">Vui Lòng Nhập Quận Huyện.</span>
                            <span class="help-block" ng-show="frmAddWard.txtWardName.$touched && frmAddWard.txtWardName.$error.minlength">Phường Xã Không Được Ngắn Hơn 2 Ký Tự.</span>
                            <span class="help-block" ng-show="frmAddWard.txtWardName.$touched && frmAddWard.txtWardName.$error.maxlength">Phường Xã Không Được Dài Hơn 100 Ký Tự.</span>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <i class="fa fa-fw fa-ban" aria-hidden="true"></i>
                        <span>Đóng</span>
                    </button>
                    <button ng-disabled="frmAddWard.$invalid" 
                            ng-click="addItemWard()"
                            type="submit" class="btn btn-primary" id="btnAdd" name="btnAdd">
                        <i class="fa fa-fw fa-plus" aria-hidden="true"></i>
                        <span>Thêm Mới</span>
                    </button>
                </div>
            </div>
        </form>
      </div>
    </div>

    <!-- Modal -->
    <div id="modalUpdateProvince" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <form class="form" class="form-inline" name="frmUpdateProvince" id="frmUpdateProvince" novalidate>
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-yellow">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-fw fa-pencil-square-o" aria-hidden="true"></i> Cập Nhật Tỉnh Thành Phố</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="txtProvinceName" class="control-label">Khu Vực Cha: [[model.request.slbProvinceParent]]</label>
                            <select chosen
                                    ng-model="model.request.slbProvinceParent"
                                    ng-options="area.area_id as area.area_name for area in model.datainit.dtgListUpdate"
                                    no-results-text="'Không có khu vực nào phù hơp'"
                                    placeholder-text-multiple="'Chọn Khu Vực Cha'"
                                    data-placeholder="'Chọn Khu Vực Cha'"
                                    disable-search="false"
                                    allow-single-deselect="true"
                                    max-selected-options="1"
                                    display-disabled-options="true"
                                    display-selected-options="true"
                                    include-group-label-in-selected="true"
                                    max-shown-results="10">
                            </select>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group" ng-class="{ 'has-error' : frmUpdateProvince.txtProvinceName.$touched && frmUpdateProvince.txtProvinceName.$invalid && !frmUpdateProvince.txtProvinceName.$pristine }">
                            <label for="txtProvinceName" class="control-label">Tỉnh Thành Phố:</label>
                            <input  tabindex="1"
                                    type="text"
                                    class="form-control" 
                                    id="txtProvinceName" 
                                    name="txtProvinceName" 
                                    placeholder="Tỉnh Thành Phố" 
                                    ng-model="model.request.txtProvinceName"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                    ng-minlength="2"
                                    ng-maxlength="50"
                                    maxlength="50"
                                    required >
                            <span class="help-block" ng-show="frmUpdateProvince.txtProvinceName.$touched && frmUpdateProvince.txtProvinceName.$error.required && frmUpdateProvince.txtProvinceName.$invalid && !frmUpdateProvince.txtName.$pristine">Vui Lòng Nhập Tỉnh Thành Phố.</span>
                            <span class="help-block" ng-show="frmUpdateProvince.txtProvinceName.$touched && frmUpdateProvince.txtProvinceName.$error.minlength">Tỉnh Thành Phố Không Được Ngắn Hơn 2 Ký Tự.</span>
                            <span class="help-block" ng-show="frmUpdateProvince.txtProvinceName.$touched && frmUpdateProvince.txtProvinceName.$error.maxlength">Tỉnh Thành Phố Không Được Dài Hơn 50 Ký Tự.</span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <i class="fa fa-fw fa-ban" aria-hidden="true"></i>&nbsp;Đóng
                    </button>
                    <button ng-disabled="frmUpdateProvince.$invalid" 
                            ng-click="updateItemProvice()"
                            type="submit" class="btn btn-warning" id="btnUpdate" name="btnUpdate">
                        <i class="fa fa-fw fa-pencil-square-o" aria-hidden="true"></i>&nbsp;Cập Nhật
                    </button>
                </div>
            </div>
        </form>
      </div>
    </div>

    <!-- Modal -->
    <div id="modalUpdateDistrict" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <form class="form" class="form-inline" name="frmUpdateDistrict" id="frmUpdateDistrict" novalidate>
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-yellow">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-fw fa-pencil-square-o" aria-hidden="true"></i> Cập Nhật Quận Huyện</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="txtDistrictName" class="control-label">Khu Vực Cha: [[model.request.slbDistrictParent]]</label>
                            <select chosen
                                    ng-model="model.request.slbDistrictParent"
                                    ng-options="area.area_id as area.area_name for area in model.datainit.dtgListUpdate"
                                    no-results-text="'Không có khu vực nào phù hơp'"
                                    placeholder-text-multiple="'Chọn Khu Vực Cha'"
                                    data-placeholder="'Chọn Khu Vực Cha'"
                                    disable-search="false"
                                    allow-single-deselect="true"
                                    max-selected-options="1"
                                    display-disabled-options="true"
                                    display-selected-options="true"
                                    include-group-label-in-selected="true"
                                    max-shown-results="10">
                            </select>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group" ng-class="{ 'has-error' : frmUpdateDistrict.txtDistrictName.$touched && frmUpdateDistrict.txtDistrictName.$invalid && !frmUpdateDistrict.txtDistrictName.$pristine }">
                            <label for="txtDistrictName" class="control-label">Quận Huyện:</label>
                            <input  tabindex="1"
                                    type="text"
                                    class="form-control" 
                                    id="txtDistrictName" 
                                    name="txtDistrictName" 
                                    placeholder="Quận Huyện" 
                                    ng-model="model.request.txtDistrictName"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                    ng-minlength="2"
                                    ng-maxlength="50"
                                    maxlength="50"
                                    required >
                            <span class="help-block" ng-show="frmUpdateDistrict.txtDistrictName.$touched && frmUpdateDistrict.txtDistrictName.$error.required && frmUpdateDistrict.txtDistrictName.$invalid && !frmUpdateDistrict.txtName.$pristine">Vui Lòng Quận Huyện.</span>
                            <span class="help-block" ng-show="frmUpdateDistrict.txtDistrictName.$touched && frmUpdateDistrict.txtDistrictName.$error.minlength">Quận Huyện Không Được Ngắn Hơn 2 Ký Tự.</span>
                            <span class="help-block" ng-show="frmUpdateDistrict.txtDistrictName.$touched && frmUpdateDistrict.txtDistrictName.$error.maxlength">Quận Huyện Không Được Dài Hơn 50 Ký Tự.</span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <i class="fa fa-fw fa-ban" aria-hidden="true"></i>&nbsp;Đóng
                    </button>
                    <button ng-disabled="frmUpdateDistrict.$invalid" 
                            ng-click="updateItemDistrict()"
                            type="submit" class="btn btn-warning" id="btnUpdate" name="btnUpdate">
                        <i class="fa fa-fw fa-pencil-square-o" aria-hidden="true"></i>&nbsp;Cập Nhật
                    </button>
                </div>
            </div>
        </form>
      </div>
    </div>

    <!-- Modal -->
    <div id="modalUpdateWard" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <form class="form" class="form-inline" name="frmUpdateWard" id="frmUpdateWard" novalidate>
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-yellow">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-fw fa-pencil-square-o" aria-hidden="true"></i> Cập Nhật Phường Xã</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="slbWardParent" class="control-label">Khu Vực Cha:</label>
                            <select chosen
                                    ng-model="model.request.slbWardParent"
                                    ng-options="area.area_id as area.area_name for area in model.datainit.dtgListUpdate"
                                    no-results-text="'Không có khu vực nào phù hơp'"
                                    placeholder-text-multiple="'Chọn Khu Vực Cha'"
                                    data-placeholder="'Chọn Khu Vực Cha'"
                                    disable-search="false"
                                    allow-single-deselect="true"
                                    max-selected-options="1"
                                    display-disabled-options="true"
                                    display-selected-options="true"
                                    include-group-label-in-selected="true"
                                    max-shown-results="10">
                            </select>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group" ng-class="{ 'has-error' : frmUpdateWard.txtWardName.$touched && frmUpdateWard.txtWardName.$invalid && !frmUpdateWard.txtWardName.$pristine }">
                            <label for="txtWardName" class="control-label">Phường Xã:</label>
                            <input  tabindex="1"
                                    type="text"
                                    class="form-control" 
                                    id="txtWardName" 
                                    name="txtWardName" 
                                    placeholder="Tỉnh Thành Phố" 
                                    ng-model="model.request.txtWardName"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                    ng-minlength="2"
                                    ng-maxlength="50"
                                    maxlength="50"
                                    required >
                            <span class="help-block" ng-show="frmUpdateWard.txtWardName.$touched && frmUpdateWard.txtWardName.$error.required && frmUpdateWard.txtWardName.$invalid && !frmUpdateWard.txtName.$pristine">Vui Lòng Nhập Phường Xã.</span>
                            <span class="help-block" ng-show="frmUpdateWard.txtWardName.$touched && frmUpdateWard.txtWardName.$error.minlength">Phường Xã Không Được Ngắn Hơn 2 Ký Tự.</span>
                            <span class="help-block" ng-show="frmUpdateWard.txtWardName.$touched && frmUpdateWard.txtWardName.$error.maxlength">Phường Xã Không Được Dài Hơn 50 Ký Tự.</span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default"
                     data-dismiss="modal">
                        <i class="fa fa-fw fa-ban" aria-hidden="true"></i>&nbsp;Đóng
                    </button>
                    <button ng-disabled="frmUpdateWard.$invalid" 
                            ng-click="updateItemWard()"
                            type="submit" class="btn btn-warning" id="btnUpdate" name="btnUpdate">
                        <i class="fa fa-fw fa-pencil-square-o" aria-hidden="true"></i>&nbsp;Cập Nhật
                    </button>
                </div>
            </div>
        </form>
      </div>
    </div>


</div>
@stop