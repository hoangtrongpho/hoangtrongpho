@extends('system.layouts.index')

@section('headTitle')
   <title>{{ Lang::get('messages.system_heade_title') }}</title>
@stop

@section('pageTitle')
   <h1>
        <i class="fa fa-fw fa-archive" aria-hidden="true"></i> Quản Trị Khách Hàng
        <small>Skyfire Sytem Version 1.0</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{URL::to('/System/Dashboard')}}"><i class="fa fa-fw fa-dashboard"></i> Hệ Thống Quản Trị Website</a>
        </li>
        <li class="active"><i class="fa fa-fw fa-archive" aria-hidden="true"></i> Danh Sách Khách Hàng</li>
    </ol>
@stop

@section('headCss')
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/daterangepicker/daterangepicker.css')}}"/>
    <link rel="stylesheet" href="{{asset('public/system/plugins/angular-ui-switch/angular-ui-switch.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-ui-switch/bootstrap-switch.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-multi-select/isteven-multi-select.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-toastr/angular-toastr.css')}}" />

    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/jquery.dataTables.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/dataTables.bootstrap.css')}}" />

    <style type="text/css">
        .control-label{
            padding-top: 5px;
            padding-right: 5px !important;
            padding-left: 5px !important;
            float: left;
        }

        #listResult {
            width: 100% !important;
        }
    </style>
@stop

@section('headJs')
    <!-- Date Time Picker -->
    <script type="text/javascript" src="{{asset('public/system/plugins/daterangepicker/daterangepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-daterangepicker/angular-daterangepicker.min.js')}}"></script>

    <script src="{{asset('public/system/plugins/angular-ui-switch/angular-ui-switch.js')}}"></script>
    <script src="{{asset('public/system/plugins/angular-ui-switch/bootstrap-switch.js')}}"></script>
    <script src="{{asset('public/system/plugins/angular-ui-switch/angular-bootstrap-switch.js')}}"></script>

    <!-- iCheck -->
    <script src="{{asset('public/system/plugins/angular_bootstrap_checkbox/angular-bootstrap-checkbox.js')}}"></script>

    <!-- File Manager -->
    <!-- Angular Multi Select -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-multi-select/isteven-multi-select.js')}}"></script>

    <!-- UI Bootstrap Modal --> 
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-bootstrap/ui-bootstrap-tpls-2.0.0.min.js')}}"></script>

    <!-- Angular Toastr -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-toastr/angular-toastr.tpls.js')}}"></script>

    <!-- Angular Confirm Dialog -->
    <script type="text/javascript" src="{{asset('public/system/plugins/bootbox/bootbox.min.js')}}"></script>

    <!-- Angular Datatables-->
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/angular-datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/colreorder/angular-datatables.colreorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/bootstrap/angular-datatables.bootstrap.min.js')}}"></script>

    <!-- JS -->
    <script type="text/javascript" src="{{asset('public/system/common/system.common.angurlar.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/common/system.listcustomers.angurlar.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function() { 
            $(window).on('load', function() {  
            });


            //Minify Left Menu
            //$("body").addClass("sidebar-collapse");
        });
    </script>
@stop

@section('container')
<div class="row" ng-app="newsNewsApp" ng-controller="newsNewsController" ng-init="pageInit()">
    <div class="col-lg-12">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-fw fa-list" aria-hidden="true"></i> Danh Sách Khách Hàng</h3>
                <!-- buton create -->
                <div class="box-tools pull-right">
                    <a style="display: none;" class="btn btn-primary" href="{{URL::to('System/CreateCustomer')}}" title="Thêm Mới Khách Hàng">
                        <i class="fa fa-fw fa-plus" aria-hidden="true"></i>
                        <span>Thêm Mới</span>
                    </a>
                    @if(Session::has('userAuth'))
                        <?php
                           $auth_id = Session::get('userAuth')->auth_id;
                        ?> 
                    @else
                        <?php
                           $auth_id = 0;
                        ?> 
                    @endif 
                </div>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <table  name="listResult" 
                                id="listResult" 
                                style="width: 100%;" 
                                datatable="ng" 
                                dt-options="dtOptions"
                                dt-column-defs="dtColumnDefs"
                                class="table table-bordered table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th class="text-center" data-column-index="0" style="width: 100px;">Tùy Chọn</th>
                                    <th class="text-center" data-column-index="3" style="width: auto;">Hình Ảnh</th>
                                    <th class="text-center" data-column-index="3" style="width: auto;">Họ Tên</th>
                                    <th class="text-center" data-column-index="3" style="width: auto;">Tài Khoản</th>
                                    <th class="text-center" data-column-index="4" style="width: auto;"> SDT</th>
                                    <th class="text-center" data-column-index="6" style="width: auto;">Email</th>
                                    <th class="text-center" data-column-index="6" style="width: auto;">CMND</th>
                                    <th class="text-center" data-column-index="6" style="width: auto;">Tg Đăng Ký</th>
                                    <th class="text-center" data-column-index="7" style="width: auto;">Trạng Thái</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr role="row" ng-repeat="cu in model.datainit.dtgListCustomer">
                                    <td class="text-center">
                                        <a href="{{URL::to('System/UpdateCustomer/[[cu.customer_id]]')}}" class="btn btn-warning btn-xs" title="sửa"><i class="fa fa-fw fa-edit fa-fw" aria-hidden="true"></i></a>

                                        <button title="xóa" type="button" 
                                        ng-click="removeItem(cu.customer_id)" 
                                        ng-show="{{$auth_id}}==1"
                                        class="btn btn-danger btn-xs"><i class="fa fa-fw fa-trash-o fa-fw"></i></button>
                                        <!-- active =1/2 -->
                                        <button ng-show="cu.status==1 || cu.status==3" title="active" type="button"  ng-click="changeStatus(cu.customer_id, 2)"  class="btn btn-success btn-xs"><i class="fa fa-fw fa-key fa-fw"></i></button>
                                        <!-- unactive =3 -->
                                        <button ng-show="cu.status==1 || cu.status==2" title="Lock" type="button" ng-click="changeStatus(cu.customer_id, 3)" class="btn btn-primary btn-xs"><i class="fa fa-fw fa-lock fa-fw"></i></button>
                                    </td>
                                    <td>
                                        <span ng-show="cu.customer_avatar=='' || cu.customer_avatar==null">Chưa nhập hình</span>
                                        <span ng-show="cu.customer_avatar!='' && cu.customer_avatar!=null"><img ng-src="[[cu.customer_avatar]]" src="" style="width: 100px;" title="[[cu.customer_avatar]]" alt="[[cu.customer_avatar]]"></span>
                                        
                                    </td>
                                    <td>[[cu.customer_fullname]]</td>
                                    <td>[[cu.customer_account]]</td>
                                    <td>[[cu.customer_phone]]</td>
                                    <td>[[cu.customer_email]]</td>
                                    <td>[[cu.customer_identify]]</td>
                                    <td style="text-align: center;">[[cu.customer_datediff]] Ngày</td>
                                    <td>
                                        <span ng-show="cu.status==1" class="label label-warning"><i class="fa fa-user-circle" aria-hidden="true"></i>&nbsp; Mới Đăng Ký</span>
                                        <span ng-show="cu.status==2" class="label label-success"><i class="fa fa-key" aria-hidden="true"></i>&nbsp; Đã Mở</span>
                                        <span ng-show="cu.status==3" class="label label-primary"><i class="fa fa-lock" aria-hidden="true"></i>&nbsp; Đã Khóa</span>
                                       
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <!-- /.box-body -->

            <!-- Loading (remove the following to stop the loading)-->
            <div id="listReLoading" class="overlay">
                <i class="fa fa-fw fa-refresh fa-spin"></i>
            </div>
            <!-- end loading -->

        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
@stop