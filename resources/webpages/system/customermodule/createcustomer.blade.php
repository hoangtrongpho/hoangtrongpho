@extends('system.layouts.index')

@section('headTitle')
   <title>{{ Lang::get('messages.system_heade_title') }}</title>
@stop

@section('pageTitle')
   <h1>
        <i class="fa fa-fw fa-archive" aria-hidden="true"></i> Thêm Khách Hàng
        <small>{{ Lang::get('messages.system_verion') }}</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{URL::to('/System/Dashboard')}}"><i class="fa fa-fw fa-dashboard"></i> Hệ Thống Quản Trị Website</a>
        </li>
        <li class="active"><i class="fa fa-fw fa-archive" aria-hidden="true"></i> Thêm Khách Hàng</li>
    </ol>
@stop

@section('headCss')
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/daterangepicker/daterangepicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-ui-switch/bootstrap-switch.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-multi-select/isteven-multi-select.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-toastr/angular-toastr.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/iCheck/flat/blue.css')}}">

    <style type="text/css">
        .has-error>.mce-tinymce{
            border-color: #dd4b39 !important;
            box-shadow: none !important;
        }
        .has-error>.input-group, .has-error>.input-group>.input-group-addon{
            border-color: #dd4b39 !important;
            box-shadow: none !important;
        }

        .has-error button, .has-error button:active{
            border-color: #dd4b39 !important;
        }


     /*   .multiSelect .checkboxLayer{
            bottom: 35px !important;
        }
        .multiSelect .buttonLabel, .multiSelect .buttonLabel{
            width: auto !important;
        }
        .fa-times{
            right: 5px !important;
        }*/
    </style>
@stop

@section('headJs')
    <!-- Date Time Picker -->
    <script type="text/javascript" src="{{asset('public/system/plugins/daterangepicker/daterangepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-daterangepicker/angular-daterangepicker.min.js')}}"></script>

    <script src="{{asset('public/system/plugins/angular-ui-switch/bootstrap-switch.js')}}"></script>
    <script src="{{asset('public/system/plugins/angular-ui-switch/angular-bootstrap-switch.js')}}"></script>

    <!-- iCheck -->
    <script type="text/javascript" src="{{asset('public/system/plugins/iCheck/icheck.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-icheck/angular-icheck.js')}}"></script>

    <!-- File Manager -->
    <script src="{{asset('vendor/unisharp/laravel-filemanager/public/js/lfm.js')}}"></script>

    <!-- Angular Multi Select -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-multi-select/isteven-multi-select.js')}}"></script>

    <!-- UI Bootstrap Modal --> 
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-bootstrap/ui-bootstrap-tpls-2.0.0.min.js')}}"></script>

    <!-- Angular Toastr -->
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-toastr/angular-toastr.tpls.js')}}"></script>

    <!-- Angular Confirm Dialog -->
    <script type="text/javascript" src="{{asset('public/system/plugins/bootbox/bootbox.min.js')}}"></script>

    <script src="{{asset('/public/system/plugins/tinymce/tinymce.min.js')}}"></script>
    <script src="{{asset('/public/system/plugins/angular-tinymce/angular-tinymce.js')}}"></script>
    <script src="{{asset('/public/system/plugins/angular-tinymce/langs/vi_VN.js')}}"></script>

    <!-- JS -->
    <script type="text/javascript" src="{{asset('public/system/common/system.common.angurlar.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/common/system.createcustomer.angurlar.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function() { 
            $(window).on('load', function() {  
                $("#addReLoading").hide();
            });
            //Laravel File Manager
            $('#selectImages').filemanager('images');
        });
        $( "#dialog-modal" ).on( "dialogopen", function( event, ui ) {
            console.log('Wayhay!!');
            window.open("http://www.google.com");
        } );
    </script>
@stop

@section('container')
<div class="row" ng-app="createCustomerApp" ng-controller="createCustomerController" ng-init="pageInit()">
    <form class="form" class="form-vertical" name="frmAdd" novalidate>
        <!--==================== Thông Tin chung ====================-->
        <div class="col-lg-8 col-xs-8">
            <div class="box box-danger">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-fw fa-newspaper-o" aria-hidden="true"></i> Chi Tiết Khách Hàng</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-fw fa-minus"></i></button>
                   </div>
                </div>
                <div class="box-body">
                    <!-- ========== =======UPDATE===================-->
                    <div class="form-group" ng-show="model.datainit.pageStatus == 'update'">
                        <!-- ================= Trở Về ==============-->
                        <a class="btn btn-default" href="{{URL::to('System/ListCustomers')}}" title="CreateNews">
                            <i class="fa fa-fw fa-arrow-left" aria-hidden="true"></i>
                            <span class="hidden-xs">Trở Về</span>
                        </a>
                        <div class="pull-right">&nbsp;</div>
                         <!-- ================= Cap Nhat ==============-->
                        <button type="button" 
                                tabindex="99"
                                ng-click="updateItem(1)"
                                class="btn btn-warning pull-right"
                                ng-disabled="frmAdd.txtCustomerEmail.$invalid
                                          || frmAdd.txtCustomerPhone.$invalid
                                          || frmAdd.txtCustomerName.$invalid
                                          || frmAdd.txtCustomerAddress.$invalid

                                          "
                                          >
                            <i class="fa fa-fw fa-edit" aria-hidden="true"></i>
                            <span class="hidden-xs">Cập Nhật</span>
                        </button>
                        <div class="pull-right">&nbsp;</div>
                    </div>
                    <!-- ======= ============INSERT========= ==============-->
                    <div class="form-group" ng-show="model.datainit.pageStatus == 'add'">
                        <!-- ================= Trở Về ==============-->
                        <a class="btn btn-info" href="{{URL::to('System/ListCustomers')}}" title="CreateNews">
                            <i class="fa fa-fw fa-arrow-left" aria-hidden="true"></i>
                            <span class="hidden-xs">Trở Về</span>
                        </a>
                        <div class="pull-right">&nbsp;</div>
                         <!-- ================= Huy ==============-->
                        <button type="button" 
                                tabindex="101"
                                class="btn btn-danger"
                                ng-click="formReset()"
                                title="Hủy">
                                <i class="fa fa-fw fa-ban" aria-hidden="true"></i>
                                <span class="hidden-xs">Hủy</span>
                        </button>
                         <!-- ================= Them Moi==============-->
                        <button type="button" 
                                tabindex="99"
                                ng-click="addItem(1)"
                                class="btn btn-primary pull-right"
                                ng-disabled="frmAdd.txtCustomerEmail.$invalid
                                          || frmAdd.txtCustomerPhone.$invalid
                                          || frmAdd.txtCustomerName.$invalid
                                          || frmAdd.txtCustomerAddress.$invalid
                                          || model.request.txtCustomerPassword != model.request.txtCustomerPasswordAgain"
                                title="Thêm Mới">
                            <i class="fa fa-fw fa-plus" aria-hidden="true"></i>
                            <span class="hidden-xs">Thêm Khách Hàng</span>
                        </button>
                         <div class="pull-right">&nbsp;</div>
                        <button style="display: none;" type="button" class="btn btn-default pull-right" data-toggle="modal" data-target="#modalAdd">
                            <i class="fa fa-fw fa-plus" aria-hidden="true"></i>
                            <span class="hidden-xs">Thêm Nhóm Khách Hàng</span>
                        </button>
                        <div class="pull-right">&nbsp;</div>
                    </div>
                    <!--   ================= Hidden feilds ================ -->
                    <div class="form-group" style="display:none;">
                        <input  
                            type="text"
                            class="form-control" 
                            id="txtId" 
                            name="txtId" 
                            placeholder="txtId" 
                            autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                            value="{{$slug}}">
                    </div>
                    <!--   ================= first line ================ -->
                    <div class="form-group row"> 
                        <!--   ============== Customer Account ============= -->
                        <div class="col-md-4">
                            <label for="txtCustomerAccount" class="control-label display-block-xs">Tài Khoản Khách Hàng:</label>  
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-fw fa-newspaper-o" aria-hidden="true"></i>
                                </span>
                                <input  
                                    tabindex="1"
                                    type="text"
                                    class="form-control" 
                                    id="txtCustomerAccount" 
                                    name="txtCustomerAccount" 
                                    placeholder="Tài Khoản Khách Hàng" 
                                    ng-model="model.request.txtCustomerAccount"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                    ng-minlength="6"
                                    ng-maxlength="20"
                                    maxlength="20"
                                    ng-disabled = "model.datainit.pageStatus == 'update'"
                                    >
                            </div>
                        </div>
                        <!--   ============== Customer Email ============= -->
                        <div class="col-md-4" ng-class="{ 'has-error' : frmAdd.txtCustomerEmail.$invalid && !frmAdd.txtCustomerEmail.$pristine }">
                            <label for="txtCustomerEmail" class="control-label display-block-xs">Email Khách Hàng:</label>  
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-fw fa-newspaper-o" aria-hidden="true"></i>
                                </span>
                                <input  
                                    tabindex="1"
                                    type="text"
                                    class="form-control" 
                                    id="txtCustomerEmail" 
                                    name="txtCustomerEmail" 
                                    placeholder="Email Khách Hàng" 
                                    ng-model="model.request.txtCustomerEmail"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                    ng-minlength="2"
                                    ng-maxlength="150"
                                    maxlength="150"
                                    required >
                            </div>
                            <span class="help-block" ng-show="frmAdd.txtCustomerEmail.$error.required && frmAdd.txtCustomerEmail.$invalid && !frmAdd.txtCustomerEmail.$pristine">Vui Lòng Nhập Email Khách Hàng.</span>
                            <span class="help-block" ng-show="frmAdd.txtCustomerEmail.$error.minlength">Email Khách Hàng Không Được Ngắn Hơn 2 Ký Tự.</span>
                            <span class="help-block" ng-show="frmAdd.txtCustomerEmail.$error.maxlength">Email Khách Hàng Không Được Dài Hơn 150 Ký Tự.</span>
                        </div>
                         <!--   ========= Customer Phone number ========= -->
                        <div class="col-md-4" ng-class="{ 'has-error' : frmAdd.txtCustomerPhone.$invalid && !frmAdd.txtCustomerPhone.$pristine }">
                            <label for="txtCustomerPhone" class="control-label display-block-xs">Số Điện Thoại:</label>  
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-fw fa-newspaper-o" aria-hidden="true"></i>
                                </span>
                                <input  
                                    tabindex="1"
                                    type="text"
                                    class="form-control" 
                                    id="txtCustomerPhone" 
                                    name="txtCustomerPhone" 
                                    placeholder="Số Điện Thoại" 
                                    ng-model="model.request.txtCustomerPhone"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                    ng-minlength="10"
                                    ng-maxlength="11"
                                    maxlength="11"
                                    required >
                            </div>
                            <span class="help-block" ng-show="frmAdd.txtCustomerPhone.$error.required && frmAdd.txtCustomerPhone.$invalid && !frmAdd.txtCustomerPhone.$pristine">Vui Lòng Nhập SDT Khách Hàng.</span>
                            <span class="help-block" ng-show="frmAdd.txtCustomerPhone.$error.minlength">SDT Khách Hàng Không Được Ngắn Hơn 10 Ký Tự.</span>
                            <span class="help-block" ng-show="frmAdd.txtCustomerPhone.$error.maxlength">SDT Khách Hàng Không Được Dài Hơn 11 Ký Tự.</span>
                        </div>
                    </div>
                    <!--   ================= second line ================ -->
                    <div class="form-group row"> 
                        <!--   ============== txtCustomerName ============= -->
                        <div class="col-md-4" ng-class="{ 'has-error' : frmAdd.txtCustomerName.$invalid && !frmAdd.txtCustomerName.$pristine }">
                            <label for="txtCustomerName" class="control-label display-block-xs">Tên Khách Hàng:</label>  
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-fw fa-newspaper-o" aria-hidden="true"></i>
                                </span>
                                <input  
                                    tabindex="1"
                                    type="text"
                                    class="form-control" 
                                    id="txtCustomerName" 
                                    name="txtCustomerName" 
                                    placeholder="Tên Khách Hàng" 
                                    ng-model="model.request.txtCustomerName"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                    ng-minlength="2"
                                    ng-maxlength="50"
                                    maxlength="50"
                                    required >
                            </div>
                            <span class="help-block" ng-show="frmAdd.txtCustomerName.$error.required && frmAdd.txtCustomerName.$invalid && !frmAdd.txtCustomerName.$pristine">Vui Lòng Nhập Tên Khách Hàng.</span>
                            <span class="help-block" ng-show="frmAdd.txtCustomerName.$error.minlength">Tên Khách Hàng Không Được Ngắn Hơn 2 Ký Tự.</span>
                            <span class="help-block" ng-show="frmAdd.txtCustomerName.$error.maxlength">Tên Khách Hàng Không Được Dài Hơn 50 Ký Tự.</span>
                        </div>
                        <!--   ============== txtCustomerIdentify ============= -->
                        <div class="col-md-4" ng-class="{ 'has-error' : frmAdd.txtCustomerIdentify.$invalid && !frmAdd.txtCustomerIdentify.$pristine }">
                            <label for="txtCustomerIdentify" class="control-label display-block-xs">CMND:</label>  
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-fw fa-newspaper-o" aria-hidden="true"></i>
                                </span>
                                <input  
                                    tabindex="1"
                                    type="text"
                                    class="form-control" 
                                    id="txtCustomerIdentify" 
                                    name="txtCustomerIdentify" 
                                    placeholder="CMND" 
                                    ng-model="model.request.txtCustomerIdentify"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                    ng-minlength="1"
                                    ng-maxlength="20"
                                    maxlength="20"
                                    required >
                            </div>
                            <span class="help-block" ng-show="frmAdd.txtCustomerIdentify.$error.required && frmAdd.txtCustomerIdentify.$invalid && !frmAdd.txtCustomerIdentify.$pristine">Vui Lòng Nhập.</span>
                            <span class="help-block" ng-show="frmAdd.txtCustomerIdentify.$error.minlength">Không Được Ngắn Hơn 1 Ký Tự.</span>
                            <span class="help-block" ng-show="frmAdd.txtCustomerIdentify.$error.maxlength">Không Được Dài Hơn 20 Ký Tự.</span>
                        </div>
                        <!--   ============== txtCustomerStudentCard ============= -->
                        <div class="col-md-4" ng-class="{ 'has-error' : frmAdd.txtCustomerStudentCard.$invalid && !frmAdd.txtCustomerStudentCard.$pristine }">
                            <label for="txtCustomerStudentCard" class="control-label display-block-xs">Mã Sinh Viên:</label>  
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-fw fa-newspaper-o" aria-hidden="true"></i>
                                </span>
                                <input  
                                    tabindex="1"
                                    type="text"
                                    class="form-control" 
                                    id="txtCustomerStudentCard" 
                                    name="txtCustomerStudentCard" 
                                    placeholder="Mã Sinh Viên" 
                                    ng-model="model.request.txtCustomerStudentCard"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                    ng-minlength="1"
                                    ng-maxlength="20"
                                    maxlength="20"
                                    required >
                            </div>
                            <span class="help-block" ng-show="frmAdd.txtCustomerStudentCard.$error.required && frmAdd.txtCustomerStudentCard.$invalid && !frmAdd.txtCustomerStudentCard.$pristine">Vui Lòng Nhập.</span>
                            <span class="help-block" ng-show="frmAdd.txtCustomerStudentCard.$error.minlength">Không Được Ngắn Hơn 1 Ký Tự.</span>
                            <span class="help-block" ng-show="frmAdd.txtCustomerStudentCard.$error.maxlength">Không Được Dài Hơn 20 Ký Tự.</span>
                        </div>
                    </div>
                    <!--   ================= third line ================ -->
                    <div class="form-group row"> 
                        <!--   ============== Customer address ============= -->
                        <div class="col-md-7" ng-class="{ 'has-error' : frmAdd.txtCustomerAddress.$invalid && !frmAdd.txtCustomerAddress.$pristine }">
                            <label for="txtCustomerAddress" class="control-label display-block-xs">Địa Chỉ Khách Hàng:</label>  
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-fw fa-newspaper-o" aria-hidden="true"></i>
                                </span>
                                <input  
                                    tabindex="1"
                                    type="text"
                                    class="form-control" 
                                    id="txtCustomerAddress" 
                                    name="txtCustomerAddress" 
                                    placeholder="Địa Chỉ Khách Hàng" 
                                    ng-model="model.request.txtCustomerAddress"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                    ng-minlength="2"
                                    ng-maxlength="150"
                                    maxlength="150"
                                    required >
                            </div>
                            <span class="help-block" ng-show="frmAdd.txtCustomerAddress.$error.required && frmAdd.txtCustomerAddress.$invalid && !frmAdd.txtCustomerAddress.$pristine">Vui Lòng Nhập Địa Chỉ Khách Hàng.</span>
                            <span class="help-block" ng-show="frmAdd.txtCustomerAddress.$error.minlength">Địa Chỉ Khách Hàng Không Được Ngắn Hơn 2 Ký Tự.</span>
                            <span class="help-block" ng-show="frmAdd.txtCustomerAddress.$error.maxlength">Địa Chỉ Khách Hàng Không Được Dài Hơn 150 Ký Tự.</span>
                        </div>
                        <!--   ============== txtCustomerUniversity ============= -->
                        <div class="col-md-5" ng-class="{ 'has-error' : frmAdd.txtCustomerUniversity.$invalid && !frmAdd.txtCustomerUniversity.$pristine }">
                            <label for="txtCustomerUniversity" class="control-label display-block-xs">Trường học:</label>  
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-fw fa-newspaper-o" aria-hidden="true"></i>
                                </span>
                                <input  
                                    tabindex="1"
                                    type="text"
                                    class="form-control" 
                                    id="txtCustomerUniversity" 
                                    name="txtCustomerUniversity" 
                                    placeholder="Trường Học" 
                                    ng-model="model.request.txtCustomerUniversity"
                                    autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"
                                    ng-minlength="1"
                                    ng-maxlength="50"
                                    maxlength="50"
                                    required >
                            </div>
                            <span class="help-block" ng-show="frmAdd.txtCustomerUniversity.$error.required && frmAdd.txtCustomerUniversity.$invalid && !frmAdd.txtCustomerUniversity.$pristine">Vui Lòng Nhập.</span>
                            <span class="help-block" ng-show="frmAdd.txtCustomerUniversity.$error.minlength">Không Được Ngắn Hơn 1 Ký Tự.</span>
                            <span class="help-block" ng-show="frmAdd.txtCustomerUniversity.$error.maxlength">Không Được Dài Hơn 50 Ký Tự.</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--====================Hinh Anh ====================-->
        <div class="col-lg-4 col-xs-4">
            <div class="box box-danger">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-fw fa-newspaper-o" aria-hidden="true"></i> Hình Ảnh</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-fw fa-minus"></i></button>
                   </div>
                </div>
                <div class="box-body">
                    <div class="form-group row"> 
                        <!--   ============== Customer Account ============= -->
                        <div class="col-md-12">
                            <label for="txtCustomerAccount" class="control-label display-block-xs">Hình Ảnh Khách Hàng:</label>  
                         
                                <div class="form-group"  >   
                                    <div class="form-group col-xs-12 col-md-12">
                                        <div class="input-group">
                                            <span class="input-group-btn">
                                                <a  id="selectImages" 
                                                    name="selectImages" 
                                                    data-input="thumbnail" 
                                                    data-preview="holder" 
                                                    class="btn btn-primary">
                                                    <i class="fa fa-fw fa-picture-o"></i>
                                                    <span>Chọn Ảnh</span>
                                                </a>
                                            </span>
                                            <input  
                                                tabindex="5"
                                                id="thumbnail" 
                                                class="form-control" 
                                                type="text" 
                                                name="filepath" 
                                                placeholder="Chọn Ảnh Đại Diện"
                                                ng-model="model.request.txtImages" 
                                                autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">
                                            <i  class="fa fa-fw fa-times pull-right" 
                                                style="position: absolute;right: 7px;z-index: 2;top: 1px; background-color: rgba(255,255,255,0.8);padding: 8px;" 
                                                ng-click="resetChoiceThumbnail()"></i> 
                                        </div>
                                    </div> 
                                    <div class="form-group col-xs-12 col-sm-6 col-sm-offset-3"> 
                                        <img ng-src="[[model.request.txtImages]]"
                                             onError="this.src='{{asset('public/images/default-image.png')}}';"
                                             id="holder" 
                                             class="img-responsive img-thumbnail" 
                                             alt="Chọn Ảnh Đại Diện" 
                                             title="Chọn Ảnh Đại Diện" 
                                             height="150px">
                                    </div>
                                </div>
                          
                        </div>
                      
                    </div>
                </div>
            </div>
        </div>  
    </form>
</div>
@stop