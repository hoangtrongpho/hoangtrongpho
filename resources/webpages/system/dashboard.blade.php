@extends('system.layouts.index')

@section('headTitle')
   <title>{{ Lang::get('messages.system_heade_title') }} - Skyfire System @ 2017</title>
@stop

@section('pageTitle')
   <h1>
        <i class="fa fa-fw fa-tachometer" aria-hidden="true"></i> Dashboard
        <small>{{ Lang::get('messages.system_verion') }}</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{URL::to('/System/Dashboard')}}"><i class="fa fa-fw fa-dashboard"></i> Hệ Thống Quản Trị Website</a>
        </li>
    </ol>
@stop

@section('headCss')
   <!-- jvectormap -->
   <link rel="stylesheet" href="{{asset('public/system/plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
   <!-- Angular Toastr -->
   <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-toastr/angular-toastr.css')}}" />
   <!-- UI Bootstrap Modal --> 
   <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-ui-switch/bootstrap-switch.css')}}"/>

   <style type="text/css">
      .pagination{
         margin: 0px;
      }
      .small-box h3
      {
         font-size: 24px !important;
         font-weight: bolder !important;
      }
   </style>
@stop

@section('headJs')
   <!-- ChartJS 1.0.1 -->
   <script src="{{asset('public/system/plugins/chartjs/Chart.min.js')}}"></script>
   <!-- Angular Toastr -->
   <script type="text/javascript" src="{{asset('public/system/plugins/angular-toastr/angular-toastr.tpls.js')}}"></script>
   <!-- UI Bootstrap Modal --> 
   <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-bootstrap/ui-bootstrap-tpls-2.0.0.min.js')}}"></script>
   <!-- JS -->
   <script type="text/javascript" src="{{asset('public/system/common/system.common.angurlar.js')}}"></script>
   <script type="text/javascript" src="{{asset('public/system/common/system.dashboard.angurlar.js')}}"></script>

   <script type="text/javascript">
        $(document).ready(function() { 
            $(window).on('load', function() {  
                //Which will run only after page is fully loaded in background.
            });

            //Minify Left Menu
            // //$("body").addClass("sidebar-collapse");
        });
    </script>
@stop

@section('container')
   @if(Session::has('userAuth'))
      <?php
         $auth_id = Session::get('userAuth')->auth_id;
      ?> 
  @else
      <?php
         $auth_id = 0;
      ?> 
  @endif
<div ng-app="dashboardApp" ng-controller="dashboardController">

   <!-- Info boxes -->
   <div class="row">
      <!-- fix for small devices only -->
      <div class="clearfix visible-sm-block"></div>
      <div class="col-md-3 col-sm-6 col-xs-12">
         <div class="small-box bg-blue">
            <div class="inner">
               <h3>[[model.datainit.totalProduct | number]]</h3>
               <strong>Sản Phẩm</strong>
               <h5>Tổng Sản Phẩm</h5>
            </div>
            <div class="icon">
               <i class="fa fa-archive" aria-hidden="true"></i>
            </div>
               <a href="{{URL::to('System/ListProducts')}}" class="small-box-footer">Danh Sách SP <i class="fa fa-fw fa-arrow-circle-right"></i>
            </a>
         </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12">
         <div class="small-box bg-red">
            <div class="inner">
               <h3>[[model.datainit.totalInvoice | number]]</h3>
               <strong>Hóa Đơn</strong>
               <h5>Khối Lượng Giao Dịch</h5>
            </div>
            <div class="icon">
               <i class="fa fa-balance-scale" aria-hidden="true"></i>
            </div>
               <a href="{{URL::to('System/ListInvoices')}}" class="small-box-footer">Quản Lý Bán Hàng <i class="fa fa-fw fa-arrow-circle-right"></i>
            </a>
         </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12">
         <div class="small-box bg-yellow">
            <div class="inner">
               <h3>[[model.datainit.totalInvoicePrice | number]]</h3>
               <strong>VNĐ</strong>
               <h5>Tổng Doanh Thu</h5>
            </div>
            <div class="icon">
               <i class="fa fa-fw fa-usd" aria-hidden="true"></i>
            </div>
               <a href="{{URL::to('System/ListInvoices')}}" class="small-box-footer">Doanh Thu <i class="fa fa-fw fa-arrow-circle-right"></i>
            </a>
         </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12" ng-show="{{$auth_id}}==1">
         <div class="small-box bg-green">
            <div class="inner">
               <h3>[[model.datainit.totalView | number]]</h3>
               <strong>Lượt Xem</strong>
               <h5>Tổng Lượt Xem</h5>
            </div>
            <div class="icon">
               <i class="fa fa-users" aria-hidden="true"></i>
            </div>
               {{-- <a href="{{URL::to('System/StatisticAccess')}}" class="small-box-footer">View Map <i class="fa fa-fw fa-arrow-circle-right"></i> --}}
               <a href="#" class="small-box-footer">Lượt Xem <i class="fa fa-fw fa-arrow-circle-right"></i>
            </a>
         </div>
      </div>
   </div>
   <!-- /.row -->

   <!-- Main row -->
   <div class="row">

      <div class="col-xs-12 col-md-12">
         <div class="box box-danger">
            <div class="box-header with-border">
               <h3 class="box-title"><i class="fa fa-fw fa-line-chart" aria-hidden="true"></i> Thống Kê Doanh Thu Toàn Hệ Thống</h3>
               <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse">
                     <i class="fa fa-fw fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" ng-click="loadChartByMonth()">
                     <i class="fa fa-fw fa-refresh"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove">
                     <i class="fa fa-fw fa-times"></i>
                  </button>
               </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               <div class="row">
                  <div class="col-md-12">
                     <div class="row">
                        <div class="col-md-6 col-xs-12 pull-left">
                           <strong id="leftChartTitle">&nbsp;</strong>
                           <strong id="rightChartTitle">&nbsp;</strong>
                        </div>

                        <div class="col-md-6 col-xs-12 pull-right text-right">
                           <div class="dataTables_paginate paging_simple_numbers" id="example1_paginate">
                              <ul class="pagination">
                                 <li class="paginate_button previous" id="example1_previous">
                                    <a href="" ng-click="previousMonth()" aria-controls="example1" data-dt-idx="0" tabindex="0"><i class="fa fa-fw fa-chevron-left" aria-hidden="true"></i></a>
                                 </li>
                                 <li class="paginate_button">
                                    <a href="" ng-click="reloadMonth()" aria-controls="example1" data-dt-idx="1" tabindex="0"><i class="fa fa-fw fa-refresh" aria-hidden="true"></i></a>
                                 </li>
                                 <li class="paginate_button next" id="example1_next">
                                    <a href="" ng-click="nextMonth()" aria-controls="example1" data-dt-idx="7" tabindex="0"><i class="fa fa-fw fa-chevron-right" aria-hidden="true"></i></a>
                                 </li>
                              </ul>
                           </div>
                        </div>

                     </div>

                     <div class="chart" id="chartContent" style="min-height: 400px;">
                        <!-- Sales Chart Canvas -->
                        <canvas id="charByMonth" style="height: 400px;"></canvas>
                     </div>
                     <!-- /.chart-responsive -->
                  </div>
                  <!-- /.col -->
               </div>
               <!-- /.row -->
            </div>
            <!-- ./box-body -->

            <!-- Loading (remove the following to stop the loading)-->
            <div ng-show="model.datainit.preLoading" class="overlay">
                <i class="fa fa-fw fa-refresh fa-spin"></i>
            </div>
            <!-- end loading -->
         </div>
         <!-- /.box -->
      </div>
      <!-- /.col -->
   </div>
   <!-- /.row -->
</div>

@stop