@extends('system.layouts.index')

@section('headTitle')
   <title>{{ Lang::get('messages.system_heade_title') }} - Skyfire System @ 2017</title>
@stop

@section('pageTitle')
   <h1>
        <i class="fa fa-fw fa-tachometer" aria-hidden="true"></i> Dashboard
        <small>{{ Lang::get('messages.system_verion') }}</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{URL::to('/System/Dashboard')}}"><i class="fa fa-fw fa-dashboard"></i> Hệ Thống Quản Trị Website</a>
        </li>
    </ol>
@stop

@section('headCss')
   <!-- jvectormap -->
   <link rel="stylesheet" href="{{asset('public/system/plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
   <!-- Angular Toastr -->
   <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-toastr/angular-toastr.css')}}" />
   <!-- UI Bootstrap Modal --> 
   <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/angular-ui-switch/bootstrap-switch.css')}}"/>
   <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/jquery.dataTables.css')}}" />
   <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css')}}">
   <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.css')}}">
   <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/datatables/dataTables.bootstrap.css')}}" />

   <style type="text/css">
      .pagination{
         margin: 0px;
      }
      .small-box h3
      {
         font-size: 24px !important;
         font-weight: bolder !important;
      }
      #chartSaleBranch-svg{
        padding-left: 6px;
      }
   </style>
@stop

@section('headJs')
    <script src = "https://cdn.zingchart.com/zingchart.min.js" ></script>  
    <script src = "https://cdn.zingchart.com/angular/zingchart-angularjs.js" ></script>  
   <!-- Angular Toastr -->
   <script type="text/javascript" src="{{asset('public/system/plugins/angular-toastr/angular-toastr.tpls.js')}}"></script>
   <!-- UI Bootstrap Modal --> 
   <script type="text/javascript" src="{{asset('public/system/plugins/angular-ui-bootstrap/ui-bootstrap-tpls-2.0.0.min.js')}}"></script>
   <!-- Angular Datatables-->
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/angular-datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/colreorder/angular-datatables.colreorder.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/system/plugins/angular-datatables/plugins/bootstrap/angular-datatables.bootstrap.min.js')}}"></script>
   <!-- JS -->
   <script type="text/javascript" src="{{asset('public/system/common/system.common.angurlar.js')}}"></script>
   <script type="text/javascript" src="{{asset('public/system/common/system.dashboard2.angurlar.js')}}"></script>
   <!-- <link rel="stylesheet" type="text/css" href="{{asset('public/system/plugins/animatedBtn/animatedBtn.js')}}" /> -->
   <script type="text/javascript">
        $(document).ready(function() { 
            $(window).on('load', function() {  
                //Which will run only after page is fully loaded in background.
            });

            //Minify Left Menu
            // //$("body").addClass("sidebar-collapse");
        });
  </script>
@stop

@section('container')
<div ng-app="dashboardApp" ng-controller="dashboardController" ng-init="pageInit()">
    <!-- first box -->
    <div class="row">
     
      <!-- fix for small devices only -->
      <div class="clearfix visible-sm-block"></div>
      <div class="col-md-3 col-sm-6 col-xs-12">
         <div class="small-box bg-blue">
            <div class="inner">
               <h3>[[model.datainit.totalProduct | number]]</h3>
               <strong>Sản Phẩm</strong>
               <h5>Tổng Sản Phẩm</h5>
            </div>
            <div class="icon">
               <i class="fa fa-archive" aria-hidden="true"></i>
            </div>
               <a href="{{URL::to('System/ListProducts')}}" class="small-box-footer">Danh Sách SP <i class="fa fa-fw fa-arrow-circle-right"></i>
            </a>
         </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12">
         <div class="small-box bg-red">
            <div class="inner">
               <h3>[[model.datainit.totalInvoice | number]]</h3>
               <strong>Hóa Đơn</strong>
               <h5>Khối Lượng Giao Dịch</h5>
            </div>
            <div class="icon">
               <i class="fa fa-balance-scale" aria-hidden="true"></i>
            </div>
               <a href="{{URL::to('System/ListInvoices')}}" class="small-box-footer">Quản Lý Bán Hàng <i class="fa fa-fw fa-arrow-circle-right"></i>
            </a>
         </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12">
         <div class="small-box bg-yellow">
            <div class="inner">
               <h3>[[model.datainit.totalInvoicePrice | number]]</h3>
               <strong>VNĐ</strong>
               <h5>Tổng Doanh Thu</h5>
            </div>
            <div class="icon">
               <i class="fa fa-fw fa-usd" aria-hidden="true"></i>
            </div>
               <a href="{{URL::to('System/ListInvoices')}}" class="small-box-footer">Doanh Thu <i class="fa fa-fw fa-arrow-circle-right"></i>
            </a>
         </div>
      </div>

      <div class="col-md-3 col-sm-6 col-xs-12" >
         <div class="small-box bg-green">
            <div class="inner">
               <h5><b>Tổng Lượt Xem: [[model.datainit.totalView | number]]</b></h5>
               <h5>Lượt Xem Trong Ngày : [[model.datainit.totalViewByDay | number]]</h5>
               <h5>Lượt Xem Trong Tháng : [[model.datainit.totalViewByMonth | number]]</h5>
               <h5>Lượt Xem Trong Năm : [[model.datainit.totalViewByYear | number]]</h5>
            </div>
            <div class="icon">
               <i class="fa fa-users" aria-hidden="true"></i>
            </div>
               <a href="#" class="small-box-footer">Lượt Xem <i class="fa fa-fw fa-arrow-circle-right"></i>
            </a>
         </div>
      </div>
    </div>
    <!-- sale system chart -->
    <div class="row">
        <div class="col-lg-12" >
          <div class="col-lg-4 pull-right" >
            <div class="input-group">
              <span style="background-color: lightblue" class="input-group-addon " id="basic-addon3">
              <i class="fa fa-calendar" aria-hidden="true"></i>
              Chọn Năm Thống Kê:
              </span>

              <span class="input-group-btn">
                <button ng-click="backYear()" class="btn btn-primary" type="button"><i class="fa fa-backward" aria-hidden="true"></i></button>
              </span>

              <input 
                  style="text-align:center; background-color: lightcyan"
                  type="text" 
                  class="form-control"
                  ng-disabled=true
                  ng-model="model.datainit.currentYear">

              <span class="input-group-btn">
                <button ng-click="nextYear()" class="btn btn-primary" type="button"><i class="fa fa-forward" aria-hidden="true"></i></button>
              </span>
            </div>
          </div>
          
        </div><br><br>
        <div class="col-lg-12" >
            <div  zingchart id="chartSaleSystem" zc-render="model.datainit.myRender" zc-json="model.chartSaleSystem" ></div>
        </div>
    </div>
 
    
</div>

@stop