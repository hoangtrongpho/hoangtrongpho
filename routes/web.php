<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// //Front-End
Route::group(array('namespace' => 'Website'), function(){

    //Đăng Nhập Đăng Xuất
    Route::get('/home',['uses'=>'HomePageController@indexHome']);
    Route::get('/',['uses'=>'HomePageController@indexHomePage']);
    Route::get('/trang-chu',['uses'=>'HomePageController@indexHomePage']);
    Route::get('/dang-xuat',['uses'=>'HomePageController@indexLogout']);

    Route::get('tim-kiem',['uses'=>'HomePageController@indexSearch']);

    Route::get('lien-he',['uses'=>'HomePageController@indexContact']);
    Route::get('gioi-thieu',['uses'=>'HomePageController@indexAbout']);

    Route::get('/chi-tiet-danh-muc',['uses'=>'HomePageController@indexCategoryDetail']);
    Route::get('/chi-tiet-danh-muc/{slug}',['uses'=>'HomePageController@indexCategoryDetail']);
    Route::get('/san-pham-giam-gia',['uses'=>'HomePageController@indexSaleOffProduct']);
    Route::get('/chi-tiet-the/{slug}',['uses'=>'HomePageController@indexTagDetail']);
    Route::get('/chi-tiet-san-pham/{slug}',['uses'=>'HomePageController@indexProductDetail']);
    Route::get('/tin-tuc',['uses'=>'HomePageController@indexListNews']);
    Route::get('/chi-tiet-ban-tin/{slug}',['uses'=>'HomePageController@indexNewsDetail']);
    Route::get('/tuyen-dung',['uses'=>'HomePageController@indexRecruitmentNews']);
    Route::get('/gio-hang',['uses'=>'HomePageController@indexCart']);
    Route::get('/dang-ky',['uses'=>'HomePageController@indexRegister']);
    Route::get('/dang-nhap',['uses'=>'HomePageController@indexLogin']);

    Route::get('/lich-su-don-hang',['uses'=>'HomePageController@indexHistoryBill']);
    Route::get('/thong-tin-khach-hang',['uses'=>'HomePageController@indexCustomerInfo']);
});

//Back-End
Route::group(array('namespace' => 'System'), function(){

    //Đăng Nhập Đăng Xuất
    Route::get('System/',['uses'=>'SystemIndexController@indexLogin']);
    Route::get('System/Login',['uses'=>'SystemIndexController@indexLogin']);
    Route::get('System/Logout',['uses'=>'SystemIndexController@indexLogout']);

    //Trang Dashboard
    // Route::get('System/Dashboard',['uses'=>'SystemIndexController@indexDashboard']);
    Route::get('System/Dashboard2',['uses'=>'StatisticIndexController@indexDashboard2']);
    //Quản Trị Tin Tức
    Route::get('System/ListNews',['uses'=>'NewsIndexController@indexListNews']);
    Route::get('System/CreateNews',['uses'=>'NewsIndexController@indexCreateNews']);
    Route::get('System/UpdateNews/{slug}',['uses'=>'NewsIndexController@indexUpdateNews']);
    //Quản Trị Thể Loại Tin Tức
    Route::get('System/ListNewsCategories',['uses'=>'NewsIndexController@indexListNewsCategories']);
    //Quản Trị Thẻ Tags Tin tức
    Route::get('System/ListNewsTags',['uses'=>'NewsIndexController@indexListNewsTags']);

    //Quản Trị Hình Ảnh
    Route::get('System/GalleryLirary',['uses'=>'GalleryIndexController@indexGalleryLirary']);

    //Quản Trị Tài Khoản
    Route::get('System/ListUsers',['uses'=>'UserIndexController@indexListUsers']);
    Route::get('System/Profile',['uses'=>'UserIndexController@indexProfile']);

    //Thống Kê
    Route::get('System/StatisticAccess',['uses'=>'StatisticIndexController@indexStatisticAccess']);
    Route::get('System/SaleReport',['uses'=>'StatisticIndexController@indexSaleReport']);
    Route::get('System/CustomerReport',['uses'=>'StatisticIndexController@indexCustomerReport']);
    Route::get('System/ZingChartTest',['uses'=>'StatisticIndexController@indexZingChartTest']);
    Route::get('System/PrivateReport',['uses'=>'StatisticIndexController@indexPrivateReport']);
    //Quản Trị Chi Nhánh - Phòng Ban - Chức Vụ
    Route::get('System/ListBranches',['uses'=>'BranchIndexController@indexListBranches']);
    Route::get('System/ListDepartments',['uses'=>'BranchIndexController@indexListDepartments']);
    Route::get('System/ListPositions',['uses'=>'BranchIndexController@indexListPotitions']);
    Route::get('System/ListBranch',['uses'=>'BranchController@ListBranch']);
    // store
    Route::get('System/RemainStore',['uses'=>'StoreIndexController@indexRemainStore']);
    Route::get('System/SaleStore',['uses'=>'StoreIndexController@indexSaleStore']);
    Route::get('System/ImportExportStore',['uses'=>'StoreIndexController@indexImportExportStore']);
    //  subadmin STORE
    Route::get('System/PrivateRemainStore',['uses'=>'StoreIndexController@indexPrivateRemainStore']);
    Route::get('System/PrivateSaleStore',['uses'=>'StoreIndexController@indexPrivateSaleStore']);
    //Quản Trị Đơn Vị Sản Phẩm
    Route::get('System/ListProductUnits',['uses'=>'ProductsIndexController@indexListUnits']);
    //Quản Trị Màu Sắc Sản Phẩm
    Route::get('System/ListProductColors',['uses'=>'ProductsIndexController@indexListColors']);
    //Quản Trị Xuất Xứ
    Route::get('System/ListProductOrigins',['uses'=>'ProductsIndexController@indexListOrigins']);
    //Quản Trị Xuất Xứ
    Route::get('System/ListProductTags',['uses'=>'ProductsIndexController@indexListProductTags']);
    //Quản Trị Nhà Sản Xuất
    Route::get('System/ListManufacturers',['uses'=>'ProductsIndexController@indexListManufacturers']);
    //Quản Trị Nhóm Sản Phẩm
    Route::get('System/ListProductCategories',['uses'=>'ProductsIndexController@indexListCategories']);

    //Quản Trị Sản Phẩm
    Route::get('System/ListProducts',['uses'=>'ProductsIndexController@indexListProducts']);
    Route::get('System/CreateProduct',['uses'=>'ProductsIndexController@indexCreateProduct']);
    Route::get('System/UpdateProduct/{slug}',['uses'=>'ProductsIndexController@indexUpdateProduct']);
    Route::get('System/ExportProducts',['uses'=>'ProductsIndexController@indexListProducts']);

    

    //Lịch Sử Phát Triển Phần Mềm----------------------
    Route::get('System/History',['uses'=>'SystemIndexController@indexHistory']);


    //Quản Trị Thông Tin Công Ty
    Route::get('System/InfomationCompany',['uses'=>'ProductsIndexController@indexInfomationCompany']);

    Route::get('System/WebConfig',['uses'=>'ProductsIndexController@indexWebConfig']);

    //Quản Trị Trang
    Route::get('System/ListPages',['uses'=>'PagesIndexController@indexListPages']);
    Route::get('System/CreatePages',['uses'=>'PagesIndexController@indexCreatePages']);
    Route::get('System/UpdatePages/{slug}',['uses'=>'PagesIndexController@indexUpdatePages']);

    //Quản Trị Trang Dịch Vụ
    Route::get('System/ListServicePages',['uses'=>'PagesIndexController@indexListServicePages']);
    Route::get('System/CreateServicePages',['uses'=>'PagesIndexController@indexCreateServicePages']);
    Route::get('System/UpdateServicePages/{slug}',['uses'=>'PagesIndexController@indexUpdateServicePages']);

    //Quản Trị Tin Tức
    Route::get('System/ListPages',['uses'=>'PageIndexController@indexListPages']);
    Route::get('System/CreatePage',['uses'=>'PageIndexController@indexCreatePage']);
    Route::get('System/UpdatePage/{slug}',['uses'=>'PageIndexController@indexUpdatePage']);

    //Quản Trị Đại Lý
    Route::get('System/ListCustomers',['uses'=>'CustomerIndexController@indexListCustomers']);
    Route::get('System/CreateCustomer',['uses'=>'CustomerIndexController@indexCreateCustomer']);
    Route::get('System/UpdateCustomer/{customerCode}',['uses'=>'CustomerIndexController@indexUpdateCustomer']);

    //Đơn Hàng
    Route::get('System/ListInvoices',['uses'=>'InvoicesIndexController@indexListInvoices']);
    Route::get('System/ListPrivateInvoices',['uses'=>'InvoicesIndexController@indexListPrivateInvoices']);
    Route::get('System/CreateInvoice',['uses'=>'InvoicesIndexController@indexCreateInvoice']);
    Route::get('System/UpdateInvoice/{slug}',['uses'=>'InvoicesIndexController@indexUpdateInvoice']);
    // thông tin hệ thống
    Route::get('System/SystemInformation',['uses'=>'SystemInformationController@indexSystemInformation']);
});
