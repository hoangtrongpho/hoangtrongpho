<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteapiProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::get('/user', function (Request $request) {
//     return $request->user();
// })->middleware('auth:api');

//System API 
Route::group(array('namespace' => 'WebsiteApi','middleware' => ['api']), function(){

    //API Đại Lý
    Route::post('WebsiteApi/checkLogin',['uses'=>'WebsiteApiAuthController@apiCheckLogin']);
    Route::post('WebsiteApi/RequestRegister',['uses'=>'WebsiteApiAuthController@apiRequestRegister']);

    //API Thông Tin Đại Lý
    Route::post('WebsiteApi/GetAgencyDetail',['uses'=>'WebsiteApiAgencyController@apiGetDetail']);
    Route::post('WebsiteApi/UpdateAgency',['uses'=>'WebsiteApiAgencyController@apiUpdate']);
    Route::post('WebsiteApi/ChangePassword',['uses'=>'WebsiteApiAgencyController@apiChangePassword']);

    //API Quên Mật Khẫu
    Route::post('WebsiteApi/GenerateSecureCode',['uses'=>'WebsiteApiAgencyController@apiGenerateSecureCode']);
    Route::post('WebsiteApi/SecureCodeConfirm',['uses'=>'WebsiteApiAgencyController@apiSecureCodeConfirm']);
    Route::post('WebsiteApi/FinishChangePassword',['uses'=>'WebsiteApiAgencyController@apiFinishChangePassword']);

    //API Khu Vực
    Route::post('WebsiteApi/GetListProvinces',['uses'=>'WebsiteApiAreasController@apiGetListProvinces']);
    //API Cart
    Route::post('WebsiteApi/AddCart',['uses'=>'WebsiteApiCartController@apiAddCart']);
    Route::post('WebsiteApi/loadCart',['uses'=>'WebsiteApiCartController@apiGetListCart']);
    //UpdateCart
    Route::post('WebsiteApi/UpdateCart',['uses'=>'WebsiteApiCartController@apiUpdateCart']);
    Route::post('WebsiteApi/SaveInvoiceSession',['uses'=>'WebsiteApiCartController@apiSaveInvoiceSession']);
    Route::post('WebsiteApi/SaveInvoice',['uses'=>'WebsiteApiCartController@apiSaveInvoice']);
    Route::post('WebsiteApi/getSuccessOrder',['uses'=>'WebsiteApiCartController@apigetSuccessOrder']);
    Route::post('WebsiteApi/getCustomerDetail',['uses'=>'WebsiteApiCartController@apigetCustomerDetail']);
    //RegisterUser
    Route::post('WebsiteApi/RegisterUser',['uses'=>'WebsiteApiUserInfoController@apiregisterUser']);
    //GetInvoiceByUser
    Route::post('WebsiteApi/GetInvoiceByUser',['uses'=>'WebsiteApiUserInfoController@apigetInvoiceByUser']);
    //ChangePass
    Route::post('WebsiteApi/GetUserDetail',['uses'=>'WebsiteApiUserInfoController@apigetUserDetail']);
    Route::post('WebsiteApi/UpdatePass',['uses'=>'WebsiteApiUserInfoController@apiupdatePass']);
    // customer info
    Route::post('WebsiteApi/GetUserDetail',['uses'=>'WebsiteApiUserInfoController@apiGetUserDetail']);
    Route::post('WebsiteApi/UpdateUserDetail',['uses'=>'WebsiteApiUserInfoController@apiUpdateUserDetail']);

});

//System API 
Route::group(array('namespace' => 'SystemApi','middleware' => ['api']), function(){

    //API Đăng Nhập Quản Trị
    Route::post('SystemApi/checkLogin',['uses'=>'SystemApiAuthController@apiCheckLogin']);

    //API Dashboard Thống Kê Lượt Truy Cập
    Route::post('SystemApi/GetDashboard',['uses'=>'SystemApiStatisticalController@apiGetDashboard']);
    Route::post('SystemApi/GetNumeral',['uses'=>'SystemApiStatisticalController@apiGetNumeral']);
    Route::post('SystemApi/GetMapVector',['uses'=>'SystemApiStatisticalController@apiGetMapVector']);

    //API Tin Tức
    Route::post('SystemApi/GetListNews',['uses'=>'SystemApiNewsController@apiGetList']);
    Route::post('SystemApi/GetNewsDetails',['uses'=>'SystemApiNewsController@apiGetDetail']);
    Route::post('SystemApi/AddNews',['uses'=>'SystemApiNewsController@apiAdd']);
    Route::post('SystemApi/UpdateNews',['uses'=>'SystemApiNewsController@apiUpdate']);
    Route::post('SystemApi/ChangeStatusNews',['uses'=>'SystemApiNewsController@apiChangeStatus']);
    Route::post('SystemApi/RemoveNews',['uses'=>'SystemApiNewsController@apiRemove']);
    // Nhóm Tin Tức
    Route::post('SystemApi/GetListNewsCategories',['uses'=>'SystemApiNewsCategoriesController@apiGetList']);
    Route::post('SystemApi/AddNewsCategories',['uses'=>'SystemApiNewsCategoriesController@apiAdd']);
    Route::post('SystemApi/UpdateNewsCategories',['uses'=>'SystemApiNewsCategoriesController@apiUpdate']);
    Route::post('SystemApi/RemoveNewsCategories',['uses'=>'SystemApiNewsCategoriesController@apiRemove']);

    //API Nhân Viên
    Route::post('SystemApi/apiGetListGroups',['uses'=>'SystemApiUserController@apiGetListGroups']);
    Route::post('SystemApi/GetListUsers',['uses'=>'SystemApiUserController@apiGetListUsers']);
    Route::post('SystemApi/GetUserDetail',['uses'=>'SystemApiUserController@apiGetUserDetail']);
    Route::post('SystemApi/AddUser',['uses'=>'SystemApiUserController@apiAdd']);
    Route::post('SystemApi/UpdateUser',['uses'=>'SystemApiUserController@apiUpdate']);
    Route::post('SystemApi/UpdateProfile',['uses'=>'SystemApiUserController@apiUpdateProfile']);
    Route::post('SystemApi/ChangeStatusUser',['uses'=>'SystemApiUserController@apiChangeStatus']);
    Route::post('SystemApi/RemoveUser',['uses'=>'SystemApiUserController@apiRemove']);


    //API Khách Hàng
    Route::post('SystemApi/GetListCustomer',['uses'=>'SystemApiCustomerController@apiGetList']);
    Route::post('SystemApi/AddCustomer',['uses'=>'SystemApiCustomerController@apiAdd']);
    Route::post('SystemApi/GetCustomerDetails',['uses'=>'SystemApiCustomerController@apiGetDetail']);
    Route::post('SystemApi/UpdateCustommer',['uses'=>'SystemApiCustomerController@apiUpdate']);
    Route::post('SystemApi/RemoveCustomer',['uses'=>'SystemApiCustomerController@apiRemove']);
    Route::post('SystemApi/ChangeStatusCustomer',['uses'=>'SystemApiCustomerController@apiChangeStatus']);
    //API Group Khach Hang
    Route::post('SystemApi/GetListCustomerGroup',['uses'=>'SystemApiCustomerGroupController@apiGetList']);
    Route::post('SystemApi/AddCustomerGroup',['uses'=>'SystemApiCustomerGroupController@apiAdd']);
    Route::post('SystemApi/UpdateCustomerGroup',['uses'=>'SystemApiCustomerGroupController@apiUpdate']);
    Route::post('SystemApi/RemoveCustomerGroup',['uses'=>'SystemApiCustomerGroupController@apiRemove']);
    //API Sản Phẩm
    Route::post('SystemApi/GetListProducts',['uses'=>'SystemApiProductsController@apiGetList']);
    Route::post('SystemApi/GetProductDetails',['uses'=>'SystemApiProductsController@apiGetDetails']);
    
    Route::post('SystemApi/AddProduct',['uses'=>'SystemApiProductsController@apiAdd']);
    Route::post('SystemApi/UpdateProduct',['uses'=>'SystemApiProductsController@apiUpdate']);
    Route::post('SystemApi/RemoveProduct',['uses'=>'SystemApiProductsController@apiRemove']);
    Route::post('SystemApi/ChangeStatusProduct',['uses'=>'SystemApiProductsController@apiChangeStatus']);
    Route::post('SystemApi/GetListProductCat',['uses'=>'SystemApiProductsController@apiGetListCat']);
    Route::post('SystemApi/GetListUnit',['uses'=>'SystemApiProductsController@apiGetListUnit']);
    Route::post('SystemApi/GetListOrigin',['uses'=>'SystemApiProductsController@apiGetListOrigin']);
    Route::post('SystemApi/GetListManufacturer',['uses'=>'SystemApiProductsController@apiGetListManufacturer']);
    Route::post('SystemApi/GetListColor',['uses'=>'SystemApiProductsController@apiGetListColor']);
    Route::post('SystemApi/GetListTag',['uses'=>'SystemApiProductsController@apiGetListTag']);
   
    
    //API Nhóm Sản Phẩm
    Route::post('SystemApi/GetListProductCategories',['uses'=>'SystemApiProductCategoriesController@apiGetList']);
    Route::post('SystemApi/GetListProductCategoriesForChosen',['uses'=>'SystemApiProductCategoriesController@apiGetListForChosen']);
    Route::post('SystemApi/AddProductCategory',['uses'=>'SystemApiProductCategoriesController@apiAdd']);
    Route::post('SystemApi/UpdateProductCategory',['uses'=>'SystemApiProductCategoriesController@apiUpdate']);
    Route::post('SystemApi/RemoveProductCategory',['uses'=>'SystemApiProductCategoriesController@apiRemove']);
     Route::post('SystemApi/GetCatParent',['uses'=>'SystemApiProductCategoriesController@apiGetCatParent']);
     
     Route::post('SystemApi/UpdatePtoPOrder',['uses'=>'SystemApiProductCategoriesController@apiUpdatePtoPOrder']);
    Route::post('SystemApi/UpdateOrder',['uses'=>'SystemApiProductCategoriesController@apiUpdateOrder']);
    //API Đơn Vị Sản Phẩm
    Route::post('SystemApi/GetListProductUnits',['uses'=>'SystemApiProductUnitController@apiGetList']);
    Route::post('SystemApi/AddProductUnit',['uses'=>'SystemApiProductUnitController@apiAdd']);
    Route::post('SystemApi/UpdateProductUnit',['uses'=>'SystemApiProductUnitController@apiUpdate']);
    Route::post('SystemApi/RemoveProductUnit',['uses'=>'SystemApiProductUnitController@apiRemove']);

    //API Màu Sắc Sản Phẩm
    Route::post('SystemApi/GetListProductColors',['uses'=>'SystemApiProductColorController@apiGetList']);
    Route::post('SystemApi/AddProductColor',['uses'=>'SystemApiProductColorController@apiAdd']);
    Route::post('SystemApi/UpdateProductColor',['uses'=>'SystemApiProductColorController@apiUpdate']);
    Route::post('SystemApi/RemoveProductColor',['uses'=>'SystemApiProductColorController@apiRemove']);

    //API Xuất Xứ Sản Phẩm
    Route::post('SystemApi/GetListProductOrigins',['uses'=>'SystemApiProductOriginController@apiGetList']);
    Route::post('SystemApi/AddProductOrigin',['uses'=>'SystemApiProductOriginController@apiAdd']);
    Route::post('SystemApi/UpdateProductOrigin',['uses'=>'SystemApiProductOriginController@apiUpdate']);
    Route::post('SystemApi/RemoveProductOrigin',['uses'=>'SystemApiProductOriginController@apiRemove']);

    //API Thẻ Sản Phẩm
    Route::post('SystemApi/GetListProductTags',['uses'=>'SystemApiProductTagController@apiGetList']);
    Route::post('SystemApi/AddProductTag',['uses'=>'SystemApiProductTagController@apiAdd']);
    Route::post('SystemApi/UpdateProductTag',['uses'=>'SystemApiProductTagController@apiUpdate']);
    Route::post('SystemApi/RemoveProductTag',['uses'=>'SystemApiProductTagController@apiRemove']);

    //API Thẻ Sản Phẩm
    Route::post('SystemApi/GetListProductManufacturers',['uses'=>'SystemApiProductManufacturerController@apiGetList']);
    Route::post('SystemApi/AddProductManufacturer',['uses'=>'SystemApiProductManufacturerController@apiAdd']);
    Route::post('SystemApi/UpdateProductManufacturer',['uses'=>'SystemApiProductManufacturerController@apiUpdate']);
    Route::post('SystemApi/RemoveProductManufacturer',['uses'=>'SystemApiProductManufacturerController@apiRemove']);


    //API Trang Thông Tin
    Route::post('SystemApi/GetListPages',['uses'=>'SystemApiPageController@apiGetList']);
    Route::post('SystemApi/GetPageDetails',['uses'=>'SystemApiPageController@apiGetDetail']);
    Route::post('SystemApi/AddPage',['uses'=>'SystemApiPageController@apiAdd']);
    Route::post('SystemApi/UpdatePage',['uses'=>'SystemApiPageController@apiUpdate']);
    Route::post('SystemApi/ChangeStatusPage',['uses'=>'SystemApiPageController@apiChangeStatus']);
    Route::post('SystemApi/RemovePage',['uses'=>'SystemApiPageController@apiRemove']);

    //Hoa Don
    Route::post('SystemApi/AddInvoice',['uses'=>'SystemApiInvoiceController@apiAdd']);
    Route::post('SystemApi/GetListInvoice',['uses'=>'SystemApiInvoiceController@apiGetList']);
    Route::post('SystemApi/ChangeStatusInvoice',['uses'=>'SystemApiInvoiceController@apiChangeStatus']);
    Route::post('SystemApi/RemoveInvoice',['uses'=>'SystemApiInvoiceController@apiRemove']);
    Route::post('SystemApi/UpdateInvoice',['uses'=>'SystemApiInvoiceController@apiUpdate']);
    Route::post('SystemApi/GetInvoiceDetail',['uses'=>'SystemApiInvoiceController@apiGetDetail']);
    Route::post('SystemApi/GetProductByBranch',['uses'=>'SystemApiInvoiceController@apiGetProductByBranch']);
    ///GetListInfo
    Route::post('SystemApi/GetListInfo',['uses'=>'SystemApiSystemInformationController@apiGetList']);
    Route::post('SystemApi/UpdateSystemInformation',['uses'=>'SystemApiSystemInformationController@apiUpdate']);
    Route::post('SystemApi/RemoveSlide',['uses'=>'SystemApiSystemInformationController@apiRemoveSlide']);

    Route::post('SystemApi/AddSlide',['uses'=>'SystemApiSystemInformationController@apiAddSlide']);
    
    Route::post('SystemApi/LoadSlide',['uses'=>'SystemApiSystemInformationController@apiLoadSlide']);
    
    // Branch
    Route::post('SystemApi/GetBranchList',['uses'=>'SystemApiBranchController@apiGetList']);
    Route::post('SystemApi/AddBranch',['uses'=>'SystemApiBranchController@apiAdd']);
    Route::post('SystemApi/UpdateBranch',['uses'=>'SystemApiBranchController@apiUpdate']);
    Route::post('SystemApi/RemoveBranch',['uses'=>'SystemApiBranchController@apiRemove']);
    //API Doanh Thu
    Route::post('SystemApi/GetRevenueByYear',['uses'=>'SystemApiStatisticalController@apiGetNumeral']);
    Route::post('SystemApi/GetCustomerSale',['uses'=>'SystemApiStatisticalController@apiGetCustomerSale']);
    Route::post('SystemApi/GetCustomerDebtSale',['uses'=>'SystemApiStatisticalController@apiGetCustomerDebtSale']);
    Route::post('SystemApi/GetProductSale',['uses'=>'SystemApiStatisticalController@apiGetProductSale']);
    Route::post('SystemApi/GetProductDebtSale',['uses'=>'SystemApiStatisticalController@apiGetProductDebtSale']);
    Route::post('SystemApi/GetBranchSale',['uses'=>'SystemApiStatisticalController@apiGetBranchSale']);
    Route::post('SystemApi/GetBranchDebtSale',['uses'=>'SystemApiStatisticalController@apiGetBranchDebtSale']);
    Route::post('SystemApi/GetSubadminSale',['uses'=>'SystemApiStatisticalController@apiGetSubadminSale']);
    Route::post('SystemApi/GetSubadminDebtSale',['uses'=>'SystemApiStatisticalController@apiGetSubadminDebtSale']);
    // ==============API private subadmin============
    // apiGetPrivateCustomerSale
    Route::post('SystemApi/GetPrivateCustomerSale',['uses'=>'SystemApiStatisticalController@apiGetPrivateCustomerSale']);
    // apiGetPrivateCustomerDebtSale
    Route::post('SystemApi/GetPrivateCustomerDebtSale',['uses'=>'SystemApiStatisticalController@apiGetPrivateCustomerDebtSale']);
    // apiGetPrivateProductSale
    Route::post('SystemApi/GetPrivateProductSale',['uses'=>'SystemApiStatisticalController@apiGetPrivateProductSale']);
    // apiGetPrivateProductDebtSale
    Route::post('SystemApi/GetPrivateProductDebtSale',['uses'=>'SystemApiStatisticalController@apiGetPrivateProductDebtSale']);
    // apiGetPrivateSubadminSale
    Route::post('SystemApi/GetPrivateSubadminSale',['uses'=>'SystemApiStatisticalController@apiGetPrivateSubadminSale']);
    // apiGetPrivateSubadminDebtSale
    Route::post('SystemApi/GetPrivateSubadminDebtSale',['uses'=>'SystemApiStatisticalController@apiGetPrivateSubadminDebtSale']);
    // ==========================chart report===========================
    Route::post('SystemApi/GetTotalSaleByDateTime',['uses'=>'SystemApiStatisticalController@apiGetTotalSaleByDateTime']);
    Route::post('SystemApi/GetTotalSaleByDateTimeBranch',['uses'=>'SystemApiStatisticalController@apiGetTotalSaleByDateTimeBranch']);
    
    // store API
    Route::post('SystemApi/GetListStore',['uses'=>'SystemApiStoreController@apiGetList']);
    Route::post('SystemApi/GetListStore2',['uses'=>'SystemApiStoreController@apiGetList2']);
    Route::post('SystemApi/InnitStore',['uses'=>'SystemApiStoreController@apiInnitStore']);
    Route::post('SystemApi/UpdateStoreSumQuan',['uses'=>'SystemApiStoreController@apiUpdateStoreSumQuan']);
    Route::post('SystemApi/UpdateStoreQuan',['uses'=>'SystemApiStoreController@apiUpdateStoreQuan']);
    Route::post('SystemApi/GetStoreByBranch',['uses'=>'SystemApiStoreController@apiGetStoreByBranch']);
    
    // subadmin quan ly don hang
    Route::post('SystemApi/GetRemainAndSaleByBranch',['uses'=>'SystemApiStoreController@apiGetRemainAndSaleByBranch']);
    
    
    
});