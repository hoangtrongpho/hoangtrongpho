jQuery(function($){

	$('html').removeClass('no-js'); 



	// PRELOADER
	$(window).load(function() {
		$('#page_preloader').delay(700).fadeOut(700);

		function preloaderRemove() {
			$('#page_preloader').remove()
		};

		setTimeout( preloaderRemove, 3000 );
	});



	// IOS HOVER
	if ((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i))) {
		$('a').on("touchstart", function() {});
	};



	// HZ
	$('ul.styles > li').click(function(){
		var className = $(this).attr('class');
		$('body').removeClass('theme-style-0 theme-style-1 theme-style-2 theme-style-3');
		$('body').addClass(className);
	});



	// PLACEHOLDER JS 
	$('[placeholder]').each(function(){
	  if ($(this).val() === '') {
		var hint = $(this).attr('placeholder');
		$(this).val(hint).addClass('hint');
	  }
	});

	$('[placeholder]').focus(function() {
	  if ($(this).val() === $(this).attr('placeholder')) {
		$(this).val('').removeClass('hint');
	  }
	}).blur(function() {
	  if ($(this).val() === '') {
		$(this).val($(this).attr('placeholder')).addClass('hint');
	  }
	});                    



	// FORM VALIDATION MINI
	$.fn.formValidation=function(){this.find("input, textarea").after('<p class="alert-form-info"></p>'),this.on("submit",function(t){if($(this).find("input, textarea").each(function(){""==$(this).val()&&($(this).addClass("alert-form").next().html("Field can't be blank").slideDown(),$(this).on("focus",function(){$(this).removeClass("alert-form").next().slideUp()}),t.preventDefault())}),$(this).find("input[type=email]").length){var e=$(this).find("input[type=email]");e.val().length>0&&(e.val().length<6||-1==e.val().indexOf("@")||-1==e.val().indexOf("."))&&(e.addClass("alert-form").next().html("Incorrect email").slideDown(),e.on("focus",function(){$(this).removeClass("alert-form").next().slideUp()}),t.preventDefault())}if(2==$(this).find("input[type=password]").length){var n=$(this).find("input[type=password]:eq(0)"),i=$(this).find("input[type=password]:eq(1)");n.val()!=i.val()&&(i.addClass("alert-form").next().html("Passwords do not match").slideDown(),i.on("focus",function(){i.removeClass("alert-form").next().slideUp()}),t.preventDefault())}})};


   
	// FORM STYLES   
	$('.address_table form, .customer_address form').addClass('form-horizontal');



	// CUSTOM SELECTS 
	$('.header_currency select, #navigation select').styler();
	$('.jq-selectbox__trigger').append('<i class="fa fa-angle-down"></i>');



	// MEGAMENU DESKTOP 
	$('.sf-menu').superfish({
		animation: {height: 'show'},
		speed: 'fast',
		onShow: imgIndent
	});

	var path = window.location.pathname.split('/');
	path = path[path.length-1];
	if (path !== undefined) {
	  $("ul.sf-menu > li").children("a[href$='" + path + "']").parents('li').children('a').addClass('active');
	};

	var path2 = window.location.pathname;
	if (path2 == '/' || path == undefined) {
	  $("ul.sf-menu > li").children("a[href$='" + path2 + "']").parents('li').children('a').addClass('active');
	};



	// MEGAMENU & SHOWCASE BANNERS IMG INDENTS
    function imgIndent() {
		$('#submenu_2__banners a, #showcase a').each(function() {
			var linkHeight = $(this).outerHeight();

			var img = $(this).find('img');
			var imgHeight = img.outerHeight();

			if ( linkHeight < imgHeight ) {
				img.css({ 'top': -(imgHeight-linkHeight)/2 });
			}
			else {
				img.css({ 'top': (imgHeight-linkHeight)/2 });
			};
			
		});
    };

	$(window).on("load resize", imgIndent);



	// MEGAMENU MOBILE 
	$(document).ready(function(){
		$(".megamenu_mobile h2").click(function(){
			$(".level_1").slideToggle("slow");
			$(this).toggleClass("active");
		});

		$(".level_1_trigger").click(function(){
			$(this).parent().parent().find(".level_2").slideToggle("slow");
			$(this).toggleClass("active");
			return false;
		});

		$(".level_2_trigger").click(function(){
			$(this).parent().parent().find(".level_3").slideToggle("slow");
			$(this).toggleClass("active");
			return false;
		});

		$('.megamenu_mobile h2').on('click touchstart', function(e){
			e.stopPropagation();
		});

		$(document).on('click', function(){
			$(".level_1").slideUp("slow");
			$(".megamenu_mobile").find("h2").removeClass("active");
		});
	});



	// MAIN PRODUCT LISTING IMAGE CHANGE
	imgChange = function (){
		if ( device.desktop() ) {
			$(document).on({
			    mouseenter: function(){
			        $(this).find(".img__2").stop().animate({"opacity": 1});
			    },
			    mouseleave: function(){
			        $(this).find(".img__2").stop().animate({"opacity": 0});
			    }
			}, '.img_change');
		};
	};
	$(window).load( imgChange );



	// BACK TO TOP BUTTON 
	$(document).ready(function(){
		$(document.body).append('<a id="back_top" title="Back to top" href="#"></a>');
		$('#back_top').hide();

		$(window).scroll(function(){
			if ( $(this).scrollTop() > 300 ) {
				$('#back_top').fadeIn("slow");
			}
			else {
				$('#back_top').fadeOut("slow");
			};
		});

		$('#back_top').on('click', function(e) {
			e.preventDefault();
			$('html, body').animate({scrollTop : 0},800);
			$('#back_top').fadeOut("slow").stop();
		});
	});



	// PRODUCT QUANTITY FORM MINI, USED ON:
	// 1. PRODUCT PAGE
	// 2. PRODUCT QUICK VIEW
	// 3. CART PAGE
	$(document).on("focusout",".quantity_input",function(){var t=$(this).val();$(this).val(isNaN(parseFloat(t))&&!isFinite(t)||0==parseInt(t)||""==t?1:parseInt(t)<0?parseInt(t)-2*parseInt(t):parseInt(t))}),$(document).on("click",".quantity_up",function(){var t=$(this).parent().find(".quantity_input");t.val(!isNaN(parseFloat(t.val()))&&isFinite(t.val())?parseInt(t.val())+1:1)}),$(document).on("click",".quantity_down",function(){var t=$(this).parent().find(".quantity_input");t.val(!isNaN(parseFloat(t.val()))&&isFinite(t.val())&&t.val()>1?parseInt(t.val())-1:1)});




	// TEXT PAGES 
	$('.page_content table').find('td:first').css({'padding-left': 0});
	$('.page_content table').find('td:last').css({'padding-right': 0});




	// PRODUCT QUICK VIEW MINI
	//	$(document.body).on("click",".quick_view_btn",function(i){i.preventDefault(),$(document.body).append('<div id="product_quick_view" style="display: none;"><div class="product_quick_wrapper"><div class="quick_view__left"><div id="img_big"></div><div class="product_images"><div class="img_gallery"></div></div></div><div class="quick_view__right"><form action="/cart/add" method="post" enctype="multipart/form-data" id="product-actions" class="quick_view_form"><p id="quick_view__name" class="product_name"></p><p id="quick_view__type"><label for="">Product type:</label> <span></span></p><p id="quick_view__vendor"><label for="">Vendor:</label> <span></span></p><p id="quick_view__variants"><label for="">Options:</label><select id="product-select" name="id" class="hidden"></select></p><p id="quick_view__price" class="product_price"></p><p id="quick_view__availability"><label for="">Availability:</label> <span></span></p><div id="quick_view__form"><label for="quantity">Choose quantity:</label><div class="quantity_box"><input min="1" type="text" name="quantity" value="1" class="quantity_input" /><span class="quantity_modifier quantity_down"><i class="fa fa-minus"></i></span><span class="quantity_modifier quantity_up"><i class="fa fa-plus"></i></span></div><button class="btn btn-cart" type="submit" id="quick_view__add">Add to cart</button></div></form></div></div></div>'),$.fancybox.showLoading(),$.fancybox.helpers.overlay.open({parent:$("body")}),$.getJSON($(this).attr("href")+".js",function(i){if($(document).on("click","#product_quick_view .img_gallery a",function(i){i.preventDefault();var e=$(this).attr("href");$("#product_quick_view #img_big img").attr("src",e)}),i.title.length<60)var e=i.title;else var e=$.trim(i.title).substring(0,75)+"...";$("#quick_view__name").html('<a href="'+i.url+'">'+e+"</a>"),$("#quick_view__type span").html(i.type),$("#quick_view__vendor span").html(i.vendor),$.each(i.variants,function(i,e){$("#product-select").append('<option value="'+e.id+'">'+e.title+" - "+e.price+"</option>")}),$("#quantity").on("focusout",function(){var i=$(this).val();$(this).val(isNaN(parseFloat(i))&&!isFinite(i)||0==parseInt(i)||""==i?1:parseInt(i)<0?parseInt(i)-2*parseInt(i):parseInt(i))}),$("#quantity_up").on("click",function(){var i=$("#quantity").val();$("#quantity").val(!isNaN(parseFloat(i))&&isFinite(i)?parseInt(i)+1:1)}),$("#quantity_down").on("click",function(){var i=$("#quantity").val();$("#quantity").val(!isNaN(parseFloat(i))&&isFinite(i)&&i>1?parseInt(i)-1:1)}),$.getScript("https://cdn.shopify.com/s/assets/themes_support/option_selection-ab46215b6f146585c0b823a42613e4c1877e368805c0adec2adedf68286e502d.js",function(){function e(i,e){var a=i.length;0===a&&e();var t=0;$(i).each(function(){$("<img>").attr("src",this).load(function(){t++,t===a&&e()})})}e(i.images,function(){$("#product_quick_view #img_big").append('<img src="'+i.images[0]+'" alt="" />'),$.each(i.images,function(i,e){$("#product_quick_view .img_gallery").append('<a href="'+e+'"><img src="'+e+'" alt="" /></a>')}),$("#product_quick_view .img_gallery").bxSlider({infiniteLoop:!0,minSlides:1,maxSlides:3,moveSlides:1,slideWidth:94,pager:!1,prevText:"",nextText:""});var e=function(i){if(i&&i.available?(jQuery("#quick_view__add").removeAttr("disabled").removeClass("disabled"),jQuery("#quick_view__price").html(i.price<i.compare_at_price?'<span class="money">'+Shopify.formatMoney(i.price,"")+'</span><span class="money compare-at-price money_sale">'+Shopify.formatMoney(i.compare_at_price,"")+'</span><span class="money_sale_percent">– '+parseInt(100-100*i.price/i.compare_at_price)+"%</span>":'<span class="money">'+Shopify.formatMoney(i.price,"")+"</span>"),jQuery("#quick_view__availability span").removeClass("notify_danger").addClass("notify_success").html(null!=i.inventory_management?"<b>"+i.inventory_quantity+"</b> item(s)":"Available")):(jQuery("#quick_view__add").addClass("disabled").attr("disabled","disabled"),jQuery("#quick_view__availability span").removeClass("notify_success").addClass("notify_danger").html("Unavailable"),jQuery("#quick_view__price").html('<span class="money">'+Shopify.formatMoney(i.price,"")+"</span>")),i&&i.featured_image){var e=$("#img_big img"),a=i.featured_image,t=e[0];Shopify.Image.switchImage(a,t,function(i){$("#img_big img").attr("src",i)})}currencyToggle("#quick_view__price .money")};new Shopify.OptionSelectors("product-select",{product:i,onVariantSelected:e,enableHistoryState:!1}),$.each($("#quick_view__variants select option"),function(){"Default Title"==$(this).val()&&$("#quick_view__variants").hide()}),$.fancybox($("#product_quick_view"),{openSpeed:500,closeSpeed:500,tpl:{wrap:'<div id="quick_view__wrap" class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner"></div></div></div></div>',closeBtn:'<a title="Close" id="quick_view__close" class="fancybox-item fancybox-close" href="javascript:;"></a>'},afterClose:function(){$("#product_quick_view").remove()}})})}),$("#quick_view__add").on("click",function(){$.fancybox.close()})})});




	// PRODUCT QUICK VIEW
	$(document.body).on('click', '.quick_view_btn', function(e) {
		e.preventDefault();

	    // CONSTRUCTING QUICK VIEW MODAL
		$(document.body).append('<div id="product_quick_view" style="display: none;"><div class="product_quick_wrapper"><div class="quick_view__left"><div id="img_big"></div><div class="product_images"><div class="img_gallery"></div></div></div><div class="quick_view__right"><form action="/cart/add" method="post" enctype="multipart/form-data" id="product-actions" class="quick_view_form"><p id="quick_view__name" class="product_name"></p><p id="quick_view__type"><label for="">Product type:</label> <span></span></p><p id="quick_view__vendor"><label for="">Vendor:</label> <span></span></p><p id="quick_view__variants"><label for="">Options:</label><select id="product-select" name="id" class="hidden"></select></p><p id="quick_view__price" class="product_price"></p><p id="quick_view__availability"><label for="">Availability:</label> <span></span></p><div id="quick_view__form"><label for="quantity">Choose quantity:</label><div class="quantity_box"><input min="1" type="text" name="quantity" value="1" class="quantity_input" /><span class="quantity_modifier quantity_down"><i class="fa fa-minus"></i></span><span class="quantity_modifier quantity_up"><i class="fa fa-plus"></i></span></div><button class="btn btn-cart" type="submit" id="quick_view__add">Add to cart</button></div></form></div></div></div>');

	    // SHOWING FANCYBOX LOADING ANIMATION
    	$.fancybox.showLoading();
	    $.fancybox.helpers.overlay.open({parent: $('body')});

	    // GETTING PRODUCT INFO (JSON)
		$.getJSON( $(this).attr( 'href' ) + '.js', function( product ) {

			// TOGGLE BIG IMAGE WHEN CLICKING A THUMB
			$(document).on('click', '#product_quick_view .img_gallery a', function(e) {
				e.preventDefault();
				var newHREF = $(this).attr('href');
				$('#product_quick_view #img_big img').attr('src', newHREF );
			});

			// PRODUCT TITLE
	        if ( product.title.length < 60 ) {
	            var productTitle = product.title;
	        }
	        else {
	            var productTitle = $.trim( product.title ).substring(0, 75) + '...';
	        };
			$('#quick_view__name').html( '<a href="' + product.url + '">' + productTitle + '</a>' );

			// PRODUCT TYPE
			$('#quick_view__type span').html( product.type );

			// PRODUCT VENDOR
			$('#quick_view__vendor span').html( product.vendor );

			// PRODUCT VARIANTS
			$.each(product.variants, function(i, variant) {
				$('#product-select').append('<option value="' + variant.id + '">' + variant.title + ' - ' + variant.price + '</option>')
			});

			// QUANTITY FORM MINI
			$("#quantity").on("focusout",function(){var t=$(this).val();$(this).val(isNaN(parseFloat(t))&&!isFinite(t)||0==parseInt(t)||""==t?1:parseInt(t)<0?parseInt(t)-2*parseInt(t):parseInt(t))}),$("#quantity_up").on("click",function(){var t=$("#quantity").val();$("#quantity").val(!isNaN(parseFloat(t))&&isFinite(t)?parseInt(t)+1:1)}),$("#quantity_down").on("click",function(){var t=$("#quantity").val();$("#quantity").val(!isNaN(parseFloat(t))&&isFinite(t)&&t>1?parseInt(t)-1:1)});

			// UPLOADING option_selection.js TO MANAGE PRODUCT VARIANTS
			$.getScript( "https://cdn.shopify.com/s/assets/themes_support/option_selection-ab46215b6f146585c0b823a42613e4c1877e368805c0adec2adedf68286e502d.js", function() {

				// IMAGES PRELOADER (FUNCTION)
				function preloadImages(images, callback) {
				    var count = images.length;
				    if(count === 0) {
				        callback();
				    }
				    var loaded = 0;
				    $(images).each(function() {
				        $('<img>').attr('src', this).load(function() {
				            loaded++;
				            if (loaded === count) {
				                callback();
				            }
				        });
				    });
				};

				// IMAGES PRELOADER (INIT)
				preloadImages( product.images, function() {

					// APPENDING BIG IMAGE
					$('#product_quick_view #img_big').append( '<img src="' + product.images[0] + '" alt="" />' );

					// APPENDING ALL IMAGES TO GALLERY
					$.each(product.images, function(i, src) {
						$('#product_quick_view .img_gallery').append( '<a href="' + src + '"><img src="' + src + '" alt="" /></a>' );
					});

					// ADDING THUMBS SLIDER
					$('#product_quick_view .img_gallery').bxSlider({
						infiniteLoop: true,
						minSlides: 1,
						maxSlides: 3,
						moveSlides: 1,
						slideWidth: 94,
						pager: false,
						prevText: '',
						nextText: ''
					});

					// VARIANT CHANGE FUNCTION
					var selectCallback = function(variant, selector) {
						if ( variant && variant.available ) {

							jQuery('#quick_view__add').removeAttr('disabled').removeClass('disabled');

							// VARIANT PRICES
							if( variant.price < variant.compare_at_price ){
								jQuery('#quick_view__price').html('<span class="money">' + Shopify.formatMoney(variant.price, Currency.money_format[Currency.currentCurrency]) + '</span><span class="money compare-at-price money_sale">' + Shopify.formatMoney(variant.compare_at_price, Currency.money_format[Currency.currentCurrency]) + '</span><span class="money_sale_percent">– ' + parseInt( 100 - ( variant.price*100 )/variant.compare_at_price ) + '%</span>');
							}
							else {
								jQuery('#quick_view__price').html('<span class="money">' + Shopify.formatMoney(variant.price, Currency.money_format[Currency.currentCurrency]) + '</span>');
							};

							// PRODUCT QUANTITY
							if ( variant.inventory_management != null ) {
								jQuery('#quick_view__availability span').removeClass('notify_danger').addClass('notify_success').html( '<b>' + variant.inventory_quantity + '</b> item(s)' );
							}
							else {
								jQuery('#quick_view__availability span').removeClass('notify_danger').addClass('notify_success').html( 'Available' );
							};
						}
						else {
							jQuery('#quick_view__add').addClass('disabled').attr('disabled', 'disabled'); // set add-to-cart button to unavailable class and disable button

							jQuery('#quick_view__availability span').removeClass('notify_success').addClass('notify_danger').html( 'Unavailable' );

							jQuery('#quick_view__price').html('<span class="money">' + Shopify.formatMoney(variant.price, "") + '</span>');
						};

						// CHANGING VARIANT IMAGE
						if ( variant && variant.featured_image ) {
							var originalImage = $("#img_big img");
							var newImage = variant.featured_image;
							var element = originalImage[0];

							Shopify.Image.switchImage(newImage, element, function (newImageSizedSrc, newImage, element) {
								$('#img_big img').attr('src', newImageSizedSrc);
							});
						};

						currencyToggle('#quick_view__price .money');
					};

					// VARIANT CHANGE FUNCTION (INIT)
					new Shopify.OptionSelectors( "product-select",
					{
						product: product,
						onVariantSelected: selectCallback,
						enableHistoryState: false
					});

					// HIDING DEFAULT VARIANT SELECTOR
					$.each( $('#quick_view__variants select option'), function() {
						if ( $(this).val() == 'Default Title' ) {
							$('#quick_view__variants').hide();
						};
					});

					// SHOWING QUICK VIEW MODAL
					$.fancybox( $('#product_quick_view'),
						{
							'openSpeed': 500,
							'closeSpeed': 500,
							'tpl': {
								wrap: '<div id="quick_view__wrap" class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner"></div></div></div></div>',
								closeBtn : '<a title="Close" id="quick_view__close" class="fancybox-item fancybox-close" href="javascript:;"></a>',
							},
							'afterClose': function() {
								$('#product_quick_view').remove(); // REMOVING QUICK VIEW MODAL AFTER CLOSE
							}
						}
					);

				});

			});

			// CLOSING QUICK VIEW MODAL AFTER ADDING TO CART
			$('#quick_view__add').on('click', function() {
				$.fancybox.close();
			});

		});

	});








});