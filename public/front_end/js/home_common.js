jQuery(document).ready(function($) {
       // NIVOSLIDER
       $('.nivoSlider').nivoSlider({
         effect:'fade',
         animSpeed:500,
         pauseTime:7000,
         startSlide:0,
         pauseOnHover:true,
         directionNav:true,
         directionNavHide:false,
         controlNav:false
       });
   });

jQuery(document).ready(function($) {
      if( device.desktop() || device.tablet() ) {
         // $('#homepage_carousel__1').bxSlider({
         //    speed: 500,
         //    auto: true,
         //    pause: 4000,
         //    autoHover: true,
         //    infiniteLoop: true,
         //    hideControlOnEnd: true,
         //    minSlides: 4,
         //    maxSlides: 4,
         //    moveSlides: 1,
         //    slideMargin: 0,
         //    pager: false,
         //    prevText: '',
         //    nextText: '',
         // });

         // $('#homepage_carousel__2').bxSlider({
         //    speed: 500,
         //    auto: true,
         //    pause: 4000,
         //    autoHover: true,
         //    autoDirection: 'prev',
         //    infiniteLoop: true,
         //    hideControlOnEnd: true,
         //    minSlides: 4,
         //    maxSlides: 4,
         //    moveSlides: 1,
         //    slideMargin: 0,
         //    pager: false,
         //    prevText: '',
         //    nextText: '',
         // });
         $('.homepage_carousel__common').bxSlider({
            speed: 500,
            auto: true,
            pause: 4000,
            autoHover: true,
            infiniteLoop: true,
            hideControlOnEnd: true,
            minSlides: 4,
            maxSlides: 4,
            moveSlides: 1,
            slideMargin: 0,
            pager: false,
            prevText: '',
            nextText: '',
         });
         // $('#homepage_carousel__2').parent().parent().find('.bx-controls').hide();

         // var carousel_1_prevBtn = $('#homepage_carousel__1').parent().parent().find('.bx-controls .bx-prev');
         // var carousel_1_nextBtn = $('#homepage_carousel__1').parent().parent().find('.bx-controls .bx-next');

         // var carousel_2_prevBtn = $('#homepage_carousel__2').parent().parent().find('.bx-controls .bx-prev');
         // var carousel_2_nextBtn = $('#homepage_carousel__2').parent().parent().find('.bx-controls .bx-next');

         
         // carousel_1_prevBtn.on('click', function(){
         //    carousel_2_nextBtn.trigger( "click" );
         // });

         // carousel_1_nextBtn.on('click', function(){
         //    carousel_2_prevBtn.trigger( "click" );
         // });
         
      };
   });

 jQuery(document).ready(function($) {
      var titleHeight = $('.featured_products .page_heading').outerHeight();
      $('.featured_products .bx-controls').css({ 'top' : -((titleHeight - 20)/2 + 20) });
   });