/*!
 *
 *	Develop by Vinh Nguyễn - Ho Chi Minh City - 2017
 * 	Copyright Â© 2016-2017 Skyfire Team. All rights reserved.
 *	Design by Skyfire Team 2.3.3
 *
 *	Phone: 0975 578 276
 *	Skype: enjoyvinh
 *
 */

"use strict";

// app.js
// create angular app
var agencyApp = angular.module('agencyApp', ['commonApp', 'ui.bootstrap']);

agencyApp.factory('AgencyModels', function() {

	var model={
		datainit : {
			pageStatus : 'add',
			appURL : "http://localhost:8000"
		},
	};

	model.request = {
		listTurnovers : [],
		dtpTurnoverTime : moment().format("MM/YYYY")
	};

	return model;
});



// create angular controller
agencyApp.controller('agencyController', function(AgencyModels, commonFunction, $scope, $rootScope, $http, $filter, $window, $location, 
	$uibModal, toastr) {

	$scope.model = AgencyModels;

	$scope.pageInit = function() {

		$scope.model.datainit.appURL = $('meta[name="app_url"]').attr('content');

		$("#addReLoading").show();

	    if($("#txtId").val() !== "")
	    {
	    	var request = {
		        agency_id : $("#txtId").val(),
		    };

		    $http.post($scope.model.datainit.appURL + "/WebsiteApi/GetAgencyDetail", JSON.stringify(request))
		    .success(function (data, status, headers, config) {
		    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
	            {
	                toastr.error(data.messages, Languages.common_error_title);

	                if(data.auth != "" && data.auth != null  && data.auth != undefined)
	                {
	                	$window.location.href = $scope.model.datainit.appURL + "/dang-nhap";
	                }
	            }
		    	else if(data.error != "" && data.error != null  && data.error != undefined)
		    	{
		    		toastr.error(data.error, Languages.common_error_title);
		    		$window.location.href = $scope.model.datainit.appURL + "/dang-nhap";
		    	}
		    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
		    	{
		    		toastr.warning(data.warning, Languages.common_warning_title);
		    		$window.location.href = $scope.model.datainit.appURL + "/dang-nhap";
		    	}
		    	else if(data.success != "" && data.success != null  && data.success != undefined)
		    	{

					angular.forEach(data.listAgencyStatistic, function(value, key) {
						$scope.model.request.dtpTurnoverTime = value.statistic_month + "/" + value.statistic_year;
						$scope.model.request.txtTurnover = value.statistic_turnover;
						$scope.model.request.txtTurnoverPoints = value.statistic_turnover_points;
						$scope.model.request.txtPointsDisplayReached = value.statistic_points_display_reached;
						$scope.model.request.txtPointsDisplayNonReached = value.statistic_points_display_nonreached;
						$scope.addItemTurnover('pageinit');
					});

					$scope.model.datainit.pageStatus = "update";
		    	}
		    	else
		    	{
		    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
		    		{
		    			toastr.error(data.systemerror, Languages.common_error_title);
		    			window.location.replace("./"+data.errorCode);
		    		}
		    		else
		    		{
		    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
		    		}
		    	}

		    })
		    .error(function (data, status, header, config) {
		    	debugger;
				toastr.error(Languages.common_error_exception, Languages.common_error_title);
				$window.location.href = $scope.model.datainit.appURL + "/dang-nhap";
		    });
	    }
	};

	$scope.addItemTurnover = function(status) {

		if($scope.model.request.listTurnovers.length > 0)
		{
			var yearExists = 0;
			var monthExists = 0;

			angular.forEach($scope.model.request.listTurnovers, function(value, key) {
				if(value.year == moment("01/"+$scope.model.request.dtpTurnoverTime,"DD/MM/YYYY").year())
				{

					angular.forEach(value.revenueMonths, function(value2, key2) {
						if(value2.no == moment("01/"+$scope.model.request.dtpTurnoverTime,"DD/MM/YYYY").month()+1)
						{
							toastr.warning("Tháng "+moment("01/"+$scope.model.request.dtpTurnoverTime,"DD/MM/YYYY").month()+1+" Năm "+moment("01/"+$scope.model.request.dtpTurnoverTime,"DD/MM/YYYY").year()+" Đã Có Doanh số. Vui Lòng Chọn Tháng Khác", "Cảnh Báo");
							monthExists = 1;
						}
					});

					if(monthExists == 0)
					{
						value.total_turnover = value.total_turnover + $scope.model.request.txtTurnover;
						value.total_point = value.total_point + $scope.model.request.txtTurnoverPoints;

						var revenueMonth = {
		                	"no": moment("01/"+$scope.model.request.dtpTurnoverTime,"DD/MM/YYYY").month()+1,
		                    "turnover": $scope.model.request.txtTurnover,
		                    "turnover_points": $scope.model.request.txtTurnoverPoints,
		                    "points_display_reached": $scope.model.request.txtPointsDisplayReached,
		                    "points_display_nonreached": $scope.model.request.txtPointsDisplayNonReached,
		                    "total_point": $scope.model.request.txtPointsDisplayReached+$scope.model.request.txtTurnoverPoints - $scope.model.request.txtPointsDisplayNonReached,
		                };

		                value.revenueMonths.push(revenueMonth);
		                value.revenueMonths.sort();
		                yearExists = 1;
		            }
				}
			});

			if(yearExists == 0 && monthExists == 0)
			{
				var turnover = {
			        "year": moment("01/"+$scope.model.request.dtpTurnoverTime,"DD/MM/YYYY").year(),
			        "total_turnover": $scope.model.request.txtTurnover,
			        "total_point": $scope.model.request.txtTurnoverPoints,
			        "gold_nineth_month": 0,
			        "gold_twelfth_month": 0,
			        "revenueMonths": [
		                {
		                	"no": moment("01/"+$scope.model.request.dtpTurnoverTime,"DD/MM/YYYY").month()+1,
		                    "turnover": $scope.model.request.txtTurnover,
		                    "turnover_points": $scope.model.request.txtTurnoverPoints,
		                    "points_display_reached": $scope.model.request.txtPointsDisplayReached,
		                    "points_display_nonreached": $scope.model.request.txtPointsDisplayNonReached,
		                    "total_point": $scope.model.request.txtPointsDisplayReached+$scope.model.request.txtTurnoverPoints - $scope.model.request.txtPointsDisplayNonReached,
		                }]
		        };

		        $scope.model.request.listTurnovers.push(turnover);
		        $scope.model.request.listTurnovers.sort();
			}
		}
		else
		{
			$scope.model.request.listTurnovers = [{
		        "year": moment("01/"+$scope.model.request.dtpTurnoverTime,"DD/MM/YYYY").year(),
		        "total_turnover": $scope.model.request.txtTurnover,
		        "total_point": $scope.model.request.txtTurnoverPoints,
		        "gold_nineth_month": 0,
		        "gold_twelfth_month": 0,
		        "revenueMonths": [
	                {
	                	"no": moment("01/"+$scope.model.request.dtpTurnoverTime,"DD/MM/YYYY").month()+1,
	                    "turnover": $scope.model.request.txtTurnover,
	                    "turnover_points": $scope.model.request.txtTurnoverPoints,
	                    "points_display_reached": $scope.model.request.txtPointsDisplayReached,
	                    "points_display_nonreached": $scope.model.request.txtPointsDisplayNonReached,
	                    "total_point": $scope.model.request.txtPointsDisplayReached+$scope.model.request.txtTurnoverPoints - $scope.model.request.txtPointsDisplayNonReached,
	                }]
	        }];
		}
	};

});