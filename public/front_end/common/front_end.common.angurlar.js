/*!
 *
 *	Develop by Vinh Nguyễn - Ho Chi Minh City - 2017
 * 	Copyright Â© 2016-2017 Skyfire Team. All rights reserved.
 *	Design by Skyfire Team 2.3.3
 *
 *	Phone: 0975 578 276
 *	Skype: enjoyvinh
 *
 */

"use strict";

// app.js
// create angular app
var commonApp = angular.module('commonApp', ['ngRoute','toastr','ui.bootstrap','ngAnimate']);

commonApp.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});

commonApp.config(["$httpProvider", function($httpProvider) {
	$httpProvider.defaults.headers.post['X-CSRF-Token'] = $('meta[name="auth_token"]').attr('content');
	$httpProvider.defaults.headers.post['Content-Type'] = 'application/json;charset=utf-8;';
	$httpProvider.defaults.headers.post['Content-Encoding'] = 'gzip';
}]);

commonApp.config(function(toastrConfig) {
	angular.extend(toastrConfig, {
		allowHtml: false,
		closeButton: true,
		closeHtml: '<button>&times;</button>',
		extendedTimeOut: 1000,
		iconClasses: {
			error: 'toast-error',
			info: 'toast-info',
			success: 'toast-success',
			warning: 'toast-warning'
		},  
		messageClass: 'toast-message',
			onHidden: null,
			onShown: null,
			onTap: null,
			progressBar: true,
			tapToDismiss: true,
			templates: {
			toast: 'directives/toast/toast.html',
			progressbar: 'directives/progressbar/progressbar.html'
		},
		timeOut: 5000,
		titleClass: 'toast-title',
		toastClass: 'toast'
	});
});

commonApp.directive('onErrorSrc', function() {
    return {
    	restrict:"EA",
        transclude:true,
        link: function(scope, element, attrs) {
          element.bind('error', function() {
            if (attrs.src != attrs.onErrorSrc) {
              attrs.$set('src', attrs.onErrorSrc);
            }
          });
        }
    }
});


/* Format Date Time for Angularjs Directive DateTimePicker */
commonApp.directive('bindPicker', function() {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, element, attrs, ngModel) {
            function parser(value) {

                returnscope.$eval(attrs.bindDateTimePicker);
                console.log("test"+attrs.bindDateTimePicker);
            }
            ngModel.$parsers.push(parser);
        }
    };
});

/* Format always uppercase input and remove whitespace*/
commonApp.directive('capitalize', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, modelCtrl) {
            
            var capitalize = function(inputValue) {
                if (inputValue == undefined)
                {
                    inputValue = '';
                }

                var capitalized = inputValue.toUpperCase();

                if (capitalized !== inputValue) {
                    modelCtrl.$setViewValue(capitalized);
                    modelCtrl.$render();
                }
                return capitalized;
            }
            modelCtrl.$parsers.push(capitalize);
            capitalize(scope[attrs.ngModel]); // capitalize initial value
        }
    };
});


commonApp.service('commonFunction', function() {
    return {
		languageReplace: function(strMessage, arrParameter) {
			var result = "";
        	if(!strMessage.trim()){
        		return "";
        	}
        	if (arrParameter.length === 0) {
        		var count = occurrences(strMessage, "$"); 
        		for(i = 0; i < count; i++){
        			result = strMessage.replace("$"+i,"");
        		}
        	}else{
        		var i = 0;

        		angular.forEach(arrParameter, function(value){
					if(!value){
						result = strMessage.replace("$"+i,"");
					}else{
						result = strMessage.replace("$"+i,value);
					}
		       		i++;
	         	});
        	}
        	return result;
		}
    };
});


// create angular controller
// commonApp.controller('commonController', function($scope, $rootScope, $http, $filter, $window, $location) {

// });