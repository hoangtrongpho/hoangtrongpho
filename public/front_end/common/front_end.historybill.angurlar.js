/*!
 *
 *	Develop by Vinh Nguyễn - Ho Chi Minh City - 2017
 * 	Copyright Â© 2016-2017 Skyfire Team. All rights reserved.
 *	Design by Skyfire Team 2.3.3
 *
 *	Phone: 0975 578 276
 *	Skype: enjoyvinh
 *
 */

"use strict";

// app.JSON
// create angular app
var cartApp1 = angular.module('cartApp1', ['commonApp', 'ngSanitize',
		'datatables','datatables.colreorder','datatables.bootstrap']);

cartApp1.factory('NewsModels', function() {

	var model={

		datainit : {
			dtgListInvoice:[],
			appURL : "http://localhost:8000"
		},
	};

	return model;
});

// create angular controller
cartApp1.controller('cart1Controller', function(NewsModels, commonFunction, $scope, $rootScope, $http, $filter, $window, $location, toastr,
	DTOptionsBuilder, DTColumnDefBuilder,DTColumnBuilder) {

	$scope.model = NewsModels;

	$scope.pageInit = function() {
		//debugger;
		$scope.model.datainit.appURL = $('meta[name="app_url"]').attr('content');

		$scope.GetInvoiceByUser();

		$scope.dtOptions = DTOptionsBuilder.newOptions()
			// .withOption('order', [])
        	.withPaginationType('full_numbers')
        	.withDisplayLength(10)
	        // .withColReorder()
	        // .withColReorderOrder([0, 1, 2, 3, 4])
	        .withBootstrap()
	        .withBootstrapOptions({
	            pagination: {
	                classes: {
	                    ul: 'pagination pagination-md'
	                }
	            }
	        })
	        .withLanguage({
	            "sEmptyTable":     "Không có dữ liệu.",
	            "sInfo":           "Hiển Thị Từ _START_ Đến _END_ Của _TOTAL_ Kết Quả",
	            "sInfoEmpty":      "Hiển Thị Từ 0 Đến 0 Của 0 Kết Quả",
	            "sInfoFiltered":   "(Đã Lọc Từ Tổng _MAX_ Kết Quả)",
	            "sInfoPostFix":    "",
	            "sInfoThousands":  ",",
	            "sLengthMenu":     "Hiển Thị&nbsp;&nbsp;&nbsp;_MENU_&nbsp;&nbsp;&nbsp;Kết Quả / 1 Trang",
	            "sLoadingRecords": "Đang Tải Dữ Liệu...",
	            "sProcessing":     "Đang Xử Lý Dữ Liệu...",
	            "sSearch":         "Từ Khóa: ",
	            "sZeroRecords":    "Không tìm thấy dữ liệu trùng khớp.",
	            "oPaginate": {
	                "sFirst":    "Đầu",
	                "sLast":     "Cuối",
	                "sNext":     "Tiếp",
	                "sPrevious": "Trước"
	            },
	            "oAria": {
	                "sSortAscending":  ": activate to sort column ascending",
	                "sSortDescending": ": activate to sort column descending"
	            }
	        })
	        .withOption('responsive', true);

	    $scope.dtColumnDefs = [
	        DTColumnDefBuilder.newColumnDef(0).notSortable(),
	        DTColumnDefBuilder.newColumnDef(1).notSortable(),
	        DTColumnDefBuilder.newColumnDef(2).notSortable(),
	        DTColumnDefBuilder.newColumnDef(3).notSortable(),
	    ];
	};
	//========================
	$scope.GetInvoiceByUser = function() {
		$("#loader").show();
		var request = {
	    };
	    $http.post($scope.model.datainit.appURL + "/WebsiteApi/GetInvoiceByUser", JSON.stringify(request))
	    	.then(
               	function (response) {
                  	var data = response.data;
					if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, "Lỗi Hệ thông");
			    		$scope.model.datainit.customer_fullname = "";
			    		$scope.model.datainit.dtgListInvoice = [];
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		toastr.warning("Đơn Hàng Trống", "Cảnh Báo");
			    		$scope.model.datainit.customer_fullname = "";
			    		$scope.model.datainit.dtgListInvoice = [];
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		toastr.success("Lấy Dữ Liệu Thành Công", "Thông Báo");
			    		//alert(data.success);
			    		$scope.model.datainit.customer_fullname = data.customer_fullname;
			    		$scope.model.datainit.dtgListInvoice = data.success;
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error("Lỗi Hệ thông", "Lỗi Hệ thông");
			    			$scope.model.datainit.customer_fullname = "";
			    			$scope.model.datainit.dtgListInvoice = [];
			    		}
			    		else
			    		{
			    			toastr.error("Lỗi Hệ thông", "Lỗi Hệ thông");
			    			$scope.model.datainit.customer_fullname = "";
			    			$scope.model.datainit.dtgListInvoice = [];
			    		}
			    	}
			    	$("#loader").hide();
			    },
               	function (response) {
                  	$("#loader").hide();
                  	var data = response.data;
                  	$scope.model.datainit.customer_fullname = "";
			    	$scope.model.datainit.dtgListInvoice = [];
					toastr.error("Lỗi Hệ thông", "Lỗi Hệ thông");
			    }
			);
	}
	//=====loadInvoiceDetail============
	$scope.loadInvoiceDetail = function(invoiceId) {
		$("#loader").show();
		if(invoiceId !== "")
	    {
	    	var request = {
		        invoice_id : invoiceId,
		    };
		    //alert($("#txtSlug").val());

		    $http.post($scope.model.datainit.appURL + "/SystemApi/GetInvoiceDetail", JSON.stringify(request))
	    	.then(
               	function (response) {
                  	var data = response.data;

				    	if(data.error != "" && data.error != null  && data.error != undefined)
				    	{
				    		toastr.error(data.error, "Lỗi Hệ thông");
				    	}
				    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
				    	{
				    		toastr.warning(data.warning, "Cảnh Báo");
				    	}
				    	else if(data.success != "" && data.success != null  && data.success != undefined)
				    	{

				    		//debugger;
				    		$scope.model.request = {
								txtCustomerId: data.success.customer_id,
					    		txtCustomerEmail :data.success.customer_email,
					    		txtCustomerPhone :data.success.customer_phone,
					    		txtCustomerName: data.success.customer_fullname,
					    		txtCustomerAddress: data.success.customer_address,
					    		txtCustomerAccount :data.success.customer_account,
					    		txtCustomerDistrict: data.success.district_name,
					    		txtReceivedName: data.success.receiver_name,
								txtReceivedPhone: data.success.receiver_phone,
								txtReceivedAddress: data.success.receiver_address,
								txtUpdatedDate: data.success.updated_date_format,

								txtBranchName: data.success.branch_name,
								txtBranchPhone: data.success.branch_phone,
								txtBranchAddress: data.success.branch_address_number,

								txtInvoiceId:data.success.invoice_id,
								txtInvoiceNumber: data.success.invoice_number,
								txtTotalSum : data.success.total_sum,
								dtpDeliverDate : moment(data.success.delivery_date).format("DD/MM/YYYY"),
								txtInvoiceNote: data.success.invoice_note,
								swExportInvoiceStatus : data.success.export_invoice_status,
								radioStatus: data.success.status.toString(),
							};
							if(data.success.payment_method==null){
								$scope.model.request.radioPaymentMethod="1";
							}
							else{
								$scope.model.request.radioPaymentMethod=data.success.payment_method.toString();
							}
							

							angular.forEach($scope.model.datainit.slbCustomer, function(value, key2) {
							    if(value.customer_id == data.success.customer_id)
							    {
							    	value.ticked = true;
							    }
							});
							$scope.model.request.dtgListProductInvoice=data.product_list;

							$scope.model.datainit.pageStatus = "update";
				    	}
				    	else
				    	{
				    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
				    		{
				    			toastr.error(data.systemerror, "Lỗi Hệ thông");
				    		}
				    		else
				    		{
				    			toastr.error("Lỗi Hệ thông", "Lỗi Hệ thông");
				    		}
				    	}
				    	$("#loader").hide();
			    },
               	function (response) {
                  	$("#loader").hide();
                 	var data = response.data;
					toastr.error("Lỗi Hệ thông", "Lỗi Hệ thông");
			    }
			);
	    }
	}
	//=====removeItem =====
	$scope.removeItem = function(id) {
		//alert(id);
		$("#loader").show();
    	if(id == "" || id == null || id == undefined)
		{
			toastr.error("Resources.common_error_exception", "Resources.common_error_title");
			return false;
		}

    	var request = {
	        invoice_id : id,
	    };

	    $http.post($scope.model.datainit.appURL + "/SystemApi/RemoveInvoice", JSON.stringify(request))
	    	.then(
					function (response) {
  					var data = response.data;

					if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, "Lỗi Hệ thông");
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		toastr.warning(data.warning, "Cảnh Báo");
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		toastr.success("Xóa Thành Công","Thông Báo");
			    		$scope.pageInit();
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, "Lỗi Hệ thông");
			    		}
			    		else
			    		{
			    			toastr.error("Lỗi Hệ thông", "Lỗi Hệ thông");
			    		}
			    	}
			    	$("#loader").hide();
	    		},
	    		function (response) {
                 	debugger;
                  	var data = response.data;	
					toastr.error("Lỗi Hệ thông", "Lỗi Hệ thông");
					$("#loader").hide();
	    		}
	    	);
    };

});