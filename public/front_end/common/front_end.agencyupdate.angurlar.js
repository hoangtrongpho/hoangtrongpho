/*!
 *
 *	Develop by Vinh Nguyễn - Ho Chi Minh City - 2017
 * 	Copyright Â© 2016-2017 Skyfire Team. All rights reserved.
 *	Design by Skyfire Team 2.3.3
 *
 *	Phone: 0975 578 276
 *	Skype: enjoyvinh
 *
 */

"use strict";

// app.js
// create angular app
var agencyApp = angular.module('agencyApp', ['commonApp', 'ui.bootstrap.datetimepicker', 'bootstrap-switch','isteven-multi-select','ui.bootstrap']);

agencyApp.factory('AgencyModels', function() {

	var model={
		datainit : {
			slbListProvinces : [],
			pageStatus : 'add',
			appURL : "http://localhost:8000"
		},
	};

	model.request = {
		dtpJoined: moment().format("DD/MM/YYYY"),
		dtpBirthDay : moment().format("DD/MM/YYYY"),
		txtId : "",
		txtCode : "",
		txtEmail : "",
		txtAgencyName : "",
		txtFullName : "",
		txtPhone : "",
		txtAddress : "",
		slbProvinces : "",
		txtProvinceId : "",
		sblPromotionType : 2,
	};

	model.localLang = {
	    selectAll       : Languages.common_button_select_all,
	    selectNone      : Languages.common_button_deselected_all,
	    reset           : Languages.common_button_cancel,
	    search          : Languages.common_input_label_search,
	    nothingSelected : "Chọn Khu Vực Đại Lý Kinh Doanh"         //default-label is deprecated and replaced with this.
	}

	model.singleDatePickerOpts = {
    	minView : 'day',
    	startView : 'day',
    	modelType : 'DD/MM/YYYY',
    };

	return model;
});



// create angular controller
agencyApp.controller('agencyController', function(AgencyModels, commonFunction, $scope, $rootScope, $http, $filter, $window, $location, $uibModal, toastr) {

	$scope.model = AgencyModels;

	$scope.slbProvincesClick = function(item)
	{
	   	$('#txtProvinceId').val(item.province_id); 
   		$('#txtProvinceId').trigger('input');

   		angular.forEach($scope.model.datainit.slbListProvinces, function(value, key) {
		    if(value.province_id == item.province_id)
		    {
		    	value.ticked = true;
		    }
		    else
		    {
		    	value.ticked = false;
		    }
		});
	};

	$scope.formReset = function() {

		$("#pagePreLoading").show();
		$("#addReLoading").show();

		$scope.model.request = {
			dtpJoined: moment().format("DD/MM/YYYY"),
			dtpBirthDay : moment().format("DD/MM/YYYY"),
			txtId : "",
			txtEmail : "",
			txtAgencyName : "",
			txtFullName : "",
			txtPhone : "",
			txtAddress : "",
			slbProvinces : "",
			txtProvinceId : "",
			sblPromotionType : 2,
		};

		angular.forEach($scope.model.datainit.slbListProvinces, function(value, key) {
			value.ticked = false;
		});

		$scope.frmAdd.$setPristine();
		$scope.frmAdd.$setUntouched();

		$scope.pageInit();

		$("#addReLoading").hide();

	};

	$scope.pageInit = function() {

		$scope.model.datainit.appURL = $('meta[name="app_url"]').attr('content');

		$("#addReLoading").show();

    	$http.post($scope.model.datainit.appURL + "/WebsiteApi/GetListProvinces")
	    .success(function (data, status, headers, config) {
	    	//debugger;
	    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
            {
                toastr.error(data.messages, Languages.common_error_title);

                if(data.auth != "" && data.auth != null  && data.auth != undefined)
                {
                	$window.location.href = $scope.model.datainit.appURL + "/dang-nhap";
                }
            }
	    	if(data.error != "" && data.error != null  && data.error != undefined)
	    	{
	    		$scope.model.datainit.slbListProvinces = [];
	    		toastr.error(data.error, Languages.common_error_title);
	    	}
	    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
	    	{
	    		$scope.model.datainit.slbListProvinces = [];
	    		toastr.warning(data.warning, Languages.common_warning_title);
	    	}
	    	else if(data.success != "" && data.success != null  && data.success != undefined)
	    	{
	    		$scope.model.datainit.slbListProvinces = data.success;
	    	}
	    	else
	    	{
	    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
	    		{
	    			toastr.error(data.systemerror, Languages.common_error_title);
	    			window.location.replace("./"+data.errorCode);
	    		}
	    		else
	    		{
	    			$scope.model.datainit.slbListProvinces = [];
	    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
	    		}
	    	}
	    })
	    .error(function (data, status, header, config) {
	    	//debugger;
	    	toastr.error(Languages.common_error_exception, Languages.common_error_title);
			window.location.replace($scope.model.datainit.appURL + "/dang-nhap");
	    });

	    if($("#txtId").val() !== "")
	    {
	    	var request = {
		        agency_id : $("#txtId").val(),
		    };

		    $http.post($scope.model.datainit.appURL + "/WebsiteApi/GetAgencyDetail", JSON.stringify(request))
		    .success(function (data, status, headers, config) {
		    	//debugger;
		    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
	            {
	                toastr.error(data.messages, Languages.common_error_title);

	                if(data.auth != "" && data.auth != null  && data.auth != undefined)
	                {
	                	$window.location.href = $scope.model.datainit.appURL + "/dang-nhap";
	                }
	            }
		    	if(data.error != "" && data.error != null  && data.error != undefined)
		    	{
		    		toastr.error(data.error, Languages.common_error_title);
		    		window.location.replace($scope.model.datainit.appURL + "/dang-nhap");
		    	}
		    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
		    	{
		    		toastr.warning(data.warning, Languages.common_warning_title);
		    		window.location.replace($scope.model.datainit.appURL + "/dang-nhap");
		    	}
		    	else if(data.success != "" && data.success != null  && data.success != undefined)
		    	{
		    		$scope.model.request = {
						dtpJoined : moment(data.success.agency_joined_date).format("DD/MM/YYYY"),
						dtpBirthDay : moment(data.success.agency_owner_birthday).format("DD/MM/YYYY"),
						txtId : data.success.agency_id,
						txtEmail : data.success.agency_owner_email,
						txtAgencyName : data.success.agency_name,
						txtPassword : "",
						txtConfirmPassword : "",
						txtFullName : data.success.agency_owner_full_name,
						txtPhone : data.success.agency_phone,
						txtAddress : data.success.agency_address_number,
						slbProvinces : { "province_id" : data.success.agency_provinces },
						txtProvinceId : data.success.agency_provinces,
						sblPromotionType : data.success.agency_promotion_type,
					};

					angular.forEach($scope.model.datainit.slbListProvinces, function(value, key) {
					    if(value.province_id == data.success.agency_provinces)
					    {
					    	value.ticked = true;
					    }
					    else
					    {
					    	value.ticked = false;
					    }
					});

					$('#txtProvinceId').val(data.success.agency_provinces); 
   					$('#txtProvinceId').trigger('input');

					$scope.model.datainit.pageStatus = "update";
		    	}
		    	else
		    	{
		    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
		    		{
		    			toastr.error(data.systemerror, Languages.common_error_title);
		    			window.location.replace("./"+data.errorCode);
		    		}
		    		else
		    		{
		    			$scope.model.datainit.slbListProvinces = [];
		    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
		    		}
		    	}

		    })
		    .error(function (data, status, header, config) {
		    	//debugger;
				toastr.error(Languages.common_error_exception, Languages.common_error_title);
				window.location.replace($scope.model.datainit.appURL + "/dang-nhap");
		    });
	    }
	};

	$scope.updateItemAgency = function(status) {
		var confirmModal = bootbox.dialog({
		  	message: commonFunction.languageReplace(Languages.common_confirm_update_question,[' Đại Lý']),
		  	title: commonFunction.languageReplace(Languages.common_confirm_update_title,[' Đại Lý']),
		  	closeButton: true,
		  	animate: true,
		  	className: "confirmModal",
		  	backdrop: true,
		  	headerBackground: "bg-yellow",
		  	size: "small",
		  	locale: "vi",
		  	show: true,
		  	buttons: {
		  		cancel : {
			      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
			      	className: "btn-default",
			      	callback: function() {
			        	confirmModal.modal('hide');
			      	}
			    },
			    success: {
		      		label: '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;'+Languages.common_button_update,
			      	className: "btn-warning",
			      	callback: function() {

			      		$("#addReLoading").show();

					    var request = {
					    	agency_id : $scope.model.request.txtId,
					        agency_name : $scope.model.request.txtAgencyName,
					        agency_address_number : $scope.model.request.txtAddress,
					        agency_provinces : $scope.model.request.txtProvinceId,
					        agency_districts : 1,
					        agency_wards : 1,
					        agency_phone : $scope.model.request.txtPhone,
					        agency_promotion_type : $scope.model.request.sblPromotionType,
					        agency_owner_full_name : $scope.model.request.txtFullName,
					        agency_owner_birthday : moment($scope.model.request.dtpBirthDay,'DD/MM/YYYY'),
					        agency_owner_email : $scope.model.request.txtEmail,
					    };

					    $http.post($scope.model.datainit.appURL + "/WebsiteApi/UpdateAgency", JSON.stringify(request))
					    .success(function (data, status, headers, config) {
					    	//debugger;
					    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
				            {
				                toastr.error(data.messages, Languages.common_error_title);

				                if(data.auth != "" && data.auth != null  && data.auth != undefined)
				                {
				                	$window.location.href = $scope.model.datainit.appURL + "/dang-nhap";
				                }
				            }
					    	if(data.error != "" && data.error != null  && data.error != undefined)
					    	{
					    		toastr.error(data.error, Languages.common_error_title);
					    	}
					    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
					    	{
					    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
					    		warning = warning.replace('["', '').replace('"]', '');
					    		toastr.warning(warning, Languages.common_warning_title);
					    	}
					    	else if(data.success != "" && data.success != null  && data.success != undefined)
					    	{
					    		toastr.success(data.success, commonFunction.languageReplace(Languages.common_success_title,[' Đại Lý']));
					    	}
					    	else
					    	{
					    		toastr.error(Languages.common_error_exception, Languages.common_error_title);
					    	}
					    	$("#addReLoading").hide();
					    })
					    .error(function (data, status, header, config) {
					    	//debugger;
							toastr.error(Languages.common_error_exception, Languages.common_error_title);
							$window.location.href = $scope.model.datainit.appURL + "/dang-nhap";
					    });
			      	}
			    },
			    
		  	}
		});
	};

});