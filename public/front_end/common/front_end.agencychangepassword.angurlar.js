/*!
 *
 *	Develop by Vinh Nguyễn - Ho Chi Minh City - 2017
 * 	Copyright Â© 2016-2017 Skyfire Team. All rights reserved.
 *	Design by Skyfire Team 2.3.3
 *
 *	Phone: 0975 578 276
 *	Skype: enjoyvinh
 *
 */

"use strict";

// app.js
// create angular app
var agencyApp = angular.module('agencyApp', ['commonApp', 'ui.bootstrap']);

agencyApp.factory('AgencyModels', function() {

	var model={
		datainit : {
			appURL : "http://localhost:8000"
		},
	};

	model.request = {
		txtAgencyName : "",
		txtPassword : "",
	};
	return model;
});

// create angular controller
agencyApp.controller('agencyController', function(AgencyModels, commonFunction, $scope, $rootScope, $http, $filter, $window, $location, $uibModal, toastr) {

	$scope.model = AgencyModels;


	$scope.formReset = function() {

		$("#pagePreLoading").show();

		$scope.model.request = {
			txtPassword : "",
			txtConfirmPassword : "",
		};

		$scope.frmAdd.$setPristine();
		$scope.frmAdd.$setUntouched();

		$scope.pageInit();
	};

	$scope.pageInit = function() {
		
		$scope.model.datainit.appURL = $('meta[name="app_url"]').attr('content');
	};

	$scope.updateItemAgency = function(status) {

		$("#btnUpdate").button('loading');

		var confirmModal = bootbox.dialog({
		  	message: commonFunction.languageReplace(Languages.common_confirm_update_question,[' Mật Khẩu']),
		  	title: commonFunction.languageReplace(Languages.common_confirm_update_title,[' Mật Khẩu']),
		  	closeButton: true,
		  	animate: true,
		  	className: "confirmModal",
		  	backdrop: true,
		  	headerBackground: "bg-yellow",
		  	size: "small",
		  	locale: "vi",
		  	show: true,
		  	buttons: {
		  		cancel : {
			      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
			      	className: "btn-default",
			      	callback: function() {
			        	confirmModal.modal('hide');
			      	}
			    },
			    success: {
		      		label: '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;'+Languages.common_button_update,
			      	className: "btn-warning",
			      	callback: function() {

			      		$("#addReLoading").show();

					    var request = {
					    	agency_id : $("#txtId").val(),
					    	agency_password : $scope.model.request.txtPassword,
					        agency_password_confirm : $scope.model.request.txtConfirmPassword,
					    };

					    $http.post($scope.model.datainit.appURL + "/WebsiteApi/ChangePassword", JSON.stringify(request))
					    .success(function (data, status, headers, config) {

					    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
				            {
				                toastr.error(data.messages, Languages.common_error_title);

				                if(data.auth != "" && data.auth != null  && data.auth != undefined)
				                {
				                	$window.location.href = $scope.model.datainit.appURL + "/dang-nhap";
				                }
				            }
					    	if(data.error != "" && data.error != null  && data.error != undefined)
					    	{
					    		toastr.error(data.error, Languages.common_error_title);
					    		$scope.formReset();
					    		$("#addReLoading").hide();
					    		$("#btnUpdate").button('reset');
					    	}
					    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
					    	{
					    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
					    		warning = warning.replace('["', '').replace('"]', '');
					    		toastr.warning(warning, Languages.common_warning_title);
					    		$scope.formReset();
					    		$("#addReLoading").hide();
					    		$("#btnUpdate").button('reset');
					    	}
					    	else if(data.success != "" && data.success != null  && data.success != undefined)
					    	{
					    		toastr.success(data.success, commonFunction.languageReplace(Languages.common_success_title,[' Mật Khẩu']));
					    		$scope.formReset();
					    		$("#addReLoading").hide();
					    		$("#btnUpdate").button('reset');
					    	}
					    	else
					    	{
					    		toastr.error(Languages.common_error_exception, Languages.common_error_title);
					    		$scope.formReset();
					    		$("#addReLoading").hide();
					    		$("#btnUpdate").button('reset');
					    	}
					    })
					    .error(function (data, status, header, config) {
							toastr.error(Languages.common_error_exception, Languages.common_error_title);
							$window.location.href = $scope.model.datainit.appURL + "/dang-nhap";
					    });
			      	}
			    },
			    
		  	}
		});
	};

});