/*!
 *
 *	Develop by Vinh Nguyễn - Ho Chi Minh City - 2017
 * 	Copyright Â© 2016-2017 Skyfire Team. All rights reserved.
 *	Design by Skyfire Team 2.3.3
 *
 *	Phone: 0975 578 276
 *	Skype: enjoyvinh
 *
 */

"use strict";

// app.js
// create angular app
var leftMenuApp = angular.module('leftMenuApp', []);

leftMenuApp.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});

leftMenuApp.factory('LeftMenuModels', function() {

	var model={
		datainit : {
			listFunctions : [
				{
					name	: "Dashboard",
					title 	: "Dashboard",
					url 	: "/System/Dashboard",
					icon 	: "fa-dashboard",
					tag		: "Dashboard - Tổng Quan"
				},
				{
					name 	: "ListNewsCategories",
					title 	: "Danh Mục Thể Loại",
					url 	: "/System/ListNewsCategories",
					icon 	: "fa-cubes",
					tag		: "Danh Mục Thể Loại Tin Tức - List News Categories"
				},
				{
					name 	: "ListNews",
					title 	: "Danh Mục Tin Tức",
					url 	: "/System/ListNews",
					icon 	: "fa-cubes",
					tag		: "Quản Trị Danh Mục Tin Tức - List Categories"
				},
				{
					name 	: "GalleryLirary",
					title 	: "Thư Viện Ảnh",
					url 	: "/System/GalleryLirary",
					icon 	: "fa-picture-o",
					tag		: "Quản Trị Thư Viện Ảnh - Images Gallery Lirary"
				},
			],
		},
	};

	model.request = {
		txtKeyword : "",
	};

	return model;
});

leftMenuApp.filter('searchFunctions', function(){
    return function(arr, keyWord){

        if(!keyWord){
        	$("#resultFunctionSearch").hide();
        	$("#menuFunction").show();
        	$("#search-btn").show();
        	$("#clearKeyWord").hide();
            return arr;
        }
        var result = [];

        keyWord = keyWord.toLowerCase();

        angular.forEach(arr, function(item){
            if(item.tag.toLowerCase().indexOf(keyWord) !== -1){
            	result.push(item);
        	}
        });

        $("#menuFunction").hide();
        $("#resultFunctionSearch").show();
        $("#search-btn").hide();
        $("#clearKeyWord").show();
        return result;
    };
});

// create angular controller
leftMenuApp.controller('leftMenuController', function(LeftMenuModels, $scope) {

	$scope.model = LeftMenuModels;

    $scope.clearKeyWord = function() {
    	$scope.model.request.txtKeyword = "";
    }
	

});

