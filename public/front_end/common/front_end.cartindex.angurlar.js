/*!
 *
 *	Develop by Vinh Nguyễn - Ho Chi Minh City - 2017
 * 	Copyright Â© 2016-2017 Skyfire Team. All rights reserved.
 *	Design by Skyfire Team 2.3.3
 *
 *	Phone: 0975 578 276
 *	Skype: enjoyvinh
 *
 */

"use strict";

// app.JSON
// create angular app
var cartApp = angular.module('cartApp', ['commonApp', 'daterangepicker', 'isteven-multi-select', 'ngSanitize',
		'datatables','datatables.colreorder','datatables.bootstrap']);

cartApp.factory('NewsModels', function() {

	var model={

		datainit : {
			slbBranch:[],
			customer_fullname:"",
			dtgListInvoice:[],
			customerInfoStatus:"",
			dtgListCart:[],
			totalSum:0,
			quantitySum:0,
			cartCount: 1000,
			appURL : "http://localhost:8000"
		},
	};

	model.request = {
		txtBranchId:"",
		txtBranchName:"",
		txtBranchPhone:"",
		txtBranchAddress:"",
		slbBranch:[],
		txtProductId:"",
		txtQuantity: 1,

		txtInvoiceId:"",
		dtpDeliverDate : moment().format("DD/MM/YYYY"),
		txtInvoiceNote:"",
		swExportInvoiceStatus : 0,
		txtInvoiceNumber:"",
		radioStatus: "2",
		radioPaymentMethod:"1",
		txtTotalSum:0,
		dtgListProduct: [],
		dtgListProductInvoice: [],
		txtReceivedName:"",
		txtReceivedPhone: "",
		txtReceivedAddress:"",
		txtCustomerDistrict:"",
		txtCustomerId:"",
		txtCustomerEmail:"",
		txtCustomerPhone:"",
		txtCustomerGroup:"",
		txtCustomerName:"",
		txtCustomerAddress:"",

		txtCustomerAccount:"",
		pageStatus:"",
		txtImages : "/public/UserPhotos/public/UserPhotos/SharePhotos/58b989e59fa96.png",
		slbCustomer : []
	};

	model.localLang = {
	    selectAll       : "Chọn Tất Cả",
	    selectNone      : "Hủy Chọn",
	    reset           : "Reset",
	    search          : "Tìm",
	    nothingSelected : "<span style='color:red'>Vui Lòng Chọn Chi Nhánh Nhận Hàng<span>"
	}
	return model;
});

// create angular controller
cartApp.controller('cartController', function(NewsModels, commonFunction, $scope, $rootScope, $http, $filter, $window, $location, toastr,
	DTOptionsBuilder, DTColumnDefBuilder,DTColumnBuilder) {

	$scope.model = NewsModels;

	$scope.pageInit = function() {
		//debugger;
		$scope.model.datainit.appURL = $('meta[name="app_url"]').attr('content');
		
		$("#loader").show();
		$scope.loadCart();
		$scope.GetBranchList();
		
		$scope.dtOptions = DTOptionsBuilder.newOptions()
			// .withOption('order', [])
        	.withPaginationType('full_numbers')
        	.withDisplayLength(10)
	        // .withColReorder()
	        // .withColReorderOrder([0, 1, 2, 3, 4])
	        .withBootstrap()
	        .withBootstrapOptions({
	            pagination: {
	                classes: {
	                    ul: 'pagination pagination-md'
	                }
	            }
	        })
	        .withLanguage({
	            "sEmptyTable":     "Không có dữ liệu.",
	            "sInfo":           "Hiển Thị Từ _START_ Đến _END_ Của _TOTAL_ Kết Quả",
	            "sInfoEmpty":      "Hiển Thị Từ 0 Đến 0 Của 0 Kết Quả",
	            "sInfoFiltered":   "(Đã Lọc Từ Tổng _MAX_ Kết Quả)",
	            "sInfoPostFix":    "",
	            "sInfoThousands":  ",",
	            "sLengthMenu":     "Hiển Thị&nbsp;&nbsp;&nbsp;_MENU_&nbsp;&nbsp;&nbsp;Kết Quả / 1 Trang",
	            "sLoadingRecords": "Đang Tải Dữ Liệu...",
	            "sProcessing":     "Đang Xử Lý Dữ Liệu...",
	            "sSearch":         "Từ Khóa: ",
	            "sZeroRecords":    "Không tìm thấy dữ liệu trùng khớp.",
	            "oPaginate": {
	                "sFirst":    "Đầu",
	                "sLast":     "Cuối",
	                "sNext":     "Tiếp",
	                "sPrevious": "Trước"
	            },
	            "oAria": {
	                "sSortAscending":  ": activate to sort column ascending",
	                "sSortDescending": ": activate to sort column descending"
	            }
	        })
	        .withOption('responsive', true);

	    $scope.dtColumnDefs = [
	        DTColumnDefBuilder.newColumnDef(0).notSortable(),
	        DTColumnDefBuilder.newColumnDef(1).notSortable(),
	        DTColumnDefBuilder.newColumnDef(2).notSortable(),
	        DTColumnDefBuilder.newColumnDef(3).notSortable(),
	    ];
	};
	$scope.generateCode= function(){
		var currentdate = new Date(); 
    	var datetime =String(currentdate.getDate())
                + String((currentdate.getMonth()+1))
                + String(currentdate.getFullYear())
                + String(currentdate.getHours())
                + String(currentdate.getMinutes())
                + String(currentdate.getSeconds());
        return datetime;
	}
	//saveInvoiceSession
	$scope.saveInvoiceSession= function(){
		if($scope.model.request.txtBranchName==null || $scope.model.request.txtBranchName==""){
			
			toastr.warning("Vui Lòng Chọn Chi Nhánh Nhận Hàng", "Cảnh Báo");
			return false;
		}
		var request = {
	        invoice_number : "HD"+$scope.generateCode(),
	        branch_id : $scope.model.request.txtBranchId,
	    };
		$http.post($scope.model.datainit.appURL + "/WebsiteApi/SaveInvoiceSession", JSON.stringify(request))
	    	.then(
               	function (response) {
                  	var data = response.data;

			    	if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, "Lỗi Hệ thông");
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		toastr.warning(data.warning, "Cảnh Báo");
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		toastr.success("thông báo", data.success);
		
						setTimeout(function(){ 
							window.location.replace($scope.model.datainit.appURL + "/lich-su-don-hang");
						}, 500);
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error("Lỗi Hệ thông", "Lỗi Hệ thông");
			    		}
			    		else
			    		{
			    			toastr.error("Lỗi Hệ thông", "Lỗi Hệ thông");
			    		}
			    	}
			    },
               	function (response) {
                  	var data = response.data;
					toastr.error("Lỗi Hệ thông", "Lỗi Hệ thông");
			    }
			);
	}
	//addCart
	$scope.addCart = function(product_id) {
		//alert(product_id);
		if(product_id == "" || product_id == null || product_id == undefined)
		{
			toastr.error(Resources.common_error_exception, Resources.common_error_title);
			return false;
		}
		//alert(product_id+"===>"+$scope.model.request.txtQuantity);
    	var request = {
	        product_id : product_id,
	        quantity : $scope.model.request.txtQuantity
	    };
		$http.post($scope.model.datainit.appURL + "/WebsiteApi/AddCart", JSON.stringify(request))
	    	.then(
               	function (response) {
                  	var data = response.data;

			    	if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, "Lỗi Hệ thông");
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		toastr.warning(data.warning, "Cảnh Báo");
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		toastr.success("Thông Báo", data.success);
			    		//alert(data.success);
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error("Lỗi Hệ thông", "Lỗi Hệ thông");
			    		}
			    		else
			    		{
			    			toastr.error("Lỗi Hệ thông", "Lỗi Hệ thông");
			    		}
			    	}
			    },
               	function (response) {
                  	debugger;
                  	var data = response.data;
					toastr.error("Lỗi Hệ thông", "Lỗi Hệ thông");
			    }
			);
	}
	$scope.loadCart = function() {
	
		//alert(product_id+"===>"+$scope.model.request.txtQuantity);
    	var request = {
	    };
		$http.post($scope.model.datainit.appURL + "/WebsiteApi/loadCart", JSON.stringify(request))
	    	.then(
               	function (response) {
                  	var data = response.data;

			    	if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, "Lỗi Hệ thông");
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		toastr.warning("Thông Báo", "Giỏ Hàng Trống");
			    		$scope.model.datainit.totalSum=0;
			    		$scope.model.datainit.quantitySum=0;
			    		$scope.model.datainit.cartCount=0;
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		$scope.model.datainit.dtgListCart=data.success;
			    		$scope.model.datainit.totalSum=data.total_sum;
			    		$scope.model.datainit.quantitySum=data.quantity_sum;
			    		$scope.model.datainit.cartCount=data.cart_count;
			    		$scope.model.datainit.customerInfoStatus=data.customerInfo_status;

			    		$("#divCartDeatil").show();
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error("Lỗi Hệ thông", "Lỗi Hệ thông");
			    			$scope.model.datainit.totalSum=0;
				    		$scope.model.datainit.quantitySum=0;
				    		$scope.model.datainit.cartCount=0;
			    		}
			    		else
			    		{
			    			toastr.error("Lỗi Hệ thông", "Lỗi Hệ thông");
			    			$scope.model.datainit.totalSum=0;
				    		$scope.model.datainit.quantitySum=0;
				    		$scope.model.datainit.cartCount=0;
			    		}
			    	}
			    	$("#loader").hide();
			    },
               	function (response) {
                  	var data = response.data;
					toastr.error("Lỗi Hệ thông", "Lỗi Hệ thông");
			    }
			);
	}
	//=====================updateInvoiceQuan remove cart===============
	$scope.updateQuantity = function(pro_id,delete_flag)
	{
		$("#loader").show();

		var new_quan= $("#pro_"+pro_id).val();
		if(new_quan < 0 || new_quan > 200 || new_quan === undefined || new_quan === null)
		{
			new_quan = 1;
		}
		//tao cờ:
		var flag=0;var newpro_quan;var removed=0;
		$.each( $scope.model.datainit.dtgListCart, function( i ) {
			$.each( $scope.model.datainit.dtgListCart[i], function (key, val) {
				
		        if(key == "product_id"){
		        	// nếu trùng săn phẩm > break vòng lặp/ cập nhật SL
		        	if(val == pro_id){
		        		flag=1;
		        	}
		        }
		        // cap nhat thanh tien= sl x don gia
		        if(flag == 1 && key == "product_imported_prices"){
		        	//neu da xoa sp thì không cần cập nhật thành tiền
		        	if(removed!=1)
		        		$scope.model.datainit.dtgListCart[i].sum=parseFloat(newpro_quan) * parseFloat(val);
		        }
		        // cap nhat sl moi
		        if(flag == 1 && key == "quantity")
		        {
		        	if($("#pro_"+pro_id).val() < 0 || $("#pro_"+pro_id).val() > 200 
						|| $("#pro_"+pro_id).val() === undefined || $("#pro_"+pro_id).val() === null
						)
					{
						var newquan=1;
					}
					else{
						var newquan=$("#pro_"+pro_id).val();
					}	
		        	//nếu flag xóa =1 (nút xóa)> xóa sp 
		        	if(delete_flag ==1){
		        		$scope.model.datainit.dtgListCart.splice(i,1);
		        		removed=1;
		        	}
		        	//nếu flag xóa =0 (nút cap nhat)
		        	else{
		        		// invalid number > delete pro
			        	if(newquan=='undefined' || newquan=="" || newquan==0)
			        	{
			        		//debugger;
			        		$scope.model.datainit.dtgListCart.splice(i,1);
			        		removed=1;
			        	}
			        	//valid > update quantity
			        	else{
				        	if($("#pro_"+pro_id).val() < 0 || $("#pro_"+pro_id).val() > 200 
								|| $("#pro_"+pro_id).val() === undefined || $("#pro_"+pro_id).val() === null
								)
							{
								newpro_quan = 1;
							}
							else{
								newpro_quan = parseInt($("#pro_"+pro_id).val());
							}	

				        	$scope.model.datainit.dtgListCart[i].quantity=newpro_quan;
			        	}
		        	}
		        }
	    	});
	    	//reset flag 
	    	flag=0;removed=0;
		});
		// tinh tong thanh tien
		var request = {
			dtgListCart: $scope.model.datainit.dtgListCart,
	    };
		$http.post($scope.model.datainit.appURL + "/WebsiteApi/UpdateCart", JSON.stringify(request))
	    	.then(
               	function (response) {
                  	var data = response.data;

			    	if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, "Lỗi Hệ Thống");
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		toastr.warning(data.warning, "Cảnh Báo");
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		toastr.success("Thông Báo", data.success);
			    		//alert(data.success);
			    		$scope.pageInit();
			    		$scope.loadCart();
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error("Lỗi Hệ thông", "Lỗi Hệ thông");
			    			
			    		}
			    		else
			    		{
			    			toastr.error("Lỗi Hệ thông", "Lỗi Hệ thông");
			    		}
			    	}
			    	$("#loader").hide();
			    },
               	function (response) {
                  	$("#loader").hide();
                  	var data = response.data;
					toastr.error("Lỗi Hệ thông", "Lỗi Hệ thông");
			    }
			);
	}
	// // 
	$scope.slbItemClick = function(item,element)
	{
   		if(element == "txtBranchId")
		{
			$('#'+element).val(item.branch_id); 
   			$('#'+element).trigger('input');
   			$scope.model.request.txtBranchId=item.branch_id;
   			$scope.model.request.txtBranchName=item.branch_name;
   			$scope.model.request.txtBranchPhone=item.branch_phone;
   			$scope.model.request.txtBranchAddress=item.branch_address_number;
		}
	};
	// select box list branch
	$scope.GetBranchList = function() {

		$("#loader").show();

		var request = {
	    };

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetBranchList", JSON.stringify(request))
	    .then(
               function (response) {
                  var data = response.data;

		    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
	            {
	                toastr.error(data.messages, Languages.common_error_title);

	                if(data.auth != "" && data.auth != null  && data.auth != undefined)
	                {
	                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
	                }
	            }
		    	else if(data.error != "" && data.error != null  && data.error != undefined)
		    	{
		    		toastr.error(data.error, Languages.common_error_title);
		    		$scope.model.datainit.slbBranch = [];
		    	}
		    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
		    	{
		    		toastr.warning(data.warning, Languages.common_warning_title);
		    		$scope.model.datainit.slbBranch = [];
		    	}
		    	else if(data.success != "" && data.success != null  && data.success != undefined)
		    	{
		    		$scope.model.datainit.slbBranch = data.success;
		    	}
		    	else
		    	{
		    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
		    		{
		    			toastr.error(data.systemerror, Languages.common_error_title);
		    			window.location.replace("./"+data.errorCode);
		    		}
		    		else
		    		{
		    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
		    		}
		    		$scope.model.datainit.slbBranch = [];
		    	}
		    	$("#loader").hide();
	    	},
           	function (response) {
          		var data = response.data;
				toastr.error(Languages.common_error_exception, Languages.common_error_title);
				$scope.model.datainit.slbBranch = [];
				$("#loader").hide();
		    });
	};
});