/*!
 *
 *  Develop by Vinh Nguyễn - Ho Chi Minh City - 2017
 *  Copyright Â© 2016-2017 Skyfire Team. All rights reserved.
 *  Design by Skyfire Team 2.3.3
 *
 *  Phone: 0975 578 276
 *  Skype: enjoyvinh
 *
 */

"use strict";

// app.js
// create angular app
var loginApp = angular.module('loginApp', ['ngRoute','commonApp', 'ngSanitize',
        ]);

loginApp.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});

// create angular controller
loginApp.controller('loginController', function($scope, $http, $filter, $window, $location,toastr) {

    $scope.model = {
        response : {
            reCaptchaResponseLogin : null,
            widgetIdLogin : null,
            reCaptchaResponse : null,
            widgetId : null
        }
    };

    

    $scope.datainit = {
        times : 3000,
        appURL : "http://localhost:8000"
    };

    $scope.loginModel= {
        userName : "",
        passWord : "",
        reMember : "0",
        userAuth : "",
        checkLogin : false,
        txtName : "",
        txtRegisterEmail : ""
    };
    $scope.registerModel= {
        registerAccount : "",
        registerPhone : "",
        registerEmail : "",
        registerPassword : "",
        registerFullName:"",

        registerAddress:"",
        registerIdentify:"",
        registerStudentCard:"",
        registerStudentUniversity:""
    };
    $scope.pageInit = function(errorMessage) {
        $("#loader").show();
        if(errorMessage != "" && errorMessage != null  && errorMessage != undefined)
        {
            toastr.error(errorMessage, "Lỗi Hệ thông");
            $("#btnLogin").button('reset');
        }

        $scope.datainit.appURL = $('meta[name="app_url"]').attr('content');
        $("#loader").hide();
    };
    // function to submit the form after all validation has occurred            
    $scope.submitForm = function(isValid) {

        // $("#btnLogin").button('loading');
        $("#loader").show();
        var request = {
            user_account : $scope.loginModel.userName,
            user_password : $scope.loginModel.passWord,
            remembered : $scope.loginModel.reMember
        };
    
        var config = {
            headers : {
                'Content-Type': 'application/json;charset=utf-8;',
                'X-CSRF-TOKEN': $('meta[name="system-secret-code"]').attr('system_content')
            }
        };

        $http.post($scope.datainit.appURL + "/WebsiteApi/checkLogin", JSON.stringify(request), config)
        .then(
               function (response) {
                  var data = response.data;

            // if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
            // {
            //     toastr.error(data.messages, "Lỗi Hệ thông");
            //     // $("#btnLogin").button('reset');
            // }
            if(data.error != "" && data.error != null  && data.error != undefined)
            {
                toastr.error(data.messages, "Lỗi Hệ thông");
                // $("#btnLogin").button('reset');
            }
            else if(data.warning != "" && data.warning != null  && data.warning != undefined)
            {
                toastr.warning(data.warning, "Cảnh Báo");
                // $("#btnLogin").button('reset');
            }
            else if(data.success != "" && data.success != null  && data.success != undefined)
            {
                toastr.success("thông báo","Đăng Nhập Thành Công");
                $window.location.href = $scope.datainit.appURL+'';
            }
            else
            {
                toastr.error(Languages.common_error_exception, "Lỗi Hệ thông");
                // $("#btnLogin").button('reset');
            }
            $("#loader").hide();
        },
               function (response) {
                  $("#loader").hide();
                  var data = response.data;
                  toastr.error(data.systemerror, "Lỗi Hệ thông");
                  // $("#btnLogin").button('reset');
               }
            );
    };

    $scope.requestRegister = function() {

        $("#btnSend").button('loading');

        // check to make sure the form is completely valid
        if($scope.model.response.reCaptchaResponse != "" && $scope.model.response.reCaptchaResponse != null  && $scope.model.response.reCaptchaResponse != undefined)
        {
            var request = {
                name : $scope.loginModel.txtName,
                email : $scope.loginModel.txtRegisterEmail
            };
        
            var config = {
                headers : {
                    'Content-Type': 'application/json;charset=utf-8;',
                    'X-CSRF-TOKEN': $('meta[name="system-secret-code"]').attr('system_content')
                }
            };

            $http.post($scope.datainit.appURL + "/WebsiteApi/RequestRegister", JSON.stringify(request), config)
            .success(function (data, status, headers, config) {

                if(data.error != "" && data.error != null  && data.error != undefined)
                {
                    $("#mailError").html(data.error);
                    $('#mailErrorContent').slideToggle("slow");
                    setTimeout(function(){ $('#mailErrorContent').slideToggle("slow"); }, $scope.datainit.times);
                    $("#btnSend").button('reset');
                }
                else if(data.warning != "" && data.warning != null  && data.warning != undefined)
                {
                    $("#mailWarning").html(data.warning);
                    $('#mailWarningContent').slideToggle("slow");
                    setTimeout(function(){ $('#mailWarningContent').slideToggle("slow"); }, $scope.datainit.times);
                    $("#btnSend").button('reset');
                }
                else if(data.success != "" && data.success != null  && data.success != undefined)
                {
                    $("#mailSuccess").html(data.success);
                    $('#mailSuccessContent').slideToggle("slow");
                    setTimeout(function(){ $('#mailSuccessContent').slideToggle("slow"); }, $scope.datainit.times);
                    $("#btnSend").button('reset');
                    $scope.loginModel= {
                        txtName : "",
                        txtRegisterEmail : ""
                    };
                    $scope.formGuiMail.$setPristine();
                    $scope.formGuiMail.$setUntouched();
                }
                else
                {
                    $("#mailError").html(Languages.common_error_exception);
                    $('#mailErrorContent').slideToggle("slow");
                    setTimeout(function(){ $('#mailErrorContent').slideToggle("slow"); }, $scope.datainit.times);
                    $("#btnSend").button('reset');
                }
            })
            .error(function (data, status, header, config) {
                $("#mailError").html(Languages.common_error_exception);
                $('#mailErrorContent').slideToggle("slow");
                setTimeout(function(){ $('#mailErrorContent').slideToggle("slow"); }, $scope.datainit.times);
                $("#btnSend").button('reset');
            });
        }
        else
        {
            $("#mailWarning").html("reCaptcha không chính xác. Vui lòng thử lại hoặc liên hệ quản trị viên để được hỗ trợ.");
            $('#mailWarningContent').slideToggle("slow");
            setTimeout(function(){ $('#mailWarningContent').slideToggle("slow"); }, $scope.datainit.times);
            $("#btnSend").button('reset');
            $scope.cbExpiration();
        }
    };

    $scope.setResponse = function (response) {
        // console.info('Response available');
        $scope.model.response.reCaptchaResponse = response;
    };

    $scope.setWidgetId = function (widgetId) {
        // console.info('Created widget ID: %s', widgetId);
        $scope.model.response.widgetId = widgetId;
    };

    $scope.cbExpiration = function() {
        // console.info('Captcha expired. Resetting response object');

        vcRecaptchaService.reload($scope.model.response.widgetId);
         
        $scope.model.response.reCaptchaResponse= null;
    };

    $scope.setResponseLogin = function (response) {
        // console.info('Response available');
        $scope.model.response.reCaptchaResponseLogin = response;
    };

    $scope.setWidgetIdLogin = function (widgetId) {
        // console.info('Created widget ID: %s', widgetId);
        $scope.model.response.widgetIdLogin = widgetId;
    };

    $scope.cbExpirationLogin = function() {
        // console.info('Captcha expired. Resetting response object');

        vcRecaptchaService.reload($scope.model.response.widgetIdLogin);
         
        $scope.model.response.reCaptchaResponseLogin = null;
    };

    var alertToggle = function(type, data) {
        if(type == "error")
        {
            $("#alertMessage").html(data);

            $('.alert-danger').slideToggle("slow");
            setTimeout(function(){ $('.alert-danger').slideToggle("slow"); }, $scope.datainit.times);
        }
        else if (type == "warning")
        {
            $("#warningMessage").html(data);

            $('.alert-warning').slideToggle("slow");
            setTimeout(function(){ $('.alert-warning').slideToggle("slow"); }, $scope.datainit.times);
        }
        else
        {
            $("#alertMessage").html(data);

            $('.alert-danger').slideToggle("slow");
            setTimeout(function(){ $('.alert-danger').slideToggle("slow"); }, $scope.datainit.times);
        }
    }
    //registerUser
    $scope.registerUser = function() {
        // txtRegisterAccount
        // txtRegisterFullName
        // txtRegisterEmail
        // txtRegisterPhone
        // txtRegisterAddress
        // txtRegisterIdentify
        // txtRegisterStudentCard
        // txtRegisterStudentUniversity
        // txtRegisterPassword
        var request = {
            registerAccount: $scope.registerModel.registerAccount,
            registerFullName: $scope.registerModel.registerFullName,
            registerEmail: $scope.registerModel.registerEmail,
            registerPhone: $scope.registerModel.registerPhone,
            registerAddress: $scope.registerModel.registerAddress,
            registerIdentify: $scope.registerModel.registerIdentify,
            registerStudentCard: $scope.registerModel.registerStudentCard,
            registerStudentUniversity: $scope.registerModel.registerStudentUniversity,
            registerPassword: $scope.registerModel.registerPassword,
        };

        $http.post($scope.datainit.appURL + "/WebsiteApi/RegisterUser", JSON.stringify(request))
            .then(
                function (response) {
                    var data = response.data;

                    if(data.error != "" && data.error != null  && data.error != undefined)
                    {
                        toastr.error(data.error, "Lỗi Hệ thông");
                    }
                    else if(data.warning != "" && data.warning != null  && data.warning != undefined)
                    {
                        toastr.warning(data.warning, "Cảnh Báo");
                    }
                    else if(data.success != "" && data.success != null  && data.success != undefined)
                    {
                        toastr.success("thông báo", data.success);
       
                        setTimeout(function(){ 
                            window.location.replace( $scope.datainit.appURL + "");
                        }, 500);
                    }
                    else
                    {
                        if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
                        {
                            toastr.error(data.systemerror, "Lỗi Hệ thông");
                        }
                        else
                        {
                            toastr.error(data.systemerror, "Lỗi Hệ thông");
                        }
                    }
                },
                function (response) {
                    debugger;
                    var data = response.data;
                    toastr.error("Lỗi Hệ Thống exception", "Lỗi Hệ thông");
                }
            );

    }
});