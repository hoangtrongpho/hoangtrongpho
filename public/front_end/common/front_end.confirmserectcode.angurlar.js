/*!
 *
 *	Develop by Vinh Nguyễn - Ho Chi Minh City - 2017
 * 	Copyright Â© 2016-2017 Skyfire Team. All rights reserved.
 *	Design by Skyfire Team 2.3.3
 *
 *	Phone: 0975 578 276
 *	Skype: enjoyvinh
 *
 */

"use strict";

// app.js
// create angular app
var loginApp = angular.module('loginApp', ['ngRoute']);

loginApp.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});

// create angular controller
loginApp.controller('passwordConfirmController', function($scope, $http, $filter, $window, $location) {


    $scope.model = {
        datainit : {
            confirmListMode : [
                {
                    'mode_id' : 'phone',
                    'mode_name' : 'Sử dụng số điện thoại đã đăng ký'
                },
                {
                    'mode_id' : 'email',
                    'mode_name' : 'Sử dụng email đã đăng ký'
                }
            ],
            times : 3000,
            appURL : "http://localhost:8000"
        },
        request : {
            txtEmail : "",
            txtEmail : "",
        }
    };

    $scope.pageInit = function() {
        $scope.model.datainit.appURL = $('meta[name="app_url"]').attr('content');
    };


	// function to submit the form after all validation has occurred            
	$scope.generateSecureCode = function(confirmMode) {

		$("#btnLogin").button('loading');

		var request = {
            method : confirmMode,
            account : $scope.model.request.txtEmail !== "" ? $scope.model.request.txtEmail : $scope.model.request.txtPhone,
        };

        if(request.account == "" || request.account == null || request.account == undefined)
        {
            if(request.method == "phone")
            {
                alertToggle("warning","Vui lòng nhập số điện thoại đã đăng ký.",3000);
            }
            else
            {
                alertToggle("warning","Vui lòng nhập email đã đăng ký.",3000);
            }
            $("#btnLogin").button('reset');
            return;
        }

        var config = {
            headers : {
                'Content-Type': 'application/json;charset=utf-8;',
        		'X-CSRF-TOKEN': $('meta[name="system-secret-code"]').attr('system_content')
            }
        };

        $http.post($scope.model.datainit.appURL + "/WebsiteApi/GenerateSecureCode", JSON.stringify(request), config)
        .success(function (data, status, headers, config) {
            debugger;

        	if(data.error != "" && data.error != null  && data.error != undefined)
        	{
        		alertToggle("error",data.error,3000);
                $("#btnLogin").button('reset');
        	}
        	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
        	{
        		alertToggle("warning",data.warning,3000);
                $("#btnLogin").button('reset');
        	}
        	else if(data.success != "" && data.success != null  && data.success != undefined)
        	{
        		$window.location.href = $scope.model.datainit.appURL+'/hoan-thanh-doi-mat-khau';
        	}
        	else
        	{
        		alertToggle("error",data.error,3000);
                $("#btnLogin").button('reset');
        	}
            $("#btnLogin").button('reset');
        })
        .error(function (data, status, header, config) {
            debugger;
			alertToggle("error",data.error,3000);
            $("#btnLogin").button('reset');
        });


	};

	var alertToggle = function(type, data, times) {
		if(type == "error")
		{
			$("#alertMessage").html(data);

    		$('.alert-danger').slideToggle("slow");
			setTimeout(function(){ $('.alert-danger').slideToggle("slow"); }, times);
		}
		else if (type == "warning")
		{
			$("#warningMessage").html(data);

    		$('.alert-warning').slideToggle("slow");
			setTimeout(function(){ $('.alert-warning').slideToggle("slow"); }, times);
		}
		else
		{
			$("#alertMessage").html(data);

    		$('.alert-danger').slideToggle("slow");
			setTimeout(function(){ $('.alert-danger').slideToggle("slow"); }, times);
		}
	}
});