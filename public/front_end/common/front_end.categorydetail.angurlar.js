/*!
 *
 *	Develop by Vinh Nguyễn - Ho Chi Minh City - 2017
 * 	Copyright Â© 2016-2017 Skyfire Team. All rights reserved.
 *	Design by Skyfire Team 2.3.3
 *
 *	Phone: 0975 578 276
 *	Skype: enjoyvinh
 *
 */

"use strict";

// app.JSON
// create angular app
var productDetailApp = angular.module('productDetailApp', ['commonApp','ngSanitize']);

productDetailApp.factory('NewsModels', function() {

	var model={
		datainit : {
			// dtgListInvoice:[],
			appURL : "http://localhost:8000"
		},
	};

	model.request = {
		txtProductId:"",
		txtQuantity: 1,
	};

	return model;
});

// create angular controller
productDetailApp.controller('productDetailController', function(NewsModels, commonFunction, $scope, $rootScope, $http, $filter, $window, $location, toastr) {

	$scope.model = NewsModels;

	$scope.pageInit = function() {
		$scope.model.datainit.appURL = $('meta[name="app_url"]').attr('content');
	};
	//addCart
	$scope.addCart = function(product_id) {
		
		if(product_id == "" || product_id == null || product_id == undefined)
		{
			toastr.error(Resources.common_error_exception, Resources.common_error_title);
			return false;
		}
		
    	var request = {
	        product_id : product_id,
	        quantity : $scope.model.request.txtQuantity
	    };
		$http.post($scope.model.datainit.appURL + "/WebsiteApi/AddCart", JSON.stringify(request))
	    	.then(
               	function (response) {
                  	var data = response.data;

			    	if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error("Lỗi Hệ Thống", data.error);
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		toastr.warning("Cảnh Báo", data.warning);
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		//toastr.success("Thông Báo", data.success);
			    		//setTimeout(function(){ 
							window.location.replace($scope.model.datainit.appURL + "/gio-hang");
						//}, 500);
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error("Lỗi Hệ Thống exception", "");
			    		}
			    		else
			    		{
			    			toastr.error("Lỗi Hệ Thống exception", "");
			    		}
			    	}
			    },
               	function (response) {
                  	debugger;
                  	var data = response.data;
					toastr.error("Lỗi Hệ Thống exception", "");
			    }
			);
	}
});