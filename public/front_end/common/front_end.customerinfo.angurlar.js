/*!
 *
 *  Develop by Vinh Nguyễn - Ho Chi Minh City - 2017
 *  Copyright Â© 2016-2017 Skyfire Team. All rights reserved.
 *  Design by Skyfire Team 2.3.3
 *
 *  Phone: 0975 578 276
 *  Skype: enjoyvinh
 *
 */

"use strict";

// app.js
// create angular app
var loginApp = angular.module('loginApp', ['ngRoute','commonApp', 'ngSanitize']);
// create angular controller
loginApp.controller('loginController', function($scope, $http, $filter, $window, $location,toastr) {

 
    

    $scope.datainit = {
        appURL : "http://localhost:8000"
    };

    $scope.request = {
        registerAccount : "",
        registerPhone : "",
        registerEmail : "",
        registerPassword : "",
        registerFullName:"",

        registerAddress:"",
        registerIdentify:"",
        registerStudentCard:"",
        registerStudentUniversity:""
    };
  
    $scope.pageInit = function(errorMessage) {
        $scope.GetUserDetail();
        $scope.datainit.appURL = $('meta[name="app_url"]').attr('content');
    };
    // GetUserDetail
    $scope.GetUserDetail = function() {
        
        $("#loader").show();
        
        $scope.datainit.appURL = $('meta[name="app_url"]').attr('content');
        var request = {
        };
        $http.post($scope.datainit.appURL + "/WebsiteApi/GetUserDetail", JSON.stringify(request))
            .then(
                function (response) {
                    var data = response.data;
                    if(data.error != "" && data.error != null  && data.error != undefined)
                    {
                        toastr.error(data.error, "Lỗi Hệ Thống");
                        setTimeout(function(){ 
                            window.location.replace($scope.datainit.appURL);
                        }, 500);
                    }
                    else if(data.warning != "" && data.warning != null  && data.warning != undefined)
                    {
                        toastr.warning(data.warning, "Cảnh Báo");
                    }
                    else if(data.success != "" && data.success != null  && data.success != undefined)
                    {
                        $scope.request.registerAccount = data.success.customer_account;
                        $scope.request.registerFullName = data.success.customer_fullname;
                        $scope.request.registerEmail = data.success.customer_email;
                        $scope.request.registerPhone = data.success.customer_phone;
                        $scope.request.registerAddress = data.success.customer_address;
                        $scope.request.registerIdentify = data.success.customer_identify;
                        $scope.request.registerStudentCard = data.success.customer_student_card;
                        $scope.request.registerStudentUniversity = data.success.customer_university;
                    }
                    else
                    {
                        if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
                        {
                            toastr.error(Languages.common_error_exception, "Lỗi Hệ Thống");
                        
                        }
                        else
                        {
                            toastr.error(Languages.common_error_exception, "Lỗi Hệ Thống");
              
                        }
                    }
                    $("#loader").hide();
                },
                function (response) {
                    $("#loader").hide();
                    var data = response.data;
                    toastr.error(Languages.common_error_exception, "Lỗi Hệ Thống");
                }
            );
    }
    // UpdateUserDetail
    $scope.UpdateUserDetail = function() {
        
        $("#loader").show();
        toastr.success("sdsad" , "Thông Báo");
        $scope.datainit.appURL = $('meta[name="app_url"]').attr('content');
        var request = {
            customer_account: $scope.request.registerAccount,
            customer_email: $scope.request.registerEmail,
            customer_fullname: $scope.request.registerFullName,
            customer_address: $scope.request.registerAddress,
            customer_phone:  $scope.request.registerPhone,
            customer_identify: $scope.request.registerIdentify,
            customer_student_card: $scope.request.registerStudentCard,
            customer_university: $scope.request.registerStudentUniversity
        };
        $http.post($scope.datainit.appURL + "/WebsiteApi/UpdateUserDetail", JSON.stringify(request))
            .then(
                function (response) {
                    var data = response.data;
                    if(data.error != "" && data.error != null  && data.error != undefined)
                    {
                        toastr.error(data.error, "Lỗi Hệ Thống");
                    }
                    else if(data.warning != "" && data.warning != null  && data.warning != undefined)
                    {
                        toastr.warning(data.warning, "Cảnh Báo");
                    }
                    else if(data.success != "" && data.success != null  && data.success != undefined)
                    {
                        alert("Cập Nhật Thành Công");
                        setTimeout(function(){ 
                            window.location.replace($scope.datainit.appURL);
                        }, 500);
                    }
                    else
                    {
                        if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
                        {
                            toastr.error(Languages.common_error_exception, "Lỗi Hệ Thống");
                        
                        }
                        else
                        {
                            toastr.error(Languages.common_error_exception, "Lỗi Hệ Thống");
              
                        }
                    }
                    $("#loader").hide();
                },
                function (response) {
                    $("#loader").hide();
                    var data = response.data;
                    toastr.error(Languages.common_error_exception, "Lỗi Hệ Thống");
                }
            );
    }
    

});