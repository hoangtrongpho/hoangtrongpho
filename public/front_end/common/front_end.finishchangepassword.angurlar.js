/*!
 *
 *	Develop by Vinh Nguyễn - Ho Chi Minh City - 2017
 * 	Copyright Â© 2016-2017 Skyfire Team. All rights reserved.
 *	Design by Skyfire Team 2.3.3
 *
 *	Phone: 0975 578 276
 *	Skype: enjoyvinh
 *
 */

"use strict";

// app.js
// create angular app
var loginApp = angular.module('loginApp', ['ngRoute']);

loginApp.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});

// create angular controller
loginApp.controller('passwordConfirmController', function($scope, $http, $filter, $window, $location) {


    $scope.model = {
        datainit : {
            confirmListMode : [
                {
                    'mode_id' : 'phone',
                    'mode_name' : 'Sử dụng số điện thoại đã đăng ký'
                },
                {
                    'mode_id' : 'email',
                    'mode_name' : 'Sử dụng email đã đăng ký'
                }
            ],
            times : 3000,
            appURL : "http://localhost:8000",
            confirm : false
        },
        request : {
            txtSecureCode : "",
            txtPassword : "",
            txtConfirmPassword : ""
        }
    };

    $scope.pageInit = function() {

        $scope.model.datainit.appURL = $('meta[name="app_url"]').attr('content');
    };


	// function to submit the form after all validation has occurred            
	$scope.checkConfirm = function() {

		$("#btnLogin").button('loading');

		var request = {
            security_code : $scope.model.request.txtSecureCode,
        };
    
        var config = {
            headers : {
                'Content-Type': 'application/json;charset=utf-8;',
        		'X-CSRF-TOKEN': $('meta[name="system-secret-code"]').attr('system_content')
            }
        };

        $http.post($scope.model.datainit.appURL + "/WebsiteApi/SecureCodeConfirm", JSON.stringify(request), config)
        .success(function (data, status, headers, config) {
        	if(data.error != "" && data.error != null  && data.error != undefined)
        	{
        		alertToggle("error",data.error,3000);
                $scope.model.datainit.confirm = false;
        	}
        	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
        	{
        		alertToggle("warning",data.warning,3000);
                $scope.model.datainit.confirm = false;
        	}
        	else if(data.success != "" && data.success != null  && data.success != undefined)
        	{
        		$scope.model.datainit.confirm = true;
                $scope.model.request.txtSecureCode = data.success;
        	}
        	else
        	{
        		alertToggle("error",data.error,3000);
                $scope.model.datainit.confirm = false;
        	}
            $("#btnLogin").button('reset');

        })
        .error(function (data, status, header, config) {
            $scope.model.datainit.confirm = false;
			alertToggle("error",data.error,3000);
            $("#btnLogin").button('reset');
        });

        $scope.model.request = {
            txtPassword : "",
            txtConfirmPassword : "",
        }
	};

    // function to submit the form after all validation has occurred            
    $scope.updatePassword = function() {

        $("#btnLogin").button('loading');


        var request = {
            security_code : $scope.model.request.txtSecureCode,
            agency_password : $scope.model.request.txtPassword,
            agency_password_confirm : $scope.model.request.txtConfirmPassword
        };
    
        var config = {
            headers : {
                'Content-Type': 'application/json;charset=utf-8;',
                'X-CSRF-TOKEN': $('meta[name="system-secret-code"]').attr('system_content')
            }
        };

        $http.post($scope.model.datainit.appURL + "/WebsiteApi/FinishChangePassword", JSON.stringify(request), config)
        .success(function (data, status, headers, config) {
            if(data.error != "" && data.error != null  && data.error != undefined)
            {
                alertToggle("error",data.error,3000);
                $window.location.href = $scope.model.datainit.appURL+'/dang-nhap';
            }
            else if(data.warning != "" && data.warning != null  && data.warning != undefined)
            {
                alertToggle("warning",data.warning,3000);
            }
            else if(data.success != "" && data.success != null  && data.success != undefined)
            {
                alertToggle("success",data.success,3000);
                $window.location.href = $scope.model.datainit.appURL+'/dang-nhap';
            }
            else
            {
                alertToggle("error",data.error,3000);
                $window.location.href = $scope.model.datainit.appURL+'/dang-nhap';
            }
        })
        .error(function (data, status, header, config) {
            alertToggle("error",data.error,3000);
            $window.location.href = $scope.model.datainit.appURL+'/dang-nhap';
        });
    };

	var alertToggle = function(type, data, times) {
        if(type == "success")
        {
            $("#successMessage").html(data);
            $('.alert-success').slideToggle("slow");
            setTimeout(function(){ $('.alert-success').slideToggle("slow"); }, times);
        }
		else if(type == "error")
		{
			$("#alertMessage").html(data);
    		$('.alert-danger').slideToggle("slow");
			setTimeout(function(){ $('.alert-danger').slideToggle("slow"); }, times);
		}
		else if (type == "warning")
		{
			$("#warningMessage").html(data);
    		$('.alert-warning').slideToggle("slow");
			setTimeout(function(){ $('.alert-warning').slideToggle("slow"); }, times);
		}
		else
		{
			$("#alertMessage").html(data);
    		$('.alert-danger').slideToggle("slow");
			setTimeout(function(){ $('.alert-danger').slideToggle("slow"); }, times);
		}
	}
});