/*!
 *
 *	Develop by Vinh Nguyễn - Ho Chi Minh City - 2017
 * 	Copyright Â© 2016-2017 Skyfire Team. All rights reserved.
 *	Design by Skyfire Team 2.3.3
 *
 *	Phone: 0975 578 276
 *	Skype: enjoyvinh
 *
 */

"use strict";

// app.js
// create angular app
var dashboardApp = angular.module('dashboardApp', ['commonApp','datatables','datatables.colreorder','datatables.bootstrap','zingchart-angularjs']);

dashboardApp.factory('dashboardModels', function() {

	var model={
		datainit : {
			// zing chart render
			myRender:{width: "100%"},
			currentYear:new Date().getFullYear(),
			dtgChartColor:["yellow brown","gray brown","cyan brown","cyan yellow","aqua yellow","blue yellow","blue green","yellow green","teal aqua","green blue","blue darkblue","blue teal","olive yellow","purple fuchsia","maroon red","brown pink","pink purple","lavender lemon","lime purple"],

			dtgListSystemSale:[],

			dtgListBranch:[],
			dtgListBranchSale:[],
			
			totalInvoicePrice: 0,	
			totalInvoice : 0,
			totalProduct : 0,
			totalView : 0,
			totalViewByDay : 0,
			totalViewByMonth : 0,
			totalViewByYear : 0,

			slbSaleCustomer:[],
			slbSaleCustomerSum:0,
			slbDebtSaleCustomer:[],
			slbDebtSaleCustomerSum:0,

			slbSaleProduct:[],
			slbSaleProductSum:0,
			slbDebtSaleProduct:[],
			slbDebtSaleProductSum:0,

			slbSaleBranch:[],
			slbSaleBranchSum:0,
			slbDebtSaleBranch:[],
			slbDebtSaleBranchSum:0,

			slbSaleSubadmin:[],
			slbSaleSubadminSum:0,
			slbDebtSubadmin:[],
			slbDebtSubadminSum:0,
		},
		preLoading : true,
		accessPreLoading : true,
		appURL : "http://localhost:8000",

	};

	model.chartSaleBranch={
				"type":"vbar3d",
			    "fill-type":"radial",
			    "3d-aspect":{
			      "true3d":false,
			      // "zoom":0.98,
			    },
				"plot":{
					"background-color": "darkblue blue",
			        "aspect":"cylinder",
			        // "bars-overlap":"100%",
			        "mode":"normal",
			        "value-box": {
      					"thousands-separator":",",
		                "format":"$%v",
		                "text":"%v",
      					"placement":"top",	
      					"font-color":"red",
      					"offset-y":-15,
      					"offset-x": 12,
      					// "font-size":11,
    				},
			        "tooltip": {
			        	// kl: tên tháng
			        	// v: giá trị
			        	// t: legend
			        	"visible":true,
		                "thousands-separator":",",
		                "format":"$%v",
		                "text":"%v Đ",
				      	"sticky": true, //Set to true.
      					"timeout": 500 //Provide value in milliseconds.
				    }
			    },	
			    "source": {
				  	"text": "Source: Developed by Vtech LDC",
				  	"background-color":"#C30",
				  	"color":"#FFF",
				  	"width":150,
				  	"border-radius":4,
				  	"width":170,
  					"height":20,
  					"x":"10%",
			    	"y":"90%",
				},	
				"title":{
				  	"text":"Doanh Số...",
				  	"background-color":"#333",
				  	"color":"#FFF",
				  	"border-radius":"4px",
				  	"font-weight":"bold",
				  	"font-size":15,
				},
			    "legend":{
			    	"layout":"2",
			    	"x":"0%",
			    	"y":"0%",
			    	"highlight-plot": true
	  			},
			  	
			  	"scale-x": {
			    	"labels": ["Tháng 1","Tháng 2","Tháng 3","Tháng 4","Tháng 5","Tháng 6","Tháng 7","Tháng 8","Tháng 9","Tháng 10","Tháng 11","Tháng 12"],
			    	"label":{
			    		"text":"Thời Gian",
      					"font-size":20,
			    	}
			  	}, 
			  	"scale-y": {
			    	"short":true, //To display scale values as short units.
			    	"short-unit":"M", //To set the short unit type.
			    	"thousands-separator":",",
			    	"label":{
			    		"text":"Doanh Số",
      					"font-size":20,
      					"offset-x": 10,
			    	},
			  	},
			  	"series" : [  
			 
			  	],
	};

	model.chartSaleSystem={
				"type":"bar3d",
			    "fill-type":"radial",
			    "3d-aspect":{
			      "true3d":false,
			      "angle":80,
			      // "zoom":0.98,
			    },
				"plot":{
					"background-color": "darkblue blue",
			        // "aspect":"cylinder",
			        "mode":"normal",
			        "value-box": {
      					"thousands-separator":",",
		                "format":"$%v",
		                "text":"%v",
      					"placement":"top",	
      					"font-color":"red",
      					"offset-y":-10,
      					"offset-x": 18,
      					// "font-size":11,
    				},
			        "tooltip": {
			        	// kl: tên tháng
			        	// v: giá trị
			        	// t: legend
			        	"visible":true,
		                "thousands-separator":",",
		                "format":"$%v",
		                "text":"%v Đ",
				      	"sticky": true, //Set to true.
      					"timeout": 500 //Provide value in milliseconds.
				    }
			    },	
			    "source": {
				  	"text": "Source: Developed by Vtech LDC",
				  	"background-color":"#C30",
				  	"color":"#FFF",
				  	"width":150,
				  	"border-radius":4,
				  	"width":170,
  					"height":20,
  					"x":"10%",
			    	"y":"90%",
				},	
				"title":{
				  	"text":"Doanh Số...",
				  	"background-color":"#333",
				  	"color":"#FFF",
				  	"border-radius":"4px",
				  	"font-weight":"bold",
				  	"font-size":15,
				},
			    "legend":{
			    	"layout":"2",
			    	"x":"0%",
			    	"y":"0%",
			    	"highlight-plot": true
	  			},
			  	
			  	"scale-x": {
			    	"labels": ["Tháng 1","Tháng 2","Tháng 3","Tháng 4","Tháng 5","Tháng 6","Tháng 7","Tháng 8","Tháng 9","Tháng 10","Tháng 11","Tháng 12"],
			    	"label":{
			    		"text":"Thời Gian",
      					"font-size":20,
			    	}
			  	}, 
			  	"scale-y": {
			    	"short":true, //To display scale values as short units.
			    	"short-unit":"M", //To set the short unit type.
			    	"thousands-separator":",",
			    	"label":{
			    		"text":"Doanh Số",
      					"font-size":20,
      					"offset-x": 10,
			    	},
			  	},
			  	"series" : [  
			 
			  	],
	};
	model.request = {
		currentMonth : moment(),
		accessCurrentMonth : moment(),
	};

	return model;
});

// create angular controller
dashboardApp.controller('dashboardController', function(dashboardModels, commonFunction, $scope, $rootScope, $http, $filter, $window, $location, $timeout, toastr,DTOptionsBuilder, DTColumnDefBuilder,DTColumnBuilder) {

	$scope.model = dashboardModels;

	$scope.pageInit = function() {

		$scope.model.datainit.appURL = $('meta[name="app_url"]').attr('content');

		$scope.GetDashboard();
		$scope.GetBranchList();
		$scope.GetTotalSaleByDateTime();

	
		$scope.GetCustomerSale();
		$scope.GetCustomerDebtSale();
		$scope.GetProductSale();
		$scope.GetProductDebtSale();	
		$scope.GetBranchSale();
		$scope.GetBranchDebtSale();
		$scope.GetSubadminSale();
		$scope.GetSubadminDebtSale();

		
		$scope.model.datainit.preLoading = false;

		$scope.dtOptions = DTOptionsBuilder.newOptions()
			.withOption('order', [])
        	.withPaginationType('full_numbers')
        	.withDisplayLength(10)
	        .withColReorder()
	        // .withColReorderOrder([0, 1, 2, 3, 4])
	        .withBootstrap()
	        .withBootstrapOptions({
	            pagination: {
	                classes: {
	                    ul: 'pagination pagination-md'
	                }
	            }
	        })
	        .withLanguage({
	            "sEmptyTable":     "Không có dữ liệu.",
	            "sInfo":           "Hiển Thị Từ _START_ Đến _END_ Của _TOTAL_ Kết Quả",
	            "sInfoEmpty":      "Hiển Thị Từ 0 Đến 0 Của 0 Kết Quả",
	            "sInfoFiltered":   "(Đã Lọc Từ Tổng _MAX_ Kết Quả)",
	            "sInfoPostFix":    "",
	            "sInfoThousands":  ",",
	            "sLengthMenu":     "Hiển Thị&nbsp;&nbsp;&nbsp;_MENU_&nbsp;&nbsp;&nbsp;Kết Quả / 1 Trang",
	            "sLoadingRecords": "Đang Tải Dữ Liệu...",
	            "sProcessing":     "Đang Xử Lý Dữ Liệu...",
	            "sSearch":         "Từ Khóa: ",
	            "sZeroRecords":    "Không tìm thấy dữ liệu trùng khớp.",
	            "oPaginate": {
	                "sFirst":    "Đầu",
	                "sLast":     "Cuối",
	                "sNext":     "Tiếp",
	                "sPrevious": "Trước"
	            },
	            "oAria": {
	                "sSortAscending":  ": activate to sort column ascending",
	                "sSortDescending": ": activate to sort column descending"
	            }
	        })
	        .withOption('responsive', true);

	    $scope.dtColumnDefs = [
	        DTColumnDefBuilder.newColumnDef(0).notSortable(),
	    ];

	};
	// ====================================================================
	// GetBranchList and init the chart
	$scope.GetBranchList = function() {

		$("#listReLoading").show();

		var request = {
	    };

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetBranchList", JSON.stringify(request))
	    .then(
               function (response) {
                  var data = response.data;

		    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
	            {
	                toastr.error(data.messages, Languages.common_error_title);

	                if(data.auth != "" && data.auth != null  && data.auth != undefined)
	                {
	                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
	                }
	            }
		    	else if(data.error != "" && data.error != null  && data.error != undefined)
		    	{
		    		toastr.error(data.error, Languages.common_error_title);
		    		$scope.model.datainit.dtgListBranch = [];
		    	}
		    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
		    	{
		    		toastr.warning(data.warning, Languages.common_warning_title);
		    		$scope.model.datainit.dtgListBranch = [];
		    	}
		    	else if(data.success != "" && data.success != null  && data.success != undefined)
		    	{
		    		// clear data and init from the begin
		    		$scope.model.datainit.dtgListBranchSale=[];
		    		$scope.model.chartSaleBranch.series=[];

		    		$scope.model.datainit.dtgListBranch = data.success;
		    		if($scope.model.datainit.dtgListBranch.length==0)
		    		{
		    			alert("Chưa Có Chy Nhánh !");
		    			return false;
		    		}
		    		// cai dat legend layout
		    		$scope.model.chartSaleBranch.legend.layout=data.success.length;
		    		$scope.model.chartSaleBranch.title.text="Doanh Thu Theo Chi Nhánh Năm "+ $scope.model.datainit.currentYear;
		    		// init chart 
		    		angular.forEach(data.success, function(value, key) {
		    			var bra_id=value.branch_id;
		    			var bra_name=value.branch_name;
		    			var bra_color=$scope.model.datainit.dtgChartColor[Math.floor(Math.random()*$scope.model.datainit.dtgChartColor.length)];
		    			$scope.GetTotalSaleByDateTimeBranch(bra_id,bra_name,bra_color);

					});
		    	}
		    	else
		    	{
		    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
		    		{
		    			toastr.error(data.systemerror, Languages.common_error_title);
		    			window.location.replace("./"+data.errorCode);
		    		}
		    		else
		    		{
		    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
		    		}
		    		$scope.model.datainit.dtgListBranch = [];
		    	}
		    	$("#listReLoading").hide();
	    	},
           	function (response) {
          		var data = response.data;
				toastr.error(Languages.common_error_exception, Languages.common_error_title);
				$scope.model.datainit.dtgListBranch = [];
				$("#listReLoading").hide();
		    });
	};
	// GetTotalSaleByDateTimeBranch: tao chart của 1 chy nhánh
	$scope.GetTotalSaleByDateTimeBranch = function(bra_id,bra_name,bra_color) {

		$("#listReLoading").show();

		var request = {
			branch_id:bra_id,
	    };

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetTotalSaleByDateTimeBranch", JSON.stringify(request))
	    .then(
               function (response) {
                  var data = response.data;

		    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
	            {
	                toastr.error(data.messages, Languages.common_error_title);

	                if(data.auth != "" && data.auth != null  && data.auth != undefined)
	                {
	                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
	                }
	            }
		    	else if(data.error != "" && data.error != null  && data.error != undefined)
		    	{
		    		toastr.error(data.error, Languages.common_error_title);
		    		$scope.model.datainit.dtgBranchChart = [];
		    	}
		    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
		    	{
		    		toastr.warning(data.warning, Languages.common_warning_title);
		    		$scope.model.datainit.dtgBranchChart = [];
		    	}
		    	else if(data.success != "" && data.success != null  && data.success != undefined)
		    	{
		    		// tạo 12 tháng dữ liệu doanh số cho chi nhánh
		    		var dataListChart=[];
		    		for(var i=1;i<=12;i++){
		    			var flag="yes";
		    			angular.forEach(data.success, function(value, key) {
						    if(value.month == i && value.year == $scope.model.datainit.currentYear)
						    {
						    	dataListChart.push(parseFloat(value.total_sum));
						    	flag="no";
						    }
						});		
						if(flag=="yes") {
							dataListChart.push(0);
						}    		
		    		}
		    		// thực thể chi nhánh
		    		var obj = {
				  		values: dataListChart, 
				  		text: bra_name,
				  		"background-color":bra_color,
				  	};
				  	// tạo chart theo chi nhánh obj
				  	$scope.model.chartSaleBranch.series.push(obj);
				  	$scope.model.datainit.dtgListBranchSale.push(data.success);
		    	}
		    	else
		    	{
		    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
		    		{
		    			toastr.error(data.systemerror, Languages.common_error_title);
		    			window.location.replace("./"+data.errorCode);
		    		}
		    		else
		    		{
		    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
		    		}
		    		$scope.model.datainit.dtgBranchChart = [];
		    	}
		    	$("#listReLoading").hide();
	    	},
           	function (response) {
          		var data = response.data;
				toastr.error(Languages.common_error_exception, Languages.common_error_title);
				$scope.model.datainit.dtgBranchChart = [];
				$("#listReLoading").hide();
		    });
	};
	// reload branch list
	$scope.ReloadBranchList = function() {
		// clear data and init from the begin
		$scope.model.chartSaleBranch.series=[];

		if($scope.model.datainit.dtgListBranch.length==0)
		{
			alert("Chưa Có Chy Nhánh !");
			return false;
		}
		// cai dat legend layout
		$scope.model.chartSaleBranch.legend.layout=$scope.model.datainit.dtgListBranch.length;
		$scope.model.chartSaleBranch.title.text="Doanh Thu Theo Chi Nhánh Năm "+ $scope.model.datainit.currentYear;
		// init chart 
		angular.forEach($scope.model.datainit.dtgListBranch, function(value, key) {
			var bra_id=value.branch_id;
			var bra_name=value.branch_name;
			var bra_color=$scope.model.datainit.dtgChartColor[key];
			// clear first value
    		var dataListChart=[];

    		for(var i=1;i<=12;i++){
    			var flag="yes";
    			angular.forEach($scope.model.datainit.dtgListBranchSale[key], function(value, key) {
				    if(value.month == i && value.year == $scope.model.datainit.currentYear)
				    {
				    	dataListChart.push(parseFloat(value.total_sum));
				    	flag="no";
				    }
				});		
				if(flag=="yes") {
					dataListChart.push(0);
				}    		
    		}
    		// thực thể chi nhánh
    		var obj = {
		  		values: dataListChart, 
		  		text: bra_name,
		  		"background-color":bra_color,
		  	};
		  	// tạo chart theo chi nhánh obj
		  	$scope.model.chartSaleBranch.series.push(obj);
		});
	};
	// ng-click="nextYear()"
	$scope.nextYear = function(){
		$scope.model.datainit.currentYear=$scope.model.datainit.currentYear+1;
		$scope.ReloadBranchList();$scope.ReloadSystemList();
	};
	// ng-click="backYear()"
	$scope.backYear = function(){
		$scope.model.datainit.currentYear=$scope.model.datainit.currentYear-1;
		$scope.ReloadBranchList();$scope.ReloadSystemList();
	};
	// get dashboard
	$scope.GetDashboard = function(){
		$http.post($scope.model.datainit.appURL + "/SystemApi/GetDashboard")
			.then(
				function (response) {
					var data = response.data;

			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		                
		                $scope.model.datainit.dtgListNews = [];
		            }
			    	if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		toastr.warning(data.warning, Languages.common_warning_title);
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		$scope.model.datainit.totalInvoice = data.success.totalInvoice;
			    		$scope.model.datainit.totalView = data.success.totalView;
			    		$scope.model.datainit.totalInvoicePrice = (data.success.totalInvoicePrice==null) ? 0 : data.success.totalInvoicePrice;
						$scope.model.datainit.totalProduct = data.success.totalProduct;
						$scope.model.datainit.totalViewByDay = data.success.totalViewByDay[0].view_count;
						$scope.model.datainit.totalViewByMonth = data.success.totalViewByMonth[0].view_count;
						$scope.model.datainit.totalViewByYear = data.success.totalViewByYear[0].view_count;
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    	}
	    		},
	    		function (response) {
              
               var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
	    		}
	    	);
	};
	// apiGetTotalSaleByDateTime
	$scope.GetTotalSaleByDateTime = function() {
		$("#listReLoading").show();
		var request = {
	    };
	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetTotalSaleByDateTime", JSON.stringify(request))
	    .then(
               function (response) {
                  var data = response.data;

		    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
	            {
	                toastr.error(data.messages, Languages.common_error_title);

	                if(data.auth != "" && data.auth != null  && data.auth != undefined)
	                {
	                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
	                }
	            }
		    	else if(data.error != "" && data.error != null  && data.error != undefined)
		    	{
		    		toastr.error(data.error, Languages.common_error_title);
		    	}
		    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
		    	{
		    		toastr.warning(data.warning, Languages.common_warning_title);
		    	}
		    	else if(data.success != "" && data.success != null  && data.success != undefined)
		    	{
		    		$scope.model.chartSaleSystem.legend.layout=1;
		    		$scope.model.chartSaleSystem.title.text="Doanh Thu Hệ Thống Năm "+ $scope.model.datainit.currentYear;
		    		// tạo 12 tháng dữ liệu doanh số cho chi nhánh
		    		var dataListChart=[];
		    		for(var i=1;i<=12;i++){
		    			var flag="yes";
		    			angular.forEach(data.success, function(value, key) {
						    if(value.month == i && value.year == $scope.model.datainit.currentYear)
						    {
						    	dataListChart.push(parseFloat(value.total_sum));
						    	flag="no";
						    }
						});		
						if(flag=="yes") {
							dataListChart.push(0);
						}    		
		    		}
		    		// thực thể chi nhánh
		    		var obj = {
				  		values: dataListChart, 
				  		text: "Doanh Số Toàn Hệ Thống",
				  		"background-color":"blue orange",
				  	};
				  	// tạo chart theo chi nhánh obj
				  	$scope.model.chartSaleSystem.series.push(obj);

				  	$scope.model.datainit.dtgListSystemSale=data.success;
		    	}
		    	else
		    	{
		    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
		    		{
		    			toastr.error(data.systemerror, Languages.common_error_title);
		    			window.location.replace("./"+data.errorCode);
		    		}
		    		else
		    		{
		    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
		    		}
		    	}
		    	$("#listReLoading").hide();
	    	},
           	function (response) {
          		var data = response.data;
				toastr.error(Languages.common_error_exception, Languages.common_error_title);
				$("#listReLoading").hide();
		    });
	}
	// ReloadSystemList
	$scope.ReloadSystemList = function() {
		// clear data and init from the begin
		$scope.model.chartSaleSystem.series=[];

		if($scope.model.datainit.dtgListSystemSale.length==0)
		{
			alert("Chưa Có Thông Tin Hệ Thông !");
			return false;
		}

		$scope.model.chartSaleSystem.title.text="Doanh Thu Hệ Thống Năm "+ $scope.model.datainit.currentYear;
		
		// clear first value
		var dataListChart=[];

		for(var i=1;i<=12;i++){
			var flag="yes";
			angular.forEach($scope.model.datainit.dtgListSystemSale, function(value, key) {
			    if(value.month == i && value.year == $scope.model.datainit.currentYear)
			    {
			    	dataListChart.push(parseFloat(value.total_sum));
			    	flag="no";
			    }
			});		
			if(flag=="yes") {
				dataListChart.push(0);
			}    		
		}
		// thực thể chi nhánh
		var obj = {
	  		values: dataListChart, 
	  		text: "Doanh Số Toàn Hệ Thống",
			"background-color":"blue orange",
	  	};
	  	// tạo chart theo chi nhánh obj
	  	$scope.model.chartSaleSystem.series.push(obj);
	};
	// ==================================================================
	// GetCustomerSale
	$scope.GetCustomerSale = function() {

		$("#listReLoading").show();

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetCustomerSale")
	    	.then(
           		function (response) {
                  	var data = response.data;
			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    		$scope.model.datainit.slbSaleCustomer = [];
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		// toastr.warning(data.warning, Languages.common_warning_title);
			    		// $scope.model.datainit.slbSaleCustomer = [];
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		$scope.model.datainit.slbSaleCustomer = data.success;
			    		$scope.getTotal = function(){
						    var total = 0;
						    for(var i = 0; i < $scope.model.datainit.slbSaleCustomer.length; i++){
						        var branch = $scope.model.datainit.slbSaleCustomer[i];
						        total += parseFloat(branch.total_sum_format);
						    }
						    return total;
						}
						$scope.model.datainit.slbSaleCustomerSum = $scope.getTotal();
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    		$scope.model.datainit.slbSaleCustomer = [];
			    	}
			    	$("#listReLoading").hide();
			    },
               	function (response) {
                  	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$scope.model.datainit.slbSaleCustomer = [];
					$("#listReLoading").hide();
			    }
			);
	}
	// GetCustomerDebtSale
	$scope.GetCustomerDebtSale = function() {

		$("#listReLoading").show();

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetCustomerDebtSale")
	    	.then(
           		function (response) {
                  	var data = response.data;
			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    		$scope.model.datainit.slbDebtSaleCustomer = [];
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		// toastr.warning(data.warning, Languages.common_warning_title);
			    		// $scope.model.datainit.slbDebtSaleCustomer = [];
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		$scope.model.datainit.slbDebtSaleCustomer = data.success;
			    		$scope.getTotal = function(){
						    var total = 0;
						    for(var i = 0; i < $scope.model.datainit.slbDebtSaleCustomer.length; i++){
						        var branch = $scope.model.datainit.slbDebtSaleCustomer[i];
						        total += parseFloat(branch.total_sum_format);
						    }
						    return total;
						}
						$scope.model.datainit.slbDebtSaleCustomerSum = $scope.getTotal();
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    		$scope.model.datainit.slbDebtSaleCustomer = [];
			    	}
			    	$("#listReLoading").hide();
			    },
               	function (response) {
                  	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$scope.model.datainit.slbDebtSaleCustomer = [];
					$("#listReLoading").hide();
			    }
			);
	}
	// GetProductSale
	$scope.GetProductSale = function() {

		$("#listReLoading").show();

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetProductSale")
	    	.then(
           		function (response) {
                  	var data = response.data;
			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    		$scope.model.datainit.slbSaleProduct = [];
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		// toastr.warning(data.warning, Languages.common_warning_title);
			    		// $scope.model.datainit.slbSaleProduct = [];
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		$scope.model.datainit.slbSaleProduct = data.success;
			    		$scope.getTotal = function(){
						    var total = 0;
						    for(var i = 0; i < $scope.model.datainit.slbSaleProduct.length; i++){
						        var branch = $scope.model.datainit.slbSaleProduct[i];
						        total += parseFloat(branch.total_sum_format);
						    }
						    return total;
						}
						$scope.model.datainit.slbSaleProductSum = $scope.getTotal();
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    		$scope.model.datainit.slbSaleProduct = [];
			    	}
			    	$("#listReLoading").hide();
			    },
               	function (response) {
                  	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$scope.model.datainit.slbSaleProduct = [];
					$("#listReLoading").hide();
			    }
			);
	}
	// GetProductDebtSale
	$scope.GetProductDebtSale = function() {

		$("#listReLoading").show();

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetProductDebtSale")
	    	.then(
           		function (response) {
                  	var data = response.data;
			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    		$scope.model.datainit.slbDebtSaleProduct = [];
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		// toastr.warning(data.warning, Languages.common_warning_title);
			    		// $scope.model.datainit.slbDebtSaleProduct = [];
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		$scope.model.datainit.slbDebtSaleProduct = data.success;
			    		$scope.getTotal = function(){
						    var total = 0;
						    for(var i = 0; i < $scope.model.datainit.slbDebtSaleProduct.length; i++){
						        var branch = $scope.model.datainit.slbDebtSaleProduct[i];
						        total += parseFloat(branch.total_sum_format);
						    }
						    return total;
						}
						$scope.model.datainit.slbDebtSaleProductSum = $scope.getTotal();
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    		$scope.model.datainit.slbDebtSaleProduct = [];
			    	}
			    	$("#listReLoading").hide();
			    },
               	function (response) {
                  	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$scope.model.datainit.slbDebtSaleProduct = [];
					$("#listReLoading").hide();
			    }
			);
	}
	// GetBranchSale
	$scope.GetBranchSale = function() {

		$("#listReLoading").show();

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetBranchSale")
	    	.then(
           		function (response) {
                  	var data = response.data;
			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    		$scope.model.datainit.slbSaleBranch = [];
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		// toastr.warning(data.warning, Languages.common_warning_title);
			    		// $scope.model.datainit.slbSaleBranch = [];
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		$scope.model.datainit.slbSaleBranch = data.success;
			    		$scope.getTotal = function(){
						    var total = 0;
						    for(var i = 0; i < $scope.model.datainit.slbSaleBranch.length; i++){
						        var branch = $scope.model.datainit.slbSaleBranch[i];
						        total += parseFloat(branch.total_sum_format);
						    }
						    return total;
						}
						$scope.model.datainit.slbSaleBranchSum = $scope.getTotal();
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    		$scope.model.datainit.slbSaleBranch = [];
			    	}
			    	$("#listReLoading").hide();
			    },
               	function (response) {
                  	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$scope.model.datainit.slbSaleBranch = [];
					$("#listReLoading").hide();
			    }
			);
	}
	// GetBranchDebtSale
	$scope.GetBranchDebtSale = function() {

		$("#listReLoading").show();

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetBranchDebtSale")
	    	.then(
           		function (response) {
                  	var data = response.data;
			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    		$scope.model.datainit.slbDebtSaleBranch = [];
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		// toastr.warning(data.warning, Languages.common_warning_title);
			    		// $scope.model.datainit.slbDebtSaleBranch = [];
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		$scope.model.datainit.slbDebtSaleBranch = data.success;
			    		$scope.getTotal = function(){
						    var total = 0;
						    for(var i = 0; i < $scope.model.datainit.slbDebtSaleBranch.length; i++){
						        var branch = $scope.model.datainit.slbDebtSaleBranch[i];
						        total += parseFloat(branch.total_sum_format);
						    }
						    return total;
						}
						$scope.model.datainit.slbDebtSaleBranchSum=$scope.getTotal();
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    		$scope.model.datainit.slbDebtSaleBranch = [];
			    	}
			    	$("#listReLoading").hide();
			    },
               	function (response) {
                  	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$scope.model.datainit.slbDebtSaleBranch = [];
					$("#listReLoading").hide();
			    }
			);
	}
	// GetSubadminSale
	$scope.GetSubadminSale = function() {

		$("#listReLoading").show();

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetSubadminSale")
	    	.then(
           		function (response) {
                  	var data = response.data;
			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    		$scope.model.datainit.slbSaleSubadmin = [];
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		// toastr.warning(data.warning, Languages.common_warning_title);
			    		// $scope.model.datainit.slbSaleSubadmin = [];
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		$scope.model.datainit.slbSaleSubadmin = data.success;
			    		$scope.getTotal = function(){
						    var total = 0;
						    for(var i = 0; i < $scope.model.datainit.slbSaleSubadmin.length; i++){
						        var branch = $scope.model.datainit.slbSaleSubadmin[i];
						        total += parseFloat(branch.total_sum_format);
						    }
						    return total;
						}
						$scope.model.datainit.slbSaleSubadminSum = $scope.getTotal();
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    		$scope.model.datainit.slbSaleSubadmin = [];
			    	}
			    	$("#listReLoading").hide();
			    },
               	function (response) {
                  	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$scope.model.datainit.slbSaleSubadmin = [];
					$("#listReLoading").hide();
			    }
			);
	}
	// GetSubadminDebtSale
	$scope.GetSubadminDebtSale = function() {

		$("#listReLoading").show();

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetSubadminDebtSale")
	    	.then(
           		function (response) {
                  	var data = response.data;
			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    		$scope.model.datainit.slbDebtSubadmin = [];
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		// toastr.warning(data.warning, Languages.common_warning_title);
			    		// $scope.model.datainit.slbDebtSubadmin = [];
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		$scope.model.datainit.slbDebtSubadmin = data.success;
			    		$scope.getTotal = function(){
						    var total = 0;
						    for(var i = 0; i < $scope.model.datainit.slbDebtSubadmin.length; i++){
						        var branch = $scope.model.datainit.slbDebtSubadmin[i];
						        total += parseFloat(branch.total_sum_format);
						    }
						    return total;
						}
						$scope.model.datainit.slbDebtSubadminSum = $scope.getTotal();
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    		$scope.model.datainit.slbDebtSubadmin = [];
			    	}
			    	$("#listReLoading").hide();
			    },
               	function (response) {
                  	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$scope.model.datainit.slbDebtSubadmin = [];
					$("#listReLoading").hide();
			    }
			);
	}
});