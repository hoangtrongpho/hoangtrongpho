/*!
 *
 *	Develop by Vinh Nguyễn - Ho Chi Minh City - 2017
 * 	Copyright Â© 2016-2017 Skyfire Team. All rights reserved.
 *	Design by Skyfire Team 2.3.3
 *
 *	Phone: 0975 578 276
 *	Skype: enjoyvinh
 *
 */

"use strict";

// app.js
// create angular app
var dashboardApp = angular.module('dashboardApp', ['commonApp']);

dashboardApp.factory('dashboardModels', function() {

	var model={
		datainit : {
			salesChartOptions : {
				//Boolean - If we should show the scale at all
				showScale: true,
				//Boolean - Whether grid lines are shown across the chart
				scaleShowGridLines: true,
				//String - Colour of the grid lines
				scaleGridLineColor: "rgba(0,0,0,.1)",
				//Number - Width of the grid lines
				scaleGridLineWidth: 1,
				//Boolean - Whether to show horizontal lines (except X axis)
				scaleShowHorizontalLines: true,
				//Boolean - Whether to show vertical lines (except Y axis)
				scaleShowVerticalLines: true,
				//Boolean - Whether the line is curved between points
				bezierCurve: true,
				//Number - Tension of the bezier curve between points
				bezierCurveTension: 0.3,
				//Boolean - Whether to show a dot for each point
				pointDot: true,
				//Number - Radius of each point dot in pixels
				pointDotRadius: 4,
				//Number - Pixel width of point dot stroke
				pointDotStrokeWidth: 2,
				//Number - amount extra to add to the radius to cater for hit detection outside the drawn point
				pointHitDetectionRadius: 20,
				//Boolean - Whether to show a stroke for datasets
				datasetStroke: true,
				//Number - Pixel width of dataset stroke
				datasetStrokeWidth: 2,
				//Boolean - Whether to fill the dataset with a color
				datasetFill: true,
				//String - A legend template
				legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li>Ngày <span style=\"background-color:<%=datasets[i].lineColor%>\"></span> Có <%if(datasets[i].label){%><%=datasets[i].label%><%}%> Hóa Đơn</li><%}%></ul>",
				//Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
				maintainAspectRatio: true,
				//Boolean - whether to make the chart responsive to window resizing
				responsive: true
		   	},
		   	barChartOptions : {
				//Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
				scaleBeginAtZero: true,
				//Boolean - Whether grid lines are shown across the chart
				scaleShowGridLines: true,
				//String - Colour of the grid lines
				scaleGridLineColor: "rgba(170,0,0,.1)",
				//Number - Width of the grid lines
				scaleGridLineWidth: 1,
				//Boolean - Whether to show horizontal lines (except X axis)
				scaleShowHorizontalLines: true,
				//Boolean - Whether to show vertical lines (except Y axis)
				scaleShowVerticalLines: true,
				//Boolean - If there is a stroke on each bar
				barShowStroke: true,
				//Number - Pixel width of the bar stroke
				barStrokeWidth: 2,
				//Number - Spacing between each of the X value sets
				barValueSpacing: 1,
				//Number - Spacing between data sets within X values
				barDatasetSpacing: 10,
				//String - A legend template
				legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
				//Boolean - whether to make the chart responsive
				responsive: true,
				maintainAspectRatio: true,
				//Boolean - Whether to fill the dataset with a color
				datasetFill: true,

				// String - Template string for single tooltips
				tooltipTemplate: "<%if (label){%> Tháng <%=label%> có <%}%><%= value %>.",
				// Boolean - Determines whether to draw tooltips on the canvas or not
			    // showTooltips: true,
			    // Array - Array of string names to attach tooltip events
			    // tooltipEvents: ["mousemove", "touchstart", "touchmove"],
			    // String - Tooltip background colour
			    tooltipFillColor: "rgba(0,0,0,1)",
			    // String - Tooltip label font declaration for the scale label
			    tooltipFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
			    // Number - Tooltip label font size in pixels
			    tooltipFontSize: 14,
			    // String - Tooltip font weight style
			    tooltipFontStyle: "normal",
			    // String - Tooltip label font colour
			    tooltipFontColor: "#fff",
			    // String - Tooltip title font declaration for the scale label
			    tooltipTitleFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
			    // Number - Tooltip title font size in pixels
			    tooltipTitleFontSize: 14,
			    // String - Tooltip title font weight style
			    tooltipTitleFontStyle: "bold",
			    // String - Tooltip title font colour
			    tooltipTitleFontColor: "#fff",
			    // Number - pixel width of padding around tooltip text
			    // tooltipYPadding: 6,
			    // Number - pixel width of padding around tooltip text
			    // tooltipXPadding: 6,
			    // Number - Size of the caret on the tooltip
			    // tooltipCaretSize: 8,
			    // Number - Pixel radius of the tooltip border
			    // tooltipCornerRadius: 6,
			    // Number - Pixel offset from point x to tooltip edge
			    // tooltipXOffset: 10,
			    // String - Template string for single tooltips
			    // multiTooltipTemplate: "<%= value %>",
		    },

		},
		totalInvoicePrice: 0,
		totalInvoice :0,
		uncompletedInvoice:0,
		totalProduct:0,
		totalNews : 0,
		totalView : 0,
		totalAgency : 0,
		totalRevenue : 0,
		preLoading : true,
		accessPreLoading : true,
		appURL : "http://localhost:8000"
	};

	model.request = {
		currentMonth : moment(),
		accessCurrentMonth : moment(),
	};

	return model;
});

// create angular controller
dashboardApp.controller('dashboardController', function(dashboardModels, commonFunction, $scope, $rootScope, $http, $filter, $window, $location, $timeout, toastr) {

	$scope.model = dashboardModels;

	$scope.pageInit = function() {

		$scope.model.datainit.appURL = $('meta[name="app_url"]').attr('content');

		//$scope.loadChartByMonth();

		$scope.loadChartByMonth();

		// $scope.chartaccessloadChartByMonth();

		$http.post($scope.model.datainit.appURL + "/SystemApi/GetDashboard")
			.then(
				function (response) {
					var data = response.data;

			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		                
		                $scope.model.datainit.dtgListNews = [];
		            }
			    	if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		toastr.warning(data.warning, Languages.common_warning_title);
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		//list_cate_id : !$scope.model.request.slbCateParent ? 1 : 0,
			    		$scope.model.datainit.totalInvoice = data.success.totalInvoice;
			    		$scope.model.datainit.totalView = data.success.totalView;
			    		$scope.model.datainit.totalInvoicePrice = (data.success.totalInvoicePrice==null) ? 0 : data.success.totalInvoicePrice;
			    		$scope.model.datainit.totalRevenue = data.success.totalRevenue;
						$scope.model.datainit.uncompletedInvoice = data.success.uncompletedInvoice;
						$scope.model.datainit.totalProduct = data.success.totalProduct;
						$scope.model.datainit.totalNews = data.success.totalNews;
						
						
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    	}
	    		},
	    		function (response) {
              
               var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
	    		}
	    	);

		$scope.model.datainit.preLoading = false;

	};

	$scope.previousMonth = function() {
		$scope.model.datainit.preLoading = true;
		var cd = $scope.model.request.currentMonth.subtract(1, 'month');
		$scope.model.request.currentMonth = cd;
		$scope.loadChartByMonth(cd);
	}

	$scope.nextMonth = function() {
		$scope.model.datainit.preLoading = true;
		var cd = $scope.model.request.currentMonth.add(1, 'month');
		$scope.model.request.currentMonth = cd;
		$scope.loadChartByMonth(cd);
	}

	$scope.reloadMonth = function() {
		$scope.model.datainit.preLoading = true;
		$scope.model.request.currentMonth = moment();
		$scope.loadChartByMonth($scope.model.request.currentMonth);
	}

	$scope.loadChartByMonth = function(current) {

		$("#chartContent").html('<canvas id="charByMonth" style="height: 400px;"></canvas>');

		$scope.model.datainit.preLoading = true;

		$scope.barChartCanvas = $("#charByMonth").get(0).getContext("2d");

		if(current === undefined)
		{
			current = moment();
		}

	    var currentMonth = current.month() +　1;

	    var currentYear = current.year();

	    var leftChartTitle = "Trong tháng "+currentMonth+"/"+currentYear;
	    $("#leftChartTitle").html(leftChartTitle);

	    var firstDay = new Date(currentYear, currentMonth, 1);
		var lastDay = new Date(currentYear, currentMonth, 0);

		// var listLabel = ["Tháng 1","Tháng 2","Tháng 3","Tháng 4","Tháng 5","Tháng 6","Tháng 7","Tháng 8","Tháng 9","Tháng 10","Tháng 11","Tháng 12"];

		var listLabel = []
		// $scope.listData = [1,2,3,4,5,6,7,8,9,10,11,12];

		for (var i = firstDay.getDate(); i <= lastDay.getDate(); i++) {
		    listLabel.push(i);
		}

		var request = {
			type : "m",
	    	firstDay : firstDay.getDate(),
	    	lastDay : lastDay.getDate(),
	    	month : currentMonth,
	    	year : currentYear
	    };

		$http.post($scope.model.datainit.appURL + "/SystemApi/GetRevenueByYear", JSON.stringify(request))
	   	.then(
				function (response) {
					var data = response.data;

			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
			         {
			             toastr.error(data.messages, Languages.common_error_title);

			             if(data.auth != "" && data.auth != null  && data.auth != undefined)
			             {
			             	$window.location.href = $scope.model.datainit.appURL + data.redirect;
			             }

			             $scope.model.datainit.dtgListNews = [];
			         }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    		$scope.model.datainit.dtgListNews = [];
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		toastr.warning(data.warning, Languages.common_warning_title);
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		$scope.listData = data.success;

			    		var totalCounter = 0;
						for (var i = 0; i < $scope.listData.length; i++) {
						    totalCounter += parseInt($scope.listData[i]);
						}

						var rightChartTitle = "có tổng " + $filter('number')(totalCounter) + " hóa đơn giao dịch thành công";
			    		$("#rightChartTitle").html(rightChartTitle);

					    $scope.barChart = new Chart($scope.barChartCanvas);

					    var barChartData = {
							labels: listLabel,
							datasets: [
								{
									label: "Doanh Thu",
									fillColor: "rgba(170,0,0,0.5)",
									strokeColor: "rgba(170,0,0,0.5)",
									pointColor: "rgba(170,0,0,0.7)",
									pointStrokeColor: "rgba(170,0,0,0.7)",
									pointHighlightFill: "#fff",
									pointHighlightStroke: "rgba(170,0,0,0.5)",
									data: $scope.listData
								},
							],
						};

						$timeout(function() {
							$scope.barChart.Line(barChartData, $scope.model.datainit.salesChartOptions); 
							$scope.model.datainit.preLoading = false;  
						}, 500);
						
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    	}
			   },
				function (response) {
             
               var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
		    	}
		  	);
	};
	
	$scope.pageInit();

});