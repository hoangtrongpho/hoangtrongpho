/*!
 *
 *	Develop by Vinh Nguyễn - Ho Chi Minh City - 2017
 * 	Copyright Â© 2016-2017 Skyfire Team. All rights reserved.
 *	Design by Skyfire Team 2.3.3
 *
 *	Phone: 0975 578 276
 *	Skype: enjoyvinh
 *
 */

"use strict";

// app.js
// create angular app
var newsNewsApp = angular.module('newsNewsApp', ['commonApp', 'daterangepicker', 'isteven-multi-select', 'ngSanitize',
		'datatables','datatables.colreorder','datatables.bootstrap']);

newsNewsApp.factory('NewsModels', function() {

	var model={
		datainit : {
			slbListCategories : [],
			dtgListNews : [],
			appURL : $('meta[name="app_url"]').attr('content')
		},
	};

	model.request = {
		dtpPublishes: {
			startDate: moment().subtract(3,"year").startOf('month'), 
			endDate: moment()
		},
		txtNewsName : "",
		slbCateParent : "",
	};

	model.datePickerOpts = {
		showDropdowns : true,
		singleDatePicker : false,
		timePicker : false,
		timePickerSeconds : false,
		timePicker24Hour : true,
		alwaysShowCalendars : false,
		showWeekNumbers : true,
		autoApply : true,
		opens: "right",
		drops: "down",
		clearLabel: Languages.common_button_cancel,
		cancelClass : 'btn-default',
		autoUpdateInput : true,
	    parentEl: '.cal-block .form-group .oneway',
        locale: {
            buttonClasses : 'btn btn-sm',
	        applyClass : 'btn-success',
            applyLabel: Languages.common_button_select_item,
            fromLabel: Languages.common_label_from,
            format: "DD/MM/YYYY",
            toLabel: Languages.common_label_to,
            weekLabel: Languages.datepicker_week_title,
            customRangeLabel: Languages.common_button_select_date_time,
            daysOfWeek: [
            				Languages.datepicker_week_monday,
            				Languages.datepicker_week_tuesday,
            				Languages.datepicker_week_wednesday,
            				Languages.datepicker_week_thursday,
            				Languages.datepicker_week_friday,
            				Languages.datepicker_week_saturday,
            				Languages.datepicker_week_sunday
            			],
            monthNames: [
	            			Languages.datepicker_month_january, 
	            			Languages.datepicker_month_february,
	            			Languages.datepicker_month_march,
	            			Languages.datepicker_month_april,
	            			Languages.datepicker_month_may,
	            			Languages.datepicker_month_june,
	            			Languages.datepicker_month_july,
	            			Languages.datepicker_month_august,
	            			Languages.datepicker_month_september,
	            			Languages.datepicker_month_october,	
	            			Languages.datepicker_month_november,
	            			Languages.datepicker_month_december
            			],
        },
        ranges: {
   			'Hôm Nay': [moment(), moment()],
			'Từ Hôm Qua': [moment().subtract(1,'days'), moment()],
			'Từ 7 Ngày Trước': [moment().subtract(7,'days'), moment()],
			'Tháng Này': [moment().startOf('month'), moment()],
			'Từ 1 Tháng Trước': [moment().subtract(1,'month').startOf('month'), moment()],
			'Từ 3 Tháng Trước': [moment().subtract(3,'month').startOf('month'), moment()]
        },
        
    };

	model.localLang = {
	    selectAll       : Languages.common_button_select_all,
	    selectNone      : Languages.common_button_deselected_all,
	    reset           : Languages.common_button_cancel,
	    search          : Languages.common_input_label_search,
	    nothingSelected : Languages.common_input_label_select        //default-label is deprecated and replaced with this.
	}

	return model;
});

// create angular controller
newsNewsApp.controller('newsNewsController', function(NewsModels, commonFunction, $scope, $rootScope, $http, $filter, $window, $location, toastr,
	DTOptionsBuilder, DTColumnDefBuilder,DTColumnBuilder) {

	$scope.model = NewsModels;

	$scope.pageInit = function() {

		$scope.model.datainit.appURL = $('meta[name="app_url"]').attr('content');

		$("#listReLoading").show();

		$http.post($scope.model.datainit.appURL + "/SystemApi/GetListNewsCategories")
	    	.then(
               	function (response) {
                  	var data = response.data;

			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    		$scope.model.datainit.slbListCategories=[];
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
	    		warning = warning.replace('["', '').replace('"]', '');
	    		toastr.warning(warning, Languages.common_warning_title);
			    		$scope.model.datainit.slbListCategories=[];
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		$scope.model.datainit.slbListCategories = data.success;
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    		$scope.model.datainit.slbListCategories=[];
			    	}
			    },
               	function (response) {
                  	debugger;
                  	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$scope.model.datainit.slbListCategories=[];
			    }
			);

		$scope.loadNewsList();

		$scope.dtOptions = DTOptionsBuilder.newOptions()
			.withOption('order', [])
        	.withPaginationType('full_numbers')
        	.withDisplayLength(10)
	        .withColReorder()
	        // .withColReorderOrder([0, 1, 2, 3, 4])
	        .withBootstrap()
	        .withBootstrapOptions({
	            pagination: {
	                classes: {
	                    ul: 'pagination pagination-md'
	                }
	            }
	        })
	        .withLanguage({
	            "sEmptyTable":     "Không có dữ liệu.",
	            "sInfo":           "Hiển Thị Từ _START_ Đến _END_ Của _TOTAL_ Kết Quả",
	            "sInfoEmpty":      "Hiển Thị Từ 0 Đến 0 Của 0 Kết Quả",
	            "sInfoFiltered":   "(Đã Lọc Từ Tổng _MAX_ Kết Quả)",
	            "sInfoPostFix":    "",
	            "sInfoThousands":  ",",
	            "sLengthMenu":     "Hiển Thị&nbsp;&nbsp;&nbsp;_MENU_&nbsp;&nbsp;&nbsp;Kết Quả / 1 Trang",
	            "sLoadingRecords": "Đang Tải Dữ Liệu...",
	            "sProcessing":     "Đang Xử Lý Dữ Liệu...",
	            "sSearch":         "Từ Khóa: ",
	            "sZeroRecords":    "Không tìm thấy dữ liệu trùng khớp.",
	            "oPaginate": {
	                "sFirst":    "Đầu",
	                "sLast":     "Cuối",
	                "sNext":     "Tiếp",
	                "sPrevious": "Trước"
	            },
	            "oAria": {
	                "sSortAscending":  ": activate to sort column ascending",
	                "sSortDescending": ": activate to sort column descending"
	            }
	        })
	        .withOption('responsive', true);

	    $scope.dtColumnDefs = [
	        DTColumnDefBuilder.newColumnDef(0).notSortable(),
	    ];

	};
	
	$scope.loadNewsList = function() {

		$("#listReLoading").show();

		var request = {
	        news_title : $scope.model.request.txtNewsName,
	        news_date_public : $scope.model.request.dtpPublishes.startDate,
	        news_date_expires : $scope.model.request.dtpPublishes.endDate,
	        list_cate_id : !$scope.model.request.slbCateParent ? [] : $scope.model.request.slbCateParent,
	    };

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetListNews", JSON.stringify(request))
	    	.then(
               	function (response) {
                  	var data = response.data;

			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    		$scope.model.datainit.dtgListNews = [];
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
	    		warning = warning.replace('["', '').replace('"]', '');
	    		toastr.warning(warning, Languages.common_warning_title);
			    		$scope.model.datainit.dtgListNews = [];
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		$scope.model.datainit.dtgListNews = [];

			    		angular.forEach(data.success, function(value, key) {

			    			var item = value;

						    item.news_release_date = new Date(value.news_release_date);
						    item.news_expiration_date = new Date(value.news_expiration_date);

						    $scope.model.datainit.dtgListNews.push(item);
						});
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    		$scope.model.datainit.dtgListNews = [];
			    	}
			    	$("#listReLoading").hide();
			    },
               	function (response) {
                 	debugger;
                  	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$scope.model.datainit.dtgListNews = [];
					$("#listReLoading").hide();
			    }
			);
	}

	$scope.removeItem = function(id) {

    	if(id == "" || id == null || id == undefined)
		{
			toastr.error(Resources.common_error_exception, Resources.common_error_title);
			return false;
		}
    	
		var confirmModal = bootbox.dialog({
			message: commonFunction.languageReplace(Languages.common_confirm_delete_question,[' Bản Tin']),
		  	title: commonFunction.languageReplace(Languages.common_confirm_delete_title,[' Bản Tin']),
		  	closeButton: true,
		  	animate: true,
		  	className: "confirmModal",
		  	backdrop: true,
		  	headerBackground: "bg-red",
		  	size: "small",
		  	locale: "vi",
		  	show: true,
		  	buttons: {
		  		cancel : {
			      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
			      	className: "btn-default",
			      	callback: function() {
			        	confirmModal.modal('hide');
			      	}
			    },
			    success: {
		      		label: '<i class="fa fa-trash" aria-hidden="true"></i>&nbsp;'+Languages.common_button_remove,
			      	className: "btn-danger",
			      	callback: function() {

			      		$("#addReLoading").show();

			        	var request = {
					        news_id : id,
					    };

					    $http.post($scope.model.datainit.appURL + "/SystemApi/RemoveNews", JSON.stringify(request))
					    	.then(
               					function (response) {
                  					var data = response.data;

							    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
						            {
						                toastr.error(data.messages, Languages.common_error_title);

						                if(data.auth != "" && data.auth != null  && data.auth != undefined)
						                {
						                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
						                }
						            }
							    	else if(data.error != "" && data.error != null  && data.error != undefined)
							    	{
							    		toastr.error(data.error, Languages.common_error_title);
							    	}
							    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
							    	{
							    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
	    		warning = warning.replace('["', '').replace('"]', '');
	    		toastr.warning(warning, Languages.common_warning_title);
							    	}
							    	else if(data.success != "" && data.success != null  && data.success != undefined)
							    	{
							    		toastr.success(data.success, commonFunction.languageReplace(Languages.common_success_removed,['Bản Tin']));
							    		$scope.loadNewsList();
							    	}
							    	else
							    	{
							    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
							    		{
							    			toastr.error(data.systemerror, Languages.common_error_title);
							    			window.location.replace("./"+data.errorCode);
							    		}
							    		else
							    		{
							    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
							    		}
							    	}
							    	$("#addReLoading").hide();
					    		},
					    		function (response) {
				                 	debugger;
				                  	var data = response.data;	
									toastr.error(Languages.common_error_exception, Languages.common_error_title);
									$("#addReLoading").hide();
					    		}
					    	);
			      	}
			    }
		  	}
		});
    };

   	$scope.changeStatus = function(id,status) {

		if(id == "" || id == null || id == undefined)
		{
			toastr.error(Resources.common_error_exception, Resources.common_error_title);
			return false;
		}
			
		var confirmModal = bootbox.dialog({
		  	message: status == 0 ? "Bạn Có Muốn Ẩn Tin Tức?" : "Bạn Có Muốn Hiện Tin Tức?" ,
		  	title: "Đổi Trạng Thái Tin Tức",
		  	closeButton: true,
		  	animate: true,
		  	className: "confirmModal",
		  	backdrop: true,
		  	headerBackground: "bg-yellow",
		  	size: "small",
		  	locale: "vi",
		  	show: true,
		  	buttons: {
		  		cancel : {
			      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
			      	className: "btn-default",
			      	callback: function() {
			        	confirmModal.modal('hide');
			      	}
			    },
			    success: {
		      		label: '<i class="fa fa-trash" aria-hidden="true"></i>&nbsp;'+Languages.common_button_update,
			      	className: "btn-warning",
			      	callback: function() {

			      		$("#listReLoading").show();

						//Khởi Tạo Dữ Liệu
						var request = {
					        news_id : id,
					        status : status
					    };

					    $http.post($scope.model.datainit.appURL + "/SystemApi/ChangeStatusNews", JSON.stringify(request))
					    	.then(
           						function (response) {
                  					var data = response.data;

							    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
						            {
						                toastr.error(data.messages, Languages.common_error_title);

						                if(data.auth != "" && data.auth != null  && data.auth != undefined)
						                {
						                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
						                }
						            }
							    	else if(data.error != "" && data.error != null  && data.error != undefined)
							    	{
							    		toastr.error(data.error, Languages.common_error_title);
							    	}
							    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
							    	{
										var warning = String(data.warning[Object.keys(data.warning)[0]]);
							    		warning = warning.replace('["', '').replace('"]', '');
							    		toastr.warning(warning, Languages.common_warning_title);
							    	}
							    	else if(data.success != "" && data.success != null  && data.success != undefined)
							    	{
							    		toastr.success(data.success, Languages.common_success_title);
							    		$scope.loadNewsList();
							    	}
							    	else
							    	{
							    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
							    		{
							    			toastr.error(data.systemerror, Languages.common_error_title);
							    			window.location.replace("./"+data.errorCode);
							    		}
							    		else
							    		{
							    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
							    		}
							    	}
							    	$("#listReLoading").hide();
							    },
					    		function (response) {
				                 	debugger;
				                  	var data = response.data;	
									toastr.error(Languages.common_error_exception, Languages.common_error_title);
									$("#addReLoading").hide();
					    		}
					    	);
			      	}
			    }
		  	}
		});
	};
	
});