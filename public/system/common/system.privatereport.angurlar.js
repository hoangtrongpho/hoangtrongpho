/*!
 *
 *	Develop by Vinh Nguyễn - Ho Chi Minh City - 2017
 * 	Copyright Â© 2016-2017 Skyfire Team. All rights reserved.
 *	Design by Skyfire Team 2.3.3
 *
 *	Phone: 0975 578 276
 *	Skype: enjoyvinh
 *
 */

"use strict";

// app.js
// create angular app
var dashboardApp = angular.module('dashboardApp', ['commonApp','datatables','datatables.colreorder','datatables.bootstrap']);

dashboardApp.factory('dashboardModels', function() {

	var model={
		datainit : {
		
			slbSaleCustomer:[],
			slbSaleCustomerSum:0,
			slbDebtSaleCustomer:[],
			slbDebtSaleCustomerSum:0,

			slbSaleProduct:[],
			slbSaleProductSum:0,
			slbDebtSaleProduct:[],
			slbDebtSaleProductSum:0,

			slbSaleSubadmin:[],
			slbSaleSubadminSum:0,
			slbDebtSubadmin:[],
			slbDebtSubadminSum:0,
		},
	
		preLoading : true,
		accessPreLoading : true,
		appURL : "http://localhost:8000",

	};

	model.request = {
	};

	return model;
});

// create angular controller
dashboardApp.controller('dashboardController', function(dashboardModels, commonFunction, $scope, $rootScope, $http, $filter, $window, $location, $timeout, toastr,DTOptionsBuilder, DTColumnDefBuilder,DTColumnBuilder) {

	$scope.model = dashboardModels;

	$scope.pageInit = function() {

		$scope.model.datainit.appURL = $('meta[name="app_url"]').attr('content');
		
		$scope.GetPrivateCustomerSale();
		$scope.GetPrivateCustomerDebtSale();
		$scope.GetPrivateProductSale();	
		$scope.GetPrivateProductDebtSale();
		$scope.GetPrivateSubadminSale();
		$scope.GetPrivateSubadminDebtSale();

	
		$scope.model.datainit.preLoading = false;

		$scope.dtOptions = DTOptionsBuilder.newOptions()
			.withOption('order', [])
        	.withPaginationType('full_numbers')
        	.withDisplayLength(10)
	        .withColReorder()
	        // .withColReorderOrder([0, 1, 2, 3, 4])
	        .withBootstrap()
	        .withBootstrapOptions({
	            pagination: {
	                classes: {
	                    ul: 'pagination pagination-md'
	                }
	            }
	        })
	        .withLanguage({
	            "sEmptyTable":     "Không có dữ liệu.",
	            "sInfo":           "Hiển Thị Từ _START_ Đến _END_ Của _TOTAL_ Kết Quả",
	            "sInfoEmpty":      "Hiển Thị Từ 0 Đến 0 Của 0 Kết Quả",
	            "sInfoFiltered":   "(Đã Lọc Từ Tổng _MAX_ Kết Quả)",
	            "sInfoPostFix":    "",
	            "sInfoThousands":  ",",
	            "sLengthMenu":     "Hiển Thị&nbsp;&nbsp;&nbsp;_MENU_&nbsp;&nbsp;&nbsp;Kết Quả / 1 Trang",
	            "sLoadingRecords": "Đang Tải Dữ Liệu...",
	            "sProcessing":     "Đang Xử Lý Dữ Liệu...",
	            "sSearch":         "Từ Khóa: ",
	            "sZeroRecords":    "Không tìm thấy dữ liệu trùng khớp.",
	            "oPaginate": {
	                "sFirst":    "Đầu",
	                "sLast":     "Cuối",
	                "sNext":     "Tiếp",
	                "sPrevious": "Trước"
	            },
	            "oAria": {
	                "sSortAscending":  ": activate to sort column ascending",
	                "sSortDescending": ": activate to sort column descending"
	            }
	        })
	        .withOption('responsive', true);

	    $scope.dtColumnDefs = [
	        DTColumnDefBuilder.newColumnDef(0).notSortable(),
	    ];

	};

	// GetCustomerSale
	$scope.GetPrivateCustomerSale = function() {

		$("#listReLoading").show();

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetPrivateCustomerSale")
	    	.then(
           		function (response) {
                  	var data = response.data;
			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    		$scope.model.datainit.slbSaleCustomer = [];
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		// toastr.warning(data.warning, Languages.common_warning_title);
			    		// $scope.model.datainit.slbSaleCustomer = [];
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		$scope.model.datainit.slbSaleCustomer = data.success;
			    		$scope.getTotal = function(){
						    var total = 0;
						    for(var i = 0; i < $scope.model.datainit.slbSaleCustomer.length; i++){
						        var branch = $scope.model.datainit.slbSaleCustomer[i];
						        total += parseFloat(branch.total_sum_format);
						    }
						    return total;
						}
						$scope.model.datainit.slbSaleCustomerSum = $scope.getTotal();
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    		$scope.model.datainit.slbSaleCustomer = [];
			    	}
			    	$("#listReLoading").hide();
			    },
               	function (response) {
                  	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$scope.model.datainit.slbSaleCustomer = [];
					$("#listReLoading").hide();
			    }
			);
	}
	// GetPrivateCustomerDebtSale
	$scope.GetPrivateCustomerDebtSale = function() {

		$("#listReLoading").show();

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetPrivateCustomerDebtSale")
	    	.then(
           		function (response) {
                  	var data = response.data;
			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    		$scope.model.datainit.slbDebtSaleCustomer = [];
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		// toastr.warning(data.warning, Languages.common_warning_title);
			    		// $scope.model.datainit.slbDebtSaleCustomer = [];
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		$scope.model.datainit.slbDebtSaleCustomer = data.success;
			    		$scope.getTotal = function(){
						    var total = 0;
						    for(var i = 0; i < $scope.model.datainit.slbDebtSaleCustomer.length; i++){
						        var branch = $scope.model.datainit.slbDebtSaleCustomer[i];
						        total += parseFloat(branch.total_sum_format);
						    }
						    return total;
						}
						$scope.model.datainit.slbDebtSaleCustomerSum = $scope.getTotal();
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    		$scope.model.datainit.slbDebtSaleCustomer = [];
			    	}
			    	$("#listReLoading").hide();
			    },
               	function (response) {
                  	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$scope.model.datainit.slbDebtSaleCustomer = [];
					$("#listReLoading").hide();
			    }
			);
	}
	// GetPrivateProductSale
	$scope.GetPrivateProductSale = function() {

		$("#listReLoading").show();

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetPrivateProductSale")
	    	.then(
           		function (response) {
                  	var data = response.data;
			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    		$scope.model.datainit.slbSaleProduct = [];
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		// toastr.warning(data.warning, Languages.common_warning_title);
			    		// $scope.model.datainit.slbSaleProduct = [];
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		$scope.model.datainit.slbSaleProduct = data.success;
			    		$scope.getTotal = function(){
						    var total = 0;
						    for(var i = 0; i < $scope.model.datainit.slbSaleProduct.length; i++){
						        var branch = $scope.model.datainit.slbSaleProduct[i];
						        total += parseFloat(branch.total_sum_format);
						    }
						    return total;
						}
						$scope.model.datainit.slbSaleProductSum = $scope.getTotal();
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    		$scope.model.datainit.slbSaleProduct = [];
			    	}
			    	$("#listReLoading").hide();
			    },
               	function (response) {
                  	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$scope.model.datainit.slbSaleProduct = [];
					$("#listReLoading").hide();
			    }
			);
	}
	// GetPrivateProductDebtSale
	$scope.GetPrivateProductDebtSale = function() {

		$("#listReLoading").show();

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetPrivateProductDebtSale")
	    	.then(
           		function (response) {
                  	var data = response.data;
			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    		$scope.model.datainit.slbDebtSaleProduct = [];
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		// toastr.warning(data.warning, Languages.common_warning_title);
			    		// $scope.model.datainit.slbDebtSaleProduct = [];
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		$scope.model.datainit.slbDebtSaleProduct = data.success;
			    		$scope.getTotal = function(){
						    var total = 0;
						    for(var i = 0; i < $scope.model.datainit.slbDebtSaleProduct.length; i++){
						        var branch = $scope.model.datainit.slbDebtSaleProduct[i];
						        total += parseFloat(branch.total_sum_format);
						    }
						    return total;
						}
						$scope.model.datainit.slbDebtSaleProductSum = $scope.getTotal();
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    		$scope.model.datainit.slbDebtSaleProduct = [];
			    	}
			    	$("#listReLoading").hide();
			    },
               	function (response) {
                  	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$scope.model.datainit.slbDebtSaleProduct = [];
					$("#listReLoading").hide();
			    }
			);
	}
	// GetSubadminSale
	$scope.GetPrivateSubadminSale = function() {

		$("#listReLoading").show();

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetPrivateSubadminSale")
	    	.then(
           		function (response) {
                  	var data = response.data;
			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    		$scope.model.datainit.slbSaleSubadmin = [];
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		// toastr.warning(data.warning, Languages.common_warning_title);
			    		// $scope.model.datainit.slbSaleSubadmin = [];
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		$scope.model.datainit.slbSaleSubadmin = data.success;
			    		$scope.getTotal = function(){
						    var total = 0;
						    for(var i = 0; i < $scope.model.datainit.slbSaleSubadmin.length; i++){
						        var branch = $scope.model.datainit.slbSaleSubadmin[i];
						        total += parseFloat(branch.total_sum_format);
						    }
						    return total;
						}
						$scope.model.datainit.slbSaleSubadminSum = $scope.getTotal();
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    		$scope.model.datainit.slbSaleSubadmin = [];
			    	}
			    	$("#listReLoading").hide();
			    },
               	function (response) {
                  	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$scope.model.datainit.slbSaleSubadmin = [];
					$("#listReLoading").hide();
			    }
			);
	}
	// GetSubadminDebtSale
	$scope.GetPrivateSubadminDebtSale = function() {

		$("#listReLoading").show();

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetPrivateSubadminDebtSale")
	    	.then(
           		function (response) {
                  	var data = response.data;
			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    		$scope.model.datainit.slbDebtSubadmin = [];
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		// toastr.warning(data.warning, Languages.common_warning_title);
			    		// $scope.model.datainit.slbDebtSubadmin = [];
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		$scope.model.datainit.slbDebtSubadmin = data.success;
			    		$scope.getTotal = function(){
						    var total = 0;
						    for(var i = 0; i < $scope.model.datainit.slbDebtSubadmin.length; i++){
						        var branch = $scope.model.datainit.slbDebtSubadmin[i];
						        total += parseFloat(branch.total_sum_format);
						    }
						    return total;
						}
						$scope.model.datainit.slbDebtSubadminSum = $scope.getTotal();
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    		$scope.model.datainit.slbDebtSubadmin = [];
			    	}
			    	$("#listReLoading").hide();
			    },
               	function (response) {
                  	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$scope.model.datainit.slbDebtSubadmin = [];
					$("#listReLoading").hide();
			    }
			);
	}
});