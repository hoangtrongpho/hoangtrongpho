/*!
 *
 *	Develop by Vinh Nguyễn - Ho Chi Minh City - 2017
 * 	Copyright Â© 2016-2017 Skyfire Team. All rights reserved.
 *	Design by Skyfire Team 2.3.3
 *
 *	Phone: 0975 578 276
 *	Skype: enjoyvinh
 *
 */

"use strict";

// app.js
// create angular app
var leftMenuApp = angular.module('leftMenuApp', ['commonApp']);

leftMenuApp.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});

leftMenuApp.factory('LeftMenuModels', function() {

	var model={
		datainit : {
			listFunctions : []
			// 	{
			// 		name	: "Dashboard",
			// 		title 	: "Dashboard",
			// 		url 	: "/System/Dashboard",
			// 		icon 	: "fa-dashboard",
			// 		tag		: "Dashboard - Tổng Quan"
			// 	},
			// 	{
			// 		name 	: "ListNewsCategories",
			// 		title 	: "Danh Mục Thể Loại",
			// 		url 	: "/System/ListNewsCategories",
			// 		icon 	: "fa-cubes",
			// 		tag		: "Danh Mục Thể Loại Tin Tức - List News Categories"
			// 	},
			// 	{
			// 		name 	: "ListNews",
			// 		title 	: "Danh Mục Tin Tức",
			// 		url 	: "/System/ListNews",
			// 		icon 	: "fa-cubes",
			// 		tag		: "Quản Trị Danh Mục Tin Tức - List Categories"
			// 	},
			// 	{
			// 		name 	: "GalleryLirary",
			// 		title 	: "Thư Viện Ảnh",
			// 		url 	: "/System/GalleryLirary",
			// 		icon 	: "fa-picture-o",
			// 		tag		: "Quản Trị Thư Viện Ảnh - Images Gallery Lirary"
			// 	},
			// ],
		},
	};

	model.request = {
		txtKeyword : "",
	};

	return model;
});

// leftMenuApp.service('leftMenuFunctions', function() {
//     return {
// 		slugify: function(str){
// 		    var title, slug;
		 
// 		    //Lấy text từ thẻ input title 
// 		    title = str;
		 
// 		    //Đổi chữ hoa thành chữ thường
// 		    slug = title.toLowerCase();
		 
// 		    //Đổi ký tự có dấu thành không dấu
// 		    slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
// 		    slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
// 		    slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
// 		    slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
// 		    slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
// 		    slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
// 		    slug = slug.replace(/đ/gi, 'd');
// 		    //Xóa các ký tự đặt biệt
// 		    slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
// 		    //Đổi khoảng trắng thành ký tự gạch ngang
// 		    slug = slug.replace(/ /gi, " ");
// 		    //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
// 		    //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
// 		    slug = slug.replace(/\-\-\-\-\-/gi, ' ');
// 		    slug = slug.replace(/\-\-\-\-/gi, ' ');
// 		    slug = slug.replace(/\-\-\-/gi, ' ');
// 		    slug = slug.replace(/\-\-/gi, ' ');
// 		    //Xóa các ký tự gạch ngang ở đầu và cuối
// 		    slug = '@' + slug + '@';
// 		    slug = slug.replace(/\@\-|\-\@|\@/gi, '');
// 		    //In slug ra textbox có id “slug”
// 		    return slug;
// 		}
//     };
// });

leftMenuApp.filter('searchFunctions', function(){

	return function(arr, keyWord){

		if(!keyWord){
			$("#resultFunctionSearch").hide();
			$("#menuFunction").show();
			$("#search-btn").show();
			$("#clearKeyWord").hide();
			return [];
		}

		arr = [];

		// keyWord = leftMenuFunctions.slugify(keyWord.toLowerCase());
		keyWord = keyWord.toLowerCase();

		angular.forEach(angular.element(".itemMenu"), function(value, key){
			var item = angular.element(value);
			var menu = item.children().find("span").text();

			if(menu.toLowerCase().indexOf(keyWord) !== -1){
				var tag = item.children().find("span").text();
				var icon = item.children().find("i").attr('class');
				var href = item.children().attr('href');
				var item = {
					name 	: tag,
					title : tag,
					url 	: href,
					icon 	: icon,
					tag	: tag
				}
				arr.push(item);
			}
		});

		$("#menuFunction").hide();
		$("#resultFunctionSearch").show();
		$("#search-btn").hide();
		$("#clearKeyWord").show();

		return arr;
	};

});

// create angular controller
leftMenuApp.controller('leftMenuController', function(LeftMenuModels, commonFunction, $scope) {

	$scope.model = LeftMenuModels;

    $scope.clearKeyWord = function() {
    	$scope.model.request.txtKeyword = "";
    }
	

});

