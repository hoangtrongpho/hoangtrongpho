/*!
 *
 *	Develop by Vinh Nguyễn - Ho Chi Minh City - 2017
 * 	Copyright Â© 2016-2017 Skyfire Team. All rights reserved.
 *	Design by Skyfire Team 2.3.3
 *
 *	Phone: 0975 578 276
 *	Skype: enjoyvinh
 *
 */

"use strict";

// app.js
// create angular app
var dashboardApp = angular.module('dashboardApp', ['commonApp','datatables','datatables.colreorder','datatables.bootstrap','zingchart-angularjs']);

dashboardApp.factory('dashboardModels', function() {

	var model={
		datainit : {
			currentYear:new Date().getFullYear(),
			dtgChartColor:["red orange","orange yellow","yellow green","green blue","blue darkblue","blue teal","teal aqua","olive yellow","purple fuchsia","maroon red","brown pink","pink purple","lavender lemon","lime purple"],
			dtgListBranch:[],
		},
		preLoading : true,
		accessPreLoading : true,
		appURL : "http://localhost:8000",
		
	};
	model.chart1={
		ChartData:{
				legend:{
	    	 	"background-color":"#ffe6e6",
			    "border-width":2,
			    "border-color":"red",
			    "border-radius":"5px",
			    "layout":"1x2",
			    "x":"40%",
			    "y":"0%",
			    "highlight-plot": true
	  			},
			  type : 'bar' ,
			  "scale-x": {
			    "labels": ["Alpha","Beta","Gamma","Delta","Epsilon","Zeta","Eta","Theta","Theta","Theta","Theta","Theta","Theta"]
			  }, 
			  series : [  
			    { values : [ 54 , 23 , 34 , 23 , 43, 54 , 23 , 34 , 23 , 43,12,34,100  ],"text":"Chi Nhánh 1" },  
			    { values : [ 10 , 15 , 16 , 20 , 40, 16 , 20 , 40,16 , 20 , 40,33,55 ],"text":"Chi Nhánh 2" }  
			  ]
			},
			myRender:{
				width: "100%"
			}
	}
	model.chart2={
		ChartData:{
				layout:"1x2",
				graphset:[
				    {
				      "type":"bar",
				      "plot":{
				        "background-color":"pink"
				      },
				      "series":[
				        {"values":[31,16,35,5,59,33]}
				      ]
				    },
				    {
				      "type":"pie",
				      "plot":{
				        "background-color":"pink",
				        "value-box":{
				          "placement":"out",
				          "font-color":"gray",
				          "font-size":12,
				          "font-weight":"normal"
				        }
				      },
				      "series": [
				        {
				          "values":[30],
				          "background-color":"pink"
				        },
				        {
				          "values":[34],
				          "background-color":"orange"
				        },
				        {
				          "values":[15],
				          "background-color":"green"
				        },
				        {
				          "values":[14],
				          "background-color":"gray"
				        },
				        {
				          "values":[5],
				          "background-color":"purple"
				        }
				      ]
				    }
				  ]
			}
	}
	model.chart3={
		ChartData:{
				"type":"vbar3d",
			    "fill-type":"radial",
			    "3d-aspect":{
			      // "true3d":false,
			      "zoom":0.85,
			    },
				"plot":{
					"background-color": "darkblue blue",
			        "aspect":"cylinder",
			        // "bars-overlap":"100%",
			        "mode":"normal",
			        "value-box": {
      					"thousands-separator":",",
		                "format":"$%v",
		                "text":"%v",
      					"placement":"top",	
      					"font-color":"red",
      					"offset-y":-10,
      					"font-size":11,
    				},
			        "tooltip": {
			        	// kl: tên tháng
			        	// v: giá trị
			        	// t: legend
			        	"visible":true,
		                "thousands-separator":",",
		                "format":"$%v",
		                "text":"%v Đ",
				      	"sticky": true, //Set to true.
      					"timeout": 1000 //Provide value in milliseconds.
				    }
			    },	
			    "source": {
				  	"text": "Source: Developed by Vtech LDC",
				  	"background-color":"#C30",
				  	"color":"#FFF",
				  	"width":150,
				  	"border-radius":4,
				  	"width":170,
  					"height":20,
  					"x":"10%",
			    	"y":"90%",
				},	
				"title":{
				  	"text":"Thống Kê Doanh Số Toàn Hệ Thống",
				  	// "width":300,
				  	// "height":20,
				  	// "x":"35%",
			    // 	"y":"0%",
				  	"background-color":"#333",
				  	"color":"#FFF",
				  	"border-radius":"4px",
				  	"font-weight":"bold",
				  	"font-size":15,
				},
			    "legend":{
	    	 	// 	"background-color":"#ffe6e6",
			    	// "border-width":2,
			    	// "border-color":"red",
			    	// "border-radius":"5px",
			    	"layout":"2",
			    	"x":"6%",
			    	"y":"0%",
			    	"highlight-plot": true
	  			},
			  	
			  	"scale-x": {
			    	"labels": ["Tháng 1","Tháng 2","Tháng 3","Tháng 4","Tháng 5","Tháng 6","Tháng 7","Tháng 8","Tháng 9","Tháng 10","Tháng 11","Tháng 12"],
			    	"label":{
			    		"text":"Thời Gian",
      					"font-size":20,
			    	}
			  	}, 
			  	"scale-y": {
			    	"short":true, //To display scale values as short units.
			    	"short-unit":"M", //To set the short unit type.
			    	"thousands-separator":",",
			    	"label":{
			    		"text":"Doanh Số",
      					"font-size":20,
			    	}
			  	},
			  	"series" : [  
			    	// { 	
			    	// "values" : [0,0,0,0,0,0,0,0,0,0,0,0],
			    	// "text":"Chi Nhánh 1" 
			    	// },  
			    	// { 	
			    	// "values" : [0,0,0,3,4,0,5,0,0,0,0,0],
			    	// "text":"Chi Nhánh 2" 
			    	// },  
			  	],
			 //  	"images":[
				//     {
				//         "src":"http://www.large-icons.com/stock-icons/large-home/buildings-icon.gif",
				//         "x":"0%",
				//         "y":"0"
				//     }
				// ],

			},
			// end chart data
			"myRender":{
				"width": "100%"
			}
	}
	model.request = {
	};

	return model;
});

// create angular controller
dashboardApp.controller('dashboardController', function(dashboardModels, commonFunction, $scope, $rootScope, $http, $filter, $window, $location, $timeout, toastr,DTOptionsBuilder, DTColumnDefBuilder,DTColumnBuilder) {
	
	$scope.model = dashboardModels;

	$scope.pageInit = function() {

		$scope.model.datainit.appURL = $('meta[name="app_url"]').attr('content');

		$scope.GetBranchList();
			

		$scope.dtOptions = DTOptionsBuilder.newOptions()
			.withOption('order', [1, 'desc'])
        	.withPaginationType('full_numbers')
        	.withDisplayLength(10)
	        .withColReorder()
	        // .withColReorderOrder([0, 1, 2, 3, 4])
	        .withBootstrap()
	        .withBootstrapOptions({
	            pagination: {
	                classes: {
	                    ul: 'pagination pagination-md'
	                }
	            }
	        })
	        .withLanguage({
	            "sEmptyTable":     "Không có dữ liệu.",
	            "sInfo":           "Hiển Thị Từ _START_ Đến _END_ Của _TOTAL_ Kết Quả",
	            "sInfoEmpty":      "Hiển Thị Từ 0 Đến 0 Của 0 Kết Quả",
	            "sInfoFiltered":   "(Đã Lọc Từ Tổng _MAX_ Kết Quả)",
	            "sInfoPostFix":    "",
	            "sInfoThousands":  ",",
	            "sLengthMenu":     "Hiển Thị&nbsp;&nbsp;&nbsp;_MENU_&nbsp;&nbsp;&nbsp;Kết Quả / 1 Trang",
	            "sLoadingRecords": "Đang Tải Dữ Liệu...",
	            "sProcessing":     "Đang Xử Lý Dữ Liệu...",
	            "sSearch":         "Từ Khóa: ",
	            "sZeroRecords":    "Không tìm thấy dữ liệu trùng khớp.",
	            "oPaginate": {
	                "sFirst":    "Đầu",
	                "sLast":     "Cuối",
	                "sNext":     "Tiếp",
	                "sPrevious": "Trước"
	            },
	            "oAria": {
	                "sSortAscending":  ": activate to sort column ascending",
	                "sSortDescending": ": activate to sort column descending"
	            }
	        })
	        .withOption('responsive', true);

	    $scope.dtColumnDefs = [
	        DTColumnDefBuilder.newColumnDef(0).notSortable(),
	    ];

	};
	// GetBranchList and init the chart
	$scope.GetBranchList = function() {

		$("#listReLoading").show();

		var request = {
	    };

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetBranchList", JSON.stringify(request))
	    .then(
               function (response) {
                  var data = response.data;

		    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
	            {
	                toastr.error(data.messages, Languages.common_error_title);

	                if(data.auth != "" && data.auth != null  && data.auth != undefined)
	                {
	                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
	                }
	            }
		    	else if(data.error != "" && data.error != null  && data.error != undefined)
		    	{
		    		toastr.error(data.error, Languages.common_error_title);
		    		$scope.model.datainit.dtgListBranch = [];
		    	}
		    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
		    	{
		    		toastr.warning(data.warning, Languages.common_warning_title);
		    		$scope.model.datainit.dtgListBranch = [];
		    	}
		    	else if(data.success != "" && data.success != null  && data.success != undefined)
		    	{
		    		// clear data and init from the begin
		    		$scope.model.chart3.ChartData.series=[];

		    		$scope.model.datainit.dtgListBranch = data.success;
		    		if($scope.model.datainit.dtgListBranch.length==0)
		    		{
		    			alert("Chưa Có Chy Nhánh !");
		    			return false;
		    		}
		    		// cai dat legend layout
		    		$scope.model.chart3.ChartData.legend.layout=data.success.length;
		    		$scope.model.chart3.ChartData.title.text="Doanh Thu Theo Chi Nhánh Năm "+ $scope.model.datainit.currentYear;
		    		// init chart 
		    		angular.forEach(data.success, function(value, key) {
		    			var bra_id=value.branch_id;
		    			var bra_name=value.branch_name;
		    			var bra_color=$scope.model.datainit.dtgChartColor[key];
		    			$scope.GetTotalSaleByDateTimeBranch(bra_id,bra_name,bra_color);
					   
					});
		    	}
		    	else
		    	{
		    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
		    		{
		    			toastr.error(data.systemerror, Languages.common_error_title);
		    			window.location.replace("./"+data.errorCode);
		    		}
		    		else
		    		{
		    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
		    		}
		    		$scope.model.datainit.dtgListBranch = [];
		    	}
		    	$("#listReLoading").hide();
	    	},
           	function (response) {
          		var data = response.data;
				toastr.error(Languages.common_error_exception, Languages.common_error_title);
				$scope.model.datainit.dtgListBranch = [];
				$("#listReLoading").hide();
		    });
	};

	// GetTotalSaleByDateTimeBranch: tao chart của 1 chy nhánh
	$scope.GetTotalSaleByDateTimeBranch = function(bra_id,bra_name,bra_color) {

		$("#listReLoading").show();

		var request = {
			branch_id:bra_id,
	    };

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetTotalSaleByDateTimeBranch", JSON.stringify(request))
	    .then(
               function (response) {
                  var data = response.data;

		    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
	            {
	                toastr.error(data.messages, Languages.common_error_title);

	                if(data.auth != "" && data.auth != null  && data.auth != undefined)
	                {
	                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
	                }
	            }
		    	else if(data.error != "" && data.error != null  && data.error != undefined)
		    	{
		    		toastr.error(data.error, Languages.common_error_title);
		    		$scope.model.datainit.dtgBranchChart = [];
		    	}
		    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
		    	{
		    		toastr.warning(data.warning, Languages.common_warning_title);
		    		$scope.model.datainit.dtgBranchChart = [];
		    	}
		    	else if(data.success != "" && data.success != null  && data.success != undefined)
		    	{
		    		// tạo 12 tháng dữ liệu doanh số cho chi nhánh
		    		var dataListChart=[];
		    		for(var i=1;i<=12;i++){
		    			var flag="yes";
		    			angular.forEach(data.success, function(value, key) {
						    if(value.month == i && value.year == $scope.model.datainit.currentYear)
						    {
						    	dataListChart.push(parseFloat(value.total_sum));
						    	flag="no";
						    }
						});		
						if(flag=="yes") {
							dataListChart.push(0);
						}    		
		    		}
		    		// thực thể chi nhánh
		    		var obj = {
				  		values: dataListChart, 
				  		text: bra_name,
				  		"background-color":bra_color,
				  	};
				  	// tạo chart theo chi nhánh obj
				  	$scope.model.chart3.ChartData.series.push(obj);
				  	
		    	}
		    	else
		    	{
		    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
		    		{
		    			toastr.error(data.systemerror, Languages.common_error_title);
		    			window.location.replace("./"+data.errorCode);
		    		}
		    		else
		    		{
		    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
		    		}
		    		$scope.model.datainit.dtgBranchChart = [];
		    	}
		    	$("#listReLoading").hide();
	    	},
           	function (response) {
          		var data = response.data;
				toastr.error(Languages.common_error_exception, Languages.common_error_title);
				$scope.model.datainit.dtgBranchChart = [];
				$("#listReLoading").hide();
		    });
	};
	// ng-click="nextYear()"
	$scope.nextYear = function(){
		$scope.model.datainit.currentYear=$scope.model.datainit.currentYear+1;
		$scope.GetBranchList();
	}
	// ng-click="backYear()"
	$scope.backYear = function(){
		$scope.model.datainit.currentYear=$scope.model.datainit.currentYear-1;
		$scope.GetBranchList();
	}






	// apiGetTotalSaleByDateTime
	$scope.GetTotalSaleByDateTime = function() {
		$("#listReLoading").show();
		var request = {
	    };
	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetTotalSaleByDateTime", JSON.stringify(request))
	    .then(
               function (response) {
                  var data = response.data;

		    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
	            {
	                toastr.error(data.messages, Languages.common_error_title);

	                if(data.auth != "" && data.auth != null  && data.auth != undefined)
	                {
	                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
	                }
	            }
		    	else if(data.error != "" && data.error != null  && data.error != undefined)
		    	{
		    		toastr.error(data.error, Languages.common_error_title);
		    	}
		    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
		    	{
		    		toastr.warning(data.warning, Languages.common_warning_title);
		    	}
		    	else if(data.success != "" && data.success != null  && data.success != undefined)
		    	{
		    		$scope.model.datainit.dtgList = data.success;
		    	}
		    	else
		    	{
		    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
		    		{
		    			toastr.error(data.systemerror, Languages.common_error_title);
		    			window.location.replace("./"+data.errorCode);
		    		}
		    		else
		    		{
		    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
		    		}
		    	}
		    	$("#listReLoading").hide();
	    	},
           	function (response) {
          		var data = response.data;
				toastr.error(Languages.common_error_exception, Languages.common_error_title);
				$("#listReLoading").hide();
		    });
	}
});