/*!
 *
 *	Develop by Vinh Nguyễn - Ho Chi Minh City - 2017
 * 	Copyright Â© 2016-2017 Skyfire Team. All rights reserved.
 *	Design by Skyfire Team 2.3.3
 *
 *	Phone: 0975 578 276
 *	Skype: enjoyvinh
 *
 */

"use strict";

// app.js
// create angular app
var newsTagsApp = angular.module('newsTagsApp', ['commonApp', 'datatables','datatables.colreorder','datatables.bootstrap','datatables.tabletools']);

newsTagsApp.factory('NewsTagsModels', function() {

	var model={
		datainit : {
			dtgListTags : [],
			appURL : "http://localhost:8000"
		},
	};

	model.request = {
		txtTagId : "",
		txtTagName : "",
	};

	return model;
});

// create angular controller
newsTagsApp.controller('newsTagsController', function(NewsTagsModels, commonFunction, $scope, $rootScope, $http, $filter, $window, $location, toastr,
	DTOptionsBuilder, DTColumnDefBuilder,DTColumnBuilder) {

	$scope.model = NewsTagsModels;

	$scope.pageInit = function() {

		$scope.model.datainit.appURL = $('meta[name="app_url"]').attr('content');
		
		$scope.loadTagsList();

	};

	$scope.loadTagsList = function() {

		$("#listReLoading").show();

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetListNewsTags")
	    	.then(
               	function (response) {
                  	var data = response.data;

			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    		$scope.model.datainit.dtgListNews = [];
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		toastr.warning(data.warning, Languages.common_warning_title);
			    		$scope.model.datainit.dtgListNews = [];
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		$scope.model.datainit.dtgListTags = data.success;
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    		$scope.model.datainit.dtgListNews = [];
			    	}
			    	$("#listReLoading").hide();
			    },
               	function (response) {
                  	debugger;
                  	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$scope.model.datainit.dtgListNews = [];
					$("#listReLoading").hide();
			    }
			);

	    $scope.dtOptions = DTOptionsBuilder.newOptions()
			.withOption('order', [])
        	.withPaginationType('full_numbers')
        	.withDisplayLength(10)
	        .withColReorder()
	        .withBootstrap()
	        .withBootstrapOptions({
	            pagination: {
	                classes: {
	                    ul: 'pagination pagination-md'
	                }
	            }
	        })
	        .withLanguage({
	            "sEmptyTable":     "Không có dữ liệu.",
	            "sInfo":           "Hiển Thị Từ _START_ Đến _END_ Của _TOTAL_ Kết Quả",
	            "sInfoEmpty":      "Hiển Thị Từ 0 Đến 0 Của 0 Kết Quả",
	            "sInfoFiltered":   "(Đã Lọc Từ Tổng _MAX_ Kết Quả)",
	            "sInfoPostFix":    "",
	            "sInfoThousands":  ",",
	            "sLengthMenu":     "Hiển Thị&nbsp;&nbsp;&nbsp;_MENU_&nbsp;&nbsp;&nbsp;Kết Quả / 1 Trang",
	            "sLoadingRecords": "Đang Tải Dữ Liệu...",
	            "sProcessing":     "Đang Xử Lý Dữ Liệu...",
	            "sSearch":         "Từ Khóa: ",
	            "sZeroRecords":    "Không tìm thấy dữ liệu trùng khớp.",
	            "oPaginate": {
	                "sFirst":    "Đầu",
	                "sLast":     "Cuối",
	                "sNext":     "Tiếp",
	                "sPrevious": "Trước"
	            },
	            "oAria": {
	                "sSortAscending":  ": activate to sort column ascending",
	                "sSortDescending": ": activate to sort column descending"
	            }
	        })
	        .withOption('responsive', true);

        $scope.dtColumnDefs = [
	        DTColumnDefBuilder.newColumnDef(0).notSortable(),
	    ];
	}

	$scope.removeItem = function(tag_id) {
		
		if (!tag_id) 
		{
			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			return false;
		}

		var confirmModal = bootbox.dialog({
		  	message: commonFunction.languageReplace(Languages.common_confirm_delete_question,[' Thẻ']),
		  	title: commonFunction.languageReplace(Languages.common_confirm_delete_title,[' Thẻ']),
		  	closeButton: true,
		  	animate: true,
		  	className: "confirmModal",
		  	backdrop: true,
		  	headerBackground: "bg-red",
		  	size: "small",
		  	locale: "vi",
		  	show: true,
		  	buttons: {
		  		cancel : {
			      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
			      	className: "btn-default",
			      	callback: function() {
			        	confirmModal.modal('hide');
			      	}
			    },
			    success: {
		      		label: '<i class="fa fa-trash" aria-hidden="true"></i>&nbsp;'+Languages.common_button_remove,
			      	className: "btn-danger",
			      	callback: function() {
			      		$("#listReLoading").show();
			        	var request = {
					        tag_id : tag_id,
					    };
					    $http.post($scope.model.datainit.appURL + "/SystemApi/RemoveNewsTags", JSON.stringify(request))
					    	.then(
               					function (response) {
                  					var data = response.data;

							    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
						            {
						                toastr.error(data.messages, Languages.common_error_title);

						                if(data.auth != "" && data.auth != null  && data.auth != undefined)
						                {
						                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
						                }
						            }
							    	else if(data.error != "" && data.error != null  && data.error != undefined)
							    	{
							    		toastr.error(data.error, Languages.common_error_title);
							    	}
							    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
							    	{
							    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
							    		warning = warning.replace('["', '').replace('"]', '');
							    		toastr.warning(warning, Languages.common_warning_title);
							    	}
							    	else if(data.success != "" && data.success != null  && data.success != undefined)
							    	{
							    		toastr.success(data.success, Languages.common_success_title);
							    		$scope.formReset();
							    	}
							    	else
							    	{
							    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
							    		{
							    			toastr.error(data.systemerror, Languages.common_error_title);
							    			window.location.replace("./"+data.errorCode);
							    		}
							    		else
							    		{
							    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
							    		}
							    	}
							    	$("#listReLoading").hide();
								},
								function (response) {
	                  				debugger;
	                  				var data = response.data;
									toastr.error(Languages.common_error_exception, Languages.common_error_title);
									$("#listReLoading").hide();
							    }
							);
			      	}
			    },
		  	}
		});
	};

	$scope.addItem = function() {

		if (!$scope.model.request.txtTagName) 
		{
			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			return false;
		}

		if ($scope.frmAdd.txtTagName.$valid) {
			var confirmModal = bootbox.dialog({
			  	message: commonFunction.languageReplace(Languages.common_confirm_add_question,[' Thẻ']),
			  	title: commonFunction.languageReplace(Languages.common_confirm_add_title,[' Thẻ']),
			  	closeButton: true,
			  	animate: true,
			  	className: "confirmModal",
			  	backdrop: true,
			  	headerBackground: "bg-blue",
			  	size: "small",
			  	locale: "vi",
			  	show: true,
			  	buttons: {
			  		cancel : {
				      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
				      	className: "btn-default",
				      	callback: function() {
				        	confirmModal.modal('hide');
				      	}
				    },
				    success: {
			      		label: '<i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;'+Languages.common_button_add,
				      	className: "btn-primary",
				      	callback: function() {

				      		$("#listReLoading").show();

				        	var request = {
						        tag_name : $scope.model.request.txtTagName,
						    };

						    $http.post($scope.model.datainit.appURL + "/SystemApi/AddNewsTags", JSON.stringify(request))
					    		.then(
               						function (response) {
                  						var data = response.data;

								    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
							            {
							                toastr.error(data.messages, Languages.common_error_title);

							                if(data.auth != "" && data.auth != null  && data.auth != undefined)
							                {
							                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
							                }
							            }
								    	else if(data.error != "" && data.error != null  && data.error != undefined)
								    	{
								    		toastr.error(data.error, Languages.common_error_title);
								    	}
								    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
								    	{
								    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
								    		warning = warning.replace('["', '').replace('"]', '');
								    		toastr.warning(warning, Languages.common_warning_title);
								    	}
								    	else if(data.success != "" && data.success != null  && data.success != undefined)
								    	{
								    		toastr.success(data.success, Languages.common_success_title);
								    		$('#modalAdd').modal('toggle');
								    		$scope.formReset();
								    	}
								    	else
								    	{
								    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
								    		{
								    			toastr.error(data.systemerror, Languages.common_error_title);
								    			window.location.replace("./"+data.errorCode);
								    		}
								    		else
								    		{
								    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
								    		}
								    	}
								    	$("#listReLoading").hide();
					    			},
									function (response) {
		                  				debugger;
		                  				var data = response.data;
										toastr.error(Languages.common_error_exception, Languages.common_error_title);
										$("#listReLoading").hide();
								    }
								);
				      	}
				    }
			  	}
			});
		}
	};

	$scope.updateItem = function() {

		if (!$scope.model.request.txtTagId || !$scope.model.request.txtTagName) 
		{
			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			return false;
		}

		if ($scope.frmAdd.txtTagName.$valid) {
			var confirmModal = bootbox.dialog({
			  	message: commonFunction.languageReplace(Languages.common_confirm_update_question,[' Thẻ']),
			  	title: commonFunction.languageReplace(Languages.common_confirm_update_title,[' Thẻ']),
			  	closeButton: true,
			  	animate: true,
			  	className: "confirmModal",
			  	backdrop: true,
			  	headerBackground: "bg-yellow",
			  	size: "small",
			  	locale: "vi",
			  	show: true,
			  	buttons: {
			  		cancel : {
				      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
				      	className: "btn-default",
				      	callback: function() {
				        	confirmModal.modal('hide');
				      	}
				    },
				    success: {
			      		label: '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;'+Languages.common_button_update,
				      	className: "btn-warning",
				      	callback: function() {

				      		$("#listReLoading").show();

				        	var request = {
				        		tag_id : $scope.model.request.txtTagId,
						        tag_name : $scope.model.request.txtTagName,
						    };

						    $http.post($scope.model.datainit.appURL + "/SystemApi/UpdateNewsTags", JSON.stringify(request))
						    	.then(
               						function (response) {
                  						var data = response.data;

								    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
							            {
							                toastr.error(data.messages, Languages.common_error_title);

							                if(data.auth != "" && data.auth != null  && data.auth != undefined)
							                {
							                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
							                }
							            }
								    	else if(data.error != "" && data.error != null  && data.error != undefined)
								    	{
								    		toastr.error(data.error, Languages.common_error_title);
								    	}
								    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
								    	{
								    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
								    		warning = warning.replace('["', '').replace('"]', '');
								    		toastr.warning(warning, Languages.common_warning_title);
								    	}
								    	else if(data.success != "" && data.success != null  && data.success != undefined)
								    	{
								    		toastr.success(data.success, Languages.common_success_title);
								    		$('#modalUpdate').modal('toggle');
								    		$scope.formReset();
								    	}
								    	else
								    	{
								    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
								    		{
								    			toastr.error(data.systemerror, Languages.common_error_title);
								    			window.location.replace("./"+data.errorCode);
								    		}
								    		else
								    		{
								    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
								    		}
								    	}
								    	$("#listReLoading").hide();
								    },
									function (response) {
		                  				debugger;
		                  				var data = response.data;
										toastr.error(Languages.common_error_exception, Languages.common_error_title);
										$("#listReLoading").hide();
								    }
								);
				      	}
				    }
			  	}
			});
		}
	};

	$scope.openUpdateItem = function(tags) {
		$('#modalUpdate').modal('toggle');
		$scope.model.request = {
			txtTagId : tags.tag_id,
			txtTagName : tags.tag_name,
		};
		$scope.frmUpdate.$setPristine();
		$scope.frmUpdate.$setUntouched();
	};

	$('#modalUpdate').on('hidden.bs.modal', function (e) {
		$scope.model.request = {
			txtTagId : "",
			txtTagName : "",
		};
		$scope.frmUpdate.$setPristine();
		$scope.frmUpdate.$setUntouched();
	});

	$scope.formReset = function() {

		$scope.pageInit();

		$scope.model.request = {
			txtTagId : "",
			txtTagName : "",
		};

		$scope.frmAdd.$setPristine();
		$scope.frmAdd.$setUntouched();
		$scope.frmUpdate.$setPristine();
		$scope.frmUpdate.$setUntouched();
	};
});
