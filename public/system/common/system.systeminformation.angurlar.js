/*!
 *
 *	Develop by Vinh Nguyễn - Ho Chi Minh City - 2017
 * 	Copyright Â© 2016-2017 Skyfire Team. All rights reserved.
 *	Design by Skyfire Team 2.3.3
 *
 *	Phone: 0975 578 276
 *	Skype: enjoyvinh
 *
 */

"use strict";

// app.JSON
// create angular app
var systemInformationApp = angular.module('systemInformationApp', ['commonApp', 'daterangepicker', 'isteven-multi-select', 'ngSanitize',
		'datatables','datatables.colreorder','datatables.bootstrap']);

systemInformationApp.factory('NewsModels', function() {

	var model={
		datainit : {
			dtgListInfo: [],
			dtgListSlide: [],
			listSlide:"",
			txtImages : "",
			appURL : "http://localhost:8000"
		},
	};

	model.request = {
		txtImages : "",
		txtSlideUrl:"",

	};

	model.localLang = {
	    selectAll       : Languages.common_button_select_all,
	    selectNone      : Languages.common_button_deselected_all,
	    reset           : Languages.common_button_cancel,
	    search          : Languages.common_input_label_search,
	    nothingSelected : Languages.common_input_label_select        //default-label is deprecated and replaced with this.
	}

	return model;
});

// create angular controller
systemInformationApp.controller('systemInformationController', function(NewsModels, commonFunction, $scope, $rootScope, $http, $filter, $window, $location, toastr,
	DTOptionsBuilder, DTColumnDefBuilder,DTColumnBuilder) {

	$scope.model = NewsModels;

	$scope.pageInit = function() {
		 
		$scope.model.datainit.appURL = $('meta[name="app_url"]').attr('content');

		//$("#listReLoading").show();

		$scope.loadInfoList();
		$scope.loadSlide();
		

		$("#listReLoading").hide();
		$scope.dtOptions = DTOptionsBuilder.newOptions()
			.withOption('order', [])
        	.withPaginationType('full_numbers')
        	.withDisplayLength(10)
	        .withColReorder()
	        // .withColReorderOrder([0, 1, 2, 3, 4])
	        .withBootstrap()
	        .withBootstrapOptions({
	            pagination: {
	                classes: {
	                    ul: 'pagination pagination-md'
	                }
	            }
	        })
	        .withLanguage({
	            "sEmptyTable":     "Không có dữ liệu.",
	            "sInfo":           "Hiển Thị Từ _START_ Đến _END_ Của _TOTAL_ Kết Quả",
	            "sInfoEmpty":      "Hiển Thị Từ 0 Đến 0 Của 0 Kết Quả",
	            "sInfoFiltered":   "(Đã Lọc Từ Tổng _MAX_ Kết Quả)",
	            "sInfoPostFix":    "",
	            "sInfoThousands":  ",",
	            "sLengthMenu":     "Hiển Thị&nbsp;&nbsp;&nbsp;_MENU_&nbsp;&nbsp;&nbsp;Kết Quả / 1 Trang",
	            "sLoadingRecords": "Đang Tải Dữ Liệu...",
	            "sProcessing":     "Đang Xử Lý Dữ Liệu...",
	            "sSearch":         "Từ Khóa: ",
	            "sZeroRecords":    "Không tìm thấy dữ liệu trùng khớp.",
	            "oPaginate": {
	                "sFirst":    "Đầu",
	                "sLast":     "Cuối",
	                "sNext":     "Tiếp",
	                "sPrevious": "Trước"
	            },
	            "oAria": {
	                "sSortAscending":  ": activate to sort column ascending",
	                "sSortDescending": ": activate to sort column descending"
	            }
	        })
	        .withOption('responsive', true);

	    $scope.dtColumnDefs = [
	        DTColumnDefBuilder.newColumnDef(0).notSortable(),
	    ];
	};

	$scope.loadInfoList = function() {

		$("#listReLoading").show();
		 
		// filter value
		var request = {
	        // product_name : $scope.model.request.txtProductName,
	        // //neu ko có > truyền rỗng / else truyền chính nó
	        // // ! + var = có giá trị / ko có ! là rỗng
	        // list_cate_id : !$scope.model.request.slbCateParent ? [] : $scope.model.request.slbCateParent,
	        // list_origin_id : !$scope.model.request.slbOrigin ? [] : $scope.model.request.slbOrigin,
	    };

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetListInfo", JSON.stringify(request))
	    .then(
               function (response) {
                  var data = response.data;

		    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
	            {
	                toastr.error(data.messages, Languages.common_error_title);

	                if(data.auth != "" && data.auth != null  && data.auth != undefined)
	                {
	                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
	                }
	            }
		    	else if(data.error != "" && data.error != null  && data.error != undefined)
		    	{
		    		toastr.error(data.error, Languages.common_error_title);
		    		$scope.model.datainit.dtgListInfo = [];
		    	}
		    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
		    	{
		    		toastr.warning(data.warning, Languages.common_warning_title);
		    		$scope.model.datainit.dtgListInfo = [];
		    	}
		    	else if(data.success != "" && data.success != null  && data.success != undefined)
		    	{
		    		$scope.model.datainit.dtgListInfo = data.success;
		    	}
		    	else
		    	{
		    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
		    		{
		    			toastr.error(data.systemerror, Languages.common_error_title);
		    			window.location.replace("./"+data.errorCode);
		    		}
		    		else
		    		{
		    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
		    		}
		    		$scope.model.datainit.dtgListInfo = [];
		    	}
		    	$("#listReLoading").hide();
	    	},
           	function (response) {
          		 
          		var data = response.data;
				toastr.error(Languages.common_error_exception, Languages.common_error_title);
				$scope.model.datainit.dtgListInfo = [];
				$("#listReLoading").hide();
		    });
	};
	
	// =================add invoice====================
	$scope.saveSystemInfo = function() {
		
		var confirmModal = bootbox.dialog({
	  	message: commonFunction.languageReplace(Languages.common_confirm_update_question,[' Đơn Hàng']),
	  	title: commonFunction.languageReplace(Languages.common_confirm_update_title,[' Đơn Hàng']),
	  	closeButton: true,
	  	animate: true,
	  	className: "confirmModal",
	  	backdrop: true,
	  	headerBackground: "bg-blue",
	  	size: "small",
	  	locale: "vi",
	  	show: true,
	  	buttons: {
	  		cancel : {
		      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
		      	className: "btn-default",
		      	callback: function() {
		        	confirmModal.modal('hide');
		      	}
		    },
		    success: {
	      		label: '<i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;'+Languages.common_button_update,
		      	className: "btn-primary",
		      	callback: function() {
			      		 
			      		$("#addReLoading").show();
			      		// ======= ========

					    var request = {
					    	dtgListInfo : $scope.model.datainit.dtgListInfo 
					    };
					    $http.post($scope.model.datainit.appURL + "/SystemApi/UpdateSystemInformation",JSON.stringify(request))
			    		.then(
		               		function (response) {
			                  	var data = response.data;

							    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
						            {
						                toastr.error(data.messages, Languages.common_error_title);

						                if(data.auth != "" && data.auth != null  && data.auth != undefined)
						                {
						                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
						                }
						            }
							    	else if(data.error != "" && data.error != null  && data.error != undefined)
							    	{
							    		toastr.error(data.error, Languages.common_error_title);
							    	}
							    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
							    	{
							    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
							    		warning = warning.replace('["', '').replace('"]', '');
							    		toastr.warning(warning, Languages.common_warning_title);
							    	}
							    	else if(data.success != "" && data.success != null  && data.success != undefined)
							    	{
							    		toastr.success(data.success, commonFunction.languageReplace(Languages.common_success_title,[' Hệ Thống']));

							    		bootbox.alert(commonFunction.languageReplace(Languages.common_success_created,[' Hệ Thống']), function() {
											window.location.replace($scope.model.datainit.appURL + "/System/SystemInformation");
										});

										setTimeout(function(){ 
											window.location.replace($scope.model.datainit.appURL + "/System/SystemInformation");
										}, 3000);
							    	}
							    	else
							    	{
							    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
							    		{
							    			toastr.error(data.systemerror, Languages.common_error_title);
							    			window.location.replace("./"+data.errorCode);
							    		}
							    		else
							    		{
							    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
							    		}
							    	}
							    	$("#addReLoading").hide();
						    },
			               	function (response) {
			                  	 
			                 	var data = response.data;
								toastr.error(Languages.common_error_exception, Languages.common_error_title);
								$("#addReLoading").hide();
						    }
						);
		      		}
			    },
		  	}
		});
		
	};
	//============removeSlide=============
	$scope.removeSlide = function(deleteItem) {

    	if(deleteItem == "" || deleteItem == null || deleteItem == undefined)
		{
			toastr.error(Resources.common_error_exception, Resources.common_error_title);
			return false;
		}
    	
		var confirmModal = bootbox.dialog({
			message: commonFunction.languageReplace(Languages.common_confirm_delete_question,[' Slide']),
		  	title: commonFunction.languageReplace(Languages.common_confirm_delete_title,[' Slide']),
		  	closeButton: true,
		  	animate: true,
		  	className: "confirmModal",
		  	backdrop: true,
		  	headerBackground: "bg-red",
		  	size: "small",
		  	locale: "vi",
		  	show: true,
		  	buttons: {
		  		cancel : {
			      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
			      	className: "btn-default",
			      	callback: function() {
			        	confirmModal.modal('hide');
			      	}
			    },
			    success: {
		      		label: '<i class="fa fa-trash" aria-hidden="true"></i>&nbsp;'+Languages.common_button_remove,
			      	className: "btn-danger",
			      	callback: function() {

			      		$("#addReLoading").show();
			      	
			        	var request = {
					        slide_id : deleteItem.slide_id,
					    };
					    $http.post($scope.model.datainit.appURL + "/SystemApi/RemoveSlide", JSON.stringify(request))
						    	.then(
	               					function (response) {
	                  					var data = response.data;

								    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
							            {
							                toastr.error(data.messages, Languages.common_error_title);

							                if(data.auth != "" && data.auth != null  && data.auth != undefined)
							                {
							                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
							                }
							            }
								    	else if(data.error != "" && data.error != null  && data.error != undefined)
								    	{
								    		toastr.error(data.error, Languages.common_error_title);
								    	}
								    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
								    	{
								    		toastr.warning(data.warning, Languages.common_warning_title);
								    	}
								    	else if(data.success != "" && data.success != null  && data.success != undefined)
								    	{
								    		toastr.success(data.success, commonFunction.languageReplace(Languages.common_success_removed,['Slide']));
								    		$scope.loadSlide();
								    	}
								    	else
								    	{
								    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
								    		{
								    			toastr.error(data.systemerror, Languages.common_error_title);
								    			window.location.replace("./"+data.errorCode);
								    		}
								    		else
								    		{
								    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
								    		}
								    	}
								    	$("#addReLoading").hide();
						    		},
						    		function (response) {
					           
					                  	var data = response.data;	
										toastr.error(Languages.common_error_exception, Languages.common_error_title);
										$("#addReLoading").hide();
						    		}
					    	);
			      	}
			    }
		  	}
		});
    };
    //============addSlide=============
	$scope.loadSlide = function() {
		var request = {
	    };
		$http.post($scope.model.datainit.appURL + "/SystemApi/LoadSlide", JSON.stringify(request))
    	.then(
           	function (response) {
              	var data = response.data;

			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		toastr.warning(data.warning, Languages.common_warning_title);
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		$scope.model.datainit.dtgListSlide=data.success;
			    		
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    	}
			    	$("#addReLoading").hide();
		    },
           	function (response) {
                 	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$("#addReLoading").hide();
		    }
		);
    };
    //============addSlide=============
	$scope.addSlide = function() {
		var addItem= $("#thumbnail").val();
		if(addItem==null || addItem ==''){
			alert("Vui Lòng Chọn Hình");
			return false;
		}
		
		var request = {
	        slide_name: $("#thumbnail").val(),
			slide_url: $scope.model.request.txtSlideUrl,
			slide_img: $("#thumbnail").val(),
	    };
		// alert($("#thumbnail").val()+"//"+$scope.model.request.txtSlideUrl);
		// API control
		$http.post($scope.model.datainit.appURL + "/SystemApi/AddSlide", JSON.stringify(request))
    	.then(
           	function (response) {
              	var data = response.data;

			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
			    		warning = warning.replace('["', '').replace('"]', '');
			    		toastr.warning(warning, Languages.common_warning_title);
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		toastr.success(data.success, commonFunction.languageReplace(Languages.common_success_title,[' Slide']));
			    		$scope.loadSlide();
			    		$scope.model.request.txtSlideUrl="";
			    		$("#thumbnail").val();
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    	}
			    	$("#addReLoading").hide();
		    },
           	function (response) {
                 	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$("#addReLoading").hide();
		    }
		);
    };
    //=======================
});