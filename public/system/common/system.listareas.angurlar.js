/*!
 *
 *	Develop by Vinh Nguyễn - Ho Chi Minh City - 2017
 * 	Copyright Â© 2016-2017 Skyfire Team. All rights reserved.
 *	Design by Skyfire Team 2.3.3
 *
 *	Phone: 0975 578 276
 *	Skype: enjoyvinh
 *
 */

"use strict";

// app.js
// create angular app
var areaApp = angular.module('areaApp', ['commonApp', 'datatables','datatables.colreorder','datatables.bootstrap','datatables.tabletools','ui.tree','localytics.directives']);

areaApp.factory('AreasModels', function() {

	var model={
		datainit : {
			dtgList : [],
			dtgListUpdate : [{
				area_id : "",
				area_name : ""
			}],
			appURL : "http://localhost:8000",
		},
	};

	model.request = {
		slbProvinceParent : 0,
		slbDistrictParent : 0,
		slbWardParent : 0,
		txtProvinceId : "",
		txtProvinceName : "",
		slbDistrictParent : "",
		txtDistrictId : "",
		txtDistrictName : "",
		txtWardId : "",
		txtWardName : "",
	};

	return model;
});

// create angular controller
areaApp.controller('provincesController', function(AreasModels, commonFunction, $scope, $rootScope, $http, $filter, $window, $location, toastr,
	DTOptionsBuilder, DTColumnDefBuilder,DTColumnBuilder) {

	$scope.model = AreasModels;

	$scope.pageInit = function() {

		$scope.model.datainit.appURL = $('meta[name="app_url"]').attr('content');

		$scope.loadList();
	  
	};

	$scope.loadList = function() {

		$("#listReLoading").show();

    	$http.post($scope.model.datainit.appURL + "/SystemApi/GetListAreas")
    		.then(
            function (response) {
            	var data = response.data;

			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    		$scope.model.datainit.dtgList = [];
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		toastr.warning(data.warning, Languages.common_warning_title);
			    		$scope.model.datainit.dtgList = [];
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		$scope.model.datainit.dtgList = data.success;
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    		$scope.model.datainit.dtgList = [];
			    	}
			    	$("#listReLoading").hide();
		    	},
         	function (response) {
               debugger;
               var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$scope.model.datainit.dtgList = [];
					$("#listReLoading").hide();
		    	}
		   );

    	$scope.dtOptions = DTOptionsBuilder.newOptions()
			.withOption('order', [1, 'asc'])
        	.withPaginationType('full_numbers')
        	.withDisplayLength(10)
	        .withColReorder()
	        .withBootstrap()
	        .withBootstrapOptions({
	            pagination: {
	                classes: {
	                    ul: 'pagination pagination-md'
	                }
	            }
	        })
	        .withLanguage({
	            "sEmptyTable":     "Không có dữ liệu.",
	            "sInfo":           "Hiển Thị Từ _START_ Đến _END_ Của _TOTAL_ Kết Quả",
	            "sInfoEmpty":      "Hiển Thị Từ 0 Đến 0 Của 0 Kết Quả",
	            "sInfoFiltered":   "(Đã Lọc Từ Tổng _MAX_ Kết Quả)",
	            "sInfoPostFix":    "",
	            "sInfoThousands":  ",",
	            "sLengthMenu":     "Hiển Thị&nbsp;&nbsp;&nbsp;_MENU_&nbsp;&nbsp;&nbsp;Kết Quả / 1 Trang",
	            "sLoadingRecords": "Đang Tải Dữ Liệu...",
	            "sProcessing":     "Đang Xử Lý Dữ Liệu...",
	            "sSearch":         "Từ Khóa: ",
	            "sZeroRecords":    "Không tìm thấy dữ liệu trùng khớp.",
	            "oPaginate": {
	                "sFirst":    "Đầu",
	                "sLast":     "Cuối",
	                "sNext":     "Tiếp",
	                "sPrevious": "Trước"
	            },
	            "oAria": {
	                "sSortAscending":  ": activate to sort column ascending",
	                "sSortDescending": ": activate to sort column descending"
	            }
	        })
	        .withOption('responsive', true);

     	$scope.dtColumnDefs = [
        	DTColumnDefBuilder.newColumnDef(0).notSortable(),
    	];

	}

	$scope.removeItem = function(item) {

		if(item.id == "" || item.id == null || item.id == undefined)
		{
			toastr.error(Resources.common_error_exception, Resources.common_error_title);
			return false;
		}

		var confirmModal = bootbox.dialog({
		  	message: commonFunction.languageReplace(Languages.common_confirm_delete_question,[' '+item.name]),
		  	title: commonFunction.languageReplace(Languages.common_confirm_delete_title,[' '+item.name]),
		  	closeButton: true,
		  	animate: true,
		  	className: "confirmModal",
		  	backdrop: true,
		  	headerBackground: "bg-red",
		  	size: "small",
		  	locale: "vi",
		  	show: true,
		  	buttons: {
		  		cancel : {
		      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
		      	className: "btn-default",
		      	callback: function() {
		        	confirmModal.modal('hide');
		      	}
		    	},
				success: {
	      		label: '<i class="fa fa-trash" aria-hidden="true"></i>&nbsp;'+Languages.common_button_remove,
		      	className: "btn-danger",
		      	callback: function() {
		      		$("#listReLoading").show();
			        	var request = {
				        	area_id : item.id,
				        	area_type : item.type,
				    	};
				    	$http.post($scope.model.datainit.appURL + "/SystemApi/UpdateStatusArea", JSON.stringify(request))
				    		.then(
			               function (response) {
			                  var data = response.data;

							    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
						            {
						                toastr.error(data.messages, Languages.common_error_title);

						                if(data.auth != "" && data.auth != null  && data.auth != undefined)
						                {
						                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
						                }
						            }
							    	else if(data.error != "" && data.error != null  && data.error != undefined)
							    	{
							    		toastr.error(data.error, Languages.common_error_title);
							    	}
							    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
							    	{
							    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
							    		warning = warning.replace('["', '').replace('"]', '');
							    		toastr.warning(warning, Languages.common_warning_title);
							    	}
							    	else if(data.success != "" && data.success != null  && data.success != undefined)
							    	{
							    		toastr.success(data.success, Languages.common_success_title);
							    		$scope.formReset();
							    	}
							    	else
							    	{
							    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
							    		{
							    			toastr.error(data.systemerror, Languages.common_error_title);
							    			window.location.replace("./"+data.errorCode);
							    		}
							    		else
							    		{
							    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
							    		}
							    	}
							    	$("#listReLoading").hide();
							   },
					    		function (response) {
				               debugger;
				               var data = response.data;
									toastr.error(Languages.common_error_exception, Languages.common_error_title);
									$("#listReLoading").hide();
					    		}
					    	);
		      	}
		    	},
		  	}
		});
	};

	$scope.addItemProvince = function() {

		if (!$scope.model.request.txtProvinceName) 
		{
			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			return false;
		}

		var confirmModal = bootbox.dialog({
		  	message: commonFunction.languageReplace(Languages.common_confirm_add_question,[' '+$scope.model.request.txtProvinceName]),
		  	title: commonFunction.languageReplace(Languages.common_confirm_add_title,[' '+$scope.model.request.txtProvinceName]),
		  	closeButton: true,
		  	animate: true,
		  	className: "confirmModal",
		  	backdrop: true,
		  	headerBackground: "bg-blue",
		  	size: "small",
		  	locale: "vi",
		  	show: true,
		  	buttons: {
		  		cancel : {
		      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
		      	className: "btn-default",
		      	callback: function() {
		        		confirmModal.modal('hide');
		      	}
		    	},
		    	success: {
		      		label: '<i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;'+Languages.common_button_add,
			      	className: "btn-primary",
			      	callback: function() {

			      		$("#listReLoading").show();

				        	var request = {
				        		area_name : $scope.model.request.txtProvinceName,
				        		area_parent_id : 0,
					        	area_type : 1,
					    	};

					    	$http.post($scope.model.datainit.appURL + "/SystemApi/AddProvince", JSON.stringify(request))
					    	.then(
			               function (response) {
			                  var data = response.data;

							    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
						            {
						                toastr.error(data.messages, Languages.common_error_title);

						                if(data.auth != "" && data.auth != null  && data.auth != undefined)
						                {
						                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
						                }
						            }
							    	else if(data.error != "" && data.error != null  && data.error != undefined)
							    	{
							    		toastr.error(data.error, Languages.common_error_title);
							    	}
							    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
							    	{
							    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
							    		warning = warning.replace('["', '').replace('"]', '');
							    		toastr.warning(warning, Languages.common_warning_title);
							    	}
							    	else if(data.success != "" && data.success != null  && data.success != undefined)
							    	{
							    		toastr.success(data.success, Languages.common_success_title);
							    		$('#modalAddProvince').modal('toggle');
							    		$scope.formReset();
							    	}
							    	else
							    	{
							    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
							    		{
							    			toastr.error(data.systemerror, Languages.common_error_title);
							    			window.location.replace("./"+data.errorCode);
							    		}
							    		else
							    		{
							    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
							    		}
							    	}
							    	$("#listReLoading").hide();
						    	},
						    	function (response) {
				               debugger;
				               var data = response.data;
									toastr.error(Languages.common_error_exception, Languages.common_error_title);
									$("#listReLoading").hide();
						    	}
						   );
			     	}
	    		},
		  	}
		});
	};

	$scope.addItemDistrict = function() {

		if (!$scope.model.request.txtDistrictName) 
		{
			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			return false;
		}

		var confirmModal = bootbox.dialog({
		  	message: commonFunction.languageReplace(Languages.common_confirm_add_question,[' '+$scope.model.request.txtDistrictName]),
		  	title: commonFunction.languageReplace(Languages.common_confirm_add_title,[' '+$scope.model.request.txtDistrictName]),
		  	closeButton: true,
		  	animate: true,
		  	className: "confirmModal",
		  	backdrop: true,
		  	headerBackground: "bg-blue",
		  	size: "small",
		  	locale: "vi",
		  	show: true,
		  	buttons: {
		  		cancel : {
		      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
		      	className: "btn-default",
		      	callback: function() {
		        		confirmModal.modal('hide');
      			}
		    	},
		    	success: {
	      		label: '<i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;'+Languages.common_button_add,
		      	className: "btn-primary",
		      	callback: function() {

		      		$("#listReLoading").show();

			        	var request = {
			        		area_name : $scope.model.request.txtDistrictName,
			        		area_parent_id : $scope.model.request.txtProvinceId,
				        	area_type : 2,
					    };

					    $http.post($scope.model.datainit.appURL + "/SystemApi/AddDistrict", JSON.stringify(request))
					    	.then(
           					function (response) {
	               			var data = response.data;

							    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
						            {
						                toastr.error(data.messages, Languages.common_error_title);

						                if(data.auth != "" && data.auth != null  && data.auth != undefined)
						                {
						                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
						                }
						            }
							    	else if(data.error != "" && data.error != null  && data.error != undefined)
							    	{
							    		toastr.error(data.error, Languages.common_error_title);
							    	}
							    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
							    	{
							    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
							    		warning = warning.replace('["', '').replace('"]', '');
							    		toastr.warning(warning, Languages.common_warning_title);
							    	}
							    	else if(data.success != "" && data.success != null  && data.success != undefined)
							    	{
							    		toastr.success(data.success, Languages.common_success_title);
							    		$('#modalAddDistrict').modal('toggle');
							    		$scope.formReset();
							    	}
							    	else
							    	{
							    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
							    		{
							    			toastr.error(data.systemerror, Languages.common_error_title);
							    			window.location.replace("./"+data.errorCode);
							    		}
							    		else
							    		{
							    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
							    		}
							    	}
							    	$("#listReLoading").hide();
							   },
						    	function (response) {
				               debugger;
				               var data = response.data
									toastr.error(Languages.common_error_exception, Languages.common_error_title);
									$("#listReLoading").hide();
					    		}
					    	);
		      	}
		    	},
		  	}
		});
	};

	$scope.addItemWard = function() {

		if (!$scope.model.request.txtWardName) 
		{
			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			return false;
		}

		var confirmModal = bootbox.dialog({
		  	message: commonFunction.languageReplace(Languages.common_confirm_add_question,[' '+$scope.model.request.txtWardName]),
		  	title: commonFunction.languageReplace(Languages.common_confirm_add_title,[' '+$scope.model.request.txtWardName]),
		  	closeButton: true,
		  	animate: true,
		  	className: "confirmModal",
		  	backdrop: true,
		  	headerBackground: "bg-blue",
		  	size: "small",
		  	locale: "vi",
		  	show: true,
		  	buttons: {
		  		cancel : {
		      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
		      	className: "btn-default",
		      	callback: function() {
		        		confirmModal.modal('hide');
		      	}
			   },
			   success: {
	      		label: '<i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;'+Languages.common_button_add,
		      	className: "btn-primary",
		      	callback: function() {

		      		$("#listReLoading").show();

			        	var request = {
			        		area_name : $scope.model.request.txtWardName,
			        		area_parent_id : $scope.model.request.txtDistrictId,
				        	area_type : 3,
				    	};

			    		$http.post($scope.model.datainit.appURL + "/SystemApi/AddWard", JSON.stringify(request))
						    .then(
			               function (response) {
			                  var data = response.data;

							    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
						            {
						                toastr.error(data.messages, Languages.common_error_title);

						                if(data.auth != "" && data.auth != null  && data.auth != undefined)
						                {
						                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
						                }
						            }
							    	else if(data.error != "" && data.error != null  && data.error != undefined)
							    	{
							    		toastr.error(data.error, Languages.common_error_title);
							    	}
							    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
							    	{
							    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
							    		warning = warning.replace('["', '').replace('"]', '');
							    		toastr.warning(warning, Languages.common_warning_title);
							    	}
							    	else if(data.success != "" && data.success != null  && data.success != undefined)
							    	{
							    		toastr.success(data.success, Languages.common_success_title);
							    		$('#modalAddWard').modal('toggle');
							    		$scope.formReset();
							    	}
							    	else
							    	{
							    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
							    		{
							    			toastr.error(data.systemerror, Languages.common_error_title);
							    			window.location.replace("./"+data.errorCode);
							    		}
							    		else
							    		{
							    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
							    		}
							    	}
							    	$("#listReLoading").hide();
								},
						    	function (response) {
				               debugger;
				               var data = response.data
									toastr.error(Languages.common_error_exception, Languages.common_error_title);
									$("#listReLoading").hide();
						    	}
						   );
					}
	    		},
		  	}
		});
	};

	$scope.updateItemProvice = function() {

		if (!$scope.model.request.txtProvinceName) 
		{
			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			return false;
		}

		var confirmModal = bootbox.dialog({
		  	message: commonFunction.languageReplace(Languages.common_confirm_update_question,[' '+$scope.model.request.txtProvinceName]),
		  	title: commonFunction.languageReplace(Languages.common_confirm_update_title,[' '+$scope.model.request.txtProvinceName]),
		  	closeButton: true,
		  	animate: true,
		  	className: "confirmModal",
		  	backdrop: true,
		  	headerBackground: "bg-yellow",
		  	size: "small",
		  	locale: "vi",
		  	show: true,
		  	buttons: {
		  		cancel : {
		      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
		      	className: "btn-default",
		      	callback: function() {
		        		confirmModal.modal('hide');
		      	}
		    	},
		    	success: {
	      		label: '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;'+Languages.common_button_update,
		      	className: "btn-warning",
		      	callback: function() {

		      		$("#listReLoading").show();

			        	var request = {
			        		area_id : $scope.model.request.txtProvinceId,
			        		area_name : $scope.model.request.txtProvinceName,
			        		area_parent_id : (!$scope.model.request.slbProvinceParent) ? 0 : $scope.model.request.slbProvinceParent,
				        	area_type : 1,
				    	};

					   $http.post($scope.model.datainit.appURL + "/SystemApi/UpdateProvince", JSON.stringify(request))
					   	.then(
				            function (response) {
				               var data = response.data;

							    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
						            {
						                toastr.error(data.messages, Languages.common_error_title);

						                if(data.auth != "" && data.auth != null  && data.auth != undefined)
						                {
						                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
						                }
						            }
							    	else if(data.error != "" && data.error != null  && data.error != undefined)
							    	{
							    		toastr.error(data.error, Languages.common_error_title);
							    	}
							    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
							    	{
							    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
							    		warning = warning.replace('["', '').replace('"]', '');
							    		toastr.warning(warning, Languages.common_warning_title);
							    	}
							    	else if(data.success != "" && data.success != null  && data.success != undefined)
							    	{
							    		toastr.success(data.success, Languages.common_success_title);
							    		$('#modalUpdateProvince').modal('toggle');
							    		$scope.formReset();
							    	}
							    	else
							    	{
							    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
							    		{
							    			toastr.error(data.systemerror, Languages.common_error_title);
							    			window.location.replace("./"+data.errorCode);
							    		}
							    		else
							    		{
							    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
							    		}
							    	}
							    	$("#listReLoading").hide();
						    	},
					    		function (response) {
				               debugger;
				               var data = response.data
									toastr.error(Languages.common_error_exception, Languages.common_error_title);
									$("#listReLoading").hide();
					    		}
					    	);
		      	}
		    	}
	  		}
		});
	};

	$scope.updateItemDistrict = function() {

		if (!$scope.model.request.txtDistrictName) 
		{
			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			return false;
		}

		var confirmModal = bootbox.dialog({
		  	message: commonFunction.languageReplace(Languages.common_confirm_update_question,[' '+$scope.model.request.txtDistrictName]),
		  	title: commonFunction.languageReplace(Languages.common_confirm_update_title,[' '+$scope.model.request.txtDistrictName]),
		  	closeButton: true,
		  	animate: true,
		  	className: "confirmModal",
		  	backdrop: true,
		  	headerBackground: "bg-yellow",
		  	size: "small",
		  	locale: "vi",
		  	show: true,
		  	buttons: {
		  		cancel : {
		      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
		      	className: "btn-default",
		      	callback: function() {
		        		confirmModal.modal('hide');
		      	}
		    	},
		    	success: {
	      		label: '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;'+Languages.common_button_update,
		      	className: "btn-warning",
		      	callback: function() {

		      		$("#listReLoading").show();

			        	var request = {
			        		area_id : $scope.model.request.txtDistrictId,
			        		area_name : $scope.model.request.txtDistrictName,
			        		area_parent_id : (!$scope.model.request.slbDistrictParent) ? 0 : $scope.model.request.slbDistrictParent,
				        	area_type : 2,
				    	};

				    	$http.post($scope.model.datainit.appURL + "/SystemApi/UpdateDistrict", JSON.stringify(request))
				    		.then(
	            			function (response) {
			               var data = response.data;

							    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
						            {
						                toastr.error(data.messages, Languages.common_error_title);

						                if(data.auth != "" && data.auth != null  && data.auth != undefined)
						                {
						                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
						                }
						            }
							    	else if(data.error != "" && data.error != null  && data.error != undefined)
							    	{
							    		toastr.error(data.error, Languages.common_error_title);
							    	}
							    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
							    	{
							    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
							    		warning = warning.replace('["', '').replace('"]', '');
							    		toastr.warning(warning, Languages.common_warning_title);
							    	}
							    	else if(data.success != "" && data.success != null  && data.success != undefined)
							    	{
							    		toastr.success(data.success, Languages.common_success_title);
							    		$('#modalUpdateDistrict').modal('toggle');
							    		$scope.formReset();
							    	}
							    	else
							    	{
							    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
							    		{
							    			toastr.error(data.systemerror, Languages.common_error_title);
							    			window.location.replace("./"+data.errorCode);
							    		}
							    		else
							    		{
							    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
							    		}
							    	}
							    	$("#listReLoading").hide();
						    	},
				    			function (response) {
				               debugger;
				               var data = response.data
									toastr.error(Languages.common_error_exception, Languages.common_error_title);
									$("#listReLoading").hide();
				    			}
				    		);
		      	}
		    	}
		  	}
		});
	};

	$scope.updateItemWard = function() {

		if (!$scope.model.request.txtWardName) 
		{
			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			return false;
		}

		var confirmModal = bootbox.dialog({
		  	message: commonFunction.languageReplace(Languages.common_confirm_update_question,[' '+$scope.model.request.txtWardName]),
		  	title: commonFunction.languageReplace(Languages.common_confirm_update_title,[' '+$scope.model.request.txtWardName]),
		  	closeButton: true,
		  	animate: true,
		  	className: "confirmModal",
		  	backdrop: true,
		  	headerBackground: "bg-yellow",
		  	size: "small",
		  	locale: "vi",
		  	show: true,
		  	buttons: {
		  		cancel : {
		      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
		      	className: "btn-default",
		      	callback: function() {
		        		confirmModal.modal('hide');
		      	}
		    	},
		    	success: {
	      		label: '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;'+Languages.common_button_update,
		      	className: "btn-warning",
		      	callback: function() {

		      		$("#listReLoading").show();

			        	var request = {
			        		area_id : $scope.model.request.txtWardId,
			        		area_name : $scope.model.request.txtWardName,
			        		area_parent_id : (!$scope.model.request.slbWardParent) ? 0 : $scope.model.request.slbWardParent,
				        	area_type : 3,
				    	};

				    	$http.post($scope.model.datainit.appURL + "/SystemApi/UpdateWard", JSON.stringify(request))
				    		.then(
            				function (response) {
              					var data = response.data;

							    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
						            {
						                toastr.error(data.messages, Languages.common_error_title);

						                if(data.auth != "" && data.auth != null  && data.auth != undefined)
						                {
						                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
						                }
						            }
							    	else if(data.error != "" && data.error != null  && data.error != undefined)
							    	{
							    		toastr.error(data.error, Languages.common_error_title);
							    	}
							    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
							    	{
							    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
							    		warning = warning.replace('["', '').replace('"]', '');
							    		toastr.warning(warning, Languages.common_warning_title);
							    	}
							    	else if(data.success != "" && data.success != null  && data.success != undefined)
							    	{
							    		toastr.success(data.success, Languages.common_success_title);
							    		$('#modalUpdateWard').modal('toggle');
							    		$scope.formReset();
							    	}
							    	else
							    	{
							    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
							    		{
							    			toastr.error(data.systemerror, Languages.common_error_title);
							    			window.location.replace("./"+data.errorCode);
							    		}
							    		else
							    		{
							    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
							    		}
							    	}
							    	$("#listReLoading").hide();
				    			},
				    			function (response) {
				               debugger;
				               var data = response.data
									toastr.error(Languages.common_error_exception, Languages.common_error_title);
									$("#listReLoading").hide();
				    			}
				    		);
		      	}
		    	}
		  	}
		});
	};

	$('#modalUpdate').on('hidden.bs.modal', function (e) {
		$scope.model.request = {
			txtId : "",
			txtName : "",
		};
		$scope.frmUpdate.$setPristine();
		$scope.frmUpdate.$setUntouched();
	});

	$scope.formReset = function() {

		$scope.pageInit();

		$scope.model.request = {
			slbProvinceParent : 0,
			slbDistrictParent : 0,
			slbWardParent : 0,
			txtProvinceId : "",
			txtProvinceName : "",
			slbDistrictParent : "",
			txtDistrictId : "",
			txtDistrictName : "",
			txtWardId : "",
			txtWardName : "",
		};

		$scope.frmAddProvince.$setPristine();
		$scope.frmAddProvince.$setUntouched();

		$scope.frmAddDistrict.$setPristine();
		$scope.frmAddDistrict.$setUntouched();
		
		$scope.frmAddWard.$setPristine();
		$scope.frmAddWard.$setUntouched();

		$scope.frmUpdateProvince.$setPristine();
		$scope.frmUpdateProvince.$setUntouched();

		$scope.frmUpdateDistrict.$setPristine();
		$scope.frmUpdateDistrict.$setUntouched();

		$scope.frmUpdateWard.$setPristine();
		$scope.frmUpdateWard.$setUntouched();
	};

	$scope.openUpdateItem = function(item) {

		if (!item) 
		{
			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			return false;
		}

		$("#listReLoading").show();

		$scope.model.request = {
			slbProvinceParent : 0,
			slbDistrictParent : 0,
			slbWardParent : 0,
			txtProvinceId : "",
			txtProvinceName : "",
			slbDistrictParent : "",
			txtDistrictId : "",
			txtDistrictName : "",
			txtWardId : "",
			txtWardName : "",
		};

		$scope.frmAddProvince.$setPristine();
		$scope.frmAddProvince.$setUntouched();

		$scope.frmAddDistrict.$setPristine();
		$scope.frmAddDistrict.$setUntouched();

		$scope.frmAddWard.$setPristine();
		$scope.frmAddWard.$setUntouched();

		$scope.frmUpdateProvince.$setPristine();
		$scope.frmUpdateProvince.$setUntouched();

		$scope.frmUpdateDistrict.$setPristine();
		$scope.frmUpdateDistrict.$setUntouched();

		$scope.frmUpdateWard.$setPristine();
		$scope.frmUpdateWard.$setUntouched();

		var request = {
     		area_id : item.id,
     		area_parent_id : item.parent_id,
     		area_type : item.type
    	};

    	$http.post($scope.model.datainit.appURL + "/SystemApi/GetListAreasForUpdateArea", JSON.stringify(request))
    		.then(
            function (response) {
              var data = response.data;

			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    		$scope.model.datainit.dtgListUpdate = [];
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		toastr.warning(data.warning, Languages.common_warning_title);
			    		$scope.model.datainit.dtgListUpdate = [];
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{

			    		var emptyItem = {
			    			'area_id' : 0,
		            	'area_name' : "Không Có Khu Vực Cha"
			    		}
			    		data.success.unshift(data.success, emptyItem);
			    		 
			    		$scope.model.datainit.dtgListUpdate = data.success;

			    		if(item.type == 1)
						{
							$("#listReLoading").show();

							$('#modalUpdateProvince').modal('toggle');

							$scope.model.request.txtProvinceId = item.id;
							$scope.model.request.txtProvinceName = item.name;
							$scope.model.request.slbDistrictParent = 0;
						}

						if(item.type == 2)
						{
							
							$('#modalUpdateDistrict').modal('toggle');

							$scope.model.request.txtDistrictId = item.id;
							$scope.model.request.txtDistrictName = item.name;
							$scope.model.request.slbDistrictParent = item.parent_id;
						}

						if(item.type == 3)
						{
							
							$('#modalUpdateWard').modal('toggle');

							$scope.model.request.txtWardId = item.id;
							$scope.model.request.txtWardName = item.name;
							$scope.model.request.slbWardParent = item.parent_id;
						}

			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    		$scope.model.datainit.dtgListUpdate = [];
			    	}
			    	$("#listReLoading").hide();
				},
					function (response) {
	               debugger;
	               var data = response.data
						toastr.error(Languages.common_error_exception, Languages.common_error_title);
						$scope.model.datainit.dtgListUpdate = [];
						$("#listReLoading").hide();
				}
			);

	};

	$scope.openAddItem = function(item) {

		if (!item) 
		{
			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			return false;
		}

		$scope.model.request = {
			slbProvinceParent : 0,
			slbDistrictParent : 0,
			slbWardParent : 0,
			txtProvinceId : "",
			txtProvinceName : "",
			slbDistrictParent : "",
			txtDistrictId : "",
			txtDistrictName : "",
			txtWardId : "",
			txtWardName : "",
		};

		$scope.frmAddProvince.$setPristine();
		$scope.frmAddProvince.$setUntouched();

		$scope.frmAddDistrict.$setPristine();
		$scope.frmAddDistrict.$setUntouched();

		$scope.frmAddWard.$setPristine();
		$scope.frmAddWard.$setUntouched();

		$scope.frmUpdateProvince.$setPristine();
		$scope.frmUpdateProvince.$setUntouched();

		$scope.frmUpdateDistrict.$setPristine();
		$scope.frmUpdateDistrict.$setUntouched();

		$scope.frmUpdateWard.$setPristine();
		$scope.frmUpdateWard.$setUntouched();


		if(item.type == 1)
		{
			$('#modalAddDistrict').modal('toggle');

			$scope.model.request.txtProvinceId = item.id;
		}
		if(item.type == 2)
		{
			$('#modalAddWard').modal('toggle');

			$scope.model.request.txtDistrictId = item.id;
		}
	}

	// $scope.remove = function (scope) {
 //        scope.remove();
 //      };

 //      $scope.toggle = function (scope) {
 //        scope.toggle();
 //      };

	// $scope.moveLastToTheBeginning = function () {
	// 	var a = $scope.data.pop();
	// 	$scope.data.splice(0, 0, a);
	// };

	// $scope.newSubItem = function (scope) {
	// 	var nodeData = scope.$modelValue;
	// 		nodeData.nodes.push({
	// 		id: nodeData.id * 10 + nodeData.nodes.length,
	// 		title: nodeData.title + '.' + (nodeData.nodes.length + 1),
	// 		nodes: []
	// 	});
	// };

	// $scope.collapseAll = function () {
	// 	$scope.$broadcast('angular-ui-tree:collapse-all');
	// };

	// $scope.expandAll = function () {
	// 	$scope.$broadcast('angular-ui-tree:expand-all');
	// };

	$scope.visible = function (item) {
		return !($scope.query && $scope.query.length > 0 && item.name.indexOf($scope.query) == -1);

	};

	$scope.findNodes = function () {

	};

	$scope.treeOptions = {
		beforeDrop : function (e) {
			var sourceValue = e.source.nodeScope.$modelValue.value,
				destValue = e.dest.nodesScope.node ? e.dest.nodesScope.node.value : undefined,
				modalInstance;

			// display modal if the node is being dropped into a smaller container
			if (sourceValue > destValue) {
				modalInstance = $modal.open({
				templateUrl: 'drop-modal.html'
			});
			// or return the simple boolean result from $modal
			if (!e.source.nodeScope.$treeScope.usePromise) 
			{
				return modalInstance.result;
			} 
			else 
			{ // return a promise
				return modalInstance.result.then(function (allowDrop) {
					if (!allowDrop) {
						return $q.reject();
					}
						return allowDrop;
					});
				}
			}
		}
	};
	

});
