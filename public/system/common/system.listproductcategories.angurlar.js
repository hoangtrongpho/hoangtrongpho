/*!
 *
 *	Develop by Vinh Nguyễn - Ho Chi Minh City - 2017
 * 	Copyright Â© 2016-2017 Skyfire Team. All rights reserved.
 *	Design by Skyfire Team 2.3.3
 *
 *	Phone: 0975 578 276
 *	Skype: enjoyvinh
 *
 */

"use strict";

// app.js
// create angular app
var categoryApp = angular.module('categoryApp', ['commonApp', 'ui.tree','isteven-multi-select', 'daterangepicker', 'localytics.directives']);

categoryApp.factory('CategoriesModels', function() {

	var model={
		datainit : {
			dtgList : [],
			modalStatus:"",
			slbParentList : [],
			dtgListParent : [{
				cate_id : "",
				cate_name : ""
			}],
			appURL : "http://localhost:8000",
		},
	};

	model.request = {
		txtParent_Cate_Id : "",
		slbParent : 0,
		txtId : "",
		txtName : "",
		txtDescription : "",
		txtAvatar : $('meta[name="app_url"]').attr('content') + "public/images/default-image.png"
	};

	model.localLang = {
	    selectAll       : Languages.common_button_select_all,
	    selectNone      : Languages.common_button_deselected_all,
	    reset           : Languages.common_button_cancel,
	    search          : Languages.common_input_label_search,
	    nothingSelected : Languages.common_input_label_select        //default-label is deprecated and replaced with this.
	}

	return model;
});

// create angular controller
categoryApp.controller('categoryController', function(CategoriesModels, commonFunction, $scope, $rootScope, $http, $filter, $window, $location, toastr) {

	$scope.model = CategoriesModels;

	$scope.pageInit = function() {

		$scope.model.datainit.appURL = $('meta[name="app_url"]').attr('content');

		$scope.loadList();
		$scope.GetCatParent();
		$scope.UpdateOrder();
		// $scope.$broadcast('angular-ui-tree:collapse-all');
	  
	};

	$scope.loadList = function() {

		$("#listReLoading").show();

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetListProductCategories")
	    	.then(
               	function (response) {
                  	var data = response.data;

			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    		$scope.model.datainit.dtgList = [];
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		toastr.warning(data.warning, Languages.common_warning_title);
			    		$scope.model.datainit.dtgList = [];
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		$scope.model.datainit.dtgList = data.success;
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    		$scope.model.datainit.dtgList = [];
			    	}
			    	$("#listReLoading").hide();
			    },
               	function (response) {
                  	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$scope.model.datainit.dtgList = [];
					$("#listReLoading").hide();
			    }
			);

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetListProductCategoriesForChosen")
	    	.then(
               	function (response) {
                  	var data = response.data;

			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    		$scope.model.datainit.dtgListParent = [{
			    			'cate_id' : 0,
		            		'cate_name' : "Nhóm Cha"
			    		}];
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		// toastr.warning(data.warning, Languages.common_warning_title);
			    		$scope.model.datainit.dtgListParent = [{
			    			'cate_id' : 0,
		            		'cate_name' : "Nhóm Cha"
			    		}];
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{

			    		var emptyItem = {
			    			'cate_id' : 0,
		            		'cate_name' : "Nhóm Cha"
			    		}
			    		data.success.unshift(data.success, emptyItem);
			    		 
			    		$scope.model.datainit.dtgListParent = data.success;

			    		$scope.model.request.slbParent = 0;
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    		$scope.model.datainit.dtgListParent = [{
			    			'cate_id' : 0,
		            		'cate_name' : "Nhóm Cha"
			    		}];
			    	}
			    },
               	function (response) {
                  	debugger;
                  	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$scope.model.datainit.dtgListParent = [
						{
			    			'cate_id' : 0,
		            		'cate_name' : "Nhóm Cha"
		    			}
		    		];
	    		}
	    	);
	}
	// UpdateOrder
	$scope.UpdateOrder = function() {

		$("#listReLoading").show();

	    $http.post($scope.model.datainit.appURL + "/SystemApi/UpdateOrder")
	    	.then(
               	function (response) {
                  	var data = response.data;

			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    		
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		toastr.warning(data.warning, Languages.common_warning_title);
			    		
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		// alert(data.success);
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    		
			    	}
			    	$("#listReLoading").hide();
			    },
               	function (response) {
                  	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					
					$("#listReLoading").hide();
			    }
			); 
	};
	// GetCatParent
	$scope.GetCatParent = function() {
		$("#listReLoading").show();

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetCatParent")
	    	.then(
               	function (response) {
                  	var data = response.data;

			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    		$scope.model.datainit.slbParentList = [];
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		toastr.warning(data.warning, Languages.common_warning_title);
			    		$scope.model.datainit.slbParentList = [];
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{	
			    		$scope.model.datainit.slbParentList.push( {"cate_id": 0,
						"cate_name" :"Danh Mục Gốc"} );
						$.each( data.success, function( i ) {
							var obj;var val1;var val2;
							$.each(data.success[i], function (key, val) {
						        if(key == "cate_id")
						        	val1=val;
								if(key == "cate_name")
						        	val2=val;
						    });
						    obj = {"cate_id": val1, "cate_name" : val2};
						    
					    	$scope.model.datainit.slbParentList.push(obj);
						});

			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    		$scope.model.datainit.slbParentList = [];
			    	}
			    	$("#listReLoading").hide();
			    },
               	function (response) {
                  	debugger;
                  	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$scope.model.datainit.slbParentList = [];
					$("#listReLoading").hide();
			    }
			);
	}

	$scope.removeItem = function(item) {

		if(item.cate_id == "" || item.cate_id == null || item.cate_id == undefined)
		{
			toastr.error(Resources.common_error_exception, Resources.common_error_title);
			return false;
		}

		var confirmModal = bootbox.dialog({
		  	message: commonFunction.languageReplace(Languages.common_confirm_delete_question,[' nhóm '+item.cate_name]),
		  	title: commonFunction.languageReplace(Languages.common_confirm_delete_title,[' nhóm '+item.cate_name]),
		  	closeButton: true,
		  	animate: true,
		  	className: "confirmModal",
		  	backdrop: true,
		  	headerBackground: "bg-red",
		  	size: "small",
		  	locale: "vi",
		  	show: true,
		  	buttons: {
		  		cancel : {
		      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
		      	className: "btn-default",
		      	callback: function() {
		        		confirmModal.modal('hide');
		      	}
			    },
			    success: {
		      		label: '<i class="fa fa-trash" aria-hidden="true"></i>&nbsp;'+Languages.common_button_remove,
			      	className: "btn-danger",
			      	callback: function() {
			      		$("#listReLoading").show();
			        		var request = {
					        cate_id : item.cate_id,
					    };
					    $http.post($scope.model.datainit.appURL + "/SystemApi/RemoveProductCategory", JSON.stringify(request))
					    	.then(
               					function (response) {
                  					var data = response.data;

							    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
						            {
						                toastr.error(data.messages, Languages.common_error_title);

						                if(data.auth != "" && data.auth != null  && data.auth != undefined)
						                {
						                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
						                }
						            }
							    	else if(data.error != "" && data.error != null  && data.error != undefined)
							    	{
							    		toastr.error(data.error, Languages.common_error_title);
							    	}
							    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
							    	{
							    		toastr.warning(data.warning, Languages.common_warning_title);
							    	}
							    	else if(data.success != "" && data.success != null  && data.success != undefined)
							    	{
							    		toastr.success(data.success, Languages.common_success_title);
							    		$scope.formReset();
							    	}
							    	else
							    	{
							    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
							    		{
							    			toastr.error(data.systemerror, Languages.common_error_title);
							    			window.location.replace("./"+data.errorCode);
							    		}
							    		else
							    		{
							    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
							    		}
							    	}
							    	$("#listReLoading").hide();
					    		},
               					function (response) {
				                  	debugger;
				                  	var data = response.data;
									toastr.error(Languages.common_error_exception, Languages.common_error_title);
									$("#listReLoading").hide();
			    				}
							);
			      	}
			    }
		  	}
		});
	};

	$scope.addItem = function() {

		if ($scope.frmAdd.$invalid) 
		{
			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			return false;
		}

		var confirmModal = bootbox.dialog({
		  	message: commonFunction.languageReplace(Languages.common_confirm_add_question,[' Nhóm '+$scope.model.request.txtName]),
		  	title: commonFunction.languageReplace(Languages.common_confirm_add_title,[' Nhóm '+$scope.model.request.txtName]),
		  	closeButton: true,
		  	animate: true,
		  	className: "confirmModal",
		  	backdrop: true,
		  	headerBackground: "bg-blue",
		  	size: "small",
		  	locale: "vi",
		  	show: true,
		  	buttons: {
		  		cancel : {
			      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
			      	className: "btn-default",
			      	callback: function() {
			        		confirmModal.modal('hide');
			      	}
			    },
			    success: {
		      		label: '<i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;'+Languages.common_button_add,
			      	className: "btn-primary",
			      	callback: function() {

			      		$("#listReLoading").show();
			      		if($.isArray($scope.model.request.txtParent_Cate_Id)){
			      			var cate_parent_id = $scope.model.request.txtParent_Cate_Id[0].cate_id;
			      		}
			      		else{
			      			cate_parent_id = $scope.model.request.txtParent_Cate_Id;
			      		}
			        	var request = {
							cate_name : $scope.model.request.txtName,
							cate_parent_id : cate_parent_id,
							cate_description : $scope.model.request.txtDescription,
							cate_thumbnail : $("#thumbnail1").val(),
				    	};
				    	// alert($scope.model.request.txtParent_Cate_Id);
			    		$http.post($scope.model.datainit.appURL + "/SystemApi/AddProductCategory", JSON.stringify(request))
					   		.then(
           						function (response) {
              						var data = response.data;

							    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
						            {
						                toastr.error(data.messages, Languages.common_error_title);

						                if(data.auth != "" && data.auth != null  && data.auth != undefined)
						                {
						                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
						                }
						            }
							    	else if(data.error != "" && data.error != null  && data.error != undefined)
							    	{
							    		toastr.error(data.error, Languages.common_error_title);
							    	}
							    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
							    	{
							    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
							    		warning = warning.replace('["', '').replace('"]', '');
							    		toastr.warning(warning, Languages.common_warning_title);
							    	}
							    	else if(data.success != "" && data.success != null  && data.success != undefined)
							    	{
							    		toastr.success(data.success, Languages.common_success_title);
							    		$('#modalAdd').modal('toggle');
							    		$scope.formReset();
							    	}
							    	else
							    	{
							    		// if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
							    		// {
							    		// 	toastr.error(data.systemerror, Languages.common_error_title);
							    		// 	window.location.replace("./"+data.errorCode);
							    		// }
							    		// else
							    		// {
							    		// 	toastr.error(Languages.common_error_exception, Languages.common_error_title);
							    		// }
							    	}
							    	$("#listReLoading").hide();
						   		},
               					function (response) {
				                  	var data = response.data;
									toastr.error(Languages.common_error_exception, Languages.common_error_title);
									$("#listReLoading").hide();
			    				}
							);
		      		}
		    	},
		  	}
		});
	};

	$scope.updateItem = function() {

		var confirmModal = bootbox.dialog({
		  	message: commonFunction.languageReplace(Languages.common_confirm_update_question,[' Nhóm  1 '+$scope.model.request.txtName]),
		  	title: commonFunction.languageReplace(Languages.common_confirm_update_title,[' Nhóm 1 '+$scope.model.request.txtName]),
		  	closeButton: true,
		  	animate: true,
		  	className: "confirmModal",
		  	backdrop: true,
		  	headerBackground: "bg-yellow",
		  	size: "small",
		  	locale: "vi",
		  	show: true,
		  	buttons: {
		  		cancel : {
		      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
		      	className: "btn-default",
		      	callback: function() {
			        	confirmModal.modal('hide');
			      }
			   },
			   success: {
	      		label: '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;'+Languages.common_button_update,
		      	className: "btn-warning",
		      	callback: function() {

	      			$("#listReLoading").show();
	      				if($scope.model.request.txtParent_Cate_Id == 0 ){
	      					var cate_parent_id = $scope.model.request.txtParent_Cate_Id;
			      		}
			      		else{
			      			var cate_parent_id = $scope.model.request.txtParent_Cate_Id[0].cate_id;
			      		}
	      				// var cate_parent_id = $scope.model.request.txtParent_Cate_Id[0].cate_id;
			      		if(cate_parent_id==null || cate_parent_id==undefined )
			      		{
			      			cate_parent_id = $scope.model.request.txtParent_Cate_Id;
			      		}
			        	var request = {
			        		cate_id : $scope.model.request.txtId,
			        		cate_parent_id : cate_parent_id,
							cate_name : $scope.model.request.txtName,
							cate_description : $scope.model.request.txtDescription,
							cate_thumbnail : $("#thumbnail1").val(),
				    	};

					   	$http.post($scope.model.datainit.appURL + "/SystemApi/UpdateProductCategory", JSON.stringify(request))
					   		.then(
               					function (response) {
                  					var data = response.data;

							    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
						            {
						                toastr.error(data.messages, Languages.common_error_title);

						                if(data.auth != "" && data.auth != null  && data.auth != undefined)
						                {
						                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
						                }
						            }
							    	else if(data.error != "" && data.error != null  && data.error != undefined)
							    	{
							    		toastr.error(data.error, Languages.common_error_title);
							    	}
							    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
							    	{
							    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
							    		warning = warning.replace('["', '').replace('"]', '');
							    		toastr.warning(warning, Languages.common_warning_title);
							    	}
							    	else if(data.success != "" && data.success != null  && data.success != undefined)
							    	{
							    		toastr.success(data.success, Languages.common_success_title);
							    		$('#modalAdd').modal('toggle');
							    		$scope.loadList();
							    	}
							    	else
							    	{
							    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
							    		{
							    			toastr.error(data.systemerror, Languages.common_error_title);
							    			window.location.replace("./"+data.errorCode);
							    		}
							    		else
							    		{
							    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
							    		}
							    	}
							    	$("#listReLoading").hide();
						   		},
               					function (response) {
				                  	debugger;
				                  	var data = response.data;
									toastr.error(Languages.common_error_exception, Languages.common_error_title);
									$("#listReLoading").hide();
			    				}
							);
	      			}
	    		}	
	  		}
		});
	};

	$('#modalAdd').on('hidden.bs.modal', function (e) {
		$scope.model.request = {
			txtId : "",
			slbParent : 0,
			txtName : "",
			txtDescription : "",
			txtAvatar : $('meta[name="app_url"]').attr('content') + "public/images/default-image.png"
		};

		$scope.frmAdd.$setPristine();
		$scope.frmAdd.$setUntouched();
	});

	$scope.formReset = function() {

		$scope.pageInit();

		$scope.model.request = {
			txtId : "",
			slbParent : 0,
			txtName : "",
			txtDescription : "",
			txtAvatar : $('meta[name="app_url"]').attr('content') + "public/images/default-image.png"
		};

		$scope.frmAdd.$setPristine();
		$scope.frmAdd.$setUntouched();

		$scope.frmUpdate.$setPristine();
		$scope.frmUpdate.$setUntouched();
	};

	$scope.openAddItem = function(item,status) {
		$scope.model.datainit.modalStatus = status;
		// reset MODAL
		$scope.model.request.txtParent_Cate_Id ="";
		$scope.model.request.txtId ="";
		$scope.model.request.txtName ="";
		$scope.model.request.txtDescription ="";
		$scope.model.request.txtAvatar = $('meta[name="app_url"]').attr('content') + "public/images/default-image.png";
		angular.forEach($scope.model.datainit.slbParentList, function(value, key2) {
		    	value.ticked = false;
			});
		// alert(item.cate_parent_id);
		if (!item) 
		{
			return false;
		}
		// add
		if(status=="add")
		{
			$scope.model.request.txtParent_Cate_Id = item.cate_id;
			// alert($scope.model.request.txtParent_Cate_Id);
			// get parent cat to select box
			angular.forEach($scope.model.datainit.slbParentList, function(value, key2) {
			    if(value.cate_id == item.cate_id)
			    {
			    	value.ticked = true;
			    }
			});
			$scope.frmAdd.$setPristine();
			$scope.frmAdd.$setUntouched();
		}
		// update
		else{
			$scope.model.request.txtParent_Cate_Id = item.cate_parent_id;
			$scope.model.request.txtId = item.cate_id;
			$scope.model.request.txtName = item.cate_name;
			$scope.model.request.txtDescription = item.cate_description;
			$scope.model.request.txtAvatar = item.cate_thumbnail;
			angular.forEach($scope.model.datainit.slbParentList, function(value, key2) {
			    if(value.cate_id == item.cate_parent_id)
			    {
			    	value.ticked = true;
			    }
			});
		}
		

		$('#modalAdd').modal('toggle');
	}

	$scope.visible = function (item) {
		return !($scope.query && $scope.query.length > 0 && item.name.indexOf($scope.query) == -1);
	};

	$scope.findNodes = function () {};

	$scope.treeOptions = {
		beforeDrop : function (e) {

			console.log(e);

			var	sourceValue = null;
			var	destValue = {
					cate_id : 0
				};
			var indexItem = e.dest.index;

			var dest_index = e.dest.index;

			var source_index = e.source.index;

			var source_cate_parent_id = "";
			var source_cate_id = "";
			var dest_cate_id = "";
			var dest_cate_parent_id = "";
			

			if(e.source.nodeScope.$modelValue) {
				sourceValue = e.source.nodeScope.$modelValue;

				source_cate_parent_id = e.source.nodeScope.$modelValue.cate_parent_id;
				source_cate_id = e.source.nodeScope.$modelValue.cate_id;
			}
			else
			{
				toastr.error(Languages.common_error_exception, Languages.common_error_title);
				return false;
			}

			if(e.dest.nodesScope.$nodeScope) {
				destValue = e.dest.nodesScope.$nodeScope.$modelValue;
				dest_cate_id = e.dest.nodesScope.$nodeScope.$modelValue.cate_id;
				dest_cate_parent_id = e.dest.nodesScope.$nodeScope.$modelValue.cate_parent_id;
			}

			if(e.source.index == e.dest.index && sourceValue.cate_parent_id == destValue.cate_id)
			{
				return false;
			}
			var confirmModal = bootbox.dialog({
		  	message: commonFunction.languageReplace(Languages.common_confirm_update_question,[' vị trí của nhóm 2 '+ sourceValue.cate_name]),
		  	title: commonFunction.languageReplace(Languages.common_confirm_update_title,[' vị trí của nhóm 2 '+ sourceValue.cate_name]),
		  	closeButton: true,
		  	animate: true,
		  	className: "confirmModal",
		  	backdrop: true,
		  	headerBackground: "bg-yellow",
		  	size: "small",
		  	locale: "vi",
		  	show: true,
		  	buttons: {
			  		cancel : {
				      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
				      	className: "btn-default",
				      	callback: function() {
				        		$("#listReLoading").hide();
				        		$scope.formReset();
				      	}
				    },
				    success: {
			      		label: '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;'+Languages.common_button_update,
				      	className: "btn-warning",
				      	callback: function() {
				      		var request = {
				        		source_index : source_index,
				        		dest_index : dest_index,
				        		source_cate_parent_id : source_cate_parent_id,
				        		source_cate_id : source_cate_id,
				        		dest_cate_id : dest_cate_id,
				        		dest_cate_parent_id : dest_cate_parent_id,
					    	};
					    	// alert("source_index:"+source_index+"/dest_index:"+dest_index);
					    	$("#listReLoading").show();
		
						    $http.post($scope.model.datainit.appURL + "/SystemApi/UpdatePtoPOrder", JSON.stringify(request))
						    	.then(
               						function (response) {
                  						var data = response.data;

								    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
							            {
							                toastr.error(data.messages, Languages.common_error_title);

							                if(data.auth != "" && data.auth != null  && data.auth != undefined)
							                {
							                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
							                }
							            }
								    	else if(data.error != "" && data.error != null  && data.error != undefined)
								    	{
								    		toastr.error(data.error, Languages.common_error_title);
								    	}
								    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
								    	{
								    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
								    		warning = warning.replace('["', '').replace('"]', '');
								    		toastr.warning(warning, Languages.common_warning_title);
								    	}
								    	else if(data.success != "" && data.success != null  && data.success != undefined)
								    	{
								    		toastr.success(data.success, Languages.common_success_title);
								    		$scope.formReset();
								    	}
								    	else
								    	{
								    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
								    		{
								    			toastr.error(data.systemerror, Languages.common_error_title);
								    			window.location.replace("./"+data.errorCode);
								    		}
								    		else
								    		{
								    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
								    		}
								    	}
								    	$("#listReLoading").hide();
						    		},
	               					function (response) {
					                  	debugger;
					                  	var data = response.data;
										toastr.error(Languages.common_error_exception, Languages.common_error_title);
										$("#listReLoading").hide();
				    				}
								);
								// // end function
					      	}
					    }
				  	}
				});
			
		}
	};
	// slbItemClick
	$scope.slbItemClick = function(item,element)
	{
   		if(element == "txtParent_Cate_Id")
		{
			$('#'+element).val(item.cate_id); 
   			$('#'+element).trigger('input');
		}
		// alert($scope.model.request.txtParent_Cate_Id);
	};
	

});
