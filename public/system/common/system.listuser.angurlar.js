/*!
 *
 *	Develop by Vinh Nguyễn - Ho Chi Minh City - 2017
 * 	Copyright Â© 2016-2017 Skyfire Team. All rights reserved.
 *	Design by Skyfire Team 2.3.3
 *
 *	Phone: 0975 578 276
 *	Skype: enjoyvinh
 *
 */

"use strict";

// app.js
// create angular app
var userApp = angular.module('userApp', ['commonApp', 'bootstrap-switch','isteven-multi-select', 'datatables',
	'datatables.colreorder','datatables.bootstrap','datatables.tabletools']);

userApp.factory('UserModels', function() {

	var model={
		datainit : {
			slbListGroup : [],
			dtgListUser : [],
			slbBranch:[],
			useraccountupdate : false,
			appURL : "http://localhost:8000"
		},
	};

	model.request = {
		txtBranchId:"",
		txtBranchName:"",
		txtBranchPhone:"",
		txtBranchAddress:"",
		slbBranch:[],

		user_id : "",
		user_account : "",
		auth_id : "",
		user_email : "",
		user_avatar : $('meta[name="app_url"]').attr('content') + "/images/default-image.png",
		user_fullname :  "",
		user_sex : 1,
		user_phone : "",
		user_password : "",
		user_password_confirm : "",
		user_birthday : moment(),
		user_login_count : 0,
		branch_id:"",
		status : 1
	};

	model.result = {

	};

	model.localLang = {
	    selectAll       : "Chọn Tất Cả",
	    selectNone      : "Bỏ Chọn Tất Cả",
	    reset           : "Hủy",
	    search          : "Tìm Kiếm...",
	    nothingSelected : "Vui Lòng Chọn"         //default-label is deprecated and replaced with this.
	}

	return model;
});

// create angular controller
userApp.controller('addUserController', function(UserModels, commonFunction, $scope, $rootScope, $http, $filter, $window, $location, $uibModal, toastr) {

	$scope.model = UserModels;

	$scope.pageInit = function() {
		
		$scope.model.datainit.appURL = $('meta[name="app_url"]').attr('content');

		$("#addReLoading").show();
		$scope.GetBranchList();
	    $http.post($scope.model.datainit.appURL + "/SystemApi/apiGetListGroups")
	    	.then(
            	function (response) {
              		var data = response.data;

			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    		$scope.model.datainit.slbListGroup = [];
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		toastr.warning(data.warning, Languages.common_warning_title);
			    		$scope.model.datainit.slbListGroup = [];
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		$scope.model.datainit.slbListGroup = data.success;
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    		$scope.model.datainit.slbListGroup = [];
			    	}
			    	$("#addReLoading").hide();
			    },
               	function (response) {
	            
		            var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$scope.model.datainit.slbListGroup = [];
					$("#addReLoading").hide();
			    }
			);
	};

	$rootScope.$on("callFormReset", function(){
		$scope.formReset();
    });

    $scope.removeItem = function(id) {

    	$rootScope.$emit("callRemoveItem", id);

    }

    $scope.resetChoiceThumbnail = function() {
    	$("#holder").attr("src","");
		$("#thumbnail").val("");
    	$scope.model.request.user_avatar = sessionStorage.systemPublicPath + "/images/default-image.png";
    };
	
	$scope.formReset = function() {

		$("#addReLoading").show();

		$("#holder").attr("src","");
		$("#thumbnail").val("");

		$scope.pageInit();

		$scope.model.request = {
			user_id : "",
			user_account : "",
			auth_id : "",
			slbListGroup : "",
			user_email : "",
			user_avatar : sessionStorage.systemPublicPath + "/images/default-image.png",
			user_fullname :  "",
			user_sex : 1,
			user_password : "",
			user_password_confirm : "",
			user_login_count : 0,
			status : 1,
		};

		$scope.model.datainit.useraccountupdate = false;

		angular.forEach($scope.model.datainit.slbListGroup, function(value, key) {
			value.ticked = false;
		});

		$scope.frmAddUser.$setPristine();
		$scope.frmAddUser.$setUntouched();

		$("#addReLoading").hide();

	};

	//Hàm Chức Năng Cho SelectBox
	$scope.slbClear = function()
	{
		angular.forEach($scope.model.datainit.slbListGroup, function(value, key) {
	    	value.ticked = false;
		});
	   	$('#txtAuthId').val(""); 
	   	$('#txtAuthId').trigger('input');
	};
	$scope.slbItemClick = function(item,element)
	{
	   	
		if(element == "txtAuthId")
		{
			$('#'+element).val(item.auth_id); 
   			$('#'+element).trigger('input');

			angular.forEach($scope.model.datainit.slbListGroup, function(value, key) {
			    if(value.auth_id == item.auth_id)
			    {
			    	value.ticked = true;
			    }
			    else
			    {
			    	value.ticked = false;
			    }
			});
		}
		else if(element == "txtBranchId"){
			$('#'+element).val(item.branch_id); 
   			$('#'+element).trigger('input');

			angular.forEach($scope.model.datainit.slbBranch, function(value, key) {
			    if(value.branch_id == item.branch_id)
			    {
			    	value.ticked = true;
			    }
			    else
			    {
			    	value.ticked = false;
			    }
			});
		}
	};
	$scope.addItem = function() {
		var confirmModal = bootbox.dialog({
			  	message: commonFunction.languageReplace(Languages.common_confirm_add_question,[' Tài Khoản']),
			  	title: commonFunction.languageReplace(Languages.common_confirm_add_title,[' Tài Khoản']),
			  	closeButton: true,
			  	animate: true,
			  	className: "confirmModal",
			  	backdrop: true,
			  	headerBackground: "bg-blue",
			  	size: "small",
			  	locale: "vi",
			  	show: true,
			  	buttons: {
			  		cancel : {
				      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
				      	className: "btn-default",
				      	callback: function() {
				        	confirmModal.modal('hide');
				      	}
				    },
				    success: {
			      		label: '<i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;'+Languages.common_button_add,
				      	className: "btn-primary",
				      	callback: function() {
			      		$("#addReLoading").show();
			        	var request = {
							user_account : $scope.model.request.user_account,
							auth_id : $scope.model.request.txtAuthId,
							user_email : $scope.model.request.user_email,
							user_avatar : $("#thumbnail").val(),
							user_fullname :  $scope.model.request.user_fullname,
							user_sex : $scope.model.request.user_sex,
							user_password : $scope.model.request.user_password,
							user_password_confirm : $scope.model.request.user_password_confirm,
							user_login_count : 0,
							branch_id :  $scope.model.request.branch_id,
							status : $scope.model.request.status
					    };

					    $http.post($scope.model.datainit.appURL + "/SystemApi/AddUser", JSON.stringify(request))
					    	.then(
				               function (response) {
				                  	var data = response.data;

							    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
						            {
						                toastr.error(data.messages, Languages.common_error_title);

						                if(data.auth != "" && data.auth != null  && data.auth != undefined)
						                {
						                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
						                }
						            }
							    	else if(data.error != "" && data.error != null  && data.error != undefined)
							    	{
							    		toastr.error(data.error, Languages.common_error_title);
							    	}
							    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
							    	{
							    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
							    		warning = warning.replace('["', '');
							    		warning = warning.replace('"]', '');
							    		toastr.warning(warning, Languages.common_warning_title);
							    	}
							    	else if(data.success != "" && data.success != null  && data.success != undefined)
							    	{
							    		toastr.success(data.success, Languages.common_success_title);
							    		$rootScope.$emit("callLoadList", {});
							    		$scope.formReset();
							    	}
							    	else
							    	{
							    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
							    		{
							    			toastr.error(data.systemerror, Languages.common_error_title);
							    			window.location.replace("./"+data.errorCode);
							    		}
							    		else
							    		{
							    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
							    		}
							    	}
							    	$("#addReLoading").hide();
					    		},
					   			function (response) {
					         
						            var data = response.data;
									toastr.error(Languages.common_error_exception, Languages.common_error_title);
									$("#addReLoading").hide();
					    		}
					    	);
			      	}
			    },
		  	}
		});
	};

	$scope.updateItem = function() {
		// alert($scope.model.request.branch_id);
		
		var confirmModal = bootbox.dialog({
		  	message: commonFunction.languageReplace(Languages.common_confirm_update_question,[' Tài Khoản']),
		  	title: commonFunction.languageReplace(Languages.common_confirm_update_title,[' Tài Khoản']),
		  	closeButton: true,
		  	animate: true,
		  	className: "confirmModal",
		  	backdrop: true,
		  	headerBackground: "bg-yellow",
		  	size: "small",
		  	locale: "vi",
		  	show: true,
		  	buttons: {
		  		cancel : {
			      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
			      	className: "btn-default",
			      	callback: function() {
			        	confirmModal.modal('hide');
			      	}
			    },
			    success: {
		      		label: '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;'+Languages.common_button_update,
			      	className: "btn-warning",
			      	callback: function() {

			      		$("#addReLoading").show();
			      		
			        	var request = {
			        		user_id : $scope.model.request.user_id,
							user_account : $scope.model.request.user_account,
							auth_id : $scope.model.request.auth_id,
							user_email : $scope.model.request.user_email,
							user_avatar : $("#thumbnail").val(),
							user_fullname :  $scope.model.request.user_fullname,
							user_sex : $scope.model.request.user_sex,
							user_password : $scope.model.request.user_password,
							user_password_confirm : $scope.model.request.user_password_confirm,
							user_login_count : 0,
							branch_id :  $scope.model.request.branch_id,
							status : $scope.model.request.status
					    };

					    $http.post($scope.model.datainit.appURL + "/SystemApi/UpdateUser", JSON.stringify(request))
					    	.then(
               					function (response) {
                  					var data = response.data;

							    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
						            {
						                toastr.error(data.messages, Languages.common_error_title);

						                if(data.auth != "" && data.auth != null  && data.auth != undefined)
						                {
						                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
						                }
						            }
							    	else if(data.error != "" && data.error != null  && data.error != undefined)
							    	{
							    		toastr.error(data.error, Languages.common_error_title);
							    	}
							    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
							    	{
							    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
							    		warning = warning.replace('["', '');
							    		warning = warning.replace('"]', '');
							    		toastr.warning(warning, Languages.common_warning_title);
							    	}
							    	else if(data.success != "" && data.success != null  && data.success != undefined)
							    	{
							    		toastr.success(data.success, Languages.common_success_title);
							    		$rootScope.$emit("callLoadList", {});
							    		$scope.formReset();
							    	}
							    	else
							    	{
							    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
							    		{
							    			toastr.error(data.systemerror, Languages.common_error_title);
							    			window.location.replace("./"+data.errorCode);
							    		}
							    		else
							    		{
							    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
							    		}
							    	}
							    	$("#addReLoading").hide();
							    },
					   			function (response) {
					          
						            var data = response.data;
									toastr.error(Languages.common_error_exception, Languages.common_error_title);
									$("#addReLoading").hide();
							    }
							);
			      	}
			    }
		  	}
		});
	};
	// GetBranchList
	$scope.GetBranchList = function() {

		$("#loader").show();

		var request = {
	    };

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetBranchList", JSON.stringify(request))
	    .then(
               function (response) {
                  var data = response.data;

		    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
	            {
	                toastr.error(data.messages, Languages.common_error_title);

	                if(data.auth != "" && data.auth != null  && data.auth != undefined)
	                {
	                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
	                }
	            }
		    	else if(data.error != "" && data.error != null  && data.error != undefined)
		    	{
		    		toastr.error(data.error, Languages.common_error_title);
		    		$scope.model.datainit.slbBranch = [];
		    	}
		    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
		    	{
		    		toastr.warning(data.warning, Languages.common_warning_title);
		    		$scope.model.datainit.slbBranch = [];
		    	}
		    	else if(data.success != "" && data.success != null  && data.success != undefined)
		    	{
		    		$scope.model.datainit.slbBranch = data.success;
		    	}
		    	else
		    	{
		    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
		    		{
		    			toastr.error(data.systemerror, Languages.common_error_title);
		    			window.location.replace("./"+data.errorCode);
		    		}
		    		else
		    		{
		    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
		    		}
		    		$scope.model.datainit.slbBranch = [];
		    	}
		    	$("#loader").hide();
	    	},
           	function (response) {
          		var data = response.data;
				toastr.error(Languages.common_error_exception, Languages.common_error_title);
				$scope.model.datainit.slbBranch = [];
				$("#loader").hide();
		    });
	};
});

// create angular controller
userApp.controller('listUsersController', function(UserModels, $scope, $rootScope, $http, $filter, $window, $location, $log, $uibModal, toastr,DTOptionsBuilder, DTColumnDefBuilder,DTColumnBuilder) {

	$scope.model = UserModels;

	$rootScope.$on("callLoadList", function(){
       $scope.loadList();
    });

    $rootScope.$on("callRemoveItem", function(event, id){
       $scope.removeItem(id);
    });

	$scope.pageInit = function() {

		$scope.model.datainit.appURL = $('meta[name="app_url"]').attr('content');

		$("#listReLoading").show();

		$scope.loadList();

		$scope.dtOptions = DTOptionsBuilder.newOptions()
			.withOption('order', [2, 'asc'])
        	.withPaginationType('full_numbers')
        	.withDisplayLength(10)
	        .withBootstrap()
	        .withBootstrapOptions({
	            pagination: {
	                classes: {
	                    ul: 'pagination pagination-md'
	                }
	            }
	        })
	        .withLanguage({
	            "sEmptyTable":     "Không có dữ liệu.",
	            "sInfo":           "Hiển Thị Từ _START_ Đến _END_ Của _TOTAL_ Kết Quả",
	            "sInfoEmpty":      "Hiển Thị Từ 0 Đến 0 Của 0 Kết Quả",
	            "sInfoFiltered":   "(Đã Lọc Từ Tổng _MAX_ Kết Quả)",
	            "sInfoPostFix":    "",
	            "sInfoThousands":  ",",
	            "sLengthMenu":     "Hiển Thị&nbsp;&nbsp;&nbsp;_MENU_&nbsp;&nbsp;&nbsp;Kết Quả / 1 Trang",
	            "sLoadingRecords": "Đang Tải Dữ Liệu...",
	            "sProcessing":     "Đang Xử Lý Dữ Liệu...",
	            "sSearch":         "Từ Khóa: ",
	            "sZeroRecords":    "Không tìm thấy dữ liệu trùng khớp.",
	            "oPaginate": {
	                "sFirst":    "Đầu",
	                "sLast":     "Cuối",
	                "sNext":     "Tiếp",
	                "sPrevious": "Trước"
	            },
	            "oAria": {
	                "sSortAscending":  ": activate to sort column ascending",
	                "sSortDescending": ": activate to sort column descending"
	            }
	        })
	        .withOption('responsive', true);

	    $scope.dtColumnDefs = [
	        DTColumnDefBuilder.newColumnDef(0).notSortable(),
	    ];
	};
	
	$scope.loadList = function() {

		$("#listReLoading").show();

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetListUsers")
	    	.then(
               	function (response) {
                  	var data = response.data;

			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    		$scope.model.datainit.dtgListUser = [];
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
						var warning = String(data.warning[Object.keys(data.warning)[0]]);
			    		warning = warning.replace('["', '').replace('"]', '');
			    		toastr.warning(warning, Languages.common_warning_title);
			    		$scope.model.datainit.dtgListUser = [];
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		$scope.model.datainit.dtgListUser = data.success;
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    		$scope.model.datainit.dtgListUser = [];
			    	}
			    	$("#listReLoading").hide();
			    },
               	function (response) {
                  	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$scope.model.datainit.dtgListUser = [];
					$("#listReLoading").hide();
    			}
    		);
	};

	$scope.changeStatus = function(id,status) {

		if(id == "" || id == null || id == undefined)
		{
			toastr.error("Vui Lòng Kiểm Tra Thông Tin Trước Khi Cập Nhật", "Cảnh Báo!");
			return false;
		}
			
		var confirmChangeStatusModal = bootbox.dialog({
		  	message: "Bạn Có Muốn Cập Nhật Trạng Thái Người Dùng?",
		  	title: "Xác Nhận Cập Nhật Trạng Thái Người Dùng",
		  	closeButton: true,
		  	animate: true,
		  	className: "confirmModal",
		  	backdrop: true,
		  	headerBackground: "bg-yellow",
		  	size: "small",
		  	locale: "vi",
		  	show: true,
		  	buttons: {
		  		cancel : {
			      	label: Languages.common_button_cancel,
			      	className: "btn-default",
			      	callback: function() {
			        	confirmAddModal.modal('hide');
			      	}
			    },
			    success: {
		      		label: Languages.common_button_update_status,
			      	className: "btn-warning",
			      	callback: function() {

			      		$("#listReLoading").show();

						//Khởi Tạo Dữ Liệu
						var request = {
					        user_id : id,
					        status : status 
					    };

					    $http.post($scope.model.datainit.appURL + "/SystemApi/ChangeStatusUser", JSON.stringify(request))
					    	.then(
               					function (response) {
                  					var data = response.data;

							    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
						            {
						                toastr.error(data.messages, Languages.common_error_title);

						                if(data.auth != "" && data.auth != null  && data.auth != undefined)
						                {
						                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
						                }
						            }
							    	else if(data.error != "" && data.error != null  && data.error != undefined)
							    	{
							    		toastr.error(data.error, Languages.common_error_title);
							    	}
							    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
							    	{
										var warning = String(data.warning[Object.keys(data.warning)[0]]);
							    		warning = warning.replace('["', '');
							    		warning = warning.replace('"]', '');
							    		toastr.warning(warning, Languages.common_warning_title);
							    	}
							    	else if(data.success != "" && data.success != null  && data.success != undefined)
							    	{
							    		toastr.success(data.success, Languages.common_success_title);

										$scope.loadList();
							    	}
							    	else
							    	{
							    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
							    		{
							    			toastr.error(data.systemerror, Languages.common_error_title);
							    			window.location.replace("./"+data.errorCode);
							    		}
							    		else
							    		{
							    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
							    		}
							    	}
						    	 	$("#listReLoading").hide();
							    },
					    		function (response) {
				              
				                  	var data = response.data;
									toastr.error(Languages.common_error_exception, Languages.common_error_title);
								 	$("#listReLoading").hide();
					    		}
					    	);
			      	}
			    }
		  	}
		});
	};

	$scope.removeItem = function(id) {

		if(id == "" || id == null || id == undefined)
		{
			toastr.error("Vui Lòng Kiểm Tra Thông Tin Trước Khi Xóa", "Cảnh Báo!");
			return false;
		}
			
		var confirmModal = bootbox.dialog({
		  	message: "Bạn Có Muốn Xóa Người Dùng?",
		  	title: "Xác Nhận Xóa",
		  	closeButton: true,
		  	animate: true,
		  	className: "confirmModal",
		  	backdrop: true,
		  	headerBackground: "bg-red",
		  	size: "small",
		  	locale: "vi",
		  	show: true,
		  	buttons: {
		  		cancel : {
			      	label: '<i class="material-icons">cancel</i>&nbsp;'+Languages.common_button_cancel,
			      	className: "btn-default",
			      	callback: function() {
			        	confirmModal.modal('hide');
			      	}
			    },
			    success: {
		      		label: '<i class="material-icons">delete</i>&nbsp;'+Languages.common_button_remove,
			      	className: "btn-danger",
			      	callback: function() {

			      		$("#listReLoading").show();

						//Khởi Tạo Dữ Liệu
						var request = {
					        user_id : id,
					    };

					    $http.post($scope.model.datainit.appURL + "/SystemApi/RemoveUser", JSON.stringify(request))
						    .then(
	           					function (response) {
                  					var data = response.data;

							    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
						            {
						                toastr.error(data.messages, Languages.common_error_title);

						                if(data.auth != "" && data.auth != null  && data.auth != undefined)
						                {
						                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
						                }
						            }
							    	else if(data.error != "" && data.error != null  && data.error != undefined)
							    	{
							    		toastr.error(data.error, Languages.common_error_title);
							    	}
							    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
							    	{
							    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
							    		warning = warning.replace('["', '');
							    		warning = warning.replace('"]', '');
							    		toastr.warning(warning, Languages.common_warning_title);
							    		
							    		$scope.loadList();
							    	}
							    	else if(data.success != "" && data.success != null  && data.success != undefined)
							    	{
							    		toastr.success(data.success, Languages.common_success_title);

							    		$rootScope.$emit("callFormReset", {});
										$scope.loadList();
							    	}
							    	else
							    	{
							    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
							    		{
							    			toastr.error(data.systemerror, Languages.common_error_title);
							    			window.location.replace("./"+data.errorCode);
							    		}
							    		else
							    		{
							    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
							    		}
							    	}
							    	$("#listReLoading").hide();
							    },
					    		function (response) {
				       
				                  	var data = response.data;
									toastr.error(Languages.common_error_exception, Languages.common_error_title);
									$("#listReLoading").hide();
					    		}
					    	);
			      	}
			    },
		  	}
		});
	};

	$scope.openUpdateItem = function(item) {

		if(item == "" || item == null || item == undefined)
		{
			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			return false;
		}
		// alert(item.branch_id);
		$scope.model.request = {
			user_id : item.user_id,
			user_account : item.user_account,
			slbGroup : [{
				"auth_id" : item.auth_id,
			}],
			auth_id : item.auth_id,
			user_email : item.user_email,
			user_avatar : item.user_avatar,
			user_fullname :  item.user_fullname,
			user_sex : item.user_sex,
			branch_id:item.branch_id,
			user_password : "",
			user_password_confirm : "",
			user_login_count : 0,
			status : item.status
		};

		angular.forEach($scope.model.datainit.slbListGroup, function(value, key) {
		    if(value.auth_id == item.auth_id)
		    {
		    	value.ticked = true;
		    }
		    else
		    {
		    	value.ticked = false;
		    }
		});

		angular.forEach($scope.model.datainit.slbBranch, function(value, key) {
		    if(value.branch_id == item.branch_id)
		    {
		    	value.ticked = true;
		    }
		    else
		    {
		    	value.ticked = false;
		    }
		});
		$('#txtAuthId').val(item.auth_id); 
   		$('#txtAuthId').trigger('input');

   		$('#txtUserId').val(item.user_id); 
   		$('#txtUserId').trigger('input');


   		$('#txtUserId').trigger('input');

   		$scope.model.datainit.useraccountupdate = true;
	};

});