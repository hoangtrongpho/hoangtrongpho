/*!
 *
 *	Develop by Vinh Nguyễn - Ho Chi Minh City - 2017
 * 	Copyright Â© 2016-2017 Skyfire Team. All rights reserved.
 *	Design by Skyfire Team 2.3.3
 *
 *	Phone: 0975 578 276
 *	Skype: enjoyvinh
 *
 */

"use strict";

// app.js
// create angular app
var loginApp = angular.module('loginApp', ['ngRoute','ui.checkbox']);

loginApp.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});

// create angular controller
loginApp.controller('loginController', function($scope, $http, $filter, $window, $location) {

    $scope.datainit = {
        times : 3000,
        appURL : "http://localhost:8000"
    };

	$scope.loginModel= {
		userName : "",
		passWord : "",
		reMember : "0",
        userAuth : "",
        checkLogin : false
    };

    $scope.pageInit = function(errorMessage) {

        console.log(errorMessage);
        if(errorMessage != "" && errorMessage != null  && errorMessage != undefined)
        {
            alertToggle("warning",errorMessage);
            $("#btnLogin").button('reset');
        }

        $scope.datainit.appURL = $('meta[name="app_url"]').attr('content');
    };

	// function to submit the form after all validation has occurred            
	$scope.submitForm = function(isValid) {

		$("#btnLogin").button('loading');

		// check to make sure the form is completely valid
		if (isValid) {

			var request = {
            user_account : $scope.loginModel.userName,
            user_password : $scope.loginModel.passWord,
            remembered : $scope.loginModel.reMember
         };
     
         var config = {
             headers : {
                 'Content-Type': 'application/json;charset=utf-8;',
         		'X-CSRF-TOKEN': $('meta[name="system-secret-code"]').attr('system_content')
             }
         };

         $http.post($scope.datainit.appURL + "/SystemApi/checkLogin", JSON.stringify(request), config)
            .then(
               function (response) {
                debugger;
                  var data = response.data;

                  if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
                  {
                     alertToggle("error",data.messages);
                     $("#btnLogin").button('reset');
                  }
                  else if(data.error != "" && data.error != null  && data.error != undefined)
                  {
                     alertToggle("error",data.error);
                     $("#btnLogin").button('reset');
                  }
                  else if(data.warning != "" && data.warning != null  && data.warning != undefined)
                  {
                     alertToggle("warning",data.warning);
                     $("#btnLogin").button('reset');
                  }
                  else if(data.success != "" && data.success != null  && data.success != undefined)
                  {
                     if (sessionStorage.getItem("currentUrl") === null) 
                     {
                        $window.location.href = $scope.datainit.appURL+'/System/Dashboard2';
                     }
                     else
                     {
                        $window.location.href = $scope.datainit.appURL + sessionStorage.currentUrl;
                     }
                  }
                  else
                  {
                     alertToggle("error",data.systemerror);
                     $("#btnLogin").button('reset');
                  }
               },
               function (response) {
                  var data = response.data;

                  alertToggle("error",data.systemerror);
                  $("#btnLogin").button('reset');
               }
            );
		} 
		else 
		{
         $("#btnLogin").button('reset');
		}

	};

	var alertToggle = function(type, data) {
		if(type == "error")
		{
			$("#alertMessage").html(data);
    		$('.alert-danger').slideToggle("slow");
			setTimeout(function(){ $('.alert-danger').slideToggle("slow"); }, $scope.datainit.times);
		}
		else if(type == "warning")
		{
			$("#warningMessage").html(data);
    		$('.alert-warning').slideToggle("slow");
			setTimeout(function(){ $('.alert-warning').slideToggle("slow"); }, $scope.datainit.times);
		}
		else
		{
			$("#alertMessage").html(data);
    		$('.alert-danger').slideToggle("slow");
			setTimeout(function(){ $('.alert-danger').slideToggle("slow"); }, $scope.datainit.times);
		}
	}
});