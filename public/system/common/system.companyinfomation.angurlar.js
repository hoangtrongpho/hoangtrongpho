/*!
 *
 *	Develop by Vinh Nguyễn - Ho Chi Minh City - 2017
 * 	Copyright Â© 2016-2017 Skyfire Team. All rights reserved.
 *	Design by Skyfire Team 2.3.3
 *
 *	Phone: 0975 578 276
 *	Skype: enjoyvinh
 *
 */

"use strict";

// app.js
// create angular app
var createNewsApp = angular.module('createNewsApp', ['commonApp', 'daterangepicker','bootstrap-switch','isteven-multi-select','ui.tinymce', 'ngAnimate', 'ngSanitize',
	'icheckAngular']);

createNewsApp.factory('NewsModels', function() {

	var model={
		datainit : {
			slbListCategories : [],
			slbListTags : [],
			slbListProvinces : [],
			pageStatus : "add",
			ratingOptions : [
		        {
		          name: '1 Sao',
		          value: 1
		        }, 
		        {
		          name: '2 Sao',
		          value: 2
		        }, 
		        {
		          name: '3 Sao',
		          value: 3
		        }, 
		        {
		          name: '4 Sao',
		          value: 4
		        }, 
		        {
		          name: '5 Sao',
		          value: 5
		        }, 
		    ],
		    appURL : "http://localhost:8000"
		},
	};

	model.request = {
		dtpPublishes: {
			startDate: moment(), 
			endDate: moment().endOf('month')
		},
		txtCateParent : "",
		slbCateParent : "",
		txtProvince : "",
		slbProvince : [],
		sblStatus : 1,
		txtAlias : "Skyfire Solution",
		txtImages : "./public/images/default-image.png",
		txtTitle : "",
		txtDescription : "",
		txaContent : "",
		txtContent : "",
		radFormatPage : 1,
		slbTags : [],
		txtNewsId : "",
		slbRating : 5,
		txtDownload : "",
		txtTitleSEO : "",
		txtKeywordSEO : "",
		txtDescriptionSEO : ""
	};

	model.datePickerOpts = {
		showDropdowns : true,
		singleDatePicker : false,
		timePicker : false,
		timePickerSeconds : false,
		timePicker24Hour : true,
		alwaysShowCalendars : false,
		showWeekNumbers : true,
		autoApply : true,
		opens: "left",
		drops: "down",
		clearLabel: Languages.common_button_cancel,
		cancelClass : 'btn-default',
		autoUpdateInput : true,
	    parentEl: '.cal-block .form-group .oneway',
        locale: {
            buttonClasses : 'btn btn-sm',
	        applyClass : 'btn-success',
            applyLabel: Languages.common_button_select_item,
            fromLabel: Languages.common_label_from,
            format: "DD/MM/YYYY",
            toLabel: Languages.common_label_to,
            weekLabel: Languages.datepicker_week_title,
            customRangeLabel: Languages.common_button_select_date_time,
            daysOfWeek: [
            				Languages.datepicker_week_monday,
            				Languages.datepicker_week_tuesday,
            				Languages.datepicker_week_wednesday,
            				Languages.datepicker_week_thursday,
            				Languages.datepicker_week_friday,
            				Languages.datepicker_week_saturday,
            				Languages.datepicker_week_sunday
            			],
            monthNames: [
	            			Languages.datepicker_month_january, 
	            			Languages.datepicker_month_february,
	            			Languages.datepicker_month_march,
	            			Languages.datepicker_month_april,
	            			Languages.datepicker_month_may,
	            			Languages.datepicker_month_june,
	            			Languages.datepicker_month_july,
	            			Languages.datepicker_month_august,
	            			Languages.datepicker_month_september,
	            			Languages.datepicker_month_october,	
	            			Languages.datepicker_month_november,
	            			Languages.datepicker_month_december
            			],
        },
        ranges: {
        	'Chỉ Hôm Nay': [moment(), moment()],
        	'Ngày Mai' : [moment(), moment().add(1, 'day')],
        	'Trong 1 Tuần': [moment(), moment().add(1, 'week').endOf('month')],
        	'Trong 2 Tuần': [moment(), moment().add(2, 'week').endOf('month')],
        	'Trong 1 Tháng': [moment(), moment().add(1, 'month').endOf('month')],
        	'Trong 3 Tháng': [moment(), moment().add(3, 'month').endOf('month')],
        	'Trong 6 Tháng': [moment(), moment().add(6, 'month').endOf('month')],
        	'Trong 1 Năm': [moment(), moment().add(1, 'year').endOf('month')],
        	'Trong 3 Năm': [moment(), moment().add(3, 'year').endOf('month')]
        },      
    };

	model.localLang = {
	    selectAll       : Languages.common_button_select_all,
	    selectNone      : Languages.common_button_deselected_all,
	    reset           : Languages.common_button_cancel,
	    search          : Languages.common_input_label_search,
	    nothingSelected : Languages.common_input_label_select
	}

	model.tinymceOptions = {
		height: "600px",
		path_absolute : "/",
	    selector: "textarea",
	    theme: 'modern',
	    readonly: false,
	    plugins: [
		    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
		    'searchreplace wordcount visualblocks visualchars code fullscreen',
		    'insertdatetime media nonbreaking save table contextmenu directionality',
		    'emoticons template paste textcolor colorpicker textpattern imagetools'
	    ],
	    toolbar1: "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
		toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | insertdatetime preview | forecolor backcolor",
		toolbar3: "table | hr removeformat | subscript superscript | charmap emoticons | print fullscreen | ltr rtl | spellchecker | visualchars visualblocks nonbreaking template pagebreak restoredraft",
		menubar: true,
		toolbar_items_size: 'small',
	    image_advtab: true,
	    resize : 'both',	
	    content_css: [
		    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
		    '//www.tinymce.com/css/codepen.min.css'
	    ],
	    relative_urls: true,
	    allow_html_in_named_anchor: true, //This option allows you to specify whether the editor should parse and keep html within named anchor tags.
	    allow_conditional_comments: false, //This option allows you to specify whether the editor should parse and keep conditional comments.
	    convert_fonts_to_spans : false, //If you set this option to true, TinyMCE will convert all font elements to span elements and generate span elements instead of font elements. 
	    element_format : 'html', //This option controls whether elements are output in the HTML or XHTML mode.
	    encoding: 'xml', //This option allows you to get XML escaped content out of TinyMCE. By setting this option to xml, posted content will be converted to an XML string escaping characters such as <, >, ", and & to <, >, ", and &.
	    entity_encoding : "raw", //named	Characters will be converted into named entities based on the entities option. For example, a non-breaking space could be encoded as &nbsp;. This value is default.
								 //numeric	Characters will be converted into numeric entities. For example, a non-breaking space would be encoded as &#160;.
								 //raw	All characters will be stored in non-entity form except these XML default entities: & < > "
		fix_list_elements : true,
		force_p_newlines : true,
		schema: 'html5', //The schema option enables you to switch between the HTML4 and HTML5 schema.
	    visual: true,
	    font_formats: 'Andale Mono=andale mono,times;'+ 'Arial=arial,helvetica,sans-serif;'+ 'Arial Black=arial black,avant garde;'+ 'Book Antiqua=book antiqua,palatino;'+ 'Comic Sans MS=comic sans ms,sans-serif;'+ 'Courier New=courier new,courier;'+ 'Georgia=georgia,palatino;'+ 'Helvetica=helvetica;'+ 'Impact=impact,chicago;'+ 'Symbol=symbol;'+ 'Tahoma=tahoma,arial,helvetica,sans-serif;'+ 'Terminal=terminal,monaco;'+ 'Times New Roman=times new roman,times;'+ 'Trebuchet MS=trebuchet ms,geneva;'+ 'Verdana=verdana,geneva;'+ 'Webdings=webdings;'+ 'Wingdings=wingdings,zapf dingbats',
	    fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt',
	    browser_spellcheck: true, //One of the several spell checking options developers have available is to use the browser's native spell checker. browser_spellcheck handles this behavior.
	    file_browser_callback : function(field_name, url, type, win) {
	        var x = 1000;
	        var y = 700;
	        var cmsURL = '/laravel-filemanager?field_name=' + field_name;

	        if (type == 'image') 
	        {
	        	cmsURL = cmsURL + "&type=Images";
	        } 
	        else
	        {
	        	cmsURL = cmsURL + "&type=Files";
	        }
	        tinyMCE.activeEditor.windowManager.open({
	            file : cmsURL,
	            title : 'Chọn Ảnh Từ Thư viện',
	            width : x * 0.8,
	            height : y * 0.8,
	            resizable : "no",
	            close_previous : "yes"
            });
        }
	};

	return model;
});

// create angular controller
createNewsApp.controller('createNewsController', function(NewsModels, commonFunction, $scope, $rootScope, $http, $filter, $window, $location, $uibModal, toastr) {

	$scope.model = NewsModels;

	$scope.openDialog = function() {

		$uibModal.open({
			animation: true,
			ariaLabelledBy: 'modal-title',
			ariaDescribedBy: 'modal-body',
			templateUrl: 'http://localhost:8000/MultimediaManager?type=Images',
			size: 'lg',
			controller: function($scope) {
				// $scope.name = 'bottom';  
			}
	    });
	};

	$scope.resetChoiceThumbnail = function() {
    	$("#holder").attr("src",$scope.model.datainit.appURL + "/public/images/default-image.png");
		$("#thumbnail").val($scope.model.datainit.appURL + "/public/images/default-image.png");
    	$scope.model.request.user_avatar = $scope.model.datainit.appURL + "/public/images/default-image.png";
    };

	$scope.pageInit = function() {

		$scope.model.datainit.appURL = $('meta[name="app_url"]').attr('content');

		$("#addReLoading").show();

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetListNewsCategoriesForCreateNews")
	    .then(
               function (response) {
                  var data = response.data;

	    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
            {
                toastr.error(data.messages, Languages.common_error_title);

                if(data.auth != "" && data.auth != null  && data.auth != undefined)
                {
                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
                }
            }
	    	else if(data.error != "" && data.error != null  && data.error != undefined)
	    	{
	    		toastr.error(data.error, Languages.common_error_title);
	    		$scope.model.datainit.slbListCategories = [];
	    	}
	    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
	    	{
	    		toastr.warning(data.warning, Languages.common_warning_title);
	    		$scope.model.datainit.slbListCategories = [];
	    	}
	    	else if(data.success != "" && data.success != null  && data.success != undefined)
	    	{
	    		$scope.model.datainit.slbListCategories = data.success;
	    	}
	    	else
	    	{
	    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
	    		{
	    			toastr.error(data.systemerror, Languages.common_error_title);
	    			window.location.replace("./"+data.errorCode);
	    		}
	    		else
	    		{
	    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
	    		}
	    		$scope.model.datainit.slbListCategories = [];
	    	}
	    },
               function (response) {
                  debugger;
                  var data = response.data;
			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			$scope.model.datainit.slbListCategories = [];
	    });

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetListNewsTags")
	    .then(
               function (response) {
                  var data = response.data;

	    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
            {
                toastr.error(data.messages, Languages.common_error_title);

                if(data.auth != "" && data.auth != null  && data.auth != undefined)
                {
                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
                }
            }
	    	else if(data.error != "" && data.error != null  && data.error != undefined)
	    	{
	    		toastr.error(data.error, Languages.common_error_title);
	    		$scope.model.datainit.slbListTags = [];
	    	}
	    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
	    	{
	    		toastr.warning(data.warning, Languages.common_warning_title);
	    		$scope.model.datainit.slbListTags = [];
	    	}
	    	else if(data.success != "" && data.success != null  && data.success != undefined)
	    	{
	    		$scope.model.datainit.slbListTags = data.success;
	    		$scope.model.datainit.slbListTags = [];
	    	}
	    	else
	    	{
	    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
	    		{
	    			toastr.error(data.systemerror, Languages.common_error_title);
	    			window.location.replace("./"+data.errorCode);
	    		}
	    		else
	    		{
	    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
	    		}
	    		$scope.model.datainit.slbListTags = [];
	    	}
	    },
               function (response) {
                  debugger;
                  var data = response.data;
			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			$scope.model.datainit.slbListTags = [];
	    });

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetListProvinces")
	    .then(
               function (response) {
                  var data = response.data;

	    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
            {
                toastr.error(data.messages, Languages.common_error_title);

                if(data.auth != "" && data.auth != null  && data.auth != undefined)
                {
                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
                }
            }
	    	else if(data.error != "" && data.error != null  && data.error != undefined)
	    	{
	    		$scope.model.datainit.slbListProvinces = [];
	    		toastr.error(data.error, Languages.common_error_title);
	    	}
	    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
	    	{
	    		$scope.model.datainit.slbListProvinces = [];
	    		toastr.warning(data.warning, Languages.common_warning_title);
	    	}
	    	else if(data.success != "" && data.success != null  && data.success != undefined)
	    	{
	    		$scope.model.datainit.slbListProvinces = data.success;
	    	}
	    	else
	    	{
	    		$scope.model.datainit.slbListProvinces = [];
	    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
	    		{
	    			toastr.error(data.systemerror, Languages.common_error_title);
	    			window.location.replace("./"+data.errorCode);
	    		}
	    		else
	    		{
	    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
	    		}
	    	}
	    },
               function (response) {
                  debugger;
                  var data = response.data;
	    	$scope.model.datainit.slbListProvinces = [];
			toastr.error(Languages.common_error_exception, Languages.common_error_title);
	    });

	    if($("#txtSlug").val() !== "")
	    {
	    	var request = {
		        news_slug : $("#txtSlug").val(),
		    };

		    $http.post($scope.model.datainit.appURL + "/SystemApi/GetNewsDetails", JSON.stringify(request))
		    .then(
               function (response) {
                  var data = response.data;

		    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
	            {
	                toastr.error(data.messages, Languages.common_error_title);

	                if(data.auth != "" && data.auth != null  && data.auth != undefined)
	                {
	                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
	                }
	            }
		    	else if(data.error != "" && data.error != null  && data.error != undefined)
		    	{
		    		toastr.error(data.error, Languages.common_error_title);
		    	}
		    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
		    	{
		    		toastr.warning(data.warning, Languages.common_warning_title);
		    	}
		    	else if(data.success != "" && data.success != null  && data.success != undefined)
		    	{
		    		console.log(data.success);

		    		$scope.model.request = {
						dtpPublishes: {
							startDate: data.success.news_release_date, 
							endDate: data.success.news_expiration_date,
						},
						slbCateParent : { "news_cate_id" : data.success.news_cate_id },
						sblStatus : data.success.status,
						txtAlias : data.success.news_author_aliases,
						txtImages : data.success.news_thumbnail,
						txtTitle : data.success.news_title,
						txtDescription : data.success.news_description,
						txaContent : "",
						txtContent : "",
						radFormatPage : data.success.news_format_page,
						txtNewsId : data.success.news_id,
						slbRating : data.success.news_ratings,
					};

					if(data.success.news_format_page == 1)
					{
						$scope.model.request.txaContent = data.success.news_content;
						$scope.model.request.txtContent = "";
					}
					else if(data.success.news_format_page == 2)
					{
						$scope.model.request.txtContent = data.success.news_content;
						$scope.model.request.txaContent = "";
					}

					$scope.model.request.slbProvince = data.listProvinces;
					angular.forEach(data.listProvinces, function(value, key) {
						angular.forEach($scope.model.datainit.slbListProvinces, function(value2, key2) {
						    if(value.province_id == value2.province_id)
						    {
						    	value2.ticked = true;
						    }
						});
					});
					$scope.model.request.slbTags = data.listTags;
					angular.forEach(data.listTags, function(value, key) {
						angular.forEach($scope.model.datainit.slbListTags, function(value2, key2) {
						    if(value.tag_id == value2.tag_id)
						    {
						    	value2.ticked = true;
						    }
						});
					});

					angular.forEach($scope.model.datainit.slbListCategories, function(value, key) {
					    if(value.news_cate_id == data.success.news_cate_id)
					    {
					    	value.ticked = true;
					    }
					    else
					    {
					    	value.ticked = false;
					    }
					});

					$('#txtCateParent').val(data.success.news_cate_id); 
   					$('#txtCateParent').trigger('input');

					$scope.model.datainit.pageStatus = "update";
		    	}
		    	else
		    	{
		    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
		    		{
		    			toastr.error(data.systemerror, Languages.common_error_title);
		    			window.location.replace("./"+data.errorCode);
		    		}
		    		else
		    		{
		    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
		    		}
		    	}

		    })
		    .error(function (data, status, header, config) {
				toastr.error(Languages.common_error_exception, Languages.common_error_title);
		    });
	    }
	};


    $scope.removeItem = function(id) {

    	if(id == "" || id == null || id == undefined)
		{
			toastr.error("Vui Lòng Kiểm Tra Thông Tin Trước Khi Xóa", "Cảnh Báo!");
			return false;
		}
    	
		var confirmModal = bootbox.dialog({
			message: commonFunction.languageReplace(Languages.common_confirm_delete_question,[' Bản Tin']),
		  	title: commonFunction.languageReplace(Languages.common_confirm_delete_title,[' Bản Tin']),
		  	closeButton: true,
		  	animate: true,
		  	className: "confirmModal",
		  	backdrop: true,
		  	headerBackground: "bg-red",
		  	size: "small",
		  	locale: "vi",
		  	show: true,
		  	buttons: {
		  		cancel : {
			      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
			      	className: "btn-default",
			      	callback: function() {
			        	confirmModal.modal('hide');
			      	}
			    },
			    success: {
		      		label: '<i class="fa fa-trash" aria-hidden="true"></i>&nbsp;'+Languages.common_button_remove,
			      	className: "btn-danger",
			      	callback: function() {

			      		$("#addReLoading").show();

			        	var request = {
					        news_id : id,
					    };

					    $http.post($scope.model.datainit.appURL + "/SystemApi/RemoveNews", JSON.stringify(request))
					    .then(
               function (response) {
                  var data = response.data;

					    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
				            {
				                toastr.error(data.messages, Languages.common_error_title);

				                if(data.auth != "" && data.auth != null  && data.auth != undefined)
				                {
				                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
				                }
				            }
					    	else if(data.error != "" && data.error != null  && data.error != undefined)
					    	{
					    		toastr.error(data.error, Languages.common_error_title);
					    	}
					    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
					    	{
					    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
					    		warning = warning.replace('["', '').replace('"]', '');
					    		toastr.warning(warning, Languages.common_warning_title);
					    	}
					    	else if(data.success != "" && data.success != null  && data.success != undefined)
					    	{
					    		toastr.success(data.success, commonFunction.languageReplace(Languages.common_success_title,[' ']));

								bootbox.alert(commonFunction.languageReplace(Languages.common_success_deleted,[' Bản Tin']), function() {
									window.location.replace($scope.model.datainit.appURL + "/System/ListNews");
								});

								setTimeout(function(){ 
									window.location.replace($scope.model.datainit.appURL + "/System/ListNews");
								}, 3000);
					    	}
					    	else
					    	{
					    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
					    		{
					    			toastr.error(data.systemerror, Languages.common_error_title);
					    			window.location.replace("./"+data.errorCode);
					    		}
					    		else
					    		{
					    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
					    		}
					    	}
					    	$("#addReLoading").hide();
					    })
					    .error(function (data, status, header, config) {
							toastr.error(Languages.common_error_exception, Languages.common_error_title);
							$("#addReLoading").hide();
					    });
			      	}
			    },
		  	}
		});
    }
	
    $scope.formReload = function() {
    	location.reload();
    };

	$scope.formReset = function() {

		$("#pagePreLoading").show();
		$("#addReLoading").show();

		$("#holder").attr("src","");
		$("#thumbnail").val("");

		$scope.model.request = {
			dtpPublishes: {
				startDate: moment(), 
				endDate: moment()
			},
			txtCateParent : "",
			slbCateParent : "",
			txtProvince : "",
			slbProvince : [],
			sblStatus : 1,
			txtAlias : "Skyfire Solution",
			txtImages : $scope.model.datainit.appURL + "/public/images/default-image.png",
			txtTitle : "",
			txtDescription : "",
			txaContent : "",
			txtContent : "",
			radFormatPage : 1,
			slbTags : [],
			txtNewsId : "",
			slbRating : 5,
			txtDownload : "",
			txtTitleSEO : "",
			txtKeywordSEO : "",
			txtDescriptionSEO : ""
		};

		angular.forEach($scope.model.datainit.slbListCategories, function(value, key) {
			value.ticked = false;
		});

		$scope.frmAdd.$setPristine();
		$scope.frmAdd.$setUntouched();

		$scope.pageInit();

		$("#addReLoading").hide();

	};


	//Hàm Chức Năng Cho SelectBox
	$scope.slbClear = function(element)
	{

		if(element == "txtProvince")
		{
			angular.forEach($scope.model.datainit.slbListProvinces, function(value, key) {
		    	value.ticked = false;
			});
		}
		else if(element == "txtCateParent")
		{
			angular.forEach($scope.model.datainit.slbListCategories, function(value, key) {
		    	value.ticked = false;
			});
		}
		
	   	$('#'+element).val(""); 
	   	$('#'+element).trigger('input');
	};

	$scope.slbItemClick = function(item,element)
	{
	   	

   		if(element == "txtProvince")
		{
			$('#'+element).val(item.province_id); 
   			$('#'+element).trigger('input');
		}
		else if(element == "txtCateParent")
		{
			$('#'+element).val(item.news_cate_id); 
   			$('#'+element).trigger('input');

			angular.forEach($scope.model.datainit.slbListCategories, function(value, key) {
			    if(value.news_cate_id == item.news_cate_id)
			    {
			    	value.ticked = true;
			    }
			    else
			    {
			    	value.ticked = false;
			    }
			});
		}
   		
	};
	//Hàm Chức Năng Cho SelectBox

	$scope.addItem = function(status) {
		// check to make sure the form is completely valid
		
		// if ($scope.frmAdd.dtpPublishes.$valid && $scope.frmAdd.txtCateParent.$valid && $scope.frmAdd.txtAlias.$valid 
			// && $scope.frmAdd.txtTitle.$valid && $scope.frmAdd.txtDescription.$valid && $scope.frmAdd.txaContent.$valid) {
			// if(angular.isDate($scope.model.request.dtpPublishes._d))
			// {
			// 	alert(angular.isDate($scope.model.request.dtpPublishes));
			// 	return false;
			// }
			
			var confirmModal = bootbox.dialog({
			  	message: commonFunction.languageReplace(Languages.common_confirm_add_question,[' Đại Lý']),
			  	title: commonFunction.languageReplace(Languages.common_confirm_add_title,[' Đại Lý']),
			  	closeButton: true,
			  	animate: true,
			  	className: "confirmModal",
			  	backdrop: true,
			  	headerBackground: "bg-blue",
			  	size: "small",
			  	locale: "vi",
			  	show: true,
			  	buttons: {
			  		cancel : {
				      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
				      	className: "btn-default",
				      	callback: function() {
				        	confirmModal.modal('hide');
				      	}
				    },
				    success: {
			      		label: '<i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;'+Languages.common_button_add,
				      	className: "btn-primary",
				      	callback: function() {

				      		$("#addReLoading").show();

						    var request = {
						        news_title : $scope.model.request.txtTitle,
						        news_description : $scope.model.request.txtDescription,
						        news_content : "",
						        news_author_aliases : $scope.model.request.txtAlias,
						        status : $scope.model.request.sblStatus,
						        news_thumbnail : $scope.model.request.txtImages,
						        news_release_date : $scope.model.request.dtpPublishes.startDate,
						        news_expiration_date : $scope.model.request.dtpPublishes.endDate,
						        news_format_page : $scope.model.request.radFormatPage,
						        visibility : status,
						        news_cate_id : $scope.model.request.txtCateParent,
						        news_ratings : $scope.model.request.slbRating,
						        news_comment_status : 0,
	 					        news_download : $scope.model.request.txtDownload,
						        news_seo_title : $scope.model.request.txtTitleSEO,
						        news_seo_description : $scope.model.request.txtKeywordSEO,
						        news_seo_keywords : $scope.model.request.txtDescriptionSEO,
						        list_tags : $scope.model.request.slbTags,
						        list_provinces : $scope.model.request.slbProvince
						    };

						    if($scope.model.request.radFormatPage == 1)
							{
								request.news_content = $scope.model.request.txaContent;
							}
							else if($scope.model.request.radFormatPage == 2)
							{
								request.news_content = $scope.model.request.txtContent;
							}
							else
							{
								request.news_format_page = 1;
								request.news_content = $scope.model.request.txaContent;
							}

						    $http.post($scope.model.datainit.appURL + "/SystemApi/AddNews", JSON.stringify(request))
						    .then(
               function (response) {
                  var data = response.data;

						    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
					            {
					                toastr.error(data.messages, Languages.common_error_title);

					                if(data.auth != "" && data.auth != null  && data.auth != undefined)
					                {
					                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
					                }
					            }
						    	else if(data.error != "" && data.error != null  && data.error != undefined)
						    	{
						    		toastr.error(data.error, Languages.common_error_title);
						    	}
						    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
						    	{
						    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
						    		warning = warning.replace('["', '').replace('"]', '');
						    		toastr.warning(warning, Languages.common_warning_title);
						    	}
						    	else if(data.success != "" && data.success != null  && data.success != undefined)
						    	{
						    		toastr.success(data.success, commonFunction.languageReplace(Languages.common_success_title,[' Bản Tin']));

						    		bootbox.alert(commonFunction.languageReplace(Languages.common_success_created,[' Bản Tin']), function() {
										window.location.replace($scope.model.datainit.appURL + "/System/ListNews");
									});

									setTimeout(function(){ 
										window.location.replace($scope.model.datainit.appURL + "/System/ListNews");
									}, 3000);
						    	}
						    	else
						    	{
						    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
						    		{
						    			toastr.error(data.systemerror, Languages.common_error_title);
						    			window.location.replace("./"+data.errorCode);
						    		}
						    		else
						    		{
						    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
						    		}
						    	}
						    	$("#addReLoading").hide();
						    })
						    .error(function (data, status, header, config) {
								toastr.error(Languages.common_error_exception, Languages.common_error_title);
								$("#addReLoading").hide();
						    });
				      	}
				    },
			  	}
			});
		// }
	};

	$scope.updateItem = function(status) {
		// check to make sure the form is completely valid
		
		// if ($scope.frmAdd.dtpPublishes.$valid && $scope.frmAdd.txtCateParent.$valid && $scope.frmAdd.txtAlias.$valid 
			// && $scope.frmAdd.txtTitle.$valid && $scope.frmAdd.txtDescription.$valid && $scope.frmAdd.txaContent.$valid) {
			// if(angular.isDate($scope.model.request.dtpPublishes._d))
			// {
			// 	alert(angular.isDate($scope.model.request.dtpPublishes));
			// 	return false;
			// }
			
			var confirmModal = bootbox.dialog({
			  	message: commonFunction.languageReplace(Languages.common_confirm_update_question,[' Bản Tin']),
			  	title: commonFunction.languageReplace(Languages.common_confirm_update_title,[' Bản Tin']),
			  	closeButton: true,
			  	animate: true,
			  	className: "confirmModal",
			  	backdrop: true,
			  	headerBackground: "bg-yellow",
			  	size: "small",
			  	locale: "vi",
			  	show: true,
			  	buttons: {
			  		cancel : {
				      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
				      	className: "btn-default",
				      	callback: function() {
				        	confirmModal.modal('hide');
				      	}
				    },
				    success: {
			      		label: '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;'+Languages.common_button_update,
				      	className: "btn-warning",
				      	callback: function() {

				      		$("#addReLoading").show();

				        	var request = {
						        news_title : $scope.model.request.txtTitle,
						        news_slug : $("#txtSlug").val(),
						        news_description : $scope.model.request.txtDescription,
						        news_content : "",
						        news_author_aliases : $scope.model.request.txtAlias,
						        status : $scope.model.request.sblStatus,
						        news_thumbnail : $scope.model.request.txtImages,
						        news_release_date : $scope.model.request.dtpPublishes.startDate,
						        news_expiration_date : $scope.model.request.dtpPublishes.endDate,
						        news_format_page : $scope.model.request.radFormatPage,
						        visibility : status,
						        news_cate_id : $scope.model.request.txtCateParent,
						        news_ratings : $scope.model.request.slbRating,
						        news_comment_status : 0,
	 					        news_download : $scope.model.request.txtDownload,
						        news_seo_title : $scope.model.request.txtTitleSEO,
						        news_seo_description : $scope.model.request.txtKeywordSEO,
						        news_seo_keywords : $scope.model.request.txtDescriptionSEO,
						        list_tags : $scope.model.request.slbTags,
						        list_provinces : $scope.model.request.slbProvince
						    };

						    if($scope.model.request.radFormatPage == 1)
							{
								request.news_content = $scope.model.request.txaContent;
							}
							else if($scope.model.request.radFormatPage == 2)
							{
								request.news_content = $scope.model.request.txtContent;
							}
							else
							{
								request.news_format_page = 1;
								request.news_content = $scope.model.request.txaContent;
							}

						    $http.post($scope.model.datainit.appURL + "/SystemApi/UpdateNews", JSON.stringify(request))
						    .then(
               function (response) {
                  var data = response.data;

						    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
					            {
					                toastr.error(data.messages, Languages.common_error_title);

					                if(data.auth != "" && data.auth != null  && data.auth != undefined)
					                {
					                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
					                }
					            }
						    	else if(data.error != "" && data.error != null  && data.error != undefined)
						    	{
						    		toastr.error(data.error, Languages.common_error_title);
						    	}
						    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
						    	{
						    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
						    		warning = warning.replace('["', '').replace('"]', '');
						    		toastr.warning(warning, Languages.common_warning_title);
						    	}
						    	else if(data.success != "" && data.success != null  && data.success != undefined)
						    	{
						    		toastr.success(data.success, commonFunction.languageReplace(Languages.common_success_title,[' Bản Tin']));

						    		bootbox.alert(commonFunction.languageReplace(Languages.common_success_updated,[' Bản Tin']), function() {
										window.location.replace($scope.model.datainit.appURL + "/System/ListNews");
									});

									setTimeout(function(){ 
										window.location.replace($scope.model.datainit.appURL + "/System/ListNews");
									}, 3000);
						    	}
						    	else
						    	{
						    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
						    		{
						    			toastr.error(data.systemerror, Languages.common_error_title);
						    			window.location.replace("./"+data.errorCode);
						    		}
						    		else
						    		{
						    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
						    		}
						    		$("#addReLoading").hide();
						    	}
						    })
						    .error(function (data, status, header, config) {
								toastr.error(Languages.common_error_exception, Languages.common_error_title);
								$("#addReLoading").hide();
						    });
				      	}
				    },
			  	}
			});
		// }
	};

});