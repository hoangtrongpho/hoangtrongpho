/*!
 *
 *	Develop by Vinh Nguyễn - Ho Chi Minh City - 2017
 * 	Copyright Â© 2016-2017 Skyfire Team. All rights reserved.
 *	Design by Skyfire Team 2.3.3
 *
 *	Phone: 0975 578 276
 *	Skype: enjoyvinh
 *
 */

"use strict";

// app.js
// create angular app
var newsNewsApp = angular.module('newsNewsApp', ['commonApp', 'daterangepicker', 'isteven-multi-select', 'ngSanitize',
		'datatables','datatables.colreorder','datatables.bootstrap']);

newsNewsApp.factory('NewsModels', function() {

	var model={
		datainit : {
			dtgListCustomer : [],
			appURL : "http://localhost:8000"
		},
	};

	model.request = {
	};

	model.localLang = {
	    selectAll       : Languages.common_button_select_all,
	    selectNone      : Languages.common_button_deselected_all,
	    reset           : Languages.common_button_cancel,
	    search          : Languages.common_input_label_search,
	    nothingSelected : Languages.common_input_label_select        //default-label is deprecated and replaced with this.
	}

	return model;
});

// create angular controller
newsNewsApp.controller('newsNewsController', function(NewsModels, commonFunction, $scope, $rootScope, $http, $filter, $window, $location, toastr,
	DTOptionsBuilder, DTColumnDefBuilder,DTColumnBuilder) {

	$scope.model = NewsModels;

	$scope.pageInit = function() {

		$scope.model.datainit.appURL = $('meta[name="app_url"]').attr('content');

		$("#listReLoading").show();

		$scope.loadCustomersList();

		$scope.dtOptions = DTOptionsBuilder.newOptions()
			.withOption('order', [])
        	.withPaginationType('full_numbers')
        	.withDisplayLength(10)
	        .withColReorder()
	        // .withColReorderOrder([0, 1, 2, 3, 4])
	        .withBootstrap()
	        .withBootstrapOptions({
	            pagination: {
	                classes: {
	                    ul: 'pagination pagination-md'
	                }
	            }
	        })
	        .withLanguage({
	            "sEmptyTable":     "Không có dữ liệu.",
	            "sInfo":           "Hiển Thị Từ _START_ Đến _END_ Của _TOTAL_ Kết Quả",
	            "sInfoEmpty":      "Hiển Thị Từ 0 Đến 0 Của 0 Kết Quả",
	            "sInfoFiltered":   "(Đã Lọc Từ Tổng _MAX_ Kết Quả)",
	            "sInfoPostFix":    "",
	            "sInfoThousands":  ",",
	            "sLengthMenu":     "Hiển Thị&nbsp;&nbsp;&nbsp;_MENU_&nbsp;&nbsp;&nbsp;Kết Quả / 1 Trang",
	            "sLoadingRecords": "Đang Tải Dữ Liệu...",
	            "sProcessing":     "Đang Xử Lý Dữ Liệu...",
	            "sSearch":         "Từ Khóa: ",
	            "sZeroRecords":    "Không tìm thấy dữ liệu trùng khớp.",
	            "oPaginate": {
	                "sFirst":    "Đầu",
	                "sLast":     "Cuối",
	                "sNext":     "Tiếp",
	                "sPrevious": "Trước"
	            },
	            "oAria": {
	                "sSortAscending":  ": activate to sort column ascending",
	                "sSortDescending": ": activate to sort column descending"
	            }
	        })
	        .withOption('responsive', true);

	    $scope.dtColumnDefs = [
	        DTColumnDefBuilder.newColumnDef(0).notSortable(),
	    ];
	};
	//get Danh Sach San Pham
	$scope.loadCustomersList = function() {

		$("#listReLoading").show();
	
		var request = {
	    };

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetListCustomer", JSON.stringify(request))
	    .then(
               function (response) {
                  var data = response.data;

		    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
	            {
	                toastr.error(data.messages, Languages.common_error_title);

	                if(data.auth != "" && data.auth != null  && data.auth != undefined)
	                {
	                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
	                }
	            }
		    	else if(data.error != "" && data.error != null  && data.error != undefined)
		    	{
		    		toastr.error(data.error, Languages.common_error_title);
		    		$scope.model.datainit.dtgListCustomer = [];
		    	}
		    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
		    	{
		    		toastr.warning(data.warning, Languages.common_warning_title);
		    		$scope.model.datainit.dtgListCustomer = [];
		    	}
		    	else if(data.success != "" && data.success != null  && data.success != undefined)
		    	{
		    		$scope.model.datainit.dtgListCustomer = data.success;
		    	}
		    	else
		    	{
		    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
		    		{
		    			toastr.error(data.systemerror, Languages.common_error_title);
		    			window.location.replace("./"+data.errorCode);
		    		}
		    		else
		    		{
		    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
		    		}
		    		$scope.model.datainit.dtgListCustomer = [];
		    	}
		    	$("#listReLoading").hide();
	    	},
           	function (response) {
          		var data = response.data;
				toastr.error(Languages.common_error_exception, Languages.common_error_title);
				$scope.model.datainit.dtgListCustomer = [];
				$("#listReLoading").hide();
		    });
	};
	//delete 
	$scope.removeItem = function(id) {

    	if(id == "" || id == null || id == undefined)
		{
			toastr.error(Resources.common_error_exception, Resources.common_error_title);
			return false;
		}
    	
		var confirmModal = bootbox.dialog({
			message: commonFunction.languageReplace(Languages.common_confirm_delete_question,[' Khách Hàng']),
		  	title: commonFunction.languageReplace(Languages.common_confirm_delete_title,[' Khách Hàng']),
		  	closeButton: true,
		  	animate: true,
		  	className: "confirmModal",
		  	backdrop: true,
		  	headerBackground: "bg-red",
		  	size: "small",
		  	locale: "vi",
		  	show: true,
		  	buttons: {
		  		cancel : {
			      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
			      	className: "btn-default",
			      	callback: function() {
			        	confirmModal.modal('hide');
			      	}
			    },
			    success: {
		      		label: '<i class="fa fa-trash" aria-hidden="true"></i>&nbsp;'+Languages.common_button_remove,
			      	className: "btn-danger",
			      	callback: function() {

			      		$("#addReLoading").show();

			        	var request = {
					        customer_id : id,
					    };

					    $http.post($scope.model.datainit.appURL + "/SystemApi/RemoveCustomer", JSON.stringify(request))
					    	.then(
               					function (response) {
                  					var data = response.data;

							    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
						            {
						                toastr.error(data.messages, Languages.common_error_title);

						                if(data.auth != "" && data.auth != null  && data.auth != undefined)
						                {
						                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
						                }
						            }
							    	else if(data.error != "" && data.error != null  && data.error != undefined)
							    	{
							    		toastr.error(data.error, Languages.common_error_title);
							    	}
							    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
							    	{
							    		toastr.warning(data.warning, Languages.common_warning_title);
							    	}
							    	else if(data.success != "" && data.success != null  && data.success != undefined)
							    	{
							    		toastr.success(data.success, commonFunction.languageReplace(Languages.common_success_removed,['Khách Hàng']));
							    		$scope.loadCustomersList();
							    	}
							    	else
							    	{
							    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
							    		{
							    			toastr.error(data.systemerror, Languages.common_error_title);
							    			window.location.replace("./"+data.errorCode);
							    		}
							    		else
							    		{
							    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
							    		}
							    	}
							    	$("#addReLoading").hide();
					    		},
					    		function (response) {
				                  	var data = response.data;	
									toastr.error(Languages.common_error_exception, Languages.common_error_title);
									$("#addReLoading").hide();
					    		}
					    	);
			      	}
			    }
		  	}
		});
    };
	// changeStatus
   	$scope.changeStatus = function(id,status) {

		if(id == "" || id == null || id == undefined)
		{
			toastr.error(Resources.common_error_exception, Resources.common_error_title);
			return false;
		}
			
		var confirmModal = bootbox.dialog({
		  	message: status == 2 ? "Bạn Có Muốn Mở Tài Khoản?" : "Bạn Có Muốn Khóa tài khoản?" ,
		  	title: "Mở/Khóa Tài Khoản",
		  	closeButton: true,
		  	animate: true,
		  	className: "confirmModal",
		  	backdrop: true,
		  	headerBackground: "bg-yellow",
		  	size: "small",
		  	locale: "vi",
		  	show: true,
		  	buttons: {
		  		cancel : {
			      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
			      	className: "btn-default",
			      	callback: function() {
			        	confirmModal.modal('hide');
			      	}
			    },
			    success: {
		      		label: '<i class="fa fa-trash" aria-hidden="true"></i>&nbsp;'+Languages.common_button_update,
			      	className: "btn-warning",
			      	callback: function() {

			      		$("#listReLoading").show();

						//Khởi Tạo Dữ Liệu
						var request = {
					        customer_id : id,
					        status : status
					    };

					    $http.post($scope.model.datainit.appURL + "/SystemApi/ChangeStatusCustomer", JSON.stringify(request))
					    .then(

					    	function (response) {
                
				                var data = response.data;

						    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
					            {
					                toastr.error(data.messages, Languages.common_error_title);

					                if(data.auth != "" && data.auth != null  && data.auth != undefined)
					                {
					                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
					                }
					            }
						    	else if(data.error != "" && data.error != null  && data.error != undefined)
						    	{
						    		toastr.error(data.error, Languages.common_error_title);
						    		$scope.model.datainit.dtgListCustomer = [];
						    	}
						    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
						    	{
						    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
						    		warning = warning.replace('["', '').replace('"]', '');
						    		toastr.warning(warning, Languages.common_warning_title);
						    		$scope.model.datainit.dtgListCustomer = [];
						    	}
						    	else if(data.success != "" && data.success != null  && data.success != undefined)
						    	{
				    				toastr.success(data.success, commonFunction.languageReplace(Languages.common_success_updated,['Trạng Thái']));
						    		$scope.loadCustomersList();
						    	}
						    	else
						    	{
						    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
						    		{
						    			toastr.error(data.systemerror, Languages.common_error_title);
						    			window.location.replace("./"+data.errorCode);
						    		}
						    		else
						    		{
						    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
						    		}
						    		$scope.model.datainit.dtgListCustomer = [];
						    	}
					    		$("#listReLoading").hide();
						    },
			               	function (response) {
			                  	var data = response.data;
								toastr.error(Languages.common_error_exception, Languages.common_error_title);
								$scope.model.datainit.dtgListCustomer = [];
								$("#listReLoading").hide();
						    }
				    	);
					    // end then
								 
			      	}
			    },
		  	}
		});
	};
});