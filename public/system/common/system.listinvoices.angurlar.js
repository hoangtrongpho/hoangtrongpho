/*!
 *
 *	Develop by Vinh Nguyễn - Ho Chi Minh City - 2017
 * 	Copyright Â© 2016-2017 Skyfire Team. All rights reserved.
 *	Design by Skyfire Team 2.3.3
 *
 *	Phone: 0975 578 276
 *	Skype: enjoyvinh
 *
 */

"use strict";

// app.JSON
// create angular app
var invoiceApp = angular.module('invoiceApp', ['commonApp', 'daterangepicker', 'isteven-multi-select', 'ngSanitize','ng-currency',
		'datatables','datatables.colreorder','datatables.bootstrap']);

invoiceApp.factory('NewsModels', function() {

	var model={
		datainit : {
			dtgListBranch: [],
			dtgListInvoice:[],
			slbCustomerDistrict:[],
			sumStatus1:0,
			sumStatus2:0,
			sumStatus3:0,
			sumStatus4:0,
			sumStatus6:0,
			sumStatus57:0,
			appURL : "http://localhost:8000"
		},
	};

	model.request = {
		slbCustomerDistrict:[],
		dtpCreatedRange: {
			startDate: moment(), 
			endDate: moment().endOf('month')
		}
	};

	model.localLang = {
	    selectAll       : Languages.common_button_select_all,
	    selectNone      : Languages.common_button_deselected_all,
	    reset           : Languages.common_button_cancel,
	    search          : Languages.common_input_label_search,
	    nothingSelected : Languages.common_input_label_select        //default-label is deprecated and replaced with this.
	}
	
	return model;
});

// create angular controller
invoiceApp.controller('invoiceController', function(NewsModels, commonFunction, $scope, $rootScope, $http, $filter, $window, $location, toastr,
	DTOptionsBuilder, DTColumnDefBuilder,DTColumnBuilder) {

	$scope.model = NewsModels;

	$scope.pageInit = function() {
	
		$scope.model.datainit.appURL = $('meta[name="app_url"]').attr('content');

		$scope.GetBranchList();
		$scope.loadInvoiceList("");

		$("#listReLoading").hide();
		$scope.dtOptions = DTOptionsBuilder.newOptions()
			.withOption('order', [])
        	.withPaginationType('full_numbers')
        	.withDisplayLength(10)
	        .withColReorder()
	        // .withColReorderOrder([0, 1, 2, 3, 4])
	        .withBootstrap()
	        .withBootstrapOptions({
	            pagination: {
	                classes: {
	                    ul: 'pagination pagination-md'
	                }
	            }
	        })
	        .withLanguage({
	            "sEmptyTable":     "Không có dữ liệu.",
	            "sInfo":           "Hiển Thị Từ _START_ Đến _END_ Của _TOTAL_ Kết Quả",
	            "sInfoEmpty":      "Hiển Thị Từ 0 Đến 0 Của 0 Kết Quả",
	            "sInfoFiltered":   "(Đã Lọc Từ Tổng _MAX_ Kết Quả)",
	            "sInfoPostFix":    "",
	            "sInfoThousands":  ",",
	            "sLengthMenu":     "Hiển Thị&nbsp;&nbsp;&nbsp;_MENU_&nbsp;&nbsp;&nbsp;Kết Quả / 1 Trang",
	            "sLoadingRecords": "Đang Tải Dữ Liệu...",
	            "sProcessing":     "Đang Xử Lý Dữ Liệu...",
	            "sSearch":         "Từ Khóa: ",
	            "sZeroRecords":    "Không tìm thấy dữ liệu trùng khớp.",
	            "oPaginate": {
	                "sFirst":    "Đầu",
	                "sLast":     "Cuối",
	                "sNext":     "Tiếp",
	                "sPrevious": "Trước"
	            },
	            "oAria": {
	                "sSortAscending":  ": activate to sort column ascending",
	                "sSortDescending": ": activate to sort column descending"
	            }
	        })
	        .withOption('responsive', true);

	    $scope.dtColumnDefs = [
	        DTColumnDefBuilder.newColumnDef(0).notSortable(),
	    ];
	};
	// GetBranchList
	$scope.GetBranchList = function() {

		$("#listReLoading").show();

		var request = {
	    };

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetBranchList", JSON.stringify(request))
	    .then(
               function (response) {
                  var data = response.data;

		    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
	            {
	                toastr.error(data.messages, Languages.common_error_title);

	                if(data.auth != "" && data.auth != null  && data.auth != undefined)
	                {
	                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
	                }
	            }
		    	else if(data.error != "" && data.error != null  && data.error != undefined)
		    	{
		    		toastr.error(data.error, Languages.common_error_title);
		    		$scope.model.datainit.dtgListBranch = [];
		    	}
		    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
		    	{
		    		toastr.warning(data.warning, Languages.common_warning_title);
		    		$scope.model.datainit.dtgListBranch = [];
		    	}
		    	else if(data.success != "" && data.success != null  && data.success != undefined)
		    	{
		    		$scope.model.datainit.dtgListBranch = data.success;
		    	}
		    	else
		    	{
		    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
		    		{
		    			toastr.error(data.systemerror, Languages.common_error_title);
		    			window.location.replace("./"+data.errorCode);
		    		}
		    		else
		    		{
		    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
		    		}
		    		$scope.model.datainit.dtgListBranch = [];
		    	}
		    	$("#listReLoading").hide();
	    	},
           	function (response) {
          		var data = response.data;
				toastr.error(Languages.common_error_exception, Languages.common_error_title);
				$scope.model.datainit.dtgListBranch = [];
				$("#listReLoading").hide();
		    });
	};
	// SearchFunction
	$scope.SearchFunction = function(item) {
		// alert(item.branch_id);	
		$scope.loadInvoiceList(item);
	};
	//get Danh Sach San Pham
	$scope.loadInvoiceList = function(filterItem) {
		// RESERT sum 
		$scope.model.datainit.sumStatus1=0;
		$scope.model.datainit.sumStatus2=0;
		$scope.model.datainit.sumStatus3=0;
		$scope.model.datainit.sumStatus4=0;
		$scope.model.datainit.sumStatus6=0;
		$scope.model.datainit.sumStatus57=0;

		$("#listReLoading").show();
		var request = {
	    };
		// filter value
		if(filterItem!="")
		{
			if(filterItem.branch_id  != null)
			{
				var request = {
					branch_id : filterItem.branch_id,
	    		};
	    		// alert(request.branch_id);
			}
		}
		

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetListInvoice", JSON.stringify(request))
	    .then(
               function (response) {
                  var data = response.data;

		    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
	            {
	                toastr.error(data.messages, Languages.common_error_title);

	                if(data.auth != "" && data.auth != null  && data.auth != undefined)
	                {
	                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
	                }
	            }
		    	else if(data.error != "" && data.error != null  && data.error != undefined)
		    	{
		    		toastr.error(data.error, Languages.common_error_title);
		    		$scope.model.datainit.dtgListInvoice = [];
		    	}
		    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
		    	{
		    		toastr.warning(data.warning, Languages.common_warning_title);
		    		$scope.model.datainit.dtgListInvoice = [];
		    	}
		    	else if(data.success != "" && data.success != null  && data.success != undefined)
		    	{
		    		$scope.model.datainit.dtgListInvoice = data.success;
		    	}
		    	else
		    	{
		    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
		    		{
		    			toastr.error(data.systemerror, Languages.common_error_title);
		    			window.location.replace("./"+data.errorCode);
		    		}
		    		else
		    		{
		    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
		    		}
		    		$scope.model.datainit.dtgListInvoice = [];
		    	}
		    	$("#listReLoading").hide();
	    	},
           	function (response) {
         
          		var data = response.data;
				toastr.error(Languages.common_error_exception, Languages.common_error_title);
				$scope.model.datainit.dtgListInvoice = [];
				$("#listReLoading").hide();
		    });
	};
	//changeInvoiceStatus: 1 2 3 4 5
   	$scope.changeInvoiceStatus = function(id,status) {

		if(id == "" || id == null || id == undefined)
		{
			toastr.error(Resources.common_error_exception, Resources.common_error_title);
			return false;
		}
		var mess="";
		switch (status) {
		    case 1:
		        mess = "Thay Đổi Trạng Thái: Hủy Đơn Hàng";
		        break;
		    case 2:
		        mess = "Thay Đổi Trạng Thái: Chưa Xử Lý Đơn Hàng";
		        break;
		    case 3:
		        mess = "Thay Đổi Trạng Thái: Đang Xử Lý Đơn Hàng";
		        break;
		    case 4:
		        mess = "Thay Đổi Trạng Thái: Đã Nhận Hàng";
		        break;
		    case 5:
		        mess = "Thay Đổi Trạng Thái: Hoàn Thành Đơn Hàng";
		        break;
	        default:
		        mess = "Thay Đổi Trạng Thái: Nhận Hàng & Thanh Toán";
		        break;
		}
		var confirmModal = bootbox.dialog({
		  	message: mess,
		  	title: "Đổi Trạng Thái Đơn Hàng",
		  	closeButton: true,
		  	animate: true,
		  	className: "confirmModal",
		  	backdrop: true,
		  	headerBackground: "bg-yellow",
		  	size: "small",
		  	locale: "vi",
		  	show: true,
		  	buttons: {
		  		cancel : {
			      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
			      	className: "btn-default",
			      	callback: function() {
			        	confirmModal.modal('hide');
			      	}
			    },
			    success: {
		      		label: '<i class="fa fa-trash" aria-hidden="true"></i>&nbsp;'+Languages.common_button_update,
			      	className: "btn-warning",
			      	callback: function() {

			      		$("#listReLoading").show();

						//Khởi Tạo Dữ Liệu
						var request = {
					        invoice_id : id,
					        status : status
					    };

					    $http.post($scope.model.datainit.appURL + "/SystemApi/ChangeStatusInvoice", JSON.stringify(request))
					    .then(function (response) {
                  			var data = response.data;

					    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
				            {
				                toastr.error(data.messages, Languages.common_error_title);

				                if(data.auth != "" && data.auth != null  && data.auth != undefined)
				                {
				                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
				                }
				            }
					    	else if(data.error != "" && data.error != null  && data.error != undefined)
					    	{
					    		toastr.error(data.error, Languages.common_error_title);
					    	}
					    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
					    	{
								var warning = String(data.warning[Object.keys(data.warning)[0]]);
					    		warning = warning.replace('["', '').replace('"]', '');
					    		toastr.warning(warning, Languages.common_warning_title);
					    	}
					    	else if(data.success != "" && data.success != null  && data.success != undefined)
					    	{
					    		toastr.success(data.success, Languages.common_success_title);
					    		$scope.loadInvoiceList();
					    	}
					    	else
					    	{
					    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
					    		{
					    			toastr.error(data.systemerror, Languages.common_error_title);
					    			window.location.replace("./"+data.errorCode);
					    		}
					    		else
					    		{
					    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
					    		}
					    	}
					    	$("#listReLoading").hide();
					    },
					    function (response) {
		                 
		                  	var data = response.data;	
							toastr.error(Languages.common_error_exception, Languages.common_error_title);
							$("#addReLoading").hide();
			    		});
			      	}
			    },
		  	}
		});
	};
	//delete 
	$scope.removeItem = function(id) {

    	if(id == "" || id == null || id == undefined)
		{
			toastr.error(Resources.common_error_exception, Resources.common_error_title);
			return false;
		}
    	
		var confirmModal = bootbox.dialog({
			message: commonFunction.languageReplace(Languages.common_confirm_delete_question,[' Đơn Hàng']),
		  	title: commonFunction.languageReplace(Languages.common_confirm_delete_title,[' Đơn Hàng']),
		  	closeButton: true,
		  	animate: true,
		  	className: "confirmModal",
		  	backdrop: true,
		  	headerBackground: "bg-red",
		  	size: "small",
		  	locale: "vi",
		  	show: true,
		  	buttons: {
		  		cancel : {
			      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
			      	className: "btn-default",
			      	callback: function() {
			        	confirmModal.modal('hide');
			      	}
			    },
			    success: {
		      		label: '<i class="fa fa-trash" aria-hidden="true"></i>&nbsp;'+Languages.common_button_remove,
			      	className: "btn-danger",
			      	callback: function() {

			      		$("#addReLoading").show();

			        	var request = {
					        invoice_id : id,
					    };

					    $http.post($scope.model.datainit.appURL + "/SystemApi/RemoveInvoice", JSON.stringify(request))
					    	.then(
               					function (response) {
                  					var data = response.data;

							    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
						            {
						                toastr.error(data.messages, Languages.common_error_title);

						                if(data.auth != "" && data.auth != null  && data.auth != undefined)
						                {
						                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
						                }
						            }
							    	else if(data.error != "" && data.error != null  && data.error != undefined)
							    	{
							    		toastr.error(data.error, Languages.common_error_title);
							    	}
							    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
							    	{
							    		toastr.warning(data.warning, Languages.common_warning_title);
							    	}
							    	else if(data.success != "" && data.success != null  && data.success != undefined)
							    	{
							    		toastr.success(data.success, commonFunction.languageReplace(Languages.common_success_removed,['Khách Hàng']));
							    		$scope.loadInvoiceList();
							    	}
							    	else
							    	{
							    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
							    		{
							    			toastr.error(data.systemerror, Languages.common_error_title);
							    			window.location.replace("./"+data.errorCode);
							    		}
							    		else
							    		{
							    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
							    		}
							    	}
							    	$("#addReLoading").hide();
					    		},
					    		function (response) {
				                
				                  	var data = response.data;	
									toastr.error(Languages.common_error_exception, Languages.common_error_title);
									$("#addReLoading").hide();
					    		}
					    	);
			      	}
			    }
		  	}
		});
    };
	//======slbItemClick==========
	$scope.slbItemClick = function(item,element)
	{
   		if(element == "txtCustomerGroup")
		{
			$('#'+element).val(item.group_id); 
   			$('#'+element).trigger('input');
		}
		else if(element == "txtCustomerDistrict")
		{
			$('#'+element).val(item.district_id); 
   			$('#'+element).trigger('input');
		}
	};
	$scope.slbClear = function(element)
	{

		if(element == "txtCustomerGroup")
		{
			angular.forEach($scope.model.datainit.slbCustomerGroup, function(value, key) {
		    	value.ticked = false;
			});
		}
		
		if(element == "txtCustomerDistrict")
		{
			angular.forEach($scope.model.datainit.slbCustomerDistrict, function(value, key) {
		    	value.ticked = false;
			});
		}
	   	$('#'+element).val(""); 
	   	$('#'+element).trigger('input');
	};
	//=====loadInvoiceDetail============
	$scope.loadInvoiceDetail = function(invoiceId) {
		$("#loader").show();
		if(invoiceId !== "")
	    {
	    	var request = {
		        invoice_id : invoiceId,
		    };
		    //alert($("#txtSlug").val());

		    $http.post($scope.model.datainit.appURL + "/SystemApi/GetInvoiceDetail", JSON.stringify(request))
	    	.then(
               	function (response) {
                  	var data = response.data;

				    	if(data.error != "" && data.error != null  && data.error != undefined)
				    	{
				    		toastr.error(data.error, "Lỗi Hệ thông");
				    	}
				    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
				    	{
				    		toastr.warning(data.warning, "Cảnh Báo");
				    	}
				    	else if(data.success != "" && data.success != null  && data.success != undefined)
				    	{

				    		//debugger;
				    		$scope.model.request = {
								txtCustomerId: data.success.customer_id,
					    		txtCustomerEmail :data.success.customer_email,
					    		txtCustomerPhone :data.success.customer_phone,
					    		txtCustomerName: data.success.customer_fullname,
					    		txtCustomerAddress: data.success.customer_address,
					    		txtCustomerAccount :data.success.customer_account,
					    		txtCustomerDistrict: data.success.district_name,
					    		txtReceivedName: data.success.receiver_name,
								txtReceivedPhone: data.success.receiver_phone,
								txtReceivedAddress: data.success.receiver_address,
								txtUpdatedDate: data.success.updated_date_format,

								txtBranchName: data.success.branch_name,
								txtBranchPhone: data.success.branch_phone,
								txtBranchAddress: data.success.branch_address_number,

								txtInvoiceId:data.success.invoice_id,
								txtInvoiceNumber: data.success.invoice_number,
								txtTotalSum : data.success.total_sum,
								dtpDeliverDate : moment(data.success.delivery_date).format("DD/MM/YYYY"),
								txtInvoiceNote: data.success.invoice_note,
								swExportInvoiceStatus : data.success.export_invoice_status,
								radioStatus: data.success.status.toString(),
							};
							if(data.success.payment_method==null){
								$scope.model.request.radioPaymentMethod="1";
							}
							else{
								$scope.model.request.radioPaymentMethod=data.success.payment_method.toString();
							}
							

							angular.forEach($scope.model.datainit.slbCustomer, function(value, key2) {
							    if(value.customer_id == data.success.customer_id)
							    {
							    	value.ticked = true;
							    }
							});
							$scope.model.request.dtgListProductInvoice=data.product_list;

							$scope.model.datainit.pageStatus = "update";
				    	}
				    	else
				    	{
				    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
				    		{
				    			toastr.error(data.systemerror, "Lỗi Hệ thông");
				    		}
				    		else
				    		{
				    			toastr.error("Lỗi Hệ thông", "Lỗi Hệ thông");
				    		}
				    	}
				    	$("#loader").hide();
			    },
               	function (response) {
                  	$("#loader").hide();
                 	var data = response.data;
					toastr.error("Lỗi Hệ thông", "Lỗi Hệ thông");
			    }
			);
	    }
	};
	// filter
	$scope.filterFinishStatus = function (item) { 
	    return item.status === 5 || item.status === 7; 
	};
});