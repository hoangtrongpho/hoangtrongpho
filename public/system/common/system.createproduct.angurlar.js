/*!
 *
 *	Develop by Vinh Nguyễn - Ho Chi Minh City - 2017
 * 	Copyright Â© 2016-2017 Skyfire Team. All rights reserved.
 *	Design by Skyfire Team 2.3.3
 *
 *	Phone: 0975 578 276
 *	Skype: enjoyvinh
 *
 */

"use strict";

// app.js
// create angular app
var createProductApp = angular.module('createProductApp', ['commonApp', 'daterangepicker','bootstrap-switch','isteven-multi-select',
	'ui.tinymce', 'ngAnimate', 'ngSanitize', , 'ui.bootstrap', 'ui.bootstrap.datetimepicker', 'icheckAngular', 'ng-currency', 
	'localytics.directives']);

createProductApp.factory('ProductModels', function() {

	var model={
		datainit : {
			slbListCategories : [],
			slbListTags : [],
			slbListUnits : [],
			slbListOrigins : [],
			slbListManufacturer: [],
			slbListColor : [],
			pageStatus : "add",
			ratingOptions : [
		        {
		          name: '1 Sao',
		          value: 1
		        }, 
		        {
		          name: '2 Sao',
		          value: 2
		        }, 
		        {
		          name: '3 Sao',
		          value: 3
		        }, 
		        {
		          name: '4 Sao',
		          value: 4
		        }, 
		        {
		          name: '5 Sao',
		          value: 5
		        }, 
		    ],
		    appURL : $('meta[name="app_url"]').attr('content')
		},
	};

	model.request = {
		txtSlug : "",
		txt_unit_id : "",
		txt_cate_id : "",
		txt_origin_id : "",
		txt_manufacturer_id : "",
		txt_color_id : "",

		slb_Tag : [],
		txt_tag_id : "",

		dtpPublishes: {
			startDate: moment(), 
			endDate: moment().endOf('month')
		},

		txt_product_name : "",
		txt_product_description:"",
		a_product_specifications:"",
		txt_product_imported_prices:"",
		txt_product_retail_prices:"",
		txt_product_deals:"",
		slb_product_ratings: 1,
		txt_product_thumbnail : $('meta[name="app_url"]').attr('content')+"/public/images/default-motocycle.png",
		txtImages1: $('meta[name="app_url"]').attr('content')+"/public/images/default-motocycle.png",
		txtImages2: $('meta[name="app_url"]').attr('content')+"/public/images/default-motocycle.png",
		txtImages3: $('meta[name="app_url"]').attr('content')+"/public/images/default-motocycle.png",
		txtImages4: $('meta[name="app_url"]').attr('content')+"/public/images/default-motocycle.png",

		txt_product_release_date:"",
		txt_product_expiration_date:"",
		sw_product_comment_status:0,
		sw_product_saleoff_pinned:0,
		sw_product_new_pinned:0,
		sw_product_quick_pinned:0,
		txt_news_seo_title : "",
		txt_news_seo_description : "",
		txt_news_seo_keywords : "",
		sbl_status : 1,

		radFormatPage : 1,
		
		txtImages : $('meta[name="app_url"]').attr('content')+"/public/images/default-motocycle.png",
	
	};

	model.datePickerOptions = {
    	minView : 'day',
    	startView : 'day',
    	modelType : 'DD/MM/YYYY'
  	};

  	model.datePickerOpts = {
		showDropdowns : true,
		singleDatePicker : false,
		timePicker : false,
		timePickerSeconds : false,
		timePicker24Hour : true,
		alwaysShowCalendars : false,
		showWeekNumbers : true,
		autoApply : true,
		opens: "left",
		drops: "down",
		clearLabel: Languages.common_button_cancel,
		cancelClass : 'btn-default',
		autoUpdateInput : true,
	    parentEl: '.cal-block .form-group .oneway',
        locale: {
            buttonClasses : 'btn btn-sm',
	        applyClass : 'btn-success',
            applyLabel: Languages.common_button_select_item,
            fromLabel: Languages.common_label_from,
            format: "DD/MM/YYYY",
            toLabel: Languages.common_label_to,
            weekLabel: Languages.datepicker_week_title,
            customRangeLabel: Languages.common_button_select_date_time,
            daysOfWeek: [
            				Languages.datepicker_week_monday,
            				Languages.datepicker_week_tuesday,
            				Languages.datepicker_week_wednesday,
            				Languages.datepicker_week_thursday,
            				Languages.datepicker_week_friday,
            				Languages.datepicker_week_saturday,
            				Languages.datepicker_week_sunday
            			],
            monthNames: [
	            			Languages.datepicker_month_january, 
	            			Languages.datepicker_month_february,
	            			Languages.datepicker_month_march,
	            			Languages.datepicker_month_april,
	            			Languages.datepicker_month_may,
	            			Languages.datepicker_month_june,
	            			Languages.datepicker_month_july,
	            			Languages.datepicker_month_august,
	            			Languages.datepicker_month_september,
	            			Languages.datepicker_month_october,	
	            			Languages.datepicker_month_november,
	            			Languages.datepicker_month_december
            			],
        },
        ranges: {
        	'Chỉ Hôm Nay': [moment(), moment()],
        	'Ngày Mai' : [moment(), moment().add(1, 'day')],
        	'Trong 1 Tuần': [moment(), moment().add(1, 'week').endOf('month')],
        	'Trong 2 Tuần': [moment(), moment().add(2, 'week').endOf('month')],
        	'Trong 1 Tháng': [moment(), moment().add(1, 'month').endOf('month')],
        	'Trong 3 Tháng': [moment(), moment().add(3, 'month').endOf('month')],
        	'Trong 6 Tháng': [moment(), moment().add(6, 'month').endOf('month')],
        	'Trong 1 Năm': [moment(), moment().add(1, 'year').endOf('month')],
        	'Trong 3 Năm': [moment(), moment().add(3, 'year').endOf('month')]
        },      
    };

	model.localLangCate = {
	    selectAll       : Languages.common_button_select_all,
	    selectNone      : Languages.common_button_deselected_all,
	    reset           : Languages.common_button_cancel,
	    search          : Languages.common_input_label_search,
	    nothingSelected : Languages.common_input_label_select
	}

	model.tinymceOptions = {
		height: "1000px",
		path_absolute : "/",
		selector: "textarea",
		theme: 'modern',
		readonly: false,
		plugins: [
			'advlist autolink lists link image charmap print preview hr anchor pagebreak',
			'searchreplace wordcount visualblocks visualchars code fullscreen',
			'insertdatetime media nonbreaking save table contextmenu directionality',
			'emoticons template paste textcolor colorpicker textpattern imagetools'
		],
	   toolbar2: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
		toolbar1: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | insertdatetime preview | forecolor backcolor",
		toolbar3: "table | hr removeformat | subscript superscript | charmap emoticons | fullscreen | ltr rtl | spellchecker | visualchars visualblocks nonbreaking pagebreak restoredraft",
		menubar: true,
		toolbar_items_size: 'small',
		image_advtab: true,
		resize : 'none',	
		content_css: [
			'//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
			'//www.tinymce.com/css/codepen.min.css'
		],
	    relative_urls: true,
	    allow_html_in_named_anchor: true, //This option allows you to specify whether the editor should parse and keep html within named anchor tags.
	    allow_conditional_comments: false, //This option allows you to specify whether the editor should parse and keep conditional comments.
	    convert_fonts_to_spans : false, //If you set this option to true, TinyMCE will convert all font elements to span elements and generate span elements instead of font elements. 
	    element_format : 'html', //This option controls whether elements are output in the HTML or XHTML mode.
	    encoding: 'xml', //This option allows you to get XML escaped content out of TinyMCE. By setting this option to xml, posted content will be converted to an XML string escaping characters such as <, >, ", and & to <, >, ", and &.
	    entity_encoding : "raw", //named	Characters will be converted into named entities based on the entities option. For example, a non-breaking space could be encoded as &nbsp;. This value is default.
								 //numeric	Characters will be converted into numeric entities. For example, a non-breaking space would be encoded as &#160;.
								 //raw	All characters will be stored in non-entity form except these XML default entities: & < > "
		fix_list_elements : true,
		force_p_newlines : true,
		schema: 'html5', //The schema option enables you to switch between the HTML4 and HTML5 schema.
    	visual: true,
    	font_formats: 'Andale Mono=andale mono,times;'+ 'Arial=arial,helvetica,sans-serif;'+ 'Arial Black=arial black,avant garde;'+ 'Book Antiqua=book antiqua,palatino;'+ 'Comic Sans MS=comic sans ms,sans-serif;'+ 'Courier New=courier new,courier;'+ 'Georgia=georgia,palatino;'+ 'Helvetica=helvetica;'+ 'Impact=impact,chicago;'+ 'Symbol=symbol;'+ 'Tahoma=tahoma,arial,helvetica,sans-serif;'+ 'Terminal=terminal,monaco;'+ 'Times New Roman=times new roman,times;'+ 'Trebuchet MS=trebuchet ms,geneva;'+ 'Verdana=verdana,geneva;'+ 'Webdings=webdings;'+ 'Wingdings=wingdings,zapf dingbats',
    	fontsize_formats: '8pt 9pt 10pt 11px 12pt 13pt 14pt 15pt 16pt 17pt 18pt 19pt 20pt 21pt 22pt 23pt 24pt 25pt 26pt 27pt 28pt 29pt 30pt 36pt 40pt 50pt 60pt 70pt',
    	browser_spellcheck: true, //One of the several spell checking options developers have available is to use the browser's native spell checker. browser_spellcheck handles this behavior.
    	file_browser_callback : function(field_name, url, type, win) {
	        var x = $( window ).width();
	        var y = $( window ).height();
	        var cmsURL = '/MultimediaManager?field_name=' + field_name;

	        // if (type == 'image') 
	        // {
	        	cmsURL = cmsURL + "&type=Images";
	        // } 
	        // else
	        // {
	        // 	cmsURL = cmsURL + "&type=Files";
	        // }
	        tinyMCE.activeEditor.windowManager.open({
	            file : cmsURL,
	            title : 'Chọn Ảnh Từ Thư viện',
	            width : x * 0.8,
	            height : y * 0.8,
	            resizable : "no",
	            close_previous : "yes"
            });
        }
	};

	return model;
});

// create angular controller
createProductApp.controller('createProductController', function(ProductModels, commonFunction, $scope, $rootScope, $http, $filter, $window, $location, $uibModal, toastr) {

	$scope.model = ProductModels;

	$scope.resetChoiceThumbnail = function(idholder,idthumnail) {
    	$("#"+idholder).attr("src",$scope.model.datainit.appURL + "/public/images/default-motocycle.png");
		$("#"+idthumnail).val($scope.model.datainit.appURL + "/public/images/default-motocycle.png");
		$('#'+idthumnail).trigger('input');
    };

	$scope.pageInit = function() {

		$scope.model.datainit.appURL = $('meta[name="app_url"]').attr('content');
		$scope.GetListProductCat();
		$scope.GetListUnit();
		$scope.GetListOrigin();
		$scope.GetListManufacturer();
		$scope.GetListColor();
		$scope.GetListTag();
		$scope.loadProductDetail();
		$("#addReLoading").show();
	};
	// GetListProductCat
	$scope.GetListProductCat = function() {
		$http.post($scope.model.datainit.appURL + "/SystemApi/GetListProductCat")
	    	.then(
				function (response) {
					var data = response.data;

					if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
					{
						toastr.error(data.messages, Languages.common_error_title);

						if(data.auth != "" && data.auth != null  && data.auth != undefined)
						{
							$window.location.href = $scope.model.datainit.appURL + data.redirect;
						}
					}
					else if(data.error != "" && data.error != null  && data.error != undefined)
					{
						toastr.error(data.error, Languages.common_error_title);
						$scope.model.datainit.slbListCategories = [];
					}
					else if(data.warning != "" && data.warning != null  && data.warning != undefined)
					{
						$scope.model.datainit.slbListCategories = [];
					}
					else if(data.success != "" && data.success != null  && data.success != undefined)
					{
						$scope.model.datainit.slbListCategories = data.success;
					}
					else
					{
						if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
						{
							toastr.error(data.systemerror, Languages.common_error_title);
							window.location.replace("./"+data.errorCode);
						}
						else
						{
							toastr.error(Languages.common_error_exception, Languages.common_error_title);
						}
						$scope.model.datainit.slbListCategories = [];
					}
				},
				function (response) {
			
					var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$scope.model.datainit.slbListCategories = [];
	    		}
	    	);
	}
	// GetListUnit
	$scope.GetListUnit = function() {
		$http.post($scope.model.datainit.appURL + "/SystemApi/GetListUnit")
	    	.then(
				function (response) {
					var data = response.data;

					if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
					{
						toastr.error(data.messages, Languages.common_error_title);

						if(data.auth != "" && data.auth != null  && data.auth != undefined)
						{
							$window.location.href = $scope.model.datainit.appURL + data.redirect;
						}
					}
					else if(data.error != "" && data.error != null  && data.error != undefined)
					{
						toastr.error(data.error, Languages.common_error_title);
						$scope.model.datainit.slbListUnits = [];
					}
					else if(data.warning != "" && data.warning != null  && data.warning != undefined)
					{
						$scope.model.datainit.slbListUnits = [];
					}
					else if(data.success != "" && data.success != null  && data.success != undefined)
					{
						$scope.model.datainit.slbListUnits = data.success;
					}
					else
					{
						if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
						{
							toastr.error(data.systemerror, Languages.common_error_title);
							window.location.replace("./"+data.errorCode);
						}
						else
						{
							toastr.error(Languages.common_error_exception, Languages.common_error_title);
						}
						$scope.model.datainit.slbListUnits = [];
					}
				},
				function (response) {
	
					var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$scope.model.datainit.slbListUnits = [];
	    		}
	    	);
	}
	// GetListUnit
	$scope.GetListOrigin = function() {
		$http.post($scope.model.datainit.appURL + "/SystemApi/GetListOrigin")
	    	.then(
				function (response) {
					var data = response.data;

					if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
					{
						toastr.error(data.messages, Languages.common_error_title);

						if(data.auth != "" && data.auth != null  && data.auth != undefined)
						{
							$window.location.href = $scope.model.datainit.appURL + data.redirect;
						}
					}
					else if(data.error != "" && data.error != null  && data.error != undefined)
					{
						toastr.error(data.error, Languages.common_error_title);
						$scope.model.datainit.slbListOrigins = [];
					}
					else if(data.warning != "" && data.warning != null  && data.warning != undefined)
					{
						$scope.model.datainit.slbListOrigins = [];
					}
					else if(data.success != "" && data.success != null  && data.success != undefined)
					{
						$scope.model.datainit.slbListOrigins = data.success;
					}
					else
					{
						if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
						{
							toastr.error(data.systemerror, Languages.common_error_title);
							window.location.replace("./"+data.errorCode);
						}
						else
						{
							toastr.error(Languages.common_error_exception, Languages.common_error_title);
						}
						$scope.model.datainit.slbListOrigins = [];
					}
				},
				function (response) {
		
					var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$scope.model.datainit.slbListOrigins = [];
	    		}
	    	);
	}
	// GetListManufacturer
	$scope.GetListManufacturer = function() {
		$http.post($scope.model.datainit.appURL + "/SystemApi/GetListManufacturer")
	    	.then(
				function (response) {
					var data = response.data;

					if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
					{
						toastr.error(data.messages, Languages.common_error_title);

						if(data.auth != "" && data.auth != null  && data.auth != undefined)
						{
							$window.location.href = $scope.model.datainit.appURL + data.redirect;
						}
					}
					else if(data.error != "" && data.error != null  && data.error != undefined)
					{
						toastr.error(data.error, Languages.common_error_title);
						$scope.model.datainit.slbListManufacturer = [];
					}
					else if(data.warning != "" && data.warning != null  && data.warning != undefined)
					{
						$scope.model.datainit.slbListManufacturer = [];
					}
					else if(data.success != "" && data.success != null  && data.success != undefined)
					{
						$scope.model.datainit.slbListManufacturer = data.success;
					}
					else
					{
						if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
						{
							toastr.error(data.systemerror, Languages.common_error_title);
							window.location.replace("./"+data.errorCode);
						}
						else
						{
							toastr.error(Languages.common_error_exception, Languages.common_error_title);
						}
						$scope.model.datainit.slbListManufacturer = [];
					}
				},
				function (response) {
	
					var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$scope.model.datainit.slbListManufacturer = [];
	    		}
	    	);
	}
	// GetListColor
	$scope.GetListColor = function() {
		$http.post($scope.model.datainit.appURL + "/SystemApi/GetListColor")
	    	.then(
				function (response) {
					var data = response.data;

					if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
					{
						toastr.error(data.messages, Languages.common_error_title);

						if(data.auth != "" && data.auth != null  && data.auth != undefined)
						{
							$window.location.href = $scope.model.datainit.appURL + data.redirect;
						}
					}
					else if(data.error != "" && data.error != null  && data.error != undefined)
					{
						toastr.error(data.error, Languages.common_error_title);
						$scope.model.datainit.slbListColor = [];
					}
					else if(data.warning != "" && data.warning != null  && data.warning != undefined)
					{
						$scope.model.datainit.slbListColor = [];
					}
					else if(data.success != "" && data.success != null  && data.success != undefined)
					{
						$scope.model.datainit.slbListColor = data.success;
					}
					else
					{
						if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
						{
							toastr.error(data.systemerror, Languages.common_error_title);
							window.location.replace("./"+data.errorCode);
						}
						else
						{
							toastr.error(Languages.common_error_exception, Languages.common_error_title);
						}
						$scope.model.datainit.slbListColor = [];
					}
				},
				function (response) {
			
					var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$scope.model.datainit.slbListColor = [];
	    		}
	    	);
	}
	// GetListTag
	$scope.GetListTag = function() {
		$http.post($scope.model.datainit.appURL + "/SystemApi/GetListTag")
	    	.then(
				function (response) {
					var data = response.data;

					if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
					{
					    toastr.error(data.messages, Languages.common_error_title);

					    if(data.auth != "" && data.auth != null  && data.auth != undefined)
					    {
					    	$window.location.href = $scope.model.datainit.appURL + data.redirect;
					    }
					}
					else if(data.error != "" && data.error != null  && data.error != undefined)
					{
						toastr.error(data.error, Languages.common_error_title);
						$scope.model.datainit.slbListTags = [];
					}
					else if(data.warning != "" && data.warning != null  && data.warning != undefined)
					{
						// toastr.warning(data.warning, Languages.common_warning_title);
						$scope.model.datainit.slbListTags = [];
					}
					else if(data.success != "" && data.success != null  && data.success != undefined)
					{
						$scope.model.datainit.slbListTags = data.success;
					}
					else
					{
						if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
						{
							toastr.error(data.systemerror, Languages.common_error_title);
							window.location.replace("./"+data.errorCode);
						}
						else
						{
							toastr.error(Languages.common_error_exception, Languages.common_error_title);
						}
						$scope.model.datainit.slbListTags = [];
					}
				},
				function (response) {
		
					var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$scope.model.datainit.slbListTags = [];
				});
	}
	// GetListProductTag
	$scope.GetProductDetails = function() {
		if($("#txtSlug").val() !== "")
    	{
    		var request = {
		      product_slug : $("#txtSlug").val(),
	    	};
		   	$http.post($scope.model.datainit.appURL + "/SystemApi/GetProductDetails", JSON.stringify(request))
		    	.then(
               		function (response) {
                  		var data = response.data;

				    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
			            {
			                toastr.error(data.messages, Languages.common_error_title);

			                if(data.auth != "" && data.auth != null  && data.auth != undefined)
			                {
			                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
			                }
			            }
				    	else if(data.error != "" && data.error != null  && data.error != undefined)
				    	{
				    		toastr.error(data.error, Languages.common_error_title);
				    	}
				    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
				    	{
				    		toastr.warning(data.warning, Languages.common_warning_title);
				    	}
				    	else if(data.success != "" && data.success != null  && data.success != undefined)
				    	{
				    		console.log(data.success);

				    		$scope.model.request.txtSlug = data.success.product_slug;
							$scope.model.request.dtpImport = moment(data.success.created_date).format('DD/MM/YYYY');
							$scope.model.request.dtpProductPurchase = moment(data.success.product_date_purchase).format('DD/MM/YYYY');
							$scope.model.request.sblStatus = data.success.status;
							$scope.model.request.txtTitle = data.success.product_name;
							$scope.model.request.txtPricesImport = data.success.product_imported_prices;
							$scope.model.request.txtRepairCosts =  data.success.product_repair_costs;
							$scope.model.request.txtRealSalePrices = data.success.product_retail_prices;
							$scope.model.request.slbProductionYear ={ value: data.success.product_year_production };
							$scope.model.request.txtNote = data.success.product_note;
							$scope.model.request.txaContent = data.success.product_description;


							$scope.model.request.txtImages = data.success.product_thumbnail;
							$scope.model.request.txtKeywordSEO = data.success.product_seo_keywords;
						
							var i=1;
							angular.forEach(data.listPhotos, function(value, key) {
								$('#thumbnail_'+i).val(value.photo_url);
								$('#thumbnail_'+i).trigger('input');
								i++;
							});

							var listTags = [];
							angular.forEach(data.listTags, function(value, key) {
						   	listTags.push(value.tag_id);
							});
							$scope.model.request.slbTags = listTags;

							angular.forEach($scope.model.datainit.slbListCategories, function(value, key) {
							    if(value.cate_id == data.success.cate_id)
							    {
							    	value.ticked = true;
							    }
							    else
							    {
							    	value.ticked = false;
							    }
							});

							$('#txtCate').val(data.success.cate_id); 
	   					$('#txtCate').trigger('input');

							$scope.model.datainit.pageStatus = "update";
				    	}
				    	else
				    	{
				    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
				    		{
				    			toastr.error(data.systemerror, Languages.common_error_title);
				    			window.location.replace("./"+data.errorCode);
				    		}
				    		else
				    		{
				    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
				    		}
				    	}

				    },
		    		function (response) {
	          
	                 	var data = response.data;
						toastr.error(Languages.common_error_exception, Languages.common_error_title);
		    		}
		    	);
	    }
	}
	// removeItem
    $scope.removeItem = function(id) {

    	if(id == "" || id == null || id == undefined)
		{
			toastr.error("Vui Lòng Kiểm Tra Thông Tin Trước Khi Xóa", "Cảnh Báo!");
			return false;
		}
    	
		var confirmModal = bootbox.dialog({
			message: commonFunction.languageReplace(Languages.common_confirm_delete_question,[' Bản Tin']),
		  	title: commonFunction.languageReplace(Languages.common_confirm_delete_title,[' Bản Tin']),
		  	closeButton: true,
		  	animate: true,
		  	className: "confirmModal",
		  	backdrop: true,
		  	headerBackground: "bg-red",
		  	size: "small",
		  	locale: "vi",
		  	show: true,
		  	buttons: {
		  		cancel : {
			      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
			      	className: "btn-default",
			      	callback: function() {
			        	confirmModal.modal('hide');
			      	}
			    },
			    success: {
		      		label: '<i class="fa fa-trash" aria-hidden="true"></i>&nbsp;'+Languages.common_button_remove,
			      	className: "btn-danger",
			      	callback: function() {

			      		$("#addReLoading").show();

			        	var request = {
					        news_id : id,
					    };

					    $http.post($scope.model.datainit.appURL + "/SystemApi/RemoveNews", JSON.stringify(request))
					    	.then(
               					function (response) {
                  					var data = response.data;

							    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
						            {
						                toastr.error(data.messages, Languages.common_error_title);

						                if(data.auth != "" && data.auth != null  && data.auth != undefined)
						                {
						                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
						                }
						            }
							    	else if(data.error != "" && data.error != null  && data.error != undefined)
							    	{
							    		toastr.error(data.error, Languages.common_error_title);
							    	}
							    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
							    	{
							    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
							    		warning = warning.replace('["', '').replace('"]', '');
							    		toastr.warning(warning, Languages.common_warning_title);
							    	}
							    	else if(data.success != "" && data.success != null  && data.success != undefined)
							    	{
							    		toastr.success(data.success, commonFunction.languageReplace(Languages.common_success_title,[' ']));

										bootbox.alert(commonFunction.languageReplace(Languages.common_success_deleted,[' Bản Tin']), function() {
											window.location.replace($scope.model.datainit.appURL + "/System/ListNews");
										});

										setTimeout(function(){ 
											window.location.replace($scope.model.datainit.appURL + "/System/ListNews");
										}, 3000);
							    	}
							    	else
							    	{
							    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
							    		{
							    			toastr.error(data.systemerror, Languages.common_error_title);
							    			window.location.replace("./"+data.errorCode);
							    		}
							    		else
							    		{
							    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
							    		}
							    	}
							    	$("#addReLoading").hide();
							    },
					    		function (response) {
				            
				                 	var data = response.data;
									toastr.error(Languages.common_error_exception, Languages.common_error_title);
									$("#addReLoading").hide();
					    		}
					    	);
			      	}
			    }
		  	}
		});
    }
	// formReload
    $scope.formReload = function() {
    	location.reload();
    };
    // formReset
	$scope.formReset = function() {

		$("#pagePreLoading").show();
		$("#addReLoading").show();

		$("#holder").attr("src","");
		$("#thumbnail").val("");

		$("#holder_1").attr("src","");
		$("#thumbnail_1").val("");
		$("#holder_2").attr("src","");
		$("#thumbnail_2").val("");
		$("#holder_3").attr("src","");
		$("#thumbnail_3").val("");
		$("#holder_4").attr("src","");
		$("#thumbnail_4").val("");
		$("#holder_5").attr("src","");
		$("#thumbnail_5").val("");
		$("#holder_6").attr("src","");
		$("#thumbnail_6").val("");

		$scope.model.request = {
			txtProductId : "",
			dtpImport: moment().format('DD/MM/YYYY'),
			dtpProductPurchase: moment().format('DD/MM/YYYY'),
			txtCate : "",
			slbCate : "",
			sblStatus : 1,
			txtTitle : "",
			txtPricesImport : 1000,
			txtRepairCosts : 1000,
			txtRealSalePrices : 1000,
			slbProductionYear : 2017,
			txtNote : "",
			txaContent : "",
			radFormatPage : 1,
			slbTags : [],
			slbRating : 5,
			txtImages : $('meta[name="app_url"]').attr('content')+"/public/images/default-motocycle.png",
			txtKeywordSEO : "",
			listGalleries: {
				txtImage_1 : $('meta[name="app_url"]').attr('content')+"/public/images/default-motocycle.png",
				txtImage_2 : $('meta[name="app_url"]').attr('content')+"/public/images/default-motocycle.png",
				txtImage_3 : $('meta[name="app_url"]').attr('content')+"/public/images/default-motocycle.png",
				txtImage_4 : $('meta[name="app_url"]').attr('content')+"/public/images/default-motocycle.png",
				txtImage_5 : $('meta[name="app_url"]').attr('content')+"/public/images/default-motocycle.png",
				txtImage_6 : $('meta[name="app_url"]').attr('content')+"/public/images/default-motocycle.png",
			},
			txtProductLicense : "",
		};

		angular.forEach($scope.model.datainit.slbListCategories, function(value, key) {
			value.ticked = false;
		});

		$scope.frmAdd.$setPristine();
		$scope.frmAdd.$setUntouched();

		$scope.pageInit();

		$("#addReLoading").hide();

	};
	// addItem
	$scope.addItem = function(status) {
		// check to make sure the form is completely valid
		var confirmModal = bootbox.dialog({
		  	message: commonFunction.languageReplace(Languages.common_confirm_add_question,[' Sản Phẩm']),
		  	title: commonFunction.languageReplace(Languages.common_confirm_add_title,[' Sản Phẩm']),
		  	closeButton: true,
		  	animate: true,
		  	className: "confirmModal",
		  	backdrop: true,
		  	headerBackground: "bg-blue",
		  	size: "small",
		  	locale: "vi",
		  	show: true,
		  	buttons: {
		  		cancel : {
			      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
			      	className: "btn-default",
			      	callback: function() {
			        	confirmModal.modal('hide');
			      	}
			    },
			    success: {
		      		label: '<i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;'+Languages.common_button_add,
			      	className: "btn-primary",
			      	callback: function() {
	
			      		$("#addReLoading").show();
			      		var list_images= $('#thumbnail1').val() +'@'
				      		+ $('#thumbnail2').val()+'@'
				      		+ $('#thumbnail3').val()+'@';
							var request = {
								unit_id : $scope.model.request.txt_unit_id,
								cate_id : $scope.model.request.txt_cate_id,
								origin_id : $scope.model.request.txt_origin_id,
								manufacturer_id : $scope.model.request.txt_manufacturer_id,
								color_id : $scope.model.request.txt_color_id,
								product_name : $scope.model.request.txt_product_name,
								product_description : $scope.model.request.txt_product_description,
								product_specifications : $scope.model.request.a_product_specifications,
								product_imported_prices : $scope.model.request.txt_product_imported_prices,
								product_retail_prices : $scope.model.request.txt_product_retail_prices,
								
								product_ratings : 5,	
								product_thumbnail : $('#thumbnail').val(),
								product_images_list : list_images,
								product_release_date : $scope.model.request.dtpPublishes.startDate,
								product_expiration_date : $scope.model.request.dtpPublishes.endDate,
								product_comment_status : 1,
								product_saleoff_pinned: $scope.model.request.sw_product_saleoff_pinned,
								product_new_pinned: $scope.model.request.sw_product_new_pinned,
								product_quick_pinned: $scope.model.request.sw_product_quick_pinned,
				
								status : status,
								list_tags : $scope.model.request.slbTag
					    	};

					    	$http.post($scope.model.datainit.appURL + "/SystemApi/AddProduct", JSON.stringify(request))
						    	.then(
	            						function (response) {
	               						var data = response.data;

								    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
							            {
							                toastr.error(data.messages, Languages.common_error_title);

							                if(data.auth != "" && data.auth != null  && data.auth != undefined)
							                {
							                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
							                }
							            }
								    	else if(data.error != "" && data.error != null  && data.error != undefined)
								    	{
								    		toastr.error(data.error, Languages.common_error_title);
								    	}
								    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
								    	{
								    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
								    		warning = warning.replace('["', '').replace('"]', '');
								    		toastr.warning(warning, Languages.common_warning_title);
								    	}
								    	else if(data.success != "" && data.success != null  && data.success != undefined)
								    	{
								    		toastr.success(data.success, commonFunction.languageReplace(Languages.common_success_title,[' Sản Phẩm']));

								    		bootbox.alert(commonFunction.languageReplace(Languages.common_success_created,[' Sản Phẩm']), function() {
												window.location.replace($scope.model.datainit.appURL + "/System/ListProducts");
											});

											setTimeout(function(){ 
												window.location.replace($scope.model.datainit.appURL + "/System/ListProducts");
											}, 3000);
								    	}
								    	else
								    	{
								    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
								    		{
								    			toastr.error(data.systemerror, Languages.common_error_title);
								    			window.location.replace("./"+data.errorCode);
								    		}
								    		else
								    		{
								    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
								    		}
								    	}
								    	$("#addReLoading").hide();
								    },
					    			function (response) {
			               
				                 	var data = response.data;
										toastr.error(Languages.common_error_exception, Languages.common_error_title);
										$("#addReLoading").hide();
						    		}
						    	);
	      		}
		    	}
		  	}
		});

	};
	// ===================load Product Detail====================
	$scope.loadProductDetail= function(){
	
		if($("#txtSlug").val() !== "")
	    {
	    	var request = {
		        product_slug : $("#txtSlug").val(),
		    };

		    $http.post($scope.model.datainit.appURL + "/SystemApi/GetProductDetails", JSON.stringify(request))
	    	.then(
               	function (response) {
                  	var data = response.data;

				    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
			            {
			                toastr.error(data.messages, Languages.common_error_title);

			                if(data.auth != "" && data.auth != null  && data.auth != undefined)
			                {
			                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
			                }
			            }
				    	else if(data.error != "" && data.error != null  && data.error != undefined)
				    	{
				    		toastr.error(data.error, Languages.common_error_title);
				    	}
				    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
				    	{
				    		toastr.warning(data.warning, Languages.common_warning_title);
				    	}
				    	else if(data.success != "" && data.success != null  && data.success != undefined)
				    	{
				    		//console.log(data.success);
				   
				    		var listImagesArr = ["","","",""];
							var listImages = data.success.product_images_list;
							if(listImages==null){
								listImages="";
							}
							listImagesArr = listImages.split('@');

							if(listImages != null && listImages != "")
							{
								var listImagesArr = listImages.split('@');
							}

				    		$scope.model.request = {
								txt_cate_id : data.success.cate_id,
								txt_unit_id : data.success.unit_id,
								txt_origin_id : data.success.origin_id,
								txt_manufacturer_id : data.success.manufacturer_id,
								txt_color_id : data.success.color_id,
								txt_tag_id : data.listTags,
								txt_product_name : data.success.product_name,
								txt_product_imported_prices : parseFloat(data.success.product_imported_prices),
								txt_product_retail_prices : parseFloat(data.success.product_retail_prices),
								txt_product_description : data.success.product_description,
								a_product_specifications : data.success.product_specifications,
								dtpPublishes: {
									startDate: data.success.product_release_date, 
									endDate: data.success.product_expiration_date
								},
								slbRating : data.success.product_ratings,
								sbl_status : data.success.status,
								sw_product_comment_status: data.success.product_comment_status,
								sw_product_saleoff_pinned: data.success.product_saleoff_pinned,
								sw_product_new_pinned: data.success.product_new_pinned,
								sw_product_quick_pinned: data.success.product_quick_pinned,
								txtImages : data.success.product_thumbnail,
								txtImages1 : listImagesArr[0],
								txtImages2 : listImagesArr[1],
								txtImages3 : listImagesArr[2],
								txtImages4 : listImagesArr[3],
								txtTitleSEO : data.success.news_seo_title,
								txtKeywordSEO : data.success.news_seo_keywords,
								txtDescriptionSEO : data.success.news_seo_description
							};

							angular.forEach($scope.model.datainit.slbListCategories, function(value, key2) {
							    if(value.cate_id == data.success.cate_id)
							    {
							    	value.ticked = true;
							    }
							});

							angular.forEach($scope.model.datainit.slbListUnits, function(value, key2) {
							    if(value.unit_id == data.success.unit_id)
							    {
							    	value.ticked = true;
							    }
							});

							angular.forEach($scope.model.datainit.slbListOrigins, function(value, key2) {
							    if(value.origin_id == data.success.origin_id)
							    {
							    	value.ticked = true;
							    }
							});

							angular.forEach($scope.model.datainit.slbListManufacturer, function(value, key2) {
							    if(value.manufacturer_id == data.success.manufacturer_id)
							    {
							    	value.ticked = true;
							    }
							});

							angular.forEach($scope.model.datainit.slbListColor, function(value, key2) {
							    if(value.color_id == data.success.color_id)
							    {
							    	value.ticked = true;
							    }
							});
						
							$scope.model.request.slbTags = data.listTags;
							angular.forEach(data.listTags, function(value, key) {
								angular.forEach($scope.model.datainit.slbListTags, function(value2, key2) {
								    if(value.tag_id == value2.tag_id)
								    {
								    	value2.ticked = true;
								    }
								});
							});

							$scope.model.datainit.pageStatus = "update";
				    	}
				    	else
				    	{
				    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
				    		{
				    			toastr.error(data.systemerror, Languages.common_error_title);
				    			window.location.replace("./"+data.errorCode);
				    		}
				    		else
				    		{
				    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
				    		}
				    	}
			    },
               	function (response) {
               
                 	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    }
			);
	    }
	}
	// updateItem
	$scope.updateItem = function(status) {

			var confirmModal = bootbox.dialog({
			  	message: commonFunction.languageReplace(Languages.common_confirm_update_question,[' Sản Phẩm']),
			  	title: commonFunction.languageReplace(Languages.common_confirm_update_title,[' Sản Phẩm']),
			  	closeButton: true,
			  	animate: true,
			  	className: "confirmModal",
			  	backdrop: true,
			  	headerBackground: "bg-yellow",
			  	size: "small",
			  	locale: "vi",
			  	show: true,
			  	buttons: {
			  		cancel : {
				      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
				      	className: "btn-default",
				      	callback: function() {
				        	confirmModal.modal('hide');
				      	}
				    },
				    success: {
			      		label: '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;'+Languages.common_button_update,
				      	className: "btn-warning",
				      	callback: function() {

				      		$("#addReLoading").show();
							//$("#txtSlug").val(),
				        	var list_images= $('#thumbnail1').val() +'@'
				      		+ $('#thumbnail2').val()+'@'
				      		+ $('#thumbnail3').val()+'@';
						    var request = {
						    	product_slug : $("#txtSlug").val(),

						        unit_id : $scope.model.request.txt_unit_id,
								cate_id : $scope.model.request.txt_cate_id,
								origin_id : $scope.model.request.txt_origin_id,
								manufacturer_id : $scope.model.request.txt_manufacturer_id,
								color_id : $scope.model.request.txt_color_id,
								product_name : $scope.model.request.txt_product_name,
								product_description : $scope.model.request.txt_product_description,
								product_specifications : $scope.model.request.a_product_specifications,
								product_imported_prices : $scope.model.request.txt_product_imported_prices,
								product_retail_prices : $scope.model.request.txt_product_retail_prices,
								
								product_ratings : 5,
								product_thumbnail : $('#thumbnail').val(),
								product_images_list : list_images,
								product_release_date : $scope.model.request.dtpPublishes.startDate,
								product_expiration_date : $scope.model.request.dtpPublishes.endDate,
								product_comment_status : 1,
								product_saleoff_pinned: $scope.model.request.sw_product_saleoff_pinned,
								product_new_pinned: $scope.model.request.sw_product_new_pinned,
								product_quick_pinned: $scope.model.request.sw_product_quick_pinned,
								// product_seo_keywords : $scope.model.request.txtKeywordSEO,
								status : status,
								// txtImage_1 : $scope.model.request.listGalleries.txtImage_1,
								// txtImage_2 : $scope.model.request.listGalleries.txtImage_2,
								// txtImage_3 : $scope.model.request.listGalleries.txtImage_3,
								// txtImage_4 : $scope.model.request.listGalleries.txtImage_4,
								// txtImage_5 : $scope.model.request.listGalleries.txtImage_5,
								// txtImage_6 : $scope.model.request.listGalleries.txtImage_6,
								list_tags : $scope.model.request.slbTag
						    };

							$http.post($scope.model.datainit.appURL + "/SystemApi/UpdateProduct",JSON.stringify(request))
					    	.then(
				               	function (response) {
				                  	var data = response.data;

								    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
							            {
							                toastr.error(data.messages, Languages.common_error_title);

							                if(data.auth != "" && data.auth != null  && data.auth != undefined)
							                {
							                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
							                }
							            }
								    	else if(data.error != "" && data.error != null  && data.error != undefined)
								    	{
								    		toastr.error(data.error, Languages.common_error_title);
								    	}
								    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
								    	{
								    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
								    		warning = warning.replace('["', '').replace('"]', '');
								    		toastr.warning(warning, Languages.common_warning_title);
								    	}
								    	else if(data.success != "" && data.success != null  && data.success != undefined)
								    	{
								    		toastr.success(data.success, commonFunction.languageReplace(Languages.common_success_title,[' Sản Phẩm']));

								    		bootbox.alert(commonFunction.languageReplace(Languages.common_success_created,[' Sản Phẩm']), function() {
												window.location.replace($scope.model.datainit.appURL + "/System/ListProducts");
											});

											setTimeout(function(){ 
												window.location.replace($scope.model.datainit.appURL + "/System/ListProducts");
											}, 3000);
								    	}
								    	else
								    	{
								    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
								    		{
								    			toastr.error(data.systemerror, Languages.common_error_title);
								    			window.location.replace("./"+data.errorCode);
								    		}
								    		else
								    		{
								    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
								    		}
								    	}
								    	$("#addReLoading").hide();
							    },
				               	function (response) {
				                  
					                 	var data = response.data;
										toastr.error(Languages.common_error_exception, Languages.common_error_title);
										$("#addReLoading").hide();
							    }
							);
						    
				      	}
				    },
			  	}
			});
	};
	

	//Hàm Chức Năng Cho SelectBox
	$scope.slbClear = function(element)
	{

		if(element == "txtCate")
		{
			angular.forEach($scope.model.datainit.slbListCategories, function(value, key) {
		    	value.ticked = false;
			});
		}
	   	$('#'+element).val(""); 
	   	$('#'+element).trigger('input');
	};

	$scope.slbItemClick = function(item,element)
	{
	   	
		if(element == "txt_cate_id")
		{
			$('#'+element).val(item.cate_id); 
   			$('#'+element).trigger('input');

			angular.forEach($scope.model.datainit.slbListCategories, function(value, key) {
			    if(value.cate_id == item.cate_id)
			    {
			    	value.ticked = true;
			    }
			    else
			    {
			    	value.ticked = false;
			    }
			});
		}
		else if(element == "txt_unit_id"){
			$('#'+element).val(item.unit_id); 
   			$('#'+element).trigger('input');

			angular.forEach($scope.model.datainit.slbListUnits, function(value, key) {
			    if(value.unit_id == item.unit_id)
			    {
			    	value.ticked = true;
			    }
			    else
			    {
			    	value.ticked = false;
			    }
			});
		}
		else if(element == "txt_origin_id"){
			$('#'+element).val(item.origin_id); 
   			$('#'+element).trigger('input');

			angular.forEach($scope.model.datainit.slbListOrigins, function(value, key) {
			    if(value.origin_id == item.origin_id)
			    {
			    	value.ticked = true;
			    }
			    else
			    {
			    	value.ticked = false;
			    }
			});
		}
		else if(element == "txt_manufacturer_id"){
			$('#'+element).val(item.manufacturer_id); 
   			$('#'+element).trigger('input');

			angular.forEach($scope.model.datainit.slbListManufacturer, function(value, key) {
			    if(value.manufacturer_id == item.manufacturer_id)
			    {
			    	value.ticked = true;
			    }
			    else
			    {
			    	value.ticked = false;
			    }
			});
		}
		else if(element == "txt_color_id"){
			$('#'+element).val(item.color_id); 
   			$('#'+element).trigger('input');

			angular.forEach($scope.model.datainit.slbListColor, function(value, key) {
			    if(value.color_id == item.color_id)
			    {
			    	value.ticked = true;
			    }
			    else
			    {
			    	value.ticked = false;
			    }
			});
		}
	};

});