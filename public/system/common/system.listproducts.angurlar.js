/*!
 *
 *	Develop by Vinh Nguyễn - Ho Chi Minh City - 2017
 * 	Copyright Â© 2016-2017 Skyfire Team. All rights reserved.
 *	Design by Skyfire Team 2.3.3
 *
 *	Phone: 0975 578 276
 *	Skype: enjoyvinh
 *
 */

"use strict";

// app.js
// create angular app
var bikeApp = angular.module('bikeApp', ['commonApp', 'daterangepicker', 'isteven-multi-select', 'ngSanitize',
		'datatables','datatables.colreorder','datatables.bootstrap']);

bikeApp.factory('BikeModels', function() {

	var model={
		datainit : {
			slbListCategories : [],
			dtgList : [],
			appURL : "http://localhost:8000"
		},
	};

	model.request = {
		dtpPublishes: {
			startDate: moment().subtract(3,"year").startOf('month'), 
			endDate: moment()
		},
		txtNewsName : "",
		slbCateParent : "",
	};

	model.datePickerOpts = {
		showDropdowns : true,
		singleDatePicker : false,
		timePicker : false,
		timePickerSeconds : false,
		timePicker24Hour : true,
		alwaysShowCalendars : false,
		showWeekNumbers : true,
		autoApply : true,
		opens: "right",
		drops: "down",
		clearLabel: Languages.common_button_cancel,
		cancelClass : 'btn-default',
		autoUpdateInput : true,
	    parentEl: '.cal-block .form-group .oneway',
        locale: {
            buttonClasses : 'btn btn-sm',
	        applyClass : 'btn-success',
            applyLabel: Languages.common_button_select_item,
            fromLabel: Languages.common_label_from,
            format: "DD/MM/YYYY",
            toLabel: Languages.common_label_to,
            weekLabel: Languages.datepicker_week_title,
            customRangeLabel: Languages.common_button_select_date_time,
            daysOfWeek: [
            				Languages.datepicker_week_monday,
            				Languages.datepicker_week_tuesday,
            				Languages.datepicker_week_wednesday,
            				Languages.datepicker_week_thursday,
            				Languages.datepicker_week_friday,
            				Languages.datepicker_week_saturday,
            				Languages.datepicker_week_sunday
            			],
            monthNames: [
	            			Languages.datepicker_month_january, 
	            			Languages.datepicker_month_february,
	            			Languages.datepicker_month_march,
	            			Languages.datepicker_month_april,
	            			Languages.datepicker_month_may,
	            			Languages.datepicker_month_june,
	            			Languages.datepicker_month_july,
	            			Languages.datepicker_month_august,
	            			Languages.datepicker_month_september,
	            			Languages.datepicker_month_october,	
	            			Languages.datepicker_month_november,
	            			Languages.datepicker_month_december
            			],
        },
        ranges: {
   			'Hôm Nay': [moment(), moment()],
			'Từ Hôm Qua': [moment().subtract(1,'days'), moment()],
			'Từ 7 Ngày Trước': [moment().subtract(7,'days'), moment()],
			'Tháng Này': [moment().startOf('month'), moment()],
			'Từ 1 Tháng Trước': [moment().subtract(1,'month').startOf('month'), moment()],
			'Từ 3 Tháng Trước': [moment().subtract(3,'month').startOf('month'), moment()]
        },
        
    };

	model.localLang = {
	    selectAll       : Languages.common_button_select_all,
	    selectNone      : Languages.common_button_deselected_all,
	    reset           : Languages.common_button_cancel,
	    search          : Languages.common_input_label_search,
	    nothingSelected : Languages.common_input_label_select        //default-label is deprecated and replaced with this.
	}

	return model;
});

// create angular controller
bikeApp.controller('bikeController', function(BikeModels, commonFunction, $scope, $rootScope, $http, $filter, $window, $location, toastr,
	DTOptionsBuilder, DTColumnDefBuilder,DTColumnBuilder) {

	$scope.model = BikeModels;
	// pageInit
	$scope.pageInit = function() {

		$scope.model.datainit.appURL = $('meta[name="app_url"]').attr('content');

		$("#listReLoading").show();

		$scope.loadList();

		$scope.dtOptions = DTOptionsBuilder.newOptions()
			.withOption('order', [])
        	.withPaginationType('full_numbers')
        	.withDisplayLength(10)
	        .withColReorder()
	        // .withColReorderOrder([0, 1, 2, 3, 4])
	        .withBootstrap()
	        .withBootstrapOptions({
	            pagination: {
	                classes: {
	                    ul: 'pagination pagination-md'
	                }
	            }
	        })
	        .withLanguage({
	            "sEmptyTable":     "Không có dữ liệu.",
	            "sInfo":           "Hiển Thị Từ _START_ Đến _END_ Của _TOTAL_ Kết Quả",
	            "sInfoEmpty":      "Hiển Thị Từ 0 Đến 0 Của 0 Kết Quả",
	            "sInfoFiltered":   "(Đã Lọc Từ Tổng _MAX_ Kết Quả)",
	            "sInfoPostFix":    "",
	            "sInfoThousands":  ",",
	            "sLengthMenu":     "Hiển Thị&nbsp;&nbsp;&nbsp;_MENU_&nbsp;&nbsp;&nbsp;Kết Quả / 1 Trang",
	            "sLoadingRecords": "Đang Tải Dữ Liệu...",
	            "sProcessing":     "Đang Xử Lý Dữ Liệu...",
	            "sSearch":         "Từ Khóa: ",
	            "sZeroRecords":    "Không tìm thấy dữ liệu trùng khớp.",
	            "oPaginate": {
	                "sFirst":    "Đầu",
	                "sLast":     "Cuối",
	                "sNext":     "Tiếp",
	                "sPrevious": "Trước"
	            },
	            "oAria": {
	                "sSortAscending":  ": activate to sort column ascending",
	                "sSortDescending": ": activate to sort column descending"
	            }
	        })
	        .withOption('responsive', true);

	    $scope.dtColumnDefs = [
	        DTColumnDefBuilder.newColumnDef(0).notSortable(),
	    ];
	};
	// loadList
	$scope.loadList = function() {

		$("#listReLoading").show();

		var request = {
	    };

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetListProducts", JSON.stringify(request))
	    .then(
	    		function (response) {
                
	                var data = response.data;

			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    		$scope.model.datainit.dtgList = [];
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
			    		warning = warning.replace('["', '').replace('"]', '');
			    		toastr.warning(warning, Languages.common_warning_title);
			    		$scope.model.datainit.dtgList = [];
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		$scope.model.datainit.dtgList = data.success;
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    		$scope.model.datainit.dtgList = [];
			    	}
		    		$("#listReLoading").hide();
			    },
               	function (response) {
               
                  	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$scope.model.datainit.dtgList = [];
					$("#listReLoading").hide();
			    }
		    );
		    //end then
	}
	// removeItem
	//delete product
	$scope.removeItem = function(id) {

    	if(id == "" || id == null || id == undefined)
		{
			toastr.error(Resources.common_error_exception, Resources.common_error_title);
			return false;
		}
    	
		var confirmModal = bootbox.dialog({
			message: commonFunction.languageReplace(Languages.common_confirm_delete_question,[' Sản Phẩm']),
		  	title: commonFunction.languageReplace(Languages.common_confirm_delete_title,[' Sản Phẩm']),
		  	closeButton: true,
		  	animate: true,
		  	className: "confirmModal",
		  	backdrop: true,
		  	headerBackground: "bg-red",
		  	size: "small",
		  	locale: "vi",
		  	show: true,
		  	buttons: {
		  		cancel : {
			      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
			      	className: "btn-default",
			      	callback: function() {
			        	confirmModal.modal('hide');
			      	}
			    },
			    success: {
		      		label: '<i class="fa fa-trash" aria-hidden="true"></i>&nbsp;'+Languages.common_button_remove,
			      	className: "btn-danger",
			      	callback: function() {

			      		$("#addReLoading").show();

			        	var request = {
					        product_id : id,
					    };

					    $http.post($scope.model.datainit.appURL + "/SystemApi/RemoveProduct", JSON.stringify(request))
					    	.then(
               					function (response) {
                  					var data = response.data;

							    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
						            {
						                toastr.error(data.messages, Languages.common_error_title);

						                if(data.auth != "" && data.auth != null  && data.auth != undefined)
						                {
						                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
						                }
						            }
							    	else if(data.error != "" && data.error != null  && data.error != undefined)
							    	{
							    		toastr.error(data.error, Languages.common_error_title);
							    	}
							    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
							    	{
							    		toastr.warning(data.warning, Languages.common_warning_title);
							    	}
							    	else if(data.success != "" && data.success != null  && data.success != undefined)
							    	{
							    		toastr.success(data.success, commonFunction.languageReplace(Languages.common_success_removed,['Sản Phẩm']));
							    		$scope.loadList();
							    	}
							    	else
							    	{
							    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
							    		{
							    			toastr.error(data.systemerror, Languages.common_error_title);
							    			window.location.replace("./"+data.errorCode);
							    		}
							    		else
							    		{
							    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
							    		}
							    	}
							    	$("#addReLoading").hide();
					    		},
					    		function (response) {
				           
				                  	var data = response.data;	
									toastr.error(Languages.common_error_exception, Languages.common_error_title);
									$("#addReLoading").hide();
					    		}
					    	);
			      	}
			    }
		  	}
		});
    };
    // changeStatus
   	$scope.changeStatus = function(id,status) {

		if(id == "" || id == null || id == undefined)
		{
			toastr.error(Resources.common_error_exception, Resources.common_error_title);
			return false;
		}
			
		var confirmModal = bootbox.dialog({
		  	message: status == 0 ? "Bạn Có Muốn Ẩn Sản Phẩm?" : "Bạn Có Muốn Hiện Sản Phẩm?" ,
		  	title: "Đổi Trạng Thái Sản Phẩm",
		  	closeButton: true,
		  	animate: true,
		  	className: "confirmModal",
		  	backdrop: true,
		  	headerBackground: "bg-yellow",
		  	size: "small",
		  	locale: "vi",
		  	show: true,
		  	buttons: {
		  		cancel : {
			      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
			      	className: "btn-default",
			      	callback: function() {
			        	confirmModal.modal('hide');
			      	}
			    },
			    success: {
		      		label: '<i class="fa fa-trash" aria-hidden="true"></i>&nbsp;'+Languages.common_button_update,
			      	className: "btn-warning",
			      	callback: function() {

			      		$("#listReLoading").show();

						//Khởi Tạo Dữ Liệu
						var request = {
					        product_id : id,
					        status : status
					    };

					    $http.post($scope.model.datainit.appURL + "/SystemApi/ChangeStatusProduct", JSON.stringify(request))
					    .then(

					    	function (response) {
                
				                var data = response.data;

						    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
					            {
					                toastr.error(data.messages, Languages.common_error_title);

					                if(data.auth != "" && data.auth != null  && data.auth != undefined)
					                {
					                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
					                }
					            }
						    	else if(data.error != "" && data.error != null  && data.error != undefined)
						    	{
						    		toastr.error(data.error, Languages.common_error_title);
						    		$scope.model.datainit.dtgList = [];
						    	}
						    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
						    	{
						    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
						    		warning = warning.replace('["', '').replace('"]', '');
						    		toastr.warning(warning, Languages.common_warning_title);
						    		$scope.model.datainit.dtgList = [];
						    	}
						    	else if(data.success != "" && data.success != null  && data.success != undefined)
						    	{
				    				toastr.success(data.success, commonFunction.languageReplace(Languages.common_success_updated,['Trạng Thái']));
						    		$scope.loadList();
						    	}
						    	else
						    	{
						    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
						    		{
						    			toastr.error(data.systemerror, Languages.common_error_title);
						    			window.location.replace("./"+data.errorCode);
						    		}
						    		else
						    		{
						    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
						    		}
						    		$scope.model.datainit.dtgList = [];
						    	}
					    		$("#listReLoading").hide();
						    },
			               	function (response) {
			              
			                  	var data = response.data;
								toastr.error(Languages.common_error_exception, Languages.common_error_title);
								$scope.model.datainit.dtgList = [];
								$("#listReLoading").hide();
						    }
				    	);
					    // end then
								 
			      	}
			    },
		  	}
		});
	};
	// end function
});