/*!
 *
 *	Develop by Vinh Nguyễn - Ho Chi Minh City - 2017
 * 	Copyright Â© 2016-2017 Skyfire Team. All rights reserved.
 *	Design by Skyfire Team 2.3.3
 *
 *	Phone: 0975 578 276
 *	Skype: enjoyvinh
 *
 */

"use strict";

var createCustomerApp = angular.module('createCustomerApp', ['commonApp', 'daterangepicker','bootstrap-switch','isteven-multi-select','ui.tinymce', 'ngAnimate', 'ngSanitize',
	'icheckAngular']);

createCustomerApp.factory('NewsModels', function() {
	var model={
		datainit : {
			pageModalStatus:"add",
			pageStatus : "add",
		    appURL : "http://localhost:8000"
		},
	};

	model.request = {
		txtCustomerDistrict:"1",
		txtCustomerId:"",
		txtCustomerEmail:"",
		txtCustomerPhone:"",
		txtCustomerGroup:"",
		txtCustomerName:"",
		txtCustomerAddress:"",

		txtCustomerIdentify:"",
		txtCustomerStudentCard:"",
		txtCustomerUniversity:"",

		txtCustomerAccount:"",
		pageStatus:""
	};


	model.localLang = {
	    selectAll       : Languages.common_button_select_all,
	    selectNone      : Languages.common_button_deselected_all,
	    reset           : Languages.common_button_cancel,
	    search          : Languages.common_input_label_search,
	    nothingSelected : Languages.common_input_label_select
	}

	return model;
});

// create angular controller
createCustomerApp.controller('createCustomerController', function(NewsModels, commonFunction, $scope, $rootScope, $http, $filter, $window, $location, $uibModal, toastr) {

	$scope.model = NewsModels;

	$scope.pageInit = function() {

		$scope.model.datainit.appURL = $('meta[name="app_url"]').attr('content');

		$("#addReLoading").show();
		$scope.loadCustomerDetail();
	};
	//=============generate code==============
	$scope.generateCode= function(){
		var currentdate = new Date(); 
    	var datetime =String(currentdate.getDate())
                + String((currentdate.getMonth()+1))
                + String(currentdate.getFullYear())
                + String(currentdate.getHours())
                + String(currentdate.getMinutes())
                + String(currentdate.getSeconds());
        return datetime;
	}
	// ===================load Detail====================
	$scope.loadCustomerDetail= function(){
		if($("#txtId").val() !== "")
	    {
	    	var request = {
		        customer_id : $("#txtId").val(),
		    };

		    $http.post($scope.model.datainit.appURL + "/SystemApi/GetCustomerDetails", JSON.stringify(request))
	    	.then(
               	function (response) {
                  	var data = response.data;

				    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
			            {
			                toastr.error(data.messages, Languages.common_error_title);

			                if(data.auth != "" && data.auth != null  && data.auth != undefined)
			                {
			                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
			                }
			            }
				    	else if(data.error != "" && data.error != null  && data.error != undefined)
				    	{
				    		toastr.error(data.error, Languages.common_error_title);
				    	}
				    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
				    	{
				    		toastr.warning(data.warning, Languages.common_warning_title);
				    	}
				    	else if(data.success != "" && data.success != null  && data.success != undefined)
				    	{
				    		$scope.model.request = {
								txtCustomerId: data.success.customer_id,
								txtCustomerEmail: data.success.customer_email,
								txtCustomerPhone: data.success.customer_phone,
								txtCustomerName: data.success.customer_fullname,
								txtCustomerAddress: data.success.customer_address,
								txtCustomerAccount: data.success.customer_account,

								txtImages:data.success.customer_avatar,

								txtCustomerIdentify: data.success.customer_identify,
								txtCustomerStudentCard: data.success.customer_student_card,
								txtCustomerUniversity: data.success.customer_university,
							};

							$scope.model.datainit.pageStatus = "update";
				    	}
				    	else
				    	{
				    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
				    		{
				    			toastr.error(data.systemerror, Languages.common_error_title);
				    			window.location.replace("./"+data.errorCode);
				    		}
				    		else
				    		{
				    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
				    		}
				    	}
			    },
               	function (response) {
                 	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    }
			);
	    }
	}
	//=====================updateItem=======================
	$scope.updateItem = function(status) {
		// alert($("#thumbnail").val());
			var confirmModal = bootbox.dialog({
			  	message: commonFunction.languageReplace(Languages.common_confirm_update_question,[' Khách Hàng']),
			  	title: commonFunction.languageReplace(Languages.common_confirm_update_title,[' Khách Hàng']),
			  	closeButton: true,
			  	animate: true,
			  	className: "confirmModal",
			  	backdrop: true,
			  	headerBackground: "bg-yellow",
			  	size: "small",
			  	locale: "vi",
			  	show: true,
			  	buttons: {
			  		cancel : {
				      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
				      	className: "btn-default",
				      	callback: function() {
				        	confirmModal.modal('hide');
				      	}
				    },
				    success: {
			      		label: '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;'+Languages.common_button_update,
				      	className: "btn-warning",
				      	callback: function() {
				      		$("#addReLoading").show();
						    var request = {
						    	customer_id : $scope.model.request.txtCustomerId,
						        group_id : 1,
						        area_id: 1,
								customer_email: $scope.model.request.txtCustomerEmail,
								customer_fullname: $scope.model.request.txtCustomerName,
								customer_address: $scope.model.request.txtCustomerAddress,
								customer_phone: $scope.model.request.txtCustomerPhone,

								customer_avatar: $("#thumbnail").val(),
								customer_identify: $scope.model.request.txtCustomerIdentify,
								customer_student_card: $scope.model.request.txtCustomerStudentCard,
								customer_university: $scope.model.request.txtCustomerUniversity,
						    };

							$http.post($scope.model.datainit.appURL + "/SystemApi/UpdateCustommer",JSON.stringify(request))
					    	.then(
				               	function (response) {
				                  	var data = response.data;

								    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
							            {
							                toastr.error(data.messages, Languages.common_error_title);

							                if(data.auth != "" && data.auth != null  && data.auth != undefined)
							                {
							                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
							                }
							            }
								    	else if(data.error != "" && data.error != null  && data.error != undefined)
								    	{
								    		toastr.error(data.error, Languages.common_error_title);
								    	}
								    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
								    	{
								    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
								    		warning = warning.replace('["', '').replace('"]', '');
								    		toastr.warning(warning, Languages.common_warning_title);
								    	}
								    	else if(data.success != "" && data.success != null  && data.success != undefined)
								    	{
								    		toastr.success(data.success, commonFunction.languageReplace(Languages.common_success_title,[' Khách Hàng']));

								    		bootbox.alert(commonFunction.languageReplace(Languages.common_success_created,[' Khách Hàng']), function() {
												window.location.replace($scope.model.datainit.appURL + "/System/ListCustomers");
											});

											setTimeout(function(){ 
												window.location.replace($scope.model.datainit.appURL + "/System/ListCustomers");
											}, 3000);
								    	}
								    	else
								    	{
								    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
								    		{
								    			toastr.error(data.systemerror, Languages.common_error_title);
								    			window.location.replace("./"+data.errorCode);
								    		}
								    		else
								    		{
								    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
								    		}
								    	}
								    	$("#addReLoading").hide();
							    },
				               	function (response) {
					                 	var data = response.data;
										toastr.error(Languages.common_error_exception, Languages.common_error_title);
										$("#addReLoading").hide();
							    }
							);
						    
				      	}
				    },
			  	}
			});
	};
	//=========================form Reset======================
	$scope.formReset = function() {

		$("#holder").attr("src","");
		$("#thumbnail").val("");

		$scope.model.request = {
			txtCustomerId:"",
			txtCustomerEmail:"",
			txtCustomerPhone:"",
			txtCustomerGroup:"",
			txtCustomerName:"",
			txtCustomerAddress:"",
			txtCustomerAccount:"",
			txtCustomerPassword : "",
			txtCustomerPasswordAgain : "",
			txtImages : "/public/UserPhotos/public/UserPhotos/SharePhotos/58b989e59fa96.png"
		};

		angular.forEach($scope.model.datainit.slbCustomerGroup, function(value, key) {
			value.ticked = false;
		});

		angular.forEach($scope.model.datainit.slbCustomerDistrict, function(value, key) {
			value.ticked = false;
		});
		$scope.frmAdd.$setPristine();
		$scope.frmAdd.$setUntouched();

		$scope.pageInit();

		//$("#addReLoading").hide();
	};
	//===============Hàm Chức Năng Cho SelectBox=====================
	$scope.slbClear = function(element)
	{

		if(element == "txtCustomerGroup")
		{
			angular.forEach($scope.model.datainit.slbCustomerGroup, function(value, key) {
		    	value.ticked = false;
			});
		}
		
		if(element == "txtCustomerDistrict")
		{
			angular.forEach($scope.model.datainit.slbCustomerDistrict, function(value, key) {
		    	value.ticked = false;
			});
		}
	   	$('#'+element).val(""); 
	   	$('#'+element).trigger('input');
	};
});