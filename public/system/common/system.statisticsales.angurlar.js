/*!
 *
 *	Develop by Vinh Nguyễn - Ho Chi Minh City - 2017
 * 	Copyright Â© 2016-2017 Skyfire Team. All rights reserved.
 *	Design by Skyfire Team 2.3.3
 *
 *	Phone: 0975 578 276
 *	Skype: enjoyvinh
 *
 */

"use strict";

var statisticAccessApp = angular.module('statisticAccessApp', ['commonApp','datatables','datatables.colreorder','datatables.bootstrap']);

statisticAccessApp.factory('statisticAccessModels', function() {

	var model={
		datainit : {
			salesChartOptions : {
				//Boolean - If we should show the scale at all
				showScale: true,
				//Boolean - Whether grid lines are shown across the chart
				scaleShowGridLines: true,
				//String - Colour of the grid lines
				scaleGridLineColor: "rgba(0,0,0,.05)",
				//Number - Width of the grid lines
				scaleGridLineWidth: 2,
				//Boolean - Whether to show horizontal lines (except X axis)
				scaleShowHorizontalLines: true,
				//Boolean - Whether to show vertical lines (except Y axis)
				scaleShowVerticalLines: true,
				//Boolean - Whether the line is curved between points
				bezierCurve: true,
				//Number - Tension of the bezier curve between points 0.3
				bezierCurveTension: 0.2,
				//Boolean - Whether to show a dot for each point
				pointDot: true,
				//Number - Radius of each point dot in pixels
				pointDotRadius: 3,
				//Number - Pixel width of point dot stroke
				pointDotStrokeWidth: 1,
				//Number - amount extra to add to the radius to cater for hit detection outside the drawn point
				pointHitDetectionRadius: 50,
				//Boolean - Whether to show a stroke for datasets
				datasetStroke: true,
				//Number - Pixel width of dataset stroke
				datasetStrokeWidth: 2,
				//Boolean - Whether to fill the dataset with a color
				datasetFill: true,
				//String - A legend template
				legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%=datasets[i].label%></li><%}%></ul>",
				//Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
				maintainAspectRatio: true,
				//Boolean - whether to make the chart responsive to window resizing
				responsive: true
		   	},
		   	barChartOptions : {
				//Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
				scaleBeginAtZero: true,
				//Boolean - Whether grid lines are shown across the chart
				scaleShowGridLines: true,
				//String - Colour of the grid lines
				scaleGridLineColor: "rgba(0,0,0,.1)",
				//Number - Width of the grid lines
				scaleGridLineWidth: 1,
				//Boolean - Whether to show horizontal lines (except X axis)
				scaleShowHorizontalLines: true,
				//Boolean - Whether to show vertical lines (except Y axis)
				scaleShowVerticalLines: true,
				//Boolean - If there is a stroke on each bar
				barShowStroke: true,
				//Number - Pixel width of the bar stroke
				barStrokeWidth: 2,
				//Number - Spacing between each of the X value sets
				barValueSpacing: 1,
				//Number - Spacing between data sets within X values
				barDatasetSpacing: 10,
				//String - A legend template
				legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
				//Boolean - whether to make the chart responsive
				responsive: true,
				maintainAspectRatio: true,
				//Boolean - Whether to fill the dataset with a color
				datasetFill: true,

				// String - Template string for single tooltips
				tooltipTemplate: "<%if (label){%> Ngày <%=label%> có <%}%><%= value %> Lượt Truy Cập.",
				// Boolean - Determines whether to draw tooltips on the canvas or not
			    // showTooltips: true,
			    // Array - Array of string names to attach tooltip events
			    // tooltipEvents: ["mousemove", "touchstart", "touchmove"],
			    // String - Tooltip background colour
			    tooltipFillColor: "rgba(0,0,0,1)",
			    // String - Tooltip label font declaration for the scale label
			    tooltipFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
			    // Number - Tooltip label font size in pixels
			    tooltipFontSize: 14,
			    // String - Tooltip font weight style
			    tooltipFontStyle: "normal",
			    // String - Tooltip label font colour
			    tooltipFontColor: "#fff",
			    // String - Tooltip title font declaration for the scale label
			    tooltipTitleFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
			    // Number - Tooltip title font size in pixels
			    tooltipTitleFontSize: 14,
			    // String - Tooltip title font weight style
			    tooltipTitleFontStyle: "bold",
			    // String - Tooltip title font colour
			    tooltipTitleFontColor: "#fff",
			    // Number - pixel width of padding around tooltip text
			    // tooltipYPadding: 6,
			    // Number - pixel width of padding around tooltip text
			    // tooltipXPadding: 6,
			    // Number - Size of the caret on the tooltip
			    // tooltipCaretSize: 8,
			    // Number - Pixel radius of the tooltip border
			    // tooltipCornerRadius: 6,
			    // Number - Pixel offset from point x to tooltip edge
			    // tooltipXOffset: 10,
			    // String - Template string for single tooltips
			    // multiTooltipTemplate: "<%= value %>",
		    },
		    preLoading : true,
			mapPreLoading : true,
			dtgListIP : [],
			appURL : "http://localhost:8000"
		},
		
	};

	model.request = {
		currentMonth : moment(),
	};

	return model;
});

// create angular controller
statisticAccessApp.controller('statisticAccessController', function(statisticAccessModels, commonFunction, $scope, $rootScope, $http, $filter, $window, $location, $timeout, 
	toastr,	DTOptionsBuilder, DTColumnDefBuilder,DTColumnBuilder) {

	$scope.model = statisticAccessModels;

	$scope.pageInit = function() {

		$scope.model.datainit.appURL = $('meta[name="app_url"]').attr('content');

		$scope.loadChartByMonth();

		$scope.loadMapVector();

	};

	$scope.previousMonth = function() {
		$scope.model.datainit.preLoading = true;
		var cd = $scope.model.request.currentMonth.subtract(1, 'months');
		$scope.model.request.currentMonth = cd;
		$scope.loadChartByMonth(cd);
	}

	$scope.nextMonth = function() {
		$scope.model.datainit.preLoading = true;
		var cd = $scope.model.request.currentMonth.add(1, 'months');
		$scope.model.request.currentMonth = cd;
		$scope.loadChartByMonth(cd);
	}

	$scope.reloadMonth = function() {
		$scope.model.datainit.preLoading = true;
		$scope.model.request.currentMonth = moment();
		$scope.loadChartByMonth($scope.model.request.currentMonth);
	}

	$scope.loadChartByMonth = function(current) {

		$scope.model.datainit.preLoading = true;

		$("#chartContent").html('<canvas id="charByMonth" style="height: 480px;"></canvas>');

		$scope.barChartCanvas = $("#charByMonth").get(0).getContext("2d");

		if(current === undefined)
		{
			current = moment();
		}

	    var currentMonth = current.month() +　1;

	    var currentYear = current.year();

	    var leftChartTitle = "Trong tháng "+ currentMonth +" năm "+currentYear;
	    $("#leftChartTitle").html(leftChartTitle);

	    var firstDay = new Date(currentYear, currentMonth, 1);
		var lastDay = new Date(currentYear, currentMonth, 0);

		var listLabel = [];

		$scope.listData = [];

		for (var i = firstDay.getDate(); i <= lastDay.getDate(); i++) {
		    listLabel.push(i);
		}

		var request = {
			type : "m",
	    	firstDay : firstDay.getDate(),
	    	lastDay : lastDay.getDate(),
	    	month : currentMonth,
	    	year : currentYear,
	    };

		$http.post($scope.model.datainit.appURL + "/SystemApi/GetNumeral", JSON.stringify(request))
	    	.then(
           		function (response) {
              		var data = response.data;

			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, "Lỗi Hệ Thống");
			    		$scope.model.datainit.dtgListNews = [];
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		toastr.warning(data.warning, Languages.common_warning_title);
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		$scope.listData = data.success;

			    		var totalCounter = 0;
						for (var i = 0; i < $scope.listData.length; i++) {
						    totalCounter += $scope.listData[i];
						}

						var rightChartTitle = "có " + totalCounter + " lượt truy cập";
			    		$("#rightChartTitle").html(rightChartTitle);

					    $scope.barChart = new Chart($scope.barChartCanvas);

					    var barChartData = {
							labels: listLabel,
							datasets: [
								{
									label: "Lượt Truy Cập",
									fillColor: "rgba(26,179,148,0.5)",
									strokeColor: "rgba(26,179,148,1)",
									pointColor: "#3b8bba",
									pointStrokeColor: "rgba(60,141,188,1)",
									pointHighlightFill: "#fff",
									pointHighlightStroke: "rgba(60,141,188,1)",
									data: $scope.listData
								},
							],
						};

						$timeout(function() {
							$scope.barChart.Bar(barChartData, $scope.model.datainit.barChartOptions); 
							$scope.model.datainit.preLoading = false;  
						}, 500);
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    	}
			    },
               	function (response) {
                  	debugger;
                  	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
	    		}
	    	);
	};


	$scope.loadMapVector = function() {

		$http.post($scope.model.datainit.appURL + "/SystemApi/GetMapVector")
	    	.then(
           		function (response) {
              		var data = response.data;

			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		toastr.warning(data.warning, Languages.common_warning_title);
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{

			    		$("#parrentWorldMap").html('<div id="world-map-markers" style="height: 480px;"></div>');

			    		var markersData = [];

			    		$scope.model.datainit.dtgListIP = data.success[0];

			    		$.each(data.success[0], function( k, v ) {
						  	var item = {
						  		latLng: [v.counter_latitude, v.counter_longitude], 
						  		name: v.counter_region_name
						  	}
						  	markersData.push(item);
						});


			    		$('#world-map-markers').vectorMap({
							map: 'world_mill_en',
							normalizeFunction: 'polynomial',
							hoverOpacity: 0.7,
							hoverColor: true,
							backgroundColor: '#4f98c3',
							regionStyle: {
								initial: {
									fill: 'rgba(210, 214, 222, 1)',
									"fill-opacity": 1,
									stroke: 'none',
									"stroke-width": 0,
									"stroke-opacity": 1
								},
								hover: {
									"fill-opacity": 0.7,
									cursor: 'pointer'
								},
								selected: {
									fill: 'yellow'
								},
									selectedHover: {}
								},
								markerStyle: {
									initial: {
									fill: '#AA0000',
									stroke: '#ffffff'
								}
							},
							markers : markersData,
						});
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    	}

			    	$scope.model.datainit.mapPreLoading = false;
			    },
               	function (response) {
              		debugger;
                  	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
	    		}
	    	);
	};

	$scope.dtOptions = DTOptionsBuilder.newOptions()
		.withOption('order', [6, 'desc'])
		.withPaginationType('full_numbers')
		.withDisplayLength(10)
	    .withColReorder()
	    // .withColReorderOrder([0, 1, 2, 3, 4])
	    .withBootstrap()
	    .withBootstrapOptions({
	        pagination: {
	            classes: {
	                ul: 'pagination pagination-md'
	            }
	        }
	    })
	    .withLanguage({
	        "sEmptyTable":     "Không có dữ liệu.",
	        "sInfo":           "Hiển Thị Từ _START_ Đến _END_ Của _TOTAL_ Kết Quả",
	        "sInfoEmpty":      "Hiển Thị Từ 0 Đến 0 Của 0 Kết Quả",
	        "sInfoFiltered":   "(Đã Lọc Từ Tổng _MAX_ Kết Quả)",
	        "sInfoPostFix":    "",
	        "sInfoThousands":  ",",
	        "sLengthMenu":     "Hiển Thị&nbsp;&nbsp;&nbsp;_MENU_&nbsp;&nbsp;&nbsp;Kết Quả / 1 Trang",
	        "sLoadingRecords": "Đang Tải Dữ Liệu...",
	        "sProcessing":     "Đang Xử Lý Dữ Liệu...",
	        "sSearch":         "Từ Khóa: ",
	        "sZeroRecords":    "Không tìm thấy dữ liệu trùng khớp.",
	        "oPaginate": {
	            "sFirst":    "Đầu",
	            "sLast":     "Cuối",
	            "sNext":     "Tiếp",
	            "sPrevious": "Trước"
	        },
	        "oAria": {
	            "sSortAscending":  ": activate to sort column ascending",
	            "sSortDescending": ": activate to sort column descending"
	        }
	    })
	    .withOption('responsive', false);

});