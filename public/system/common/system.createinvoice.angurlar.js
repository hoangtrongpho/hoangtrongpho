/*!
 *
 *	Develop by Vinh Nguyễn - Ho Chi Minh City - 2017
 * 	Copyright Â© 2016-2017 Skyfire Team. All rights reserved.
 *	Design by Skyfire Team 2.3.3
 *
 *	Phone: 0975 578 276
 *	Skype: enjoyvinh
 *
 */

"use strict";

// app.js
// create angular app
var createInvocieApp = angular.module('createInvocieApp', ['commonApp','ui.bootstrap.datetimepicker', 'daterangepicker','bootstrap-switch','isteven-multi-select','ui.tinymce', 'ngAnimate', 'ngSanitize',
	'icheckAngular','datatables','datatables.colreorder','datatables.bootstrap']);

createInvocieApp.factory('NewsModels', function() {

	var model={
		datainit : {
			dtgListProduct: [],
			slbCustomer : [],
			pageStatus : "add",
		    appURL : "http://localhost:8000"
		},
	};

	model.request = {
		txtBranchId:"",
		txtInvoiceId:"",
		dtpCreatedDate : moment().format("DD/MM/YYYY"),
		txtInvoiceNote:"",
		swExportInvoiceStatus : 0,
		txtInvoiceNumber:"",
		radioStatus: "2",
		radioPaymentMethod:"1",
		txtTotalSum:0,
		dtgListProductInvoice: [],
		txtReceivedName:"",
		txtReceivedPhone: "",
		txtReceivedAddress:"",
		txtCustomerDistrict:"",
		txtCustomerId:"",
		txtCustomerEmail:"",
		txtCustomerPhone:"",
		txtCustomerGroup:"",
		txtCustomerName:"",
		txtCustomerAddress:"",
		txtCustomerAccount:"",
		txtCustomerIdentify:"",
		txtCustomerStudentCard:"",
		txtCustomerUniversity:"",

	};
	model.singleDatePickerOpts = {
    	minView : 'day',
    	startView : 'day',
    	modelType : 'DD/MM/YYYY',
    };

	model.localLang = {
	    selectAll       : Languages.common_button_select_all,
	    selectNone      : Languages.common_button_deselected_all,
	    reset           : Languages.common_button_cancel,
	    search          : Languages.common_input_label_search,
	    nothingSelected : Languages.common_input_label_select
	}

	return model;
});

// create angular controller
createInvocieApp.controller('createInvoiceController', function(NewsModels, commonFunction, $scope, $rootScope, $http, $filter, $window, $location, $uibModal, toastr,DTOptionsBuilder, DTColumnDefBuilder,DTColumnBuilder) {

	$scope.model = NewsModels;

	$scope.openDialog = function() {

		$uibModal.open({
			animation: true,
			ariaLabelledBy: 'modal-title',
			ariaDescribedBy: 'modal-body',
			templateUrl: 'http://lavendershop94.com/MultimediaManager?type=Images',
			size: 'lg',
			controller: function($scope) {
				// $scope.name = 'bottom';  
			}
	    });
	};
	// =================pageInit======================
	$scope.pageInit = function() {

		$scope.model.datainit.appURL = $('meta[name="app_url"]').attr('content');

		$("#addReLoading").show();
		$scope.loadCustomerList();
        $scope.model.request.txtInvoiceNumber = "HD"+$scope.generateCode();
		$scope.loadInvoiceDetail();

		$scope.dtOptions = DTOptionsBuilder.newOptions()
			.withOption('order', [2, 'desc'])
        	.withPaginationType('full_numbers')
        	.withDisplayLength(10)
	        .withColReorder()
	        // .withColReorderOrder([0, 1, 2, 3, 4])
	        .withBootstrap()
	        .withBootstrapOptions({
	            pagination: {
	                classes: {
	                    ul: 'pagination pagination-md'
	                }
	            }
	        })
	        .withLanguage({
	            "sEmptyTable":     "Không có dữ liệu.",
	            "sInfo":           "Hiển Thị Từ _START_ Đến _END_ Của _TOTAL_ Kết Quả",
	            "sInfoEmpty":      "Hiển Thị Từ 0 Đến 0 Của 0 Kết Quả",
	            "sInfoFiltered":   "(Đã Lọc Từ Tổng _MAX_ Kết Quả)",
	            "sInfoPostFix":    "",
	            "sInfoThousands":  ",",
	            "sLengthMenu":     "Hiển Thị&nbsp;&nbsp;&nbsp;_MENU_&nbsp;&nbsp;&nbsp;Kết Quả / 1 Trang",
	            "sLoadingRecords": "Đang Tải Dữ Liệu...",
	            "sProcessing":     "Đang Xử Lý Dữ Liệu...",
	            "sSearch":         "Từ Khóa: ",
	            "sZeroRecords":    "Không tìm thấy dữ liệu trùng khớp.",
	            "oPaginate": {
	                "sFirst":    "Đầu",
	                "sLast":     "Cuối",
	                "sNext":     "Tiếp",
	                "sPrevious": "Trước"
	            },
	            "oAria": {
	                "sSortAscending":  ": activate to sort column ascending",
	                "sSortDescending": ": activate to sort column descending"
	            }
	        })
	        .withOption('responsive', true);
	};
	// ==============get customer list IN select box==============
	$scope.loadCustomerList= function(){
		$http.post($scope.model.datainit.appURL + "/SystemApi/GetListCustomer")
	    	.then(
               	function (response) {
                  	var data = response.data;

			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    		$scope.model.datainit.slbCustomer=[];
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		toastr.warning(data.warning, Languages.common_warning_title);
			    		$scope.model.datainit.slbCustomer=[];
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		$scope.model.datainit.slbCustomer.push( {"customer_id": "new",
						"customer_fullname" :"Tạo Mới Khách Hàng"} );
						$.each( data.success, function( i ) {
							var obj;var val1;var val2;
							$.each(data.success[i], function (key, val) {
						        if(key == "customer_id")
						        	val1=val;
								if(key == "customer_fullname")
						        	val2=val;
						    });
						    obj = {"customer_id": val1, "customer_fullname" : val2};
				
					    	$scope.model.datainit.slbCustomer.push(obj);
						});
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    		$scope.model.datainit.slbCustomer=[];
			    	}
			    },
               	function (response) {
                  	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$scope.model.datainit.slbCustomer=[];
			    }
			);
	};
	// ===================load Detail====================
	$scope.loadCustomerDetail= function(id){
		
		if(id !== "")
	    {
	    	var request = {
		        customer_id : id,
		    };

		    $http.post($scope.model.datainit.appURL + "/SystemApi/GetCustomerDetails", JSON.stringify(request))
	    	.then(
               	function (response) {
                  	var data = response.data;
				    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
			            {
			                toastr.error(data.messages, Languages.common_error_title);

			                if(data.auth != "" && data.auth != null  && data.auth != undefined)
			                {
			                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
			                }
			            }
				    	else if(data.error != "" && data.error != null  && data.error != undefined)
				    	{
				    		toastr.error(data.error, Languages.common_error_title);
				    	}
				    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
				    	{
				    		toastr.warning(data.warning, Languages.common_warning_title);
				    	}
				    	else if(data.success != "" && data.success != null  && data.success != undefined)
				    	{
				    	
				    		$scope.model.request.txtCustomerId = data.success.customer_id;
				    		$scope.model.request.txtCustomerEmail = data.success.customer_email;
				    		$scope.model.request.txtCustomerPhone = data.success.customer_phone;
				    		$scope.model.request.txtCustomerName = data.success.customer_fullname;
				    		$scope.model.request.txtCustomerAddress = data.success.customer_address;
				    		$scope.model.request.txtCustomerAccount = data.success.customer_account;
				    		$scope.model.request.txtCustomerDistrict = data.success.district_name;
							
				    	}
				    	else
				    	{
				    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
				    		{
				    			toastr.error(data.systemerror, Languages.common_error_title);
				    			window.location.replace("./"+data.errorCode);
				    		}
				    		else
				    		{
				    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
				    		}
				    	}
			    },
               	function (response) {
                  
                 	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    }
			);
	    }
	};
	//===========get Danh Sach San Pham===========
	$scope.loadProductsList = function(bra_id) {
		// alert(bra_id);
		$("#listReLoading").show();
		
		var request = {
			branch_id : bra_id,
	    };

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetProductByBranch", JSON.stringify(request))
	    .then(
               function (response) {
                  var data = response.data;

			if(data.error != "" && data.error != null  && data.error != undefined)
	    	{
	    		toastr.error(data.error, Languages.common_error_title);
	    		$scope.model.datainit.dtgListProduct = [];
	    	}
	    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
	    	{
	    		// toastr.warning(data.warning, Languages.common_warning_title);
	    		// $scope.model.datainit.dtgListProduct = [];
	    	}
	    	else if(data.success != "" && data.success != null  && data.success != undefined)
	    	{
	    		$scope.model.datainit.dtgListProduct = data.success;
	    	}
	    	else
	    	{
	    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
	    		{
	    			toastr.error(data.systemerror, Languages.common_error_title);
	    			window.location.replace("./"+data.errorCode);
	    		}
	    		else
	    		{
	    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
	    		}
	    		$scope.model.datainit.dtgListProduct = [];
	    	}
	    	$("#listReLoading").hide();
	    },
       	function (response) {
          
          	var data = response.data;
			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			$scope.model.datainit.dtgListProduct = [];
			$("#listReLoading").hide();
	    });
	};
	//=================Load Invoice Detail==============
	$scope.loadInvoiceDetail= function(){

		if($("#txtSlug").val() !== "")
	    {
	    	var request = {
		        invoice_id : $("#txtSlug").val(),
		    };
		    //alert($("#txtSlug").val());

		    $http.post($scope.model.datainit.appURL + "/SystemApi/GetInvoiceDetail", JSON.stringify(request))
	    	.then(
               	function (response) {
                  	var data = response.data;

				    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
			            {
			                toastr.error(data.messages, Languages.common_error_title);

			                if(data.auth != "" && data.auth != null  && data.auth != undefined)
			                {
			                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
			                }
			            }
				    	else if(data.error != "" && data.error != null  && data.error != undefined)
				    	{
				    		toastr.error(data.error, Languages.common_error_title);
				    	}
				    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
				    	{
				    		toastr.warning(data.warning, Languages.common_warning_title);
				    	}
				    	else if(data.success != "" && data.success != null  && data.success != undefined)
				    	{
				    		$scope.model.request = {
								txtCustomerId: data.success.customer_id,
					    		txtCustomerEmail :data.success.customer_email,
					    		txtCustomerPhone :data.success.customer_phone,
					    		txtCustomerName: data.success.customer_fullname,
					    		txtCustomerAddress: data.success.customer_address,
					    		txtCustomerAccount :data.success.customer_account,
								txtInvoiceId:data.success.invoice_id,
								txtInvoiceNumber: data.success.invoice_number,
								txtTotalSum : data.success.total_sum,
								txtInvoiceNote: data.success.invoice_note,
								radioStatus: data.success.status.toString(),
							};
							$scope.model.request.txtBranchId=data.success.branch_id;
							$scope.model.request.txtCustomerIdentify=data.success.customer_identify;
							$scope.model.request.txtCustomerStudentCard=data.success.customer_student_card;
							$scope.model.request.txtCustomerUniversity=data.success.customer_university;

							if(data.success.created_date==null || data.success.created_date==""){
								$scope.model.request.dtpCreatedDate=moment().format("DD/MM/YYYY");
							}
							else{
								$scope.model.request.dtpCreatedDate=moment(data.success.created_date).format("DD/MM/YYYY");
							}
							angular.forEach($scope.model.datainit.slbCustomer, function(value, key2) {
							    if(value.customer_id == data.success.customer_id)
							    {
							    	value.ticked = true;
							    }
							});
							$scope.model.request.dtgListProductInvoice=data.product_list;

							$scope.model.datainit.pageStatus = "update";
				    	}
				    	else
				    	{
				    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
				    		{
				    			toastr.error(data.systemerror, Languages.common_error_title);
				    			window.location.replace("./"+data.errorCode);
				    		}
				    		else
				    		{
				    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
				    		}
				    	}
			    },
               	function (response) {
                 	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    }
			);
	    }
	};
	//===================addCart================
	$scope.addCart = function(obj){
	
		// gio hang trong
		if($scope.model.request.dtgListProductInvoice.length==0){
			var new_obj =
			{"product_id": obj[0][0].product_id,
			"quantity" : $("#pro_"+obj[0][0].product_id).val(),
			"product_retail_prices" : obj[0][0].product_retail_prices,
			"product_imported_prices" : obj[0][0].product_imported_prices,
			"product_thumbnail" : obj[0][0].product_thumbnail,
			"product_name" : obj[0][0].product_name,
			"unit_name" : obj[0][0].unit_name,
			"cate_name" : obj[0][0].cate_name,
			"color_name" : obj[0][0].color_name,
			"branch_store_quantity_remain" : obj[0][0].branch_store_quantity_remain,
			"total_price" : parseFloat($("#pro_"+obj[0][0].product_id).val()) * parseFloat(obj[0][0].product_retail_prices) *80/100
			};

			$scope.model.request.dtgListProductInvoice.push(new_obj);
		}
		else
		{
			//tao cờ:
			var flag=0;var outflag=0;
			var oldpro_id;var oldpro_quan;var newpro_quan;
			$.each( $scope.model.request.dtgListProductInvoice, function( i ) {
				$.each( $scope.model.request.dtgListProductInvoice[i], function (key, val) {
					
			        if(key == "product_id"){
			        	// nếu trùng săn phẩm > break vòng lặp/ cập nhật SL
			        	if(val == obj[0][0].product_id){
			        		flag=1;outflag=1;
			        		oldpro_id=val;
			        	}
			        }

			        // cap nhat thanh tien
			        if(flag == 1 && key == "product_retail_prices"){
			        	$scope.model.request.dtgListProductInvoice[i].total_price=parseFloat(newpro_quan) * parseFloat(val);

			        }
			        // cap nhat sl moi
			        if(flag == 1 && key == "quantity")
			        {
			        	oldpro_quan=val;
			        	newpro_quan = parseInt(oldpro_quan) + parseInt($("#pro_"+obj[0][0].product_id).val());
			        	$scope.model.request.dtgListProductInvoice[i].quantity=newpro_quan;
			        	$("#invoice_"+obj[0][0].product_id).val(newpro_quan);
			        }
			        // check ton kho
			        if(flag == 1 && key == "branch_store_quantity_remain"){
			        	// alert(val+"/"+newpro_quan);
			        	// alert(oldpro_quan+"/"+newpro_quan);
			        	if(parseInt(newpro_quan)>parseInt(val)){
			        		toastr.warning("Vượt Quá SL Tồn Kho:("+val+")", Languages.common_warning_title);
			        		$scope.loadInvoiceDetail();
			        	}
			        }
		    	});
		    	flag=0;
			});
			//nếu cờ outflag = 0 >không trùng săn phẩm > thêm mới
			if(outflag==0){
				var new_obj =
				{"product_id": obj[0][0].product_id,
				"quantity" : $("#pro_"+obj[0][0].product_id).val(),
				"product_retail_prices" : obj[0][0].product_retail_prices,
				"product_imported_prices" : obj[0][0].product_imported_prices,
				"product_thumbnail" : obj[0][0].product_thumbnail,
				"product_name" : obj[0][0].product_name,
				"unit_name" : obj[0][0].unit_name,
				"cate_name" : obj[0][0].cate_name,
				"color_name" : obj[0][0].color_name,
				"branch_store_quantity_remain" : obj[0][0].branch_store_quantity_remain,
				"total_price" : parseFloat($("#pro_"+obj[0][0].product_id).val()) * parseFloat(obj[0][0].product_retail_prices) *80/100
				};

				$scope.model.request.dtgListProductInvoice.push(new_obj);
			}
		}
	
		$scope.model.request.txtTotalSum = $scope.CaculateTotalSum();
	};
	// =================add invoice / update invoice====================
	$scope.addItem = function(status) {
		//Update invoice
		if(status==2){
			var confirmModal = bootbox.dialog({
		  	message: commonFunction.languageReplace(Languages.common_confirm_update_question,[' Đơn Hàng']),
		  	title: commonFunction.languageReplace(Languages.common_confirm_update_title,[' Đơn Hàng']),
		  	closeButton: true,
		  	animate: true,
		  	className: "confirmModal",
		  	backdrop: true,
		  	headerBackground: "bg-blue",
		  	size: "small",
		  	locale: "vi",
		  	show: true,
		  	buttons: {
		  		cancel : {
			      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
			      	className: "btn-default",
			      	callback: function() {
			        	confirmModal.modal('hide');
			      	}
			    },
			    success: {
		      		label: '<i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;'+Languages.common_button_update,
			      	className: "btn-primary",
			      	callback: function() {
			      		
			      		$("#addReLoading").show();
			      	

						    var request = {
						    	invoice_id : $("#txtSlug").val(),
						    	list_product : $scope.model.request.dtgListProductInvoice,
						        invoice_number : $scope.model.request.txtInvoiceNumber,
						        customer_id : $scope.model.request.txtCustomerId,
						        receiver_name : $scope.model.request.txtReceivedName,
						        receiver_phone : $scope.model.request.txtReceivedPhone,
						        receiver_address : $scope.model.request.txtReceivedAddress,
						        total_sum : $scope.model.request.txtTotalSum,
						        // delivery_date : moment($scope.model.request.dtpCreatedDate,'DD/MM/YYYY'),
						        invoice_note : $scope.model.request.txtInvoiceNote,
						        export_invoice_status : $scope.model.request.swExportInvoiceStatus,
						        payment_method : $scope.model.request.radioPaymentMethod,
						        status: $scope.model.request.radioStatus
						    };
						    
						    $http.post($scope.model.datainit.appURL + "/SystemApi/UpdateInvoice",JSON.stringify(request))
				    	.then(
			               	function (response) {
				                  	var data = response.data;

								    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
							            {
							                toastr.error(data.messages, Languages.common_error_title);

							                if(data.auth != "" && data.auth != null  && data.auth != undefined)
							                {
							                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
							                }
							            }
								    	else if(data.error != "" && data.error != null  && data.error != undefined)
								    	{
								    		toastr.error(data.error, Languages.common_error_title);
								    	}
								    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
								    	{
								    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
								    		warning = warning.replace('["', '').replace('"]', '');
								    		toastr.warning(warning, Languages.common_warning_title);
								    	}
								    	else if(data.success != "" && data.success != null  && data.success != undefined)
								    	{
								    		toastr.success(data.success, commonFunction.languageReplace(Languages.common_success_title,[' Đơn Hàng']));

								    		bootbox.alert(commonFunction.languageReplace(Languages.common_success_created,[' Đơn Hàng']), function() {
												window.location.replace($scope.model.datainit.appURL + "/System/ListInvoices");
											});

											setTimeout(function(){ 
												window.location.replace($scope.model.datainit.appURL + "/System/ListInvoices");
											}, 3000);
								    	}
								    	else
								    	{
								    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
								    		{
								    			toastr.error(data.systemerror, Languages.common_error_title);
								    			window.location.replace("./"+data.errorCode);
								    		}
								    		else
								    		{
								    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
								    		}
								    	}
								    	$("#addReLoading").hide();
							    },
				               	function (response) {
				              
				                 	var data = response.data;
									toastr.error(Languages.common_error_exception, Languages.common_error_title);
									$("#addReLoading").hide();
							    }
							);
						

				      	}
				    },
			  	}
			});
		}
		//Add invoice
		else{
			var confirmModal = bootbox.dialog({
		  	message: commonFunction.languageReplace(Languages.common_confirm_add_question,[' Đơn Hàng']),
		  	title: commonFunction.languageReplace(Languages.common_confirm_add_title,[' Đơn Hàng']),
		  	closeButton: true,
		  	animate: true,
		  	className: "confirmModal",
		  	backdrop: true,
		  	headerBackground: "bg-blue",
		  	size: "small",
		  	locale: "vi",
		  	show: true,
		  	buttons: {
		  		cancel : {
			      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
			      	className: "btn-default",
			      	callback: function() {
			        	confirmModal.modal('hide');
			      	}
			    },
			    success: {
		      		label: '<i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;'+Languages.common_button_add,
			      	className: "btn-primary",
			      	callback: function() {
			      		$("#addReLoading").show();
					    var request = {
					    	list_product : $scope.model.request.dtgListProductInvoice,
					        invoice_number : $scope.model.request.txtInvoiceNumber,
					        customer_id : $scope.model.request.txtCustomerId,
					        receiver_name : $scope.model.request.txtReceivedName,
					        receiver_phone : $scope.model.request.txtReceivedPhone,
					        receiver_address : $scope.model.request.txtReceivedAddress,
					        total_sum : $scope.model.request.txtTotalSum,
					        // delivery_date : moment($scope.model.request.dtpCreatedDate,'DD/MM/YYYY'),
					        invoice_note : $scope.model.request.txtInvoiceNote,
					        export_invoice_status : $scope.model.request.swExportInvoiceStatus,
					        payment_method : $scope.model.request.radioPaymentMethod,
					        status: $scope.model.request.radioStatus
					    };
					    
						$http.post($scope.model.datainit.appURL + "/SystemApi/AddInvoice",JSON.stringify(request))
				    	.then(
			               	function (response) {
				                  	var data = response.data;

								    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
							            {
							                toastr.error(data.messages, Languages.common_error_title);

							                if(data.auth != "" && data.auth != null  && data.auth != undefined)
							                {
							                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
							                }
							            }
								    	else if(data.error != "" && data.error != null  && data.error != undefined)
								    	{
								    		toastr.error(data.error, Languages.common_error_title);
								    	}
								    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
								    	{
								    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
								    		warning = warning.replace('["', '').replace('"]', '');
								    		toastr.warning(warning, Languages.common_warning_title);
								    	}
								    	else if(data.success != "" && data.success != null  && data.success != undefined)
								    	{
								    		toastr.success(data.success, commonFunction.languageReplace(Languages.common_success_title,[' Đơn Hàng']));

								    		bootbox.alert(commonFunction.languageReplace(Languages.common_success_created,[' Đơn Hàng']), function() {
												window.location.replace($scope.model.datainit.appURL + "/System/ListInvoices");
											});

											setTimeout(function(){ 
												window.location.replace($scope.model.datainit.appURL + "/System/ListInvoices");
											}, 3000);
								    	}
								    	else
								    	{
								    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
								    		{
								    			toastr.error(data.systemerror, Languages.common_error_title);
								    			window.location.replace("./"+data.errorCode);
								    		}
								    		else
								    		{
								    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
								    		}
								    	}
								    	$("#addReLoading").hide();
							    },
				               	function (response) {
				                 	var data = response.data;
									toastr.error(Languages.common_error_exception, Languages.common_error_title);
									$("#addReLoading").hide();
							    }
							);
				      	}
				    },
			  	}
			});
		}
		//end else
	};
	//=====================updateInvoiceQuan remove cart===============
	$scope.updateInvoiceQuan = function(pro_id,delete_flag)
	{
		var new_quan= $("#invoice_"+pro_id).val();
		//alert(new_quan);
		//tao cờ:
		var flag=0;var newpro_quan;var removed=0;
		$.each( $scope.model.request.dtgListProductInvoice, function( i ) {
			$.each( $scope.model.request.dtgListProductInvoice[i], function (key, val) {
				
		        if(key == "product_id"){
		        	// nếu trùng săn phẩm > break vòng lặp/ cập nhật SL
		        	if(val == pro_id){
		        		flag=1;
		        	}
		        }
		        // check ton kho
		        if(flag == 1 && key == "branch_store_quantity_remain"){
		        	// alert(val+"/"+newpro_quan);
		        	if(parseInt(newpro_quan)>parseInt(val)){
		        		toastr.warning("Vượt Quá SL Tồn Kho:("+val+")", Languages.common_warning_title);
		        		$scope.loadInvoiceDetail();
		        	}
		        }
		        // cap nhat thanh tien= sl x don gia
		        if(flag == 1 && key == "product_retail_prices"){
		        	// alert(val);
		        	//neu da xoa sp thì không cần cập nhật thành tiền
		        	if(removed!=1)
		        	{
		        		if(newpro_quan<3){
		                	$scope.model.request.dtgListProductInvoice[i].total_price=parseFloat(newpro_quan) * parseFloat(val)*80/100;
		                }
		                // product_quantity < 5 : giam 22%
		                else if(newpro_quan<5){
		                	$scope.model.request.dtgListProductInvoice[i].total_price=parseFloat(newpro_quan) * parseFloat(val)*78/100;
		                }
		                // product_quantity < 10 : giam 24%
		                else if(newpro_quan<10){
		                	$scope.model.request.dtgListProductInvoice[i].total_price=parseFloat(newpro_quan) * parseFloat(val)*76/100;
		                }
		                // product_quantity < 20 : giam 26%
		                else if(newpro_quan<20){
		                	$scope.model.request.dtgListProductInvoice[i].total_price=parseFloat(newpro_quan) * parseFloat(val)*74/100;
		                }
		                // product_quantity < 50 : giam 28%
		                else if(newpro_quan<50){
		                	$scope.model.request.dtgListProductInvoice[i].total_price=parseFloat(newpro_quan) * parseFloat(val)*72/100;
		                }
		                // product_quantity < 100 : giam 30%
		                else if(newpro_quan<100){
		                	$scope.model.request.dtgListProductInvoice[i].total_price=parseFloat(newpro_quan) * parseFloat(val)*70/100;
		                }
		                // product_quantity >= 100 : giam 35%
		                else {
		                    $scope.model.request.dtgListProductInvoice[i].total_price=parseFloat(newpro_quan) * parseFloat(val)*65/100;
		                }
		        		
		        	}
		        }
		        // cap nhat sl moi
		        if(flag == 1 && key == "quantity")
		        {
		        	var newquan=$("#invoice_"+pro_id).val();
		        	//nếu flag xóa =1 (nút xóa)> xóa sp 
		        	if(delete_flag ==1){
		        		$scope.model.request.dtgListProductInvoice.splice(i,1);
		        		removed=1;
		        	}
		        	//nếu flag xóa =0 (nút cap nhat)
		        	else{
		        		// invalid number > delete pro
			        	if(newquan=='undefined' || newquan=="" || newquan==0)
			        	{
			        	
			        		$scope.model.request.dtgListProductInvoice.splice(i,1);
			        		removed=1;
			        	}
			        	//valid > update quantity
			        	else{
				        	newpro_quan = parseInt($("#invoice_"+pro_id).val());
				        	$scope.model.request.dtgListProductInvoice[i].quantity=newpro_quan;
			        	}
		        	}
		        }

	    	});
	    	//reset flag 
	    	flag=0;removed=0;
		});
		// tinh tong thanh tien
	
		$scope.model.request.txtTotalSum = $scope.CaculateTotalSum();
	};
	//====================CaculateTotalSum=============
	$scope.CaculateTotalSum = function(){
		var total_sum=0;var quan=0;var price=0;
		if($scope.model.request.dtgListProductInvoice != null){
			$.each( $scope.model.request.dtgListProductInvoice, function( i ) {
				$.each( $scope.model.request.dtgListProductInvoice[i], function (key, val) {

			        // cap nhat thanh tien
			        if(key == "product_retail_prices"){
			        	price = val;
			        	// product_quantity < 3 : giam 20%
		                if(quan<3){
		                	total_sum +=price*quan*80/100;
		                }
		                // product_quantity < 5 : giam 22%
		                else if(quan<5){
		                	total_sum +=price*quan*78/100;
		                }
		                // product_quantity < 10 : giam 24%
		                else if(quan<10){
		                	total_sum +=price*quan*76/100;
		                }
		                // product_quantity < 20 : giam 26%
		                else if(quan<20){
		                	total_sum +=price*quan*74/100;
		                }
		                // product_quantity < 50 : giam 28%
		                else if(quan<50){
		                	total_sum +=price*quan*72/100;
		                }
		                // product_quantity < 100 : giam 30%
		                else if(quan<100){
		                	total_sum +=price*quan*70/100;
		                }
		                // product_quantity >= 100 : giam 35%
		                else {
		                    total_sum +=price*quan*65/100;
		                }
			        }
			        // cap nhat sl moi
			        if(key == "quantity")
			        {
			        	quan=val;

			        }
		    	});
		    	quan=0;price=0;
			});
			//alert(total_sum);
			return total_sum;
		}
		else
		{
			//alert(total_sum);
			return total_sum;
		}	
	};
	//=====================================Ham Phu====================================
    $scope.formReload = function() {
    	location.reload();
    };
    //=========================form Reset======================
	$scope.formReset = function() {

		// $("#pagePreLoading").show();
		// $("#addReLoading").show();

		$("#holder").attr("src","");
		$("#thumbnail").val("");

		$scope.model.request = {
			txtCustomerId:"",
			txtCustomerEmail:"",
			txtCustomerPhone:"",
			txtCustomerGroup:"",
			txtCustomerName:"",
			txtCustomerAddress:"",
			txtCustomerAccount:"",
			txtCustomerPassword : "",
			txtCustomerPasswordAgain : "",
		};

		angular.forEach($scope.model.datainit.slbCustomer, function(value, key) {
			value.ticked = false;
		});

		$scope.frmAdd.$setPristine();
		$scope.frmAdd.$setUntouched();

		$scope.pageInit();

		//$("#addReLoading").hide();
	};
	//===============Hàm Chức Năng Cho SelectBox=====================
	$scope.slbClear = function(element)
	{

		if(element == "txtCustomerGroup")
		{
			angular.forEach($scope.model.datainit.slbCustomer, function(value, key) {
		    	value.ticked = false;
			});
		}
		
	   	$('#'+element).val(""); 
	   	$('#'+element).trigger('input');
	};

	$scope.slbItemClick = function(item,element)
	{
   		if(element == "txtCustomerGroup")
		{
			$('#'+element).val(item.group_id); 
   			$('#'+element).trigger('input');
		}
		else if(element == "txtUnit")
		{
			$('#'+element).val(item.unit_id); 
   			$('#'+element).trigger('input');
		}
		else if(element == "txtCustomer"){
			$('#'+element).val(item.customer_id); 
   			$('#'+element).trigger('input');
   			if(item.customer_id == "new"){
   				window.location.replace($scope.model.datainit.appURL + "/System/CreateCustomer");
   			}
   			else{
   				$scope.loadCustomerDetail(item.customer_id)
   			}
		}
	};
	//=============generate code==============
	$scope.generateCode= function(){
		var currentdate = new Date(); 
    	var datetime =String(currentdate.getDate())
                + String((currentdate.getMonth()+1))
                + String(currentdate.getFullYear())
                + String(currentdate.getHours())
                + String(currentdate.getMinutes())
                + String(currentdate.getSeconds());
        return datetime;
	};
});