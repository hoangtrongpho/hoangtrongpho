/*!
 *
 *	Develop by Vinh Nguyễn - Ho Chi Minh City - 2017
 * 	Copyright Â© 2016-2017 Skyfire Team. All rights reserved.
 *	Design by Skyfire Team 2.3.3
 *
 *	Phone: 0975 578 276
 *	Skype: enjoyvinh
 *
 */

"use strict";

// app.js
// create angular app
var bikeApp = angular.module('bikeApp', ['commonApp', 'ngSanitize',
		'datatables','datatables.colreorder','datatables.bootstrap']);

bikeApp.factory('BikeModels', function() {

	var model={
		datainit : {
			dtgListBranchItems : [],
			dtgList2 : [],
			dtgListBranch:[],
			appURL : "http://localhost:8000"
		},
	};

	model.request = {
		searchStr:"",
	};


	model.localLang = {
	    selectAll       : Languages.common_button_select_all,
	    selectNone      : Languages.common_button_deselected_all,
	    reset           : Languages.common_button_cancel,
	    search          : Languages.common_input_label_search,
	    nothingSelected : Languages.common_input_label_select        //default-label is deprecated and replaced with this.
	}

	return model;
});

// create angular controller
bikeApp.controller('bikeController', function(BikeModels, commonFunction, $scope, $rootScope, $http, $filter, $window, $location, toastr,
	DTOptionsBuilder, DTColumnDefBuilder,DTColumnBuilder) {

	$scope.model = BikeModels;
	// pageInit
	$scope.pageInit = function() {

		$scope.model.datainit.appURL = $('meta[name="app_url"]').attr('content');

		$("#listReLoading").show();
		$scope.InnitStore();
		$scope.GetBranchList();
		$scope.GetStoreByBranch();
		
		$scope.dtOptions = DTOptionsBuilder.newOptions()
			.withOption('order', [])
        	.withPaginationType('full_numbers')
        	.withDisplayLength(50)
	        .withColReorder()
	        // .withColReorderOrder([0, 1, 2, 3, 4])
	        .withBootstrap()
	        .withBootstrapOptions({
	            pagination: {
	                classes: {
	                    ul: 'pagination pagination-md'
	                }
	            }
	        })
	        .withLanguage({
	            "sEmptyTable":     "Không có dữ liệu.",
	            "sInfo":           "Hiển Thị Từ _START_ Đến _END_ Của _TOTAL_ Kết Quả",
	            "sInfoEmpty":      "Hiển Thị Từ 0 Đến 0 Của 0 Kết Quả",
	            "sInfoFiltered":   "(Đã Lọc Từ Tổng _MAX_ Kết Quả)",
	            "sInfoPostFix":    "",
	            "sInfoThousands":  ",",
	            "sLengthMenu":     "Hiển Thị&nbsp;&nbsp;&nbsp;_MENU_&nbsp;&nbsp;&nbsp;Kết Quả / 1 Trang",
	            "sLoadingRecords": "Đang Tải Dữ Liệu...",
	            "sProcessing":     "Đang Xử Lý Dữ Liệu...",
	            "sSearch":         "Từ Khóa: ",
	            "sZeroRecords":    "Không tìm thấy dữ liệu trùng khớp.",
	            "oPaginate": {
	                "sFirst":    "Đầu",
	                "sLast":     "Cuối",
	                "sNext":     "Tiếp",
	                "sPrevious": "Trước"
	            },
	            "oAria": {
	                "sSortAscending":  ": activate to sort column ascending",
	                "sSortDescending": ": activate to sort column descending"
	            }
	        })
	        .withOption('responsive', true);

	    $scope.dtColumnDefs = [
	        DTColumnDefBuilder.newColumnDef(0).notSortable(),
	    ];
	};
	// loadList
	$scope.loadList2 = function(searchStr) {

		$("#listReLoading").show();

		var request = {
			searchStr : searchStr,
	    };

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetListStore2", JSON.stringify(request))
	    .then(
	    		function (response) {
                
	                var data = response.data;

			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    		$scope.model.datainit.dtgList2 = [];
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
			    		warning = warning.replace('["', '').replace('"]', '');
			    		toastr.warning(warning, Languages.common_warning_title);
			    		$scope.model.datainit.dtgList2 = [];
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		$scope.model.datainit.dtgList2 = data.success;
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    		$scope.model.datainit.dtgList2 = [];
			    	}
		    		$("#listReLoading").hide();
			    },
               	function (response) {
               
                  	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$scope.model.datainit.dtgList2 = [];
					$("#listReLoading").hide();
			    }
		    );
		    //end then
	}
	// apiInnitStore
	$scope.InnitStore = function() {

		$("#listReLoading").show();

		var request = {
	    };

	    $http.post($scope.model.datainit.appURL + "/SystemApi/InnitStore", JSON.stringify(request))
	    .then(
	    		function (response) {
                
	                var data = response.data;

			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    	
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		// toastr.success(data.success, Languages.common_success_title);

			    		$scope.GetBranchList();
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    	}
		    		$("#listReLoading").hide();
			    },
               	function (response) {
                  	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$("#listReLoading").hide();
			    }
		    );
		    //end then
	}
	//UpdateStoreQuan
	// element: lay root value
	//bra_store_quantity: SL dau ky cua kho/ new_quan=bra_store_quantity+ new_input
	$scope.UpdateStoreQuan = function(element,new_quan_root,pro_id,bra_id,sum_deliver_bra_remain)
	{

		var new_quan= parseInt($("#"+element+"_"+bra_id+"_"+pro_id).val())+parseInt(new_quan_root);
		// alert($("#"+element+"_"+bra_id+"_"+pro_id).val()+"/"+new_quan_root+"/"+new_quan);
		// return false;
		var new_quan_balance= new_quan-new_quan_root;
		// neu bo trong bao loi
		if (new_quan === undefined || new_quan === null || new_quan === "" || new_quan <= 0
			) {
			toastr.warning("Không Hợp Lệ", Languages.common_warning_title);
		   	$("#"+element+pro_id).val(0);
		   	return false;
		}
		// Nhập Kho Chi Nhánh Không Vượt Quá SL tồn kho tổng
		else if(parseInt(new_quan_balance) > parseInt(sum_deliver_bra_remain)){
			toastr.warning("Không Được Vượt Quá Số Lượng Tồn Kho là:"+(parseInt(sum_deliver_bra_remain)), Languages.common_warning_title);
			$("#"+element+pro_id).val(0);
			return false;
		}

		$("#listReLoading").show();

		var request = {
			product_id:pro_id,
			branch_id:bra_id,
			branch_store_quantity:new_quan,
	    };

	    $http.post($scope.model.datainit.appURL + "/SystemApi/UpdateStoreQuan", JSON.stringify(request))
	    .then(
	    		function (response) {
                
	                var data = response.data;

			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
	    				$window.location.href = $scope.model.datainit.appURL + "/System/ImportExportStore";
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    	}
		    		$("#listReLoading").hide();
			    },
               	function (response) {
               
                  	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$("#listReLoading").hide();
			    }
		    );
	};
	//UpdateStoreQuan
	// element: lay root value
	//bra_store_quantity: SL dau ky cua kho/ new_quan=bra_store_quantity+ new_input
	$scope.UpdateReturnStoreQuan = function(element,pro_id,bra_id,bra_remain,bra_store_quantity)
	{
		var new_quan=parseInt($("#"+element+"_"+bra_id+"_"+pro_id).val());
		// value to update
		var branch_store_quantity= parseInt(bra_store_quantity)-parseInt(new_quan);
		// alert("id:"+pro_id+"/bra_id:"+bra_id+"/"+new_quan+"/"+bra_remain+"/"+branch_store_quantity);
		// return false;
		// Khong hop le
		if (new_quan === undefined || new_quan === null || new_quan === "" || new_quan <= 0
			) {
			toastr.warning("Không Được Bỏ Trống", Languages.common_warning_title);
		   	$("#"+element+pro_id).val(0);
		   	return false;
		}
		// Trả về Kho thì không trả lố vượt quá  SL tồn kho: bra_remain
		else if(parseInt(new_quan) > parseInt(bra_remain)){
			toastr.warning("Vượt Quá SL Tồn:"+parseInt(bra_remain), Languages.common_warning_title);
		   	$("#"+element+pro_id).val(0);
		   	return false;
		}

		$("#listReLoading").show();

		var request = {
			product_id:pro_id,
			branch_id:bra_id,
			branch_store_quantity:branch_store_quantity,
	    };

	    $http.post($scope.model.datainit.appURL + "/SystemApi/UpdateStoreQuan", JSON.stringify(request))
	    .then(
	    		function (response) {
                
	                var data = response.data;

			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
		    			toastr.success(data.success, Languages.common_success_title);
			    		$window.location.href = $scope.model.datainit.appURL + "/System/ImportExportStore";
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    	}
		    		$("#listReLoading").hide();
			    },
               	function (response) {
               
                  	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$("#listReLoading").hide();
			    }
		    );
	};
	// UpdateStoreSumQuan
	$scope.UpdateStoreSumQuan = function(element,pro_id,store_quantity)
	{
		// alert();
		var new_quan= parseInt($("#"+element+pro_id).val());
		var update_store_quantity=parseInt(new_quan)+parseInt(store_quantity);
		// alert(new_quan+"/"+update_store_quantity);
		// return false;
		if (new_quan === undefined || new_quan === null || new_quan === "" 
			) {
			toastr.warning("Không Hợp Lệ", Languages.common_warning_title);
		   	$("#"+element+pro_id).val(0);
		   	return false;
		}
		else if(parseInt(new_quan) > 9999){
			toastr.warning("Không Được Vượt Quá 9999", Languages.common_warning_title);
			$("#"+element+pro_id).val(0);
			return false;
		}
		$("#listReLoading").show();

		var request = {
			product_id:pro_id,
			store_quantity:update_store_quantity,
	    };

	    $http.post($scope.model.datainit.appURL + "/SystemApi/UpdateStoreSumQuan", JSON.stringify(request))
	    .then(
	    		function (response) {
                
	                var data = response.data;

			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
		    			toastr.success(data.success, Languages.common_success_title);
			    		$window.location.href = $scope.model.datainit.appURL + "/System/ImportExportStore";
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    	}
		    		$("#listReLoading").hide();
			    },
               	function (response) {
               
                  	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$("#listReLoading").hide();
			    }
		    );
	};
	$scope.GetBranchList = function() {

		$("#listReLoading").show();

		var request = {
	    };

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetBranchList", JSON.stringify(request))
	    .then(
               function (response) {
                  var data = response.data;

		    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
	            {
	                toastr.error(data.messages, Languages.common_error_title);

	                if(data.auth != "" && data.auth != null  && data.auth != undefined)
	                {
	                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
	                }
	            }
		    	else if(data.error != "" && data.error != null  && data.error != undefined)
		    	{
		    		toastr.error(data.error, Languages.common_error_title);
		    		$scope.model.datainit.dtgListBranch = [];
		    	}
		    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
		    	{
		    		// toastr.warning(data.warning, Languages.common_warning_title);
		    		// $scope.model.datainit.dtgListBranch = [];
		    	}
		    	else if(data.success != "" && data.success != null  && data.success != undefined)
		    	{
		   //  		$scope.model.datainit.dtgListBranch = data.success;
		   //  		var queryStr0 = "select DISTINCT q.product_id,q.product_name,q.product_thumbnail,q.store_quantity,q.sum_deliver_branch_store_quantity,q.sum_deliver_branch_remain";
		   //  		angular.forEach(data.success, function(value, key) {
	    // 				queryStr0+=",q2.branch_store_quantity"+key+",q4.sum_sale_branch_quantity"+key;
					// });	
		   //  		// alert(queryStr0);
		   //  		// =========================== query 1 ================================
		   //  		var queryStr1="select pd.product_id,pd.product_name,pd.product_thumbnail,pd.store_quantity,SUM(st.branch_store_quantity) AS sum_deliver_branch_store_quantity,CASE WHEN (pd.store_quantity - IFNULL(SUM(st.branch_store_quantity), 0)) IS null THEN 0 ELSE (pd.store_quantity - IFNULL(SUM(st.branch_store_quantity), 0)) END AS sum_deliver_branch_remain FROM store_detail AS st INNER JOIN products_detail AS pd ON st.product_id=pd.product_id INNER JOIN branches_detail AS br ON br.branch_id=st.branch_id GROUP BY pd.product_id,pd.product_name,pd.product_thumbnail,pd.store_quantity";
		   //  		// $("#queryTest").val(queryStr1);
		   //  		// =========================== query 2 ===================================
		   //  		var queryStr2="SELECT pd.product_id,pd.product_name,SUM(ivd.product_quantity) AS sum_sale_product_quantity FROM products_detail as pd LEFT JOIN invoice_detail AS ivd ON ivd.product_id=pd.product_id LEFT JOIN invoice AS iv ON iv.invoice_id=ivd.invoice_id WHERE iv.status= 4 OR iv.status= 5 OR iv.status= 6 OR iv.status= 7 GROUP BY pd.product_id,pd.product_name";
		   //  		// ========================== query 3 =======================================
		   //  		var queryStr3="SELECT pd.product_id";
		   //  		var queryStr3First = "";
		   //  		var queryStr3From=" FROM products_detail AS pd";
		   //  		var queryStr3Join = "";
		   //  		// value = obj[branchid:...,branch_name:...]
		   //  		angular.forEach(data.success, function(value, key) {
		   //  			queryStr3First +=",CASE WHEN a"+key+".sum_sale_product_quantity IS null THEN 0 ELSE a"+key+".sum_sale_product_quantity END AS sum_sale_branch_quantity"+key;
					//    	// alert(value.branch_id+"/"+key);
					//    	queryStr3Join +=" LEFT JOIN( SELECT pd.product_id,pd.product_name,iv.branch_id,SUM(ivd.product_quantity) AS sum_sale_product_quantity FROM products_detail as pd LEFT JOIN invoice_detail AS ivd ON ivd.product_id=pd.product_id LEFT JOIN invoice AS iv ON iv.invoice_id=ivd.invoice_id WHERE (iv.status= 4 OR iv.status= 5 OR iv.status= 6 OR iv.status= 7) AND iv.branch_id="+value.branch_id+" GROUP BY pd.product_id,pd.product_name,iv.branch_id ) as a"+key+" ON pd.product_id=a"+key+".product_id";

					// });	
					// queryStr3 +=queryStr3First;
					// queryStr3 +=queryStr3From;
					// queryStr3 +=queryStr3Join;
					// // =========================== query 4 ==================================
					// var queryStr4="select s1.product_id,s0.branch_store_quantity0,s1.branch_store_quantity1 FROM ( select pd.product_id,st.branch_store_quantity as branch_store_quantity0 FROM store_detail AS st INNER JOIN products_detail AS pd ON st.product_id=pd.product_id WHERE st.branch_id=1 ) AS s0, ( select pd.product_id,st.branch_store_quantity as branch_store_quantity1 FROM store_detail AS st INNER JOIN products_detail AS pd ON st.product_id=pd.product_id WHERE st.branch_id=4) AS s1 WHERE s0.product_id=s1.product_id";
		   //          // ==============sum query=============
		   //          var sumquery=queryStr0+" FROM("+queryStr1+") as q LEFT JOIN ("+queryStr2+") AS q3 ON q.product_id=q3.product_id LEFT JOIN ("+queryStr3+") AS q4 ON q4.product_id=q.product_id,("+queryStr4+") as q2 WHERE q.product_id=q2.product_id";
					// $("#queryTest").val(sumquery);

					$scope.model.datainit.dtgListBranch = data.success;
					// alert($scope.model.request.searchStr);
					$scope.loadList2($scope.model.request.searchStr);
		    	}
		    	else
		    	{
		    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
		    		{
		    			toastr.error(data.systemerror, Languages.common_error_title);
		    			window.location.replace("./"+data.errorCode);
		    		}
		    		else
		    		{
		    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
		    		}
		    		$scope.model.datainit.dtgListBranch = [];
		    	}
		    	$("#listReLoading").hide();
	    	},
           	function (response) {
          		var data = response.data;
				toastr.error(Languages.common_error_exception, Languages.common_error_title);
				$scope.model.datainit.dtgListBranch = [];
				$("#listReLoading").hide();
		    });
	};
	// GetStoreByBranch
	$scope.GetStoreByBranch = function() {

		$("#listReLoading").show();

		var request = {
	    };

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetStoreByBranch", JSON.stringify(request))
	    .then(
               function (response) {
                  var data = response.data;

		    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
	            {
	                toastr.error(data.messages, Languages.common_error_title);

	                if(data.auth != "" && data.auth != null  && data.auth != undefined)
	                {
	                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
	                }
	            }
		    	else if(data.error != "" && data.error != null  && data.error != undefined)
		    	{
		    		toastr.error(data.error, Languages.common_error_title);
		    		$scope.model.datainit.dtgListBranch = [];
		    	}
		    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
		    	{
		    		// toastr.warning(data.warning, Languages.common_warning_title);
		    		// $scope.model.datainit.dtgListBranch = [];
		    	}
		    	else if(data.success != "" && data.success != null  && data.success != undefined)
		    	{
		    		// $scope.GetBranchList();
		  			$scope.model.datainit.dtgListBranchItems=data.success;
		    	}
		    	else
		    	{
		    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
		    		{
		    			toastr.error(data.systemerror, Languages.common_error_title);
		    			window.location.replace("./"+data.errorCode);
		    		}
		    		else
		    		{
		    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
		    		}
		    		$scope.model.datainit.dtgListBranch = [];
		    	}
		    	$("#listReLoading").hide();
	    	},
           	function (response) {
          		var data = response.data;
				toastr.error(Languages.common_error_exception, Languages.common_error_title);
				$scope.model.datainit.dtgListBranch = [];
				$("#listReLoading").hide();
		    });
	};
});