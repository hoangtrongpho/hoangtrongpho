/*!
 *
 *	Develop by Vinh Nguyễn - Ho Chi Minh City - 2017
 * 	Copyright Â© 2016-2017 Skyfire Team. All rights reserved.
 *	Design by Skyfire Team 2.3.3
 *
 *	Phone: 0975 578 276
 *	Skype: enjoyvinh
 *
 */

"use strict";

// app.js
// create angular app
var bikeApp = angular.module('bikeApp', ['commonApp', 'ngSanitize',
		'datatables','datatables.colreorder','datatables.bootstrap']);

bikeApp.factory('BikeModels', function() {

	var model={
		datainit : {
			dtgList : [],
			appURL : "http://localhost:8000"
		},
	};

	model.request = {
	};


	model.localLang = {
	    selectAll       : Languages.common_button_select_all,
	    selectNone      : Languages.common_button_deselected_all,
	    reset           : Languages.common_button_cancel,
	    search          : Languages.common_input_label_search,
	    nothingSelected : Languages.common_input_label_select        //default-label is deprecated and replaced with this.
	}

	return model;
});

// create angular controller
bikeApp.controller('bikeController', function(BikeModels, commonFunction, $scope, $rootScope, $http, $filter, $window, $location, toastr,
	DTOptionsBuilder, DTColumnDefBuilder,DTColumnBuilder) {

	$scope.model = BikeModels;
	// pageInit
	$scope.pageInit = function() {

		$scope.model.datainit.appURL = $('meta[name="app_url"]').attr('content');

		$("#listReLoading").show();
		$scope.loadList();
		$scope.InnitStore();

		$scope.dtOptions = DTOptionsBuilder.newOptions()
			.withOption('order', [])
        	.withPaginationType('full_numbers')
        	.withDisplayLength(10)
	        .withColReorder()
	        // .withColReorderOrder([0, 1, 2, 3, 4])
	        .withBootstrap()
	        .withBootstrapOptions({
	            pagination: {
	                classes: {
	                    ul: 'pagination pagination-md'
	                }
	            }
	        })
	        .withLanguage({
	            "sEmptyTable":     "Không có dữ liệu.",
	            "sInfo":           "Hiển Thị Từ _START_ Đến _END_ Của _TOTAL_ Kết Quả",
	            "sInfoEmpty":      "Hiển Thị Từ 0 Đến 0 Của 0 Kết Quả",
	            "sInfoFiltered":   "(Đã Lọc Từ Tổng _MAX_ Kết Quả)",
	            "sInfoPostFix":    "",
	            "sInfoThousands":  ",",
	            "sLengthMenu":     "Hiển Thị&nbsp;&nbsp;&nbsp;_MENU_&nbsp;&nbsp;&nbsp;Kết Quả / 1 Trang",
	            "sLoadingRecords": "Đang Tải Dữ Liệu...",
	            "sProcessing":     "Đang Xử Lý Dữ Liệu...",
	            "sSearch":         "Từ Khóa: ",
	            "sZeroRecords":    "Không tìm thấy dữ liệu trùng khớp.",
	            "oPaginate": {
	                "sFirst":    "Đầu",
	                "sLast":     "Cuối",
	                "sNext":     "Tiếp",
	                "sPrevious": "Trước"
	            },
	            "oAria": {
	                "sSortAscending":  ": activate to sort column ascending",
	                "sSortDescending": ": activate to sort column descending"
	            }
	        })
	        .withOption('responsive', true);

	    $scope.dtColumnDefs = [
	        DTColumnDefBuilder.newColumnDef(0).notSortable(),
	    ];
	};
	// loadList
	$scope.loadList = function() {

		$("#listReLoading").show();

		var request = {
	    };

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetRemainAndSaleByBranch", JSON.stringify(request))
	    .then(
	    		function (response) {
                
	                var data = response.data;

			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    		$scope.model.datainit.dtgList = [];
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
			    		warning = warning.replace('["', '').replace('"]', '');
			    		toastr.warning(warning, Languages.common_warning_title);
			    		$scope.model.datainit.dtgList = [];
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		$scope.model.datainit.dtgList = data.success;
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    		$scope.model.datainit.dtgList = [];
			    	}
		    		$("#listReLoading").hide();
			    },
               	function (response) {
               
                  	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$scope.model.datainit.dtgList = [];
					$("#listReLoading").hide();
			    }
		    );
		    //end then
	}
	// apiInnitStore
	$scope.InnitStore = function() {

		$("#listReLoading").show();

		var request = {
	    };

	    $http.post($scope.model.datainit.appURL + "/SystemApi/InnitStore", JSON.stringify(request))
	    .then(
	    		function (response) {
                
	                var data = response.data;

			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    	
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		// toastr.success(data.success, Languages.common_success_title);
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    	}
		    		$("#listReLoading").hide();
			    },
               	function (response) {
                  	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$("#listReLoading").hide();
			    }
		    );
		    //end then
	}
});