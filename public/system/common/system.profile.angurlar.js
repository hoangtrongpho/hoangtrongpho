
/*!
 *
 *  SYSTEM EXE VIETNAM
 *  Developed by Vinh Nguyễn - Ho Chi Minh City - 2016
 *
 *  Skype: enjoyvinh
 *
 */

"use strict";

// app.js
// create angular app
var userApp = angular.module('userApp', ['commonApp', 'bootstrap-switch']);

userApp.factory('UserModels', function() {

	var model={
		datainit : {
			slbListGroup : [],
			useraccountupdate : false,
			appURL : "http://localhost:8000"
		},
	};

	model.request = {
		user_id : "",
		user_account : "",
		auth_id : "",
		user_email : "",
		user_avatar : $('meta[name="app_url"]').attr('content') + "/images/default-image.png",
		user_fullname :  "",
		user_sex : 1,
		user_phone : "",
		user_password : "",
		user_password_confirm : "",
		user_login_count : 0,
		status : 1,
		slbGroup : ""
	};

	model.localLang = {
	    selectAll       : Languages.common_button_select_all,
	    selectNone      : Languages.common_button_deselected_all,
	    reset           : Languages.common_button_cancel,
	    search          : Languages.common_input_label_search,
	    nothingSelected : Languages.common_input_label_select
	}

	return model;
});


// create angular controller
userApp.controller('profileController', function(UserModels, commonFunction, $scope, $rootScope, $http, $filter, $window, $location, $uibModal, toastr) {

	$scope.model = UserModels;

	//Hàm Chức Năng Cho SelectBox
	$scope.slbClear = function()
	{
		angular.forEach($scope.model.datainit.slbListGroup, function(value, key) {
	    	value.ticked = false;
		});
	   	$('#txtAuthId').val(""); 
	   	$('#txtAuthId').trigger('input');
	};

	$scope.slbItemClick = function(item)
	{
	   	$('#txtAuthId').val(item.auth_id); 
   		$('#txtAuthId').trigger('input');

   		angular.forEach($scope.model.datainit.slbListGroup, function(value, key) {
		    if(value.auth_id == item.auth_id)
		    {
		    	value.ticked = true;
		    }
		    else
		    {
		    	value.ticked = false;
		    }
		});
		
	};

	$scope.resetChoiceThumbnail = function() {
    	$("#holder").attr("src",$scope.model.datainit.appURL + "/images/default-image.png");
		$("#thumbnail").val($scope.model.datainit.appURL + "/images/default-image.png");
    	$scope.model.request.user_avatar = $scope.model.datainit.appURL + "/images/default-image.png";
    };

	$scope.pageInit = function(user_id) {

		$scope.model.datainit.appURL = $('meta[name="app_url"]').attr('content');

		$scope.model.request.user_id = user_id;

		$("#addReLoading").show();

		$http.post($scope.model.datainit.appURL + "/SystemApi/apiGetListGroups")
	    	.then(
               	function (response) {
              		var data = response.data;

			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    		$scope.model.datainit.slbListGroup = [];
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		toastr.warning(data.warning, Languages.common_warning_title);
			    		$scope.model.datainit.slbListGroup = [];
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		$scope.model.datainit.slbListGroup = data.success;
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    		$scope.model.datainit.slbListGroup = [];
			    	}
			    },
               	function (response) {
		     
		          	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$scope.model.datainit.slbListGroup = [];
	    		}
	    	);

		var request = {
    		user_id : $scope.model.request.user_id,
    	}

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetUserDetail", JSON.stringify(request))
	    	.then(
               	function (response) {
                  	var data = response.data;

			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    		$scope.model.datainit.slbListGroup = [];
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		toastr.warning(data.warning, Languages.common_warning_title);
			    		$scope.model.datainit.slbListGroup = [];
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		var user = data.success;
			    		$scope.model.request = {
							user_id : user.user_id,
							user_account : user.user_account,
							user_email : user.user_email,
							user_avatar : user.user_avatar,
							user_fullname :  user.user_fullname,
							user_sex : user.user_sex,
							user_password : "",
							user_password_confirm : "",
							user_login_count : user.user_login_count,
							status : user.status,
						};

						angular.forEach($scope.model.datainit.slbListGroup, function(value, key) {
						    if(value.auth_id == user.auth_id)
						    {
						    	value.ticked = true;
						    }
						    else
						    {
						    	value.ticked = false;
						    }
						});
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    		$scope.model.datainit.slbListGroup = [];
			    	}
			    	$("#addReLoading").hide();
			    },
               	function (response) {
                
                  	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$scope.model.datainit.slbListGroup = [];
					$("#addReLoading").hide();
	    		}
	    	);
	};

	$scope.updateItem = function() {

		var confirmModal = bootbox.dialog({
		  	message: commonFunction.languageReplace(Languages.common_confirm_update_question,[' Tài Khoản']),
		  	title: commonFunction.languageReplace(Languages.common_confirm_update_title,[' Tài Khoản']),
		  	closeButton: true,
		  	animate: true,
		  	className: "confirmModal",
		  	backdrop: true,
		  	headerBackground: "bg-yellow",
		  	size: "small",
		  	locale: "vi",
		  	show: true,
		  	buttons: {
		  		cancel : {
			      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
			      	className: "btn-default",
			      	callback: function() {
			        	confirmModal.modal('hide');
			      	}
			    },
			    success: {
		      		label: '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;'+Languages.common_button_update,
			      	className: "btn-warning",
			      	callback: function() {

			      		$("#addReLoading").show();
			      		
			        	var request = {
			        		user_id : $scope.model.request.user_id,
			        		user_account : $scope.model.request.user_account,
							user_email : $scope.model.request.user_email,
							user_avatar : $("#thumbnail").val(),
							user_fullname :  $scope.model.request.user_fullname,
							user_sex : $scope.model.request.user_sex,
							user_password : $scope.model.request.user_password,
							user_password_confirm : $scope.model.request.user_password_confirm,
					    };

					    $http.post($scope.model.datainit.appURL + "/SystemApi/UpdateProfile", JSON.stringify(request))
					    	.then(
				               	function (response) {
				                  	var data = response.data;

							    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
						            {
						                toastr.error(data.messages, Languages.common_error_title);

						                if(data.auth != "" && data.auth != null  && data.auth != undefined)
						                {
						                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
						                }
						            }
							    	else if(data.error != "" && data.error != null  && data.error != undefined)
							    	{
							    		toastr.error(data.error, Languages.common_error_title);
							    	}
							    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
							    	{
							    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
							    		warning = warning.replace('["', '').replace('"]', '');
							    		toastr.warning(warning, Languages.common_warning_title);
							    	}
							    	else if(data.success != "" && data.success != null  && data.success != undefined)
							    	{
							    		toastr.success(data.success, "Thành Công");
							    		location.reload();
							    	}
							    	else
							    	{
						    			if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
							    		{
							    			toastr.error(data.systemerror, Languages.common_error_title);
							    			window.location.replace("./"+data.errorCode);
							    		}
							    		else
							    		{
							    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
							    		}
							    	}
							    	$("#addReLoading").hide();
							    },
					    		function (response) {
				                
				                  	var data = response.data;
									toastr.error(Languages.common_error_exception, Languages.common_error_title);
									$("#addReLoading").hide();
					    		}
					    	);
			      	}
			    }
		  	}
		});
	};

});