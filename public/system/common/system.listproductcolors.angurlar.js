/*!
 *
 *	Develop by Vinh Nguyễn - Ho Chi Minh City - 2017
 * 	Copyright Â© 2016-2017 Skyfire Team. All rights reserved.
 *	Design by Skyfire Team 2.3.3
 *
 *	Phone: 0975 578 276
 *	Skype: enjoyvinh
 *
 */

"use strict";

// app.js
// create angular app
var productColorApp = angular.module('productColorApp', ['commonApp', 'colorpicker.module', 'datatables','datatables.colreorder','datatables.bootstrap','datatables.tabletools']);

productColorApp.factory('ColorModels', function() {

	var model={
		datainit : {
			dtgList : [],
			appURL : "http://localhost:8000"
		},
	};

	model.request = {
		txtId : "",
		txtSlug : "",
		txtName : "",
		pckColor : "",
		pckColor2 : "",
		txtDescription : "",
	};

	return model;
});

// create angular controller
productColorApp.controller('productColorsController', function(ColorModels, commonFunction, $scope, $rootScope, $http, $filter, $window, $location, toastr,
	DTOptionsBuilder, DTColumnDefBuilder,DTColumnBuilder) {

	$scope.model = ColorModels;

	$scope.pageInit = function() {

		$scope.model.datainit.appURL = $('meta[name="app_url"]').attr('content');
		
		$scope.loadList();

	};

	$scope.loadList = function() {

		$("#listReLoading").show();

	    $http.post($scope.model.datainit.appURL + "/SystemApi/GetListProductColors")
	    	.then(
               	function (response) {
                  	var data = response.data;
			    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
		            {
		                toastr.error(data.messages, Languages.common_error_title);

		                if(data.auth != "" && data.auth != null  && data.auth != undefined)
		                {
		                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
		                }
		            }
			    	else if(data.error != "" && data.error != null  && data.error != undefined)
			    	{
			    		toastr.error(data.error, Languages.common_error_title);
			    		$scope.model.datainit.dtgList = [];
			    	}
			    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
			    	{
			    		toastr.warning(data.warning, Languages.common_warning_title);
			    		$scope.model.datainit.dtgList = [];
			    	}
			    	else if(data.success != "" && data.success != null  && data.success != undefined)
			    	{
			    		$scope.model.datainit.dtgList = data.success;
			    	}
			    	else
			    	{
			    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
			    		{
			    			toastr.error(data.systemerror, Languages.common_error_title);
			    			window.location.replace("./"+data.errorCode);
			    		}
			    		else
			    		{
			    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			    		}
			    		$scope.model.datainit.dtgList = [];
			    	}
			    	$("#listReLoading").hide();
			    },
               	function (response) {
                  	debugger;
                  	var data = response.data;
					toastr.error(Languages.common_error_exception, Languages.common_error_title);
					$scope.model.datainit.dtgList = [];
					$("#listReLoading").hide();
			    }
			);

	    $scope.dtOptions = DTOptionsBuilder.newOptions()
			.withOption('order', [])
        	.withPaginationType('full_numbers')
        	.withDisplayLength(50)
	        .withColReorder()
	        .withBootstrap()
	        .withBootstrapOptions({
	            pagination: {
	                classes: {
	                    ul: 'pagination pagination-md'
	                }
	            }
	        })
	        .withLanguage({
	            "sEmptyTable":     "Không có dữ liệu.",
	            "sInfo":           "Hiển Thị Từ _START_ Đến _END_ Của _TOTAL_ Kết Quả",
	            "sInfoEmpty":      "Hiển Thị Từ 0 Đến 0 Của 0 Kết Quả",
	            "sInfoFiltered":   "(Đã Lọc Từ Tổng _MAX_ Kết Quả)",
	            "sInfoPostFix":    "",
	            "sInfoThousands":  ",",
	            "sLengthMenu":     "Hiển Thị&nbsp;&nbsp;&nbsp;_MENU_&nbsp;&nbsp;&nbsp;Kết Quả / 1 Trang",
	            "sLoadingRecords": "Đang Tải Dữ Liệu...",
	            "sProcessing":     "Đang Xử Lý Dữ Liệu...",
	            "sSearch":         "Từ Khóa: ",
	            "sZeroRecords":    "Không tìm thấy dữ liệu trùng khớp.",
	            "oPaginate": {
	                "sFirst":    "Đầu",
	                "sLast":     "Cuối",
	                "sNext":     "Tiếp",
	                "sPrevious": "Trước"
	            },
	            "oAria": {
	                "sSortAscending":  ": activate to sort column ascending",
	                "sSortDescending": ": activate to sort column descending"
	            }
	        })
	        .withOption('responsive', true);

        $scope.dtColumnDefs = [
	        DTColumnDefBuilder.newColumnDef(0).notSortable(),
	        DTColumnDefBuilder.newColumnDef(1).notSortable(),
	    ];
	}

	$scope.removeItem = function(item) {
		
		if (!item.color_id) 
		{
			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			return false;
		}

		var confirmModal = bootbox.dialog({
		  	message: commonFunction.languageReplace(Languages.common_confirm_delete_question,[' Màu "'+item.color_name+'" ']),
		  	title: commonFunction.languageReplace(Languages.common_confirm_delete_title,[' Màu']),
		  	closeButton: true,
		  	animate: true,
		  	className: "confirmModal",
		  	backdrop: true,
		  	headerBackground: "bg-red",
		  	size: "small",
		  	locale: "vi",
		  	show: true,
		  	buttons: {
		  		cancel : {
			      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
			      	className: "btn-default",
			      	callback: function() {
			        	confirmModal.modal('hide');
			      	}
			    },
			    success: {
		      		label: '<i class="fa fa-trash" aria-hidden="true"></i>&nbsp;'+Languages.common_button_remove,
			      	className: "btn-danger",
			      	callback: function() {
			      		$("#listReLoading").show();
			        	var request = {
					        color_id : item.color_id,
					    };
					    $http.post($scope.model.datainit.appURL + "/SystemApi/RemoveProductColor", JSON.stringify(request))
				    		.then(
               					function (response) {
                  					var data = response.data;

							    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
						            {
						                toastr.error(data.messages, Languages.common_error_title);

						                if(data.auth != "" && data.auth != null  && data.auth != undefined)
						                {
						                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
						                }
						            }
							    	else if(data.error != "" && data.error != null  && data.error != undefined)
							    	{
							    		toastr.error(data.error, Languages.common_error_title);
							    	}
							    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
							    	{
							    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
							    		warning = warning.replace('["', '').replace('"]', '');
							    		toastr.warning(warning, Languages.common_warning_title);
							    	}
							    	else if(data.success != "" && data.success != null  && data.success != undefined)
							    	{
							    		toastr.success(data.success, Languages.common_success_title);
							    		$scope.formReset();
							    	}
							    	else
							    	{
							    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
							    		{
							    			toastr.error(data.systemerror, Languages.common_error_title);
							    			window.location.replace("./"+data.errorCode);
							    		}
							    		else
							    		{
							    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
							    		}
							    	}
							    	$("#listReLoading").hide();
							    },
               					function (response) {
				                  	debugger;
				                  	var data = response.data;
									toastr.error(Languages.common_error_exception, Languages.common_error_title);
									$("#listReLoading").hide();
							    }
							);
			      	}
			    },
		  	}
		});
	};

	$scope.addItem = function() {

		if (!$scope.model.request.txtName) 
		{
			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			return false;
		}

		if ($scope.frmAdd.$valid) {
			var confirmModal = bootbox.dialog({
			  	message: commonFunction.languageReplace(Languages.common_confirm_add_question,[' Màu "'+$scope.model.request.txtName+'" ']),
			  	title: commonFunction.languageReplace(Languages.common_confirm_add_title,[' Màu']),
			  	closeButton: true,
			  	animate: true,
			  	className: "confirmModal",
			  	backdrop: true,
			  	headerBackground: "bg-blue",
			  	size: "small",
			  	locale: "vi",
			  	show: true,
			  	buttons: {
			  		cancel : {
				      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
				      	className: "btn-default",
				      	callback: function() {
				        	confirmModal.modal('hide');
				      	}
				    },
				    success: {
			      		label: '<i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;'+Languages.common_button_add,
				      	className: "btn-primary",
				      	callback: function() {

				      		$("#listReLoading").show();

				        	var request = {
						        color_name : $scope.model.request.txtName,
						        color_code : $scope.model.request.pckColor,
						        color_description : $scope.model.request.txtDescription,
						    };

						    $http.post($scope.model.datainit.appURL + "/SystemApi/AddProductColor", JSON.stringify(request))
					    		.then(
               						function (response) {
                  						var data = response.data;

								    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
							            {
							                toastr.error(data.messages, Languages.common_error_title);

							                if(data.auth != "" && data.auth != null  && data.auth != undefined)
							                {
							                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
							                }
							            }
								    	else if(data.error != "" && data.error != null  && data.error != undefined)
								    	{
								    		toastr.error(data.error, Languages.common_error_title);
								    	}
								    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
								    	{
								    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
								    		warning = warning.replace('["', '').replace('"]', '');
								    		toastr.warning(warning, Languages.common_warning_title);
								    	}
								    	else if(data.success != "" && data.success != null  && data.success != undefined)
								    	{
								    		toastr.success(data.success, Languages.common_success_title);
								    		$('#modalAdd').modal('toggle');
								    		$scope.formReset();
								    	}
								    	else
								    	{
								    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
								    		{
								    			toastr.error(data.systemerror, Languages.common_error_title);
								    			window.location.replace("./"+data.errorCode);
								    		}
								    		else
								    		{
								    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
								    		}
								    	}
								    	$("#listReLoading").hide();
								    },
	               					function (response) {
					                  	debugger;
					                  	var data = response.data;
										toastr.error(Languages.common_error_exception, Languages.common_error_title);
										$("#listReLoading").hide();
								    }
								);
				      	}
				    },
			  	}
			});
		}
	};

	$scope.updateItem = function() {

		if (!$scope.model.request.txtId || !$scope.model.request.txtSlug || !$scope.model.request.txtName) 
		{
			toastr.error(Languages.common_error_exception, Languages.common_error_title);
			return false;
		}

		if ($scope.frmUpdate.$valid) {
			var confirmModal = bootbox.dialog({
			  	message: commonFunction.languageReplace(Languages.common_confirm_update_question,[' Màu "'+$scope.model.request.txtName+'" ']),
			  	title: commonFunction.languageReplace(Languages.common_confirm_update_title,[' Màu']),
			  	closeButton: true,
			  	animate: true,
			  	className: "confirmModal",
			  	backdrop: true,
			  	headerBackground: "bg-yellow",
			  	size: "small",
			  	locale: "vi",
			  	show: true,
			  	buttons: {
			  		cancel : {
				      	label: '<i class="fa fa-ban" aria-hidden="true"></i>&nbsp;'+Languages.common_button_cancel,
				      	className: "btn-default",
				      	callback: function() {
				        	confirmModal.modal('hide');
				      	}
				    },
				    success: {
			      		label: '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;'+Languages.common_button_update,
				      	className: "btn-warning",
				      	callback: function() {

				      		$("#listReLoading").show();

				        	var request = {
				        		color_id : $scope.model.request.txtId,
				        		color_slug : $scope.model.request.txtSlug,
						        color_name : $scope.model.request.txtName,
						        color_code : $scope.model.request.pckColor,
						        color_description : $scope.model.request.txtDescription,
						    };

						    $http.post($scope.model.datainit.appURL + "/SystemApi/UpdateProductColor", JSON.stringify(request))
					    		.then(
               						function (response) {
                  						var data = response.data;

								    	if(data.auth != "" && data.auth != null  && data.auth != undefined && data.auth)
							            {
							                toastr.error(data.messages, Languages.common_error_title);

							                if(data.auth != "" && data.auth != null  && data.auth != undefined)
							                {
							                	$window.location.href = $scope.model.datainit.appURL + data.redirect;
							                }
							            }
								    	else if(data.error != "" && data.error != null  && data.error != undefined)
								    	{
								    		toastr.error(data.error, Languages.common_error_title);
								    	}
								    	else if(data.warning != "" && data.warning != null  && data.warning != undefined)
								    	{
								    		var warning = String(data.warning[Object.keys(data.warning)[0]]);
								    		warning = warning.replace('["', '').replace('"]', '');
								    		toastr.warning(warning, Languages.common_warning_title);
								    	}
								    	else if(data.success != "" && data.success != null  && data.success != undefined)
								    	{
								    		toastr.success(data.success, Languages.common_success_title);
								    		$('#modalUpdate').modal('toggle');
								    		$scope.formReset();
								    	}
								    	else
								    	{
								    		if(data.systemerror != "" && data.systemerror != null  && data.systemerror != undefined)
								    		{
								    			toastr.error(data.systemerror, Languages.common_error_title);
								    			window.location.replace("./"+data.errorCode);
								    		}
								    		else
								    		{
								    			toastr.error(Languages.common_error_exception, Languages.common_error_title);
								    		}
								    	}
								    	$("#listReLoading").hide();
								    },
	               					function (response) {
					                  	debugger;
					                  	var data = response.data;
										toastr.error(Languages.common_error_exception, Languages.common_error_title);
										$("#listReLoading").hide();
								    }
								);
				      	}
				    }
			  	}
			});
		}
	};

	$('#modalAdd').on('shown.bs.modal', function () {
	    $('#txtName').focus();

		$('#txtName').val("");
		$('#pckColor').val("");
		$('#txtDescription').val("");

	    $scope.frmAdd.$setPristine();
		$scope.frmAdd.$setUntouched();
		$scope.frmUpdate.$setPristine();
		$scope.frmUpdate.$setUntouched();
	});

	$('#modalUpdate').on('shown.bs.modal', function () {
	    $('#txtName').focus();

	    $scope.frmAdd.$setPristine();
		$scope.frmAdd.$setUntouched();
		$scope.frmUpdate.$setPristine();
		$scope.frmUpdate.$setUntouched();
	})   

	$scope.openUpdateItem = function(item) {

		$scope.model.request = {
			txtId : item.color_id,
			txtSlug : item.color_slug,
			txtName : item.color_name,
			pckColor : item.color_primary_code,
			pckColor2 : item.color_secondary_code,
			txtDescription : item.color_description,
		};

		$('#modalUpdate').modal('toggle');

		$scope.frmAdd.$setPristine();
		$scope.frmAdd.$setUntouched();
		$scope.frmUpdate.$setPristine();
		$scope.frmUpdate.$setUntouched()
	};

	$scope.formReset = function() {

		$scope.pageInit();

		$scope.model.request = {
			txtId : "",
			txtSlug : "",
			txtName : "",
			pckColor : "",
			pckColor2 : "",
			txtDescription : "",
		};

		$scope.frmAdd.$setPristine();
		$scope.frmAdd.$setUntouched();
		$scope.frmUpdate.$setPristine();
		$scope.frmUpdate.$setUntouched();
	};
});
